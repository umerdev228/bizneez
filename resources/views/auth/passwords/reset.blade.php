<script type="text/javascript">
    window.history.forward();
    function noBack() {
        window.history.forward();
    }
</script>
<style >
    .form-control{
        margin-left: -14px;
        border-radius: 0px;
    }
</style>
@extends('layout.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="support-company-area fix">
                    <div class="container">
                        <div class="row align-items-center">
                            <div class="col-xl-5 col-lg-6">
                                <div class="support-location-img">
                                    <img src="{{ asset('/theme/assets/img/login.png') }}" alt="" >
                                </div>
                            </div>
                            <div class="col-xl-7 col-lg-6">
                                <div class="right-caption">
                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    @if(session()->has('message'))
                                        <div class="alert alert-danger col-md-6" id="myElem">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            {{ session()->get('message') }}
                                        </div>
                                    @endif
                                        <form class="form-contact contact_form"   id="frmForgot" method="POST" action="{{ route('password.update') }}"  >
                                            @csrf
                                            <div class="row"></div>
                                            <h2>Reset Password</h2>
                                            <input type="hidden" name="email" value="{{ $email }}">
                                            <div class="input-group col-12" >
                                                <input  type="password" class="form-control @error('email') is-invalid @enderror" value="" id="password" placeholder="Enter Password"  name="password" autocomplete="new-password"   minlength="6" required>
                                                @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{$errors->first('password')}}</strong>
                                                </span>
                                                @enderror
                                                <i  type="button" id="pass-status" class="fa fa-eye" aria-hidden="true" onClick="viewPassword()" style="background: #ccc; border-top-right-radius: 5px; border-bottom-right-radius: 5px;"></i>
                                            </div>
                                            <br>
                                            <div class="input-group col-12">
                                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation"  placeholder="Enter confirm Password" required autocomplete="new-password">
                                                <i  type="button" id="confirm-pass-status" class="fa fa-eye" aria-hidden="true" onClick="viewPassword1()" style="background: #ccc; border-top-right-radius: 5px; border-bottom-right-radius: 5px;"></i>
                                                @error('password_confirmation')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('password_confirmations') }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                            <br>
                                            <div class="form-group mt-1">
                                                <button type="submit" class="genric-btn info" name="btnsubmit" style="width: 320px;">
                                                    {{ __('Reset Password') }}
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @endsection
<style>
    #preloader-active{
    display: none;
}
.input-group i {
background: #ccc;
margin-left:  0px;
 padding: 8px;
}


</style>
<script>


 
 

function viewPassword()
{
  var passwordInput = document.getElementById('password');
  var passStatus = document.getElementById('pass-status');
 
  if (passwordInput.type == 'password'){
    passwordInput.type='text';
    passStatus.className='fa fa-eye-slash';
    
  }
  else{
    passwordInput.type='password';
    passStatus.className='fa fa-eye';
  }
}


function viewPassword1()
{
  var passwordInput = document.getElementById('password-confirm');
  var passStatus = document.getElementById('confirm-pass-status');
 
  if (passwordInput.type == 'password'){
    passwordInput.type='text';
    passStatus.className='fa fa-eye-slash';
    
  }
  else{
    passwordInput.type='password';
    passStatus.className='fa fa-eye';
  }

}
  $(document).ready(function(){
          // alert('asdsa');
        $("#myElem").show();
        setTimeout(function() 
          { $("#myElem").hide(); 
          }, 8000);
          })
</script>
<style>
    .fa-eye, .fa-eye-slash{
    border: 1px solid #ccc;
    padding: 9px 13px;
    background-color: rgb(255, 255, 255);
  }

  
 </style>  
