<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Twilio\Jwt\AccessToken;
use Twilio\Jwt\Grants\VideoGrant;

class AccessTokenController extends Controller
{
    //
    public function generate_token(Request $request)
    {
        // Substitute your Twilio Account SID and API Key details
        $accountSid = "AC866b414f0e54b0ef0b93a8aa3fcc833e";
        $apiKeySid = "SK3ae3d2ffafe5950b77cbb06449f72c1b";
        $apiKeySecret = "3m31XK1tdqwIUvfevDo1efqhRZmWalnV";

        $identity = uniqid();
        $identity = $request['code'];

//        dd($accountSid, $apiKeySid, $apiKeySecret, $identity);

        // Create an Access Token
        $token = new AccessToken(
            $accountSid,
            $apiKeySid,
            $apiKeySecret,
            3600,
            $identity
        );

        // Grant access to Video
        $grant = new VideoGrant();
        $grant->setRoom('myRoom');
        $token->addGrant($grant);

        // Serialize the token as a JWT
        echo $token->toJWT();
    }

    public function generate_token_get()
    {
        // Substitute your Twilio Account SID and API Key details
        $accountSid = "AC866b414f0e54b0ef0b93a8aa3fcc833e";
        $apiKeySid = "SK3ae3d2ffafe5950b77cbb06449f72c1b";
        $apiKeySecret = "3m31XK1tdqwIUvfevDo1efqhRZmWalnV";

        $identity = uniqid();

//        dd($accountSid, $apiKeySid, $apiKeySecret, $identity);

        // Create an Access Token
        $token = new AccessToken(
            $accountSid,
            $apiKeySid,
            $apiKeySecret,
            3600,
            $identity
        );

        // Grant access to Video
        $grant = new VideoGrant();
        $grant->setRoom('myRoom');
        $token->addGrant($grant);

        // Serialize the token as a JWT
        echo $token->toJWT();
    }
}
