<!DOCTYPE html>
<html dir="ltr" lang="en-US">
  <head>
  <meta charset="utf-8" />
  <title>Bizneez.net Catalogue</title>
<style>
			 	body {
			  counter-reset: chapternum figurenum;
			}
				.page-break {
					page-break-after: always;
				}
			 
			 @page :right {
			  margin-left: 0cm;
			  margin-right: 0cm;
			  margin-top:0cm;
			  margin-bottom: 0cm;
			  width:100%; 
			}
			@page {
				@footnote {
			 
			    padding-top: 3pt;
			    margin-top:-10px;
			  }
			 @page :left {
			  margin-left: 0cm;
			  margin-top:0cm;
			  margin-bottom: 0cm;
			  margin-right: 0cm;
			}

			.center {
			  display: block; 
			  margin: 15% 35%;
			  
			 
			  color: #fff;
			  height: 60px;  
			}
</style>
  </head>
  <body style="background-color: #404040; margin:0px; pading:0px">
  <img src="{{$data['logo']}}" alt="Paris" class="center">

@for($i = 1; $i <= $data['page']; $i++)
@php
				$company = \App\Company::where('id', $data['company'.$i])->first();

                $image1 = '';
                $image2 = '';
                $image3 = '';
                $image4 = '';
                

                if ($data['imageValue-'.$i.'-1']) {
                    $image1 = \App\Media::where('id', $data['imageValue-'.$i.'-1'])->first()->path;
                    }
                if ($data['imageValue-'.$i.'-2']) {
                    $image2 = \App\Media::where('id', $data['imageValue-'.$i.'-2'])->first()->path;
                    }
                if ($data['imageValue-'.$i.'-3']) {
                    $image3 = \App\Media::where('id', $data['imageValue-'.$i.'-3'])->first()->path;
                    }
                if ($data['imageValue-'.$i.'-4']) {
                    $image4 = \App\Media::where('id', $data['imageValue-'.$i.'-4'])->first()->path;
                    }
                
			@endphp


 
 
<table style="width:100%;" border="0"> 
	<tr>
		<td>
			<table>
				<?php
				if($data['imageValue-'.$i.'-1']!='' && $data['imageValue-'.$i.'-2']=='' && $data['imageValue-'.$i.'-3']=='' && $data['imageValue-'.$i.'-4']==''){ 
				?>				
				 <tr>
						<td>
							@if ($data['imageValue-'.$i.'-1'])
								<img style="width:1150px; height:780px; margin-left:-15px"  src="{{public_path($image1)}}">
							@endif
						</td>
				</tr>
			<?php }
			if($data['imageValue-'.$i.'-2']!='' && $data['imageValue-'.$i.'-1']!='' && $data['imageValue-'.$i.'-3']=='' && $data['imageValue-'.$i.'-4']==''){ 
			?>		
					 <tr>
						<td>
							@if ($data['imageValue-'.$i.'-1'])
								<img style="width:1150px; height:387px; margin-left:-15px"  src="{{public_path($image1)}}">
							@endif
						</td> </tr>
						<tr>
						<td>
							@if ($data['imageValue-'.$i.'-2'])
								<img style="width:1150px; height:387px; margin-left:-15px"  src="{{public_path($image2)}}">
							@endif
						</td> 
					</tr>

			<?php }  

			if($data['imageValue-'.$i.'-1']!='' && $data['imageValue-'.$i.'-2']!='' && $data['imageValue-'.$i.'-3']!='' && $data['imageValue-'.$i.'-4']==''){ 
			?>		
					 <tr>
						<td>
							@if ($data['imageValue-'.$i.'-1'])
								<img style="width:570px; height:387px; margin-left:-15px"  src="{{public_path($image1)}}">
							@endif
						</td> 
						<td>
						    @if ($data['imageValue-'.$i.'-2'])
								<img style="width:570px; height:387px; margin-left:-18px"    src="{{public_path($image2)}}">
							@endif
							
						</td> 
					</tr>
					<tr>
						<td colspan="2">
							@if ($data['imageValue-'.$i.'-3'])
								<img style="width:1150px; height:390px; margin-left:-15px"   src="{{public_path($image3)}}">
							@endif
						</td>
					</tr>	

			<?php } 
			if($data['imageValue-'.$i.'-2']!='' && $data['imageValue-'.$i.'-1']!='' && $data['imageValue-'.$i.'-3']!='' && $data['imageValue-'.$i.'-4']!=''){ 
			?>

					<tr>
						<td>
							@if ($data['imageValue-'.$i.'-1'])
								<img style="width:570px; height:387px; margin-left:-15px"   src="{{public_path($image1)}}">
							@endif
						</td>
						<td>
							@if ($data['imageValue-'.$i.'-2'])
								<img style="width:570px; height:387px; margin-left:-18px"    src="{{public_path($image2)}}">
							@endif
						</td> 
					</tr> 
					<tr>
						<td>
							@if ($data['imageValue-'.$i.'-3'])
								<img style="width:570px; height:387px; margin-left:-15px"    src="{{public_path($image3)}}">
							@endif
						</td>
						<td>
							@if ($data['imageValue-'.$i.'-4'])
								<img style="width:570px; height:387px; margin-left:-18px"    src="{{public_path($image4)}}">
							@endif
						</td> 
					</tr> 


					 <?php }?>
			</table>
		</td>
	</tr>
 
 
</table>


 @endfor


 
<br><br><br><br><br><br><br>
<br><br><br><br><br><br><br>
<br><br><br><br><br><br><br>
<br><br><br><br><br><br><br>
<br><br><br><br><br><br><br>
<br><br><br>
<div style="color:white;">
	<img src="{{$data['logo']}}" style="width:250px; height: 40px" alt="bizneez" >	 
 
</div>
 


</body> 

 </html>