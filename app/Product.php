<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //

    protected $guarded = [];

    public function media() {
        return $this->hasMany(Media::class, 'item_id', 'id')->where('item_type', 'product');
    }

    public function colors() {
        return $this->hasMany(Media::class, 'item_id', 'id')->where('item_type', 'product_color');
    }

    public function categories() {
        return $this->belongsToMany(Category::class, 'product_categories', 'product_id', 'category_id');
    }

    public function user() {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function manufacturer() {
        return $this->hasOne(User::class, 'id', 'user_id')->with('profile');
    }
}
