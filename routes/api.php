<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/gallery/files/upload', 'MediaController@storeApiMediaFiles');
Route::post('/gallery/file/upload', 'MediaController@storeApiMediaFile');

Route::get('/admin/getAllUsers', 'UserController@getAllUsers');
Route::get('/admin/find/event', 'UserController@getEventById');

Route::get('access_token', 'API\AccessTokenController@generate_token');
//Route::get('access_token', 'API\GenerateAccessTokenController@generate_token');

Route::get('getAllColorCodes', 'ColorCodeController@getAllColorCodes');
