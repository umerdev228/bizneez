<?php

namespace App\Http\Controllers;

use App\SocialMediaAdvertisementComment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SocialMediaAdvertisementCommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        SocialMediaAdvertisementComment::create([
            'user_id' => Auth::id(),
            'post_id' => $request->post_id,
            'comment' => $request->comment,
        ]);
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SocialMediaAdvertisementComment  $socialMediaAdvertisementComment
     * @return \Illuminate\Http\Response
     */
    public function show(SocialMediaAdvertisementComment $socialMediaAdvertisementComment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SocialMediaAdvertisementComment  $socialMediaAdvertisementComment
     * @return \Illuminate\Http\Response
     */
    public function edit(SocialMediaAdvertisementComment $socialMediaAdvertisementComment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SocialMediaAdvertisementComment  $socialMediaAdvertisementComment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SocialMediaAdvertisementComment $socialMediaAdvertisementComment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SocialMediaAdvertisementComment  $socialMediaAdvertisementComment
     * @return \Illuminate\Http\Response
     */
    public function destroy(SocialMediaAdvertisementComment $socialMediaAdvertisementComment)
    {
        //
    }
}
