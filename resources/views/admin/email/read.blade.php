@extends('includes.meeting_section.master')

@section('content')
    <div  style="margin-top: 70px !important;"></div>
<div class="row">

    <!-- Right Sidebar -->
    <div class="col-lg-12">
        <div class="card-box">
            <!-- Left sidebar -->
            @include('admin.email.left-side')
            <!-- End Left sidebar -->

            <div class="inbox-rightbar">

                <div class="" role="toolbar">
                    <div class="btn-group">
                        <form action="{{route('email.delete')}}" method="post">
                            @csrf
                            <input type="hidden" name="id" value="{{$email->id}}">
                            <button type="submit" class="btn btn-sm btn-light waves-effect" data-toggle="tooltip" data-placement="top" title="Delete">
                                <i class="mdi mdi-archive font-18 vertical-middle"></i>
                            </button>
                        </form>
                        {{--                    <form action="{{route('email.action')}}" method="post">--}}
                        {{--                        @csrf--}}
                        {{--                        <input type="hidden" name="ids" id="ids">--}}
                        {{--                        <button type="submit" class="btn btn-sm btn-light waves-effect" data-toggle="tooltip" data-placement="top" title="Tooltip on top">--}}
                        {{--                            <i class="mdi mdi-alert-octagon font-18 vertical-middle"></i>--}}
                        {{--                        </button>--}}
                        {{--                    </form>--}}
                        <form action="{{route('email.draft')}}" method="post">
                            @csrf
                            <input type="hidden" name="id" value="{{$email->id}}">
                            <button type="submit" class="btn btn-sm btn-light waves-effect" data-toggle="tooltip" data-placement="top" title="Add To Draft">
                                <i class="mdi mdi-delete-variant font-18 vertical-middle"></i>
                            </button>
                        </form>
                        <form action="{{route('admin.email.reply')}}" method="post">
                            @csrf
                            <input type="hidden" name="id" value="{{$email->id}}">
                            <button type="submit" class="btn btn-sm btn-light waves-effect" data-toggle="tooltip" data-placement="top" title="Reply">
                                <i class="mdi mdi-reply font-18 vertical-middle"></i>
                            </button>
                        </form>
                    </div>
{{--                    <div class="btn-group">--}}
{{--                        <button type="button" class="btn btn-sm btn-light dropdown-toggle waves-effect" data-toggle="dropdown" aria-expanded="false">--}}
{{--                            <i class="mdi mdi-folder font-18 vertical-middle"></i>--}}
{{--                            <b class="caret m-l-5"></b>--}}
{{--                        </button>--}}
{{--                        <div class="dropdown-menu">--}}
{{--                            <span class="dropdown-header">Move to</span>--}}
{{--                            <a class="dropdown-item" href="javascript: void(0);">Social</a>--}}
{{--                            <a class="dropdown-item" href="javascript: void(0);">Promotions</a>--}}
{{--                            <a class="dropdown-item" href="javascript: void(0);">Updates</a>--}}
{{--                            <a class="dropdown-item" href="javascript: void(0);">Forums</a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="btn-group">--}}
{{--                        <button type="button" class="btn btn-sm btn-light dropdown-toggle waves-effect" data-toggle="dropdown" aria-expanded="false">--}}
{{--                            <i class="mdi mdi-label font-18 vertical-middle"></i>--}}
{{--                            <b class="caret m-l-5"></b>--}}
{{--                        </button>--}}
{{--                        <div class="dropdown-menu">--}}
{{--                            <span class="dropdown-header">Label as:</span>--}}
{{--                            <a class="dropdown-item" href="javascript: void(0);">Updates</a>--}}
{{--                            <a class="dropdown-item" href="javascript: void(0);">Social</a>--}}
{{--                            <a class="dropdown-item" href="javascript: void(0);">Promotions</a>--}}
{{--                            <a class="dropdown-item" href="javascript: void(0);">Forums</a>--}}
{{--                        </div>--}}
{{--                    </div>--}}

{{--                    <div class="btn-group">--}}
{{--                        <button type="button" class="btn btn-sm btn-light dropdown-toggle waves-effect" data-toggle="dropdown" aria-expanded="false">--}}
{{--                            <i class="mdi mdi-dots-horizontal font-18 vertical-middle"></i> More--}}
{{--                        </button>--}}
{{--                        <div class="dropdown-menu">--}}
{{--                            <span class="dropdown-header">More Option :</span>--}}
{{--                            <a class="dropdown-item" href="javascript: void(0);">Mark as Unread</a>--}}
{{--                            <a class="dropdown-item" href="javascript: void(0);">Add to Tasks</a>--}}
{{--                            <a class="dropdown-item" href="javascript: void(0);">Add Star</a>--}}
{{--                            <a class="dropdown-item" href="javascript: void(0);">Mute</a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                </div>

                <div class="mt-4">
                    <h5>{{$email->subject}}</h5>

                    <hr/>

                    <div class="media mb-4 mt-1">
{{--                        <img class="d-flex mr-3 rounded-circle thumb-sm" src="{{asset('assets/images/users/avatar-2.jpg')}}" alt="Generic placeholder image">--}}
                        <div class="media-body">
{{--                            <span class="float-right">07:23 AM</span>--}}
                            <h6 class="m-0">{{$email->contact->name}}</h6>
                            <small class="text-muted">To: {{$email->to}}</small>
                        </div>
                    </div>
                    {!! $email->body !!}
                    {{$email->signature}}
                    <hr/>
                </div>


            </div>

            <div class="clearfix"></div>
        </div>

    </div> <!-- end Col -->

</div><!-- End row -->
@endsection