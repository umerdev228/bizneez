@extends('layout.app')
@section('content') 
<main>
    <!-- Signup Start -->
    <div class="testimonial-area testimonial-padding pb-0 gray-bg">
        <div class="col-lg-12">
            <div class="section-tittle text-center">
                <h2>Try TMS for 15 days</h2>
            </div>
        </div>
        <div class="container-fluid">
            <!-- Signup contents -->
            <div class="row d-flex justify-content-center sect-bg">
                <div class="col-lg-5">
                    
                    {{-- {{ Form::open(array('url' => 'foo/bar', 'method'=>'post', 'id'=>'contactForm', 'class'=>'form-contact contact_form'  )) }}   --}}
                {{--     <form class="form-contact contact_form" action="{{url('merchant_new_register')}}" method="post" id="signinForm" >
                        @csrf
 --}}
  @if(session()->has('message'))
    <div class="alert alert-success"  >
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
        {{ session()->get('message') }}
    </div>
    @endif
    @if(session()->has('error'))
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
        {{ session()->get('error') }}
    </div>
    @endif
    

                    <form method="POST" action="{{ route('merchant_new_register') }}"> 
                    @csrf


                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input value="{{ old('fname') }}" class="form-control valid" name="fname" id="fname" type="text" onfocus="this.placeholder = ''" onblur="this.placeholder ='First Name'" placeholder="First Name" pattern="[A-Za-z\s]+"    maxlength="20" required="required" />
                                    @if ($errors->has('fname'))
                                    <span class="alert-dange">{{ $errors->first('fname') }}</span>
                                    @endif   
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input  value="{{ old('lname') }}" class="form-control valid" name="lname" id="lname" type="text" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Last Name'" placeholder="Last Name" pattern="[A-Za-z\s]+"   maxlength="20" required="required" />
                                     @if ($errors->has('lname'))
                                                <span class="alert-dange">{{ $errors->first('lname') }}</span>
                                     @endif  
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                <input   class="form-control valid"  value="{{ old('email') }}" name="email" id="email" type="email" onfocus="this.placeholder = ''"  onblur="this.placeholder = 'Email'" placeholder="Email" required="required"/>
                                 @if ($errors->has('email'))
                                 <span class="alert-dange" style="color: red">{{ $errors->first('email') }}</span>
                                @endif 
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                <input value="{{ old('company') }}" class="form-control" name="company" id="company" type="text" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Company'" placeholder="Company" pattern="[A-Za-z0-9@.\s]+"  required="required"   maxlength="30" />
                                @if ($errors->has('company'))
                                <span class="alert-dange">{{ $errors->first('company') }}</span>
                                @endif  
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                <input  value="{{ old('phone') }}" class="form-control" name="phone" id="phone" pattern="^[0-9-+\s()]*$" minlength="4"  maxlength="15" type="tel" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Phone'" placeholder="Phone" required="required" />
                                @if ($errors->has('phone'))
                                <span class="alert-danger">{{ $errors->first('phone') }}</span>
                                @endif  
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group mt-5">
                             <input class="genric-btn info  primary-bg   w-100 btn_1" type="submit" value="SIGN UP FOR FREE">
                            <a href="{{ url('signin')}}"><p>if you have already Signup?</p></a>
                            
                                <!-- <a href="#">terms</a> and acknowledge reading  
                                our <a href="#">privacy notice.</a>-->
                             
                        </div>
                    </form>
                </div>
                
            </div>
        </div>
    </div>
    <!-- Signup End -->
    
</main>
@endsection
 