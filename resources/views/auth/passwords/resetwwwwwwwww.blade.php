@extends('layout.app')
@section('content')

   <div class=support-location-img style="margin-top: -60px; margin-left: 20px;">
    <img src="https://tms.cybexo.net/theme/assets/images/cybexo_logo.png" height="69" >
  </div>
 <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

                        <div class="support-location-img">
                            <img src="theme/assets/img/login.png" alt="" width="50px">
                        </div>

  


                     <div class="alert alert-danger msg_password_mismatch" role="alert" style="margin-left: 50px;">
                       <button type="button" class="close" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                      </button>
                      Password and Confirm password is not matched.
                      </div>

                    <form onSubmit="return validatePassword();"  id="frmForgot" method="POST" action="{{ route('password.update') }}">
                        @csrf

                        <input type="hidden" name="token" value="{{ $token }}">
                       
                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>


                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" readonly="readonly" required autocomplete="email" autofocus >

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                      
                                <div class="input-group" >
                                <label for="password" class="col-md-4 col-form-label text-md-right"   >{{ __('Password') }}</label>
                                <input  type="password" class="form-control @error('email') is-invalid @enderror" id="password" placeholder="Enter Password"  name="password" autocomplete="new-password"   minlength="6" required="" style="border-radius: 0.25rem " >
                                 @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{$errors->first('password')}}</strong>
                                    </span>
                                @enderror
                                <i  type="button" id="pass-status" class="eye fa fa-eye" aria-hidden="true" onClick="viewPassword()" style=" padding-left: 15px"   ></i>

                               
                                </div>

                            <br>
                            <div class="input-group">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>
                                  <input id="password-confirm" type="password" class="form-control" name="password_confirmation"  placeholder="Enter confirm Password" required autocomplete="new-password" style="border-radius: 0.25rem;">
                                    <i  type="button" id="confirm-pass-status" class="eye fa fa-eye" aria-hidden="true" onClick="viewPassword1()" style="padding-left: 15px"  ></i>
                                     @error('password_confirmation')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password_confirmations') }}</strong>
                                    </span>
                                    @enderror
                            </div>
                            <br>
                            
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Reset Password') }}
                                </button>
                            </div>
                                            </form>
                </div>
            </div>
        </div>
    </div>
</div>

    @endsection
<style>
    #preloader-active{
    display: none;
}
.input-group i {
background: #ccc;
margin-right: 120px;l
 padding: 8px;
}


</style>
<script>

  
function validatePassword() {
    if($('#password').val() != $('#password_confirmation').val() ){
            $('.msg_password_mismatch').fadeIn(2000);
            return false;
            e.preventDefault();
            
        }
        $( "#frmForgot" ).submit();



        var validator = $("#frmForgot").validate({
            rules: {
                password: "required",
                confirmpassword: {
                    equalTo: "#password_confirmation"
                }
            },
            messages: {
                password: " Enter Password",
                confirmpassword: " Enter Confirm Password Same as Password"
            }
        });
        if (validator.form()) {
            alert('Sucess');
        }

       
    }

 

    

function viewPassword()
{
  var passwordInput = document.getElementById('password');
  var passStatus = document.getElementById('pass-status');
 
  if (passwordInput.type == 'password'){
    passwordInput.type='text';
    passStatus.className='fa fa-eye-slash';
    
  }
  else{
    passwordInput.type='password';
    passStatus.className='fa fa-eye';
  }
}


function viewPassword1()
{
  var passwordInput = document.getElementById('password-confirm');
  var passStatus = document.getElementById('confirm-pass-status');
 
  if (passwordInput.type == 'password'){
    passwordInput.type='text';
    passStatus.className='fa fa-eye-slash';
    
  }
  else{
    passwordInput.type='password';
    passStatus.className='fa fa-eye';
  }

}
</script>
<style>
   .fa-eye, .fa-eye-slash{
    border: 1px solid #ccc;
    padding: 10px;
     width: 57px; 
    box-shadow: 0px 6px 29px 0px rgba(36,43,94,0.08);
    background-color: rgb(255, 255, 255);
    padding: 8px;
  }
 </style>  
