<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    //
    protected $guarded = [];


    public function comments() {
        return $this->hasMany(Comment::class, 'event_id')->with('user')->where('type', 'appointment');
    }
}
