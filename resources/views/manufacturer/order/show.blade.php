@extends('admin.layouts.app')
@section('content')
    @include('admin.layouts.topbar')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    @if(session()->has('message'))
                        <div class="alert alert-success mt-5" id="myElem" >
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-12 mt-5">
                            <div class="card">
                                <div class="card-body">
                                    @if($order->status == 'approve')
                                        <form action="{{ route('export.excel.sheet') }}" method="post">
                                            @csrf
                                            <input name="products" value="{{ $products }}" type="hidden">
                                            <input name="order_id" value="{{ $order_id }}" type="hidden">
                                            <input type="submit" class="btn btn-success" value="Export PDF Invoice">
                                        </form>
                                    @endif
                                </div>
                                <div class="card-body">
                                    <table class="table table-hover m-0 tickets-list table-actions-bar dt-responsive nowrap" id="normal-table">
                                        <thead>
                                        <tr>
                                            <th>Image</th>
                                            <th>Id</th>
                                            <th>Name</th>
                                            <th>Discount</th>
                                            <th>Quantity</th>
                                            <th>Manufacturer </th>
                                            <th>Price</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($products as $product)
                                            @if($product->user_id == Auth::id())
                                                <tr>
                                                    <td>
                                                        <img src="{{count($product->media) > 0 ? asset($product->media[0]->path) : ''}}" alt="">
                                                    </td>
                                                    <td>{{$product->id}}</td>
                                                    <td>{{$product->name}}</td>
                                                    <td><span>&#163;</span>{{$product->discount}}</td>
                                                    <td>{{$product->pivot->quantity}}</td>

                                                    <td>{{$product->manufacturer->name}}</td>
                                                    <td><span>&#163;</span>{{$product->price}}</td>
                                                </tr>
                                            @endif
                                        @endforeach
                                        <tfoot>
                                        <tr>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th> </th>
                                            <th>Total: {{ $total }}</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div> <!-- end card -->
                            </div><!-- end col -->
                        </div><!-- end col -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('admin.layouts.footer')
@endsection
