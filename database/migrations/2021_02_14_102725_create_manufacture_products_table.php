<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateManufactureProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manufacture_products', function (Blueprint $table) {
            $table->id();
             $table->integer('manfacture_categories_id')->nullable();
             $table->integer('paranet_product_id')->nullable();
               $table->string('product_image')->nullable();
            $table->string('product_name')->nullable();
            $table->string('size')->nullable();
            $table->string('color_code')->nullable();
            $table->string('retail_price1')->nullable();
            $table->string('retail_price2')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('manufacture_products');
    }
}
