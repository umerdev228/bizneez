<?php

namespace App\Http\Controllers;

use App\History_log;
use Illuminate\Http\Request;
use Auth;
use App\Merchant;
use App\Notification;


class NotficationController extends Controller
{

   public function __construct()
    {
            $this->middleware('auth');
    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Notfication  $notfication
     * @return \Illuminate\Http\Response
     */
    public function show(Notfication $notfication)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Notfication  $notfication
     * @return \Illuminate\Http\Response
     */
    public function edit(Notfication $notfication)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Notfication  $notfication
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Notfication $notfication)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Notfication  $notfication
     * @return \Illuminate\Http\Response
     */
    public function destroy(Notfication $notfication)
    {
        //
    }



    //notification_counter_update
        public function notification_counter_update()
    {
         
           /*$notificatoin = Notificatoin::all(); 
           $notificatoin->intstatus = 1;
           $notificatoin->save();
*/
        //   Notification::where('instatus', '=', 0)->update(['instatus' =>1]);
            $notification = \App\History_log::where('intstatus',0)->orderBy('id', 'desc')->get();
            return count($notification);
            
}
  public function notification()
    {

         $notification_data =  History_log::all();
         $notification = History_log::where('user_id', Auth::user()->id);
     

            $data= [
                    'user' => $notification,
                    'notification_data' => $notification_data,
                    'breadcrum' => 'Dashboard/Notification', 
                    ];
                  
                   return view('admin.notification')->with($data); 
       
    }  




}