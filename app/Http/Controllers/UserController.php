<?php

namespace App\Http\Controllers;
use App\Profile;
use Illuminate\Support\Facades\File;
//use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Response;
//use App\Customer;
use App\User;
use App\Merchant;
//use App\Timezone;
use Auth;
use Mail;
use Stripe;
use App\Packages;
use App\PaymentHistory;
use App\Notification;
use App\History_log;
use App\Setting;
//use App\History;


class UserController extends Controller {

     /**
     * Create a new controller instance.
     *
     * @return void
     */

     public function __construct()
     {
//         $this->middleware('auth');
     }

     public function password(Request $request){
         $breadcrumb = "Account Settings";
         $user = User::where('id', Auth::id())->with('profile')->first();
         return view('admin.profile.password', ['user' => $user, 'breadcrumb' => $breadcrumb]);
     }

     public function edit() {
         $user = User::where('id', \Illuminate\Support\Facades\Auth::id())->first();
         return view('admin.profile.edit', ['user' => $user]);
     }

     public function pricing(){
         $packages = Packages::all();
         $data = ['packages' => $packages,];
         return view('pricing')->with($data);
     }

     /**
     * Insert resource in database/create new agent.
     *
     * @return \Illuminate\Http\Response
     */

     public function update(Request $request)
     {
         $user = User::where('id', \Illuminate\Support\Facades\Auth::id())->first();
         User::where('id', Auth::id())->update([
             "name" => $request->first_name . ' ' . $request->middle_name . ' ' . $request->last_name,
             "first_name" => $request->first_name,
             "middle_name" => $request->middle_name,
             "last_name" => $request->last_name,
             "email" => $request->email,
         ]);

         if ($request->has('image')) {

             $image = $request->file('image');
             $imageName = $image->getClientOriginalName();
             $image->move(public_path('uploads/users/'), $imageName);

             Profile::where('user_id', Auth::id())->update([
                 'image' => 'uploads/users/' . $imageName,
             ]);
         }

         Profile::where('user_id', Auth::id())->update([
             "description" => $request->description,
             "phone" => $request->phone,
             "mobile" => $request->mobile,
             "assistant_number" => $request->assistant_number,
             "address" => $request->address,
         ]);
         if (request()->filled('old_password')) {  
             if(!Hash::check($request->old_password, $user->password)) {
                 toastr()->error('Password not matching');
                 return redirect()->back()->with('error', 'The specified password does not match the database password');
             }
             else {
                 User::where('id', Auth::id())->update([
                     'password' => Hash::make($request->new_password)
                 ]);
             }

         }
         toastr()->success('Profile Updated Successfully');
         return redirect()->back();
     }


    public function passwordUpdate(Request $request){
        $user = User::where('id', Auth::id())->first();
        if(!Hash::check($request->current_password, $user->password)) {
            toastr()->error('Password not matching');
            return redirect()->back()->with('error', 'The specified password does not match the database password');
        }
        else {
            request()->validate([
                'password' => 'required|min:3|confirmed',
                ]);
            $user->password = Hash::make($request->password);
            $user->save();
        }
        toastr()->success('Password Updated Successfully');
        return redirect('admin/account')->with('message', 'Your password updated successfully.');
    }


    public function setting(Request $request){
         $user =   User::where('id', 1)->first();
         $setting =   Setting::where('id', 1)->first();
         $data= [
             'user' => $user,
             'setting' => $setting,
             'breadcrumb' => 'DashBoard/Profile view',
             ];
         return view('admin/setting')->with($data);
    }



     public function verify(Request $request){
         if(Merchant::where('token',$request->get('v'))->count()){
             $merchant = Merchant::where('token', $request->get('v') )->first();
             $data= [
                 'merchant' => $merchant,
                 'breadcrumb' => 'Generate Password'
             ];
             return view('verify')->with($data);
         }
         else {
             return redirect('signin')->with('message', 'Link is expired or you have already created password, please login.');
         }
     }

     public function reset_password(Request $request){
         $merchant = Merchant::where('user_id', $request->post('user_id') )->first();
         $user = User::findOrFail($request->post('user_id'));
         $user->password = Hash::make($request->post('password'));
         $user->save();

     $merchant->token = '';
     $merchant->save();
     return redirect('signin')->with('message', 'You are setup password please login.');

}

public function view($id){

                try {
                    $user = User::findOrFail($id);
                }
                catch (\Throwable  $e) {
                  // dd($e);
                     History_log::where('user_id', $id)->update(['intstatus' =>1]);
                 return redirect('admin/dashboard')->with('message', 'User Not found!');

            }

            $user =   Merchant::where('user_id', $id )->first();
            $Payment_History = PaymentHistory::with('packages')->where('user_id', $id )->get();
            History_log::where('user_id', $id)->update(['intstatus' =>1]);
            $data= [
                    'user' => $user,
                    'breadcrumb' => 'DashBoard/Profile view',
                    'payment_history'=>$Payment_History,
                    ];
                   return view('admin.user_view')->with($data);




             //

}





public function confirm_reset_activation(Request $request) {

                $merchant = Merchant::where('user_id', Auth::user()->id)->first();
                // print_r($merchant);
                 // dd('asadsa');
                $name =   "";
                $title = "Paymeny Request";
                $url = env('APP_URL');
                $token = "";
                 $email="";
                  Mail::to($request->id)->send(new \App\Mail\MerchantActivation($name, $title, $url ,$token,$email));
                  //print_r(Mail);
                // Mail::to('user_id')->send(new SendMailable($user_id));
                // $merchant->profile_status = 2;  //update profile status
               // $merchant->save();
                return 1;
            }



public function update_profile(Request $request)
{
  $user = User::where('id', 1)->first();

  request()->validate([
    'Name' => 'required|max:20',
    ]);

    $user->update([
      'name' => $request->Name
    ]);

    $user->save();

    return redirect('admin/account')->with('message', 'Your account updated successfully.');

}

public function reset_admin_password(Request $request)
        {
     // dd('asdsa') ;
     // $merchant = Merchant::where('user_id',  Auth::user()->id)->first();

     // $user = User::findOrFail($request->post('user_id'));
     // $user->password = Hash::make($request->post('password'));
     // $user->save();
        $user = User::findOrFail(Auth::user()->id);
        $user->password = Hash::make($request->post('password'));
        $user->save();
        $data = ['breadcrumb' => 'Dashboard / Admin Change Password' ,];
        return redirect('admin/dashboard')->with('message', 'Your password is changed.');
     }

     //this function are for edit
        public function admin_setting_update($id){
        // dd('xcvbnm,');
           $setting =   Setting::where('id', 1)->first();
           $user =   User::where('id', 1)->first();
           // dd($setting);
            $data = ['setting' => $setting , 'user' =>$user,'breadcrumb'=> 'Setting Update'];
            // dd($data);
            return view('admin.setting_update')->with($data);
             // $setting->email = $request->Email_ID;
          // dd($setting->email);
          // $setting->Stripe_ID = $request->Stripe_ID;
          // $setting->Stripe_Password = $request->Stripe_Password;
          // $setting->Address = $request->Address;
          // $setting->Phone = $request->Phone;
          // $setting->Description = $request->description;
          // // $setting->save();
          // dd($setting);


        // return view('admin.setting');
    }
    public function admin_setting_portal_update(Request $request){
            // dd('asdasdasd');
            $setting =   Setting::where('id', 1)->first();
            // dd($setting);
            $setting->email = $request->Email_ID;

          $setting->Stripe_Email = $request->Stripe_ID;
           // dd($setting->Stripe_Email);
          $setting->Stripe_Password = $request->Stripe_Password;
          $setting->Address = $request->Address;
          $setting->Phone = $request->Phone;
          $setting->Description = $request->description;
          $setting->save();
          // dd($setting);
           return redirect('admin/setting')->with('message', 'setting Updated Successfully.');

    }
    public function delete(Request $request)
    {
            User::destroy($request->id);
            Merchant::where('user_id',$request->id)->delete();
            History_log::where('user_id',$request->id)->delete();
            PaymentHistory::where('user_id',$request->id)->delete();

            return redirect('admin/dashboard')->with('message', 'User is deleted successfully.');
    }

     public function expire(Request $request)
    {
            $merchant = Merchant::findOrFail($request->id);
            // dd($merchant);

            $expire_date=Date('y:m:d', strtotime('-1 days'));
            // dd($expire_date);
            $merchant->expire_date =$expire_date;
            // dd($merchant->expire_date);
            $merchant->save();
            // dd($merchant);
            // $expire_date=Date('y:m:d', strtotime('-15 days'));
            // dd($expire_date);`
            // Merchant::where('user_id',$request->id)->delete();



            //expire the user from TMS===================
            $this->curl_update_expiry_date($merchant->email,$expire_date);
            //==================================

            return redirect('admin/dashboard')->with('message', 'User is expired.');
    }



           Public function curl_update_expiry_date($email_id,$exp_date){
        //print "test"; die();
          $ch = curl_init();

          $url = env('APP_URL'). "/api/update_expire_date?email=".$email_id."&expire_date=".$exp_date;
          curl_setopt($ch,CURLOPT_URL,$url);
          //curl_setopt($ch,CURLOPT_POST, 0);                //0 for a get request
          //curl_setopt($ch,CURLOPT_POSTFIELDS,$postvars);
          curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch,CURLOPT_CONNECTTIMEOUT ,3);
          curl_setopt($ch,CURLOPT_TIMEOUT, 20);
          $response = curl_exec($ch);
        //  print "curl response is:" . $response;
          curl_close ($ch);

    }


/*********************************************************************************************************
                                    API's
*********************************************************************************************************/

    public function getAllUsers() {
        $users = User::with('roles')->with('profile')->get();
        return response()->json(['type' => 'success', 'users' => $users]);
    }

}
