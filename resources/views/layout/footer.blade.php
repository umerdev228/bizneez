<style>
        .site-footer
        {
                background-color:#26272b;
                padding:45px 0 20px;
                font-size:11px;
                line-height:24px;
                color:#737373;
        }
        .site-footer p
        {
                background-color:#26272b;
                padding:45px 0 20px;
                font-size:11px;
                line-height:15px;
                color:#737373;
        }
        .site-footer hr
        {
                border-top-color:#bbb;
                opacity:0.5
        }
        .site-footer hr.small
        {
                margin:20px 0
        }
        .site-footer h6
        {
                color:#fff;
                font-size:11px;
                text-transform:uppercase;
                margin-top:5px;
                letter-spacing:2px
        }
        .site-footer a
        {
                color:#737373;
        }
        .site-footer a:hover
        {
                color:#3366cc;
                text-decoration:none;
        }
        .footer-links
        {
                padding-left:0;
                list-style:none
        }
        .footer-links li
        {
                display:block
        }
        .footer-links a
        {
                color:#737373
        }
        .footer-links a:active,.footer-links a:focus,.footer-links a:hover
        {
                color:#3366cc;
                text-decoration:none;
        }
        .footer-links.inline li
        {
                display:inline-block
        }
        .site-footer .social-icons
        {
                text-align:right
        }
        .site-footer .social-icons a
        {
                width:40px;
                height:40px;
                line-height:40px;
                margin-left:6px;
                margin-right:0;
                border-radius:100%;
                background-color:#33353d
        }
        .copyright-text
        {
                margin:0
        }
        @media (max-width:991px)
        {
                .site-footer [class^=col-]
                {
                        margin-bottom:30px
                }
        }
        @media (max-width:767px)
        {
                .site-footer
                {
                        padding-bottom:0
                }
                .site-footer .copyright-text,.site-footer .social-icons
                {
                        text-align:center
                }
        }
        .social-icons
        {
                padding-left:0;
                margin-bottom:0;
                list-style:none
        }
        .social-icons li
        {
                display:inline-block;
                margin-bottom:4px
        }
        .social-icons li.title
        {
                margin-right:15px;
                text-transform:uppercase;
                color:#96a2b2;
                font-weight:700;
                font-size:11px
        }
        .social-icons a{
                background-color:#eceeef;
                color:#818a91;
                font-size:11px;
                display:inline-block;
                line-height:44px;
                width:44px;
                height:44px;
                text-align:center;
                margin-right:8px;
                border-radius:100%;
                -webkit-transition:all .2s linear;
                -o-transition:all .2s linear;
                transition:all .2s linear
        }
        .social-icons a:active,.social-icons a:focus,.social-icons a:hover
        {
                color:#fff;
                background-color:#29aafe
        }
        .social-icons.size-sm a
        {
                line-height:34px;
                height:34px;
                width:34px;
                font-size:11px
        }
        .social-icons a.facebook:hover
        {
                background-color:#3b5998
        }
        .social-icons a.twitter:hover
        {
                background-color:#00aced
        }
        .social-icons a.linkedin:hover
        {
                background-color:#007bb6
        }
        .social-icons a.dribbble:hover
        {
                background-color:#ea4c89
        }
        @media (max-width:767px)
        {
                .social-icons li.title
                {
                        display:block;
                        margin-right:0;
                        font-weight:600
                }
        }
</style>

<!-- Site footer -->
<footer class="site-footer">
        <div class="container">
                <div class="row">
                        <div class="col-sm-12 col-md-6">
                                <img width="200" src="{{$header_logo}}" alt="">
                                <p style="font-size: 11px;" class="text-justify">
                                        Bizneez is second to none. We provide office furniture and office design. We believe that with the right type of products and design layout can change any company over-night by inspiring your workforce to work better and more efficient, this is how you should see your self as a business. By investing in the right office design plan. Bizneez Group Ltd will then work around to suit your budget. We know the design world and we also know having the right type of office furniture makes a business succeed, so sourcing both the aspects of design and furniture will place you in the perfect solution when designing your business.
                                </p>
                        </div>
                        <div class="col-xs-6 col-md-2">
                            <!--     <h6>Categories</h6>
                                <ul class="footer-links">
                                        <li><a href="">C</a></li>
                                        <li><a href="">UI Design</a></li>
                                        <li><a href="">PHP</a></li>
                                        <li><a href="">Java</a></li>
                                        <li><a href="">Android</a></li>
                                        <li><a href="">Templates</a></li>
                                </ul> -->
                        </div>

                        <div class="col-xs-6 col-md-2">
                               <!--  <h6>Quick Links</h6>
                                <ul class="footer-links">
                                        <li><a href="">About Us</a></li>
                                        <li><a href="">Contact Us</a></li>
                                        <li><a href="">Contribute</a></li>
                                        <li><a href="">Privacy Policy</a></li>
                                        <li><a href="">Sitemap</a></li>
                                </ul> -->
                        </div>

                        <div class="col-xs-6 col-md-2">
                                <!-- <h6>Quick Links</h6>
                                <ul class="footer-links">
                                        <li><a href="">About Us</a></li>
                                        <li><a href="">Contact Us</a></li>
                                        <li><a href="">Contribute</a></li>
                                        <li><a href="policy/">Privacy Policy</a></li>
                                        <li><a href="">Sitemap</a></li>
                                </ul> -->
                        </div>
                </div>
                <hr>
        </div>
        <div class="container">
                <div class="row">
                        <div class="col-md-8 col-sm-6 col-xs-12">
                                <p class="copyright-text">Copyright &copy; 2021 All Rights Reserved by
                                        
                                </p>
                        </div>

                        <div class="col-md-4 col-sm-6 col-xs-12">
                                <ul class="social-icons">
                                        <li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
                                        <li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
                                        <li><a class="dribbble" href="#"><i class="fa fa-dribbble"></i></a></li>
                                        <li><a class="linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>
                                </ul>
                        </div>
                </div>
        </div>
</footer>

        <!-- All JS Custom Plugins Link Here here -->
        <script src="{{asset('assets/js/vendor/modernizr-3.5.0.min.js')}}"></script>
        <!-- Jquery, Popper, Bootstrap -->
        <script src="{{asset('assets/js/vendor/jquery-1.12.4.min.js')}}"></script>
        <script src="{{asset('assets/js/popper.min.js')}}"></script>
        <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
        <!-- Jquery Mobile Menu -->
        <script src="{{asset('assets/js/jquery.slicknav.min.js')}}"></script>

        <!-- Jquery Slick , Owl-Carousel Plugins -->
        <script src="{{asset('assets/js/owl.carousel.min.js')}}"></script>
        <script src="{{asset('assets/js/slick.min.js')}}"></script>
        <script src="{{asset('assets/js/price_rangs.js')}}"></script>

        <!-- One Page, Animated-HeadLin -->
        <script src="{{asset('assets/js/wow.min.js')}}"></script>
        <script src="{{asset('assets/js/animated.headline.js')}}"></script>
        <script src="{{asset('assets/js/jquery.magnific-popup.js')}}"></script>

        <!-- Scrollup, nice-select, sticky -->
        <script src="{{asset('assets/js/jquery.scrollUp.min.js')}}"></script>
        <script src="{{asset('assets/js/jquery.nice-select.min.js')}}"></script>
        <script src="{{asset('assets/js/jquery.sticky.js')}}"></script>

        <!-- contact js -->
        <script src="{{asset('assets/js/contact.js')}}"></script>
        <script src="{{asset('assets/js/jquery.form.js')}}"></script>
        <script src="{{asset('assets/js/jquery.validate.min.js')}}"></script>
        <script src="{{asset('assets/js/mail-script.js')}}"></script>
        <script src="{{asset('assets/js/jquery.ajaxchimp.min.js')}}"></script>

        <!-- Jquery Plugins, main Jquery -->
        <script src="{{asset('assets/js/plugins.js')}}"></script>
        <script src="{{asset('assets/js/main.js')}}"></script>
    <!-- Image Input -->

    <script src="{{ asset('theme/assets/js/input-image/scripts.bundle.js') }}"></script>
    <script src="{{ asset('theme/assets/js/input-image/image-input.js') }}"></script>


    <!-- Image Input -->






    <script>
 $(function() {
        $('#fname').on('keypress', function(e) {
            if (e.which == 32){

                return false;
            }
        });
        });
       $(function() {
        $('#lname').on('keypress', function(e) {
            if (e.which == 32){

                return false;
            }
        });
        });

         $(document).ready(function(){
          // alert('asdsa');
        $("#myElem").show();
        setTimeout(function()
          { $("#myElem").hide();
          }, 8000);
          });
         $(document).ready(function() {
  $(".set > a").on("click", function() {
    if ($(this).hasClass("active")) {
      $(this).removeClass("active");
      $(this)
        .siblings(".content")
        .slideUp(200);
      $(".set > a i")
        .removeClass("fa-minus")
        .addClass("fa-plus");
    } else {
      $(".set > a i")
        .removeClass("fa-minus")
        .addClass("fa-plus");
      $(this)
        .find("i")
        .removeClass("fa-plus")
        .addClass("fa-minus");
      $(".set > a").removeClass("active");
      $(this).addClass("active");
      $(".content").slideUp(200);
      $(this)
        .siblings(".content")
        .slideDown(200);
    }
  });
});

</script>


