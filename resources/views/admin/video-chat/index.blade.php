{{--@extends('includes.meeting_section.master')--}}
{{--@section('content')--}}
{{--<div id="app">--}}
{{----}}
{{--    <video-chat></video-chat>--}}
{{--    <chat-index></chat-index>--}}
    {{--    <video-chat--}}
{{--            :user="{{ $user }}"--}}
{{--            :others="{{ $others }}"--}}
{{--            pusher-key="{{ env('PUSHER_APP_KEY') }}"--}}
{{--            pusher-cluster="{{ env('PUSHER_APP_CLUSTER') }}"--}}
{{--    ></video-chat>--}}

{{--</div>--}}

{{--<script src="{{asset('js/app.js')}}"></script>--}}

{{--@endsection--}}


<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Bizneez') }}</title>
    <meta content="Coderthemes" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- App favicon -->
    <link rel="shortcut icon" href="{{asset('/theme/admin/assets/images/favicon.png')}}">
    <!-- Tooltipster css -->
    <link rel="stylesheet" href="{{ asset('/theme/admin/plugins/tooltipster/tooltipster.bundle.min.css') }}">
    <link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel = "stylesheet">

    <!-- Plugins css-->
    <link href="{{asset('/theme/admin/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{asset('/theme/admin/plugins/switchery/switchery.min.css')}}" />
    <link href="{{asset('/theme/admin/plugins/footable/css/footable.core.css')}}" rel="stylesheet">

    <!-- App css -->
    <link href="{{asset('/theme/admin/assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('/theme/admin/assets/css/icons.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('/theme/admin/assets/css/metismenu.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('/theme/admin/assets/css/style.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('/theme/admin/assets/css/metismenu.min.css')}}" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <script>var APP_URL = '{!! url('/') !!}';</script>


    @toastr_css

</head>
<body style="background-color: rgb(49 49 49)">

<div id="wrapper">
{{--@include('includes.meeting_section.sidebar')--}}
<!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
{{--    @include('includes.meeting_section.header')--}}
    <div class="content-page m-0">
        <div class="container-fluid">

            <div id="app">
                <video-chat
                        :logged="{{json_encode($logged)}}"
                        :auth-code="{{json_encode($code)}}"
                ></video-chat>
            </div>

        </div>
        {{--        @include('includes.meeting_section.footer')--}}
    </div>
</div>
<script src="{{asset('js/app.js')}}"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="{{asset('/theme/admin/assets/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('/theme/admin/assets/js/metisMenu.min.js')}}"></script>
<script src="{{asset('/theme/admin/assets/js/waves.js')}}"></script>
<script src="{{asset('/theme/admin/assets/js/jquery.slimscroll.js')}}"></script>
<script src="{{asset('/theme/admin/plugins/footable/js/footable.all.min.js')}}"></script>
<script src="{{asset('/theme/admin/plugins/switchery/switchery.min.js')}}"></script>
<script src="{{asset('/theme/admin/plugins/bootstrap-tagsinput/js/bootstrap-tagsinput.min.js')}}"></script>
<script src="{{asset('/theme/admin/plugins/select2/js/select2.min.js')}}" type="text/javascript"></script>
<script src="{{asset('/theme/admin/plugins/bootstrap-select/js/bootstrap-select.js')}}" type="text/javascript"></script>
<script src="{{asset('/theme/admin/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js')}}" type="text/javascript"></script>
<script src="{{asset('/theme/admin/plugins/bootstrap-maxlength/bootstrap-maxlength.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{asset('/theme/admin/plugins/autocomplete/jquery.autocomplete.min.js')}}"></script>
<script type="text/javascript" src="{{asset('/theme/admin/plugins/autocomplete/countries.js')}}"></script>
<script type="text/javascript" src="{{asset('/theme/admin/assets/pages/jquery.autocomplete.init.js')}}"></script>
<script type="text/javascript" src="{{asset('/theme/admin/assets/pages/jquery.form-advanced.init.js')}}"></script>
<script src="{{asset('/theme/admin/assets/js/jquery.core.js')}}"></script>
<script src="{{asset('/theme/admin/assets/js/jquery.app.js')}}"></script>
{{--    <script src="{{asset('/theme/admin/plugins/autocomplete/jquery.mockjax.js')}}"></script>--}}
<script src="{{asset('/theme/admin/assets/js/modernizr.min.js')}}"></script>
<script src="{{asset('/theme/admin/plugins/counterup/jquery.counterup.min.js')}}"></script>
<script src="{{asset('/theme/admin/plugins/waypoints/lib/jquery.waypoints.min.js')}}"></script>
<script src="{{asset('/theme/admin/plugins/jquery-knob/excanvas.js')}}"></script>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />

<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.js"></script>

@toastr_js
@toastr_render

<script src="https://cdn.ckeditor.com/4.15.1/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace( 'editor' );
</script>

<script>

    var events = $('#events');
    var normal_table = $('#normal-table').DataTable({
        "order": [[ 0, "desc" ]]
    });
    var table = $('#example').DataTable();
</script>
</body>
</html>
