<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductRange extends Model
{
    //
    protected $guarded = [];

    public function images() {
        return $this->hasMany(Media::class, 'item_id', 'id')->where('item_type', 'product-range');
    }
}
