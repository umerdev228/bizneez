<?php

namespace App\Imports;

use App\EmailTemplate;
use Maatwebsite\Excel\Concerns\ToModel;

class EmailListImport implements ToModel
{
    public $data;

    public function __construct($data)
    {
        $this->data = $data;
    }


    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        \Mail::to($row[0])->send(new \App\Mail\PromotionEmail($this->data));
    }
}
