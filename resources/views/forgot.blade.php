@extends('layout.app')
@section('content')
    <div class="support-company-area ">
        <div class="container">
            <div class="row align-items-center mt-5 mb-5 pt-5 pb-5">
                    
                <div class="col-xl-5 col-lg-6">
                    <div class="support-location-img">
                        <img src="theme/assets/img/login.png" alt="">
                            
                    </div>
                </div>
                <div class="col-xl-7 col-lg-6">
                    <div class="right-caption">
                        @if(session()->has('message'))
                            <div class="alert alert-success" id="myElem"> 
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                {{ session()->get('message') }}
                            </div>
                        @endif
                        @if(session()->has('error'))
                                <div class="alert alert-danger" id="myElem">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    {{ session()->get('error') }}
                                </div>
                            @endif
                            <form class="form-contact contact_form"  method="POST" action="{{ route('password.email') }}" >
                                @csrf
                                <div class="row">
                                    <div class="col-lg-7">
                                        <div class="section-tittle mb-20">
                                            <h2 class="mb-0">Forgot Password</h2>
                                        </div>
                                        <div class="form-group">
                                            <input value="{{ old('email') }}" class="form-control @error('email') is-invalid @enderror" name="email" id="email" type="email" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter your email'" placeholder="Enter your email" required="required" />
                                            @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>                 
                                </div>
                                <div class="form-group mt-1">
                                    <input type="submit" class="genric-btn info" value="Send Email"> 
                                </div>
                                <small>
                                    <small><a href="signup">Dont Have Account? Please Signup</a></small>
                                </small>
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{--
<main> 
        
        <div class="testimonial-area testimonial-padding pb-0 gray-bg">
			<div class="col-lg-12">
                        <div class="section-tittle text-center">
                            <h2>Try TMS to Login</h2>
                        </div>
                    </div>
            <div class="container-fluid">
                 
                <div class="row d-flex justify-content-center sect-bg">
                    <div class="col-lg-5">


                         @if(session()->has('message'))
                            <div class="alert alert-success">
                                {{ session()->get('message') }}
                            </div>
                            @endif
                            @if(session()->has('error'))
                            <div class="alert alert-danger">
                                {{ session()->get('error') }}
                            </div>
                         @endif
 

                    
                       <form method="POST" action="{{ route('login') }}">
                        @csrf   
                        <div class="row">
                                
								<div class="col-sm-12">
                                    <div class="form-group">
                                        <input value="{{ old('email') }}" class="form-control valid" name="email" id="email" type="email" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email'" placeholder="Email" required="required" />
                                            @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <input class="form-control" name="password" id="password" type="password" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Password'" placeholder="Password" required="required" />
                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
								 
                            </div>
                            <div class="form-group mt-3">
								<button class="btn rounded-0 primary-bg text-white w-100 btn_1" type="submit">SIGN UP FOR FREE</button>
								<div class="mt-3"><small>By clicking on "SIGN UP FOR FREE" you agree to our <a href="#">terms</a> and acknowledge reading our <a href="#">privacy notice.</a></small></div>
                            </div>
                       </form>
                    </div>     
                </div>
            </div>
        </div> 
    </main>
 --}}
@endsection
<style>

    .alert-success{
        width: 378px;
    }
    .alert-danger{
        width: 378px;
    }
</style>