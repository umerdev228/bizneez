<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MerchantSuportTeam extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
   public $name;
   public $url;
   public $title;
   public $token;
     

    public function __construct($name,$title,$url,$token)
    {
         
        $this->name = $name;
        $this->url = $url;
        $this->title = $title;
        $this->token = $token;
 
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
 
        return $this->from('info@bizneez.com')->view('mail.mail_merchant_suport', ['url' => $this->url, 'token' => $this->token ]);
         //   return view('pages_merchant_admin.customer_list', compact('customer_list','title'));
    }
}
