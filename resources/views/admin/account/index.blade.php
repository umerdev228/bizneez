@extends('admin.layouts.app')
@section('content')
    @include('admin.layouts.topbar')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    @if(session()->has('message'))
                        <div class="alert alert-success mt-5" id="myElem" >
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-12 mt-5">
                            <div class="card">
                                <div class="card-body">
                                    <a href="{{route('admin.accounts.create')}}"  class="btn btn-primary" ><i class="fa fa-plus"></i>Add New Account </a>
                                </div>
                                <div class="card-body">
                                    <table class="table table-hover m-0 tickets-list table-actions-bar dt-responsive nowrap" width="100%" id="normal-table">
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th> Type </th>
                                            <th>Email</th>
                                            <th>Amount</th>
                                            <th>Amount Type</th>
                                            <th>Date Time</th>
                                            <th>Source</th>
{{--                                            <th> Details </th>--}}
                                            <th data-orderable="false">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($accounts as $account)
                                            <tr>
                                                <td>{{$account->name}}</td>
                                                <td>{{$account->type}}</td>
                                                <td>{{$account->email}}</td>
                                                <td>{{$account->amount}}</td>
                                                <td>{{$account->amount}}</td>
                                                <td>{{$account->amount_type}}</td>
                                                <td>{{$account->date_time}}</td>
{{--                                                <td>{{$account->details}}</td>--}}
                                                <td>
                                                    <div class="btn-group">
                                                        <a class="btn" href="{{ route('admin.accounts.edit', ['id' => $account->id] ) }}">
                                                            <i class="mdi mdi-pencil"></i>
                                                        </a>
                                                        <a class="btn" href="{{ route('admin.accounts.delete', ['id' => $account->id] ) }}">
                                                            <i class="fa fa-close"></i>
                                                        </a>
{{--                                                        <a class="btn" href="{{ route('admin.leads.show', ['id' => $account->id, 'role' => Auth::user()->roles[0]->name] ) }}">--}}
{{--                                                            <i class="mdi mdi-eye"></i>--}}
{{--                                                        </a>--}}
                                                    </div>

                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div> <!-- end card -->
                            </div> <!-- end card -->
                        </div><!-- end col -->
                    </div>

                </div>
            </div>

        </div> <!-- container -->
    </div> <!-- content -->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

    @include('admin.layouts.footer')
@endsection
