<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ManufactureProducts extends Model
{
      protected $table = 'manufacture_products';
    protected $guarded = [];

     protected $fillable = [
        'manfacture_categories_id', 'paranet_product_id','product_name','size','color_code','retail_price1', 'retail_price2'  
    ];
}
