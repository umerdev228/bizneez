 
<!--  
<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

 
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'TMS') }}</title>

     
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
 

 
 -->



<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}"> 
        <title>{{ config('app.name', 'TMS') }}</title> 
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App favicon -->
        <link rel="shortcut icon" href="theme/admin/assets/images/favicon.png">
		
		<!-- Tooltipster css -->  
        <link rel="stylesheet" href="{{ URL::asset('theme/admin/plugins/tooltipster/tooltipster.bundle.min.css') }}">  
  
        <!-- Custom box css -->
        <link href="{{ asset('theme/admin/plugins/custombox/css/custombox.min.css') }}" rel="stylesheet">
		
        


 <!-- DataTables -->  
        <!-- reason datatable filter are not show <link href="{{ asset('theme/admin/plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css"/>  -->
        
      <!--   <link href="{{ asset('theme/admin/plugins/datatables/responsive.bootstrap4.min.csss') }}" rel="stylesheet" type="text/css"/> -->
        <!-- reason datatable filter are not show <link href="{{ URL::asset('theme/admin/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css"/> -->

        <link href='https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.4/summernote.css' rel='stylesheet'>
        

        <!-- App css -->
        <link href="{{ asset('theme/admin/plugins/bootstrap-select/css/bootstrap-select.min.css') }}" rel="stylesheet" />
        <link href="{{ asset('theme/admin/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" /> 

        <link href="{{ asset('theme/admin/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" /> 
        <link href="{{ asset('theme/admin/assets/css/icons.css') }}" rel="stylesheet" type="text/css" /> 
        <link href="{{ asset('theme/admin/assets/css/metismenu.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('theme/admin/assets/css/style.css') }}" rel="stylesheet" type="text/css" />
        
        <script src="{{ asset('theme/admin/assets/js/jquery.min.js') }}"></script>
		    <script src="{{ asset('theme/admin/assets/js/bootstrap.min.js') }}"></script> 
        <script src="http://local-tms-cybexo.com/theme/admin/plugins/summernote/summernote-bs4.min.js"></script>
        
        <script src="theme/admin/assets/js/modernizr.min.js"></script>
        <script src="{{ asset('theme/admin/assets/js/modernizr.min.js') }}"></script> 

      <!-- summmer html editer config files --> 
<!--       
<link href='http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.4/summernote.css' rel='stylesheet'>
<script src='http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.4/summernote.js'></script>

 -->
		
    </head>
    <body class="enlarged" data-keep-enlarged="true">

        <!-- Begin page -->
        <div id="wrapper">
         
        @include('admin.layouts.left-sidebar') 
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->

            
                 @yield('content')   
            


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->

 

   <!-- jQuery  -->
 

   <script src="{{ asset('theme/admin/assets/js/bootstrap.bundle.min.js') }}"></script>  
   <script src="{{ asset('theme/admin/assets/js/metisMenu.min.js') }}"></script>
   <script src="{{ asset('theme/admin/assets/js/waves.js') }}"></script>
   <script src="{{ asset('theme/admin/assets/js/jquery.slimscroll.js') }}"></script>


 
 <!-- Responsive examples -->
  <script src="{{ asset('theme/admin/plugins/datatables/dataTables.responsive.min.js') }}"></script>
  <script src="{{ asset('theme/admin/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>


 
 <!-- App js --> 
 <script src="{{ asset('theme/admin/assets/js/jquery.core.js') }}"></script>
 <script src="{{ asset('theme/admin/assets/js/jquery.app.js') }}"></script>
 
 <!-- Counter Up  -->
 <script src="{{ asset('theme/admin/plugins/waypoints/lib/jquery.waypoints.min.js') }}"></script>
 <script src="{{ asset('theme/admin/plugins/counterup/jquery.counterup.min.js') }}"></script>
 <script src="{{ asset('theme/admin/plugins/counterup/jquery.counterup.min.js') }}"></script>
      
        
 <!-- Select 2  -->
 <script src="{{ asset('theme/admin/plugins/select2/js/select2.min.js') }}"></script>
 <script src="{{ asset('theme/admin/plugins/bootstrap-select/js/bootstrap-select.js') }}" type="text/javascript"></script>
        
 <script src="{{ asset('theme/admin/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js') }}" type="text/javascript"></script>
 <script src="{{ asset('theme/admin/plugins/bootstrap-maxlength/bootstrap-maxlength.js') }}" type="text/javascript"></script>
         
 
 <!-- Init Js file -->
 <script src="{{ asset('theme/admin/assets/pages/jquery.form-advanced.init.js') }}"></script>
       

 <!-- Chart JS -->
 <script src="{{ asset('theme/admin/plugins/chart.js/chart.bundle.js') }}"></script>
 <script src="{{ asset('theme/admin/assets/pages/jquery.chartjs.init.js') }}"></script>
         


<script src="{{ asset('theme/admin/plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
   <script src="{{ asset('theme/admin/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
       

       

  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	 <!--  	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
		<script>$.noConflict();</script> -->
        


<!-- summmer html editer config files --> 
 <link href='http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.4/summernote.css' rel='stylesheet'>
<!--<script src='http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.4/summernote.js'></script>
 -->




 <!-- Datatable  -->  
 <link href="{{ URL::asset('theme/admin/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css"/>
  <link href="{{ asset('theme/admin/plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css"/> 
  <script src="{{ asset('theme/admin/plugins/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('theme/admin/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script> 


 
        <script>
   $(document).ready(function () {  
       $('#datatable').dataTable({
        "aaSorting": []
       });
     });
    
 
   
 function function_call_url(a){ 
   $("#datatable tbody").on('click', 'tr td:not(:first-child,:last-child)', function () {
   window.document.location = a; 
   });
   } 
            /* Fix Contacts Header */			
                var fixmeTop = $('.fixme').offset().top;
                $(window).scroll(function() {
                var currentScroll = $(window).scrollTop();
                if (currentScroll >= fixmeTop) {
                $('.fixme').css({
                    position: 'fixed',
                    top: '0',
                    width: '90.5%',
                    
                });
                } else {
                $('.fixme').css({
                    position: 'static',
                    width: '100%',
                });
                }
            });	

        </script>


</body>
</html>

<link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css"
         rel = "stylesheet">
      <script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

<script type="text/javascript">


/* For the Message display and hide */
 $(document).ready(function(){
  // alert('asdsa');
$("#myElem").show();
setTimeout(function() 
  { $("#myElem").hide(); 
  }, 5000);


});





 $(document).ready(function(){

          setInterval(function(){ 
                   var url_call = "{!! url('/') !!}/admin/ajaxNotification";  
                                     $.ajax({ 
                                         type : 'get',
                                         url  : url_call,   
                                         contentType: 'application/json; charset=utf-8',  
                                         success :  function(data){  
                                         //alert(data); 
                                         $("#notification-list").empty();
                                         $("#notification-list").append(data);
                                              
                          }
                          });

                     var url_call = "{!! url('/') !!}/notification/notification_counter_update";  
                                     $.ajax({ 
                                         type : 'get',
                                         url  : url_call,   
                                         contentType: 'application/json; charset=utf-8',  
                                         success :  function(data){  
                                         //alert(data); 
                                         $(".noti-icon-badge").empty();
                                         $(".noti-icon-badge").append(data);
                                              
                          }
                          });

 
             }, 5000);



});
  

/*function forward_agent(ticket_id){
  agent_id = $('.forward').val();
  update_agent(ticket_id,agent_id);
}*/

     //date picker at ticket listing in filters at created.
//==============================================
jQuery('#datepicker1').datepicker({
    format: "mm/dd/yyyy",
    clearBtn: true,
  //  multidate: true,
    multidateSeparator: ","
});
 
jQuery('#datepicker2').datepicker({
    format: "mm/dd/yyyy",
    clearBtn: true,
  //  multidate: true,
    multidateSeparator: ","
});
 
//=============================================

 

function notification_counter_update(){  //reset notification counter
   
     var url_call = "{!! url('/') !!}/notification/notification_counter_update";  
           $.ajax({ 
               type : 'get',
               url  : url_call,   
               contentType: 'application/json; charset=utf-8',  
               success :  function(data){   
                   //$("#todo"+data).hide();
                   // alert('efefef');
                   //$('.noti-icon-badge').text("0");

}
});
}


                                  
 
 function RightSidebar(x) {
   x.classList.toggle("fa-angle-double-right");
 }
 $('#btnSlide').on('click', function () {
     $('#ticketsection').toggleClass('ticket-listing-full');
 });


// Right sidebar
function ShowHideSidebar() {
var x = document.getElementById("hp");
       if (x.style.display === "none") {
         x.style.display = "block";
       } else {
         x.style.display = "none";
       }
     }



       // Show Hide Search Filter on Right sidebar
       function ShowHideSearch() {
       var x = document.getElementById("SearchFilter");
       if (x.style.display === "none") {
         x.style.display = "block";
       } else {
         x.style.display = "none";
       }
     }


  


 function function_call_url(a){ 
   $("#datatable tbody").on('click', 'tr td:not(:first-child,:last-child)', function () {
   window.document.location = a; 
   });
   } 
//dashboard blade data
//   $(document).ready(function () {
//        $('#datatable2').dataTable2({
//         "aaSorting": []
//        });
//        });
   
// $("#datatable2 #checkall").click(function () {
//        if ($("#datatable2 #checkall").is(':checked')) {
//            $("#datatable2 input[type=checkbox]").each(function () {
//                $(this).prop("checked", true);
//            });

//        } else {
//            $("#datatable2 input[type=checkbox]").each(function () {
//                $(this).prop("checked", false);
//            });
//        }
//  });
   
//  function function_call_url(a){ 
//    $("#datatable2 tbody").on('click', 'tr td:not(:first-child,:last-child)', function () {
//    window.document.location = a; 
//    });
//    } 




/* Checkbox Check All */
 jQuery(function () {
         $(".chkbtn").click(function () {
             if ($(this).is(":checked")) {
                 $(".btnall").show();
                 $(".sortbyfilter").hide();
                 
             } else {
                 $(".btnall").hide();
                 $(".sortbyfilter").show();

             }
         });
   });
      $(function() {
        $('#fname').on('keypress', function(e) {
            if (e.which == 32){
               
                return false;
            }
        });
});
         $(function() {
        $('#lname').on('keypress', function(e) {
            if (e.which == 32){
               
                return false;
            }
        });
});
   
/* Fix Contacts Header */			
 var fixmeTop = $('.fixme').offset().top;
 $(window).scroll(function() {
     var currentScroll = $(window).scrollTop();
     if (currentScroll >= fixmeTop) {
         $('.fixme').css({
             position: 'fixed',
             top: '0',
             width: '90.5%',

             
             
         });
     } else {
         $('.fixme').css({
             position: 'static',
             width: '100%',
         });
     }
 });		
   
  

// Group/Agent active
$('.agent a').click(function(){
   $('#agent').text($(this).text());
   $('.agent a').removeClass('active-status');
   $(this).addClass("active-status");
   });

// Sort by active
$('.sortby a').click(function(){
   $('#sortby').text($(this).text());
   $('.sortby a').removeClass('active-status');
   $(this).addClass("active-status");
   });


$(function() {
        $('#fname').on('keypress', function(e) {
            if (e.which == 32){
               
                return false;
            }
        });
});

$(function() {
        $('#lname').on('keypress', function(e) {
            if (e.which == 32){
               
                return false;
            }
        });
});

/*$(document).ready(function(){
  sendRequest();
  function sendRequest(){
      $.ajax({
        url: "awais.php",
        success: 
          function(data){
           $('#test').html(data); //insert text of test.php into your div
           
        },
        complete: function() {
       
       setInterval(sendRequest, 5000); // The interval set to 5 seconds
     }
    });
  };
});*/


</script>
<!-- <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
     -->
    


<!--Summernote js-->
    <script src="{{ asset('theme/admin/plugins/summernote/summernote-bs4.min.js') }}"></script>
 

<script>
    jQuery(document).ready(function(){
        $('.summernote').summernote({
            height: 150,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            focus: false                 // set focus to editable area after initializing summernote
        });

        $('.inline-editor').summernote({
            airMode: true
        });
    });
</script>


 <style>
  /*becoz summer text editer showing top tool */
  .note-popover.popover{display:none;}
  #frmAddNotes{display: none;}
  .previous-conversation{background-color: transparent!important;  border-left:dotted 1px;  height: 80px;}
  .border-secondary{display: none;}
  .assign_dropdown{
  width: 200px;
    float: left;
} 
.arrow-none{margin-top:20px;}
.custom_css{line-height: 25px!important;}
 </style>
 
