@extends('admin.layouts.app')
@section('content')
    @include('admin.layouts.topbar')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    @if(session()->has('message'))
                        <div class="alert alert-success mt-5" id="myElem" >
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-12 mt-5">
                            <div class="card">
                                <div class="card-box">
{{--                                    <form action="{{ route('export.excel.sheet') }}" method="post">--}}
{{--                                        <input name="products" value="{{ $list }}" type="hidden">--}}

{{--                                    </form>--}}
                                    <a href="{{route('order.export.view.custom')}}"  class="btn btn-primary" ><i class="fa fa-plus"></i>
                                        Export Excel Sheet
                                    </a>
                                    <table class="table table-hover m-0 tickets-list table-actions-bar dt-responsive nowrap" id="example">
                                        <thead>
                                        <tr>
                                            <th>Image</th>
                                            <th>Id</th>
                                            <th>Name</th>
                                            <th>Quantity</th>
                                            <th>Manufacturer </th>
                                            <th>Price</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($selected_products as $product)
                                            <tr>
                                                <td>
                                                    <img src="{{count($product->media) > 0 ? asset($product->media[0]->path) : ''}}" alt="">
                                                </td>
                                                <td>{{$product->id}}</td>
                                                <td>{{$product->name}}</td>
                                                <td>{{$product->quantity}}</td>
                                                <td>{{$product->manufacturer->name}}</td>=
                                                <td><span>&#163;</span>{{$product->price}}</td>
                                            </tr>
                                        @endforeach
                                        <tfoot>
                                        <tr>
                                            <th>Image</th>
                                            <th>Id</th>
                                            <th>Name</th>
                                            <th>Quantity</th>
                                            <th>Manufacturer </th>
                                            <th>Price</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div> <!-- end card -->
                            </div><!-- end col -->
                        </div><!-- end col -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('admin.layouts.footer')
@endsection
