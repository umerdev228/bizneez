@extends('admin.layouts.app')
@section('content')
    @include('admin.layouts.topbar')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    @if(session()->has('message'))
                        <div class="alert alert-success mt-5" id="myElem" >
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-12 mt-5">
                            <div class="card">
                                <div class="card-body">
{{--                                    <a href="{{route('admin.manufacturers.create')}}"  class="btn btn-primary" ><i class="fa fa-plus"></i>Add New Manufacturer </a>--}}
                                </div>
                                <div class="card-body">
                                    <table class="table table-hover m-0 tickets-list table-actions-bar dt-responsive nowrap" width="100%" id="normal-table">
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Company Name</th>
                                            <th>Telephone</th>
                                            <th>Email</th>
                                            <th>Date</th>
                                            <th>Appointment Time</th>
{{--                                            <th>Number Of Directors</th>--}}
                                            <th>Number Of Employees</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($appointments as $appointment)
                                            <tr>
                                                <td>{{$appointment->name}}</td>
                                                <td>{{$appointment->company_name}}</td>
                                                <td>{{$appointment->telephone}}</td>
                                                <td>{{$appointment->email}}</td>
                                                <td>{{$appointment->date}}</td>
                                                <td>{{$appointment->appointment_time}}</td>
{{--                                                <td>{{$appointment->number_of_directors}}</td>--}}
                                                <td>{{$appointment->number_of_employees}}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div> <!-- end card -->
                        </div><!-- end col -->
                    </div>

                </div>
            </div>

        </div> <!-- container -->
    </div> <!-- content -->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

    @include('admin.layouts.footer')
@endsection
