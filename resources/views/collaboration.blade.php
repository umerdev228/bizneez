@extends('layout.app')


@section('content')


<main>

        <!-- slider Area Start-->
        <div class="support-company-area p-5 fix gray-bg">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-xl-7 col-lg-7">
                        <div class="right-caption">
                            <!-- Section Tittle -->
                            <div class="section-tittle section-tittle2">
                                <h2>Now solve your Customer Issues together as a teamwork. </h2>
                            </div>
                            <div class="support-caption">
                                <p class="pera-top">Bizneez TMS helps you to work collaboratively to provide quick solutions to your customers in most efficient way.</p>
                            </div>
                            <a href="{{ url('signup')}}" class="genric-btn info text-uppercase">Sign up for free</a>
                        </div>
                        
                    </div>
                    <div class="col-xl-5 col-lg-5">
                        <div class="support-location-img">
                            <img src="theme/assets/img/collaboration.png" alt="">
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- slider Area End-->
        
        
        <div class="support-company-area fix pt-50 pb-50">
            <div class="container">
                
                <div class="section-tittle text-center">
                    <h2>Teamwork and efficiency with Cybexo TMS</h2>
                    <div class="support-caption">
                        <p class="font-weight-bold">Bizneez TMS bring customer communications from different channels to one place where all team members can access and communicate with customers. So, stop division of your team to cover multiple channels. Stay on Cybexo TMS, work as a single team, and save your time.</p>
                    </div>
                </div>
                
                <div align="center">
                    <img src="theme/assets/img/feat-2.png" class="img-fluid" alt=""  width="800" height="533" > 
                </div>
                
            </div>
            
        </div>
        

        <!-- slider Area Start-->
        <div class="support-company-area p-5 fix gray-bg">
            <div class="container">
                <div class="row align-items-center">
                    
                    <div class="col-xl-6 col-lg-6">
                        <div class="right-caption">
                            <!-- Section Tittle -->
                            <div class="section-tittle section-tittle2">
                                <h2>Prioritize Ticketing With Divide and Conquer</h2>
                            </div>
                            
                                <p>Simplified your task by splitting tickets in their respective section so it’s easy for the team members to answer them efficiently. For this, create separate lists of problems individually. Whenever a new ticket generates, assign it to its relevant section. </p>

                            
                        </div>
                        
                    </div>
                    
                    <div class="col-xl-6 col-lg-6">
                        <div class="support-location-img">
                            <img src="theme/assets/img/feat-3.png" alt="">
                            
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        <!-- slider Area End-->
        
        <!-- slider Area Start-->
        <div class="support-company-area p-5 fix">
            <div class="container">
                <div class="row align-items-center">
                    
                    <div class="col-xl-6 col-lg-6">
                        <div class="support-location-img">
                            <img src="theme/assets/img/feat-3.png" alt="">
                            
                        </div>
                    </div>
                    
                    <div class="col-xl-6 col-lg-6">
                        <div class="right-caption">
                            <!-- Section Tittle -->
                            <div class="section-tittle section-tittle2">
                                <h2>Solve issues together with linked agents</h2>
                            </div>
                            
                                <p>When a ticket generates multiple issues, an agent can link it to other agent to answer the customer in more informative way. Send consistent replies in more efficient way. </p>
                        </div>
                    </div>
                    
                    
                    
                </div>
            </div>
        </div>
        <!-- slider Area End-->
        
        
         <div class="testimonial-area testimonial-padding pb-0 gray-bg">
            <div class="container-fluid">
                <!-- Testimonial contents -->
                <div class="row d-flex justify-content-center sect-bg pt-40 pb-80">
                   <div class="section-tittle text-center p-4">
                        <h2 style="margin: 0;">Sign up for TMS today</h2>
                        <h5>Start your 15-day free trial. No credit card required. No strings attached.</h5>
                            <div class="mt-50"><a href="{{ url('signup')}}" class="btn btn-primary text-uppercase">start free trial</a></div>
                        </div>
                    
                </div>
            </div>
        </div>
        
    </main>


  
@endsection