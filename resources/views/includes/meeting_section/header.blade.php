@php
    $header_color = \App\SiteSetting::where('key', 'header_color')->first()->data;
    $text_color = \App\SiteSetting::where('key', 'text_color')->first()->data;
@endphp
<div class="topbar">

    <nav class="navbar-custom" style="background: {{$header_color}}">

        <ul class="list-unstyled topbar-right-menu float-right mb-0">

            {{--            <li class="dropdown notification-list" style="margin-right: 2px; margin-top: 3px;">--}}
            {{--                <a class="nav-link" href="{{ route('admin.cart.index') }}" role="button">--}}
            {{--                    <i class="fa fa-shopping-cart p-2 text-white" style="font-size: 20px"></i>--}}
            {{--                    <div class="badge badge-danger badge-pill" id="numberOfItems">0</div>--}}
            {{--                </a>--}}
            {{--            </li>--}}


            {{--            <li class="hide-phone app-search">--}}
            {{--                <form>--}}
            {{--                    <input type="text" placeholder="Search..." class="form-control">--}}
            {{--                    <button type="submit"><i class="fa fa-search"></i></button>--}}
            {{--                </form>--}}
            {{--            </li>--}}

            {{--            <li class="dropdown notification-list">--}}
            {{--                <a class="nav-link dropdown-toggle arrow-none" data-toggle="dropdown" href="#" role="button"--}}
            {{--                   aria-haspopup="false" aria-expanded="false">--}}
            {{--                    <i class="fi-bell noti-icon"></i>--}}
            {{--                    <span class="badge badge-danger badge-pill noti-icon-badge">4</span>--}}
            {{--                </a>--}}
            {{--                <div class="dropdown-menu dropdown-menu-right dropdown-menu-animated dropdown-lg">--}}
            {{--                    <div class="dropdown-item noti-title">--}}
            {{--                        <h5 class="m-0">--}}
            {{--                            <span class="float-right">--}}
            {{--                                <a href="" class="text-dark">--}}
            {{--                                    <small>Clear All</small>--}}
            {{--                                </a>--}}
            {{--                            </span>--}}
            {{--                            Notification--}}
            {{--                        </h5>--}}
            {{--                    </div>--}}

            {{--                    <div class="slimscroll" style="max-height: 230px;">--}}
            {{--                        <!-- item-->--}}
            {{--                        <a href="javascript:void(0);" class="dropdown-item notify-item">--}}
            {{--                            <div class="notify-icon bg-success"><i class="mdi mdi-comment-account-outline"></i></div>--}}
            {{--                            <p class="notify-details">Caleb Flakelar commented on Admin<small class="text-muted">1 min ago</small></p>--}}
            {{--                        </a>--}}

            {{--                        <!-- item-->--}}
            {{--                        <a href="javascript:void(0);" class="dropdown-item notify-item">--}}
            {{--                            <div class="notify-icon bg-info"><i class="mdi mdi-account-plus"></i></div>--}}
            {{--                            <p class="notify-details">New user registered.<small class="text-muted">5 hours ago</small></p>--}}
            {{--                        </a>--}}

            {{--                        <!-- item-->--}}
            {{--                        <a href="javascript:void(0);" class="dropdown-item notify-item">--}}
            {{--                            <div class="notify-icon bg-danger"><i class="mdi mdi-heart"></i></div>--}}
            {{--                            <p class="notify-details">Carlos Crouch liked <b>Admin</b><small class="text-muted">3 days ago</small></p>--}}
            {{--                        </a>--}}

            {{--                        <!-- item-->--}}
            {{--                        <a href="javascript:void(0);" class="dropdown-item notify-item">--}}
            {{--                            <div class="notify-icon bg-warning"><i class="mdi mdi-comment-account-outline"></i></div>--}}
            {{--                            <p class="notify-details">Caleb Flakelar commented on Admin<small class="text-muted">4 days ago</small></p>--}}
            {{--                        </a>--}}

            {{--                        <!-- item-->--}}
            {{--                        <a href="javascript:void(0);" class="dropdown-item notify-item">--}}
            {{--                            <div class="notify-icon bg-purple"><i class="mdi mdi-account-plus"></i></div>--}}
            {{--                            <p class="notify-details">New user registered.<small class="text-muted">7 days ago</small></p>--}}
            {{--                        </a>--}}

            {{--                        <!-- item-->--}}
            {{--                        <a href="javascript:void(0);" class="dropdown-item notify-item">--}}
            {{--                            <div class="notify-icon bg-custom"><i class="mdi mdi-heart"></i></div>--}}
            {{--                            <p class="notify-details">Carlos Crouch liked <b>Admin</b><small class="text-muted">13 days ago</small></p>--}}
            {{--                        </a>--}}
            {{--                    </div>--}}

            {{--                    <!-- All-->--}}
            {{--                    <a href="javascript:void(0);" class="dropdown-item text-center text-primary notify-item notify-all">--}}
            {{--                        View all <i class="fi-arrow-right"></i>--}}
            {{--                    </a>--}}

            {{--                </div>--}}
            {{--            </li>--}}

            {{--            <li class="dropdown notification-list">--}}
            {{--                <a class="nav-link dropdown-toggle arrow-none" data-toggle="dropdown" href="#" role="button"--}}
            {{--                   aria-haspopup="false" aria-expanded="false">--}}
            {{--                    <i class="fi-speech-bubble noti-icon"></i>--}}
            {{--                    <span class="badge badge-custom badge-pill noti-icon-badge">6</span>--}}
            {{--                </a>--}}
            {{--                <div class="dropdown-menu dropdown-menu-right dropdown-menu-animated dropdown-lg">--}}


            {{--                    <!-- item-->--}}
            {{--                    <div class="dropdown-item noti-title">--}}
            {{--                        <h5 class="m-0"><span class="float-right"><a href="" class="text-dark"><small>Clear All</small></a> </span>Chat</h5>--}}
            {{--                    </div>--}}

            {{--                    <div class="slimscroll" style="max-height: 230px;">--}}
            {{--                        <!-- item-->--}}
            {{--                        <a href="javascript:void(0);" class="dropdown-item notify-item">--}}
            {{--                            <div class="notify-icon"><img src="assets/images/users/avatar-2.jpg" class="img-fluid rounded-circle" alt="" /> </div>--}}
            {{--                            <p class="notify-details">Cristina Pride</p>--}}
            {{--                            <p class="text-muted font-13 mb-0 user-msg">Hi, How are you? What about our next meeting</p>--}}
            {{--                        </a>--}}

            {{--                        <!-- item-->--}}
            {{--                        <a href="javascript:void(0);" class="dropdown-item notify-item">--}}
            {{--                            <div class="notify-icon"><img src="assets/images/users/avatar-3.jpg" class="img-fluid rounded-circle" alt="" /> </div>--}}
            {{--                            <p class="notify-details">Sam Garret</p>--}}
            {{--                            <p class="text-muted font-13 mb-0 user-msg">Yeah everything is fine</p>--}}
            {{--                        </a>--}}

            {{--                        <!-- item-->--}}
            {{--                        <a href="javascript:void(0);" class="dropdown-item notify-item">--}}
            {{--                            <div class="notify-icon"><img src="assets/images/users/avatar-4.jpg" class="img-fluid rounded-circle" alt="" /> </div>--}}
            {{--                            <p class="notify-details">Karen Robinson</p>--}}
            {{--                            <p class="text-muted font-13 mb-0 user-msg">Wow that's great</p>--}}
            {{--                        </a>--}}

            {{--                        <!-- item-->--}}
            {{--                        <a href="javascript:void(0);" class="dropdown-item notify-item">--}}
            {{--                            <div class="notify-icon"><img src="assets/images/users/avatar-5.jpg" class="img-fluid rounded-circle" alt="" /> </div>--}}
            {{--                            <p class="notify-details">Sherry Marshall</p>--}}
            {{--                            <p class="text-muted font-13 mb-0 user-msg">Hi, How are you? What about our next meeting</p>--}}
            {{--                        </a>--}}

            {{--                        <!-- item-->--}}
            {{--                        <a href="javascript:void(0);" class="dropdown-item notify-item">--}}
            {{--                            <div class="notify-icon"><img src="assets/images/users/avatar-6.jpg" class="img-fluid rounded-circle" alt="" /> </div>--}}
            {{--                            <p class="notify-details">Shawn Millard</p>--}}
            {{--                            <p class="text-muted font-13 mb-0 user-msg">Yeah everything is fine</p>--}}
            {{--                        </a>--}}
            {{--                    </div>--}}

            {{--                    <!-- All-->--}}
            {{--                    <a href="javascript:void(0);" class="dropdown-item text-center text-primary notify-item notify-all">--}}
            {{--                        View all <i class="fi-arrow-right"></i>--}}
            {{--                    </a>--}}

            {{--                </div>--}}
            {{--            </li>--}}






      {{--  <li class="dropdown notification-list">--}}  
                 {{-- <a onClick=window.open("{{route('video.chat')}}","Ratting","width=800,height=400,left=150,top=200,toolbar=0,status=0,"); href="javascript:void(0)"
                   class="nav-link" title="Video Calling">--}} 
                   {{--   <i class="fa fa-video-camera" aria-hidden="true" title="Video Calling"></i>--}} 

                {{--  </a>--}} 
             {{-- </li> --}} 

 




  <li class="dropdown notification-list">
                <a onClick=window.open("{{route('video_chat_webrtc')}}","Ratting","toolbar=0,status=0,width=1300,height=700"); href="javascript:void(0)"
                   class="nav-link" title="Video Calling">
                    &nbsp;<i class="fa fa-video-camera"  aria-hidden="true" title="Client to Client Video Chat" style="color: #014b85"></i>
                                     
                </a>
            </li>



<!--   <li class="dropdown notification-list">
                <a onClick=window.open("{{route('video_chat_webrtc')}}","Ratting","toolbar=0,status=0,"); href="javascript:void(0)"
                   class="nav-link" title="Video Calling">
                    &nbsp;<i class="fa fa-video-camera"  aria-hidden="true" title="Admin Video Calling"></i>

                </a>
            </li>
 -->

                             


            <li class="dropdown notification-list text-white">

                <a class="nav-link dropdown-toggle nav-user text-white" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">

                    <span><?=date('g:i a')?></span> <img src="{{asset('theme/admin/assets/images/users/avatar-1.jpg')}}" alt="user" class="rounded-circle">

                    <span class="ml-1"> {{Auth::user()->name}} <em class="mdi mdi-chevron-down"></em> </span>


                </a>

                <div class="dropdown-menu dropdown-menu-right dropdown-menu-animated profile-dropdown text-white">

                    <div class="dropdown-item noti-title">
                        <h6 class="text-overflow m-0">Welcome !</h6>
                    </div>


                    <a href="{!! url('admin/account') !!}" class="dropdown-item notify-item">
                        <i class="fi-head"></i><span>Account</span>
                    </a>

                    <a href="{{ route('admin.site.setting.index') }}" class="dropdown-item notify-item">
                        <i class="fa fa-gear"></i> <span>Settings</span>
                    </a>

                    <a href="{{ route('admin.auth.codes.index') }}" class="dropdown-item notify-item">
                        <i class="fa fa-sign-in"></i> <span>Auth Codes</span>
                    </a>

                    <a href="{{ route('admin.demo.appointments') }}" class="dropdown-item notify-item">
                        <i class="fa fa-file-photo-o"></i> <span>Demo Request</span>
                    </a>

                    <a  href="{{ route('logout') }}" class="dropdown-item notify-item">

                        <i class="fi-power"></i> <span>Logout</span>

                    </a>

                </div>

            </li>
        </ul>

        <ul class="list-inline menu-left mb-0">
            <li class="float-left">
                <button class="button-menu-mobile open-left disable-btn">
                    <i class="dripicons-menu"></i>
                </button>
            </li>
            <li>
                <div class="page-title-box">
                    <h4 style="color: {{$text_color}} !important;" class="page-title">{{ ucfirst(Auth::user()->roles[0]->name) }} </h4>
                    <ol style="color: {{$text_color}} !important;" class="breadcrumb">
                        {{ $breadcrumb ?? '' }}
                    </ol>
                </div>
            </li>

        </ul>

    </nav>

</div>





<style>

    .topscroller{position: relative; overflow: hidden; width: auto; height: 414px;}
    /*.breadcrumb{float:left!important;position: absolute !important;margin-top: 11px !important;margin-left: 72px !important;}*/

    .bottomscroller{max-height: 230px; overflow: auto; width: auto; height: 414px;}
    .enlarged #wrapper .left.side-menu{width: 191px;}
    .navbar-custom{left: 140px;}
    /*.container-fluid{margin-left:88px;}*/
    .navbar-custom{background-color: #313541; color: #fff;}
    .enlarged #wrapper .footer{left:188px;}
    .fa-video-camera{font-size: 24px!important;
        /* margin-top: 19px; */
        padding-top: 28px;}


</style>


