@extends('admin.layouts.app')
@section('content')
    <!-- Google Chart JS -->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <!-- Start Page content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if(session()->has('message'))
                            <div class="alert alert-success" style="width:803px ">
                                {{ session()->get('message') }}
                            </div>
                        @endif

                        <div class="row">
                            <div class="col-9 mt-5">
                                <form method="POST" id="employee-form" action="{{ route('admin.employees.update', ['id' => $employee->id]) }}" enctype="multipart/form-data">
                                    @csrf
                                    <ul class="nav nav-tabs">
                                        <li class="nav-item">
                                            <a href="#create-employee" data-toggle="tab" aria-expanded="false" class="nav-link active show">
                                                <i class="fi-torso mr-2"></i> Employee
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="#salary" data-toggle="tab" aria-expanded="true" class="nav-link">
                                                <i class="fi-gallary mr-2"></i>Salary
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="#analytics-perform" data-toggle="tab" aria-expanded="false" class="nav-link">
                                                <i class="fi-bookmark mr-2"></i>Analytics and Performance
                                            </a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane active show" id="create-employee">
                                            <div class="card">
                                                <div class="card-box">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="first_name">First Name<span class="text-danger">*</span></label>
                                                                <input type="text" class="form-control valid" name="first_name" value="{{$employee->first_name}}" id="first_name"  />
                                                                @if ($errors->has('first_name'))
                                                                    <span class="error">{{ $errors->first('first_name') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="middle_name">Middle Name<span class="text-danger"></span></label>
                                                                <input type="text" class="form-control valid" name="middle_name" value="{{$employee->middle_name}}" id="middle_name"/>
                                                                @if ($errors->has('middle_name'))
                                                                    <span class="error">{{ $errors->first('middle_name') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="last_name">Last Name<span class="text-danger">*</span></label>
                                                                <input type="text" class="form-control valid" name="last_name" value="{{$employee->last_name}}" id="last_name"  />
                                                                @if ($errors->has('last_name'))
                                                                    <span class="error">{{ $errors->first('first_name') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="email">Email<span class="text-danger">*</span></label>
                                                                <input type="email" class="form-control valid" name="email" value="{{$employee->email}}" id="email"  />
                                                                @if ($errors->has('email'))
                                                                    <span class="error">{{ $errors->first('email') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="password">Password<span class="text-danger">*</span></label>
                                                                <input type="password" class="form-control valid" name="password" id="password"/>
                                                                @if ($errors->has('password'))
                                                                    <span class="error">{{ $errors->first('password') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="confirm_password">Confirm Password<span class="text-danger">*</span></label>
                                                                <input type="password" class="form-control valid" name="confirm_password" id="confirm_password"/>
                                                                @if ($errors->has('confirm_password'))
                                                                    <span class="error">{{ $errors->first('confirm_password') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="phone">Phone<span class="text-danger">*</span></label>
                                                                <input type="number" class="form-control valid" name="phone" value="{{$employee->profile->phone}}" id="phone"  />
                                                                @if ($errors->has('phone'))
                                                                    <span class="error">{{ $errors->first('phone') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="mobile">Mobile<span class="text-danger">*</span></label>
                                                                <input type="number" class="form-control valid" name="mobile" value="{{$employee->profile->mobile}}" id="mobile"  />
                                                                @if ($errors->has('mobile'))
                                                                    <span class="error">{{ $errors->first('mobile') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="assistant_number">Assistant Number<span class="text-danger">*</span></label>
                                                                <input type="number" class="form-control valid" name="assistant_number" value="{{$employee->profile->assistant_number}}" id="assistant_number"  />
                                                                @if ($errors->has('assistant_number'))
                                                                    <span class="error">{{ $errors->first('assistant_number') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="address">Address<span class="text-danger">*</span></label>
                                                                <input type="text" class="form-control valid" name="address" value="{{$employee->profile->address}}" id="address"  />
                                                                @if ($errors->has('address'))
                                                                    <span class="error">{{ $errors->first('address') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="location_id">Location<span class="text-danger">*</span></label>
                                                                <select class="form-control valid" name="location_id" id="location_id" >
                                                                    <option value="1">Location</option>
                                                                </select>
                                                                @if ($errors->has('location_id'))
                                                                    <span class="error">{{ $errors->first('location_id') }}</span>

                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="area_id">Area<span class="text-danger">*</span></label>
                                                                <select class="form-control valid" name="area_id" id="area_id" >
                                                                    <option value="1">Area</option>
                                                                </select>
                                                                @if ($errors->has('area_id'))
                                                                    <span class="error">{{ $errors->first('area_id') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="created_by">Created By<span class="text-danger">*</span></label>
                                                                <select class="form-control valid" name="created_by" id="created_by" >
                                                                    @foreach($users as $user)
                                                                        <option @if($user->id == $employee->created_by) selected @endif value="{{ $user->id }}">{{ $user->name }}</option>
                                                                    @endforeach
                                                                </select>
                                                                @if ($errors->has('created_by'))
                                                                    <span class="error">{{ $errors->first('created_by') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="created_by">Childs<span class="text-danger">*</span></label>
                                                                <input type="text" class="form-control valid" name="children[]" id="category"  />
                                                                @if ($errors->has('created_by'))
                                                                    <span class="error">{{ $errors->first('created_by') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="image">Company Logo<span class="text-danger">*</span></label>
                                                                <input type="file" class="form-control valid" name="image" value="{{$employee->profile->image}}" id="image"/>
                                                                @if ($errors->has('image'))
                                                                    <span class="error">{{ $errors->first('image') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="area_id">Team Building<span class="text-danger">*</span></label>
                                                                <select class="form-control valid" name="area_id" id="area_id" >
                                                                    <option value="1">1</option>
                                                                    <option value="2">2</option>
                                                                    <option value="3">3</option>
                                                                    <option value="4">4</option>
                                                                    <option value="5">5</option>
                                                                    <option value="6">6</option>
                                                                </select>

                                                                @if ($errors->has('job_description'))
                                                                    <span class="error">{{ $errors->first('job_description') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>


                                                        <div class="col-12">
                                                            <div class="form-group">
                                                                <label for="Description">Description<span class="text-danger">*</span></label>
                                                                <textarea id="Description" class="form-control rounded-0" name="description" rows="5"> {{ $employee->profile->description }}</textarea>
                                                                @if ($errors->has('description'))
                                                                    <span class="error">{{ $errors->first('description') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="salary">
                                            <div class="card">
                                                <div class="card-box">
                                                <!--                                            <form method="POST" action="{{ route('admin.employees.salary.store') }}" enctype="multipart/form-data">
                                                @csrf-->
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="invoice_number">Invoice Number<span class="text-danger">*</span></label>
                                                                <input type="text" class="form-control valid" name="invoice_number" value="{{$employee->salary->invoice_number}}" id="invoice_number"  />
                                                                @if ($errors->has('invoice_number'))
                                                                    <span class="error">{{ $errors->first('invoice_number') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="account_invoice">Account Invoices<span class="text-danger"></span></label>
                                                                <input type="text" class="form-control valid" name="account_invoice" value="{{$employee->salary->account_invoice}}" id="account_invoice"/>
                                                                @if ($errors->has('account_invoice'))
                                                                    <span class="error">{{ $errors->first('account_invoice') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="description">Description<span class="text-danger">*</span></label>
                                                                <input type="text" class="form-control valid" name="description" value="{{$employee->salary->description}}" id="description"  />
                                                                @if ($errors->has('description'))
                                                                    <span class="error">{{ $errors->first('description') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="price">Price<span class="text-danger">*</span></label>
                                                                <input type="text" class="form-control valid" name="price" value="{{$employee->salary->price}}" id="price"  />
                                                                @if ($errors->has('price'))
                                                                    <span class="error">{{ $errors->first('price') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="phone">Phone<span class="text-danger">*</span></label>
                                                                <input type="tel" class="form-control valid" name="phone" value="{{$employee->salary->phone}}" id="phone"  />
                                                                @if ($errors->has('phone'))
                                                                    <span class="error">{{ $errors->first('phone') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="discount">Discount<span class="text-danger">*</span></label>
                                                                <input type="text" class="form-control valid" name="discount" value="{{$employee->salary->discount}}" id="discount"  />
                                                                @if ($errors->has('discount'))
                                                                    <span class="error">{{ $errors->first('discount') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="profit">Profit<span class="text-danger">*</span></label>
                                                                <input type="text" class="form-control valid" name="profit" value="{{$employee->salary->profit}}" id="profit"  />
                                                                @if ($errors->has('profit'))
                                                                    <span class="error">{{ $errors->first('profit') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="vat">VAT%<span class="text-danger">*</span></label>
                                                                <input type="text" class="form-control valid" name="vat" value="{{$employee->salary->vat}}" id="vat"  />
                                                                @if ($errors->has('vat'))
                                                                    <span class="error">{{ $errors->first('vat') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="total">Total</label>
                                                                <input type="text" class="form-control valid" name="total" value="{{$employee->salary->total}}" value="" id="total"  />
                                                                @if ($errors->has('total'))
                                                                    <span class="error">{{ $errors->first('total') }}</span>

                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="commission">Commission</label>
                                                                <input type="text" class="form-control valid" name="commission" value="{{$employee->salary->commission}}" id="commission"/>
                                                                @if ($errors->has('commission'))
                                                                    <span class="error">{{ $errors->first('commission') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="quarter_bonuses">Quarter Bonuses </label>
                                                                <input type="text" class="form-control valid" name="quarter_bonuses" value="{{$employee->salary->quarter_bonuses}}" value="" id="quarter_bonuses"  />
                                                                @if ($errors->has('quarter_bonuses'))
                                                                    <span class="error">{{ $errors->first('quarter_bonuses') }}</span>

                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="your_bonus">Your Bonuses</label>
                                                                <input type="text" class="form-control valid" name="your_bonus" value="{{$employee->salary->your_bonus}}" value="" id="your_bonus"  />
                                                                @if ($errors->has('your_bonus'))
                                                                    <span class="error">{{ $errors->first('your_bonus') }}</span>

                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="total_salary">Total Salary</label>
                                                                <input type="text" class="form-control valid" name="total_salary" value="{{$employee->salary->total_salary}}" value="" id="total_salary"  />
                                                                @if ($errors->has('total_salary'))
                                                                    <span class="error">{{ $errors->first('total_salary') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <div class="form-group mt-3 float-right">
                                                                <button form="employee-form" class="btn btn-primary" type="submit" >Update</button>
                                                                <button onclick="history.back()" type="button" class="btn">Cancel</button>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="analytics-perform">
                                            <h1>analytics-perform</h1>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>

                </div>
            </div>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <style href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/dropzone.css"></style>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/dropzone.js"></script>



    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-beta/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
    <link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/examples/assets/app.css">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.0.4/popper.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.11.1/typeahead.bundle.min.js"></script>
    <script src="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>



    <script>

        var data = '{!! json_encode($users) !!}'
        console.log(data)
        //get data pass to json
        var task = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace("name"),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            local: jQuery.parseJSON(data) //your can use json type
        });

        task.initialize();

        var elt = $("#category");
        elt.tagsinput({
            itemValue: "id",
            itemText: "name",
            typeaheadjs: {
                name: "name",
                displayKey: "name",
                source: task.ttAdapter()
            }
        });
        var selected_categories = {!! json_encode($childs) !!}
        console.log(selected_categories)
        //insert data to input in load page
        for (var i = 0; i < selected_categories.length; i++) {
            console.log(selected_categories[i].id)
            elt.tagsinput("add", {
                id: selected_categories[i].id,
                name: selected_categories[i].name,
            });
        }

    </script>


@endsection
