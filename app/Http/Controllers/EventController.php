<?php

namespace App\Http\Controllers;

use App\Event;
use App\EventUser;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $events = Event::with('users')->with('comments')->get();
        $breadcrumb = 'CRM / Events';
        return view('admin.event.index', compact('events', 'breadcrumb'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
//        $start_ = $request['start_time_date'];

        $start = new DateTime($request['date'].' '.$request['time']);

        $start_date = $start->format('Y-m-d');
        $start_time = $start->format('H:i:s');

        $end_date = $start_date;
        $end_time = date('H:i:s', strtotime($start_time.' +2 hour'));

        $event = Event::create([
            "title" => $request['title'],
            "start_time" => date($start_time),
            "start_date" => date($start_date),
            "end_time" => date($end_time),
            "end_date" => date($end_date),
            "start" => $start,
            "end" => date("Y-m-d H:i:s", strtotime($start_time."+2 hours")),
            "description" => $request['description'],
            "color" => $request['color'],
            "created_by" => Auth::id(),
        ]);
        foreach ($request->users as $user) {
            EventUser::create([
                'user_id' => $user['id'],
                'event_id' => $event->id
            ]);
        }
        $events = Event::with('users')->with('comments')->get();
        $msg = 'Event Created Successfully';
        return response()->json([
            'type' => 'success',
            'events' => $events,
            'message' => $msg,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function show(Event $event)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function edit(Event $event)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Event $event)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function destroy(Event $event)
    {
        //
    }
}
