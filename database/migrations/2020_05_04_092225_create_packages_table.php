<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packages', function (Blueprint $table) {
            $table->id();
            $table->tinyInteger('inttype')->nullable();  
            $table->string('name', 225)->nullable();
            $table->decimal('amount',9,2);
            $table->text('description')->nullable(); 
            $table->string('package_duration', 225)->nullable();
            $table->string('nickname', 225)->nullable();
            $table->tinyInteger('interval_count')->nullable();  
            $table->string('stripe_plan_key', 225)->nullable();
            $table->Integer('days')->nullable();  
            $table->tinyInteger('status')->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packages');
    }
}
