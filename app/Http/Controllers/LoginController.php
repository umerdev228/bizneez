<?php

namespace App\Http\Controllers;

use App\AuthCode;
use App\Profile;
use App\User;
use DateTime;
use DateTimeZone;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('demo.login');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function login(Request $request) {
        $code_ = strtoupper($request->number_code_0.$request->number_code_1.$request->number_code_2.$request->number_code_3.$request->number_code_4.$request->number_code_5);
        $code = AuthCode::where('code', $code_)->first();
        $email = 'demo@bizneez.co.uk';
        $password = '123456789';


        if (!$code) {
           $user = Profile::where('demo_code', $code_)->first();
           if (!!$user) {
               $credentials = [
                   'email' => $email,
                   'password' => $password,
               ];
               Auth::attempt($credentials);
               return redirect()->route('video.chat', ['code' => $code_]);
           }
           else {
               toastr()->error('Code not found');
               return redirect()->back();
           }
        }


        $status = false;
        if ($code) {
            if ($code->status == 'active') {
                $status = true;
            }
            else {
                toastr()->error('Code Expired');
                return redirect()->back();
            }
        }
        else {
            toastr()->error('Code not found');
            return redirect()->back();
        }
        $sourceTimezone = new DateTimeZone('UTC');
        $dt = date('m/d/Y h:i A', time());
        $diff = abs(strtotime($dt) - strtotime($code->created_at));

        if((integer)date('H', $diff) > 2) {
            AuthCode::where('code', $code_)->update([
                'status' => 'expired'
            ]);
            toastr()->error('Code Expired');
            return redirect()->back();
        }
        if ($status) {
            $user = User::where('email', $email)->first();
            if (!$user) {
                $user = User::create([
                    'name' => 'demo',
                    'email' => $email,
                    'password' => Hash::make('123456789'),
                ]);
                $user->assignRole('demo');
                \App\Profile::create([
                    'user_id' => $user->id,
                ]);
            }
        }
        $credentials = [
            'email' => $email,
            'password' => $password,
        ];
        Auth::attempt($credentials);
        $count = $code->count;
        AuthCode::where('code', $code_)->update([
            'count' => $count + 1
        ]);
        if ($count > 10) {
            $code = AuthCode::where('code', $code_)->update([
                'status' => 'expired'
            ]);
        }
        return redirect()->route('video.chat', ['code' => $code_]);
    }
}
