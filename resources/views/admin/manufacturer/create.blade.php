@extends('admin.layouts.app')
@section('content')
    <!-- Google Chart JS -->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <!-- Start Page content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if(session()->has('message'))
                            <div class="alert alert-success" style="width:803px ">
                                {{ session()->get('message') }}
                            </div>
                        @endif
                        <div class="card-box">
                            <h4 class="header-title m-t-0 m-b-30"> </h4>
                    
                        </div>
                
                
                        <ul class="nav nav-tabs">
                            <li class="nav-item">
                                <a href="#home" data-toggle="tab" aria-expanded="false" class="nav-link active show">
                                    <i class="fi-torso mr-2"></i> Manufacturer
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#profile" data-toggle="tab" aria-expanded="true" class="nav-link">
                                    <i class="fi-gallary mr-2"></i>Images Gallery
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#messages" data-toggle="tab" aria-expanded="false" class="nav-link">
                                    <i class="fi-bookmark mr-2"></i><span  class="fs-4 fw-bold">+</span> Price List
                                </a>
                            </li>
                    
                            <li class="nav-item">
                                <a href="#product_categories" data-toggle="tab" aria-expanded="false" class="nav-link">
                                    <i class="fi-bookmark mr-2"></i><span  class="fs-4 fw-bold">+</span> New Price List
                                </a>
                            </li>
                    
                            <li class="nav-item">
                                <a href="#price_list_product" data-toggle="tab" aria-expanded="false" class="nav-link">
                                    <i class="fi-bookmark mr-2"></i>Price List Products
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#dealer_network" data-toggle="tab" aria-expanded="false" class="nav-link">
                                    <i class="fi-bookmark mr-2"></i>Dealer Network
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#inventory" data-toggle="tab" aria-expanded="false" class="nav-link">
                                    <i class="fi-bookmark mr-2"></i>Inventory
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active show" id="home">
                        
                                <div class="row">
                                    <form method="POST" action="{{ route('admin.manufacturers.store') }}" enctype="multipart/form-data">
                                        <div class="col-9 mt-5">
                                            <div class="card-box">
                                                @csrf
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label for="first_name">Manufacturer Name<span class="text-danger">*</span></label>
                                                            <input type="text" class="form-control valid" name="first_name" id="first_name" required />
                                                            @if ($errors->has('first_name'))
                                                                <span class="error">{{ $errors->first('first_name') }}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label for="area_id">Job Description<span class="text-danger">*</span></label>
                                                            <input placeholder="Job Description Exp: Director" type="text" class="form-control valid" name="job_description" value="" id="job_description"  />
                                                            @if ($errors->has('job_description'))
                                                                <span class="error">{{ $errors->first('job_description') }}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label for="email">Email<span class="text-danger">*</span></label>
                                                            <input type="email" class="form-control valid" name="email" id="email" required />
                                                            @if ($errors->has('email'))
                                                                <span class="error">{{ $errors->first('email') }}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                            
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label for="web_url">Web URL</label>
                                                            <input type="text" class="form-control valid" name="web_url" id="web_url" />
                                                            @if ($errors->has('web_url'))
                                                                <span class="error">{{ $errors->first('web_url') }}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label for="address">Address<span class="text-danger">*</span></label>
                                                            <input type="text" class="form-control valid" name="address" id="address" required />
                                                            @if ($errors->has('address'))
                                                                <span class="error">{{ $errors->first('address') }}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label for="phone">Phone<span class="text-danger">*</span></label>
                                                            <input type="tel" class="form-control valid" name="phone" id="phone" required />
                                                            @if ($errors->has('phone'))
                                                                <span class="error">{{ $errors->first('phone') }}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                            
                                            
                                            
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label for="area_id">Enter Password<span class="text-danger">*</span></label>
                                                            <input placeholder="Please Enter Password" type="password" class="form-control valid" name="password" value="" id="password"  />
                                                            @if ($errors->has('password'))
                                                                <span class="error">{{ $errors->first('password') }}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label for="area_id">Confirm Password<span class="text-danger">*</span></label>
                                                            <input placeholder="Please Enter Password" type="password" class="form-control valid" name="password" value="" id="password2"  />
                                                            @if ($errors->has('password'))
                                                                <span class="error">{{ $errors->first('password') }}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div id="representative" style="width:100%; margin-left:19px">
                                                        <div id="0" class="row">
                                                            <div class="col-sm-3">
                                                                <div class="form-group">
                                                                    <label for="location_id">Representative Name </label>
                                                                    <input placeholder="Please Enter Representative Name" type="text" class="form-control valid" name="represent_name[]" value="" id="represent_name"  />
                                                                    @if ($errors->has('location_id'))
                                                                        <span class="error">{{ $errors->first('represent_name') }}</span>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <div class="form-group">
                                                                    <label for="area_id">Representative Email </label>
                                                                    <input placeholder="Please Enter Representative email" type="text" class="form-control valid" name="represent_email[]" value="" id="represent_email"  />
                                                                    @if ($errors->has('represent_email'))
                                                                        <span class="error">{{ $errors->first('represent_email') }}</span>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <div class="form-group">
                                                                    <label for="area_id">Representative Phone </label>
                                                                    <input placeholder="Please Enter Representative Phone" type="text" class="form-control valid" name="represent_mobile[]" value="" id="represent_mobile"  />
                                                                    @if ($errors->has('represent_mobile'))
                                                                        <span class="error">{{ $errors->first('represent_mobile') }}</span>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <div class="form-group">
                                                                    <button type="button" onclick="appendFieldsR()" style="    margin-top: 30px;border-radius: 10px" class="btn btn-success valid align-baseline"><i class="fa fa-plus"></i> </button>
                                                                    {{--                                                                <button style="border-radius: 10px" class="btn btn-danger valid align-baseline"><i class="fa fa-minus"></i> </button>--}}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label for="image">Company Logo<span class="text-danger">*</span></label>
                                                            <input type="file" class="form-control valid" name="image" id="image" required />
                                                            @if ($errors->has('image'))
                                                                <span class="error">{{ $errors->first('image') }}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-12">
                                                        <div class="form-group">
                                                            <label for="Description">Manufacturer Description<span class="text-danger">*</span></label>
                                                            <textarea id="Description" class="form-control rounded-0" placeholder="Exp: We are manufacturer of office furniture" name="description" rows="5"></textarea>
                                                            @if ($errors->has('description'))
                                                                <span class="error">{{ $errors->first('description') }}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                            
                                            
                                                    <div class="col-12">
                                                        <div class="form-group">
                                                            <label for="Description">Company Keywords<span class="text-danger">*</span></label>
                                                            <textarea id="Description" class="form-control rounded-0" placeholder="" id="company_keywords" name="company_keywords" rows="5"></textarea>
                                                            @if ($errors->has('company_keywords'))
                                                                <span class="error">{{ $errors->first('company_keywords') }}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group mt-3">
                                                    <input class="btn btn-primary" type="submit" value="Create Manufacturer">
                                                    <button onclick="history.back()" type="button" class="btn">Cancel</button>
                                                </div>
                                            </div> <!-- end card-box -->
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="tab-pane" id="profile">
                            
                                <div class="row">
                                    <div class="col-12">
                                        <div class="card-box">
                                            <button type="button" class="btn btn-custom btn-rounded w-md waves-effect waves-light float-right" data-toggle="modal" style="width:300px" data-target=".bs-example-modal-lg"
                                            ><i class="mdi mdi-upload"></i> Upload Files</button>
                                            <h4 class="header-title m-b-30">Product Images Gallery</h4>
                                        
                                            <div class="row">
                                                <div class="col-md-9">
                                                    <div class="row">
                                                        @foreach($media as $image)
                                                            <div class="col-lg-3 col-xl-2">
                                                                <div class="file-man-box">
                                                                    <a href="{{ route('gallery.products.images.delete', ['id' => $image->id]) }}" class="file-close">
                                                                        <i class="mdi mdi-close-circle"></i>
                                                                    </a>
                                                                    <div class="file-img-box" >
                                                                        <img   src="{{ asset($image->path) }}" alt="icon">
                                                                    </div>
                                                                <!-- <a onclick="addToSessions({{$image->id}})" href="javascript:void(0)" class="file-download">
                                                            <i id="{{$image->id}}" class="mdi mdi-check "></i>
                                                        </a> -->
                                                        
                                                                    <div class="file-man-title">
                                                            
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                    </div></div>
                                                <div class="col-md-3" style="border-left: 1px solid #d2cece;">
                                                    <div align="center">
                                                
                                                        <select name="" id="" class="form-control">
                                                            <option>--Select Manufacture--</option>
                                                            <option value="Rock Systems">Rock Systems</option>
                                                            <option value="Rock Systems">MDD</option>
                                                            <option value="Rock Systems">Estres3</option>
                                                            <option value="Rock Systems">Sigma crescent desk</option>
                                                            <option value="Rock Systems">Star 2 desks</option>

                                                        </select> <br>
                                                        <input type="button" value="Get Images" class="btn btn-primary" />
                                                    </div>
                                                 
                                                </div>
                                            </div>
                                        
                                        </div><!-- end col -->
                                    </div>
                                </div>
                            </div>
                            
                            
                            <div class="tab-pane" id="messages">
                                
                                <div id="individual_products" class="ml-4">
                                    
                                    <form action="{{route('AdminManufactureCategoryAdd')}}" method="post">
                                        @csrf
                                        <div >
                                            <div  class="row" id="product">
                                                <div class="col-md-4 no-padding">
                                                    <input type="text" name="category_top[]" id="cateogry_top0" placeholder="" class="form-control float-left height-50 " list="cat" />
                                                    <!--   <datalist id="cat">
                                                    <option>Ergonomic Furniture</option>
                                                    <option>Office Desks</option>
                                                    <option>Reception Furniture</option>
                                                    <option>Executive Furniture</option>
                                                    <option>Accessories</option>
                                                    <option>Meeting Room</option>
                                                    <option>Task Seating</option>
                                                    
                                                    </datalist> -->
                                                    &nbsp;<button id="top-plus"    type="button" onclick="appendFields(0)" class="btn btn-primary btn-custom height-50 valid align-baseline rounded"><i class="fa fa-plus"></i> </button>
                                                    <!-- <button type="button" onclick="appendFields()" class="btn btn-secondary btn-custom red height-50 valid align-baseline rounded"><i class="fa fa-minus"></i> </button> -->
                                                    <button id="top-tick" type="button" onclick="show_category_name('category0','cateogry_top0')" class="btn btn-custom height-50 btn-success valid align-baseline rounded"><i class="fa fa-check"></i> </button>
                                                    
                                                </div>
                                            </div>
                                        </div>    <br>
                                        <br>
                                        <!--    <hr class="style1">
                                        
                                        <div class="row" id="pro_ranges">
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div id="element2" class="row">
                                                        <div class="p-2">
                                                            <input placeholder="" type="text" value="" id="range_0" name="range_0" class="form-control height-50 width-200 float-left">
                                                        </div>
                                                    </div>
                                                    <button type="button" onclick="add_element2('element2')"  class="btn mt-2 btn-custom brown valid align-baseline rounded"><i class="fa fa-arrow-right"></i></button>
                                                    <button type="button" onclick="remove_element2('pro_ranges')"  class="btn mt-2 btn-custom  red valid align-baseline rounded"> <i class="fa fa-minus"></i> </button>
                                                    <button type="button" onclick="func_add_range()" class="btn mt-2 btn-primary valid btn-custom align-baseline rounded"><i class="fa fa-plus"></i> </button>
                                                    <button type="button" class="btn btn-success valid btn-custom mt-2 align-baseline rounded"><i class="fa fa-check"></i> </button>
                                                    
                                                </div>
                                            </div>
                                        </div> -->
                                        <br>
                                        <hr class="style1">
                                        <br>
                                        <div class="cat_product" id="category">
                                            
                                            <div class="row" id="prd0">
                                                
                                                <div class="col remove-pad no-padding row">
                                                    <div id="element0" class="row pb-3" style="flex-wrap: nowrap;  overflow-x: auto;">
                                                        <div class="p-2">
                                                            <lable for="p_name0" style="top: -22px; font-weight: 500;" class="category0 lbl"></lable>
                                                            <input type="hidden" name="cat[]" id="p_name0">
                                                            <input class="form-control width-200 right height-50"  name="p_name[]" id="p_name0">
                                                        </div>
                                                    </div>
                                                    <!--  <button type="button" onclick="add_element('element0')"  class="btn mt-2  brown valid btn-custom align-baseline rounded"> <i class="fa fa-arrow-right"></i> </button>
                                                    
                                                    <button type="button" onclick="show_sub_product(0)"  class="btn mt-2 btn-primary valid btn-custom align-baseline rounded"><i class="fa fa-plus"></i> </button> -->
                                                    &nbsp;<button type="button" onclick="remove_element('category')"  class="btn mt-2  red valid btn-custom align-baseline rounded"> <i class="fa fa-minus"></i> </button>
                                                    <!--  <button type="button" class="btn valid mt-2 btn-success btn-custom align-baseline rounded"><i class="fa fa-check"></i> </button> -->
                                                    &nbsp;<button type="button"  class="btn mt-2 btn-custom valid align-baseline rounded purp3"><i class="fa fa-arrow-down"></i> </button>
                                                </div>
                                            </div>
                                            <div id="sub_row0"></div>
                                        </div>
                                        

                                </div>
                                <div class="mt-5 center"> <input type="submit" class="btn btn-primary " value="Save & Submit" name="btn" /> </div>
                                </form>
                            </div>
                            <div class="tab-pane" id="price_list_product">
                                
                                <section class="section-preview col-md-4">
                                    <form action="{{route('admin.price.range.import')}}" method="post" enctype="multipart/form-data">
                                        @csrf
                                        <div class="custom-file w-50">
                                            <input type="file" name="file" id="files" class="custom-file-input">
                                            <label class="custom-file-label success" for="customFile">Choose xlxs file</label>
                                        </div>

                                        {{--                                        <input type="file" name="file" id="files" class="hidden" required/>--}}
                                        <input class="btn btn-custom btn-rounded w-md" type="submit" value="Upload Excel Price List" />
                                        <!-- <label for="files" class="btn btn-custom btn-rounded w-md waves-effect waves-light float-right" data-toggle="modal" data-target=".bs-example-modal-lg">Upload file</label> -->
                                    </form>
                                </section>
                                <div class="card-body">
                                    <form action="" method="post">

                                        <table class="table dt-responsive" id="example">
                                            <thead>
                                                   
                                            <tr>
                                   
                                                <th data-orderable="false"><div class="row">Code &nbsp;<input type="text" name="COL2" value="{{$COL2}}" id="COL2" onchange="updateOrder('COL2')" class="sml-text" size="1" /></div></th>
                                                <th data-orderable="false"><div class="row">Product &nbsp;<input class="sml-text" type="text" name="COL7" value="{{$COL7}}" id="COL7" onchange="updateOrder('COL7')" size="1" /></div></th>
                                                <th data-orderable="false"><div class="row">Category &nbsp;<input class="sml-text" type="text" name="COL4" value="{{$COL4}}" id="COL4" onchange="updateOrder('COL4')" size="1" /></div></th>
                                                <th data-orderable="false"><div class="row">Range &nbsp;<input class="sml-text" type="text" name="COL5" value="{{$COL5}}" id="COL5" onchange="updateOrder('COL5')" size="1" /></div></th>
                                                <th data-orderable="false"><div class="row">Price &nbsp;<input class="sml-text" type="text" name="COL6" value="{{$COL6}}" id="COL6" onchange="updateOrder('COL6')" size="1" /></div></th>
                                                <th data-orderable="false"><div class="row">Length &nbsp; <input  class="sml-text" type="text" name="COL8" value="{{$COL8}}" id="COL8" onchange="updateOrder('COL8')" size="1" /></div></th>
                                                <th data-orderable="false"><div class="row">Width &nbsp;<input class="sml-text" type="text" name="COL10" value="{{$COL10}}" id="COL10" onchange="updateOrder('COL10')" size="1" /></div></th>
                                                <th data-orderable="false"><div class="row">Height &nbsp;<input class="sml-text" type="text" name="COL11" value="{{$COL11}}" id="COL11" onchange="updateOrder('COL11')" size="1" /></div></th>
                                                <th data-orderable="false"><div class="row">Depth &nbsp;<input class="sml-text" type="text" name="COL12" value="{{$COL12}}" id="COL12" onchange="updateOrder('COL12')" size="1" /></div></th>
                                            <!--  <th>Created </th>
                                                @if(Auth::user()->hasRole(['admin', 'manufacturer']))
                                                <th data-orderable="false">Action</th>
                                                @endif -->
                                            
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($products as $product)
                                                <tr>
                                                
                                                    <td>{{ $product->COL2 }}</td>
                                                
                                                    <td>{{ $product->COL7 }}</td>
                                                    <td>{{ $product->COL4 }}</td>
                                                    <td>{{ $product->COL5 }} ({{ $product->COL6 }})</td>
                                                    <td><span>&#163;</span>{{ str_replace("?","",$product->COL8)   }}</td>
                                                    <td><span></span>{{ $product->COL10 }}</td>
                                                    <td><span></span>{{ $product->COL11 }}</td>
                                                    <td><span></span>{{ $product->COL12 }}</td>
                                                    <td><span></span>{{ $product->COL13 }}</td>
                                                
                                                </tr>
                                            @endforeach
                                        </table>
                                        {{-- <input type="submit" value="Update/Save Sorting" />--}}
                                    </form>
                                    @if(Auth::user()->hasRole(['architect']))
                                        <div class="float-right">
                                            <form action="{{ route('architect.products.order.store') }}" method="POST">
                                                @csrf
                                                <input type="hidden" name="products[]" id="selected_products">
                                                <input type="hidden" name="quantity[]" id="selected_products_quantity">
                                                <input class="btn btn-success" type="submit" value="Order Selected Item" id="submit_button">
                                            </form>
                                        </div>
                                    @endif
                                </div> <!-- end card -->
                            </div>
                            <div class="tab-pane" id="dealer_network">
                            </div>
                            <div class="tab-pane" id="inventory">
                                <form action="{{route('AdminManufactureCategoryAdd')}}" method="post">
                                    @csrf




                                       
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                                
                                                <input type="text" class="form-control custom-txt float-left" name="category_top[]" id="cateogry_2top0" >
                                                <button id="top-tick" type="button" onclick="show_category_name2('category0','cateogry_top0')" class="btn btn-custom height-50 btn-success valid align-baseline rounded"><i class="fa fa-check"></i> </button>
                                                            
                                                           
                                                                
                                            </div>
                                        </div></div>
                                                     





                                    <div >
                                        <div  class="row" id="product">
                                                       
                                            <!--       <div class="row">
                                                                <div class="col-md-3">
                                                                    <div class="form-group">
                                                                        <input style="    width: 47%;" type="text" name="category_top[]" id="cateogry_2top0" placeholder="" class="form-control custom-txt float-left" list="cat" />
                                                                        <button id="top-plus"    type="button" onclick="appendFields(0)" class="btn btn-primary btn-custom height-50 valid align-baseline rounded"><i class="fa fa-plus"></i> </button>
                                                                        
                                                                        <button id="top-tick" type="button" onclick="show_category_name2('category0','cateogry_top0')" class="btn btn-custom height-50 btn-success valid align-baseline rounded"><i class="fa fa-check"></i> </button>
                                                                    </div></div></div> -->
                                                            
                                        </div>
                                    </div>    <br>
                                </form>
                                <!--        <div class="row" id="prd0">
                                                        
                                                        <div class="col remove-pad no-padding row">
                                                            <div id="element0" class="row pb-3">
                                                                <div class="p-2">
                                                                    <lable style="top: -22px; font-weight: 500;" class="category0 lbl"></lable>
                                                                    <input type="hidden" name="cat[]" id="p_2name0">
                                                                    <input class="form-control width-200 right height-50"  name="p_name[]" id="p_name0">
                                                                </div>
                                                            </div>
                                                            <button type="button" onclick="remove_element('category')"  class="btn mt-2  red valid btn-custom align-baseline rounded"> <i class="fa fa-minus"></i> </button>
                                                            <button type="button"  class="btn mt-2 btn-custom valid align-baseline rounded purp3"><i class="fa fa-arrow-down"></i> </button>
                                                        </div>
                                                    </div>
                                                    <div id="sub_row0"></div> -->
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="object-lable" >Object Lable</label>
                                            <input type="email" class="form-control custom-txt" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                                            <button type="button" onclick="remove_element('category')" class="btn mt-2 btn_align_red  red valid btn-custom align-baseline rounded"> <i class="fa fa-minus"></i> </button>
                                                                
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="object-lable text-center">Object Lable</label>
                                            <input type="email" class="form-control custom-txt" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="">
                                            <button type="button" onclick="remove_element('category')" class="btn mt-2 btn_align_red  red valid btn-custom align-baseline rounded"> <i class="fa fa-minus"></i> </button>
                                                                
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="object-lable">Object Lable</label>
                                            <input type="email" class="form-control custom-txt" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="">
                                            <button type="button" onclick="remove_element('category')" class="btn mt-2 btn_align_red  red valid btn-custom align-baseline rounded"> <i class="fa fa-minus"></i> </button>
                                                                
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="object-lable">Object Lable</label>
                                            <input type="email" class="form-control custom-txt" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="">
                                            <button type="button" onclick="remove_element('category')" class="btn mt-2 btn_align_red  red valid btn-custom align-baseline rounded"> <i class="fa fa-minus"></i> </button>
                                                                
                                        </div>
                                    </div>
                                                        
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="object-lable">Object Lable</label>
                                            <input type="email" class="form-control custom-txt" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="">
                                            <button type="button" onclick="remove_element('category')" class="btn mt-2 btn_align_red  red valid btn-custom align-baseline rounded"> <i class="fa fa-minus"></i> </button>
                                            <button type="button" class="btn mt-2 btn-custom valid align-baseline rounded btn_align_perpal purp3"><i class="fa fa-arrow-down"></i> </button>
                                        </div>
                                    </div>
                                </div>
                                                    
                            </div>
                                                
                        </div>
                </div>
            </div>
        </div>
        <style>
            .side-menu .topbar-left{top:24px;}
            .notification-list .noti-icon{margin-top:20px;}
            .btn_align_red{position: absolute;
                left: 79%;
                top: 22px;}
            .btn_align_perpal{position: absolute;
                left: 92%;
                top: 22px;}
            .object-lable{    text-align: center;
                width: 270px;}
            .custom-txt{width: 79%;}
            .lbl{
                position: absolute;
                top: -20px;
                left: 11px;
            }
            .lbl2{    top: -22px;
                position: absolute;
                /* left: 4px; */
                margin-left: -203px;
            }
            .category{font-weight: bold; font-size:20px}
            .no-padding{
                padding-left: 2px;
                padding-right: 0px;
            }
                                
            .waves-light{width: 75%;}
            .btn-success{font-size: 16px;}
            .remove-pad{padding-right: 0px; padding-left: 0px;}
            .purpal{background-color: #a311d8}
            .red{background-color: red; color: #fff}
            .right{float: left}
            .blue{background-color: #2d7bf4; color: #fff}
            .purple{background: purple; color: #fff}
            .purp3{background: purple; color: #fff}
            .brown{background-color:#07922e; color: #fff}
                                
            .file[type="file"] {
                display: none;
            }
            .height-50{height: 34px!important;}
            .width-200{width:200px; margin-right: 7px;}
            #cateogry_top0{  margin-left: -11px; width:200px; }
            #p_name0{  margin-left: 12px; }
            .mt-c{    margin-top: 4px;}
            .btn-custom   {
                height: 34px;     margin: 0px 2px;
                padding-top: 3px;
            }
            .btn-custom-mr{margin: 0px 17px; height: 34px;}
            .btn-custom.focus, .btn-custom:focus, .btn-outline-custom.focus, .btn-outline-custom:focus, .btn-custom:not([disabled]):not(.disabled).active, .btn-custom:not([disabled]):not(.disabled):active, .show > .btn-custom.dropdown-toggle{box-shadow: none!important;}
            .btn-height-34{height: 34px;}
            .sml-text{width: 20px;}
        </style>
                                
        <script>
            var selectIncrement = 1;
            var i = 1;
            var j = 0;
            var c =0;
            function add_element2(element_id){
                var element = '#'+element_id;
                let ele_input = 'input_'+selectIncrement;
                console.log(ele_input)
                var element_data = `<div class="p-2" id="`+ele_input+`"><input placeholder="" type="text" value="" name="range_0" class="form-control width-200 float-left height-50"><a style='margin-right: 8px;height: 34px;' href="javascript:void(0)" class="btn btn-danger " onclick="removeElement(`+ele_input+`)"><i class="fa fa-close  mt-c"></i></a></div>`;
                $(element).append(element_data);
                selectIncrement++
            }
            function add_element(element_id){
                //console.log(element_id)
                category_name = $('#cateogry_top0').val();
                var element = '#'+element_id;
                let ele_select = 'select_'+selectIncrement;
                var element_data = `<div  class="p-2" id="`+ele_select+`"> <label for="p_name`+element_id+`" class="lbl2" >`+category_name+`</label><input type="hidden" name="cat[]" value="`+category_name+`"><input class="form-control height-50 width-200 right" name="p_name[]" id="p_name`+element_id+`"><a style='margin-right: 8px;height: 34px;margin-left: -40px;' href="javascript:void(0)" onclick="removeElement(`+ele_select+`)" class="btn btn-danger  "><i class="fa fa-close  mt-c"></i></a></div>`;
                $(element).append(element_data);
                selectIncrement++
            }
            function remove_element(element_id){
                $('#'+element_id).remove();
            }
            function remove_element2(element_id){
                $('#'+element_id).remove();
            }
            function removeElement(element_id){
                console.log(element_id)
                $(element_id).remove();
            }
                                
            function show_category_name(class_name,category_id){
                var  cls = '.'+class_name;
                var cat_id = '#'+category_id;
                $(cls).text( $(cat_id).val() );
                $('#p_name0').val( $(cat_id).val() );
                $("#top-tick").hide();
                // $("#top-plus").hide();
                $("#cateogry_top0").val('');
                $('#top-plus').prop('disabled', true);
            }
            function show_category_name2(class_name,category_id){
                var  cls = '.'+class_name;
                var cat_id = '#'+category_id;
                $(cls).text( $(cat_id).val() );
                $('#p_2name0').val( $(cat_id).val() );
                $("#top-tick").hide();
                // $("#top-plus").hide();
                $("#cateogry_2top0").val('');
                $('#top-plus').prop('disabled', true);
            }
            function show_sub_product(id){
                c=c+1;
                var render_div = '#prd'+id;
                var html_product_data =
                    ` <h5 class="category0"></h5>
                                <div class="row" id="id="prd_`+c+`">

                                    <div class="col remove-pad no-padding row">
                                        <div id="element0" class="row">
                                            <div class="p-2" id="element_`+c+`">
                                                <input class="form-control width-200 right height-50" name="p_name_0" id="p_name0">
                                            </div>
                                        </div>
                                        <button type="button" onclick="add_element('element_`+c+`')"  class="btn btn-custom mt-2 brown valid align-baseline rounded"> <i class="fa fa-arrow-right"></i> </button>

                                        <button type="button" onclick="func_common_remove(`+'prd_'+c+`)"   class="btn btn-custom mt-2 red valid align-baseline rounded"> <i class="fa fa-minus"></i> </button>
                                        <button type="button" onclick="show_sub_product(0)"  class="btn btn-primary btn-custom mt-2 valid align-baseline rounded"><i class="fa fa-plus"></i> </button>
                                        <button type="button" class="btn  valid btn-success btn-custom mt-2 align-baseline rounded"><i class="fa fa-check"></i> </button>
                                        <button type="button" onclick="clonetreetimes('category')" class="btn mt-2 btn-success btn-custom valid align-baseline purple rounded"><i class="fa fa-arrow-down"></i> </button>
                                    </div>
                                </div> `
                                
                $('#category').append(html_product_data);
            }
            /*  function func_add_row_range(){
                                c=c+1;
                                var html_input_category_ranges =
                                `<div class="col-md-4" id="ranges`+c+`">
                                    <div class="range">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <input placeholder="" type="text" value="" name="range_0" class="form-control height-50 w-25 float-left">
                                                <button type="button" onclick="func_add_range()" class="btn btn-custom btn-primary valid align-baseline rounded"><i class="fa fa-plus"></i> </button>
                                                <button type="button" onclick="func_common_remove('#ranges`+c+`')"  class="btn red btn-custom btn-secondary btn-danger valid align-baseline rounded"><i class="fa fa-minus"></i> </button>
                                                <button type="button" class="btn btn-custom btn-success valid align-baseline rounded"><i class="fa fa-check"></i> </button>
                                                <button type="button" onclick="func_add_row_range()" class="btn purple btn-custom  valid align-baseline rounded"><i class="fa fa-arrow-down"></i> </button>
                                            </div>
                                            
                                        </div></div>
                                    </div>
                                    <div class="col-md-4" id="ranges`+c+`">
                                        <div class="range">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <input placeholder="" type="text" value="" name="range_0" class="form-control height-50 w-25 float-left">
                                                    <button type="button" onclick="func_add_range()" class="btn btn-custom btn-primary valid align-baseline rounded"><i class="fa fa-plus"></i> </button>
                                                    <button type="button" onclick="func_common_remove('#ranges`+c+`')"  class="btn btn-custom btn-secondary red btn-danger valid align-baseline rounded"><i class="fa fa-minus"></i> </button>
                                                    <button type="button" class="btn btn-custom valid align-baseline rounded"><i class="fa fa-check"></i> </button>
                                                    <button type="button" onclick="func_add_row_range()" class="btn purple btn-custom  valid align-baseline rounded"><i class="fa fa-arrow-down purple"></i> </button>
                                                </div>
                                                
                                            </div></div>
                                        </div>
                                        <div class="col-md-4" id="ranges`+c+`">
                                            <div class="range">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input placeholder="" type="text" value="" name="range_0" class="form-control height-50 w-25 float-left">
                                                        <button type="button" onclick="func_add_range()" class="btn btn-primary valid btn-custom align-baseline rounded"><i class="fa fa-plus"></i> </button>
                                                        <button type="button" onclick="func_common_remove('#ranges`+c+`')"  class="btn red btn-custom btn-secondary btn-danger valid align-baseline rounded"><i class="fa fa-minus"></i> </button>
                                                        <button type="button" class="btn  valid align-baseline btn-custom rounded"><i class="fa fa-check"></i> </button>
                                                        <button type="button" onclick="func_add_row_range()" class="btn purple btn-custom  valid align-baseline rounded"><i class="fa fa-arrow-down purple"></i> </button>
                                                    </div>
                                                    
                                                </div></div>
                                            </div></div>
                                            `;
                                            $('#pro_ranges').append(html_input_category_ranges);
                                            }*/
            function func_add_row_range(){
                c=c+1;
                var html_input_category_ranges =
                    ` `;
                $('#pro_ranges').append(html_input_category_ranges);
            }
            function func_add_range(){
                c=c+1;
                var html_input_category_ranges =
                    `<div class="col-md-12" id="row_ranges_`+c+`" >
                        <div class="row">
                            <div id="ranges`+c+`" class="row">
                                <div class="p-2">
                                    <input placeholder="" type="text" value="" id="range_0" name="range_0_`+c+`" class="form-control width-200 height-50 float-left">
                                </div>
                            </div>
                            <button type="button" onclick="add_element2('ranges`+c+`')"  class="btn mt-2 btn-custom btn-success brown valid align-baseline rounded"><i class="fa fa-arrow-right"></i></button>
                            <button type="button" onclick="func_common_remove('#row_ranges_`+c+`')"  class="btn btn-custom btn-danger red mt-2 btn-secondary valid align-baseline rounded"><i class="fa fa-minus"></i> </button>
                            <button type="button" onclick="func_add_range()" class="btn btn-primary valid btn-custom mt-2 align-baseline rounded"><i class="fa fa-plus"></i> </button>
                            <button type="button" class="btn btn-success btn-custom valid align-baseline mt-2 rounded"><i class="fa fa-check"></i> </button>
                            <button type="button" onclick="func_add_row_range()" class="btn btn-custom purple mt-2  valid align-baseline rounded"><i class="fa fa-arrow-down"></i> </button>
                        </div>
                    </div>`;
                $('#pro_ranges').append(html_input_category_ranges);
            }
            function func_common_remove(id){
                $(id).remove();
            }
            function show_subrow() {
                j = j+1;
                $('#product_sub_counter_0').val(j);
                //console.log('clicked')
                var html_input =
                    `<div class="row" id="sub_row`+j+`">
                        <div class="col-md-1">

                        </div>
                        <div class="col-md-2 remove-pad">
                            <input type="text" value="`+$('#p_name').val()+`" name="p_name_0_`+j+`" placeholder="exp Product Name" class="form-control w-75" />
                        </div>
                        <div class="col-md-2 remove-pad">
                            <input type="text" value="`+$('#p_size').val()+`" name="p_size_0_`+j+`"  placeholder="exp Size Options" class="form-control w-75" />
                        </div>
                        <div class="col-md-1 remove-pad">
                            <input type="text" value="`+$('#p_code').val()+`" name="p_code_0_`+j+`" placeholder="exp Code" class="form-control w-75" />
                        </div>
                        <div class="col-md-1 remove-pad">
                            <input type="text" value="`+$('#p_retial_price').val()+`" name="p_retail_price1_0_`+j+`"  placeholder="exp Retail Price" class="form-control">
                        </div>
                        <div class="col-md-1 remove-pad">
                            <input type="text" value="`+$('#p_retial_price2').val()+`" name="p_retial_price2_0_`+j+`" placeholder="exp Retail Price" class="form-control">
                        </div>
                        <div class="col-md-3 remove-pad">
                            <button type="button" onclick="show_subrow_remove1(`+j+`)"  class="btn btn-secondary btn-danger valid align-baseline rounded "><i class="fa fa-minus"></i> </button>
                        </div>

                        </div><div id="sub_row`+j+`">
                        </div>`
                $('#sub_row0').append(html_input);
            }
            function show_subrow_remove1(id) {
                $('#sub_row'+id).remove();
            }
            function show_subrow_inner(e,f=0) { //alert(e);
                var f=f+1;
                var html_input =
                    `<div class="row" id="sub2_row`+e+`">
                                                    <div class="col-md-1">
                                                        
                                                    </div>
                                                    <div class="col-md-2 remove-pad">
                                                        <input type="text" value="`+$('#p_name').val()+`"  name="p_name_`+e+`_`+f+`" placeholder="" class="form-control w-75" />
                                                    </div>
                                                    
                                                    <div class="col-md-3 remove-pad">
                                                        <button type="button"   class="btn btn-custom btn-primary valid align-baseline rounded"><i class="fa fa-plus"></i> </button>
                                                        <button type="button" onclick="show_subrow_remove(`+e+`)"  class="btn btn-custom btn-danger btn-secondary valid align-baseline rounded "><i class="fa fa-minus"></i> </button>
                                                        <button type="button" class="btn btn-success btn-custom valid align-baseline rounded"><i class="fa fa-check"></i> </button>
                                                        <button type="button" class="btn  purple valid btn-custom align-baseline rounded"><i class="fa fa-arrow-down"></i> </button>
                                                        
                                                    </div>
                                                    
                                                </div>`
                $("#sub1_row"+e).append(html_input);
            }
            function show_subrow_remove(id) {
                $('#sub2_row'+id).remove();
            }
            var c = 0;
            function appendFields() {
                add_element('element0');
                $("#cateogry_top0").val('');
                $('#top-plus').prop('disabled', true);
                /*  c = c+1;
                                                d = c-1;
                                                //console.log('clicked');
                                                c_name = '#category_top'+d;
                                                //alert( $(c_name).val());
                                                $('.category').text( $(c_name).val() );
                                                var html_input =
                                                `<div class="col-md-4 no-padding" id="prod_`+c+`">
                                                    <input style='margin-left: -11px;' type="text" name="category_top[]" id="cateogry_top`+c+`" placeholder="" class="form-control width-200 height-50 float-left" list="cat" />
                                                    
                                                    <button type="button" style='margin-left:-4px;' onclick="appendFields(0)" class="btn btn-primary btn-custom valid align-baseline rounded"><i class="fa fa-plus"></i> </button>
                                                    <button type="button"  onclick="func_common_remove('#prod_`+c+`')"  class="btn red  valid btn-custom align-baseline rounded"><i class="fa fa-minus"></i> </button>
                                                    <button type="button" onclick="show_category_name('category`+c+`','cateogry_top`+c+`')" class="btn btn-custom btn-success  valid align-baseline rounded"><i class="fa fa-check"></i> </button>
                                                    
                                                    
                                                </div>`
                                                
                                                $('#product').append(html_input);
                                                
                                                
                                                var html_input_category =
                                                `<div id="top_row_c`+c+`"><hr class="style1 mt-3">
                                                    <h5 class="category`+c+`"></h5>
                                                    <div class="row" id="prd`+c+`">
                                                        <div style='    margin-left: -6px;' class="col-md-4 remove-pad no-padding">
                                                            <input class="form-control width-200 right height-50" name="p_name_0" id="p_name">
                                                            <button type="button" onclick="add_element('element0')"  class="btn   brown valid btn-height-34  align-baseline rounded"> <i class="fa fa-arrow-right"></i> </button>
                                                            <button type="button" onclick="show_sub_product(`+c+`)"  class="btn btn-primary btn-height-34  valid align-baseline rounded"><i class="fa fa-plus"></i> </button>
                                                            <button type="button" onclick="func_common_remove('#prd`+c+`')"  class="btn red  btn-height-34  valid align-baseline rounded "><i class="fa fa-minus"></i> </button>
                                                            <button type="button" class="btn btn-height-34 btn-success valid align-baseline   rounded"><i class="fa fa-check"></i> </button>
                                                            <button type="button" class="btn purple btn-height-34  valid align-baseline rounded"><i class="fa fa-arrow-down"></i> </button>
                                                        </div>
                                                    </div></div>
                                                    `
                                                    $('#category').append(html_input_category)*/
                                                    }
                                                    
                                                    function removeField(id) {
                                                    console.log('removeField', id)
                                                    $('#'+id).remove();
                                                    $('#top_row_c'+id).remove();
                                                    $('#c'+id).remove();
                                                    $('#ranges'+id).remove();
                                                    
                                                    }
                                                    
                                                    function removeField2(id) {
                                                    console.log('removeField', id)
                                                    $('#'+id).remove();
                                                    }
                                                    function appendFieldsR() {
                                                    i = i+1
                                                    console.log('clicked')
                                                    //$('#company_keywords').val()='fefe';
                                                    var html_input =
                                                    `
                                                    <div id="r`+i+`" class="row">
                                                        <div class="col-sm-3">
                                                            <div class="form-group">
                                                                <label for="location_id">Representative Name </label>
                                                                <input placeholder="Please Enter Representative Name" type="text" class="form-control valid" name="represent_name[]" value="" id="represent_name"  />
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <div class="form-group">
                                                                <label for="area_id">Representative Email </label>
                                                                <input placeholder="Please Enter Representative email" type="text" class="form-control valid" name="represent_email[]" value="" id="represent_email"  />
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <div class="form-group">
                                                                <label for="area_id">Representative Phone </label>
                                                                <input placeholder="Please Enter Representative Phone" type="text" class="form-control valid" name="represent_mobile[]" value="" id="represent_mobile"  />
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <div class="form-group">
                                                                <button onclick="removeFieldR(`+i+`)" style="border-radius: 10px; margin-top: 29px;" class="btn btn-danger valid align-baseline"><i class="fa fa-minus"></i> </button>
                                                            </div>
                                                        </div>
                                                    </div>`
                                                    $('#representative').append(html_input)
                                                    }
                                                    function removeFieldR(id) {
                                                    
                                                    $('#r'+id).remove();
                                                    }
                                                    /*     function clonetreetimes(id) {
                                                    for (let i = 0; i < 3; i++) {
                                                    c = c + 1;
                                                    console.log(id)
                                                    let elements =
                                                    `<div class="row" id="prd_`+c+`">
                                                        <div class="col remove-pad no-padding row">
                                                            <div id="element_`+c+`" class="row pl-2">
                                                                <input class="form-control width-200 height-50 right" style='margin-top:5px;    margin-right: 15px;'  name="p_name_0" id="p_name0">
                                                            </div>
                                                            <button type="button" onclick="add_element('element_`+c+`')"  class="btn   brown valid align-baseline btn-custom rounded"> <i class="fa fa-arrow-right"></i> </button>
                                                            <button type="button" onclick="remove_element('prd_`+c+`')"  class="btn  red valid align-baseline btn-custom rounded"> <i class="fa fa-minus"></i> </button>
                                                            <button type="button" onclick="show_sub_product(0)"  class="btn btn-primary valid align-baseline btn-custom rounded"><i class="fa fa-plus"></i> </button>
                                                            <button type="button" class="btn  btn-success valid align-baseline btn-custom rounded"><i class="fa fa-check"></i> </button>
                                                            <button type="button" onclick="clonetreetimes('category')" class="btn  btn-custom valid align-baseline purple rounded"><i class="fa fa-arrow-down"></i> </button>
                                                        </div>
                                                    </div>`;
                                                    $('#'+id).append(elements)
                                                    }
                                                    }*/
                                                    </script>
                                                    @endsection
                                                    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                                        <div class="modal-dialog modal-lg">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                                    <h4 class="modal-title" id="myLargeModalLabel">Upload Products Images</h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <div class="col-12">
                                                                        <div class="form-group">
                                                                            <label for="image"> Images <span class="text-danger"></span></label>
                                                                            <div id="dropzone">
                                                                                <div class="dropzone needsclick" id="gallery-image-upload">
                                                                                    <div class="dz-message needsclick">
                                                                                        Upload Products Images
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <br/>
                                                                            <div id="gallery-preview-image" style="display: none;">
                                                                                <div class="dz-preview dz-file-preview">
                                                                                    <div class="dz-image">
                                                                                        <img data-dz-thumbnail=""/>
                                                                                    </div>
                                                                                    <div class="dz-details">
                                                                                        <div class="dz-size"><span data-dz-size=""></span></div>
                                                                                        <div class="dz-filename"><span data-dz-name=""></span></div></div>
                                                                                        <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress=""></span></div>
                                                                                        <div class="dz-error-message"><span data-dz-errormessage=""></span></div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    </div><!-- /.modal-content -->
                                                                    </div><!-- /.modal-dialog -->
                                                                    </div><!-- /.modal -->
                                                                    <!--    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>-->
                                                                    <script type="text/javascript"  src="//code.jquery.com/jquery-1.6.4.js"></script>
                                                                    <script>
                                                                    $(".purple").live('click', function() {
                                                                    var $row    = $(this).closest('.row');
                                                                    var $clone = $row.clone();
                                                                    // $clone.find(':text').val('');
                                                                    $row.after($clone);
                                                                    });
                                                                    $(".purp3").live('click', function() {
                                                                    var $row    = $(this).closest('.cat_product');
                                                                    var $clone = $row.clone();
                                                                    // $clone.find(':text').val('');
                                                                    $row.after($clone);
                                                                    });
                                                                    $(document).ready(function() {
                                                                    $('#top-plus').prop('disabled', true);
                                                                    $('#cateogry_top0').keyup(function() {
                                                                    if($(this).val() != '') {
                                                                    $('#top-plus').prop('disabled', false);
                                                                    }
                                                                    });
                                                                    });
                                                                    </script>
                                                                    <style href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/dropzone.css"></style>
                                                                    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/dropzone.js"></script>
                                                                    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-beta/css/bootstrap.min.css">
                                                                    <link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
                                                                    <link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/examples/assets/app.css">
                                                                    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.0.4/popper.js"></script>
                                                                    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
                                                                    <script src="https://cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.11.1/typeahead.bundle.min.js"></script>
                                                                    <script src="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
                                                                    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
                                                                    <script>
                                                                    var data = '{!! json_encode($categories) !!}'
                                                                    console.log(data)
                                                                    //get data pass to json
                                                                    var task = new Bloodhound({
                                                                    datumTokenizer: Bloodhound.tokenizers.obj.whitespace("name"),
                                                                    queryTokenizer: Bloodhound.tokenizers.whitespace,
                                                                    local: jQuery.parseJSON(data) //your can use json type
                                                                    });
                                                                    task.initialize();
                                                                    var elt = $("#category");
                                                                    elt.tagsinput({
                                                                    itemValue: "id",
                                                                    itemText: "name",
                                                                    typeaheadjs: {
                                                                    name: "name",
                                                                    displayKey: "name",
                                                                    source: task.ttAdapter()
                                                                    }
                                                                    });
                                                                    Dropzone.autoDiscover = false;
                                                                    var dropzone = new Dropzone(document.getElementById('colors-upload'), {
                                                                    url: "{{route('product.color.image.update')}}", // Set the url
                                                                    previewTemplate: document.querySelector('#preview-color-image').innerHTML,
                                                                    parallelUploads: 2,
                                                                    thumbnailHeight: 120,
                                                                    thumbnailWidth: 120,
                                                                    headers: {
                                                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                                                    },
                                                                    filesizeBase: 1000,
                                                                    thumbnail: function(file, dataUrl) {
                                                                    if (file.previewElement) {
                                                                    file.previewElement.classList.remove("dz-file-preview");
                                                                    var images = file.previewElement.querySelectorAll("[data-dz-thumbnail]");
                                                                    for (var i = 0; i < images.length; i++) {
                                                                    var thumbnailElement = images[i];
                                                                    thumbnailElement.alt = file.name;
                                                                    thumbnailElement.src = dataUrl;
                                                                    }
                                                                    setTimeout(function() { file.previewElement.classList.add("dz-image-preview"); }, 1);
                                                                    }
                                                                    }
                                                                    });
                                                                    //    Gallery Uploading
                                                                    var dropzone = new Dropzone(document.getElementById('gallery-image-upload'), {
                                                                    url: "{{route('gallery.products.images.upload')}}", // Set the url
                                                                    previewTemplate: document.querySelector('#gallery-preview-image').innerHTML,
                                                                    parallelUploads: 2,
                                                                    thumbnailHeight: 120,
                                                                    thumbnailWidth: 120,
                                                                    headers: {
                                                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                                                    },
                                                                    filesizeBase: 1000,
                                                                    thumbnail: function(file, dataUrl) {
                                                                    if (file.previewElement) {
                                                                    file.previewElement.classList.remove("dz-file-preview");
                                                                    var images = file.previewElement.querySelectorAll("[data-dz-thumbnail]");
                                                                    for (var i = 0; i < images.length; i++) {
                                                                    var thumbnailElement = images[i];
                                                                    thumbnailElement.alt = file.name;
                                                                    thumbnailElement.src = dataUrl;
                                                                    }
                                                                    setTimeout(function() { file.previewElement.classList.add("dz-image-preview"); }, 1);
                                                                    }
                                                                    }
                                                                    });
                                                                    function addToSessions(id) {
                                                                    $("#"+id).addClass("text-success");
                                                                    // Request a new token
                                                                    axios.post('{{route('add.to.session')}}', {
                                                                    'id': id,
                                                                    })
                                                                    .then(function (response) {
                                                                    console.log(response.data)
                                                                    if (response.data.type === 'add') {
                                                                    $("#"+id).addClass("text-success");
                                                                    }
                                                                    if (response.data.type === 'remove') {
                                                                    $("#"+id).removeClass("text-success");
                                                                    }
                                                                    })
                                                                    .catch(function (error) {
                                                                    console.log(error);
                                                                    })
                                                                    .then(function () {
                                                                    console.log('hello world')
                                                                    });
                                                                    }


                                                                    function updateOrder(id) {
                                                                        let index = document.getElementById(id).value
                                                                        console.log(id, index)

                                                                        axios
                                                                            .post(APP_URL+'/updateOrderPriceListColumn', {
                                                                                id: id,
                                                                                index: index
                                                                            })
                                                                            .then(response => {
                                                                                console.log(response)
                                                                            })
                                                                            .catch(error => {
                                                                                console.log(error)
                                                                            })

                                                                    }
                                                                    </script>