@extends('admin.layouts.app')
@section('content')
@include('admin.layouts.topbar')
<div class="container" style="position:absolute; width:100%">


<script> 
function validatePassword() {
    if($('#password').val() != $('#cpassword').val() ){
            $('.msg_password_mismatch').fadeIn(2000);
            return false;
            e.preventDefault();
            
        }
        $( "#frmForgot" ).submit();



        var validator = $("#frmForgot").validate({
            rules: {
                password: "required",
                confirmpassword: {
                    equalTo: "#cpassword"
                }
            },
            messages: {
                password: " Enter Password",
                confirmpassword: " Enter Confirm Password Same as Password"
            }
        });
        if (validator.form()) {
            alert('Sucess');
        }

       
    }

</script>
 
<!-- <div class="alert alert-primary alert-dismissible fade show" role="alert">
UI need to implements<br>
  CC email popup is also need to implement
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div> 
<h3 align=center>Add New Contact</h3>-->
 <div class="content">
    <div class="row">  
           <div class="col-lg-9 mt-5">
            @if(session()->has('message'))
            <div class="alert alert-success" id="myElem">
              <button type="button" class="close" data-dismiss="alert"  aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        
                {{ session()->get('message') }}
            </div>
        @endif

        @if(session()->has('error'))
            <div class="alert alert-danger" id="myElem">
              <button type="button" class="close" data-dismiss="alert"  aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        
                {{ session()->get('error') }}
            </div>
        @endif
<div class="card-box">  


 
          <!-- <h3>Portals</h3> -->
          <ul class="nav nav-tabs tabs-bordered">
            <li class="nav-item">
              <a href="#Profile" data-toggle="tab" aria-expanded="true" class="nav-link active show">Profile</a>
            </li>
            <li class="nav-item">
              <a href="#Password" data-toggle="tab" aria-expanded="fale" class="nav-link ">Password</a> </li>
          </ul>
          <div class="tab-content">
            
                            
          <div class="tab-pane active show" id="Profile">
            <h4>Personal Information</h4>
            <div class="form-row">

              <div class="col-md-4 mt-4">
                <img src={{ url('/')."/theme/admin/assets/images/users/avatar-1.jpg" }} width="82">
              </div>
              
              
            </div>
              <div class="form-row">

                <div class="col-md-4 mt-4">
                  <div>Full Name <span class="text-danger">*</span></div>
                  <div class="font-weight-bold">{{$user->name}}</div>
                </div>
                
                
              </div>

              <div class="form-row">
                <div class="col-md-4 mt-4">
                  <div>Email Address<span class="text-danger">*</span></div>
                  <div class="font-weight-bold">{{$user->email}}</div>
                </div>
                
                

              </div>
            <hr>
            <div> 
              {{-- <button type="submit"  href="{{ url('merchant/account')}}" class="btn btn-primary">Edit Profile</button> --}}
            <a class="btn btn-primary" href="{{ route('viewAdminEditProfile')}}">Edit Profile</a>
            </div>
          </div>

          <div class="tab-pane" id="Password">
            <div class="row">
              <div class="col-7">
                <h4 class="text-">Change Password</h4>

               <form class="form-contact contact_form" action="{{ route('AdminPasswordUpdate') }}"  method="POST"> 
                {{ csrf_field() }}

                 <div class="input-group" >
                                 <input  type="password" class="form-control @error('email') is-invalid @enderror" value="" id="current-password" placeholder="Enter Current Password"  name="current_password" autocomplete="new-password"   minlength="6" required=""   >
                                     @error('current-password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{$errors->first('current-password')}}</strong>
                                        </span>
                                    @enderror
                                    <i  type="button" id="current-pass-status" class="eye fa fa-eye" aria-hidden="true" onClick="viewPassword3()" style="background: #ccc; border-top-right-radius: 5px; border-bottom-right-radius: 5px;"
   ></i>
                            </div>
                            <br>
                            <!-- End of the current password -->
                            <div class="input-group" >
                                 <input  type="password" class="form-control @error('email') is-invalid @enderror" value="" id="password" placeholder="Enter Password"  name="password" autocomplete="new-password"   minlength="6" required=""   >
                                     @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{$errors->first('password')}}</strong>
                                        </span>
                                    @enderror
                                    <i  type="button" id="pass-status" class="eye fa fa-eye" aria-hidden="true" onClick="viewPassword()" style="background: #ccc; border-top-right-radius: 5px; border-bottom-right-radius: 5px;"
   ></i>
                            </div>
                            <br>
                            <div class="input-group">
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation"  placeholder="Enter confirm Password" required autocomplete="new-password" value=""  >
                                    <i  type="button" id="confirm-pass-status" class="eye fa fa-eye" aria-hidden="true" onClick="viewPassword1()" style="background: #ccc; border-top-right-radius: 5px; border-bottom-right-radius: 5px;"
 ></i>
                                     @error('password_confirmation')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password_confirmations') }}</strong>
                                    </span>
                                    @enderror
                            </div>
         
                            <br>
                          <div>
                          <button type="submit"   class="btn btn-primary" >Save</button>
                        {{-- <button type="button" class="btn btn-light">Cancel</button> --}}
                    
                        </div>
                      </form>

                  
                
                
                
              </div>
            </div>
            <hr>
            
          </div>
        </div>
            </div>
        </div>
          <div class="col-lg-3 mt-5">
            <div class="card-box">
            <h5>Setting up your profile</h5>
            <p>Here you can edit or update your profile information (i.e) Name, Phone number etc.</p>
            <h5>Update Password</h5>
            <p>You can create or update your password <strong>(A strong password will keep your account secure).</strong></p>
          </div>
          </div>
      </div>
    <!-- container -->
  </div>
</div>

 
<style>
.error{color:red;} 
.note-popover.popover{display:none;}
</style>

<script>
  
    function readURL(input) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();
          var image_x = document.getElementById('old_pic');

          reader.onload = function (e) {
            $('#blah').removeClass('d-none');
            if(image_x != null)
            {
                image_x.parentNode.removeChild(image_x);
            }  
            $('#blah')
                  .attr('src', e.target.result)
                  .width(100)
                  .height(100);
          };
  
          reader.readAsDataURL(input.files[0]);
      }
  }
  
    </script>

<script> 
  




function viewPassword3()
{
  var passwordInput = document.getElementById('current-password');
  var passStatus = document.getElementById('current-pass-status');
 
  if (passwordInput.type == 'password'){
    passwordInput.type='text';
    passStatus.className='fa fa-eye-slash';
    
  }
  else{
    passwordInput.type='password';
    passStatus.className='fa fa-eye';
  }

}


function viewPassword()
{
  var passwordInput = document.getElementById('password');
  var passStatus = document.getElementById('pass-status');
 
  if (passwordInput.type == 'password'){
    passwordInput.type='text';
    passStatus.className='fa fa-eye-slash';
    
  }
  else{
    passwordInput.type='password';
    passStatus.className='fa fa-eye';
  }
}


function viewPassword1()
{
  var passwordInput = document.getElementById('password-confirm');
  var passStatus = document.getElementById('confirm-pass-status');
 
  if (passwordInput.type == 'password'){
    passwordInput.type='text';
    passStatus.className='fa fa-eye-slash';
    
  }
  else{
    passwordInput.type='password';
    passStatus.className='fa fa-eye';
  }

}





</script>


<style>

 .fa-eye, .fa-eye-slash{
    border: 1px solid #ccc;
    padding: 9px 13px;
    background-color: rgb(255, 255, 255);
 

      #preloader-active{
    display: none;
}
.input-group i {
background: #ccc;
margin-left:  0px;
 padding: 8px;
}

  .pass_show{position: relative} 

.pass_show .ptxt { 

position: absolute; 

top: 66%; 

right: 10px; 

z-index: 1; 

color: #2d7bf4; 

margin-top: -10px; 

cursor: pointer; 

transition: .3s ease all; 

} 

.pass_show .ptxt:hover{color: #333333;} 

.msg_password_mismatch{display:none;}
</style>
@endsection
