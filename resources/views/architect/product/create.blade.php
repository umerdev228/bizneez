@extends('admin.layouts.app')
@section('content')
    @include('admin.layouts.topbar')
    <!-- Google Chart JS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/min/dropzone.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/dropzone.js"></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <!-- Start Page content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if(session()->has('message'))
                        <div class="alert alert-success" style="width:803px ">
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-9 mt-5">
                            <div class="card-box">
                                <form method="POST" action="{{ route('admin.products.store') }}" enctype="multipart/form-data">
                                    @csrf
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="name"> Name <span class="text-danger">*</span></label>
                                                <input type="text" class="form-control valid" name="name" id="name"  required />
                                                @if ($errors->has('name'))
                                                    <span class="error">{{ $errors->first('name') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="price"> Price <span class="text-danger">*</span></label>
                                                <input type="text" class="form-control valid" name="price" id="price"  required />
                                                @if ($errors->has('price'))
                                                    <span class="error">{{ $errors->first('price') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="second_price"> Second Price <span class="text-danger">*</span></label>
                                                <input type="text" class="form-control valid" name="second_price" id="second_price"  required />
                                                @if ($errors->has('second_price'))
                                                    <span class="error">{{ $errors->first('second_price') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="discount"> Discount <span class="text-danger">*</span></label>
                                                <input type="text" class="form-control valid" name="discount" id="discount"  required />
                                                @if ($errors->has('discount'))
                                                    <span class="error">{{ $errors->first('discount') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="quantity"> Quantity <span class="text-danger">*</span></label>
                                                <input type="text" class="form-control valid" name="quantity" id="quantity"  required />
                                                @if ($errors->has('quantity'))
                                                    <span class="error">{{ $errors->first('quantity') }}</span>
                                                @endif
                                            </div>
                                        </div>
{{--                                        <div class="col-12">--}}
{{--                                            <div class="form-group">--}}
{{--                                                <label for="color"> Color <span class="text-danger">*</span></label>--}}
{{--                                                <input type="color" class="form-control valid" name="color" id="color"  required />--}}
{{--                                                @if ($errors->has('color'))--}}
{{--                                                    <span class="error">{{ $errors->first('color') }}</span>--}}
{{--                                                @endif--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="Description">Description<span class="text-danger">*</span></label>
                                                <textarea id="Description" class="form-control rounded-0" name="description" rows="5"></textarea>
                                                @if ($errors->has('description'))
                                                    <span class="error">{{ $errors->first('description') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="name"> Categories <span class="text-danger">*</span></label>
                                                <select name="categories[]" class="select2 form-control select2-multiple" multiple data-placeholder="Choose ...">
                                                    <optgroup label="Categories">
                                                        @foreach($categories as $category)
                                                            <option value="{{ $category->id }}"> {{ $category->name }} </option>
                                                        @endforeach
                                                    </optgroup>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="color"> Colors <span class="text-danger"></span></label>
                                                <input type="file" class="form-control valid" name="color[]" id="color" multiple/>
                                                @if ($errors->has('color'))
                                                    <span class="error">{{ $errors->first('color') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="row p-3" id="uploaded_colors">

                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="image"> Images <span class="text-danger">*</span></label>
                                                <input type="file" class="form-control valid" name="image[]" id="image"  required multiple/>
                                                @if ($errors->has('image'))
                                                    <span class="error">{{ $errors->first('image') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group row p-3" id="uploaded_images">

                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group mt-3">
                                        <input class="btn btn-primary" type="submit" value="Create Product">
                                        <button onclick="history.back()" type="button" class="btn">Cancel</button>
                                    </div>
                                </form>
                            </div> <!-- end card-box -->
                        </div>

                    </div> <!-- container -->
                </div> <!-- content -->
            </div>
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

    <script>
        $(document).ready(function() {
            if (window.File && window.FileList && window.FileReader) {
                $("#image").on("change", function(e) {
                    var files = e.target.files,
                        filesLength = files.length;
                    for (var i = 0; i < filesLength; i++) {
                        var f = files[i]
                        var fileReader = new FileReader();
                        fileReader.onload = (function(e) {
                            var file = e.target;
                            $("#uploaded_images").append( $("<span class=\"pip\">" +
                                "<img class=\"imageThumb m-3\" width='100' src=\"" + e.target.result + "\" title=\"" + file.name + "\"/>" +
                                "<br/><span class=\"remove ml-3 \">Remove image</span>" +
                                "</span>"));
                            $(".remove").click(function(){
                                $(this).parent(".pip").remove();
                            });
                        });
                        fileReader.readAsDataURL(f);
                    }
                    console.log(files);
                });
                $("#color").on("change", function(e) {
                    var files = e.target.files,
                        filesLength = files.length;
                    for (var i = 0; i < filesLength; i++) {
                        var f = files[i]
                        var fileReader = new FileReader();
                        fileReader.onload = (function(e) {
                            var file = e.target;
                            $("#uploaded_colors").append( $("<span class=\"pip\">" +
                                "<img class=\"imageThumb m-3\" width='50' src=\"" + e.target.result + "\" title=\"" + file.name + "\"/>" +
                                "<br/><span class=\"remove ml-3 \">Remove image</span>" +
                                "</span>"));
                            $(".remove").click(function(){
                                $(this).parent(".pip").remove();
                            });
                        });
                        fileReader.readAsDataURL(f);
                    }
                    console.log(files);
                });
            } else {
                alert("Your browser doesn't support to File API")
            }
        });
    </script>

@endsection
