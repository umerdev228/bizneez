@extends('admin.layouts.app')
@section('content')
<style>
    textarea.form-control{min-width: 300px!important;}
    .fa-remove{font-size: 24px;
    color: red;
    left: p;
    position: absolute;
    /* left: 104px; */
    margin-right: 0px;}
</style>  
    <div class="content">
        <div class="container-fluid">  
            <form action="{{route('admin.catalogue.page.store')}}" method="post" enctype="multipart/form-data">
                @csrf
                <input style="display: none;" value="1" type="text" name="page" id="page">
                <div id="template" class="row">
                    <div id="p1" style="display: none;" class="col-12">
                        <br><br><br><br><br>
                        <div class="row">
                        <div class="col-lg-12">
                            <h3>Catalogue</h3>
                        </div>
                            <div class="col-md-3">
                                <div>
                                    <div class="form-group">
                                        <select onchange="onCompanyChange(1)" class="form-control" name="company1" id="company1" >
                                            <option value="0">--Select Company--</option>
                                            @foreach($companies as $company)
                                                <option value="{{$company->id}}">{{$company->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <select onchange="onManufacturerSelect(1)" class="form-control" name="manufacturer1" id="manufacturer1" >
                                            <option value="0">--Select Manufacture--</option>
                                            @foreach($manufacturers as $manufacturer)
                                                <option value="{{$manufacturer->id}}">{{$manufacturer->name}}</option>
                                            @endforeach
                                        </select>
                                        <br>
                                        <select onchange="onRangeChange(1)" class="form-control" name="range1" id="range1" >
                                            <option>--Select Ranges--</option>
                                        </select>
                                    </div>
{{--                                    <div class="form-group">--}}
{{--                                        <button type="button" class="btn btn-dark" data-toggle="modal" data-target="#descriptionModalLong">Choose Price List</button>--}}
{{--                                    </div>--}}
                                    <ul style="list-style: none">
                                         
                                        <li>
                                            <input onclick="showImageField(1)" checked id="option_1_1" name="option_1_1" type="checkbox" class="form-check form-check-inline">
                                            <label for="option_1_1">Option 1</label>
                                        </li>
                                        <li>
                                            <input onclick="showImageField(2)" checked id="option_1_2" name="option_1_2" type="checkbox" class="form-check form-check-inline">
                                            <label for="option_1_2">Option 2</label>
                                        </li>
                                        <li>
                                            <input onclick="showImageField(3)" checked id="option_1_3" name="option_1_3" type="checkbox" class="form-check form-check-inline">
                                            <label for="option_1_3">Option 3</label>
                                        </li>
                                        <li>
                                            <input onclick="showImageField(4)" checked id="option_1_4" name="option_1_4" type="checkbox" class="form-check form-check-inline">
                                            <label for="option_1_4">Option 4</label>
                                        </li> 
                                       <!--  <li>
                                            <input onclick="showImageField(5)" checked id="option_1_5" name="option_1_5" type="checkbox" class="form-check form-check-inline">
                                            <label for="option_1_5">Option 5</label>
                                        </li>
                                        <li>
                                            <input onclick="showImageField(6)" checked id="option_1_6" name="option_1_6" type="checkbox" class="form-check form-check-inline">
                                            <label for="option_1_6">Option 6</label>
                                        </li>
                                        <li>
                                            <input onclick="showImageField(7)" checked id="option_1_7" name="option_1_7" type="checkbox" class="form-check form-check-inline">
                                            <label for="option_1_7">Option 7</label>
                                        </li>
                                        <li>
                                            <input onclick="showImageField(8)" checked id="option_1_8" name="option_1_8" type="checkbox" class="form-check form-check-inline">
                                            <label for="option_1_8">Option 8</label>
                                        </li> 
                                        <li>
                                            <input onclick="showImageField(9)" checked id="option_1_9" name="option_1_9" type="checkbox" class="form-check form-check-inline">
                                            <label for="option_1_9">Option 9</label>
                                        </li> -->
                                    </ul>
                                    <input type="button" class="btn btn-primary" value="View Saved Catalogue" onclick="location.href='{{route('catalogue-saved-list')}}'">
                                </div>
                                <div class="clearfix">
                                    <br><br><br><br>
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="title1">Please Enter Text</label>
                                            <table style="width:100%">
                                                <tr>
                                                    <td>
                                                        <input type="text" name="title1" class="form-control" id="title1" style="float: left">
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" id="title_check1" name="title_check1">
                                                        <label for="title_check1">Show/Hide</label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="col-md-6" align="right">
                                        <div class="form-group">
                                            <label for="company_logo_1">Company Logo</label>
                                            <img width="50" src="{{asset('/assets/images/img.png')}}" id="company_logo_1">
                                        </div>
                                    </div>
                                </div>
                                <input style="display: none;" type="text" name="imageValue-1-1" id="imageValue-1-1">
                                <input style="display: none;" type="text" name="imageValue-1-2" id="imageValue-1-2">
                                <input style="display: none;" type="text" name="imageValue-1-3" id="imageValue-1-3">
                                <input style="display: none;" type="text" name="imageValue-1-4" id="imageValue-1-4">
                                 <input style="display: none;" type="text" name="logo" id="logo">
                                

                                <div class="form-group" align="center">
                                    <div class="row">
                                        <div class="col-md-6 mb-3">
                                            <div class=" div1">
                                                <img  onclick="selectImage('image-1-1', 'imageValue-1-1')"  alt="image" width="300px" height="300px" id="image-1-1" src="{{asset('assets/images/img.png')}}">  <i class="fa fa-remove d-none" id="r_image-1-1" onclick="removeImage('image-1-1', 'imageValue-1-1')"></i>
                                                <textarea  placeholder="Please enter text" name="description-1-1" class="form-control w-25" id="description-1-1" rows="3"></textarea>
                                                <input type="button"  value="Price List"  onclick="showModal(1,1)" class="btn btn-dark"> <br>
                                            </div>
                                        </div>
                                        <div class="col-md-6 mb-3"><div class=" div2">
                                            <img onclick="selectImage('image-1-2', 'imageValue-1-2')"  alt="image" width="300px" height="300px" id="image-1-2" src="{{asset('assets/images/img.png')}}"><i id="r_image-1-2"  class="fa fa-remove d-none" onclick="removeImage('image-1-2', 'imageValue-1-2')"></i>
                                            <textarea  placeholder="Please enter text"  name="description-1-2" class="form-control w-25" id="description-1-2" rows="3"></textarea>
                                             <input type="button"  value="Price List"  onclick="showModal(1,2)" class="btn btn-dark"> <br>
                                        </div></div>
                                        <div class="col-md-6 mb-3"><div class=" div3">
                                            <img  onclick="selectImage('image-1-3', 'imageValue-1-3')" alt="image" width="300px" height="300px" id="image-1-3" src="{{asset('assets/images/img.png')}}"><i id="r_image-1-3" class="fa fa-remove d-none" onclick="removeImage('image-1-3', 'imageValue-1-3')"></i>
                                             <textarea  placeholder="Please enter text"  name="description-1-3" class="form-control w-25" id="description-1-3" rows="3"></textarea>
                                              <input type="button"  value="Price List"  onclick="showModal(1,3)" class="btn btn-dark"> <br></div>
                                        </div>
                                        <div class="col-md-6 mb-3"><div class=" div4">
                                            <img  onclick="selectImage('image-1-4', 'imageValue-1-4')"  alt="image" width="300px" height="300px" id="image-1-4" src="{{asset('assets/images/img.png')}}"><i id="r_image-1-4" class="fa fa-remove d-none" onclick="removeImage('image-1-4', 'imageValue-1-4')"></i>
                                             <textarea  placeholder="Please enter text"  name="description-1-4" class="form-control w-25" id="description-1-4" rows="3"></textarea>
                                              <input type="button"  value="Price List"  onclick="showModal(1,4)" class="btn btn-dark"> <br></div>
                                        </div> 
                                    </div>
                                </div>
                                <div class="row text-light" style="background-color: #1d1e1f; margin: 0;">
                                    <div id="company_title_1" class="col-md-3">Bizneez.net</div> 
                                    <div id="company_address_1" class="col-md-7" align="center">Kemp House, 160 City Road, London, United Kingdom, EC1V 2NX</div>
                                    <div id="company_phone_1" class="col-md-3 float-right" align="right">+44(0)20 8524 5294
                                    </div>
                                </div>
                                <br>
                                <div class="form-group">
                                    <div align="center">
{{--                                        <button name="button" type="submit" value="submit" class="btn btn-success mb-2">Submit</button>--}}
                                        <button onclick="nextPage()" type="button" class="btn btn-primary mb-2">Next Page ++</button>
                                        <button name="button" type="submit" value="download" class="btn btn-dark mb-2"><i class="fa fa-download" aria-hidden="true"></i> Download PDF</button>
                                         <button formtarget="_blank" name="button" type="submit" value="preview" class="btn btn-success mb-2"> Preview</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div> <!-- container -->
    </div> <!-- content -->

    @include('admin.catalogue.includes.image-gallery')
    @include('admin.catalogue.includes.price-ranges')

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

    <script>


        let currentPage = 1;
        let currentId = 1;
        let selectedElement = 0;
        let imagesData = [];

        let public_path = '{{asset('/')}}';
        let selectedId = '';
        let selectedInputId = '';
        let lastSelectedImage = '';
        $('#p'+currentPage).css('display', 'block')
        $('.page'+currentPage).css('border', '4px solid green')

        function onManufacturerSelect(id) {
            let manufacturer = $('#manufacturer'+id).val()
            axios.post(APP_URL+'/getRangesByManufacturer', {id: manufacturer}).then(response => {
                console.log(response.data)
                response.data.ranges.forEach(function (range) {
                    console.log(range)
                    $('#range'+id).append(`<option value="`+range.id+`">`+range.name+`</option>`)
                })
            });
        }

        function onCompanyChange(id) {
            let val = document.getElementById('company'+id).value
            axios.post(APP_URL+'/getCompanyById', {id: val}).then(response => {
                let company = response.data.company

                $('#company_logo_'+id).removeAttr('src').attr('src', APP_URL+company.logo)
                $('#company_title_'+id).text(company.url)
                $('#company_address_'+id).text(company.address)
                $('#company_phone_'+id).text(company.phone);
                $('#logo').val(APP_URL+company.logo);
            });
        }

        function selectImage(id, inputId) {  
            selectedId = id
            selectedInputId = inputId
            $('#imageGallery').modal('toggle');
            $('#r_'+id).removeClass('d-none');
        }

        function selectImages(id) {      
            if (lastSelectedImage !== '') {
                $('#'+lastSelectedImage).css('border', '')
            }
        
            let image = imagesData.find(image => image.id === JSON.parse(id))
            $('#'+id).css('border', '2px solid green')
            $('#'+selectedId).attr('src', public_path+image.path);
            $('#'+selectedInputId).val(id)
            lastSelectedImage = id

        }

        function removeImage(id,image_value){ 
            //let image = imagesData.find(image => image.id === JSON.parse(id))
              $('#'+id).attr('src', '{{asset('assets/images/img.png')}}');
              $('#r_'+id).addClass('d-none');
              $('#'+image_value).val('');
             
        }

        function nextPage() {
            $('#p'+currentPage).css('display', 'none')
            $('.page'+currentPage).css('border', '')
            currentPage++
            appendPageSection()
            $('#p'+currentPage).css('display', 'block')
            $('.page'+currentPage).css('border', '4px solid green')
            $('#page').val(currentPage)
        }

        function showImageField(id) {
            console.log(document.getElementById('option_'+currentPage+'_'+id).checked)
            var d = '.div'+id;   
            if (document.getElementById('option_'+currentPage+'_'+id).checked) { 
                $('#image-'+currentPage+'-'+id).css('display', 'block')
                $(d).show()
            }
            else { 
                $('#image-'+currentPage+'-'+id).css('display', 'none')
                $(d).hide();
            }
        }
        function selectPriceList(data) {
            let record = JSON.parse(data)
            let description = record.COL5
            let product_code = record.COL2
            let price = record.COL8
            let length = record.COL10
            let width = record.COL11
            let height = record.COL12
            let depth = record.COL13
            price = price.replace('?','')
            price = price.replace(',','')
            document.getElementById('description-'+currentPage+'-'+selectedElement).value = product_code + '  ' + description + ' ' + depth + ' ' + length + ' ' + width + ' ' + height
            // document.getElementById('price'+selectedRow).value = parseFloat(price)
            $('#priceListModal').modal('hide');
        }

        function showModal(page, ele) {
            selectedElement = ele
            $('#priceListModal').modal('toggle')
        }

        function onRangeChange(id) {
            let val = document.getElementById('range'+id).value;
          
            axios.post(APP_URL+'/getImagesByRange', {id: val}).then(response => {
                console.log(response.data)
                if (response.data.type === 'success') {
                    $('#range-images').empty()
                    imagesData = []
                    imagesData = response.data.images
                    response.data.images.forEach(function (image) {
                        console.log(image)
                        let ele =  `
                                <div id="`+image.id+`" class="col-lg-3 col-xl-2 border-success p-2">
                                    <img width="120px" onclick="selectImages(`+image.id+`)" src="`+APP_URL+`/`+image.path+`" alt="icon" class="p-2">
                                </div>`
                        $('#range-images').append(ele)
                    })
                }

            });
        }

        function appendPageSection() {
            let ele = `
                <div id="p`+currentPage+`" class="col-12">
                    <br><br><br><br><br>
                        <div class="row">
                            <div class="col-lg-12">
                                <h3>Catalogue</h3>
                            </div>
                            <div class="col-md-3">
                                <div>
                                    <div class="form-group">
                                        <select onchange="onCompanyChange(`+currentPage+`)" class="form-control" name="company`+currentPage+`" id="company`+currentPage+`" >
                                            <option value="0">--Select Company--</option>
                                            @foreach($companies as $company)
                                                <option value="{{$company->id}}">{{$company->name}}</option>
                                            @endforeach
                                        </select>
                                        </div>
                                        <div class="form-group">
                                        <select onchange="onManufacturerSelect(`+currentPage+`)" class="form-control" name="manufacturer`+currentPage+`" id="manufacturer`+currentPage+`" >
                                            <option value="0">--Select Manufacture--</option>
                                            @foreach($manufacturers as $manufacturer)
                                                <option value="{{$manufacturer->id}}">{{$manufacturer->name}}</option>
                                            @endforeach
                                        </select>
                                        <br>
                                            <select class="form-control" onchange="onRangeChange(`+currentPage+`)"   name="range`+currentPage+`" id="range`+currentPage+`" >
                                                <option>--Select Ranges--</option>

                                            </select>
                                        </div>
                                       <ul style="list-style: none">
                                            <li>
                                            <input onclick="showImageField(1)" checked id="option_`+currentPage+`_1" name="option_`+currentPage+`_1" type="checkbox" class="form-check form-check-inline">
                                            <label for="option_`+currentPage+`_1">Option 1</label>
                                            </li>
                                            <li>
                                            <input onclick="showImageField(2)" checked id="option_`+currentPage+`_2" name="option_`+currentPage+`_2" type="checkbox" class="form-check form-check-inline">
                                            <label for="option_`+currentPage+`_2">Option 2</label>
                                            </li>
                                            <li>
                                            <input onclick="showImageField(3)" checked id="option_`+currentPage+`_3" name="option_`+currentPage+`_3" type="checkbox" class="form-check form-check-inline">
                                            <label for="option_`+currentPage+`_3">Option 3</label>
                                            </li>
                                            <li>
                                            <input onclick="showImageField(4)" checked id="option_`+currentPage+`_4" name="option_`+currentPage+`_4" type="checkbox" class="form-check form-check-inline">
                                            <label for="option_`+currentPage+`_4">Option 4</label>
                                            </li>
                                            <li>
                                           
                                       </ul>
                                    </div>
                                    <div class="clearfix">
                                        <br><br><br><br>
                                    </div>
                                </div>
                                <div class="col-md-7">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="title1">Please Enter Text</label>
                                                <table style="width:100%">
                                                    <tr>
                                                        <td>
                                                            <input type="text" name="title`+currentPage+`" class="form-control" id="title`+currentPage+`" style="float: left">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" id="title_check`+currentPage+`" name="title_check`+currentPage+`">
                                                            <label for="title_check`+currentPage+`">Show/Hide</label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-md-6" align="right">
                                            <div class="form-group">
                                                <label for="company_logo_`+currentPage+`">Company Logo</label>
                                                <img width="50" src="{{asset('/assets/images/img.png')}}" id="company_logo_`+currentPage+`">
                                            </div>
                                        </div>
                                    </div>

                                        <input style="display: none;" type="text" name="imageValue-`+currentPage+`-1" id="imageValue-`+currentPage+`-1">
                                        <input style="display: none;" type="text" name="imageValue-`+currentPage+`-2" id="imageValue-`+currentPage+`-2">
                                        <input style="display: none;" type="text" name="imageValue-`+currentPage+`-3" id="imageValue-`+currentPage+`-3">
                                        <input style="display: none;" type="text" name="imageValue-`+currentPage+`-4" id="imageValue-`+currentPage+`-4">
                                   

                                        <div class="form-group" align="center">
                                            <div class="row">
                                                <div class="col-md-6 mb-3">
                                                    <div class=" div1">
                                                        <img  onclick="selectImage('image-`+currentPage+`-1', 'imageValue-`+currentPage+`-1')"  alt="image" width="300px" height="300px" id="image-`+currentPage+`-1" src="{{asset('assets/images/img.png')}}">
                                                        <textarea placeholder="Please enter text" name="description-`+currentPage+`-1" class="form-control w-25" id="description-`+currentPage+`-1" rows="3"></textarea>
                                                        <input type="button"  value="Price List"  onclick="showModal(`+currentPage+`, 1)" class="btn btn-dark"> <br>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 mb-3">
                                                <div class=" div2">
                                                    <img onclick="selectImage('image-`+currentPage+`-2', 'imageValue-`+currentPage+`-2')"  alt="image" width="300px" height="300px" id="image-`+currentPage+`-2" src="{{asset('assets/images/img.png')}}">
                                                    <textarea  placeholder="Please enter text"  name="description-`+currentPage+`-2" class="form-control w-25" id="description-`+currentPage+`-2" rows="3"></textarea>
                                                     <input type="button"  value="Price List"  onclick="showModal(`+currentPage+`, 2)" class="btn btn-dark"> <br>
                                                </div>
                                                </div>
                                                <div class="col-md-6 mb-3">
                                                <div class=" div3">
                                                    <img  onclick="selectImage('image-`+currentPage+`-3', 'imageValue-`+currentPage+`-3')" alt="image" width="300px" height="300px" id="image-`+currentPage+`-3" src="{{asset('assets/images/img.png')}}">
                                                     <textarea  placeholder="Please enter text"  name="description-`+currentPage+`-3" class="form-control w-25" id="description-`+currentPage+`-3" rows="3"></textarea>
                                                      <input type="button"  value="Price List"  onclick="showModal(`+currentPage+`, 3)" class="btn btn-dark">
                                                      <br>
                                                      </div>
                                                </div>
                                                <div class="col-md-6 mb-3"><div class=" div4">
                                                    <img  onclick="selectImage('image-`+currentPage+`-4', 'imageValue-`+currentPage+`-4')"  alt="image" width="300px" height="300px" id="image-`+currentPage+`-4" src="{{asset('assets/images/img.png')}}">
                                                     <textarea  placeholder="Please enter text"  name="description-`+currentPage+`-4" class="form-control w-25" id="description-`+currentPage+`-4" rows="3"></textarea>
                                                      <input type="button"  value="Price List"  onclick="showModal(`+currentPage+`, 4)" class="btn btn-dark"> <br></div>
                                                </div>
                                                
                                            </div>
                                        </div> 

                                        <div class="row text-light" style="background-color: #1d1e1f; margin: 0px;">
                                            <div id="company_title_`+currentPage+`" class="col-md-3">Bizneez.net</div>
                                            <!--   <div class="col-md-3"><a href="">Unsubscribe</a></div> -->
                                            <div id="company_address_`+currentPage+`" class="col-md-7" align="center">Kemp House, 160 City Road, London, United Kingdom, EC1V 2NX</div>
                                            <div id="company_phone_`+currentPage+`" class="col-md-3 float-right" align="right">+44(0)20 8524 5294
                                            </div>
                                        </div>
                                        <br>
                                        <div class="form-group">
                                            <div align="center">
                                        <div align="center">
                                        <button onclick="nextPage()" type="button" class="btn btn-primary mb-2">Next Page ++</button>
                                        <button name="button" type="submit" value="download" class="btn btn-dark mb-2"><i class="fa fa-download" aria-hidden="true"></i> Download PDF</button>
                                        <button formtarget="_blank" name="button" type="submit" value="preview" class="btn btn-success mb-2"> Preview</button>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>`

            $('#template').append(ele)
        }

    </script>
@endsection
