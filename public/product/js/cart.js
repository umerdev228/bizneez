
// Get Total Cart Items
    axios.get('/get-cart-total-item')
        .then(function (response) {
            // handle success
            if (response.data.type === 'success') {
                if (response.data.itemsCount) {
                    $('#processNext').css('display', 'block')
                }
                else {
                    $('#processNext').css('display', 'none')
                }
            }
            else {
                $('#processNext').css('display', 'none')
            }
        })
        .catch(function (error) {
            console.log(error);
        })

