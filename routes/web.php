<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Pusher\Pusher;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('auth')->put('user/{user}/online', 'UserOnlineController');
Route::middleware('auth')->put('user/{user}/offline', 'UserOfflineController');

//$pusher = new Pusher("1e9f3b1b538dc56a0e96", "8bafb23c202d5a59bfb2", "1237311", array('cluster' => 'ap2'));
//
//$pusher->trigger('chat', 'userOnSite', array('user_id' => Auth::id()));

Route::group(['middleware' => 'auth'], function() {
    Route::get('video_chat_webrtc_client', 'Webrtc\VideoChatController@clientToClient')->name('video_chat_webrtc_client_to_client');
    Route::get('video_chat_webrtc_employee_to_demo', 'Webrtc\VideoChatController@employeeToUser')->name('video_chat_webrtc_employee_to_demo');
    Route::get('video_chat_webrtc', 'Webrtc\VideoChatController@index')->name('video_chat_webrtc');
    Route::post('auth/video_chat_webrtc', 'Webrtc\VideoChatController@auth');
    Route::get('send-call-notification/{id}', 'NotificationController@index');
    Route::get('send-group-call-notification/{id}', 'NotificationController@groupCallNotification');


//    Group Video Calling
    Route::get('group_chat/{room}', 'Webrtc\GroupVideoChatController@index');

});

Route::post('access_token', 'API\AccessTokenController@generate_token');
Route::get('access_token', 'API\AccessTokenController@generate_token_Get');

//LOGOUT
Route::get('/logout', function () {
    \App\User::where('id', \Illuminate\Support\Facades\Auth::id())->update([
        'status' => 'offline',
        'on_site_status' => 'offline',
    ]);
    $pusher = new Pusher("1e9f3b1b538dc56a0e96", "8bafb23c202d5a59bfb2", "1237311", array('cluster' => 'ap2'));
    $pusher->trigger('webrtc-video', 'logoutUser', array('user_id' => Auth::id()));

    Auth::logout();
    //return redirect()->route('login');
    return redirect('/');   });
Route::get('welcome', function () {
    return view('welcome');
});

Route::get('/cache', function () {
    \Artisan::call('optimize:clear');

});

Route::get('/', function () {
   // return redirect()->route('login');
    return view('signin');
});
Route::get('/contact', function () {
    return view('contact');
});
Route::get('/faq', function () {
    return view('faq');
});

Route::get('/signup', function () {
    return redirect()->route('login');
    return view('signup');
});


Route::get('/signin', function () {
    return view('signin');
});

Route::get('/forgot', function () {
    return view('forgot');
});
// Route::get('forgot_password_sent', "UserController@forgot_password_sent");


Route::POST('contactus', 'ContactController@create');



Route::get('api/getPackages', "ApiController@getPackagesJson");
Route::get('api/get_payment_history', 'ApiController@get_payment_history');
Route::post('/contact/store', 'ContactController@store')->name('contact.store');
Route::get('admin/contact/remove/{id}', 'ContactController@destroy')->name('admin.contact.remove');


//cms pages
// Route::get('/ticketing', function () {
//     return view('ticketing');
// });
// Ticketing controller are used for the Home blade show
Route::get('ticketing', 'TicketingController@show');

Route::get('/collaboration', function () {
    return view('collaboration');
});
Route::get('/widget', function () {
    return view('widget');
});

/*Route::get('admin/manufacturers/view_all_inventory', function () {
    return view('admin.manufacturer.inventory.view_all_inventory');
})->name('view_all_inventory');
*/
Route::get('admin/manufacturers/view_all_inventory', 'InventoryController@index')->name('view_all_inventory');


Route::get('demo/login', 'LoginController@index')->name('demo.login');
Route::post('demo/login', 'LoginController@login')->name('demo.login');




Route::post('demo/appointment', 'AppointmentController@store')->name('demo.appointment');
 Route::get('notification/notification_counter_update' , 'NotficationController@notification_counter_update');
Route::post('admin/update-profile', 'UserController@update_profile')->name('AdminProfileUpdate');


Route::post('admin/manufacturers/cat_product_add', 'ManufacturerController@manufacture_cat_product_add')->name('AdminManufactureCategoryAdd');




Route::get('admin/contact', 'ContactController@index');



Route::get('admin/setting', 'UserController@setting');
Route::post('admin/setting/potal', 'UserController@admin_setting');
Route::get('admin/setting/potal/update/{id}', 'UserController@admin_setting_update');
Route::post('admin/setting/potal/update', 'UserController@admin_setting_portal_update');


Route::post('reset_admin_password' , 'UserController@reset_admin_password');
// Route::post('merchant_new_register', "RegisterController@create");
Route::post('merchant_new_register', array('as' => 'merchant_new_register', 'uses' => 'RegisterController@create'));
Route::get('merchant/verify/', "RegisterController@verify");
Route::post('merchent_reset_password', array('as' => 'merchent_reset_password', 'uses' => 'RegisterController@reset_password'));
Route::post('signin' ,'UserController@view' );













// Route::post('merchant_new_register', array('as' => 'merchant_new_register', 'uses' => 'UserController@create'));

//Route::get('merchant/verify/', "UserController@verify");


// Route::post('merchent_reset_password', array('as' => 'merchent_reset_password', 'uses' => 'UserController@reset_password'));

Route::get('stripe', 'StripePaymentController@stripe');
Route::post('stripe', 'StripePaymentController@stripePost')->name('stripe.post');

Route::get('call_api', "MerchantController@call_api");
/*Route::post('confirm_detail', function () {
  dd('fefefe');
});
*/

/*Route::post('merchant_new_register', function () {
         die('fefefefe');
});
*/

//if logout

Route::post('confirm_detail', array('as' => 'confirm_detail', 'uses' => 'MerchantController@confirm_detail'));
Route::get('confirm_reset_activation', array('as' => 'confirm_reset_activation', 'uses' => 'MerchantController@confirm_reset_activation'));
Route::get('confirm_url', array('as' => 'confirm_url', 'uses' => 'MerchantController@confirm_url'));
Route::get('channels', array('as' => 'confirm_url', 'uses' => 'MerchantController@channels'));
Route::post('support_team_invitation', array('as' => 'support_team_invitation', 'uses' => 'MerchantController@support_team_invitation'));


Auth::routes();



Route::group(['middleware'=> ['web','auth']  ], function(){

	// Route::get('/home', 'HomeController@index')->name('home');

	Route::get('/home', function(){        //Supper Admin
  //dd(Auth::user()->role);
        if (Auth::user()->role ==0 ) {
            //return view('getting-started');

//              return redirect('get_start_dashboard');
        	//dd(Auth::user()->email);

        }
        else  if (Auth::user()->role ==1 ) {//Merchant Admin
            //$users['users'] = \App\User::all();
            //return view('merchant_admin', $users);
            return redirect('admin/dashboard');
        }

    });

});

//Route::get('get_start_dashboard', 'MerchantController@index')->name('get_start_dashboard');
// Route::post('login', array('as' => 'getting_start', 'uses' => 'MerchantController@getting_start'));





Route::get('pricing', 'StripePaymentController@pricing');
//Route::post('confirm_detail', array('as' => 'confirm_detail', 'uses' => 'MerchantController@confirm_detail'));



//Route::post('admin/packages/store', 'PackageOption@store');
//Admin Routes
/*Route::get('admin/dashboard', function () {
    return view('admin.dashboard');
});*/
Route::get('admin/ajaxNotification', 'DashboardController@ajaxNotification');
Route::get('admin/notification', 'NotficationController@notification');




// Route::get('admin/reset_password', 'DashboardController@reset_password');

//Route::post('Search' , 'DashboardController@Search');admin/packages_add


Route::get('admin/packages', 'PackagesController@index');
Route::get('admin/packages/create', 'PackagesController@create');
Route::get('admin/packages_add', 'PackagesController@create');//for the package are not select teh option
Route::post('admin/packages/store', 'PackagesController@store');

Route::get('admin/packages/edit/{id}', 'PackagesController@edit');
Route::post('admin/packages/update', 'PackagesController@update');
Route::get('admin/user/view/{id}', 'UserController@view');
Route::get('admin/user/edit/{id}', 'UserController@edit');
Route::get('admin/user/delete/{id}', 'UserController@delete');
Route::get('admin/user/expire/{id}', 'UserController@expire');


//options
Route::get('admin/option', 'PackageOption@show'); //list
Route::get('admin/option/add', 'PackageOption@create'); //add paksage option
Route::post('admin/option/store', 'PackageOption@store');
Route::get('admin/option/edit/{id}', 'PackageOption@edit');
Route::post('admin/option/update', 'PackageOption@update');

Route::get('admin/option/remove/{id}', 'PackageOption@destroy');







//architects
 Route::get('admin/architects' , 'ArchitectsController@index');


  //clients
 Route::get('admin/clients' , 'ClientsController@index');


// Route::get('admin/packagesoption/create', 'PackageOption@create');
// admin/packagesoption/create
//Route::post('admin/user/update', 'UserController@update');
Route::post('admin/user/update', array('as' => 'admin/user/update', 'uses' => 'UserController@update'));

Route::get('admin/payment_history', 'PaymentHistoryController@index');
Route::get('admin/payment/search', 'PaymentHistoryController@search');

//test


//awais
// for the pratice on the stripe account


/* ************************************************************************************
 *
 *
 *
 *                                          New Work
 *
 *
 *
 ***************************************************************************************/

Route::group(['middleware' => 'auth'], function () {
    Route::get('{role}/profile/edit', 'UserController@edit')->name('profile.edit');
    Route::post('{role}/profile/update', 'UserController@update')->name('profile.update');


//    ADMIN
    Route::group(['prefix' => 'admin'], function() {

//        PASSWORD
        Route::post('password/update', 'UserController@passwordUpdate')->name('admin.password.update');

//        PROFILE
        Route::get('account', 'UserController@password')->name('admin.account');
//        Route::get('profile/edit', 'UserController@edit')->name('admin.profile.edit');
//        Route::post('profile/update', 'UserController@update')->name('admin.profile.update');

//        QUOTATIONS
        Route::get('quotations/index', 'OrderController@index')->name('admin.orders.index');
        Route::get('quotations/show/{id}', 'OrderController@show')->name('admin.orders.show');
        Route::get('quotations/invoice/show/{id}', 'OrderController@orderInvoiceShow')->name('admin.orders.invoice.show');
        Route::get('quotations/approve/{id}', 'OrderController@approve')->name('admin.orders.approve');
        Route::get('quotations/decline/{id}', 'OrderController@decline')->name('admin.orders.decline');

        Route::get('quotations/edit/{id}', 'OrderController@edit')->name('admin.orders.edit');

//        ORDERS
        Route::get('/orders/create', 'OrderController@create')->name('admin.orders.create');
        Route::get('/products/filter/by/category/{id}', 'ProductController@filterByCategory')->name('admin.product.filter.by.category');
        Route::get('/products/orders/store', 'OrderController@store')->name('admin.products.order.store');

//        RANGE
        Route::get('/product/range', 'ProductRangeController@index')->name('admin.product.ranges.index');
        Route::get('/product/range/show/{id}', 'ProductRangeController@show')->name('admin.product.range.show');

//        SEO
        Route::get('/product/images/seo', 'MediaController@seo')->name('admin.product.images.seo');
        Route::post('/product/images/seo', 'MediaController@seoPost')->name('admin.product.images.seo');

//        LEADS
        Route::get('/leads/', 'LeadController@index')->name('admin.leads.index');
        Route::get('/leads/create', 'LeadController@create')->name('admin.leads.create');
        Route::post('/leads/store', 'LeadController@store')->name('admin.leads.store');
        Route::get('/leads/edit/{id}', 'LeadController@edit')->name('admin.leads.edit');
        Route::post('/leads/update/{id}', 'LeadController@update')->name('admin.leads.update');
        Route::get('/leads/delete/{id}', 'LeadController@destroy')->name('admin.leads.delete');

//        ACCOUNTS
        Route::get('/account/', 'AccountController@index')->name('admin.accounts.index');
        Route::get('/account/create', 'AccountController@create')->name('admin.accounts.create');
        Route::post('/account/store', 'AccountController@store')->name('admin.accounts.store');
        Route::get('/account/edit/{id}', 'AccountController@edit')->name('admin.accounts.edit');
        Route::post('/account/update/{id}', 'AccountController@update')->name('admin.accounts.update');
        Route::get('/account/delete/{id}', 'AccountController@destroy')->name('admin.accounts.delete');

//        CART
        Route::get('/quotations', 'CartController@index')->name('admin.cart.index');

//        EVENT
        Route::get('events', 'EventController@index')->name('admin.events.index');
        Route::post('events/store', 'EventController@store')->name('admin.events.store');

//        CHAT
        Route::get('/chat', 'ChatController@index')->name('admin.chat.index');
        Route::get('/messages', 'ChatController@fetchMessages');
        Route::post('/messages', 'ChatController@sendMessage');

//        EMAIL
        Route::get('/email', 'EmailController@create')->name('admin.email.create');
        Route::post('/email/store', 'EmailController@store')->name('admin.email.store');
        Route::get('/email/inbox', 'EmailController@index')->name('admin.email.inbox');
        Route::get('/email/sent', 'EmailController@sent')->name('admin.email.sent');
        Route::get('/email/draft', 'EmailController@draft')->name('admin.email.draft');
        Route::get('/email/trash', 'EmailController@trash')->name('admin.email.trash');
        Route::get('/email/read/{id}', 'EmailController@show')->name('admin.email.read');
        Route::post('/email/draft/create', 'EmailController@updateDraft')->name('email.draft.create');
        Route::post('/email/delete/create', 'EmailController@deleteCreate')->name('email.delete.create');

        Route::post('/email/draft', 'EmailController@singleDraft')->name('email.draft');
        Route::post('/email/delete', 'EmailController@singleDelete')->name('email.delete');
        Route::post('/email/reply', 'EmailController@reply')->name('admin.email.reply');
        Route::post('/email/reply/send', 'EmailController@replySend')->name('admin.email.reply.store');

//        CONTACT
        Route::get('/contact', 'ContactController@index')->name('admin.contact.index');
        Route::get('/contact/create', 'ContactController@create')->name('admin.contact.create');
        Route::post('/contact/store', 'ContactController@store')->name('admin.contact.store');

//        SETTINGS
        Route::get('/settings', 'SiteSettingController@index')->name('admin.site.setting.index');
        Route::post('/settings/update', 'SiteSettingController@update')->name('admin.site.setting.update');

//        CATALOGUE
        Route::get('/catalogue', 'CatalogueController@index')->name('admin.catalogue.index');
        Route::post('/catalogue/store', 'CatalogueController@catalogueStore')->name('admin.catalogue.catalogueStore');

//        TEMPLATE
        Route::get('/template/create', 'CatalogueController@create')->name('admin.template.create');
        Route::post('/template/store', 'CatalogueController@store')->name('admin.template.store');

//        MOBILE UPLOADS GALLERY
        Route::get('/mobile/upload/gallery', 'MediaController@allMobileUploads')->name('admin.mobile.uploads.media');

//        PROMOTION EMAILS
        Route::get('/promotion/email', 'PromotionEmailController@index')->name('admin.promotion.email.index');
        Route::get('/promotion/email/create', 'PromotionEmailController@create')->name('admin.promotion.email.create');
        Route::post('/promotion/email/store', 'PromotionEmailController@store')->name('admin.promotion.email.store');

        Route::get('/promotion/email/send/{id}', 'PromotionEmailController@sendEmail')->name('admin.promotion.email.send');
        Route::get('/promotion/email/show/{id}', 'PromotionEmailController@show')->name('admin.promotion.email.show');
        Route::get('/promotion/email/template/{id}', 'PromotionEmailController@emailTemplate');

        Route::post('/promotion/email/update/{id}', 'PromotionEmailController@update')->name('admin.promotion.email.update');
        Route::get('/promotion/email/delete/{id}', 'PromotionEmailController@destroy')->name('admin.promotion.email.delete');
        Route::get('/promotion/email/edit/{id}', 'PromotionEmailController@edit')->name('admin.promotion.email.edit');

//        EMAIL TEMPLATEs
        Route::post('/email/template/store', 'EmailTemplateController@store')->name('admin.email.template.store');

//        SUBSCRIBERS
        Route::get('/subscriber', 'SubscriberController@index')->name('admin.subscriber.index');
        Route::post('/subscriber/store', 'SubscriberController@store')->name('admin.subscriber.store');
        Route::get('/subscriber/{id}/delete', 'SubscriberController@destroy')->name('admin.subscriber.delete');

//        BULK EMAILS
        Route::get('send/bulk/email/{id}', 'PromotionEmailController@sendBulk')->name('admin.send.bulk.email');

//        LOGIN CODE
        Route::get('/auth/codes', 'AuthCodeController@index')->name('admin.auth.codes.index');
        Route::get('/auth/codes/admin', 'AuthCodeController@get_auth_code_webrtc');
        Route::get('/auth/codes/generate', 'AuthCodeController@generateCode');  //removeGeneratedCode

        Route::get('/auth/codes/removecode/{id}', 'AuthCodeController@removeGeneratedCode');

        Route::get('/auth/codes/create', 'AuthCodeController@create')->name('admin.auth.codes.create');
        Route::get('/auth/codes/edit/destroy/{id}', 'AuthCodeController@destroy')->name('admin.auth.codes.delete');
        Route::get('appointments', 'AppointmentController@index')->name('admin.demo.appointments');
        Route::get('draggable/content', 'OrderController@draggable')->name('admin.draggable.index');


    });


//    LOCATION
    Route::get('/locations/index', 'LocationController@index')->name('admin.location.index');
    Route::get('/locations/create', 'LocationController@create')->name('admin.location.create');
    Route::post('/locations/store', 'LocationController@store')->name('admin.location.store');
    Route::get('/locations/edit/{slug}', 'LocationController@edit')->name('admin.location.edit');
    Route::post('/locations/update/{id}', 'LocationController@update')->name('admin.location.update');
    Route::get('/locations/delete/{slug}', 'LocationController@destroy')->name('admin.location.delete');

//    MANUFACTURERS
    Route::group(['prefix' => 'manufacturer', 'namespace' => 'Manufacturer'], function() {

//        PASSWORD
        Route::post('password/update', 'UserController@passwordUpdate')->name('manufacturer.password.update');

//        PROFILE
        Route::get('account', 'UserController@password')->name('manufacturer.account');
        Route::get('profile/edit', 'UserController@edit')->name('manufacturer.profile.edit');
        Route::post('profile/update', 'UserController@update')->name('manufacturer.profile.update');

//        CATEGORIES
        Route::get('categories', 'CategoryController@index')->name('manufacturer.categories.index');
        Route::get('categories/create', 'CategoryController@create')->name('manufacturer.categories.create');
        Route::post('categories/store', 'CategoryController@store')->name('manufacturer.categories.store');
        Route::get('categories/edit/{id}', 'CategoryController@edit')->name('manufacturer.categories.edit');
        Route::post('categories/update/{id}', 'CategoryController@update')->name('manufacturer.categories.update');
        Route::get('categories/delete/{id}', 'CategoryController@destroy')->name('manufacturer.categories.delete');
        Route::get('categories/show/{id}', 'CategoryController@show')->name('manufacturer.categories.show');

//        PRODUCTS
        Route::get('/products', 'ProductController@index')->name('manufacturer.products.index');
        Route::get('/products/create', 'ProductController@create')->name('manufacturer.products.create');
        Route::post('/products/store', 'ProductController@store')->name('manufacturer.products.store');
        Route::get('/products/show/{id}', 'ProductController@show')->name('manufacturer.products.show');
        Route::get('/products/edit/{id}', 'ProductController@edit')->name('manufacturer.products.edit');
        Route::post('/products/update/{id}', 'ProductController@update')->name('manufacturer.products.update');
        Route::get('/products/delete/{id}', 'ProductController@destroy')->name('manufacturer.products.delete');
        Route::post('/products/image/upload/{id}', 'MediaController@productImageUpload')->name('manufacturer.products.image.upload');
        Route::get('/products/image/remove/{id}', 'MediaController@productImageRemove')->name('manufacturer.products.image.remove');
        Route::get('/products/image/delete/{id}', 'MediaController@productColorImageRemove')->name('manufacturer.products.color.image.delete');

//        RANGE
        Route::get('/product/range', 'ProductRangeController@index')->name('manufacturer.product.range.index');
        Route::get('/product/range/create', 'ProductRangeController@create')->name('manufacturer.product.range.create');
        Route::post('/product/range/store', 'ProductRangeController@store')->name('manufacturer.product.range.store');

//        ORDERS
        Route::get('/orders/index', 'OrderController@index')->name('manufacturer.orders.index');
        Route::get('/orders/show/{id}', 'OrderController@show')->name('manufacturer.orders.show');
        Route::get('/orders/invoice/show/{id}', 'OrderController@orderInvoiceShow')->name('manufacturer.orders.invoice.show');

//        EVENTS
        Route::get('events', 'EventController@index')->name('manufacturer.events.index');

    });


//    ARCHITECT
    Route::group(['prefix' => 'architect', 'namespace' => 'Architect'], function() {

//        PASSWORD
        Route::post('password/update', 'UserController@passwordUpdate')->name('architect.password.update');

//        PROFILE
        Route::get('account', 'UserController@password')->name('architect.account');
        Route::get('profile/edit', 'UserController@edit')->name('architect.profile.edit');
        Route::post('profile/update', 'UserController@update')->name('architect.profile.update');

//        PRODUCTS
        Route::get('/products', 'ProductController@index')->name('architect.products.index');
        Route::get('/products/create', 'ProductController@create')->name('architect.products.create');
        Route::post('/products/store', 'ProductController@store')->name('architect.products.store');
        Route::get('/products/show/{id}', 'ProductController@show')->name('architect.products.show');
        Route::get('/products/edit/{id}', 'ProductController@edit')->name('architect.products.edit');
        Route::post('/products/update/{id}', 'ProductController@update')->name('architect.products.update');
        Route::get('/products/delete/{id}', 'ProductController@destroy')->name('architect.products.delete');
        Route::post('/products/image/upload/{id}', 'MediaController@productImageUpload')->name('architect.products.image.upload');
        Route::get('/products/image/remove/{id}', 'MediaController@productImageRemove')->name('architect.products.image.remove');
        Route::get('/products/image/delete/{id}', 'MediaController@productColorImageRemove')->name('architect.products.color.image.delete');

//        FILTER
        Route::get('/products/filter/by/category/{id}', 'ProductController@filterByCategory')->name('product.filter.by.category');

//        ORDERS
        Route::get('orders/index', 'OrderController@index')->name('architect.orders.index');
        Route::get('orders/show/{id}', 'OrderController@show')->name('architect.orders.show');
        Route::get('orders/invoice/show/{id}', 'OrderController@orderInvoiceShow')->name('architect.orders.invoice.show');
        Route::get('products/order/store', 'OrderController@store')->name('architect.products.order.store');

//        CART
        Route::get('/quotations', 'CartController@index')->name('architect.cart.index');

//        EVENTS
        Route::get('events', 'EventController@index')->name('architect.events.index');

    });

    Route::group(['prefix' => 'employee', 'namespace' => 'Employee'], function() {

//        ARCHITECT
        Route::get('/architects', 'ArchitectController@index')->name('employee.architects.index');
        Route::get('/architects/create', 'ArchitectController@create')->name('employee.architects.create');
        Route::post('/architects/store', 'ArchitectController@store')->name('employee.architects.store');
        Route::get('/architects/edit/{id}', 'ArchitectController@edit')->name('employee.architects.edit');
        Route::post('/architects/update/{id}', 'ArchitectController@update')->name('employee.architects.update');
        Route::get('/architects/delete/{id}', 'ArchitectController@destroy')->name('employee.architects.delete');
        Route::get('/architects/show/{id}', 'ArchitectController@show')->name('employee.architects.show');

//        CLIENTS
        Route::get('/clients', 'ClientController@index')->name('employee.clients.index');
        Route::get('/clients/create', 'ClientController@create')->name('employee.clients.create');
        Route::post('/clients/store', 'ClientController@store')->name('employee.clients.store');
        Route::get('/clients/edit/{id}', 'ClientController@edit')->name('employee.clients.edit');
        Route::post('/clients/update/{id}', 'ClientController@update')->name('employee.clients.update');
        Route::get('/clients/delete/{id}', 'ClientController@update')->name('employee.clients.delete');
        Route::get('/clients/show/{id}', 'ClientController@show')->name('employee.clients.show');

//        PRODUCTS
        Route::get('/products', 'ProductController@index')->name('employee.products.index');
        Route::get('/products/create', 'ProductController@create')->name('employee.products.create');
        Route::post('/products/store', 'ProductController@store')->name('employee.products.store');
        Route::get('/products/show/{id}', 'ProductController@show')->name('employee.products.show');
        Route::get('/products/edit/{id}', 'ProductController@edit')->name('employee.products.edit');
        Route::post('/products/update/{id}', 'ProductController@update')->name('employee.products.update');
        Route::get('/products/delete/{id}', 'ProductController@destroy')->name('employee.products.delete');
        Route::post('/products/image/upload/{id}', 'MediaController@productImageUpload')->name('employee.products.image.upload');
        Route::get('/products/image/remove/{id}', 'MediaController@productImageRemove')->name('employee.products.image.remove');
        Route::get('/products/image/delete/{id}', 'MediaController@productColorImageRemove')->name('employee.products.color.image.delete');

//        ORDERS
        Route::get('orders/index', 'OrderController@index')->name('employee.orders.index');
        Route::get('orders/show/{id}', 'OrderController@show')->name('employee.orders.show');
        Route::get('orders/invoice/show/{id}', 'OrderController@orderInvoiceShow')->name('employee.orders.invoice.show');
        Route::get('products/order/store', 'OrderController@store')->name('employee.products.order.store');

//        CART
        Route::get('/quotations', 'CartController@index')->name('employee.cart.index');

//        EVENTS
        Route::get('events', 'EventController@index')->name('employee.events.index');

    });

//DASHBOARD
    Route::get('{role}/dashboard', 'DashboardController@index')->name('dashboard');

//MANUFACTURES
    Route::get('admin/manufacturers', 'ManufacturerController@index')->name('admin.manufacturers.index');
    Route::get('admin/manufacturers/create', 'ManufacturerController@create')->name('admin.manufacturers.create');
    Route::post('admin/manufacturers/store', 'ManufacturerController@store')->name('admin.manufacturers.store');
    Route::get('admin/manufacturers/edit/{id}', 'ManufacturerController@edit')->name('admin.manufacturers.edit');
    Route::get('admin/manufacturers/delete/{id}', 'ManufacturerController@destroy')->name('admin.manufacturers.delete');
    Route::post('admin/manufacturers/update/{id}', 'ManufacturerController@update')->name('admin.manufacturers.update');
    Route::get('{role}/manufacturers/show/{id}', 'ManufacturerController@show')->name('admin.manufacturers.show');

    Route::get('admin/manufacturers/image_gallry', 'ManufacturerController@image_gallry')->name('admin.manufacturers.image_gallry');
    Route::get('admin/manufacturers/price_list', 'ManufacturerController@price_list')->name('admin.manufacturers.price_list');
    Route::get('admin/manufacturers/new_price_list', 'ManufacturerController@new_price_list')->name('admin.manufacturers.new_price_list');

    Route::get('admin/manufacturers/inventory', 'ManufacturerController@inventory')->name('admin.manufacturers.inventory');

    Route::post('admin/manufacturers/inventory', 'InventoryController@index')->name('inventorySearch');



    Route::get('admin/dealers', 'DealerController@index')->name('admin.dealers.index');
    Route::get('admin/dealers/create', 'DealerController@create')->name('admin.dealers.create');
    Route::post('admin/dealers/store', 'DealerController@store')->name('admin.dealers.store');
    Route::get('admin/dealers/edit/{id}', 'DealerController@edit')->name('admin.dealers.edit');
    Route::get('admin/dealers/show/{id}', 'DealerController@show')->name('admin.dealers.show');
    Route::post('admin/dealers/edit/{id}', 'DealerController@update')->name('admin.dealers.update');
    Route::get('admin/dealers/delete/{id}', 'DealerController@destroy')->name('admin.dealers.delete');


    Route::get('admin/color/index', 'ColorCodeController@index')->name('admin.colors.index');
    Route::post('admin/color/store', 'ColorCodeController@store')->name('admin.colors.store');
    Route::get('admin/color/delete/{id}', 'ColorCodeController@destroy')->name('admin.colors.delete');



//ARCHITECTS
    Route::get('admin/architects', 'ArchitectController@index')->name('admin.architects.index');
    Route::get('admin/architects/create', 'ArchitectController@create')->name('admin.architects.create');
    Route::post('admin/architects/store', 'ArchitectController@store')->name('admin.architects.store');
    Route::get('admin/architects/edit/{id}', 'ArchitectController@edit')->name('admin.architects.edit');
    Route::post('admin/architects/update/{id}', 'ArchitectController@update')->name('admin.architects.update');
    Route::get('admin/architects/delete/{id}', 'ArchitectController@destroy')->name('admin.architects.delete');
    Route::get('{role}/architects/show/{id}', 'ArchitectController@show')->name('admin.architects.show');

//CLIENTS
    Route::get('admin/clients', 'ClientController@index')->name('admin.clients.index');
    Route::get('admin/clients/create', 'ClientController@create')->name('admin.clients.create');
    Route::post('admin/clients/store', 'ClientController@store')->name('admin.clients.store');
    Route::get('admin/clients/edit/{id}', 'ClientController@edit')->name('admin.clients.edit');
    Route::post('admin/clients/update/{id}', 'ClientController@update')->name('admin.clients.update');
    Route::get('admin/clients/delete/{id}', 'ClientController@destroy')->name('admin.clients.delete');
    Route::get('{role}/clients/show/{id}', 'ClientController@show')->name('admin.clients.show');

//EMPLOYEES
    Route::get('admin/employees', 'EmployeeController@index')->name('admin.employees.index');
    Route::get('admin/employees/create', 'EmployeeController@create')->name('admin.employees.create');
    Route::post('admin/employees/store', 'EmployeeController@store')->name('admin.employees.store');
    Route::get('admin/employees/edit/{id}', 'EmployeeController@edit')->name('admin.employees.edit');
    Route::post('admin/employees/update/{id}', 'EmployeeController@update')->name('admin.employees.update');
    Route::get('admin/employees/delete/{id}', 'EmployeeController@destroy')->name('admin.employees.delete');
    Route::get('{role}/employees/show/{id}', 'EmployeeController@show')->name('admin.employees.show');

//    EMPLOYEE SALARY
    Route::post('admin/employees/salary/store', 'EmployeeSalaryController@store')->name('admin.employees.salary.store');

//CATEGORIES
    Route::get('admin/categories', 'CategoryController@index')->name('admin.categories.index');
    Route::get('admin/categories/create', 'CategoryController@create')->name('admin.categories.create');
    Route::post('admin/categories/store', 'CategoryController@store')->name('admin.categories.store');
    Route::get('admin/categories/edit/{id}', 'CategoryController@edit')->name('admin.categories.edit');
    Route::post('admin/categories/update/{id}', 'CategoryController@update')->name('admin.categories.update');
    Route::get('admin/categories/delete/{id}', 'CategoryController@destroy')->name('admin.categories.delete');
    Route::get('{role}/categories/show/{id}', 'CategoryController@show')->name('admin.categories.show');


//COMPANIES
    Route::get('admin/companies', 'CompanyController@index')->name('admin.companies.index');
    Route::get('admin/companies/create', 'CompanyController@create')->name('admin.companies.create');
    Route::post('admin/companies/store', 'CompanyController@store')->name('admin.companies.store');
    Route::get('admin/companies/edit/{id}', 'CompanyController@edit')->name('admin.companies.edit');
    Route::post('admin/companies/update/{id}', 'CompanyController@update')->name('admin.companies.update');
    Route::get('admin/companies/delete/{id}', 'CompanyController@destroy')->name('admin.companies.delete');
    Route::get('{role}/companies/show/{id}', 'CompanyController@show')->name('admin.companies.show');
    Route::post('admin/companies/setdefault', 'CompanyController@setdefault')->name('admin.companies.setdefault');


//PRODUCTS
    Route::get('admin/products', 'ProductController@index')->name('admin.products.index');
    Route::get('admin/products/create', 'ProductController@create')->name('admin.products.create');
    Route::post('admin/products/store', 'ProductController@store')->name('admin.products.store');
    Route::get('admin/products/show/{id}', 'ProductController@show')->name('admin.products.show');
    Route::get('admin/products/edit/{id}', 'ProductController@edit')->name('admin.products.edit');
    Route::post('admin/products/update/{id}', 'ProductController@update')->name('admin.products.update');
    Route::get('admin/products/delete/{id}', 'ProductController@destroy')->name('admin.products.delete');
    Route::post('admin/products/image/upload/{id}', 'MediaController@productImageUpload')->name('admin.products.image.upload');
    Route::get('admin/products/image/remove/{id}', 'MediaController@productImageRemove')->name('admin.products.image.remove');
    Route::get('admin/products/image/delete/{id}', 'MediaController@productColorImageRemove')->name('admin.products.color.image.delete');

//QUOTATIONS
    Route::get('{role}/quotations', 'QuotationController@index')->name('admin.quotations.index');
    Route::get('admin/quotations/show', 'QuotationController@show')->name('admin.quotations.show');
    Route::get('admin/quotations/delete/{id}', 'QuotationController@destroy')->name('admin.quotations.delete');
    Route::post('orders/quotations/export/list', 'OrderController@pdfExportQuotation')->name('orders.quotations.export.list');



    Route::get('orders/export/view', 'OrderController@exportExcelCustomDesign')->name('order.export.view.custom');
    Route::post('export/excel/sheet', 'OrderController@pdfExportQuotation')->name('export.excel.sheet');


Route::post('product/image/upload', 'MediaController@imageUpload')->name('product.image.update');
Route::post('product/image/delete', 'MediaController@imageDelete')->name('product.image.delete');
Route::post('product/color/image/upload', 'MediaController@colorImageUpload')->name('product.color.image.update');
Route::post('product/color/image/delete', 'MediaController@imageDelete')->name('product.color.image.delete');


Route::post('image/download', 'MediaController@download')->name('image.download');

//GALLERY
Route::post('gallery/products/images/upload', 'MediaController@store')->name('gallery.products.images.upload');
Route::get('gallery/products/images/delete/{id}', 'MediaController@destroy')->name('gallery.products.images.delete');


Route::post('add/to/session', 'MediaController@addToSessions')->name('add.to.session');



//CART
Route::get('get-cart-total-item', 'CartController@getCartTotalItem')->name('items.in.cart');
Route::post('add-to-cart', 'CartController@addToCart')->name('add.to.cart');
Route::post('update-item-cart-size', 'CartController@updateCartItemSize')->name('cart.item.update.size');
Route::post('increment-to-cart', 'CartController@incrementToCartItem')->name('cart.increment.quantity');
Route::post('decrement-to-cart', 'CartController@decrementToCartItem')->name('cart.decrement.quantity');
Route::post('delete-item-cart', 'CartController@deleteCartItem')->name('cart.item.delete');


//COMMENT
Route::post('/add/comment', 'CommentController@store');


//VIDEO
Route::group(['middleware' => 'auth'], function(){
    Route::get('video_chat', 'VideoChatController@index');
    Route::post('auth/video_chat', 'VideoChatController@auth');
    Route::get('/video_calling', 'ChatController@video_calling')->name('video_calling');
    Route::get('/live-demo-{code}', 'VideoChatController@index')->name('video.chat');

});



Route::get('/chat', 'ChatController@index');
Route::get('/messages', 'ChatController@fetchMessages');
Route::post('/messages', 'ChatController@sendMessage');





//Delivery
Route::get('/admin/delivery', 'DeliveryController@index')->name('delivery');

//Company Performance
Route::get('/admin/company_performance', 'CompanyController@company_performance')->name('company_performance');

//salaries
Route::get('/admin/salaries', 'SalaryController@index')->name('salaries');

//recruitment
Route::get('/admin/recruitment/create', 'RecuritmentController@create')->name('admin.recruitment.create');
Route::post('/admin/recruitment/store', 'RecuritmentController@show')->name('admin.recruitment.show');

//Advertising
Route::get('/admin/advertising/create', 'AdvertisingController@create')->name('admin.advertising.create');
Route::post('/admin/advertising/store', 'AdvertisingController@show')->name('admin.advertising.show');

//Marketing / Catalogue
Route::get('/admin/catalogues', 'CatalogController@index')->name('catalogues');
Route::post('/admin/catalogue/page/store', 'CataloguePageController@store')->name('admin.catalogue.page.store');
Route::post('/admin/catalogue/preview', 'CataloguePageController@show')->name('admin.catalogue.preview');
Route::get('/admin/catalogue/saved', 'CataloguePageController@show_saved_list')->name('catalogue-saved-list');
Route::post('/admin/catalogue/saved', 'CataloguePageController@send_email_attachment')->name('sendemail');



//Marketing / Mailshot
Route::get('/admin/mailshot', 'MailshotController@index')->name('mailshot');

// search_engine
Route::get('/admin/search_engine', 'AdvertisementController@index')->name('search_engine');

Route::get('/admin/advertisement/create', 'AdvertisementController@create')->name('admin.advertisement.create');
Route::post('/admin/advertisement/store', 'AdvertisementController@store')->name('admin.advertisement.store');
Route::get('/admin/advertisement/edit/{id}', 'AdvertisementController@edit')->name('admin.advertisement.edit');
Route::post('/admin/advertisement/update/{id}', 'AdvertisementController@update')->name('admin.advertisement.update');
Route::get('/admin/advertisement/delete/{id}', 'AdvertisementController@destroy')->name('admin.advertisement.delete');
Route::get('/admin/advertisement/show/{id}', 'AdvertisementController@show')->name('admin.advertisement.show');

// social mediea
Route::get('/admin/social_media', 'SocialMediaAdvertisementController@index')->name('social_media.index');
Route::get('/admin/social_media/create', 'SocialMediaAdvertisementController@create')->name('social_media');
Route::post('/admin/social_media/store', 'SocialMediaAdvertisementController@store')->name('social.media.store');
Route::get('/admin/social_media/show/{id}', 'SocialMediaAdvertisementController@show')->name('social.media.show');
Route::get('/admin/social_media/edit/{id}', 'SocialMediaAdvertisementController@edit')->name('social.media.edit');
Route::post('/admin/social_media/{id}', 'SocialMediaAdvertisementController@update')->name('social.media.update');
Route::get('/admin/social_media/delete/{id}', 'SocialMediaAdvertisementController@destroy')->name('social.media.delete');

//Social Media
Route::get('/admin/social_media/posts', 'SocialMediaAdvertisementController@post')->name('social.media.post');
Route::get('/admin/social_media/posts/likes/{id}', 'SocialMediaAdvertisementController@like')->name('social.media.post.like');
Route::post('/admin/social_media/comment/post', 'SocialMediaAdvertisementCommentController@store')->name('social.media.post.comment');


Route::post('/admin/quotation/store', 'QuotationController@store')->name('quotation.store');
Route::post('/admin/quotation/update/{id}', 'QuotationController@update')->name('quotation.update');
Route::get('/admin/quotation/get_ranges', 'QuotationController@get_ranges');

//design_build
Route::get('/admin/design_build/create', 'DesignController@create')->name('admin.design.build.create');
Route::post('/admin/design_build/show', 'DesignController@show')->name('admin.design.build.show');

Route::get('/admin/quotation/show/{id}', 'QuotationController@quotationShow')->name('quotation_show');
Route::get('/admin/order/remove/{id}', 'OrderController@destroy')->name('order.remove');

Route::post('quotation/search', 'OrderController@search')->name('quotation.search');

Route::post('/product/range/images/store', 'MediaController@productRangeImages')->name('product.range.images.upload');
Route::post('getImagesBymanufacturer', 'MediaController@getImagesBymanufacturer');
Route::post('getImagesByRange', 'MediaController@getImagesByRange');
Route::get('removeImageFromSession', 'MediaController@removeImageFromSession')->name('delete.uploaded.images');

Route::post('updateOrderPriceListColumn', 'OrderController@updateOrderPriceListColumn')->name('update.order.price.list.column');
Route::get('admin/screen/share', 'MediaController@screenShare')->name('admin.screen.share');

Route::post('upload/excel/price/range', 'ProductRangeController@import')->name('admin.price.range.import');




//Get Ranges By Manufacturer
Route::post('/getRangesByManufacturer', 'ProductRangeController@getRangesByManufacturer')->name('ranges.by.manufacturer');

Route::post('getCompanyById', 'CompanyController@getCompanyById');

});
