@extends('admin.layouts.app')
@section('content')
    @include('admin.layouts.topbar')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    @if(session()->has('message'))
                        <div class="alert alert-success mt-5" id="myElem" >
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-12 mt-5">
                            <div class="card">

                                <div class="card-body">
                                    <div class="modal-body">
                                        <div class="col-md-12">
                                            <form action="{{route('admin.promotion.email.update', ['id' => $email->id])}}" method="post">
                                                @csrf
                                            <div class="row">
                                                <div class="form-group w-100">
                                                    <label for="subject">Subject</label>
                                                    <input class="form-group form-control w-100" type="text" name="subject" id="subject" value="{{$email->subject}}">
                                                </div>
                                                <div class="form-group w-100">
                                                    <label for="Description">Description<span class="text-danger">*</span></label>
                                                    <textarea id="Description" class="form-control rounded-0 summernote" name="description" rows="5">{!! $email->body !!}</textarea>
                                                    @if ($errors->has('description'))
                                                        <span class="error">{{ $errors->first('description') }}</span>
                                                    @endif
                                                </div>

                                                <div class="form-group w-100">
                                                    <input class="btn btn-success float-right" type="submit" value="Update Email" id="subject">
                                                </div>
                                            </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
