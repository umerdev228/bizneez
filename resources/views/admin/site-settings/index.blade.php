@extends('admin.layouts.app')
@section('content')
{{--    @include('admin.layouts.topbar')--}}
    <!-- Google Chart JS -->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <!-- Start Page content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">

                    @if(session()->has('message'))
                        <div class="alert alert-success col-md-9 mt-5">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    @if(session()->has('error'))
                            <div class="alert alert-danger col-md-9 mt-5" >
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                {{ session()->get('error') }}
                            </div>
                    @endif
                        <div class="row mt-5">
                            <div class="col-9">
                                <div class="card-box">
                                    <form action="{{ route('admin.site.setting.update') }}" method="post" enctype="multipart/form-data">
                                        @csrf
                                        <div class="form-row">
                                            <div class="form-group col-md-12">
                                                <label for="header_logo">Header Logo<span class="text-danger">*</span></label>
                                                <input type="file" name="header_logo" class="form-control">
                                                @if ($errors->has('header_logo'))
                                                    <span class="error">{{ $errors->first('header_logo') }}</span>
                                                @endif
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label for="side_bar_color">Sidebar Color<span class="text-danger">*</span></label>
                                                <input type="color" name="side_bar_color"  value="{{$side_bar_color}}" autocomplete="off" class="form-control" required/>
                                                @if ($errors->has('side_bar_color'))
                                                    <span class="error">{{ $errors->first('side_bar_color') }}</span>
                                                @endif
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label for="header_color">Header Color<span class="text-danger">*</span></label>
                                                <input type="color" name="header_color"  value="{{$header_color}}" autocomplete="off" class="form-control" required/>
                                                @if ($errors->has('header_color'))
                                                    <span class="error">{{ $errors->first('header_color') }}</span>
                                                @endif
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label for="text_color">Text Color<span class="text-danger">*</span></label>
                                                <input type="color" name="text_color" id="text_color" value="{{$text_color}}" autocomplete="off" class="form-control" required/>
                                                @if ($errors->has('text_color'))
                                                    <span class="error">{{ $errors->first('text_color') }}</span>
                                                @endif
                                            </div>

                                            <div class="form-group col-md-12">
                                                <label for="facebook_link">facebook Link<span class="text-danger">*</span></label>
                                                <input type="text" name="facebook_link" id="facebook_link" value="{{$facebook_link}}" autocomplete="off" class="form-control" required/>
                                                @if ($errors->has('text_color'))
                                                    <span class="error">{{ $errors->first('text_color') }}</span>
                                                @endif
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label for="facebook_link">instagram Link<span class="text-danger">*</span></label>
                                                <input type="text" name="instagram_link" id="facebook_link" value="{{$instagram_link}}" autocomplete="off" class="form-control" required/>
                                                @if ($errors->has('text_color'))
                                                    <span class="error">{{ $errors->first('text_color') }}</span>
                                                @endif
                                            </div>

                                            <div class="form-group col-md-12">
                                                <label for="facebook_link">twitter Link<span class="text-danger">*</span></label>
                                                <input type="text" name="twitter_link" id="facebook_link" value="{{$twitter_link}}" autocomplete="off" class="form-control" required/>
                                                @if ($errors->has('text_color'))
                                                    <span class="error">{{ $errors->first('text_color') }}</span>
                                                @endif
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label for="facebook_link">linkedIn Link<span class="text-danger">*</span></label>
                                                <input type="text" name="linkedIn_link" id="facebook_link" value="{{$linkedIn_link}}" autocomplete="off" class="form-control" required/>
                                                @if ($errors->has('text_color'))
                                                    <span class="error">{{ $errors->first('text_color') }}</span>
                                                @endif
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label for="cc_email">CC Email<span class="text-danger">*</span></label>
                                                <input type="email" name="cc_email" id="cc_email" value="{{$cc_email}}" autocomplete="off" class="form-control" required/>
                                                @if ($errors->has('cc_email'))
                                                    <span class="error">{{ $errors->first('cc_email') }}</span>
                                                @endif
                                            </div>

                                            <div class="form-group col-md-12">
                                                <button class="btn btn-success" type="submit">Submit</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
{{--    @include('admin.layouts.footer')--}}
@endsection
