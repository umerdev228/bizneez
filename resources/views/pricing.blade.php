@extends('layout.app')
@section('content')
<main>
	<!-- Testimonial Start -->
	<div class="testimonial-area testimonial-padding pb-0 gray-bg">
		
		<div class="container-fluid">
			<!-- Testimonial contents -->
			<div class="row d-flex justify-content-center sect-bg pt-40 pb-80">
				<div class="section-tittle text-center">
					<h2>Affordable and Elegant Omnichannel Helpdesk</h2>
					<h3>Email, chat, phone, social — Be there for your customers, wherever they are!</h3>
					<div class="mt-50"><a href="{{ url('signup')}}" class="genric-btn info text-uppercase">SIGNUP</a></div>
				</div>
				
			</div>
		</div>
	</div>
	<!-- Testimonial End -->
	
	
	
	<!-- Pricint Table -->
	<section class="pricing py-5">
		<div class="container">
			
			<div class="col-lg-12">
				<div class="section-tittle text-center">
					<h2 class="text-white">Pricing Table</h2>
				</div>
			</div>
			<div class="row d-flex justify-content-center">
				
				{{----
				 	<div class="col-lg-4">
					<div class="card mb-5 mb-lg-0">
						<div class="card-body">
							<h5 class="card-title text-muted text-uppercase text-center">MONTLY</h5>
							<h6 class="card-price text-center">$49<span class="period">/month</span></h6>
							<hr>
							<ul class="fa-ul">
								<li><span class="fa-li"><i class="fas fa-check"></i></span><strong>5 Users</strong></li>
								<li><span class="fa-li"><i class="fas fa-check"></i></span>50GB Storage</li>
								<li><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited Public Projects</li>
								<li><span class="fa-li"><i class="fas fa-check"></i></span>Community Access</li>
								<li><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited Private Projects</li>
								<li><span class="fa-li"><i class="fas fa-check"></i></span>Dedicated Phone Support</li>
								<li><span class="fa-li"><i class="fas fa-check"></i></span>Free Subdomain</li>
								<li><span class="fa-li"><i class="fas fa-check"></i></span>Monthly Status Reports</li>
								<li class="text-muted"><span class="fa-li"><i class="fas fa-times"></i></span>Monthly Status Reports</li>
								<li class="text-muted"><span class="fa-li"><i class="fas fa-times"></i></span>Monthly Status Reports</li>
								<li class="text-muted"><span class="fa-li"><i class="fas fa-times"></i></span>Monthly Status Reports</li>
							</ul>
							<a href="{{ url('stripe')}}?price=50" class="btn btn-block btn-primary text-uppercase">SELECT</a>
						</div>
					</div>
				</div>
				
				<div class="col-lg-4">
					<div class="card">
						<div class="card-body">
							<h5 class="card-title text-muted text-uppercase text-center">YEARLY</h5>
							<h6 class="card-price text-center">$500<span class="period">/year</span></h6>
							<hr>
							<ul class="fa-ul">
								<li><span class="fa-li"><i class="fas fa-check"></i></span><strong>Unlimited Users</strong></li>
								<li><span class="fa-li"><i class="fas fa-check"></i></span>150GB Storage</li>
								<li><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited Public Projects</li>
								<li><span class="fa-li"><i class="fas fa-check"></i></span>Community Access</li>
								<li><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited Private Projects</li>
								<li><span class="fa-li"><i class="fas fa-check"></i></span>Dedicated Phone Support</li>
								<li><span class="fa-li"><i class="fas fa-check"></i></span><strong>Unlimited</strong> Free Subdomains</li>
								<li><span class="fa-li"><i class="fas fa-check"></i></span>Monthly Status Reports</li>
								<li class="text-muted"><span class="fa-li"><i class="fas fa-times"></i></span>Monthly Status Reports</li>
							</ul>
							<a href="{{ url('stripe')}}?price=550" class="btn btn-block btn-primary text-uppercase">SELECT</a>
						</div>
					</div>

				</div>
						<div class="col-lg-4">
				<div class="card mb-5 mt-4 mb-lg-0" style="height: 30em">
				  <div class="card-body">
					<h5 class="card-title text-muted text-uppercase text-center">YEARLY</h5>
					<h6 class="card-price text-center">$500<span class="period">/year</span></h6>
					<hr>
					<ul class="fa-ul">
					  <li><span class="fa-li"><i class="fas fa-check"></i></span><strong>Unlimited Users</strong></li>
					  <li><span class="fa-li"><i class="fas fa-check"></i></span>150GB Storage</li>
					  <li><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited Public Projects</li>
					  <li><span class="fa-li"><i class="fas fa-check"></i></span>Dedicated Phone Support</li>
					  <li><span class="fa-li"><i class="fas fa-check"></i></span><strong>Unlimited</strong> Free Subdomains</li>
					  <li><span class="fa-li"><i class="fas fa-check"></i></span>Monthly Status Reports</li>
					</ul>
					<a href="#" class="btn form-shadow genric-btn info">SELECT</a>
				  </div>
				</div>
			  </div> --}}
				 @foreach ($packages as $pack)
				<div class="col-lg-4">
					<div class="card">
						<div class="card-body">
							<h5 class="card-title text-muted text-uppercase text-center">{{$pack->name}}</h5>
							<h6 class="card-price text-center">${{$pack->amount}}<span class="period">{{$pack->package_duration}}</span></h6>
							<hr>
							<P>{!! $pack->description !!}</P>
							    <ul class="fa-ul">

                                @foreach($option as $opt)
                                @php  
								$options = DB::table('pacakge_relation_options')->select('*')->where('package_id', $pack->id)->where('option_id',$opt->id)->get();  
                                @endphp 
                                @if(count($options)>0)
                                 
                                <li><span class="fa-li" ><i class="fas fa-check"></i></span>{{$opt->option_name}}</li>      
                                 
                                 @else
                                <li><span class="fa-li" ><i class="fa fa-times"></i></span>{{$opt->option_name}}</li>      

                                 @endif
                              
                                 @endforeach
                            </ul> 

							
 
							<!-- <a href="{{ url('stripe')}}?p_id={{$pack->id}}&price={{number_format((float)$pack->amount, 0, '.', '')}}" class="btn btn-block btn-primary text-uppercase">SELECT</a> -->
							<a href="{{ url('stripe')}}?p_id={{$pack->id}}&price={{$pack->amount}}" class="genric-btn  info btn-block btn-primary text-uppercase">SELECT</a>
						</div>
					</div>
					&nbsp;
				</div>
				@endforeach 
				

			</div>
		</div>
	</section>
	<!-- Pricint Table -->
	
	
	
</main>
@endsection
  <style>
        .card-body{
            min-height: 316px;
        }
          p{
            padding-left: 35px;
        }
    </style>