<?php

namespace App\Http\Controllers;

use App\Chat;
use App\Company;
use App\Events\MessageSent;
use App\Message;
use App\Profile;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Category;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use App\Location;
use Spatie\Permission\Models\Role;
use App\Media;
use \DB;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = Company::all();
        return view('admin.company.index', compact('companies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $locations = Location::all();
        return view('admin.company.create', compact('locations'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $validated = $request->validate([
            'name' => 'required',
            'image' => 'required',
            'phone' => 'required',
            'url' => 'required',
        ]);
        $imageName = '';

        if ($request->has('image')) {
            $imageName = time().'.'.$request->image->extension();
            $request->image->move(public_path('/uploads/users/companies'), $imageName);
        }

            Company::create([
                "name" => $request['name'],
                "logo" => '/uploads/users/companies/' . $imageName,
                "address" => $request['address'],
                "phone" => $request['phone'],
                "url" => $request['url'],
                "company_registration_no" => $request['company_registration_no'],
                "term_and_condition" => $request['term_and_condition'],
                "location_id" => $request['location'],
            ]);

        toastr()->success('Company Created Successfully');
        return redirect()->route('admin.companies.index');
    }

    public function setdefault(Request $request){
            
         
        DB::table('company')->update(['by_default' => 0]);
        Company::where('id', $request->company_id)->update([
            "by_default" => 1,           
        ]);
        
        return redirect()->route('admin.companies.index');
       }

    /**
     * Display the specified resource.
     *
     * @param  \App\Chat  $chat
     * @return \Illuminate\Http\Response
     */
    public function show(Chat $chat)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Chat  $chat
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $company = Company::where('id', $id)->first();
        return view('admin.company.edit', compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Chat  $chat
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $validated = $request->validate([
            'name' => 'required',
            'phone' => 'required',
            'url' => 'required',
        ]);
        $imageName = '';

        if ($request->has('image')) {
            $imageName = time().'.'.$request->image->extension();
            $request->image->move(public_path('/uploads/users/companies'), $imageName);
            Company::where('id', $id)->update([
                "logo" => '/uploads/users/companies/' . $imageName,
                ]);
        }

        Company::where('id', $id)->update([
            "name" => $request['name'],
            "address" => $request['address'],
            "phone" => $request['phone'],
            "url" => $request['url'],
            "company_registration_no" => $request['company_registration_no'],
            "term_and_condition" => $request['term_and_condition'],
//            "description" => $request['description'],
        ]);

        toastr()->success('Company Created Successfully');
        return redirect()->route('admin.companies.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Chat  $chat
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Company::where('id', $id)->delete();
        return redirect()->back();
    }
 

    public function company_performance() {
        $users = User::with('messages')->orderBy("created_at", "DESC")->get();
        $media = Media::where('name', 'gallery')->get();
        $roles = Role::with('users')->get();
        $categories = Category::all();
        $breadcrumb = 'Dashboar / Company Information';
        $products  = DB::table('mdd_price_list_csv')->get();
        return view('admin.company_performance.create', compact('users', 'roles', 'breadcrumb', 'media' ,'products' , 'categories'));
    }

    public function getCompanyById(Request $request) {
        $company = Company::where('id', $request['id'])->first();
        return response()->json(['type' => 'success', 'company' => $company]);
    }
}
