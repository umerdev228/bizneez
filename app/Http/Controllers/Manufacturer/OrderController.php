<?php

namespace App\Http\Controllers\Manufacturer;

use App\Exports\OrderExport;
use App\Http\Controllers\Controller;
use App\ManufacturerOrder;
use App\Order;
use App\OrderProduct;
use App\Quotation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

class OrderController extends Controller
{

    public function __construct()
    {
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        //
        $orders = ManufacturerOrder::where('manufacturer_id', Auth::id())->with('architect')->with('order')->orderBy('id', 'desc')->get();
        $breadcrumb = ucfirst(Auth::user()->roles[0]->name)." / Purchase / Orders";
        return view('manufacturer.order.index', compact('orders', 'breadcrumb'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $breadcrumb = ucfirst(Auth::user()->roles[0]->name) . " / Purchase / Order / Products ";
        $order = Order::where('id', $id)->with('products')->first();
        $total = 0;
        $products = $order->products;
        foreach ($products as $product) {
            if ($product->user_id == Auth::id()) {
                $total += (integer)$product->price * $product->pivot->quantity;
            }
        }
        $order_id = $order->id;
        return view('manufacturer.order.show', compact('order', 'products', 'order_id', 'breadcrumb', 'total'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
    }


    public function exportExcelCustomDesign ()
    {
        return Excel::download(new OrderExport(), 'invoices.xlsx');
    }

    public function exportExcelCustomDesignPost ()
    {
        return Excel::download(new OrderExport(), 'invoices.xlsx');
    }

    public function pdfExportQuotation (Request $request)
    {
        $qutation = Quotation::where('id', $request->order_id)->first();
        return response()->download(public_path($qutation->path));

        $orders = json_decode($request->products);
        view()->share('orders',$orders);
        $file_name = 'Quo-'.date('YmdHis').'.pdf';
        PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif']);
        $pdf = PDF::loadView('admin.order.pdfQuotation');
        $pdf->save(public_path('/uploads/orders/quotations/').$file_name);
        Quotation::create([
            'name' => $file_name,
            'order_id' => $file_name,
            'date' => date('Y-m-d H:i:s'),
            'path' => '/uploads/orders/quotations/'.$file_name
        ]);
        return $pdf->download('pdfview.pdf');
        return view('admin.order.pdfQuotation', compact('orders'));
    }

    public function orderInvoiceShow($id) {
        $order = Order::where('id', $id)->with('products')->first();
        $total = 0;
        foreach ($order->products as $product) {
            $total += $product->price * $product->quantity;
        }
        return view('architect.invoice.show', compact('order', 'total'));
    }






}
