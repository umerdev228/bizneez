<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->string('image')->nullable();
            $table->string('description')->nullable();
            $table->string('phone')->nullable();
            $table->string('mobile')->nullable();
            $table->string('assistant_number')->nullable();
            $table->string('address')->nullable();
            $table->string('location_id')->nullable();
            $table->string('area_id')->nullable();
            $table->string('company_keywords')->nullable();
            $table->string('web')->nullable();

            $table->string('video_call_demo')->nullable();
            $table->string('demo_code')->nullable();
            $table->string('city')->nullable(); 
            $table->string('team_building')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
