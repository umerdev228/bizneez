/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
require('./bootstrap');
import Vue from 'vue';
window.Vue = require('vue');
import BootstrapVue from 'bootstrap-vue'
// Import Bootstrap an BootstrapVue CSS files (order is important)
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(BootstrapVue)
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */



// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('delivery-component', require('./components/delivery/DeliveryAppoinmentBooking').default);
Vue.component('event-calendar', require('./components/calendar/calendar').default);
Vue.component('create-event', require('./components/calendar/addEvent').default);
Vue.component('video-chat', require('./components/video/VideoChat').default);
Vue.component('video-chat-updated', require('./components/video/admin/VideoCallComponent').default);
Vue.component('chat-index', require('./components/chat/indexChatComponent.vue').default);
Vue.component('chat-messages', require('./components/chat/ChatMessages.vue').default);
Vue.component('chat-form', require('./components/chat/ChatForm.vue').default);
Vue.component('all-user-list', require('./components/chat/allUsersComponent.vue').default);
Vue.component('catalogue-component', require('./components/template/templateComponent').default);
Vue.component('user-event-calendar', require('./components/user/calendar/calendar').default);
Vue.component('active-user', require('./components/video/ActiveUserComponent').default);

Vue.component('user-event-calendar', require('./components/user/calendar/calendar').default);



Vue.component('video-chat-webrtc', require('./components/webrtc/VideoChatComponent.vue').default);
Vue.component('video-chat-webrtc-client-to-client', require('./components/webrtc/client-to-client/VideoChatComponent.vue').default);
Vue.component('video-chat-webrtc-employee-to-demo', require('./components/webrtc/employee-to-demo/VideoChatComponent.vue').default);


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

if (document.getElementById("app")) {
    const vueApp = new Vue({
        el: '#app',
        // data: {}

    });
}

//


