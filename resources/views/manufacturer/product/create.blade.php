@extends('admin.layouts.app')
@section('content')
    @include('admin.layouts.topbar')

    <style>
        .dropzone {
            background: white;
            border-radius: 5px;
            border: 2px dashed rgb(0, 135, 247);
            border-image: none;
            width: 100%;
            /*max-width: 500px;*/
            margin-left: auto;
            margin-right: auto;
        }

        .bootstrap-filestyle {
            display: none;
        }
        .label-info {
            background-color: #5bc0de;
            display: inline-block;
            padding: 0.2em 0.6em 0.3em;
            font-size: 75%;
            font-weight: 700;
            line-height: 1;
            color: #fff;
            text-align: center;
            white-space: nowrap;
            vertical-align: baseline;
            border-radius: 0.25em;
        }

    </style>
    <!-- Google Chart JS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/min/dropzone.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/dropzone.js"></script>
    <!-- Start Page content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if(session()->has('message'))
                        <div class="alert alert-success" style="width:803px ">
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-9 mt-5">
                            <div class="card-box">
                                <form method="POST" action="{{ route('manufacturer.products.store') }}" enctype="multipart/form-data">
                                    @csrf
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="name"> Name <span class="text-danger">*</span></label>
                                                <input type="text" class="form-control valid" name="name" id="name"  required />
                                                @if ($errors->has('name'))
                                                    <span class="error">{{ $errors->first('name') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="price"> Price <span class="text-danger">*</span></label>
                                                <input type="text" class="form-control valid" name="price" id="price"  required />
                                                @if ($errors->has('price'))
                                                    <span class="error">{{ $errors->first('price') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="second_price"> Second Price <span class="text-danger">*</span></label>
                                                <input type="text" class="form-control valid" name="second_price" id="second_price"  required />
                                                @if ($errors->has('second_price'))
                                                    <span class="error">{{ $errors->first('second_price') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="discount"> Discount <span class="text-danger">*</span></label>
                                                <input type="text" class="form-control valid" name="discount" id="discount"  required />
                                                @if ($errors->has('discount'))
                                                    <span class="error">{{ $errors->first('discount') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="quantity"> Quantity <span class="text-danger">*</span></label>
                                                <input type="text" class="form-control valid" name="quantity" id="quantity"  required />
                                                @if ($errors->has('quantity'))
                                                    <span class="error">{{ $errors->first('quantity') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="Description">Description<span class="text-danger">*</span></label>
                                                <textarea id="Description" class="form-control rounded-0" name="description" rows="5"></textarea>
                                                @if ($errors->has('description'))
                                                    <span class="error">{{ $errors->first('description') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="name"> Categories <span class="text-danger">*</span></label>
                                                <input type="text" class="form-control valid" name="categories[]" id="category" required />
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="size"> Size  <span class="text-danger">*</span> <small class="text-success">Enter Commas Seprated Sizes</small></label>
                                                <input class="form-control valid" type="text" name="size" id="size" required>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="range"> Range Types  <span class="text-danger">*</span></label>
                                                <select name="range" id="range" class="form-control valid">
                                                    <option value="0">Choose Range</option>
                                                    <option value="1">Range 1</option>
                                                    <option value="2">Range 2</option>
                                                    <option value="3">Range 3</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="range"> Color Type  <span class="text-danger">*</span></label>
                                                <select name="color_type" id="range" class="form-control valid" >
                                                    <option value="0">Choose Color Type</option>
                                                    <option value="1">Desktop</option>
                                                    <option value="2">Leg Frame</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="color"> Colors <span class="text-danger"></span></label>
                                                <div id="dropzone">
                                                    <div class="dropzone needsclick" id="colors-upload">
                                                        <div class="dz-message needsclick">
                                                            Upload Your Product Color Images
                                                        </div>
                                                    </div>
                                                </div>
                                                <br/>
                                                <div id="preview-color-image" style="display: none;">
                                                    <div class="dz-preview dz-file-preview">
                                                        <div class="dz-image">
                                                            <img data-dz-thumbnail=""/>
                                                        </div>
                                                        <div class="dz-details">
                                                            <div class="dz-size"><span data-dz-size=""></span></div>
                                                            <div class="dz-filename"><span data-dz-name=""></span></div></div>
                                                        <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress=""></span></div>
                                                        <div class="dz-error-message"><span data-dz-errormessage=""></span></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="image"> Images <span class="text-danger"></span></label>
                                                <div id="dropzone">
                                                    <div class="dropzone needsclick" id="image-upload">
                                                        <div class="dz-message needsclick">
                                                            Upload Your Product Images
                                                        </div>
                                                    </div>
                                                </div>
                                                <br/>
                                                <div id="preview-image" style="display: none;">
                                                    <div class="dz-preview dz-file-preview">
                                                        <div class="dz-image">
                                                            <img data-dz-thumbnail=""/>
                                                        </div>
                                                        <div class="dz-details">
                                                            <div class="dz-size"><span data-dz-size=""></span></div>
                                                            <div class="dz-filename"><span data-dz-name=""></span></div></div>
                                                        <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress=""></span></div>
                                                        <div class="dz-error-message"><span data-dz-errormessage=""></span></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group mt-3">
                                        <input class="btn btn-primary" type="submit" value="Create Product">
                                        <button onclick="history.back()" type="button" class="btn">Cancel</button>
                                    </div>
                                </form>
                            </div> <!-- end card-box -->
                        </div>

                    </div> <!-- container -->
                </div> <!-- content -->
            </div>
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <style href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/dropzone.css"></style>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/dropzone.js"></script>


    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-beta/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
    <link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/examples/assets/app.css">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.0.4/popper.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.11.1/typeahead.bundle.min.js"></script>
    <script src="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>

    <script>

        var data = '{!! json_encode($categories) !!}'
        console.log(data)
        //get data pass to json
        var task = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace("name"),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            local: jQuery.parseJSON(data) //your can use json type
        });

        task.initialize();

        var elt = $("#category");
        elt.tagsinput({
            itemValue: "id",
            itemText: "name",
            typeaheadjs: {
                name: "name",
                displayKey: "name",
                source: task.ttAdapter()
            }
        });


        Dropzone.autoDiscover = false;
        // $(document).ready(function() {
        var dropzone = new Dropzone(document.getElementById('image-upload'), {
            url: "{{route('product.image.update')}}", // Set the url
            previewTemplate: document.querySelector('#preview-image').innerHTML,
            parallelUploads: 2,
            thumbnailHeight: 120,
            thumbnailWidth: 120,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            filesizeBase: 1000,
            thumbnail: function(file, dataUrl) {
                if (file.previewElement) {
                    file.previewElement.classList.remove("dz-file-preview");
                    var images = file.previewElement.querySelectorAll("[data-dz-thumbnail]");
                    for (var i = 0; i < images.length; i++) {
                        var thumbnailElement = images[i];
                        thumbnailElement.alt = file.name;
                        thumbnailElement.src = dataUrl;
                    }
                    setTimeout(function() { file.previewElement.classList.add("dz-image-preview"); }, 1);
                }
            }

        });

        var dropzone = new Dropzone(document.getElementById('colors-upload'), {
            url: "{{route('product.color.image.update')}}", // Set the url
            previewTemplate: document.querySelector('#preview-color-image').innerHTML,
            parallelUploads: 2,
            thumbnailHeight: 120,
            thumbnailWidth: 120,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            filesizeBase: 1000,
            thumbnail: function(file, dataUrl) {
                if (file.previewElement) {
                    file.previewElement.classList.remove("dz-file-preview");
                    var images = file.previewElement.querySelectorAll("[data-dz-thumbnail]");
                    for (var i = 0; i < images.length; i++) {
                        var thumbnailElement = images[i];
                        thumbnailElement.alt = file.name;
                        thumbnailElement.src = dataUrl;
                    }
                    setTimeout(function() { file.previewElement.classList.add("dz-image-preview"); }, 1);
                }
            }

        });
    </script>
@endsection
