@extends('admin.layouts.app')
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    @if(session()->has('message'))
                        <div class="alert alert-success mt-5" id="myElem" >
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-12 mt-5">
                            <div class="card">
                                <div class="card-body">

                                    <form action="{{ route('admin.location.update', ['id' => $location->id]) }}" method="post">
                                        @csrf
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label for="name"> Name <span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control valid" name="name" value="{{$location->name}}" id="name" required />
                                                    @if ($errors->has('name'))
                                                        <span class="error">{{ $errors->first('name') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="latitude"> Latitude <span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control valid" name="latitude" value="{{$location->latitude}}" id="latitude" required />
                                                    @if ($errors->has('latitude'))
                                                        <span class="error">{{ $errors->first('latitude') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="longitude"> Longitude <span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control valid" name="longitude" value="{{$location->longitude}}" id="longitude" required />
                                                    @if ($errors->has('longitude'))
                                                        <span class="error">{{ $errors->first('longitude') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="area_id">Location Type<span class="text-danger">*</span></label>
                                                    <select class="form-control valid" name="type" id="type" required>
                                                        <option @if($location->type == 'area') selected @endif value="country">Country</option>
                                                        <option @if($location->type == 'city') selected @endif  value="city">City</option>
                                                        <option @if($location->type == 'area') selected @endif  value="area">Area</option>
                                                    </select>

                                                    @if ($errors->has('type'))
                                                        <span class="error">{{ $errors->first('type') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="parent">Parent<span class="text-danger"></span></label>
                                                    <select class="form-control valid" name="parent" id="parent">
                                                        <option value="1">Choose Parent</option>
                                                        @foreach($locations as $loc)
                                                            <option @if($location->parent_id == $loc->id) selected @endif value="{{ $loc->id }}">{{ $loc->name }}</option>
                                                        @endforeach

                                                    </select>

                                                    @if ($errors->has('parent'))
                                                        <span class="error">{{ $errors->first('parent') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group mt-3">

                                            <input class="btn btn-primary" type="submit" value="Create Location">
                                            <button onclick="history.back()" type="button" class="btn">Cancel</button>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection