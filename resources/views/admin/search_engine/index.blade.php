@extends('admin.layouts.app')
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    @if(session()->has('message'))
                        <div class="alert alert-success mt-5" id="myElem" >
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-12 mt-5">
                            <div class="card">
                                <div class="card-body">
                                    <a href="{{route('admin.advertisement.create')}}"  class="btn btn-primary" ><i class="fa fa-plus"></i>Add New Advertisement </a>
                                </div>
                                <div class="card-body">
                                    <table class="table table-hover m-0 tickets-list table-actions-bar dt-responsive nowrap" width="100%" id="normal-table">
                                        <thead>
                                        <tr>
                                            <th>Title</th>
                                            <th>Shape</th>
                                            <th>Image</th>
                                            <th>Date</th>
                                            <th>Time</th>
                                            <th data-orderable="false">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($advertisements as $advertisement)
                                            <tr>
                                                <td>{{$advertisement->title}}</td>
                                                <td>{{$advertisement->shape}}</td>
                                                <td><img src="{{asset($advertisement->media->path)}}" width="100"></td>
                                                <td>{{$advertisement->date}}</td>
                                                <td>{{$advertisement->time}}</td>
                                                <td>
                                                    <div class="btn-group">
                                                        <a class="btn" href="{{ route('admin.advertisement.edit', ['id' => $advertisement->id] ) }}">
                                                            <i class="mdi mdi-pencil"></i>
                                                        </a>
                                                        <a class="btn" href="{{ route('admin.advertisement.delete', ['id' => $advertisement->id] ) }}">
                                                            <i class="fa fa-close"></i>
                                                        </a>
                                                        <a class="btn" href="{{ route('admin.advertisement.show', ['id' => $advertisement->id] ) }}">
                                                            <i class="fa fa-eye"></i>
                                                        </a>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div> <!-- end card -->
                        </div><!-- end col -->
                    </div>

                </div>
            </div>

        </div> <!-- container -->
    </div> <!-- content -->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
@endsection
