@extends('admin.layouts.app')
@section('content')
@include('admin.layouts.topbar')

    <style>
    .right-area{
    background-color: #eee;
    color: #000;
    min-height: 1200px;
    margin-top: 31px;
    font-size: 20px;
    padding: 20px;}
    .right-area-footer{
    background-color: #eee;
    color:#000;
    width: 100%;
    font-size:20px;
    min-height: 300px;
    padding:20px;}
      textarea.form-control{width: 438px!important;min-height: 42px!important;}

    </style>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-8"> 
                <div class="row">

                    
           
                         
                                
                                @foreach($media as $post)
                                            <div class="col-md-6">   
                                                <div class="inner-main-body p-2 p-sm-3 collapse forum-content show mb-2">
                                    <div>
                                        <img src="{{asset($post->image)}}" class="img-responsive" style="height: 300px"/>
                                        <h3>{{$post->detail}}</h3>
                                        <div class="tab-pane fade show active" id="posts" role="tabpanel" aria-labelledby="posts-tab">
                                            <form action="{{route('social.media.post.comment')}}" method="post">
                                                @csrf
                                                <div class="form-group">
                                                    <input type="hidden" name="post_id" value="{{$post->id}}">
                                                    <label class="sr-only" for="message">Post</label>
                                                    <textarea name="comment" class="form-control" id="message" placeholder="What are you thinking?"></textarea>
                                                    <input type="submit" class="btn btn-primary m-2" value="Submit">
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                  
                                    @foreach($post->comments as $comment)
                                    @php
                                    $user = \App\User::where('id', $comment->user_id)->first();
                                    $profile = \App\Profile::where('user_id', $comment->user_id)->first();
                                    $date2 = time();
                                    $date1 = strtotime($comment->created_at);
                                    $mins = ($date2 - $date1) / 60;
                                    if ($mins > 60) {
                                    $mins = $mins / 60;
                                    $mins = number_format((float)$mins, 0, '.', '') . 'Hours';
                                    }
                                    else {
                                    $mins = number_format((float)$mins, 0, '.', '') . 'Minutes';
                                    }
                                    @endphp
                                    <div class="card mb-2">
                                        <div class="card-body p-2 p-sm-3">
                                            <div class="media forum-item">
                                                <a href="#" data-toggle="collapse" data-target=".forum-content">
                                                    @if($profile->photo)
                                                    <img src="{{asset($profile->photo)}}" class="mr-3 rounded-circle" width="50" alt="User"/>
                                                    @else
                                                    <img src="{{asset('theme/admin/assets/images/users/avatar-1.jpg')}}" class="mr-3 rounded-circle" width="50" alt="User"/>
                                                    @endif
                                                </a>
                                                <div class="media-body">
                                                    <h6><a href="#" data-toggle="collapse" data-target=".forum-content" class="text-body">{{$user->name}}</a></h6>
                                                    <p class="text-secondary">
                                                        {{$comment->comment}}
                                                    </p>
                                                    <p class="text-muted"><a href="javascript:void(0)">{{$user->name}}</a> comment <span class="text-secondary font-weight-bold">{{$mins}} ago</span></p>
                                                </div>
                                                <div class="text-muted small text-center align-self-center">
                                                    <a href="{{route('social.media.post.like', ['id' => $post->id])}}"><span class="d-none d-sm-inline-block"><i class="fa fa-thumbs-up"></i> {{$comment->likes}}</span></a>
                                                    <span><i class="fa fa-comment ml-2"></i> {{count($post->comments)}} </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                                <hr>
                                <!-- /Forum List -->
                                @endforeach
                           
                            <div class="pagination-area">
                                {{ $media->links() }}
                            </div>
                            <!-- /Inner main -->
                      
                        <!-- New Thread Modal -->
                    </div>
                  </div>
            </form>
            <div class="col-md-4">
                <div class="right-area"> Advertisment Area</div>
            </div>
            
            </div> <!-- content -->
        </div>
    </div>
    
    
    @endsection
