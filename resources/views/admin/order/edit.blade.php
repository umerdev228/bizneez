@extends('admin.layouts.app')
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    @if(session()->has('message'))
                        <div class="alert alert-success mt-5" id="myElem" >
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    <br><br><br><br><br>
                    <form action="{{ route('quotation.update', ['id' => $order->id]) }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label for="location_id">Select Company</label>
                                    <select required type="text" class="form-control valid" name="company" id="">
                                        @foreach($companies as $company)
                                            <option @if($company->id == $quotation->company_id) selected @endif value="{{$company->id}}">{{$company->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3">

                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label for="location_id">Quotation {{$order->serial_number}}</label>
                                    <input type="hidden" name="serial_number" id="serial_number" value="{{$order->serial_number}}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="location_id">Contact Name</label>
                                    <input required type="text" class="form-control valid" name="contact_person" value="{{$contact->contact_person}}" id=""/>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="location_id">Company Address</label>
                                    <input required type="text" class="form-control valid" name="company_address" value="{{$company->address}}" id=""/>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label for="location_id">Client Address</label>
                                    <input required type="text" class="form-control valid" name="client_address" value="{{$contact->invoice_address}}" id=""/>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label for="location_id">Delivery Address</label>
                                    <input required type="text" class="form-control valid" name="invoice_address" value="{{$contact->client_address}}" id=""/>
                                </div>
                            </div>
                        </div>
                        <hr/>
                        @php
                            $subtotal = 0;
                        @endphp
                        <div id="products" >
                            @foreach($products as $index => $product)
                                <input type="hidden" name="product_id[]" value="{{$product->id}}">
                            <div id="r{{$index}}" class="row">
                                &nbsp;
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label for="location_id">Image Upload</label>

                                        <button id="imageButton-{{$index}}" onclick="showModalImageUpload({{$index}})" type="button" style="@if($product->image) display: none; @endif  height: 35px; margin-top: 2px;border-radius: 10px; background-color: #a08bff;" class="btn btn-success valid align-baseline">
                                            <i class="fa fa-picture-o"></i>
                                        </button>
                                        <input type="hidden" value="{{$product->image}}" name="file{{$index}}" id="file{{$index}}">
                                        <a onclick=" removeImage({{$index}})" style="@if($product->image) display:block; @else display:none;  @endif" id="fileClose{{$index}}" href="javascript:void(0)" class="file-close">
                                            <i class="mdi mdi-close-circle"></i>
                                        </a>
                                        <img id="image_show{{$index}}" width="50px" src="{{$product->image}}">
                                    </div>
                                    <div id="imgThumbnailPreview{{$index}}" style="width: 300px;"></div>
                                    <input type="hidden" name="title[]" id="title{{$index}}" >
                                    <input type="hidden" name="keywords[]" id="keywords{{$index}}" >
                                    <input type="hidden" name="image_description[]" id="image_description{{$index}}" >
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label for="location_id">Description</label>
                                        <div class="row">
                                            <input style="width: 190px;" required type="text" class="form-control valid" name="description[]" id="description{{$index}}" value="{{$product->description}}"/>
                                            <button onclick="showModalSelectRow(0)" type="button" class="btn btn-sm btn-small btn-primary" data-toggle="modal" data-target="#descriptionModalLong">
                                                <i class="fa fa-pie-chart"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label for="area_id">Qty </label>
                                        <input required type="text" class="form-control valid"  name="qty[]" id="qty{{$index}}" value="{{$product->qty}}" onchange="getValues(0)"/>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label for="area_id">Price </label>
                                        <input required type="text" class="form-control valid" name="price[]" id="price{{$index}}" value="{{$product->price}}" onchange="getValues(0)"/>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label for="area_id">Discount(%)  </label>
                                        <input required type="text" class="form-control valid" name="percentage[]" id="percentage{{$index}}" value="{{$product->percentage}}" onchange="getValues(0)"/>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label for="area_id">Disct Price </label>
                                        <input required type="text" class="form-control valid" name="discount_price[]" id="discount_price{{$index}}" value="{{$product->discount_price}}" onchange="getValues(0)"/>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        <label for="area_id">Total </label>
                                        <input required type="text" class="form-control valid" name="total[]" id="total{{$index}}" value="{{$product->discount_price * $product->qty}}" />
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group">
                                        @if(!$index > 0)
                                            <button type="button" onclick="appendFieldsR()" style="margin-top: 30px; border-radius: 10px; height: 35px;" class="btn btn-success valid align-baseline"><i class="fa fa-plus"></i> </button>
                                        @else
                                            <button type="button" onclick="removeFieldR({{$index}})" style="border-radius: 10px; margin-top: 35px;" class="btn btn-danger valid align-baseline"><i class="fa fa-minus"></i> </button>
                                        @endif
                                        <!-- Modal -->
                                        <div style="width:100%; z-index: 999999" class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document" style="    max-width: 73%;">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Image Gallery</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="row">
                                                            <div class="col-md-8">
                                                                <div class="row" id="range-images">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4" style="border-left: 1px solid #d2cece;">
                                                                <div align="center">

                                                                    <select onchange="manufacturerChange()" name="manufacturer" id="manufacturer" class="form-control">
                                                                        <option>--Select Manufacture--</option>
                                                                        @foreach($manufacturers as $manufacturer)
                                                                            <option value="{{$manufacturer->id}}">{{ $manufacturer->name }}</option>
                                                                        @endforeach
                                                                    </select> <br>

                                                                    <select onchange="rangeChange()" name="ranges" id="ranges" class="form-control">
                                                                        <option>--Select Ranges--</option>
                                                                    </select>

                                                                    <div class="mt-3">
                                                                        <div class="form-group">
                                                                            <label for="keywords">Keyword</label>
                                                                            <input class="form-control" type="text" id="keywords">
                                                                        </div>

                                                                        <div class="form-group">
                                                                            <label for="title">Title</label>
                                                                            <input class="form-control" type="text" id="title">
                                                                        </div>

                                                                        <div class="form-group">
                                                                            <label for="image_description">Description</label>
                                                                            <input class="form-control" type="text" id="image_description">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @php
                                $subtotal += $product->discount_price * $product->qty;
                            @endphp
                            @endforeach
                        </div>
                        <div class="row">
                            <div class="col-sm-6">

                            </div>
                            <div class="col-sm-2">
                                <table class="table table-striped">
                                    <tr>
                                        <td>Sub Total</td><td id="sub_total">{{$subtotal}}</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div style="float: left; margin-top: 5px;">VAT% </div>
                                            <input type="text" value="{{$quotation->vat}}" class="input-sm" onchange="getValues(0)" id="vat" name="vat">
                                        </td>
                                        <td> <div id="vat_value">{{number_format((float)$quotation->vat_value_hid, 2, '.', '')}}</div>
                                            <input type="hidden" id="vat_value_hid" name="vat_value_hid" value="{{number_format((float)$quotation->vat_value_hid, 2, '.', '')}}" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Gross Total</td><td id="gross_total">{{$quotation->vat_value_hid + $subtotal}}</td>
                                        <td> &nbsp;  </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6"><input name="termconditions" id="termconditions" type="checkbox" class="checkbox">
                                <label for="termconditions">Term & Condition</label></div>
                            <div class="col-md-6">
                                <input style="background: #0acf97;" href="#" type="submit" class="btn btn-primary" value="Update" name="button">
                                <input style="background-color: #abafb5;" id="preview" class="submit ml-2 btn btn-success" type="submit" value="Preview" name="button">
                            </div>
                        </div>


                    </form>
                    <div class="term_condition">
                        <h3>Term and Condition</h3>
                        <h4 class="hd">1.Quotation</h4>
                        <div class="quatation">
                            Quotations are not an offer but instead an invitation to treat and no contract shall come into existence unless and until Bizneez.co.uk Ltd has accepted in writing your official order to carry out the service and supply the goods specified in Bizneez Group Ltd quotation. Unless otherwise expressly agreed in writing the contract shall be on the terms, and subject to, the conditions hereinafter set out.<br>
                            a)  Unless otherwise stated quotations will be based upon current prices for material, labour and transport and the price quoted will be subject to adjustment to reflect any alterations during the period between the date of the quotation and order (if later) and the date of the completion of the contract in the prices of the materials or in any wage rates governed by national agreement in any transport costs.<br>
                            b)  All sums given in any quotation unless otherwise stated are exclusive of VAT which will be payable in addition at the rate for the time of being in force.
                        </div>
                        <h4 class="hd">2.Orders</h4>
                        <div class="quatation">
                            a)  All orders are accepted on the clear understanding that these conditions of business shall apply irrespective of any conditions printed on customer’s orders. No variation will be accepted unless agreed by a Director of Bizneez.co.uk Ltd in writing.<br>
                            b)  Orders cannot be canceled unless by agreement with Bizneez.co.uk Ltd and upon payment by the buyer of a cancellation fee (settlement) equal to any loss sustained by Bizneez.co.uk Ltd.
                        </div>
                        <h4 class="hd">3. Deliveries  </h4>
                        <div class="quatation">
                            a) Bizneez.co.uk Ltd shall use its best endeavors to start and complete the contract within the agreed time but Bizneez.co.uk Ltd shall in no circumstances be liable for any loss and damage, in contract and/or tort, consequential or otherwise caused directly or indirectly by any delay in the delivery of goods and completion of the contract.<br>
                            b)  Claims for goods rejected as damaged can only be allowed if the goods have been signed for as “damaged” or “unexamined upon delivery” and Bizneez.co.uk Ltd receives notice within 3 working days of delivery. The non-receipt of goods must be notified to Bizneez.co.uk Ltd Ltd within 7 working days of the date of advice of dispatch otherwise such goods will be deemed to have been received in a satisfactory condition.
                        </div>
                        <h4 class="hd">4. Installation   </h4>
                        <div class="quatation">
                            a)Unless otherwise agreed prices are based on Bizneez.co.uk Ltd’s normal working week. Work carried out at other times at the customer’s request will lead to an additional charge.<br>
                            b)  Delays on site, which are not the responsibility of Bizneez.co.uk Ltd before or during installation, caused by other trade and from any other cause will lead to an additional charge.<br>
                            c)  Every effort will be made to carry out specification as detailed but Bizneez.co.uk Ltd reserves the right at its own discretion to effect minor modifications and improvements.
                        </div>
                        <h4 class="hd">5. Payment   </h4>
                        <div class="quatation">
                            a)  Unless otherwise stated in writing Bizneez.co.uk Ltd terms of payment are<br>
                            b)  In all First time contracts the customer shall pay 100% of the total order value on placing the order.<br>
                            c)  In all clients contracts including installation, payment of the balance shall be made within 14 days of completion of the installation. ( Based only for named clients after 1st order ) A client is all parties in which has previously placed a first order with Bizneez.co.uk Ltd.<br>
                            d)  Interest at the rate of one and a half percent per month will be charged on monies overdue under clause (a) and (b) hereof.<br>
                            e)  All payment must be made on or before the due date as a condition precedent to the future deliveries and in the event of late payment Bizneez.co.uk Ltd Ltd shall be entitled to suspend any work relating to the contract and to claim any cost associated with such suspension.<br>
                            f)  The customer shall be liable for all legal action and other costs incurred in Bizneez.co.uk Ltd having to instruct solicitors to recover outstanding invoices, including all those costs and disbursements (if any) charged to Bizneez.co.uk Ltd Ltd on an indemnity basis.<br>
                        </div>
                        <h4 class="hd">6. Property in Goods    </h4>
                        <div class="quatation">
                            ) Property in all goods supplied shall remain in Bizneez.co.uk Ltd until the invoice relating thereto shall have been paid in full including any interest or other sums due thereon by the customer.<br>
                            b)  Risk of damage to or loss of the goods shall pass to the buyer at the time of delivery. This does not affect the passing of property ownership as above.<br>
                            c)  If payment of account is overdue (time being of the essence) or the buyer becomes insolvent, commences winding up procedures, commits an act of bankruptcy or has a Receiver or Administrator appointed, Bizneez.co.uk Ltd may immediately reclaim the goods, if necessary by entering any premises owned or occupied by the buyer<br>
                            d)  If the buyer sells the goods before title to these goods passes to him the sale shall be made by the buyer as fiduciary agent on behalf of Bizneez.co.uk Ltd. The monies due to Bizneez.co.uk Ltd for payment of goods from such sale must be kept separate to other monies and be clearly denoted as monies held in trust on behalf of Bizneez.co.uk Ltd. Regardless of such agency the buyer has no right to bind Bizneez.co.uk Ltd Ltd and shall, as between the buyer and his customers, act as principal
                        </div>
                        <h4 class="hd">7. Copyright     </h4>
                        <div class="quatation">
                            All contracts to which these conditions apply shall be construed in accordance with English law and proceedings shall take place in English Courts.
                        </div>
                        <h4 class="hd">9. No Assignment        </h4>
                        <div class="quatation">
                            The customer may not assign to a third party the benefit or burden of any contract without the consent in writing of the from Bizneez.co.uk Ltd.
                        </div>
                        <h4 class="hd">10. Limitation of Liability       </h4>
                        <div class="quatation">
                            Bizneez.co.uk Ltd’s total liability for any one claim or for the total of all claims arising under this contract shall not exceed the price of the goods and material in respect of which the claim is made.
                        </div>
                        <h4 class="hd">11. Leasing     </h4>
                        <div class="quatation">
                            Leasing terms & conditions vary on the size of the project. Subject to status and the terms of the leasing company.
                        </div>
                        <h4 class="hd">The terms set out above are given solely upon the understanding that they do not constitute any part of an offer or a contract, but are merely an invitation to treat. Subject to a one - off administration charge.    </h4>

                        <br><br><br><br><br><br><br><br><br>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="descriptionModalLong" tabindex="-1" role="dialog" aria-labelledby="descriptionModalLongTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog modal-lg" role="document" style="min-width: 1150px;">
            <div class="modal-content">




                <div class="modal-header">
                    <h5 class="modal-title" id="descriptionModalLongTitle">
                        Price List
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="col-lg-12">
                        <table class="table dt-responsive" id="example">
                            <thead>

                            <tr>

                                <th>Code </th>
                                <th>Product </th>
                                <th>Category </th>
                                <th>Range </th>
                                <th>Price </th>
                                <th>Length </th>
                                <th>Width </th>
                                <th>Height </th>
                                <th>Depth </th>
                                <th>Action </th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($productsmdd as $product)
                                <tr>

                                    <td>{{ $product->COL2 }}</td>

                                    <td>{{ $product->COL7 }}</td>
                                    <td>{{ $product->COL4 }}</td>
                                    <td>{{ $product->COL5 }} ({{ $product->COL6 }})</td>
                                    <td><span>&#163;</span>{{ str_replace("?","",$product->COL8)   }}</td>
                                    <td><span></span>{{ $product->COL10 }}</td>
                                    <td><span></span>{{ $product->COL11 }}</td>
                                    <td><span></span>{{ $product->COL12 }}</td>
                                    <td><span></span>{{ $product->COL13 }}</td>
                                    <td>
                                        <button onclick="selectPriceList('{{json_encode($product)}}')" class="btn btn-success btn-sm">Select</button>
                                    </td>

                                </tr>
                            @endforeach
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>



    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>

    <script>
        var i=0;
        let selectedRow = {{count($products)}};
        function appendFieldsR() {
            i = i+1
            console.log('clicked')
//$('#company_keywords').val()='fefe';
            var html_input =
                `  <div id="r`+i+`" class="row">

                    <div class="col-sm-1">
                    <div class="form-group">
                    <label for="location_id">Image Upload</label>
                    <button id="imageButton-`+i+`" onclick="showModalImageUpload(`+i+`)" type="button"  style=" height: 35px; margin-top: 2px;border-radius: 10px; background-color: #a08bff;" class="btn btn-success valid align-baseline"><i class="fa fa-picture-o"></i> </button>
                    <input style="color: #f3f6f8;" type="hidden" name="file`+i+`" id="file`+i+`"/>
                    <a style="display:none;" id="fileClose`+i+`" href="javascript:void(0)" class="file-close">
                        <i class="mdi mdi-close-circle"></i>
                    </a>
                    <img id="image_show`+i+`" width="50px">
                    </div>
                    <div id="imgThumbnailPreview`+i+`" style="width: 300px;"></div>
                    <input type="hidden" name="title[]" id="title`+i+`" >
                    <input type="hidden" name="keywords[]" id="keywords`+i+`" >
                    <input type="hidden" name="image_description[]" id="image_description`+i+`" >
                    </div>
                    <div class="col-sm-2">
                    <div class="form-group">
                    <label for="location_id">Description</label>
                    <div class="row">
                    <input style="width: 150px;" required type="text" class="form-control valid" name="description[]" id="description`+i+`"  />
                    <button onclick="showModalSelectRow(`+i+`)" type="button" class="btn btn-sm btn-small btn-primary" data-toggle="modal" data-target="#descriptionModalLong">
                    <i class="fa fa-pie-chart"></i>
                    </button>
                    </div>
                    </div>
                    </div>
                    <div class="col-sm-1">
                    <div class="form-group">
                    <label for="area_id">Qty </label>
                    <input required type="text" class="form-control valid" name="qty[]" id="qty`+i+`" onchange="getValues(`+i+`)" />
                    </div>
                    </div>
                    <div class="col-sm-1">
                    <div class="form-group">
                    <label for="area_id">Price </label>
                    <input required type="text" class="form-control valid" name="price[]" id="price`+i+`" onchange="getValues(`+i+`)" />
                    </div>
                    </div>
                    <div class="col-sm-1">
                    <div class="form-group">
                    <label for="area_id">Discount(%) </label>
                    <input required type="text" class="form-control valid" name="percentage[]" id="percentage`+i+`" value="40" onchange="getValues(`+i+`)"/>
                    </div>
                    </div>



                    <div class="col-sm-1">
                    <div class="form-group">
                    <label for="area_id">Disct Price </label>
                    <input required type="text" class="form-control valid" name="discount_price[]" id="discount_price`+i+`" onchange="getValues(`+i+`)" />
                    </div>
                    </div>
                    <div class="col-sm-1">
                    <div class="form-group">
                    <label for="area_id">Total </label>
                    <input required type="text" class="form-control valid" name="total[]" id="total`+i+`" onchange="getValues(`+i+`)" />

                    </div>
                    </div>
                    <div class="col-sm-1">
                    <div class="form-group">
                    <button onclick="removeFieldR(`+i+`)" style="border-radius: 10px; margin-top: 35px;" class="btn btn-danger valid align-baseline"><i class="fa fa-minus"></i> </button>
                    </div>
                    </div>

                    </div>
                    </div> `
            $('#products').append(html_input)
        }
        function removeFieldR(id) {
            $('#r'+id).remove();
        }
        const CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        function addToCart(id) {
            $.ajax({
                type:'POST',
                url:"{{ route('add.to.cart') }}",
                data:{_token: CSRF_TOKEN, id:id},
                success:function(response){
                    $('#processNext').css('display', 'block')
                    toastr.success(response.quantity + ' Product Added Successfully')
                }
            });
        }
        function getValues(id) {
            let qty = document.querySelector('#qty'+id).value
            let price = document.querySelector('#price'+id).value
            let discount_price = document.querySelector('#discount_price'+id).value
            let percentage = document.querySelector('#percentage'+id).value
            let total = document.querySelector('#total'+id).value
            document.querySelector('#qty'+id).value = qty
            document.querySelector('#price'+id).value = price
            document.querySelector('#discount_price'+id).value = price - (price/100*percentage)
            document.querySelector('#percentage'+id).value = percentage
            document.querySelector('#total'+id).value = Math.round((price - (price/100*percentage)) * qty);
            let sub_total = 0.00;
            for (k = 0; k <= i; k++) {
                let qty = Math.round(parseFloat(document.querySelector('#qty'+k).value));
                let price = Math.round(parseFloat(document.querySelector('#price'+k).value));
                let discount_price = Math.round(parseFloat(document.querySelector('#discount_price'+k).value));
                let percentage = Math.round(parseFloat(document.querySelector('#percentage'+k).value));
                let total = Math.round(parseFloat(document.querySelector('#total'+k).value));
                console.log(total)
                sub_total += total
            }
            console.log(sub_total, document.getElementById('#sub_total'))
            let vat = parseFloat(document.getElementById('vat').value);

            var vat_value =  (parseFloat(sub_total) / 100 * vat) ;//alert(vat_value);
            document.getElementById('vat_value').innerHTML = vat_value;
            $('#vat_value_hid').val(vat_value);

            document.getElementById('gross_total').innerHTML = (parseFloat(sub_total) / 100 * vat) + parseFloat(sub_total)
            document.getElementById('sub_total').innerHTML = Math.round(parseFloat(sub_total));
        }
        /*function imageUpload() {
            console.log('hello world 3')
//Check File API support
            if(window.File && window.FileList && window.FileReader)
            {
                var filesInput = document.getElementById("file"+i);
                let files = (filesInput.files)
                console.log('changes')
                var output = document.getElementById("imgThumbnailPreview"+i);
                for(var k = 0; k< files.length; k++)
                {
                    var file = files[k];
//Only pics
                    if(!file.type.match('image'))
                        continue;
                    var picReader = new FileReader();
                    picReader.addEventListener("load",function(event){
                        var picSrc = event.target.result;
                        var imgThumbnailElem = "<div id="+i+" class='dragable_card'><div class='IMGthumbnail' ><img width='100px' src='" + picSrc + "'" +
                            "title='"+file.name + "'/><span onclick='removeobject(selectIncrement)'><i class='fa fa-close'/></span>";
                        output.innerHTML = output.innerHTML + imgThumbnailElem;
                        $(".dragable_card").draggable();
                    });
//Read the image
                    picReader.readAsDataURL(file);
                }
            }
            else
            {
                alert("Your browser does not support File API");
            }
        }*/
        $(".checkbox").on("click", function() {
            if ($('input.checkbox').is(':checked')) {
                $('.term_condition').show();
            }
            else
            {
                $('.term_condition').hide();
            }
        });

        function manufacturerChange(){
            let sec = document.getElementById('maufacturer-images')
            let id = document.getElementById('manufacturer').value
            axios.post(APP_URL+'/getImagesBymanufacturer', {id: id}).then(response => {
                console.log(response.data)
                if (response.data.type === 'success') {
                    response.data.ranges.forEach(function (range) {
                        console.log(range)
                        let ele =
                            `<option value="`+range.id+`">`+range.name+`</option>`
                        $('#ranges').append(ele)
                    })
                }
            });
        }
        function rangeChange(){
            let sec = document.getElementById('maufacturer-images')
            let id = document.getElementById('ranges').value
            axios.post(APP_URL+'/getImagesByRange', {id: id}).then(response => {
                console.log(response.data)
                if (response.data.type === 'success') {
                    $('#range-images').empty()
                    response.data.images.forEach(function (image) {
                        let ele =  `
                            <div class="col-lg-3 col-xl-2">
                                <div class="file-man-box">

                                    <div class="file-img-box" >
                                        <img   src="`+APP_URL+`/`+image.path+`" alt="icon">
                                    </div>
                                    <a onclick="showSelectedImage('`+image.path+`')" href="javascript:void(0)" class="file-download">
                                        <i id="`+image.id+`" class="mdi mdi-check "></i>
                                    </a>
                                    <div class="file-man-title">
                                    </div>
                                </div>
                            </div>`
                        $('#range-images').append(ele)
                    })
                }
            });
        }
        function showSelectedImage(image) {
            console.log(image, selectedRow)
            $('#image_show'+selectedRow).attr('src', APP_URL+'/'+image)
            $('#image_show'+selectedRow).css('display', 'block')
            $('#fileClose'+selectedRow).css('display', 'block')
            $('#fileClose'+selectedRow).attr('onclick', 'removeImage('+selectedRow+')')
            $('#file'+selectedRow).val(image)
            $('#exampleModal').modal('toggle')
            document.getElementById("imageButton-"+selectedRow).style.display = 'none'


            let keyword = $('#keywords').val()
            let title = $('#title').val()
            let image_description = $('#image_description').val()

            $('#keywords'+selectedRow).val(keyword)
            $('#title'+selectedRow).val(title)
            $('#image_description'+selectedRow).val(image_description)
        }
        function removeImage(id) {
            document.getElementById("imageButton-"+id).style.display = 'block'
            $('#image_show'+id).css('display', 'none')
            $('#fileClose'+id).css('display', 'none')
            // $('#fileClose'+id).remove()
        }

        function showModalImageUpload(id) {
            selectedRow = id
            $('#exampleModal').modal('show')

        }

        function get_price(e){
            $('#price0').val(e.value);
        }
        @php

            $ele1 = App\SiteSetting::where('key', 1)->first()->data ?? 'COL8';
            $ele2 = App\SiteSetting::where('key', 2)->first()->data ?? 'COL1';
            $ele3 = App\SiteSetting::where('key', 3)->first()->data ?? 'COL2';
            $ele5 = App\SiteSetting::where('key', 5)->first()->data ?? 'COL3';
            $ele6 = App\SiteSetting::where('key', 6)->first()->data ?? 'COL4';
            $ele7 = App\SiteSetting::where('key', 7)->first()->data ?? 'COL5';
            $ele4 = App\SiteSetting::where('key', 4)->first()->data ?? 'COL6';
            $ele8 = App\SiteSetting::where('key', 8)->first()->data ?? 'COL7';
        @endphp

        $("#description0").change(function() {
            var description = this.value;
            var tabId = description.split(" ").pop();
            $('#price0').val(tabId);

        });



        $(".manufactures").change(function() {
            var url_call = "{!! url('/') !!}/admin/quotation/get_ranges";

            $.ajax({
                type : 'get',
                url  : url_call,
                data: { manufactures:$('.manufactures').val() },
                contentType: 'application/json; charset=utf-8',
                success :  function(data){
                    $('#ranges_div').empty();
                    $('#ranges_div').html(data);
                }
            });//ajax

        })

        function selectPriceList(data) {
            let record = JSON.parse(data)
            let description = record.COL5
            let product_code = record.COL2
            let price = record.COL8
            price = price.replace('?','')
            price = price.replace(',','')

            document.getElementById('description'+selectedRow).value = product_code + '  ' + description
            document.getElementById('price'+selectedRow).value = parseFloat(price)

            $('#descriptionModalLong').modal('toggle')
        }

        function showModalSelectRow(id) {
            selectedRow = id
        }

    </script>
@endsection
<style>
    #submit{display: none;}

    .hd{    background: #708398;
        width: fit-content;
        padding: 5px; font-size: 13px;
        color: #fff;}
    .top-logo{
        width: 290px;
        height: 75px;
        padding: 10px;
        margin-left: 16px;
    }
    .bg-gray{
        background-color: #708398; color: #fff;
    }
    .editable{ background:#EAEAEA}
    .img-css{    width: 105px;
        height: 102px;}
    .term_condition{display: none;}
    #vat{height: 25px;width: 32px;margin-left: 6px;}
    .side-menu .topbar-left .logo{padding-top: 23px;}
</style>