<link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->
@php
    $header_logo = \App\SiteSetting::where('key', 'header_logo')->first()->data;
@endphp
<style>
    #invoice{
        padding: 30px;
    }

    .invoice {
        position: relative;
        background-color: #FFF;
        min-height: 680px;
        padding: 15px
    }

    .invoice header {
        padding: 10px 0;
        margin-bottom: 20px;
        border-bottom: 1px solid #292929
    }

    .invoice .company-details {
        text-align: right
    }

    .invoice .company-details .name {
        margin-top: 0;
        margin-bottom: 0
    }

    .invoice .contacts {
        margin-bottom: 20px
    }

    .invoice .invoice-to {
        text-align: left
    }

    .invoice .invoice-to .to {
        margin-top: 0;
        margin-bottom: 0
    }

    .invoice .invoice-details {
        text-align: right
    }

    .invoice .invoice-details .invoice-id {
        margin-top: 0;
        color: #1f1e1e
    }

    .invoice main {
        padding-bottom: 50px
    }

    .invoice main .thanks {
        margin-top: -100px;
        font-size: 2em;
        margin-bottom: 50px
    }

    .invoice main .notices {
        padding-left: 6px;
        border-left: 6px solid #131212
    }

    .invoice main .notices .notice {
        font-size: 1.2em
    }

    .invoice table {
        width: 100%;
        border-collapse: collapse;
        border-spacing: 0;
        margin-bottom: 20px
    }

    .invoice table td,.invoice table th {
        padding: 15px;
        background: #eee;
        border-bottom: 1px solid #fff
    }

    .invoice table th {
        white-space: nowrap;
        font-weight: 400;
        font-size: 16px
    }

    .invoice table td h3 {
        margin: 0;
        font-weight: 400;
        color: #1d1c1c;
        font-size: 1.2em
    }

    .invoice table .qty,.invoice table .total,.invoice table .unit {
        text-align: right;
        font-size: 1.2em
    }

    .invoice table .no {
        color: #fff;
        font-size: 1.6em;
        background: #1d1e1f
    }

    .invoice table .unit {
        background: #ddd
    }

    .invoice table .total {
        background: #1d1c1c;
        color: #fff
    }

    .invoice table tbody tr:last-child td {
        border: none
    }

    .invoice table tfoot td {
        background: 0 0;
        border-bottom: none;
        white-space: nowrap;
        text-align: right;
        padding: 10px 20px;
        font-size: 1.2em;
        border-top: 1px solid #aaa
    }

    .invoice table tfoot tr:first-child td {
        border-top: none
    }

    .invoice table tfoot tr:last-child td {
        color: #1e1f1f;
        font-size: 1.4em;
        border-top: 1px solid #141415
    }

    .invoice table tfoot tr td:first-child {
        border: none
    }

    .invoice footer {
        width: 100%;
        text-align: center;
        color: #777;
        border-top: 1px solid #aaa;
        padding: 8px 0
    }

    @media print {
        .invoice {
            font-size: 11px!important;
            overflow: hidden!important
        }

        .invoice footer {
            position: absolute;
            bottom: 10px;
            page-break-after: always
        }

        .invoice>div:last-child {
            page-break-before: always
        }
    }
</style>

<div id="invoice">

    <div class="toolbar hidden-print">
        <div class="text-right">
            <button id="printInvoice" class="btn btn-info"><i class="fa fa-print"></i> Print</button>
{{--            <button class="btn btn-info"><i class="fa fa-file-pdf-o"></i> Export as PDF</button>--}}
        </div>
        <hr>
    </div>
    <div class="invoice overflow-auto">
        <div style="min-width: 600px">
            <header>
                <div class="row">
                    <div class="col">
                        <a target="_blank" href="{{ route('dashboard', ['role' => \Illuminate\Support\Facades\Auth::user()->roles[0]->name]) }}">
                            <img src="{{asset($header_logo)}}" width="200"/>
                        </a>
                    </div>
{{--                    <div class="col company-details">--}}
{{--                        <h2 class="name">--}}
{{--                            <a target="_blank" href="https://lobianijs.com">--}}
{{--                                Arboshiki--}}
{{--                            </a>--}}
{{--                        </h2>--}}
{{--                        <div>455 Foggy Heights, AZ 85004, US</div>--}}
{{--                        <div>(123) 456-789</div>--}}
{{--                        <div>company@example.com</div>--}}
{{--                    </div>--}}
                </div>
            </header>
            <main>
                <div class="row contacts">
                    <div class="col invoice-to">
                        <div class="text-gray-light">INVOICE TO:</div>
                        <h2 class="to">{{ $order->user->name }}</h2>
                        <div class="address">{{ $order->user->profile->address }}</div>
                        <div class="email"><a href="mailto:{{$order->user->email}}">{{$order->user->email}}</a></div>
                    </div>
{{--                    <div class="col invoice-details">--}}
{{--                        <h1 class="invoice-id">INVOICE 3-2-1</h1>--}}
{{--                        <div class="date">Date of Invoice: 01/10/2018</div>--}}
{{--                        <div class="date">Due Date: 30/10/2018</div>--}}
{{--                    </div>--}}
                </div>
                <table border="0" cellspacing="0" cellpadding="0">
                    <thead>
                    <tr>
                        <th>Quantity</th>
                        <th class="text-left">Products</th>
                        <th class="text-right">Price</th>
                        <th class="text-right">Total</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($order->products as $product)
                    <tr>
                        <td class="no">{{ $product->pivot->quantity }}</td>
                        <td class="text-left">
                            {{$product->name}}
                        </td>
                        <td class="unit"><span>&#163;</span>{{ $product->price }}</td>
                        <td class="total"><span>&#163;</span>{{ $product->pivot->price * $product->pivot->quantity }}</td>
                    </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="2"></td>
                        <td colspan="2">SUBTOTAL</td>
                        <td>{{$total}}</td>
                    </tr>
                    <tr>
                        <td colspan="2"></td>
{{--                        <td colspan="2">TAX 25%</td>--}}
{{--                        <td>$1,300.00</td>--}}
                    </tr>
                    <tr>
                        <td colspan="2"></td>
                        <td colspan="2">GRAND TOTAL</td>
                        <td>{{$total}}</td>
                    </tr>
                    </tfoot>
                </table>
                <div class="thanks">Thank you!</div>
{{--                <div class="notices">--}}
{{--                    <div>NOTICE:</div>--}}
{{--                    <div class="notice">A finance charge of 1.5% will be made on unpaid balances after 30 days.</div>--}}
{{--                </div>--}}
            </main>
            <footer>
                Invoice was created on a computer and is valid without the signature and seal.
            </footer>
        </div>
        <!--DO NOT DELETE THIS div. IT is responsible for showing footer always at the bottom-->
        <div></div>
    </div>
</div>


<script>
    $('#printInvoice').click(function(){
        Popup($('.invoice')[0].outerHTML);
        function Popup(data)
        {
            window.print();
            return true;
        }
    });
</script>