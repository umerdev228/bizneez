<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class PaymentHistory extends Model {
    protected $table = 'payment_histories';

    protected $fillable = [  
          'user_id' , 'amount' , 'transaction_id' , 'package_id', 'expire_date',
    ]; 

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function packages() {
        return $this->belongsTo('App\Packages', 'package_id', 'id');
   }

   public function packagesCondition() {
        return $this->belongsTo('App\Packages', 'package_id', 'id')->where('status',1);
   }
 
}
