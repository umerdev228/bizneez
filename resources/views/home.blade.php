@extends('layout.app')

<style>
    h1, h2, h3, h4, h5, h6 {
        margin: 10px 0;
        font-family: inherit;
        font-weight: bold;
        line-height: 20px;
        color: inherit;
        text-rendering: optimizelegibility;
    }
    h1, h2, h3 {
        line-height: 40px;
    }
    h3 {
        display: block;
        font-size: 1.17em;
        margin-block-start: 1em;
        margin-block-end: 1em;
        margin-inline-start: 0px;
        margin-inline-end: 0px;
        font-weight: bold;
    }

</style>
@section('content')
<main>
    <!-- slider Area Start-->
    <div class="support-company-area p-5 fix gray-bg">
        <div class="container">
            <div id="span10">
                <div id="asw-login" class="asw-module">
                    <div class="asw-module-inner clearfix">
                        <form id="asw-login-form" name="login" method="POST" action="{{url('login')}}">
                           @csrf
                            <h2 class="cfn">
                                Login
                            </h2>
                            <a href="{{url('/')}}/forgot" id="forgot-password" onclick="document.getElementById('forgotPassword').style.display='inline';">Forgot Username / Password?</a>
                            <div class="field">
                                <label for="username">Email</label>
                                <input id="email" name="email" class="required input-text" tabindex="1" type="email">
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                          </span>
                                @enderror
                            </div>
                            <div class="field">
                                <label for="password">Password</label>
                                <input id="password" name="password" class=" required input-text" tabindex="2" type="password">
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                          </span>
                                @enderror
                            </div>

                            <div class="field">
                                <input id="long_lived" name="remember" type="checkbox">
                                <label for="long_lived" style="width:auto; font-weight: normal; font-size:11px; vertical-align:middle;padding-top:8px;">
                                    Keep me logged in on this computer
                                </label>
                            </div>
                            <div style="text-align:right;width:330px;"><button class="btn" type="submit" style="color:#000;">Login</button></div>
                            <input name="rurl" value="" type="hidden">



                        </form>

                        <div class="aside" style="text-align:center;width:auto;">


                            <p class="special_font">Welcome to OrangeBerry Design Web Portal.</p><br>

{{--                                <div class="row">--}}
{{--                                    <label for="telephone" style="color:#FFF;">Signup by selecting your Sector</label> <br>--}}

{{--                                    <select name="sector"><option value="architects">Architects</option>--}}
{{--                                        <option value="Procurement">Procurement</option>--}}
{{--                                        <option value="Project Finance">Project Finance</option>--}}
{{--                                        <option value="Projects">Projects</option>--}}
{{--                                        <option value="Manufacturers">Manufacturers</option>--}}
{{--                                        <option value="other">Other</option>--}}
{{--                                    </select>--}}

{{--                                    <a href="signup_manufacturers.php" class="btn sign-up">Sign Up</a>--}}

{{--                                </div>--}}
                            <!--<a href="signup.php?type=1">Signup for Employee</a> <br />
                             <a href="signup_manufacturers.php">Signup/Login for Manufacturers</a>-->
                        </div>
                    </div>
                </div>
                <br>
                <br>
                <ul class="thumbnails home-thumb">

                    <li class="span4">
                        <div class="thumbnail">
                            <img src="{{asset('assets/images/architect-thumb.jpg')}}" alt="Architects" class="img-rounded">
                            <h3><a href="javascript:void(0)">Architects</a></h3>
                        </div>
                    </li>



                    <li class="span4">
                        <div class="thumbnail">
                            <img src="{{asset('assets/images/manufacturer-thumb.jpg')}}" alt="Manufactuers" class="img-rounded">
                            <h3><a href="javascript:void(0)">Manufacturers</a> </h3>
                        </div>

                    </li>
                    <li class="span4">
                        <div class="thumbnail">
                            <img src="{{asset('assets/images/procurement-thumb.jpg')}}" alt="Facilites" class="img-rounded">
                            <h3><a href="javascript:void(0)">Procurement</a></h3>
                        </div>
                    </li>


                    <li class="span4">
                        <div class="thumbnail">
                            <img src="{{asset('assets/images/1.png')}}" alt="Architects" class="img-rounded">
                            <h3><a href="javascript:void(0)">Project Finance</a></h3>
                        </div>
                    </li>


                    <li class="span4">
                        <div class="thumbnail">
                            <img src="{{asset('assets/images/3.png')}}" alt="Facilites" class="img-rounded">
                            <h3><a href="javascript:void(0)">Projects</a></h3>
                        </div>
                    </li>
                    <li class="span4">
                        <div class="thumbnail">
                            <!--  <a href="cv.php" class="photo" style="">--><img src="{{asset('assets/images/2.png')}}" alt="Manufactuers" class="img-rounded">
                            <h3><a href="javascript:void(0)">Curriculum Vitae</a></h3>
                        </div>

                    </li>
                </ul>

            </div>
{{--                <div class="row align-items-center">--}}
{{--                    <div class="col-xl-6 col-lg-6">--}}
{{--                        <div class="right-caption">--}}
{{--                            <!-- Section Tittle -->--}}
{{--                            <div class="section-tittle section-tittle2">--}}
{{--                                <h2>Delight your customers and win them for life</h2>--}}
{{--                            </div>--}}
{{--                            <div class="support-caption">--}}
{{--                                <p class="pera-top">Looking for an Omnichannel, AI-driven or Self Service solution for customer service? We've got you covered!</p>--}}
{{--                                --}}
{{--                                </div>--}}

{{--                                <form  action="{{ url('signup') }}" class="search-box">--}}
{{--                             <!--    <div class="input-form">--}}
{{--                                <input class="form-control valid" autocomplete="off"   id="email" type="text" placeholder="Enter the E-mail" required  />--}}
{{--                                            @if ($errors->has('email'))--}}
{{--                                         <span class="error">{{ $errors->first('email') }}</span>--}}
{{--                                         @endif--}}

{{--                                </div> -->--}}
{{--                            <div  class="search-form" >--}}

{{--                        <!-- <a href="{{ url('signup')}}"  class="btn form-shadow">Sign up for free</a> -->--}}
{{--                        <input  class="btn form-shadow genric-btn info" type="submit" value="SIGN UP FOR FREE">--}}

{{--                        </div>--}}

{{--                                    </form>--}}
{{--                            </div>--}}

{{--                        </div>--}}
{{--                        <div class="col-xl-6 col-lg-6">--}}
{{--                            <div class="support-location-img">--}}
{{--                                <img src="assets/img/support.png" alt="">--}}

{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                </div>

            </div>
            <!-- slider Area End-->
        
        <!-- slider Area Start-->
        <div class="support-company-area p-5 fix">
            <div class="container">
                <div class="row align-items-center">
                    
                    <div class="col-xl-5 col-lg-6">
                        <div class="support-location-img">
                            <img src="assets/img/features.jpg" alt="">
                            
                        </div>
                    </div>
                    
                    <div class="col-xl-7 col-lg-6">
                        <div class="right-caption">
                            <!-- Section Tittle -->
                            <div class="section-tittle section-tittle2">
                                <h2>With TMS, you can…</h2>
                            </div>
                            <div class="support-caption">
                                <!-- <p class="pera-top">Looking for an Omnichannel, AI-driven or Self Service solution for customer service? We've got you covered!</p> -->
                                
                                <ul class="unordered-list">
                                    <li>Streamline all your customer conversations in one place</li>
                                    <li>Automate your repetitive work and save time</li>
                                    <li>Collaborate with other teams to resolve issues faster</li>
                                </ul>
                                
                            </div>
                            
                        </div>
                        
                    </div>
                    
                </div>
            </div>
        </div>
        <!-- slider Area End-->
        
        
         <div class="online-cv cv-bg section-overly pt-90 pb-120"  data-background="assets/img/cv_bg.jpg">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-xl-10">
                        <div class="cv-caption text-center">
                            <p class="pera2">So, what brings you here today?</p>
                            <a href="{{ url('signup')}}" class="border-btn2 border-btn4">Call to action</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    <div class="support-company-area fix pt-50 pb-50">
            <div class="container">
                
                <div class="section-tittle text-center">
                    <h2>Support, now simplified</h2>
                </div>
                
                <div class="row align-items-center">
                    <div class="col-xl-4 col-lg-6">
                        <div class="right-caption">
                            <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
              <a class="nav-link active" id="feature-tab-1" data-toggle="pill" href="#feature-1" role="tab" aria-controls="feature-1" aria-selected="true">Conversations made easier</a>
              <a class="nav-link" id="feature-tab-2" data-toggle="pill" href="#feature-2" role="tab" aria-controls="feature-2" aria-selected="false">Collaborations made stronger</a>
              <a class="nav-link" id="feature-tab-3" data-toggle="pill" href="#feature-3" role="tab" aria-controls="feature-3" aria-selected="false">Support made more automatic</a>
              <a class="nav-link" id="feature-tab-4" data-toggle="pill" href="#feature-4" role="tab" aria-controls="feature-4" aria-selected="false">Answers made available faster</a>
            <a class="nav-link" id="feature-tab-5" data-toggle="pill" href="#feature-5" role="tab" aria-controls="feature-5" aria-selected="false">Answers made available faster</a>
            </div>
                        </div>
                    </div>
                    <div class="col-xl-8 col-lg-6">
                        <div class="support-location-img">
                            <div class="tab-content" id="v-pills-tabContent">
              <div class="tab-pane fade show active" id="feature-1" role="tabpanel" aria-labelledby="feature-tab-1"><img src="assets/img/feat-1.jpg" alt=""></div>
              <div class="tab-pane fade" id="feature-2" role="tabpanel" aria-labelledby="feature-tab-2"><img src="assets/img/feat-1.jpg" alt=""></div>
              <div class="tab-pane fade" id="feature-3" role="tabpanel" aria-labelledby="feature-tab-3"><img src="assets/img/feat-1.jpg" alt=""></div>
              <div class="tab-pane fade" id="feature-4" role="tabpanel" aria-labelledby="feature-tab-4"><img src="assets/img/feat-1.jpg" alt=""></div>
              <div class="tab-pane fade" id="feature-5" role="tabpanel" aria-labelledby="feature-tab-5"><img src="assets/img/feat-1.jpg" alt=""></div>
            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        
        <!-- Our Services Start -->
        <div class="our-services p-5">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="section-tittle text-center">
                            <h2 >Browse Top Categories </h2>
                        </div>
                    </div>
                </div>
                <div class="row d-flex justify-contnet-center">
                    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                        <div class="single-services text-center mb-30">
                            <div class="services-ion">
                                <span class="flaticon-tour"></span>
                            </div>
                            <div class="services-cap">
                               <h5><a href="#">Design & Creative</a></h5>
                            
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                        <div class="single-services text-center mb-30">
                            <div class="services-ion">
                                <span class="flaticon-cms"></span>
                            </div>
                            <div class="services-cap">
                               <h5><a href="#">Design & Development</a></h5>
                              
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                        <div class="single-services text-center mb-30">
                            <div class="services-ion">
                                <span class="flaticon-report"></span>
                            </div>
                            <div class="services-cap">
                               <h5><a href="#">Sales & Marketing</a></h5>
                               
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                        <div class="single-services text-center mb-30">
                            <div class="services-ion">
                                <span class="flaticon-app"></span>
                            </div>
                            <div class="services-cap">
                               <h5><a href="#">Mobile Application</a></h5>
                               
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                        <div class="single-services text-center mb-30">
                            <div class="services-ion">
                                <span class="flaticon-helmet"></span>
                            </div>
                            <div class="services-cap">
                               <h5><a href="#">Construction</a></h5>
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                        <div class="single-services text-center mb-30">
                            <div class="services-ion">
                                <span class="flaticon-high-tech"></span>
                            </div>
                            <div class="services-cap">
                               <h5><a href="#">Information Technology</a></h5>
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                        <div class="single-services text-center mb-30">
                            <div class="services-ion">
                                <span class="flaticon-real-estate"></span>
                            </div>
                            <div class="services-cap">
                               <h5><a href="#">Real Estate</a></h5>
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                        <div class="single-services text-center mb-30">
                            <div class="services-ion">
                                <span class="flaticon-content"></span>
                            </div>
                            <div class="services-cap">
                               <h5><a href="#">Content Writer</a></h5>
                               
                            </div>
                        </div>
                    </div>
                </div>
                <!-- More Btn -->
                <!-- Section Button -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="browse-btn2 text-center mt-50">
                            <a href="{{ url('signup')}}" class="border-btn2">Browse All Sectors</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Our Services End -->
        
       
        <!-- How  Apply Process Start-->
        <div class="apply-process-area apply-bg pt-50 pb-50" data-background="assets/img/how-applybg.png">
            <div class="container">
                <!-- Section Tittle -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="section-tittle white-text text-center">
                            <h2> How it works</h2>
                        </div>
                    </div>
                </div>
                <!-- Apply Process Caption -->
                <div class="row">
                    <div class="col-lg-4 col-md-6">
                        <div class="single-process text-center mb-30">
                            <div class="process-ion">
                                <span class="flaticon-search"></span>
                            </div>
                            <div class="process-cap">
                               <h5>1. Create Account</h5>
                               <p>Bizneez ticket management system is free with full features. Just sign up, and get started.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="single-process text-center mb-30">
                            <div class="process-ion">
                                <span class="flaticon-curriculum-vitae"></span>
                            </div>
                            <div class="process-cap">
                               <h5>2. Support Customers</h5>
                               <p>Now Support unlimited customers and get all customer data from different channels in one place.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="single-process text-center mb-30">
                            <div class="process-ion">
                                <span class="flaticon-tour"></span>
                            </div>
                            <div class="process-cap">
                               <h5>3. Retain Customer</h5>
                               <p>That’s it. Resolve customer’s problems at the right time, and keep them attached to your brand.</p>
                            </div>
                        </div>
                    </div>
                </div>
             </div>
        </div>
        <!-- How  Apply Process End-->
        <!-- Testimonial Start -->
        <div class="testimonial-area testimonial-padding">
            <div class="col-lg-12">
                        <div class="section-tittle text-center">
                            <h2>Why our customers choose us</h2>
                        </div>
                    </div>
            <div class="container">
                <!-- Testimonial contents -->
                <div class="row d-flex justify-content-center">
                    <div class="col-xl-8 col-lg-8 col-md-10">
                        <div class="h1-testimonial-active dot-style">
                            
                            <!-- Single Testimonial -->
                            <div class="single-testimonial text-center">
                                <!-- Testimonial Content -->
                                <div class="testimonial-caption ">
                                    <!-- founder -->
                                    <div class="testimonial-founder  ">
                                        <div class="founder-img mb-30">
                                            <img src="assets/img/testimonial-1.png" alt="">
                                            <span>Tylor Jack</span>
                                            <p>Creative Director</p>
                                        </div>
                                    </div>
                                    <div class="testimonial-top-cap">
                                        <p>“I love Bizneez TMS, and could not imagine doing business without it. Cybexo TMS made a huge impact on our turn-around time for resolving issues.”</p>
                                    </div>
                                </div>
                            </div>
                            <!-- Single Testimonial -->
                            <div class="single-testimonial text-center">
                                <!-- Testimonial Content -->
                                <div class="testimonial-caption ">
                                    <!-- founder -->
                                    <div class="testimonial-founder  ">
                                        <div class="founder-img mb-30">
                                            <img src="assets/img/testimonial-2.png" alt="">
                                            <span>Tylor Jack</span>
                                            <p>Creative Director</p>
                                        </div>
                                    </div>
                                    <div class="testimonial-top-cap">
                                        <p>“I can prioritize my clients according to the urgency bases, not only this I can manage all my tickets in one page. Thank you Bizneez TMS.”</p>
                                    </div>
                                </div>
                            </div>
                            <!-- Single Testimonial -->
                            <div class="single-testimonial text-center">
                                <!-- Testimonial Content -->
                                <div class="testimonial-caption ">
                                    <!-- founder -->
                                    <div class="testimonial-founder  ">
                                        <div class="founder-img mb-30">
                                            <img src="assets/img/testimonial-3.png" alt="">
                                            <span>Baldwin</span>
                                            <p>Assistant Business Manager</p>
                                        </div>
                                    </div>
                                    <div class="testimonial-top-cap">
                                        <p>“I am addicted to use of Cybexo ticket management system, it is efficient and it allows unlimited ticket’s support. The best feature is its efficiency remains same, no matters even thousands of tickets are active at a time. It’s really helping tool in my business success.”</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Testimonial End -->
        
    </main>

    @endsection