<?php

namespace App\Http\Controllers;

use App\Company;
use App\EmailTemplate;
use App\Imports\EmailListImport;
use App\Media;
use App\PromotionEmail;
use App\Subscriber;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;

class  PromotionEmailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $emailTemplates = EmailTemplate::with('company')->get();
        return view('admin.bulk-email.index', compact('emailTemplates'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $emails = PromotionEmail::all();
        $images = Media::where('name', 'gallery')->get();
        $companies = Company::all();
        return view('admin.bulk-email.create', compact('emails', 'companies', 'images'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        PromotionEmail::create([
            'subject' => $request->subject,
            'body' => $request->description,
        ]);
        toastr()->success('Emails Store Successfully');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PromotionEmail  $promotionEmail
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $template = EmailTemplate::where('id', $id)->with('company')->with('media')->first();
        return view('admin.bulk-email.show', compact('template'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PromotionEmail  $promotionEmail
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $email = PromotionEmail::where('id', $id)->first();
        return view('admin.bulk-email.edit', compact('email'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PromotionEmail  $promotionEmail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $email = PromotionEmail::where('id', $id)->update([
            'subject' => $request->subject,
            'body' => $request->description,
            ]);
        toastr()->success('Emails Updated Successfully');
        return redirect()->route('admin.promotion.email.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PromotionEmail  $promotionEmail
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $email = PromotionEmail::where('id', $id)->delete();
        return redirect()->back();
    }

    public function sendBulk($id) {
        $email = PromotionEmail::where('id', $id)->first();
        $subscribers = Subscriber::all('email');

        foreach ($subscribers as $subscriber) {
            $data["email"] = $subscriber->email;
            $data["title"] = $email->subject;
            $data["body"] = $email->body;


            Mail::send('mail.custom_mail', $data, function($message)use($data) {
                $message->to($data["email"], $data["email"])
                    ->subject($data["title"]);
            });
        }
        toastr()->success('Emails Send Successfully');

        return redirect()->back();
    }

    public function sendEmail($id) {
        $email = EmailTemplate::where('id', $id)->first();
        Excel::import(new EmailListImport($email), public_path('/').$email->emails_list);
        toastr()->success('Emails Send Successfully');
        return redirect()->route('admin.promotion.email.index');
    }

    public function emailTemplate($id) {
        $template = EmailTemplate::where('id', $id)->first();
        return view('admin.bulk-email.email', compact('template'));
    }
}
