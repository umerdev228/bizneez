<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
   <!--  <meta name="viewport" content="width=device-width, initial-scale=1"> -->
   
   <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ config('app.name', 'Bizneez') }}</title>
    <meta content="Coderthemes" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- App favicon -->
    <link rel="shortcut icon" href="{{asset('/theme/admin/assets/images/favicon.png')}}">
    <!-- Tooltipster css -->
    <link rel="stylesheet" href="{{ URL::asset('/theme/admin/plugins/tooltipster/tooltipster.bundle.min.css') }}">
    <link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel = "stylesheet">

    <!-- Plugins css-->
    <link href="{{asset('/theme/admin/plugins/bootstrap-tagsinput/css/bootstrap-tagsinput.css')}}" rel="stylesheet" />
    <link href="{{asset('/theme/admin/plugins/bootstrap-select/css/bootstrap-select.min.css')}}" rel="stylesheet" />
    <link href="{{asset('/theme/admin/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{asset('/theme/admin/plugins/switchery/switchery.min.css')}}" />
    <link href="{{asset('/theme/admin/plugins/footable/css/footable.core.css')}}" rel="stylesheet">

    <!-- App css -->
    <link href="{{asset('/theme/admin/assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('/theme/admin/assets/css/icons.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('/theme/admin/assets/css/metismenu.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('/theme/admin/assets/css/style.css')}}" rel="stylesheet" type="text/css" />
    <!-- Summernote css -->
    <link href="{{asset('/theme/admin/plugins/summernote/summernote-bs4.css')}}" rel="stylesheet" />

    <style>
        .side-menu {
            z-index: 999999;
        }
    </style>

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.css">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <script>var APP_URL = '{!! url('/') !!}';</script>
    @toastr_css
    <script src="{{asset('/theme/admin/assets/js/modernizr.min.js')}}"></script>
 <script>
 (function () {
   var e,i=["https://fastbase.com/fscript.js","5fvHK0txe7","script"],a=document,s=a.createElement(i[2]);
   s.async=!0,s.id=i[1],s.src=i[0],(e=a.getElementsByTagName(i[2])[0]).parentNode.insertBefore(s,e)
 })();
</script>

</head>
<body>

<div id="wrapper">
    @include('admin.layouts.left-sidebar')
    <div class="content-page">
        @include('admin.layouts.topbar')
        @yield('content')
{{--        @include('admin.layouts.footer')--}}
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="{{asset('/assets/js/jquery-ui.js')}}"></script>

<script src="{{asset('/theme/admin/assets/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('/theme/admin/assets/js/metisMenu.min.js')}}"></script>
<script src="{{asset('/theme/admin/assets/js/waves.js')}}"></script>
<script src="{{asset('/theme/admin/assets/js/jquery.slimscroll.js')}}"></script>

<!-- Flot chart -->
<script src="{{asset('theme/admin/plugins/flot-chart/jquery.flot.min.js')}}"></script>
<script src="{{asset('theme/admin/plugins/flot-chart/jquery.flot.time.js')}}"></script>
<script src="{{asset('theme/admin/plugins/flot-chart/jquery.flot.tooltip.min.js')}}"></script>
<script src="{{asset('theme/admin/plugins/flot-chart/jquery.flot.resize.js')}}"></script>
<script src="{{asset('theme/admin/plugins/flot-chart/jquery.flot.pie.js')}}"></script>
<script src="{{asset('theme/admin/plugins/flot-chart/jquery.flot.crosshair.js')}}"></script>
<script src="{{asset('theme/admin/plugins/flot-chart/curvedLines.js')}}"></script>
<script src="{{asset('theme/admin/plugins/flot-chart/jquery.flot.axislabels.js')}}"></script>
<script type="text/javascript" src="{{asset('theme/admin/plugins/jquery-knob/excanvas.js')}}"></script>
<script src="{{asset('theme/admin/plugins/jquery-knob/jquery.knob.js')}}"></script>

<script src="{{asset('theme/admin/assets/pages/jquery.dashboard.init.js')}}"></script>

<script src="{{asset('/theme/admin/plugins/footable/js/footable.all.min.js')}}"></script>
<script src="{{asset('/theme/admin/plugins/switchery/switchery.min.js')}}"></script>
<script src="{{asset('/theme/admin/plugins/bootstrap-tagsinput/js/bootstrap-tagsinput.min.js')}}"></script>
<script src="{{asset('/theme/admin/plugins/select2/js/select2.min.js')}}" type="text/javascript"></script>
<script src="{{asset('/theme/admin/plugins/bootstrap-select/js/bootstrap-select.js')}}" type="text/javascript"></script>
<script src="{{asset('/theme/admin/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js')}}" type="text/javascript"></script>
<script src="{{asset('/theme/admin/plugins/bootstrap-maxlength/bootstrap-maxlength.js')}}" type="text/javascript"></script>
{{--<script type="text/javascript" src="{{asset('/theme/admin/plugins/autocomplete/jquery.autocomplete.min.js')}}"></script>--}}
{{--<script type="text/javascript" src="{{asset('/theme/admin/plugins/autocomplete/countries.js')}}"></script>--}}
{{--<script type="text/javascript" src="{{asset('/theme/admin/assets/pages/jquery.autocomplete.init.js')}}"></script>--}}
{{--<script type="text/javascript" src="{{asset('/theme/admin/assets/pages/jquery.form-advanced.init.js')}}"></script>--}}
<script src="{{asset('/theme/admin/assets/js/jquery.core.js')}}"></script>
<script src="{{asset('/theme/admin/assets/js/jquery.app.js')}}"></script>
{{--    <script src="{{asset('/theme/admin/plugins/autocomplete/jquery.mockjax.js')}}"></script>--}}
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />

<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="{{asset('/theme/admin/plugins/summernote/summernote-bs4.min.js')}}"></script>


@toastr_js
@toastr_render
<!--Summernote js-->

<script>
    jQuery(document).ready(function(){
        $('.summernote').summernote({
            height: 350,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            focus: false                 // set focus to editable area after initializing summernote
        });

        $('.inline-editor').summernote({
            airMode: true
        });
    });
</script>
<script>
    var normal_table = $('#normal-table').DataTable({
        "order": [[ 0, "desc" ]]
    });
    var table = $('#example').DataTable();
    var table = $('#example2').DataTable();
</script>
</body>
</html>
