@extends('layout.app')


@section('content')

 
     <main>

        <!-- slider Area Start-->
        <div class="support-company-area p-5 fix gray-bg">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-xl-7 col-lg-7">
                        <div class="right-caption">
                            <!-- Section Tittle -->
                            <div class="section-tittle section-tittle2">
                                <h2>Power of Collaborative Ticketing Including AI, Delivers the Best Customer Experience.</h2>
                            </div>
                            <div class="support-caption">
                                <p class="pera-top">Bizneez offers automatic lead distribution, time tracking, durability, scalability, reminders, and notifications. </p>
                            </div>
                            <a href="{{ url('signup')}}" class="genric-btn info text-uppercase ">Sign up for free</a>
                            <!-- yah per change hona hai -->
                        </div>
                        
                    </div>
                    <div class="col-xl-5 col-lg-5">
                        <div class="support-location-img">
                            <img src="theme/assets/img/ticketing.png" alt="">
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- slider Area End-->
        
        
        <div class="support-company-area fix pt-50 pb-50">
            <div class="container">
                
                <div class="section-tittle text-center">
                    <h2>Get the Most Popular Ticketing System!</h2>
                    <div class="support-caption">
                        <p class="font-weight-bold">Now convert your customer issues from any channel into Ticket Management System. Stay on top and take immediate action based on the priority.</p>
                    </div>
                </div>
                
                <div align="center">
                    <img src="theme/assets/img/feat-2.png" class="img-fluid" alt=""  width="800" height="533" > 
                </div>
                
            </div>
            
        </div>
        
        <div class="support-company-area p-5 fix gray-bg">
            <div class="container">
                <div class="row align-items-center">
                    <h2>Cybexo TMS</h2>
                    <p>Bizneez ticket management system is designed for team collaboration to resolve issues efficiently and productivity of agents. It’s not only for sending replies but also for control access, visual analytics, real time monitoring and updates, status tracking, service level management, etc. In Cybexo TMS ticket filtration based on specific properties, so the agent can handle most important ticket from front and center. </p>
                    
                    
                    <div class="our-services p-5">
                     <div class="container">
                
                    <div class="row d-flex justify-contnet-center">
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6">
                        <div class="single-services text-center mb-30">
                            <div class="services-ion">
                                <span class="flaticon-tour"></span>
                            </div>
                            <div class="services-cap">
                               <h5><a href="#">Collision detection</a></h5>
                                <p>Updated with customer issues and team replies. </p>


                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6">
                        <div class="single-services text-center mb-30">
                            <div class="services-ion">
                                <span class="flaticon-cms"></span>
                            </div>
                            <div class="services-cap">
                               <h5><a href="#">Canned responses</a></h5>
                                <p>Save replies to common tickets and reuse them.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6">
                        <div class="single-services text-center mb-30">
                            <div class="services-ion">
                                <span class="flaticon-report"></span>
                            </div>
                            <div class="services-cap">
                               <h5><a href="#">Smart notifications</a></h5>
                                <p>Get all notifications within the tool.</p>

                            </div>
                        </div>
                    </div>
                    
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6">
                        <div class="single-services text-center mb-30">
                            <div class="services-ion">
                                <span class="flaticon-helmet"></span>
                            </div>
                            <div class="services-cap">
                               <h5><a href="#">Custom ticket views</a></h5>
                                <p>Choose the tickets on priority Bases.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6">
                        <div class="single-services text-center mb-30">
                            <div class="services-ion">
                                <span class="flaticon-high-tech"></span>
                            </div>
                            <div class="services-cap">
                               <h5><a href="#">Merging tickets</a></h5>
                               <p>Merge tickets same issues into one.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6">
                        <div class="single-services text-center mb-30">
                            <div class="services-ion">
                                <span class="flaticon-real-estate"></span>
                            </div>
                            <div class="services-cap">
                               <h5><a href="#">Activity log</a></h5>
                                <p>View every agent and system activity on the ticket.</p>
                            </div>
                        </div>
                    </div>
                   
                </div>
                <!-- More Btn -->
              
            </div>
        </div>
                    
                </div>
            </div>
        </div>
    
        
        <!-- slider Area Start-->
        <div class="support-company-area p-5 fix">
            <div class="container">
                <div class="row align-items-center">
                    
                    <div class="col-xl-6 col-lg-6">
                        <div class="right-caption">
                            <!-- Section Tittle -->
                            <div class="section-tittle section-tittle2">
                                <h2>Collaborative ticketing</h2>
                            </div>
                            <div class="support-caption">
                                <p class="pera-top">Efficient work, Better support experience, Bizneez Ticket Management System. We manage your support Tickets Effectively.</p>
                                
                                <ul class="unordered-list">
                                    <li>Put customer data across different channels and mange support effortlessly.</li>
                                    <li>Add private notes and forward replies to another agent for the better ticket management.</li>
                                    <li>Cybexo Ticket management system benefits are Employee satisfaction, efficient ticket resolution, ticket prioritization, agent productivity, and customer retention. </li>
                                </ul>
                                
                            </div>
                            
                        </div>
                        
                    </div>
                    
                    <div class="col-xl-6 col-lg-6">
                        <div class="support-location-img">
                            <img src="theme/assets/img/feat-3.png" alt="">
                            
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        <!-- slider Area End -->
        <section class="pricing py-5" id="price">
        <div class="container">
            <br>
            <br>
            <div class="col-lg-12">
                <div class="section-tittle text-center">
                    <h2 class="text-white">Our Package</h2>
                </div>
            </div>
            <div class="row d-flex justify-content-center" >
                @foreach ($packages as $pack)
                <div class="col-lg-4 " >
                    <div class="card mb-5 mb-lg-0" >
                        <div class="card-body"  >
                            <h5 class="card-title text-muted text-uppercase text-center">{{$pack->name}}</h5>
                            <h6 class="card-price text-center">${{$pack->amount}}
                            <span class="period" >{{$pack->package_duration}}</span></h6>
                            <hr>
                            <p >{!! $pack->description !!}</p>
                             <ul class="fa-ul">

                                @foreach($option as $opt)
                                @php  
                                $options = DB::table('pacakge_relation_options')->select('*')->where('package_id', $pack->id)->where('option_id',$opt->id)->get();  
                                @endphp 
                                @if(count($options)>0)
                                 
                                <li><span class="fa-li" ><i class="fas fa-check"></i></span>{{$opt->option_name}}</li>      
                                 
                                 @else
                                <li><span class="fa-li" ><i class="fa fa-times"></i></span>{{$opt->option_name}}</li>      

                                 @endif
                              
                                 @endforeach
                            </ul>   
                        </div>
                    </div>
                    &nbsp;
                </div>
                @endforeach
            </div>
        </div>
    </section>
    </main>
    @endsection
   