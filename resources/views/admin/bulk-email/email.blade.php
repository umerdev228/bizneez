<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ config('app.name', 'Bizneez') }}</title>
    <meta content="Coderthemes" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- App favicon -->
    <link rel="shortcut icon" href="{{asset('/theme/admin/assets/images/favicon.png')}}">
    <!-- Tooltipster css -->
    <link rel="stylesheet" href="{{ URL::asset('/theme/admin/plugins/tooltipster/tooltipster.bundle.min.css') }}">
    <link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel = "stylesheet">

    <!-- Plugins css-->
    <link href="{{asset('/theme/admin/plugins/bootstrap-tagsinput/css/bootstrap-tagsinput.css')}}" rel="stylesheet" />
    <link href="{{asset('/theme/admin/plugins/bootstrap-select/css/bootstrap-select.min.css')}}" rel="stylesheet" />
    <link href="{{asset('/theme/admin/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{asset('/theme/admin/plugins/switchery/switchery.min.css')}}" />
    <link href="{{asset('/theme/admin/plugins/footable/css/footable.core.css')}}" rel="stylesheet">

    <!-- App css -->
    <link href="{{asset('/theme/admin/assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('/theme/admin/assets/css/icons.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('/theme/admin/assets/css/metismenu.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('/theme/admin/assets/css/style.css')}}" rel="stylesheet" type="text/css" />
    <!-- Summernote css -->
    <link href="{{asset('/theme/admin/plugins/summernote/summernote-bs4.css')}}" rel="stylesheet" />


</head>
<body>

<div id="wrapper">
    <div class="content-page">
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        @if(session()->has('message'))
                            <div class="alert alert-success mt-5" id="myElem" >
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                {{ session()->get('message') }}
                            </div>
                        @endif
                        <br><br><br><br><br>
                            <form action="{{route('admin.email.template.store')}}" method="post" enctype="multipart/form-data">
                                @csrf
                                <input type="text" style="display: none;" name="images" id="images">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="clearfix">
                                            <br><br><br><br>
                                        </div>
                                    </div>
                                    <div class="col-md-9">

                                        <div class="form-group">
                                            <h1>{{$template->title}}</h1>
                                        </div>
                                        <div class="form-group" align="center">
                                            <img src="{{asset($template->media->path)}}" alt="">
                                        </div>
                                        <div class="form-group">
                                            <p>{{$template->description}}</p>
                                        </div>
                                        <div class="row text-light" style="background-color: #1d1e1f; margin: 0px;">
                                            <div class="col-md-2">Bizneez.net</div>
                                            <div class="col-md-3"><a href="">Unsubscribe</a></div>
                                            <div class="col-md-5" align="center">Kemp House, 160 City Road, London, United Kingdom, EC1V 2NX</div>
                                            <div class="col-md-3 float-right" align="right">+44(0)20 8524 5294

                                            </div>
                                        </div>
                                        <br>
                                        <div class="form-group">
                                            <div align="center">
                                                <a href="{{ route('admin.promotion.email.send', ['id' => $template->id] ) }}" type="submit" class="btn btn-primary mb-2">Send</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="{{asset('/assets/js/jquery-ui.js')}}"></script>

<script src="{{asset('/theme/admin/assets/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('/theme/admin/assets/js/metisMenu.min.js')}}"></script>
<script src="{{asset('/theme/admin/assets/js/waves.js')}}"></script>
<script src="{{asset('/theme/admin/assets/js/jquery.slimscroll.js')}}"></script>

<!-- Flot chart -->
<script src="{{asset('theme/admin/plugins/flot-chart/jquery.flot.min.js')}}"></script>
<script src="{{asset('theme/admin/plugins/flot-chart/jquery.flot.time.js')}}"></script>
<script src="{{asset('theme/admin/plugins/flot-chart/jquery.flot.tooltip.min.js')}}"></script>
<script src="{{asset('theme/admin/plugins/flot-chart/jquery.flot.resize.js')}}"></script>
<script src="{{asset('theme/admin/plugins/flot-chart/jquery.flot.pie.js')}}"></script>
<script src="{{asset('theme/admin/plugins/flot-chart/jquery.flot.crosshair.js')}}"></script>
<script src="{{asset('theme/admin/plugins/flot-chart/curvedLines.js')}}"></script>
<script src="{{asset('theme/admin/plugins/flot-chart/jquery.flot.axislabels.js')}}"></script>
<script type="text/javascript" src="{{asset('theme/admin/plugins/jquery-knob/excanvas.js')}}"></script>
<script src="{{asset('theme/admin/plugins/jquery-knob/jquery.knob.js')}}"></script>

<script src="{{asset('theme/admin/assets/pages/jquery.dashboard.init.js')}}"></script>

<script src="{{asset('/theme/admin/assets/js/jquery.core.js')}}"></script>
<script src="{{asset('/theme/admin/assets/js/jquery.app.js')}}"></script>
{{--    <script src="{{asset('/theme/admin/plugins/autocomplete/jquery.mockjax.js')}}"></script>--}}
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />


</body>
</html>


























@extends('admin.layouts.app')

<script !src="">
    let selectedImages = []
    let lastSelectedImage = ''
    function selectImages(id) {

        if (lastSelectedImage !== '') {
            $('#'+lastSelectedImage).css('border', '')
        }
        lastSelectedImage = id
        $('#'+id).css('border', '2px solid green')
        $('#images').val(id)


        // if (selectedImages.includes(id)) {
        //     let index = selectedImages.indexOf(id);
        //     if (index > -1) {
        //         selectedImages.splice(index, 1);
        //     }
        //     $('#'+id).css('border', '')
        // }
        // else {
        //     selectedImages.push(id)
        //     $('#'+id).css('border', '2px solid green')
        // }
        // $('#images').val(selectedImages)
    }
</script>

<style>
    .fa-custom{  font-size: 200px!important;}
</style>