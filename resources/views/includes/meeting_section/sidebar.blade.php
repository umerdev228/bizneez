<!-- ========== Left Sidebar Start ========== -->
@php
    $sidebar_color = \App\SiteSetting::where('key', 'side_bar_color')->first()->data;
    $header_logo = \App\SiteSetting::where('key', 'header_logo')->first()->data;
    $text_color = \App\SiteSetting::where('key', 'text_color')->first()->data;
@endphp
<div class="left side-menu" style="background: {{$sidebar_color}}">
    <div class="slimscroll-menu" id="remove-scroll">
        <!-- LOGO -->
        <div class="topbar-left" style="background: {{$sidebar_color}}">
            <a  style="color: {{$text_color}} !important;"  href="{{route('dashboard', ['role' => Auth::user()->roles[0]->name])}}" class="logo">
                <span>
                    <img src="{{asset($header_logo)}}" alt="" height="22">
                </span>
                <i>
{{--                    <img src="{{asset('assets/images/logo_sm.png')}}" alt="" height="28">--}}
                </i>
            </a>
        </div>
        <!-- User box -->
        <div class="user-box">
            {{--            <div class="user-img">--}}
            {{--                <img src="{{asset('/theme/admin/assets/images/users/avatar-1.jpg')}}" alt="user-img" title="Mat Helme" class="rounded-circle img-fluid">--}}
            {{--            </div>--}}
            {{--            <h5><a  style="color: {{$text_color}} !important;"  href="#">{{ Auth::user()->name }}</a> </h5>--}}
            {{--            <p class="text-muted">{{ ucfirst(Auth::user()->roles[0]->name) }}</p>--}}
        </div>

        <div id="sidebar-menu">
            <ul class="metismenu with-tooltip mt-5" id="side-menu" style="color: {{$text_color}} !important;">


                <li data-toggle="tooltip" data-placement="right" title="" data-original-title="Contacts">
                    <a  style="color: {{$text_color}} !important;" href="{{ url('admin/dashboard')}}">
                        <i class="mdi mdi-contacts"></i>
                        Dashboard
                    </a>
                </li>
                <li data-toggle="tooltip" data-placement="right" title="" data-original-title="Appointment">
                    <a  style="color: {{$text_color}} !important;" href="{{ route('admin.events.index') }}">
                        <i class="mdi mdi-calendar"    >
                        </i> Appointment
                    </a>
                </li>

                <li data-toggle="tooltip" data-placement="right" title="" data-original-title="Contacts">
                    <a  style="color: {{$text_color}} !important;" href="{{ route('admin.contact.index') }}">
                        <i class="mdi mdi-contacts"    >
                        </i> Contacts
                    </a>
                </li>
                <li data-toggle="tooltip" data-placement="right" title="" data-original-title="Messages">
                    <a  style="color: {{$text_color}} !important;" href="{{ route('admin.chat.index') }}">
                        <i class="mdi mdi-inbox-arrow-down"    >
                        </i> Messages
                    </a>
                </li>
                <li data-toggle="tooltip" data-placement="right" title="" data-original-title="Client to Client Video Chat">
                    <a  style="color: {{$text_color}} !important;" href="{{ route('video_chat_webrtc_client_to_client') }}">
                        <i class="mdi mdi-message-video"    >
                        </i> Video Chat
                    </a>
                </li>

                <li data-toggle="tooltip" data-placement="right" title="" data-original-title="Email Inbox">
                    <a  style="color: {{$text_color}} !important;" href="{{ route('admin.email.inbox') }}">
                        <i class="mdi mdi-inbox"    >
                        </i> Email Inbox
                    </a>
                </li>
            
            </ul>
        </div>
        <div class="clearfix"></div>
    </div>
</div>






