@extends('admin.layouts.app')
@section('content')
    @include('admin.layouts.topbar')
    <!-- Google Chart JS -->

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/min/dropzone.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/dropzone.js"></script>

    <!-- Start Page content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if(session()->has('message'))
                        <div class="alert alert-success" style="width:803px ">
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-9 mt-5">
                            <div class="card-box">
                                <form id="update-product-form" method="POST" action="{{ route('admin.products.update', ['id' => $product->id]) }}" enctype="multipart/form-data">
                                    @csrf
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="name"> Name <span class="text-danger">*</span></label>
                                                <input type="text" class="form-control valid" name="name" value="{{$product->name}}" id="name" maxlength="20" required />
                                                @if ($errors->has('name'))
                                                    <span class="error">{{ $errors->first('name') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="price"> Net Price <span class="text-danger">*</span></label>
                                                <input type="text" class="form-control valid" name="price" value="{{$product->price}}" id="price" maxlength="20" required />
                                                @if ($errors->has('price'))
                                                    <span class="error">{{ $errors->first('price') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="second_price"> Retail Price <span class="text-danger">*</span></label>
                                                <input type="text" class="form-control valid" name="second_price" value="{{$product->second_price}}" id="price" maxlength="20" required />
                                                @if ($errors->has('second_price'))
                                                    <span class="error">{{ $errors->first('second_price') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="discount"> Discount <span class="text-danger">*</span></label>
                                                <input type="text" class="form-control valid" name="discount" value="{{$product->discount}}" id="discount" maxlength="20" required />
                                                @if ($errors->has('discount'))
                                                    <span class="error">{{ $errors->first('discount') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="quantity"> Quantity <span class="text-danger">*</span></label>
                                                <input type="text" class="form-control valid" name="quantity" value="{{$product->quantity}}" id="quantity" maxlength="20" required />
                                                @if ($errors->has('quantity'))
                                                    <span class="error">{{ $errors->first('quantity') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="Description">Description<span class="text-danger">*</span></label>
                                                <textarea id="Description" class="form-control rounded-0" name="description" rows="5">{{$product->description}}</textarea>
                                                @if ($errors->has('description'))
                                                    <span class="error">{{ $errors->first('description') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="category"> Categories <span class="text-danger">*</span></label>
                                                <input type="text" class="form-control valid" name="categories[]" id="category" required />
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="size"> Size  <span class="text-danger">*</span> <small class="text-success">Enter Commas Seprated Sizes</small></label>
                                                <input class="form-control valid" type="text" name="size" id="size" value="{{ $sizes }}" required>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="range"> Range Types  <span class="text-danger">*</span></label>
                                                <select name="range" id="range" class="form-control valid">
                                                    <option value="0" @if($product->range == '0') selected @endif>Choose Range</option>
                                                    <option value="1" @if($product->range == '1') selected @endif>Range 1</option>
                                                    <option value="2" @if($product->range == '2') selected @endif>Range 2</option>
                                                    <option value="3" @if($product->range == '3') selected @endif>Range 3</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="range"> Color Type  <span class="text-danger">*</span></label>
                                                <select name="color_type" id="range" class="form-control valid" >
                                                    <option value="0" @if($product->color_type == '0') selected @endif>Choose Color Type</option>
                                                    <option value="1" @if($product->color_type == '1') selected @endif>Desktop</option>
                                                    <option value="2" @if($product->color_type == '2') selected @endif>Leg Frame</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="color"> Colors <span class="text-danger"></span></label>
                                                <div id="dropzone">
                                                    <div class="dropzone needsclick" id="colors-upload">
                                                        <div class="dz-message needsclick">
                                                            Upload Your Product Color Images
                                                        </div>
                                                    </div>
                                                </div>
                                                <br/>
                                                <div id="preview-color-image" style="display: none;">
                                                    <div class="dz-preview dz-file-preview">
                                                        <div class="dz-image">
                                                            <img data-dz-thumbnail=""/>
                                                        </div>
                                                        <div class="dz-details">
                                                            <div class="dz-size"><span data-dz-size=""></span></div>
                                                            <div class="dz-filename"><span data-dz-name=""></span></div></div>
                                                        <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress=""></span></div>
                                                        <div class="dz-error-message"><span data-dz-errormessage=""></span></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="image"> Images <span class="text-danger"></span></label>
                                                <div id="dropzone">
                                                    <div class="dropzone needsclick" id="image-upload">
                                                        <div class="dz-message needsclick">
                                                            Upload Your Product Images
                                                        </div>
                                                    </div>
                                                </div>
                                                <br/>
                                                <div id="preview-image" style="display: none;">
                                                    <div class="dz-preview dz-file-preview">
                                                        <div class="dz-image">
                                                            <img data-dz-thumbnail=""/>
                                                        </div>
                                                        <div class="dz-details">
                                                            <div class="dz-size"><span data-dz-size=""></span></div>
                                                            <div class="dz-filename"><span data-dz-name=""></span></div></div>
                                                        <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress=""></span></div>
                                                        <div class="dz-error-message"><span data-dz-errormessage=""></span></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <div class="form-group mt-3">
                                    <input class="btn btn-primary" type="submit" value="Update" form="update-product-form">
                                    <button onclick="history.back()" type="button" class="btn">Cancel</button>
                                </div>

                            </div>
                            <!-- end card-box -->
                        </div>

                    </div> <!-- container -->
                </div> <!-- content -->
            </div>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <style href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/dropzone.css"></style>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/dropzone.js"></script>



    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-beta/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
    <link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/examples/assets/app.css">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.0.4/popper.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.11.1/typeahead.bundle.min.js"></script>
    <script src="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>

    <script>

        var data = '{!! json_encode($categories) !!}'
        console.log(data)
        //get data pass to json
        var task = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace("name"),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            local: jQuery.parseJSON(data) //your can use json type
        });

        task.initialize();

        var elt = $("#category");
        elt.tagsinput({
            itemValue: "id",
            itemText: "name",
            typeaheadjs: {
                name: "name",
                displayKey: "name",
                source: task.ttAdapter()
            }
        });
        var selected_categories = {!! json_encode($product->categories) !!}
        console.log(selected_categories)
        //insert data to input in load page
        for (var i = 0; i < selected_categories.length; i++) {
            console.log(selected_categories[i].id)
            elt.tagsinput("add", {
                id: selected_categories[i].id,
                name: selected_categories[i].name,
            });
        }




        Dropzone.autoDiscover = false;
        var product_dropzone = new Dropzone(document.getElementById('image-upload'), {
            url: "{{route('product.image.update')}}", // Set the url
            previewTemplate: document.querySelector('#preview-image').innerHTML,
            parallelUploads: 2,
            thumbnailHeight: 120,
            thumbnailWidth: 120,
            addRemoveLinks: true,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            filesizeBase: 1000,
            thumbnail: function(file, dataUrl) {
                if (file.previewElement) {
                    file.previewElement.classList.remove("dz-file-preview");
                    var images = file.previewElement.querySelectorAll("[data-dz-thumbnail]");
                    for (var i = 0; i < images.length; i++) {
                        var thumbnailElement = images[i];
                        thumbnailElement.alt = file.id;
                        thumbnailElement.src = dataUrl;
                    }
                    setTimeout(function() { file.previewElement.classList.add("dz-image-preview"); }, 1);
                }
            },

            init: function() {
                myDropzone = this;
                var self = this;
                var color_images = {!! json_encode($product->media) !!}
                for (var step = 0; step < color_images.length; step++) {
                    var file = '{{asset('/')}}' + color_images[step].path
                    console.log(file)

                    var mockFile = { name: color_images[step].id, size: 0};

                    myDropzone.emit("addedfile",mockFile );
                    myDropzone.emit("thumbnail", mockFile, file);
                    myDropzone.emit("complete",mockFile );
                }
            },

            removedfile: function (file) {
                console.log(file, 'color_dropzone')
                var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                var color_images = {!! json_encode($product->media) !!}
                for(var i = 0; i < color_images.length; ++i){
                    if(color_images[i].id === file.name) {
                        console.log(color_images[i].id)
                        $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            type:'POST',
                            url:'{{ route('product.color.image.delete') }}',
                            data: {_token: CSRF_TOKEN, id: color_images[i].id},
                            success:function(response) {
                                console.log(response)
                            }
                        });
                    }
                }
                var _ref;
                return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
            }

        });

        var color_dropzone = new Dropzone(document.getElementById('colors-upload'), {
            url: "{{route('product.color.image.update')}}", // Set the url
            previewTemplate: document.querySelector('#preview-color-image').innerHTML,
            parallelUploads: 2,
            thumbnailHeight: 120,
            thumbnailWidth: 120,
            addRemoveLinks: true,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            filesizeBase: 1000,
            thumbnail: function(file, dataUrl) {
                if (file.previewElement) {
                    file.previewElement.classList.remove("dz-file-preview");
                    var images = file.previewElement.querySelectorAll("[data-dz-thumbnail]");
                    for (var i = 0; i < images.length; i++) {
                        var thumbnailElement = images[i];
                        thumbnailElement.alt = file.name;
                        thumbnailElement.src = dataUrl;
                    }
                    setTimeout(function() { file.previewElement.classList.add("dz-image-preview"); }, 1);
                }
            },
            init: function() {
                myDropzones = this;
                var self = this;
                var color_images = {!! json_encode($product->colors) !!}
                for (var step = 0; step < color_images.length; step++) {
                    var file = '{{asset('/')}}' + color_images[step].path
                    var mockFile = { name: color_images[step].id, size: 0};
                    myDropzones.emit("addedfile",mockFile );
                    myDropzones.emit("thumbnail", mockFile, file);
                    myDropzones.emit("complete",mockFile );

                }
            },
            removedfile: function (file) {
                console.log(file, 'color_dropzone')
                var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                var color_images = {!! json_encode($product->colors) !!}
                for(var i = 0; i < color_images.length; ++i){
                    if(color_images[i].id === file.name) {
                        console.log(color_images[i].id)
                        $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            type:'POST',
                            url:'{{ route('product.color.image.delete') }}',
                            data: {_token: CSRF_TOKEN, id: color_images[i].id},
                            success:function(response) {
                                console.log(response)
                            }
                        });
                    }
                }
                var _ref;
                return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
            }
        });
    </script>

@endsection
