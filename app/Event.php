<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    //

    protected $guarded = [];

    public function users() {
        return $this->belongsToMany(User::class, 'event_users', 'event_id', 'user_id');
    }

    public function comments() {
        return $this->hasMany(Comment::class, 'event_id')->with('user')->where('type', 'event');
    }
}
