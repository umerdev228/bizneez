@extends('admin.layouts.app')
@section('content')
    <div class="content">
        <div class="container-fluid" style="margin-top: 75px;">
            <div class="row">
                <div class="col-lg-12">
                    @if(session()->has('message'))
                        <div class="alert alert-success mt-5" id="myElem" >
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    <div class="row">
                        <ul class="nav nav-tabs">
                            <li class="nav-item">
                                <a href="#create-employee" data-toggle="tab" aria-expanded="false" class="nav-link active show">
                                    <i class="fi-torso mr-2"></i> Create Employee
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#salary" data-toggle="tab" aria-expanded="true" class="nav-link">
                                    <i class="fi-gallary mr-2"></i>Salary
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#analytics-perform" data-toggle="tab" aria-expanded="false" class="nav-link">
                                    <i class="fi-bookmark mr-2"></i>Analytics and Performance
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="tab-content">
                        <div class="tab-pane active show" id="create-employee">
                            <div class="row">
                                <div class="col-lg-12">
                                    {{--                                        <div class="card-box">--}}
                                    {{--                                            <div class="media">--}}
                                    {{--                                                <img class="d-flex mr-3 rounded-circle" alt="64x64" src="{{asset($user->profile->image)}}" style="width: 48px; height: 48px;">--}}
                                    {{--                                                <div class="media-body">--}}
                                    {{--                                                    <h5 class="media-heading mb-0 mt-0">{{ $user->name }}</h5>--}}
                                    {{--                                                </div>--}}
                                    {{--                                            </div>--}}
                                    {{--                                            <h4 class="m-b-20">Address</h4>--}}
                                    {{--                                            <p class="text-muted"> {{ $user->profile->address }} </p>--}}
                                    {{--                                            <ul class="list-inline task-dates m-b-0 mt-5">--}}
                                    {{--                                                <li>--}}
                                    {{--                                                    <h5 class="m-b-5">Role</h5>--}}
                                    {{--                                                    <p> {{ ucfirst($user->roles[0]->name) }}</p>--}}
                                    {{--                                                </li>--}}

                                    {{--                                                <li>--}}
                                    {{--                                                    <h5 class="m-b-5">Mobile</h5>--}}
                                    {{--                                                    <p>{{ $user->profile->mobile }}</p>--}}
                                    {{--                                                </li>--}}
                                    {{--                                            </ul>--}}
                                    {{--                                            <div class="clearfix"></div>--}}
                                    {{--                                            <div class="clearfix"></div>--}}
                                    {{--                                            <div class="assign-team mt-4">--}}

                                    {{--                                            </div>--}}

                                    {{--                                        </div>--}}
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <img class="d-flex mr-3 rounded-circle" alt="64x64" src="{{asset($user->profile->image)}}" style="width: 48px; height: 48px;">
                                                </div>
                                                <div class="col-lg-3 pb-2"><strong>Name</strong> </div>
                                                <div class="col-lg-3">{{$user->name}}</div>
                                                <div class="col-lg-3 pb-2"><strong>Email</strong> </div>
                                                <div class="col-lg-3">{{$user->email}}</div>
                                                <div class="col-lg-3 pb-2"><strong>Description</strong> </div>
                                                <div class="col-lg-3">{{$user->description}}</div>
                                                <div class="col-lg-3 pb-2"><strong>Phone</strong> </div>
                                                <div class="col-lg-3">{{$user->phone}}</div>
                                                <div class="col-lg-3 pb-2"><strong>Mobile</strong> </div>
                                                <div class="col-lg-3">{{$user->mobile}}</div>
                                                <div class="col-lg-3 pb-2"><strong>Assistant Phone Number</strong> </div>
                                                <div class="col-lg-3">{{$user->assistant_number}}</div>
                                                <div class="col-lg-3 pb-2"><strong>Address</strong> </div>
                                                <div class="col-lg-3">{{$user->address}}</div>
                                                <div class="col-lg-3 pb-2"><strong>Web</strong> </div>
                                                <div class="col-lg-3">{{$user->web}}</div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="salary">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-lg-3 pb-2"><strong>Account Invoices</strong> </div>
                                        <div class="col-lg-3">{{$user->salary->account_invoice}}</div>
                                        <div class="col-lg-3 pb-2"><strong>Invoice Number</strong> </div>
                                        <div class="col-lg-3">{{$user->salary->invoice_number}}</div>
                                        <div class="col-lg-3 pb-2"><strong>Description</strong> </div>
                                        <div class="col-lg-3">{{$user->salary->description}}</div>
                                        <div class="col-lg-3 pb-2"><strong>Price</strong> </div>
                                        <div class="col-lg-3">{{$user->salary->price}}</div>
                                        <div class="col-lg-3 pb-2"><strong>Phone</strong> </div>
                                        <div class="col-lg-3">{{$user->salary->phone}}</div>
                                        <div class="col-lg-3 pb-2"><strong>Discount</strong> </div>
                                        <div class="col-lg-3">{{$user->salary->discount}}</div>
                                        <div class="col-lg-3 pb-2"><strong>Profit</strong> </div>
                                        <div class="col-lg-3">{{$user->salary->profit}}</div>
                                        <div class="col-lg-3 pb-2"><strong>VAT%</strong> </div>
                                        <div class="col-lg-3">{{$user->salary->vat}}</div>
                                        <div class="col-lg-3 pb-2"><strong>Total</strong> </div>
                                        <div class="col-lg-3">{{$user->salary->total}}</div>
                                        <div class="col-lg-3 pb-2"><strong>Commission</strong> </div>
                                        <div class="col-lg-3">{{$user->salary->commission}}</div>
                                        <div class="col-lg-3 pb-2"><strong>Quarter Bonuses</strong> </div>
                                        <div class="col-lg-3">{{$user->salary->quarter_bonuses}}</div>
                                        <div class="col-lg-3 pb-2"><strong>Your Bonuses</strong> </div>
                                        <div class="col-lg-3">{{$user->salary->your_bonus}}</div>
                                        <div class="col-lg-3 pb-2"><strong>Total Salary</strong> </div>
                                        <div class="col-lg-3">{{$user->salary->salary}}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="analytics-perform">
{{--                            <h1>analytics-perform</h1>--}}
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
