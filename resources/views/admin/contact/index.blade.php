@extends('includes.meeting_section.master')
@section('content')
    <div class="row" style="margin-top: 100px;">
        <div class="card">
            <a href="{{ route('admin.contact.create') }}" class="btn btn-success">Create Contact</a>
        </div>
    </div>



<br>
    <div class="row">
        @foreach($contact as $cont)
            <div class="col-xl-3 col-sm-6 mb-4 "><a href="{{ route('admin.contact.remove', ['id' => $cont->id] ) }}"><i class="remove-icon fa fa-remove"></i></a>
                <div class="card text-center">
                    <div class="card-body" data-toggle="tooltip" data-placement="top" title="{!! strip_tags($cont->description) !!}">
                        <div class="member-card py-2">
                            <div class="member-thumb mx-auto">
                                <img src="{{asset('theme/admin/assets/images/users/avatar-1.jpg')}}" class="img-fluid w-25 rounded-circle" alt="" />
                            </div>
                            <div class="mt-3">
                                <h4 class="mb-1">{{ $cont->name }}</h4>
                                <p class="text-muted">{{$cont->phone}} <span> <br> </span> <span> {{$cont->email}} </span></p>
                            </div>
                        </div>
                    </div>
                </div>

            </div> <!-- end col -->
 
 
        @endforeach
    </div>
    {{ $contact->links() }}

 
    </div>


    <style>
     .remove-icon{font-size: 22px;
    color: red;
    border: 1px sold;
    border: 2px solid #c1b6b6;
     }

     #team {
            background: #eee !important;
        }

        .btn-primary:hover,
        .btn-primary:focus {
            background-color: #108d6f;
            border-color: #108d6f;
            box-shadow: none;
            outline: none;
        }

        .btn-primary {
            color: #fff;
            background-color: #007b5e;
            border-color: #007b5e;
        }

        section {
            padding: 60px 0;
        }

        section .section-title {
            text-align: center;
            color: #007b5e;
            margin-bottom: 50px;
            text-transform: uppercase;
        }

        #team .card {
            border: none;
            background: #ffffff;
        }

        .image-flip:hover .backside,
        .image-flip.hover .backside {
            -webkit-transform: rotateY(0deg);
            -moz-transform: rotateY(0deg);
            -o-transform: rotateY(0deg);
            -ms-transform: rotateY(0deg);
            transform: rotateY(0deg);
            border-radius: .25rem;
        }

        .image-flip:hover .frontside,
        .image-flip.hover .frontside {
            -webkit-transform: rotateY(180deg);
            -moz-transform: rotateY(180deg);
            -o-transform: rotateY(180deg);
            transform: rotateY(180deg);
        }

        .mainflip {
            -webkit-transition: 1s;
            -webkit-transform-style: preserve-3d;
            -ms-transition: 1s;
            -moz-transition: 1s;
            -moz-transform: perspective(1000px);
            -moz-transform-style: preserve-3d;
            -ms-transform-style: preserve-3d;
            transition: 1s;
            transform-style: preserve-3d;
            position: relative;
        }

        .frontside {
            position: relative;
            -webkit-transform: rotateY(0deg);
            -ms-transform: rotateY(0deg);
            z-index: 2;
            margin-bottom: 30px;
        }

        .backside {
            position: absolute;
            top: 0;
            left: 0;
            background: white;
            -webkit-transform: rotateY(-180deg);
            -moz-transform: rotateY(-180deg);
            -o-transform: rotateY(-180deg);
            -ms-transform: rotateY(-180deg);
            transform: rotateY(-180deg);
            -webkit-box-shadow: 5px 7px 9px -4px rgb(158, 158, 158);
            -moz-box-shadow: 5px 7px 9px -4px rgb(158, 158, 158);
            box-shadow: 5px 7px 9px -4px rgb(158, 158, 158);
        }

        .frontside,
        .backside {
            -webkit-backface-visibility: hidden;
            -moz-backface-visibility: hidden;
            -ms-backface-visibility: hidden;
            backface-visibility: hidden;
            -webkit-transition: 1s;
            -webkit-transform-style: preserve-3d;
            -moz-transition: 1s;
            -moz-transform-style: preserve-3d;
            -o-transition: 1s;
            -o-transform-style: preserve-3d;
            -ms-transition: 1s;
            -ms-transform-style: preserve-3d;
            transition: 1s;
            transform-style: preserve-3d;
        }

        .frontside .card,
        .backside .card {
            min-height: 312px;
        }

        .backside .card a {
            font-size: 18px;
            color: #007b5e !important;
        }

        .frontside .card .card-title,
        .backside .card .card-title {
            color: #007b5e !important;
        }

        .frontside .card .card-body img {
            width: 120px;
            height: 120px;
            border-radius: 50%;
        }
        .list-group-item{
            max-width: 240px !important;
        }
    </style>

@endsection