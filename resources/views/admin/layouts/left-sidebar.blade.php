<!-- ========== Left Sidebar Start ========== -->
@php
    $sidebar_color = \App\SiteSetting::where('key', 'side_bar_color')->first()->data;
    $header_logo = \App\SiteSetting::where('key', 'header_logo')->first()->data;
    $text_color = \App\SiteSetting::where('key', 'text_color')->first()->data;
@endphp
<div class="left side-menu" style="background: {{$sidebar_color}}">
    <div class="slimscroll-menu" id="remove-scroll">
        <!-- LOGO -->
        <div class="topbar-left" style="background: {{$sidebar_color}}">
            <a  style="color: {{$text_color}} !important;"  href="{{route('dashboard', ['role' => Auth::user()->roles[0]->name])}}" class="logo">
                <span>
                    <img src="{{asset($header_logo)}}" alt="" height="22">
                </span>
                <i>
                    <img src="{{asset('assets/images/logo_sm.png')}}" alt="" height="28">
                </i>
            </a>
        </div>
        <!-- User box -->
        <div class="user-box">
            {{--            <div class="user-img">--}}
            {{--                <img src="{{asset('/theme/admin/assets/images/users/avatar-1.jpg')}}" alt="user-img" title="Mat Helme" class="rounded-circle img-fluid">--}}
            {{--            </div>--}}
            {{--            <h5><a  style="color: {{$text_color}} !important;"  href="#">{{ Auth::user()->name }}</a> </h5>--}}
            {{--            <p class="text-muted">{{ ucfirst(Auth::user()->roles[0]->name) }}</p>--}}
        </div>

        <div id="sidebar-menu">
            <ul class="metismenu with-tooltip mt-5" id="side-menu" style="color: {{$text_color}} !important;">

                <li data-toggle="tooltip" data-placement="right" title="" data-original-title="Dashboard">
                    <a style="color: {{$text_color}} !important;" href="{{ route('dashboard',  ['role' => Auth::user()->roles[0]->name])}}">
                        <i class="fa fa-dashboard"></i>
                        Dashboard
                    </a>
                </li>
                @if(Auth::user()->hasRole(['admin']))


                    <li  data-toggle="tooltip" data-placement="right" title="Manufactures" data-original-title="Manufactures">
                        <a  style="color: {{$text_color}} !important;"  href="{{ url('admin/manufacturers')}}" aria-expanded="false">
                            <i class="fa fa-product-hunt" aria-hidden="true"></i><span onclick="show_usb()"> Manufactures </span> <span class="menu-arrow"></span></a>
                        <ul class="nav-second-level collapse s" aria-expanded="false" >
                            <li><a  style="color: {{$text_color}} !important;"  href="{{ url('admin/manufacturers/image_gallry')}}">Image Gallary</a></li>
                            <li><a  style="color: {{$text_color}} !important;"  href="{{ url('admin/manufacturers/price_list')}}">Price List</a></li>
                            <li><a  style="color: {{$text_color}} !important;"  href="{{ url('admin/manufacturers/new_price_list')}}">New Price List</a></li>
                            <li><a  style="color: {{$text_color}} !important;"  href="{{ url('admin/dealers')}}">Dealers</a></li>
                            <li><a  style="color: {{$text_color}} !important;"  href="{{ url('admin/dealers/create')}}">Add New Dealer</a></li>
                            <li><a  style="color: {{$text_color}} !important;"  href="{{ url('admin/manufacturers/inventory')}}">Inventory</a></li>
                            <li><a  style="color: {{$text_color}} !important;"  href="{{ url('admin/manufacturers')}}">Manufactures</a></li>

                        </ul>
                    </li>
                @endif



                @if(Auth::user()->hasRole(['admin']))
                   

                    <li  data-toggle="tooltip" data-placement="right" title="Caompany" data-original-title="Manufactures">
                        <a  style="color: {{$text_color}} !important;"  href="#" aria-expanded="false">
                            <i class="fa fa-users" aria-hidden="true"></i><span onclick="show_usb()"> Company </span> <span class="menu-arrow"></span></a>
                        <ul class="nav-second-level collapse s" aria-expanded="false" >
                            <li><a  style="color: {{$text_color}} !important;"  href="{{ url('admin/companies')}}">New Company</a></li>
                            <li><a  style="color: {{$text_color}} !important;"  href="{{ route('admin.location.index')}}">Location</a></li>
                            

                        </ul>
                    </li>


                @endif




                @if(Auth::user()->hasRole(['admin']))
                   
                     
{{--                    <li  data-toggle="tooltip" data-placement="right" title="Leads" data-original-title="Auth Codes">--}}
{{--                        <a  style="color: {{$text_color}} !important;"  href="{{ route('admin.auth.codes.index') }}">--}}
{{--                            <i class="fa fa-sign-in"></i>--}}
{{--                            Auth Codes--}}
{{--                        </a>--}}
{{--                    </li>--}}
{{--                    <li  data-toggle="tooltip" data-placement="right" title="Leads" data-original-title="Auth Codes">--}}
{{--                        <a  style="color: {{$text_color}} !important;"  href="{{ route('admin.demo.appointments') }}">--}}
{{--                            <i class="fa fa-sign-in"></i>--}}
{{--                            Demo Request--}}
{{--                        </a>--}}
{{--                    </li>--}}
                @endif



                <li  data-toggle="tooltip" data-placement="right" title="CRM" data-original-title="CRM">
                    <a  style="color: {{$text_color}} !important;"  href="{{ route('admin.events.index') }}" aria-expanded="false"><i class="fa fa-television" aria-hidden="true"></i><span> CRM </span> <span class="menu-arrow"></span></a>
                    <ul class="nav-second-level collapse" aria-expanded="false"  >
                        <li><a  style="color: {{$text_color}} !important;"  href="{{ route('admin.events.index') }}">Appointment</a></li>
                        <li><a  style="color: {{$text_color}} !important;"  href="{{ route('admin.contact.index') }}">Contacts</a></li>
                        <li><a  style="color: {{$text_color}} !important;"  href="{{ route('admin.chat.index') }}">Messages</a></li>
                        <li><a  style="color: {{$text_color}} !important;"  href="{{ route('admin.email.inbox') }}">Email Inbox</a></li>
<li><a  style="color: {{$text_color}} !important;"  href="{{ route('admin.leads.index') }}">Leads</a></li>
 


{{--                        <li><a  onClick=window.open("{{route('video.chat')}}","Ratting","width=800,height=400,left=150,top=200,toolbar=0,status=0,"); href="javascript:void(0)">Video Calling</a></li>--}}
                        {{--                        <li><a  href="{{route('video.chat')}}">Video Calling</a></li>--}}
                        {{--                        <li><a  style="color: {{$text_color}} !important;"  href="{{route('admin.promotion.email.index')}}">Bulk Email Creation</a></li>--}}
                    </ul>
                </li>




                @if(Auth::user()->hasRole(['admin']))
                    <li  data-toggle="tooltip" data-placement="right" title="" data-original-title="Architectures">

                        <a  style="color: {{$text_color}} !important;"  href="{{ url('admin/architects')}}">

                            <i class="fi-layers"></i> Architects

                        </a>

                    </li>

                @endif

                @if(Auth::user()->hasRole(['employee']))

                    <li  data-toggle="tooltip" data-placement="right" title="" data-original-title="Architectures">

                        <a  style="color: {{$text_color}} !important;"  href="{{ route('employee.architects.index')}}">

                            <i class="fi-layers"></i> Architects

                        </a>

                    </li>

                @endif

                @if(Auth::user()->hasRole(['admin']))

                    <li  data-toggle="tooltip" data-placement="right" title="" data-original-title="Clients">
                        <a  style="color: {{$text_color}} !important;"  href="{{ url('admin/clients')}}">
                            <i class="fa fa-address-book"></i> Clients
                        </a>
                    </li>
                @endif

                @if(Auth::user()->hasRole(['employee']))
                    <li  data-toggle="tooltip" data-placement="right" title="" data-original-title="Clients">
                        <a  style="color: {{$text_color}} !important;"  href="{{ route('employee.clients.index')}}">
                            <i class="fa fa-address-book"></i> Clients
                        </a>
                    </li>
                @endif

                @if(Auth::user()->hasRole(['admin']))
                    <li  data-toggle="tooltip" data-placement="right" title="" data-original-title="Employees">
                        <a  style="color: {{$text_color}} !important;"  href="{{ route('admin.employees.index') }}">
                            <i class="fa fa-users"></i> Employees
                        </a>
                    </li>
                @endif

                @if(Auth::user()->hasRole(['admin']))
                    <li  data-toggle="tooltip" data-placement="right" title=" Products" data-original-title="Products">
                        <a  style="color: {{$text_color}} !important;"  href="{{ route('admin.products.index') }}" aria-expanded="false">
                            <i class="fa fa-product-hunt" aria-hidden="true"></i><span onclick="show_usb()"> Products </span> <span class="menu-arrow"></span></a>
                        <ul class="nav-second-level collapse s" aria-expanded="false" >
                            <li><a  style="color: {{$text_color}} !important;"  href="{{ url('admin/products/create')}}">Product Gallary</a></li>
                            <li><a  style="color: {{$text_color}} !important;"  href="{{ url('admin/categories')}}">Categories</a></li>
                           <!--  <li><a  style="color: {{$text_color}} !important;"  href="{{ route('admin.products.index') }}">Stock Items</a></li> -->
                            <li><a  style="color: {{$text_color}} !important;"  href="{{ route('admin.product.ranges.index') }}">Product Ranges</a></li>
                            <li><a  style="color: {{$text_color}} !important;"  href="{{ route('admin.product.images.seo') }}">Images SEO</a></li>
                        </ul>
                    </li>
                @endif

                @if(Auth::user()->hasRole(['manufacturer']))

                    <li  data-toggle="tooltip" data-placement="right" title=" Products" data-original-title="Products">
                        <a  style="color: {{$text_color}} !important;"  href="{{ route('admin.products.index') }}" aria-expanded="false"><i class="fi-mail"></i><span> Products </span> <span class="menu-arrow"></span></a>
                        <ul class="nav-second-level collapse" aria-expanded="false"  >
                            <li><a  style="color: {{$text_color}} !important;"  href="{{ url('manufacturer/categories')}}">Categories</a></li>
                            <li><a  style="color: {{$text_color}} !important;"  href="{{ route('manufacturer.products.index') }}">Stock Items</a></li>
                            <li><a  style="color: {{$text_color}} !important;"  href="{{ route('manufacturer.product.range.index') }}">Product Ranges</a></li>
                        </ul>
                    </li>

                @endif

                @if(Auth::user()->hasRole(['architect']))

                    <li  data-toggle="tooltip" data-placement="right" title=" Products" data-original-title="Products">
                        <a  style="color: {{$text_color}} !important;"  href="{{ route('architect.products.index') }}" aria-expanded="false"><i class="fi-mail"></i><span> Products </span> <span class="menu-arrow"></span></a>
                        <ul class="nav-second-level collapse" aria-expanded="false"  >
                            <li><a  style="color: {{$text_color}} !important;"  href="{{ route('admin.products.index') }}">Stock Items</a></li>
                        </ul>
                    </li>

                    <li data-toggle="tooltip" data-placement="right" title=" Quotations" data-original-title=" Quotations">

                        <a  style="color: {{$text_color}} !important;"  href="{{ route('architect.orders.index') }}">

                            <i class="fa fa-first-order"></i> Quotations

                        </a>

                    </li>

                    <li data-toggle="tooltip" data-placement="right" title="Quotations" data-original-title="Quotations">
                        <a  style="color: {{$text_color}} !important;"  href="{{ route('architect.cart.index') }}">
                            <i class="fa fa-shopping-bag"></i>
                            <span> Cart </span>
                        </a>
                    </li>
                @endif

                @if(Auth::user()->hasRole(['employee']))


                    <li  data-toggle="tooltip" data-placement="right" title=" Products" data-original-title="Products">
                        <a  style="color: {{$text_color}} !important;"  href="{{ route('employee.products.index') }}" aria-expanded="false"><i class="fi-mail"></i><span> Products </span> <span class="menu-arrow"></span></a>
                        <ul class="nav-second-level collapse" aria-expanded="false"  >
                            <li><a  style="color: {{$text_color}} !important;"  href="{{ route('employee.products.index') }}">Stock Items</a></li>
                        </ul>
                    </li>

                    <li data-toggle="tooltip" data-placement="right" title=" Quotations" data-original-title=" Quotations">

                        <a  style="color: {{$text_color}} !important;"  href="{{ route('employee.orders.index') }}">

                            <i class="fa fa-first-order"></i> Quotations

                        </a>

                    </li>

                    <li data-toggle="tooltip" data-placement="right" title=" Quotations" data-original-title=" Quotations">

                        <a  style="color: {{$text_color}} !important;"  href="{{ route('employee.cart.index') }}">

                            <i class="fa fa-shopping-bag"></i>
                            Cart
                        </a>
                    </li>
                @endif

                @if(Auth::user()->hasRole(['admin']))
                    <li data-toggle="tooltip" data-placement="right" title=" Quotations" data-original-title="Quotations">
                        <a  style="color: {{$text_color}} !important;"  href="{{ route('admin.orders.index') }}">
                            <i class="fa fa-first-order"></i> Quotations
                        </a>
                    </li>
                    {{--                    <li data-toggle="tooltip" data-placement="right" title="Quotations" data-original-title="Quotations">--}}
                    {{--                        <a  style="color: {{$text_color}} !important;"  href="{{ route('admin.cart.index') }}">--}}
                    {{--                            <i class="fa fa-shopping-bag"></i>--}}
                    {{--                            <span> Cart </span>--}}
                    {{--                        </a>--}}
                    {{--                    </li>--}}

                    <li  data-toggle="tooltip" data-placement="right" title="Technologies" data-original-title="Technologies">
                        <a  style="color: {{$text_color}} !important;"  href="javascript: void(0);" aria-expanded="false"><i class="fa fa-cubes" aria-hidden="true"></i><span>Technologies</span> <span class="menu-arrow"></span></a>
                        <ul class="nav-second-level collapse" aria-expanded="false">
                              {{--  <li><a  style="color: {{$text_color}} !important;"  href="{{ route('admin.draggable.index') }}">Draggable Content</a></li> --}}
                            <li><a  style="color: {{$text_color}} !important;"  href="{{ route('admin.mobile.uploads.media') }}">AI</a></li>
                            <li><a  style="color: {{$text_color}} !important;"  href="#">AR</a></li>
                            <li> <a  style="color: {{$text_color}} !important;"  href="{{ route('admin.design.build.create') }}">Design & Build</a></li>
                            <li data-toggle="tooltip" data-placement="right" title="" data-original-title="Color">
                               
                            </li >
                        </ul>
                    </li>





                @endif
                @if(Auth::user()->hasRole(['manufacturer']))
                    <li data-toggle="tooltip" data-placement="right" title="Purchase Orders" data-original-title="Purchase Orders">
                        <a  style="color: {{$text_color}} !important;"  href="{{ route('manufacturer.orders.index') }}">
                            <i class="fa fa-first-order"></i> Purchase Orders
                        </a>
                    </li>
                @endif


                <li  data-toggle="tooltip" data-placement="right" title="Marketing" data-original-title="Marketing">
                    <a  style="color: {{$text_color}} !important;"  href="#" aria-expanded="false"><i class="fa fa-random" aria-hidden="true"></i> Marketing </span> <span class="menu-arrow"></span></a>
                    <ul class="nav-second-level collapse" aria-expanded="false" >
                        <li><a  style="color: {{$text_color}} !important;"  href="#">Video Maker</a></li>
                        <li><a  style="color: {{$text_color}} !important;"  href="{{ route('admin.catalogue.index') }}">Catalogue</a></li>
                        <li><a  style="color: {{$text_color}} !important;"  href="{{route('admin.promotion.email.index')}}">Mailshot</a></li>
                        <li><a  style="color: {{$text_color}} !important;"  href="{{route('admin.subscriber.index')}}">Subscribers</a></li>
                        <li><a  style="color: {{$text_color}} !important;"  href="{{ route('social_media') }}">Social Media</a></li>
                        <li><a  style="color: {{$text_color}} !important;"  href="{{ route('social.media.post') }}">Business Media</a></li>

                    </ul>
                </li>
                <li data-toggle="tooltip" data-placement="right" title="" data-original-title="Search Engine">
                    <a  style="color: {{$text_color}} !important;"  href="{{ route('search_engine') }}">
                        <i class="fa fa-search-plus" aria-hidden="true"></i>
                        Search Engine
                    </a>
                </li>
                <li data-toggle="tooltip" data-placement="right" title="" data-original-title="Advertising">
                    <a  style="color: {{$text_color}} !important;"  href="{{ route('admin.advertising.create') }}">
                        <i class="fa fa-line-chart" aria-hidden="true"></i>
                        Advertising
                    </a>
                </li>
                <li data-toggle="tooltip" data-placement="right" title="" data-original-title="Recruitment">
                    <a  style="color: {{$text_color}} !important;"  href="{{ route('admin.recruitment.create') }}">
                        <i class="fa fa-tasks" aria-hidden="true"></i>
                        Recruitment
                    </a>
                </li>
                {{--                <li  data-toggle="tooltip" data-placement="right" title=" Accounting" data-original-title="Accounting">--}}
                {{--                    <a  style="color: {{$text_color}} !important;"  href="javascript: void(0);" aria-expanded="false"><i class="fa fa-usd" aria-hidden="true"></i> Accounting <span class="menu-arrow"></span></a>--}}
                {{--                    <ul class="nav-second-level collapse" aria-expanded="false" >--}}
                {{--                         <li><a  style="color: {{$text_color}} !important;"  href="{{ route('admin.accounts.index') }}">Account</a></li>--}}
                {{--                        <li><a  style="color: {{$text_color}} !important;"  href="{{ route('salaries') }}">Salaries</a></li>--}}
                {{--                    </ul>--}}
                {{--                </li>--}}
                <li data-toggle="tooltip" data-placement="right" title="" data-original-title="Performing Companies">
                    <a  style="color: {{$text_color}} !important;"  href="{{ route('company_performance') }}">
                        <i class="fa fa-handshake-o" aria-hidden="true"></i>
                        Company Performance
                    </a>
                </li>
                <li data-toggle="tooltip" data-placement="right" title="" data-original-title="Delivery And Installs">
                    <a  style="color: {{$text_color}} !important;"  href="{{ route('delivery') }}">
                        <i class="fa fa-truck"></i>
                        Delivery And Installs
                    </a>
                </li>
            </ul>
        </div>
        <div class="clearfix"></div>
    </div>
</div>