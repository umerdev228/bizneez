<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SocialMediaAdvertisement extends Model
{
    //

    protected $guarded = [];

    public function comments() {
        return $this->hasMany(SocialMediaAdvertisementComment::class, 'post_id', 'id');
    }
}
