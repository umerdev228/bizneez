<?php

namespace App\Http\Controllers;

use App\Profile;
use App\Represent;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $clients = User::role('client')->with('user')->with('profile')->get();
        $data= [
            'clients' => $clients,
            'breadcrumb' => 'Dashboard / Clients List',
            'url' => env('APP_ENV_LINK'),
        ];
        return view('admin.client.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $breadcrumb = ucfirst(Auth::user()->roles[0]->name)." / Client / New";
        return view('admin.client.create', compact('breadcrumb'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
//        $request['password'] = $password = Str::random(8);

        $validated = $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            'password' => 'required',
        ]);
        $user = User::where('email', $request['email'])->first();
        if ($user == null) {
            User::create([
                "name" => $request['first_name'] . ' ' . $request['middle_name'] . ' ' . $request['last_name'],
                "first_name" => $request['first_name'],
                "middle_name" => $request['middle_name'],
                "last_name" => $request['last_name'],
                "email" => $request['email'],
                "created_by" => Auth::id(),
                "password" => Hash::make($request['password']),
            ]);
            $user = User::where('email', $request['email'])->first();
            $user->assignRole('client');
            Profile::create([
                "user_id" => $user->id,
                "phone" => $request['phone'],
                "mobile" => $request['mobile'],
                "assistant_number" => $request['assistant_number'],
                "address" => $request['address'],
                "location_id" => $request['location_id'],
                "area_id" => $request['area_id'],
                "description" => $request['description'],
                "company_keywords" => $request['company_name'],
            ]);
            if ($request->has('image')) {
                $imageName = time().'.'.$request->image->extension();
                $request->image->move(public_path('/uploads/users/clients'), $imageName);
                Profile::where('user_id', $user->id)->update([
                    "image" => '/uploads/users/clients/' . $imageName,
                ]);
            }
            foreach ($request->represent_name as $index => $item) {

                Represent::create([
                    'user_id' => $user->id,
                    'name' => $request->represent_name[$index],
                    'email' => $request->represent_email[$index],
                    'mobile' => $request->represent_mobile[$index],
                ]);
            }
//            Mail::to($user->email)->send(new \App\Mail\MerchantActivation($user->name, '', '', '', $request['password']));

        }
        toastr()->success('Client Created Successfully');
        return redirect()->route('admin.clients.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $breadcrumb = ucfirst(Auth::user()->roles[0]->name)." / Client / Show";
        $id = (\request()->route()->parameter('id'));
        $user = User::with('roles')->where('id', $id)->with('representatives')->with('profile')->first();
        return view('admin.client.show', compact('user', 'breadcrumb'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $breadcrumb = ucfirst(Auth::user()->roles[0]->name)." / Client / Edit";
        $users = User::all();
        $client = User::role('client')->where('id', $id)->with('representatives')->with('profile')->first();
        return view('admin.client.edit', compact('client', 'breadcrumb', 'users'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        User::where('id', $id)->update([
            "name" => $request['first_name'] . ' ' . $request['middle_name'] . ' ' . $request['last_name'],
            "first_name" => $request['first_name'],
            "middle_name" => $request['middle_name'],
            "last_name" => $request['last_name'],
            "email" => $request['email'],
            "created_by" => Auth::id(),
        ]);
        Profile::where('user_id', $id)->update([
            "phone" => $request['phone'],
            "mobile" => $request['mobile'],
            "assistant_number" => $request['assistant_number'],
            "address" => $request['address'],
            "location_id" => $request['location_id'],
            "area_id" => $request['area_id'],
            "description" => $request['first_name'],
            "company_keywords" => $request['company_keywords'],
        ]);
        if ($request->has('image')) {
            $imageName = time().'.'.$request->image->extension();
            $request->image->move(public_path('/uploads/users/clients'), $imageName);
            Profile::where('user_id', $id)->update([
                "image" => '/uploads/users/clients/' . $imageName,
            ]);
        }
        if ($request->has('password')) {
            User::where('id', $id)->update([
                "password" => $request['password'],
            ]);
        }
        Represent::where('user_id', $id)->delete();
        foreach ($request->represent_name as $index => $item) {

            Represent::create([
                'user_id' => $id,
                'name' => $request->represent_name[$index],
                'email' => $request->represent_email[$index],
                'mobile' => $request->represent_mobile[$index],
            ]);
        }
        toastr()->success('Client Updated Successfully');
        return redirect()->route('admin.clients.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        User::where('id', $id)->delete();
        Profile::where('user_id', $id)->delete();
        toastr()->success('Architect Deleted Successfully');
        return redirect()->route('admin.clients.index');
    }
}
