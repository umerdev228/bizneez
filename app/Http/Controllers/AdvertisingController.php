<?php

namespace App\Http\Controllers;

use App\Chat;
use App\Company;
use App\Events\MessageSent;
use App\Message;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Category;
use Spatie\Permission\Models\Role;
use App\Media;
use \DB;

class AdvertisingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::with('messages')->orderBy("created_at", "DESC")->get();
            $media = Media::where('name', 'gallery')->get();
        $roles = Role::with('users')->get();
         $categories = Category::all();
        $breadcrumb = 'Dashboar / Advertising';
        $products  = DB::table('mdd_price_list_csv')->get(); 
        return view('admin.advertisement', compact('users', 'roles', 'breadcrumb', 'media' ,'products' , 'categories'));
   
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $breadcrumb = 'Dashboard / Advertising';
        $companies = Company::all();
        return view('admin.advertisement.create', compact('breadcrumb', 'companies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Chat  $chat
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        //
        $data = $request->all();
        $count = $data['rows'];

        $companies = Company::all();
        $companies = Company::where('id', $data['company_id'])->first();
        $breadcrumb = 'Dashboard / Advertising / Show';
        return view('admin.advertisement.preview', compact('breadcrumb', 'data', 'count', 'companies'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Chat  $chat
     * @return \Illuminate\Http\Response
     */
    public function edit(Chat $chat)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Chat  $chat
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Chat $chat)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Chat  $chat
     * @return \Illuminate\Http\Response
     */
    public function destroy(Chat $chat)
    {
        //
    }
 
   

}
