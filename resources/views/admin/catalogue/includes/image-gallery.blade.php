<div class="modal fade" id="imageGallery" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                Image Gallery
            </div>
            <div class="modal-body">
                <div class="col-12">
                    <div class="row" id="range-images">
                        @foreach($images as $image)
                            <div id="{{$image->id}}" class="col- border-success m-2 ">
                                <img class="" onclick="selectImages({{$image->id}}, {{$image}})" width="150px" src="{{asset($image->path)}}" alt="" srcset="">
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="float-right">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">Done</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
