<?php

namespace App\Http\Controllers;
use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Packages;
use Auth;
use App\Packageoptions;

class TicketingController extends Controller
{
    public function show()
    {
    	// dd('xdfghjk');
    	// return view('ticketing');
    	$packages = Packages::where('status', 1)->get();
    	$option = Packageoptions::get();
    
         $data = [
             'packages'=> $packages,
             'option' =>$option,
             'breadcrumb' => 'packages List',

        ];
        // dd($data );
        return view('ticketing')->with($data);

    }
}
