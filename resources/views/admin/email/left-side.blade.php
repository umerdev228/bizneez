<div class="inbox-leftbar">

    <a href="{{ route('admin.email.create') }}" class="btn btn-danger btn-block waves-effect waves-light">Compose</a>

    <div class="mail-list mt-4">
        <a href="{{route('admin.email.inbox')}}" class="list-group-item border-0">
            <i class="mdi mdi-inbox font-18 align-middle mr-2"></i>
            Inbox
{{--            <span class="badge badge-danger float-right ml-2 mt-1">{{count($emails)}}</span>--}}
        </a>
{{--        <a href="#" class="list-group-item border-0"><i class="mdi mdi-star font-18 align-middle mr-2"></i>Starred</a>--}}
        <a href="{{route('admin.email.draft')}}" class="list-group-item border-0"><i class="mdi mdi-file-document-box font-18 align-middle mr-2"></i>
            Draft
{{--            <span class="badge badge-info float-right ml-2 mt-1">32</span>--}}
        </a>
        <a href="{{route('admin.email.sent')}}" class="list-group-item border-0"><i class="mdi mdi-send font-18 align-middle mr-2"></i>Sent Mail</a>
        <a href="#" class="list-group-item border-0"><i class="mdi mdi-delete font-18 align-middle mr-2"></i>Trash</a>
    </div>

    {{--                <h6 class="mt-5 m-b-15">Labels</h6>--}}

    {{--                <div class="list-group b-0 mail-list">--}}
    {{--                    <a href="#" class="list-group-item border-0"><span class="fa fa-circle text-info mr-2"></span>Web App</a>--}}
    {{--                    <a href="#" class="list-group-item border-0"><span class="fa fa-circle text-warning mr-2"></span>Recharge</a>--}}
    {{--                    <a href="#" class="list-group-item border-0"><span class="fa fa-circle text-purple mr-2"></span>Wallet Balance</a>--}}
    {{--                    <a href="#" class="list-group-item border-0"><span class="fa fa-circle text-pink mr-2"></span>Friends</a>--}}
    {{--                    <a href="#" class="list-group-item border-0"><span class="fa fa-circle text-success mr-2"></span>Family</a>--}}
    {{--                </div>--}}

</div>


