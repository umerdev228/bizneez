<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ManufactureRanges extends Model
{
     protected $table = 'manufacture_ranges';
    protected $guarded = [];

     protected $fillable = [
        'manfacture_categories_id', 'range1','range2','range3','range4',  
    ];
}
