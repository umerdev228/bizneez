@extends('admin.layouts.app')
@section('content')
    @include('admin.layouts.topbar')
    <!-- Google Chart JS -->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <!-- Start Page content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if(session()->has('message'))
                        <div class="alert alert-success" style="width:803px ">
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-9 mt-5">
                            <div class="card-box">
                                <form method="POST" action="{{ route('admin.architects.update', ['id' => $architect->id]) }}" enctype="multipart/form-data">
                                    @csrf
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="first_name">First Name<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control valid" name="first_name" value="{{$architect->first_name}}" id="first_name" required />
                                                @if ($errors->has('first_name'))
                                                    <span class="error">{{ $errors->first('first_name') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="middle_name">Middle Name<span class="text-danger"></span></label>
                                                <input type="text" class="form-control valid" name="middle_name" value="{{$architect->middle_name}}" id="middle_name"/>
                                                @if ($errors->has('middle_name'))
                                                    <span class="error">{{ $errors->first('middle_name') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="last_name">Last Name<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control valid" name="last_name" value="{{$architect->last_name}}" id="last_name" required />
                                                @if ($errors->has('last_name'))
                                                    <span class="error">{{ $errors->first('first_name') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="email">Email<span class="text-danger">*</span></label>
                                                <input type="email" class="form-control valid" name="email" value="{{$architect->email}}" id="email" required />
                                                @if ($errors->has('email'))
                                                    <span class="error">{{ $errors->first('email') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="phone">Phone<span class="text-danger">*</span></label>
                                                <input type="number" class="form-control valid" name="phone" value="{{$architect->profile->phone}}" id="phone" required />
                                                @if ($errors->has('phone'))
                                                    <span class="error">{{ $errors->first('phone') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="mobile">Mobile<span class="text-danger">*</span></label>
                                                <input type="number" class="form-control valid" name="mobile" value="{{$architect->profile->mobile}}" id="mobile" required />
                                                @if ($errors->has('mobile'))
                                                    <span class="error">{{ $errors->first('mobile') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="assistant_number">Assistant Number<span class="text-danger">*</span></label>
                                                <input type="number" class="form-control valid" name="assistant_number" value="{{$architect->profile->assistant_number}}" id="assistant_number" required />
                                                @if ($errors->has('assistant_number'))
                                                    <span class="error">{{ $errors->first('assistant_number') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="address">Address<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control valid" name="address" value="{{$architect->profile->address}}" id="address" required />
                                                @if ($errors->has('address'))
                                                    <span class="error">{{ $errors->first('address') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="location_id">Location<span class="text-danger">*</span></label>
                                                <input placeholder="Please Enter Location" type="text" class="form-control valid" name="location_id" value="" id="location_id"  />
                                                @if ($errors->has('location_id'))
                                                    <span class="error">{{ $errors->first('location_id') }}</span>

                                                @endif
                                            </div>
                                        </div>
                                          <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="area_id">Job Description<span class="text-danger">*</span></label>
                                                <input placeholder="Job Description Exp: Director" type="text" class="form-control valid" name="job_description" value="" id="job_description"  />
                                               
                                                @if ($errors->has('job_description'))
                                                    <span class="error">{{ $errors->first('job_description') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="created_by">Created By<span class="text-danger">*</span></label>
                                                <select class="form-control valid" name="created_by" id="created_by" required>
                                                    @foreach($users as $user)
                                                    <option @if($user->id == $architect->created_by) selected @endif value="{{ $user->id }}">{{ $user->name }}</option>
                                                    @endforeach
                                                </select>
                                                @if ($errors->has('created_by'))
                                                    <span class="error">{{ $errors->first('created_by') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="image">Company Logo<span class="text-danger">*</span></label>
                                                <input type="file" class="form-control valid" name="image" value="{{$architect->profile->image}}" id="image"/>
                                                @if ($errors->has('image'))
                                                    <span class="error">{{ $errors->first('image') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="Description">Description<span class="text-danger">*</span></label>
                                                <textarea  placeholder="Exp: We are architect of office furniture"  id="Description" class="form-control rounded-0" name="description" rows="5"> {{ $architect->profile->description }}</textarea>
                                                @if ($errors->has('description'))
                                                    <span class="error">{{ $errors->first('description') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group mt-3">

                                        <input class="btn btn-primary" type="submit" value="Update">
                                        <button onclick="history.back()" type="button" class="btn">Cancel</button>
                                    </div>
                                </form>
                            </div> <!-- end card-box -->
                        </div>

                    </div> <!-- container -->
                </div> <!-- content -->
            </div>
        </div>
    </div>

@endsection
