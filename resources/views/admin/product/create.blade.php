@extends('admin.layouts.app')
@section('content')
    @include('admin.layouts.topbar')

    <style>
        .dropzone {
            background: white;
            border-radius: 5px;
            border: 2px dashed rgb(0, 135, 247);
            border-image: none;
            width: 100%;
            /*max-width: 500px;*/
            margin-left: auto;
            margin-right: auto;
        }

        .bootstrap-filestyle {
            display: none;
        }
        .label-info {
            background-color: #5bc0de;
            display: inline-block;
            padding: 0.2em 0.6em 0.3em;
            font-size: 75%;
            font-weight: 700;
            line-height: 1;
            color: #fff;
            text-align: center;
            white-space: nowrap;
            vertical-align: baseline;
            border-radius: 0.25em;
        }

    </style>
    <!-- Google Chart JS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/min/dropzone.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/dropzone.js"></script>
    <!-- Start Page content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if(session()->has('message'))
                            <div class="alert alert-success" style="width:803px ">
                                {{ session()->get('message') }}
                            </div>
                        @endif
                        <form method="POST" action="{{ route('admin.products.store') }}" enctype="multipart/form-data">
                            @csrf

                        <div class="row">
                            <div class="col-xl-12">
                                <div class="card-box">
                                    <h4 class="header-title m-t-0 m-b-30"></h4>

                                    <ul class="nav nav-tabs tabs-bordered">
                                        <li class="nav-item">
                                            <a href="#home-b1" data-toggle="tab" aria-expanded="true" class="nav-link active">
                                                <i class="fi-monitor mr-2"></i> Create Products
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="#profile-b1" data-toggle="tab" aria-expanded="false" class="nav-link">
                                                <i class="fi-image mr-2"></i> Product Images Gallery
                                            </a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="home-b1">
                                            <div class="row">
                                                <div class="col-9 mt-5">
                                                    <div class="card-box">
                                                            <input type="text" style="display:none;" name="images" id="product_images">
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="form-group">
                                                                        <label for="manufacturer_name"> Manufacturer Name <span class="text-danger">*</span></label>
                                                                        <input type="text" class="form-control valid" name="manufacturer_name" id="manufacturer_name"  required />
                                                                        @if ($errors->has('manufacturer_name'))
                                                                            <span class="error">{{ $errors->first('manufacturer_name') }}</span>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                                <div class="col-12">
                                                                    <div class="form-group">
                                                                        <label for="name"> Name <span class="text-danger">*</span></label>
                                                                        <input type="text" class="form-control valid" name="name" id="name"  required />
                                                                        @if ($errors->has('name'))
                                                                            <span class="error">{{ $errors->first('name') }}</span>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                                <div class="col-12">
                                                                    <div class="form-group">
                                                                        <label for="price"> Net Price <span class="text-danger">*</span></label>
                                                                        <input type="text" class="form-control valid" name="price" id="price"  required />
                                                                        @if ($errors->has('price'))
                                                                            <span class="error">{{ $errors->first('price') }}</span>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                                <div class="col-12">
                                                                    <div class="form-group">
                                                                        <label for="second_price"> Retail Price <span class="text-danger">*</span></label>
                                                                        <input type="text" class="form-control valid" name="second_price" id="second_price"  required />
                                                                        @if ($errors->has('second_price'))
                                                                            <span class="error">{{ $errors->first('second_price') }}</span>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                                <div class="col-12">
                                                                    <div class="form-group">
                                                                        <label for="discount"> Discount <span class="text-danger">*</span></label>
                                                                        <input type="text" class="form-control valid" name="discount" id="discount"  required />
                                                                        @if ($errors->has('discount'))
                                                                            <span class="error">{{ $errors->first('discount') }}</span>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                                <div class="col-12">
                                                                    <div class="form-group">
                                                                        <label for="quantity"> Quantity <span class="text-danger">*</span></label>
                                                                        <input type="text" class="form-control valid" name="quantity" id="quantity"  required />
                                                                        @if ($errors->has('quantity'))
                                                                            <span class="error">{{ $errors->first('quantity') }}</span>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                                <div class="col-12">
                                                                    <div class="form-group">
                                                                        <label for="Description">Description<span class="text-danger">*</span></label>
                                                                        <textarea id="Description" class="form-control rounded-0" name="description" rows="5"></textarea>
                                                                        @if ($errors->has('description'))
                                                                            <span class="error">{{ $errors->first('description') }}</span>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                                <div class="col-12">
                                                                    <div class="form-group">
                                                                        <label for="name"> Categories <span class="text-danger">*</span></label>
                                                                        <input type="text" class="form-control valid" name="categories[]" id="category" required />
                                                                    </div>
                                                                </div>
                                                                <div class="col-12">
                                                                    <div class="form-group">
                                                                        <label for="size"> Size  <span class="text-danger">*</span> <small class="text-success">Enter Commas Seprated Sizes</small></label>
                                                                        <input class="form-control valid" type="text" name="size" id="size" required>
                                                                    </div>
                                                                </div>
                                                                <div class="col-12">
                                                                    <div class="form-group">
                                                                        <label for="range"> Range Types  <span class="text-danger">*</span></label>
                                                                        <select name="range" id="range" class="form-control valid">
                                                                            <option value="0">Choose Range</option>
                                                                            <option value="1">Range 1</option>
                                                                            <option value="2">Range 2</option>
                                                                            <option value="3">Range 3</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-12">
                                                                    <div class="form-group">
                                                                        <label for="range"> Color Type  <span class="text-danger">*</span></label>
                                                                        <select name="color_type" id="range" class="form-control valid" >
                                                                            <option value="0">Choose Color Type</option>
                                                                            <option value="1">Desktop</option>
                                                                            <option value="2">Leg Frame</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-12">
                                                                    <div class="form-group">
                                                                        <label for="color"> Colors <span class="text-danger"></span></label>
                                                                        <div id="dropzone">
                                                                            <div class="dropzone needsclick" id="colors-upload">
                                                                                <div class="dz-message needsclick">
                                                                                    Upload Your Product Color Images
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <br/>
                                                                        <div id="preview-color-image" style="display: none;">
                                                                            <div class="dz-preview dz-file-preview">
                                                                                <div class="dz-image">
                                                                                    <img data-dz-thumbnail=""/>
                                                                                </div>
                                                                                <div class="dz-details">
                                                                                    <div class="dz-size"><span data-dz-size=""></span></div>
                                                                                    <div class="dz-filename"><span data-dz-name=""></span></div></div>
                                                                                <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress=""></span></div>
                                                                                <div class="dz-error-message"><span data-dz-errormessage=""></span></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                {{--                                                                <div class="col-12">--}}
                                                                {{--                                                                    <div class="form-group">--}}
                                                                {{--                                                                        <label for="image"> Images <span class="text-danger"></span></label>--}}
                                                                {{--                                                                        <div id="dropzone">--}}
                                                                {{--                                                                            <div class="dropzone needsclick" id="image-upload">--}}
                                                                {{--                                                                                <div class="dz-message needsclick">--}}
                                                                {{--                                                                                    Upload Your Product Images--}}
                                                                {{--                                                                                </div>--}}
                                                                {{--                                                                            </div>--}}
                                                                {{--                                                                        </div>--}}
                                                                {{--                                                                        <br/>--}}
                                                                {{--                                                                        <div id="preview-image" style="display: none;">--}}
                                                                {{--                                                                            <div class="dz-preview dz-file-preview">--}}
                                                                {{--                                                                                <div class="dz-image">--}}
                                                                {{--                                                                                    <img data-dz-thumbnail=""/>--}}
                                                                {{--                                                                                </div>--}}
                                                                {{--                                                                                <div class="dz-details">--}}
                                                                {{--                                                                                    <div class="dz-size"><span data-dz-size=""></span></div>--}}
                                                                {{--                                                                                    <div class="dz-filename"><span data-dz-name=""></span></div></div>--}}
                                                                {{--                                                                                <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress=""></span></div>--}}
                                                                {{--                                                                                <div class="dz-error-message"><span data-dz-errormessage=""></span></div>--}}
                                                                {{--                                                                            </div>--}}
                                                                {{--                                                                        </div>--}}
                                                                {{--                                                                    </div>--}}
                                                                {{--                                                                </div>--}}
                                                            </div>

                                                    </div> <!-- end card-box -->
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane show" id="profile-b1">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="card-box">
                                                        @include('admin.product.gallery.gallery')
                                                    </div>
                                                </div><!-- end col -->
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div> <!-- end col -->
                        </div>
                        </form>
                </div> <!-- content -->
            </div>
        </div>
    </div>
    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myLargeModalLabel">Upload Products Images</h4>
                </div>
                <div class="modal-body">
                    <div class="col-12">
                        <div class="form-group">
                            <label for="image"> Images <span class="text-danger"></span></label>
                            <div id="dropzone">
                                <div class="dropzone needsclick" id="gallery-image-upload">
                                    <div class="dz-message needsclick">
                                        Upload Products Images
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <div id="gallery-preview-image" style="display: none;">
                                <div class="dz-preview dz-file-preview">
                                    <div class="dz-image">
                                        <img data-dz-thumbnail=""/>
                                    </div>
                                    <div class="dz-details">
                                        <div class="dz-size"><span data-dz-size=""></span></div>
                                        <div class="dz-filename"><span data-dz-name=""></span></div></div>
                                    <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress=""></span></div>
                                    <div class="dz-error-message"><span data-dz-errormessage=""></span></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <style href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/dropzone.css"></style>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/dropzone.js"></script>


    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-beta/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
    <link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/examples/assets/app.css">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.0.4/popper.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.11.1/typeahead.bundle.min.js"></script>
    <script src="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>

    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>

    <script>

        var data = '{!! json_encode($categories) !!}'
        console.log(data)
        //get data pass to json
        var task = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace("name"),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            local: jQuery.parseJSON(data) //your can use json type
        });

        task.initialize();

        var elt = $("#category");
        elt.tagsinput({
            itemValue: "id",
            itemText: "name",
            typeaheadjs: {
                name: "name",
                displayKey: "name",
                source: task.ttAdapter()
            }
        });


        Dropzone.autoDiscover = false;
        // $(document).ready(function() {
        {{--var dropzone = new Dropzone(document.getElementById('image-upload'), {--}}
        {{--    url: "{{route('product.image.update')}}", // Set the url--}}
        {{--    previewTemplate: document.querySelector('#preview-image').innerHTML,--}}
        {{--    parallelUploads: 2,--}}
        {{--    thumbnailHeight: 120,--}}
        {{--    thumbnailWidth: 120,--}}
        {{--    maxFilesize: 3,--}}
        {{--    headers: {--}}
        {{--        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')--}}
        {{--    },--}}
        {{--    filesizeBase: 1000,--}}
        {{--    thumbnail: function(file, dataUrl) {--}}
        {{--        if (file.previewElement) {--}}
        {{--            file.previewElement.classList.remove("dz-file-preview");--}}
        {{--            var images = file.previewElement.querySelectorAll("[data-dz-thumbnail]");--}}
        {{--            for (var i = 0; i < images.length; i++) {--}}
        {{--                var thumbnailElement = images[i];--}}
        {{--                thumbnailElement.alt = file.name;--}}
        {{--                thumbnailElement.src = dataUrl;--}}
        {{--            }--}}
        {{--            setTimeout(function() { file.previewElement.classList.add("dz-image-preview"); }, 1);--}}
        {{--        }--}}
        {{--    }--}}

        {{--});--}}

        var dropzone = new Dropzone(document.getElementById('colors-upload'), {
            url: "{{route('product.color.image.update')}}", // Set the url
            previewTemplate: document.querySelector('#preview-color-image').innerHTML,
            parallelUploads: 2,
            thumbnailHeight: 120,
            thumbnailWidth: 120,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            filesizeBase: 1000,
            thumbnail: function(file, dataUrl) {
                if (file.previewElement) {
                    file.previewElement.classList.remove("dz-file-preview");
                    var images = file.previewElement.querySelectorAll("[data-dz-thumbnail]");
                    for (var i = 0; i < images.length; i++) {
                        var thumbnailElement = images[i];
                        thumbnailElement.alt = file.name;
                        thumbnailElement.src = dataUrl;
                    }
                    setTimeout(function() { file.previewElement.classList.add("dz-image-preview"); }, 1);
                }
            }
        });

        //    Gallery Uploading
        var dropzone = new Dropzone(document.getElementById('gallery-image-upload'), {
            url: "{{route('gallery.products.images.upload')}}", // Set the url
            previewTemplate: document.querySelector('#gallery-preview-image').innerHTML,
            parallelUploads: 2,
            thumbnailHeight: 120,
            thumbnailWidth: 120,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            filesizeBase: 1000,
            thumbnail: function(file, dataUrl) {
                if (file.previewElement) {
                    file.previewElement.classList.remove("dz-file-preview");
                    var images = file.previewElement.querySelectorAll("[data-dz-thumbnail]");
                    for (var i = 0; i < images.length; i++) {
                        var thumbnailElement = images[i];
                        thumbnailElement.alt = file.name;
                        thumbnailElement.src = dataUrl;
                    }
                    setTimeout(function() { file.previewElement.classList.add("dz-image-preview"); }, 1);
                }
            }

        });

        function addToSessions(id) {
            $("#"+id).addClass("text-success");
            // Request a new token
            axios.post('{{route('add.to.session')}}', {
                'id': id,
            })
                .then(function (response) {
                    console.log(response.data)
                    if (response.data.type === 'add') {
                        $("#"+id).addClass("text-success");
                    }
                    if (response.data.type === 'remove') {
                        $("#"+id).removeClass("text-success");
                    }
                })
                .catch(function (error) {
                    console.log(error);
                })
                .then(function () {
                    console.log('hello world')
                });
        }
    </script>
@endsection
