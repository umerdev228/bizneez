@extends('layout.app')
@section('content')
<main>
    <br /><br /><br /><br /><br /><br />
    <form method="get" name="redirectFrm" action="{{ env('APP_URL')}}upgrade_user" id="redirectFrm">
        <input type="hidden" name="expire_date" value="{{$expire_date}}"  />
        <input type="hidden" name="inttype" value="1"  />
        <input type="hidden" name="user_id" value="{{$user_id}}">
    </form>
   



<div class="container">    
    <p align="center"><div align="center" class="loader"></div></p><br/>
    <div class="d-flex justify-content-center"> 
        <div class="alert alert-success" role="alert" id="myElem">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
            You are subscribed successfully.
        </div>
        
    </div>
</div>    
    <br /><br /><br /><br /><br /><br />
    
</main>
@endsection




<style>
.loader {
border: 16px solid #f3f3f3;
border-radius: 50%;
border-top: 16px solid #3498db;
width: 120px;
height: 120px;
-webkit-animation: spin 2s linear infinite; /* Safari */
animation: spin 2s linear infinite;
    margin-left: 45%;
}
/* Safari */
@-webkit-keyframes spin {
0% { -webkit-transform: rotate(0deg); }
100% { -webkit-transform: rotate(360deg); }
}
@keyframes spin {
0% { transform: rotate(0deg); }
100% { transform: rotate(360deg); }
}
</style>
<script>
 window.onload=function(){
  window.setTimeout(function() { 
            func_logout();
            document.redirectFrm.submit(); 

        }, 2000);

}; 




function func_logout(){
     
    var url_call = "{!! url('/') !!}/logout";   

    $.ajax({ 
                type : 'get',
                url  : url_call,  
                 
                contentType: 'application/json; charset=utf-8',  
                success :  function(data){  
                 } 
    });//ajax
 


}
</script>