@extends('layout.app')
@section('content')
<div class="support-company-area p-5 fix">
            <div class="container">
                <div class="row align-items-center">
                    
                    <div class="col-xl-5 col-lg-6">
                        <div class="support-location-img">
                            <img src="theme/assets/img/login.png" alt="">
                            
                        </div>
                    </div>
                    
                    <div class="col-xl-7 col-lg-6">
                        <div class="right-caption">
                            
                         @if(session()->has('message'))
                            <div class="alert alert-success">
                                {{ session()->get('message') }}
                            </div>
                            @endif
                            @if(session()->has('error'))
                            <div class="alert alert-danger">
                                {{ session()->get('error') }}
                            </div>
                         @endif


                             
  <form class="form-contact contact_form"  method="POST" action="{{url('password/email')}}" >
                     @csrf
                        <div class="row">
                            <div class="col-lg-9">
                                <div class="section-tittle mb-20">
                                    <h2 class="mb-0">Reset Password</h2>
                                </div>
        <div class="form-group">
                                   <input value="{{ old('email') }}" class="form-control valid" name="email" id="email" type="email" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter your email'" placeholder="Enter your email" required="required" />
                                                 @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                 @enderror
                                                 <br>
<input  class="form-control valid" name="password" id="password" type="password" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter your password'" placeholder="Enter the password" required="required" />
<br>
<input  class="form-control valid" name="confirm password" id="confirm password" type="confirm password" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter your confirm password'" placeholder="Enter the confirm password" required="required" />

                                           
                                            </div>
                            </div>
                                         
                        </div>
                                    <div class="form-group mt-1">
                                        <input type="submit" class="genric-btn info" value="Send Password"> 
                                    </div>
                                    <small><br/>
  <small class="ml-10"><a href="signup">Dont Have Account? Please Signup</a></small>  
                    </form> 
                    
                        </div> 
                    </div> 
                </div>
            </div>
        </div>


    @endsection