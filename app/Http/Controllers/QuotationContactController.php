<?php

namespace App\Http\Controllers;

use App\QuotationContact;
use Illuminate\Http\Request;

class QuotationContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\QuotationContact  $quotationContact
     * @return \Illuminate\Http\Response
     */
    public function show(QuotationContact $quotationContact)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\QuotationContact  $quotationContact
     * @return \Illuminate\Http\Response
     */
    public function edit(QuotationContact $quotationContact)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\QuotationContact  $quotationContact
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, QuotationContact $quotationContact)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\QuotationContact  $quotationContact
     * @return \Illuminate\Http\Response
     */
    public function destroy(QuotationContact $quotationContact)
    {
        //
    }
}
