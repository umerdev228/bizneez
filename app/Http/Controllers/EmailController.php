<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Email;
use App\SiteSetting;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class EmailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $breadcrumb = 'CRM / Emails';
        $emails = Email::with('contact')->latest()->get();
        return view('admin.email.inbox', compact('emails', 'breadcrumb'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $breadcrumb = 'CRM / Create Events';
        $contacts = Contact::all();
        $emails = Email::with('contact')->get();

        return view('admin.email.create', compact('contacts', 'emails', 'breadcrumb'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $settings = SiteSetting::where('key', 'cc_email')->first();

        foreach ($request->to as $to) {
            $email = Contact::where('id', $to)->first();
            Email::create([
                'from' => Auth::id(),
                'to' => $email->email,
                'subject' => $request->subject,
                'body' => $request->body,
                'signature' => $request->signature,
                'status' => 'inbox'
            ]);

            $data["email"] = $email->email;
            $data["title"] = $request->subject;
            $data["body"] = $request->body;
            $data["cc"] = $settings['data'];


            Mail::send('mail.custom_mail', $data, function($message)use($data) {
                $message
                    ->to($data["email"], $data["email"])
                    ->cc($data["cc"], $data["email"])
                    ->subject($data["title"]);
            });
        }

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Email  $email
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $email = Email::where('id', $id)->with('contact')->first();
        $emails = Email::with('contact')->get();
        $breadcrumb = 'CRM / Email';

        return view('admin.email.read', compact('email', 'emails', 'breadcrumb'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Email  $email
     * @return \Illuminate\Http\Response
     */
    public function edit(Email $email)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Email  $email
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Email $email)
    {
        //
        $categories = explode(',', $request->ids);
        dd($categories, $request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Email  $email
     * @return \Illuminate\Http\Response
     */
    public function destroy(Email $email)
    {
        //
    }



    public function sent() {
        $emails = Email::where('from', Auth::user()->id)->with('contact')->latest()->get();
        $breadcrumb = 'CRM / Emails';
        return view('admin.email.sent', compact('emails', 'breadcrumb'));
    }


    public function draft() {
        $emails = Email::where('to', Auth::user()->email)->where('status', 'draft')->with('contact')->latest()->get();
        $breadcrumb = 'CRM / Emails';
        return view('admin.email.sent', compact('emails', 'breadcrumb'));
    }


    public function trash() {
        $emails = Email::where('to', Auth::user()->email)->where('status', 'trash')->with('contact')->latest()->get();
        $breadcrumb = 'CRM / Emails';
        return view('admin.email.sent', compact('emails', 'breadcrumb'));
    }

    public function updateDraft(Request $request) {
        $emails = explode(',', $request->ids);
        foreach ($emails as $email) {
            if ($email !== "") {
                Email::where('id', $email)->update(['status' => 'draft']);
            }
        }
        toastr()->success('Added To Draft Successfully');
        return redirect()->back();
    }


    public function deleteCreate(Request $request) {
        $emails = explode(',', $request->ids);
        foreach ($emails as $email) {
            if ($email !== "") {
                Email::where('id', $email)->update(['status' => 'trash']);
            }
        }
        toastr()->success('Added To Delete Successfully');
        return redirect()->back();
    }

    public function singleDraft(Request $request){
        Email::where('id', $request->id)->update(['status' => 'draft']);
        toastr()->success('Added To Draft Successfully');

        return redirect()->back();

    }
    public function singleDelete(Request $request){
        Email::where('id', $request->id)->update(['status' => 'trash']);
        toastr()->success('Added To Delete Successfully');
        return redirect()->back();

    }
    public function reply(Request $request) {
        $email = Email::where('id', $request->id)->first();
        $user = User::where('id', $email->from)->first();
        $email = $user->email;
        return view('admin.email.reply', compact('email'));
    }
    public function replySend(Request $request) {

        Email::create([
            'from' => Auth::id(),
            'to' => $request->to,
            'subject' => $request->subject,
            'body' => $request->body,
            'status' => 'inbox'
        ]);

        $data["email"] = $request->to;
        $data["title"] = $request->subject;
        $data["body"] = $request->body;


        Mail::send('mail.custom_mail', $data, function($message)use($data) {
            $message->to($data["email"], $data["email"])
                ->subject($data["title"]);
        });
        toastr()->success('Reply Send Successfully');

        return redirect()->route('admin.email.inbox');
    }

}
