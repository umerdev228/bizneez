@extends('includes.meeting_section.master')

@section('content')
    <div  style="margin-top: 70px !important;"></div>
    <div class="col-lg-12">
        <div class="card-box">
            <!-- Left sidebar -->
        @include('admin.email.left-side')
        <!-- End Left sidebar -->

            <div class="inbox-rightbar">

                <div class="" role="toolbar">
                    <div class="btn-group">
                        <form action="{{route('email.delete.create')}}" method="post">
                            @csrf
                            <input type="hidden" name="ids" id="ids1">
                            <button type="submit" class="btn btn-sm btn-light waves-effect" data-toggle="tooltip" data-placement="top" title="Delete">
                                <i class="mdi mdi-archive font-18 vertical-middle"></i>
                            </button>
                        </form>
                        {{--                    <form action="{{route('email.action')}}" method="post">--}}
                        {{--                        @csrf--}}
                        {{--                        <input type="hidden" name="ids" id="ids">--}}
                        {{--                        <button type="submit" class="btn btn-sm btn-light waves-effect" data-toggle="tooltip" data-placement="top" title="Tooltip on top">--}}
                        {{--                            <i class="mdi mdi-alert-octagon font-18 vertical-middle"></i>--}}
                        {{--                        </button>--}}
                        {{--                    </form>--}}
                        <form action="{{route('email.draft.create')}}" method="post">
                            @csrf
                            <input type="hidden" name="ids" id="ids2">
                            <button type="submit" class="btn btn-sm btn-light waves-effect" data-toggle="tooltip" data-placement="top" title="Add To Draft">
                                <i class="mdi mdi-delete-variant font-18 vertical-middle"></i>
                            </button>
                        </form>
                    </div>
                    {{--                <div class="btn-group">--}}
                    {{--                    <button type="button" class="btn btn-sm btn-light dropdown-toggle waves-effect" data-toggle="dropdown" aria-expanded="false">--}}
                    {{--                        <i class="mdi mdi-folder font-18 vertical-middle"></i>--}}
                    {{--                        <b class="caret m-l-5"></b>--}}
                    {{--                    </button>--}}
                    {{--                    <div class="dropdown-menu">--}}
                    {{--                        <span class="dropdown-header">Move to</span>--}}
                    {{--                        <a class="dropdown-item" href="javascript: void(0);">Social</a>--}}
                    {{--                        <a class="dropdown-item" href="javascript: void(0);">Promotions</a>--}}
                    {{--                        <a class="dropdown-item" href="javascript: void(0);">Updates</a>--}}
                    {{--                        <a class="dropdown-item" href="javascript: void(0);">Forums</a>--}}
                    {{--                    </div>--}}
                    {{--                </div>--}}
                    {{--                <div class="btn-group">--}}
                    {{--                    <button type="button" class="btn btn-sm btn-light dropdown-toggle waves-effect" data-toggle="dropdown" aria-expanded="false">--}}
                    {{--                        <i class="mdi mdi-label font-18 vertical-middle"></i>--}}
                    {{--                        <b class="caret m-l-5"></b>--}}
                    {{--                    </button>--}}
                    {{--                    <div class="dropdown-menu">--}}
                    {{--                        <span class="dropdown-header">Label as:</span>--}}
                    {{--                        <a class="dropdown-item" href="javascript: void(0);">Updates</a>--}}
                    {{--                        <a class="dropdown-item" href="javascript: void(0);">Social</a>--}}
                    {{--                        <a class="dropdown-item" href="javascript: void(0);">Promotions</a>--}}
                    {{--                        <a class="dropdown-item" href="javascript: void(0);">Forums</a>--}}
                    {{--                    </div>--}}
                    {{--                </div>--}}

                    {{--                <div class="btn-group">--}}
                    {{--                    <button type="button" class="btn btn-sm btn-light dropdown-toggle waves-effect" data-toggle="dropdown" aria-expanded="false">--}}
                    {{--                        <i class="mdi mdi-dots-horizontal font-18 vertical-middle"></i> More--}}
                    {{--                    </button>--}}
                    {{--                    <div class="dropdown-menu">--}}
                    {{--                        <span class="dropdown-header">More Option :</span>--}}
                    {{--                        <a class="dropdown-item" href="javascript: void(0);">Mark as Unread</a>--}}
                    {{--                        <a class="dropdown-item" href="javascript: void(0);">Add to Tasks</a>--}}
                    {{--                        <a class="dropdown-item" href="javascript: void(0);">Add Star</a>--}}
                    {{--                        <a class="dropdown-item" href="javascript: void(0);">Mute</a>--}}
                    {{--                    </div>--}}
                    {{--                </div>--}}
                </div>

                <div class="">
                    <div class="mt-4">
                        <div class="">
                            <ul class="message-list">
                                @foreach($emails as $index => $email)
                                    <li>
                                        {{--                            <li class="unread">--}}
                                        <div class="col-mail col-mail-1">
                                            <div class="checkbox-wrapper-mail">
                                                <input onclick="addToCheckList({{$email->id}})" type="checkbox" value="{{$email->id}}" id="chk-{{$index}}">
                                                <label for="chk-{{$index}}" class="toggle"></label>
                                            </div>
                                            <a href="{{route('admin.email.read', ['id' => $email->id])}}" class="title">{{$email->subject}}</a>
                                            {{--                                    <span class="star-toggle fa fa-star-o"></span>--}}
                                        </div>
                                        <div class="col-mail col-mail-2">
                                            <a href="{{route('admin.email.read', ['id' => $email->id])}}" class="subject">{{ $email->contact->name }} &nbsp;&ndash;&nbsp;
                                                {{--                                        <span class="teaser">--}}
                                                {{--                                            {{$email->body}}--}}
                                                {{--                                        </span>--}}
                                            </a>
                                            <div class="date">Mar 8</div>
                                        </div>
                                    </li>
                                @endforeach

                            </ul>
                        </div>

                    </div> <!-- card body -->
                </div> <!-- card -->

                <div class="row">


{{--                    <div class="col-5">--}}
{{--                        <div class="btn-group float-right">--}}
{{--                            <button type="button" class="btn btn-custom waves-light waves-effect btn-sm"><i class="fa fa-chevron-left"></i></button>--}}
{{--                            <button type="button" class="btn btn-custom waves-effect waves-light btn-sm"><i class="fa fa-chevron-right"></i></button>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

    <script>
        function addToCheckList(id) {
            console.log($('#ids').val())
            let vall1 = $('#ids1').val()
            let vall2 = $('#ids2').val()
            let data1  = id+','+vall1
            let data2  = id+','+vall2
            $('#ids1').val(data1)
            $('#ids2').val(data2)

        }
    </script>
@endsection