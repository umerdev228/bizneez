<!doctype>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ config('app.name', 'Bizneez') }}</title>
    <link href="{{asset('bootstrap-3-3-6/bootstrap-3.3.6-dist/css/bootstrap.min.css')}}" rel="stylesheet">
</head>
<body>

<div>
    <div>
        <div class="">
            <div class="container">

                    <div id="template" class="row">
                        @for($i = 1; $i <= $data['page']; $i++)
                        <div id="p1" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <h3>Catalogue</h3>
                                </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
{{--                                <div class="col-lg-3 hidden-print">--}}
{{--                                    <div>--}}
{{--                                        <div class="form-group">--}}
{{--                                            <select class="form-control" required>--}}
{{--                                                <option value="0">--Select Manufacture--</option>--}}
{{--                                                @foreach($manufacturers as $manufacturer)--}}
{{--                                                    <option @if($data['manufacturer'.$i] == $manufacturer->id) selected @endif  value="{{$manufacturer->id}}">{{$manufacturer->name}}</option>--}}
{{--                                                @endforeach--}}
{{--                                            </select>--}}
{{--                                            <br>--}}

{{--                                            <select class="form-control" name="range1" id="range1" required>--}}
{{--                                                <option>--Select Ranges--</option>--}}
{{--                                                @foreach($ranges as $range)--}}
{{--                                                    <option @if($data['range'.$i] == $range->id) selected @endif  value="{{$range->id}}">{{$range->name}}</option>--}}
{{--                                                @endforeach--}}
{{--                                            </select>--}}
{{--                                        </div>--}}
{{--                                        <ul style="list-style: none">--}}
{{--                                            <li>--}}
{{--                                                <input @if($data['option_'.$i.'_1']) checked @endif type="checkbox" class="form-check form-check-inline">--}}
{{--                                                <label for="option_1_1">Option 1</label>--}}
{{--                                            </li>--}}
{{--                                            <li>--}}
{{--                                                <input @if($data['option_'.$i.'_2']) checked @endif type="checkbox" class="form-check form-check-inline">--}}
{{--                                                <label for="option_1_2">Option 2</label>--}}
{{--                                            </li>--}}
{{--                                            <li>--}}
{{--                                                <input @if($data['option_'.$i.'_3']) checked @endif type="checkbox" class="form-check form-check-inline">--}}
{{--                                                <label for="option_1_3">Option 3</label>--}}
{{--                                            </li>--}}
{{--                                            <li>--}}
{{--                                                <input @if($data['option_'.$i.'_4']) checked @endif type="checkbox" class="form-check form-check-inline">--}}
{{--                                                <label for="option_1_4">Option 4</label>--}}
{{--                                            </li>--}}
{{--                                            <li>--}}
{{--                                                <input @if($data['option_'.$i.'_5']) checked @endif type="checkbox" class="form-check form-check-inline">--}}
{{--                                                <label for="option_1_5">Option 5</label>--}}
{{--                                            </li>--}}
{{--                                            <li>--}}
{{--                                                <input @if($data['option_'.$i.'_6']) checked @endif type="checkbox" class="form-check form-check-inline">--}}
{{--                                                <label for="option_1_6">Option 6</label>--}}
{{--                                            </li>--}}
{{--                                            <li>--}}
{{--                                                <input @if($data['option_'.$i.'_7']) checked @endif type="checkbox" class="form-check form-check-inline">--}}
{{--                                                <label for="option_1_7">Option 7</label>--}}
{{--                                            </li>--}}
{{--                                            <li>--}}
{{--                                                <input @if($data['option_'.$i.'_8']) checked @endif type="checkbox" class="form-check form-check-inline">--}}
{{--                                                <label for="option_1_8">Option 8</label>--}}
{{--                                            </li>--}}
{{--                                            <li>--}}
{{--                                                <input @if($data['option_'.$i.'_9']) checked @endif type="checkbox" class="form-check form-check-inline">--}}
{{--                                                <label for="option_1_9">Option 9</label>--}}
{{--                                            </li>--}}
{{--                                        </ul>--}}

{{--                                        <div class="">--}}
{{--                                            <button class="btn btn-danger" type="button" onclick="printPage()">Print</button>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
                                <div id="pdf-section" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 border" style="border: 2px solid gray">
                                    <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                @if(isset($data['title_check'.$i]))
                                                    @if($data['title_check'.$i] == 'on')
{{--                                                <label for="title1">Please Enter Text</label>--}}
{{--                                                        <input value="{{$data['title'.$i]}}" required type="text" name="title1" class="form-control" id="title1">--}}
                                                        <h5>{{$data['title'.$i]}}</h5>
                                                    @endif
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">

                                                @php
                                                $company = \App\Company::where('id', $data['company'.$i])->first();

                                                $image1 = \App\Media::where('id', $data['imageValue-'.$i.'-1'])->first()->path;
                                                $image2 = \App\Media::where('id', $data['imageValue-'.$i.'-2'])->first()->path;
                                                $image3 = \App\Media::where('id', $data['imageValue-'.$i.'-3'])->first()->path;
                                                $image4 = \App\Media::where('id', $data['imageValue-'.$i.'-4'])->first()->path;
                                                $image5 = \App\Media::where('id', $data['imageValue-'.$i.'-5'])->first()->path;
                                                $image6 = \App\Media::where('id', $data['imageValue-'.$i.'-6'])->first()->path;
                                                $image7 = \App\Media::where('id', $data['imageValue-'.$i.'-7'])->first()->path;
                                                $image8 = \App\Media::where('id', $data['imageValue-'.$i.'-8'])->first()->path;
                                                $image9 = \App\Media::where('id', $data['imageValue-'.$i.'-9'])->first()->path;
                                                @endphp

{{--                                                <label for="logo1">Select Logo</label>--}}
                                                <img width="100px" src="{{ asset($company['logo']) }}" alt="{{$company['name']}}" srcset="">
                                            </div>
                                        </div>
                                    </div>
                                    </div>


                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3 mb-3">
                                                <div class=" div1">
                                                    <img src="{{asset($image1)}}" alt="image" width="100px">
                                                    <p id="description-1-1" >{{ $data['description-'.$i.'-1'] }}</p>
{{--                                                    <input type="button" value="Price List" class="btn btn-dark"> --}}
                                                    <br>
                                                </div>
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <div class=" div2">
                                                    <img src="{{asset($image2)}}" alt="image" width="100px">
                                                    <p id="description-1-2" >{{ $data['description-'.$i.'-2'] }}</p>
{{--                                                    <input type="button" value="Price List" class="btn btn-dark">--}}
                                                    <br>
                                                </div></div>
                                            <div class="col-md-3 mb-3">
                                                <div class=" div3">
                                                    <img src="{{asset($image3)}}" alt="image" width="100px">
                                                    <p id="description-1-3" >{{ $data['description-'.$i.'-3'] }}</p>
{{--                                                    <input type="button" value="Price List" class="btn btn-dark">--}}
                                                    <br>
                                                </div>
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <div class=" div4">
                                                    <img src="{{asset($image4)}}" alt="image" width="100px">
                                                    <p id="description-1-4" >{{ $data['description-'.$i.'-4'] }}</p>
{{--                                                    <input type="button" value="Price List" class="btn btn-dark">--}}
                                                    <br>
                                                </div>
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <div class=" div5">
                                                    <img src="{{asset($image5)}}" alt="image" width="100px">
                                                    <p id="description-1-5" >{{ $data['description-'.$i.'-5'] }}</p>
{{--                                                    <input type="button" value="Price List" class="btn btn-dark">--}}
                                                    <br>
                                                </div>
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <div class=" div6">
                                                    <img src="{{asset($image6)}}" alt="image" width="100px">
                                                    <p id="description-1-6" >{{ $data['description-'.$i.'-6'] }}</p>
{{--                                                    <input type="button" value="Price List" class="btn btn-dark"> --}}
                                                    <br>
                                                </div>
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <div class="div7">
                                                    <img src="{{asset($image7)}}" alt="image" width="100px">
                                                    <p id="description-1-7" >{{ $data['description-'.$i.'-7'] }}</p>
{{--                                                    <input type="button" value="Price List" class="btn btn-dark">--}}
                                                    <br>
                                                </div>
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <div class="div8">
                                                    <img src="{{asset($image8)}}" alt="image" width="100px">
                                                    <p id="description-1-8" >{{ $data['description-'.$i.'-8'] }}</p>
{{--                                                    <input type="button" value="Price List" class="btn btn-dark">--}}
                                                    <br>
                                                </div></div>
                                            <div class="col-md-3 mb-3">
                                                <div class="div9">
                                                    <img src="{{asset($image9)}}" alt="image" width="100px">
                                                    <p id="description-1-9" >{{ $data['description-'.$i.'-9'] }}</p>
{{--                                                    <input type="button" value="Price List" class="btn btn-dark"> --}}
                                                    <br>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
                                        <div class="row text-light" style="background-color: #1d1e1f; width: 100%;">
                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 pull-left">{{$company->url}}</div>
                                            <!--   <div class="col-lg-3"><a href="">Unsubscribe</a></div> -->
                                            <div class="col-lg-6">{{$company->address}}</div>
                                            <div class="col-lg-3 pull-right">
                                                {{$company->phone}}
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                </div>
                            </div>
                            </div>
                        </div>
                        @endfor
                    </div>
            </div> <!-- container -->
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="{{asset('bootstrap-3-3-6/bootstrap-3.3.6-dist/js/bootstrap.min.js')}}"></script>
</body>

</html>
