<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class TicketStatusSpam extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
   public $name;
   public $ticket_status;
   public $title;
   public $ticket_id;
   public $description;
     

    public function __construct($name,$title,$ticket_status,$description,$ticket_id)
    {
         
        $this->name = $name;
        $this->ticket_status = $ticket_status;
        $this->title = $title;
        $this->ticket_id = $ticket_id;
        $this->description = $description;
 
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
         return $this->view('mail_ticket_status_spam', ['status' => $this->ticket_status,'description'=>$this->description, 'ticket_id' => $this->ticket_id ]);
         //   return view('pages_merchant_admin.customer_list', compact('customer_list','title'));
    }
}
