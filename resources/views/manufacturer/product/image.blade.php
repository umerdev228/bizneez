<div class="col-12">
    <div class="form-group">
        <form method="post" action="{{  route("admin.products.image.upload", ['id' => $product->id])  }}" enctype="multipart/form-data"
              class="dropzone" id="dropzone">
            @csrf
        </form>
    </div>
</div>

<script type="text/javascript">
    Dropzone.options.dropzone =
        {
            renameFile: function(file) {
                var dt = new Date();
                var time = dt.getTime();
                return time+file.name;
            },
            addRemoveLinks: true,
            timeout: 50000,
            removedfile: function(file)
            {
                var name = file.name
                $.ajax({
                    type: 'GET',
                    url: '/admin/products/image/remove/'+file.id,
                    data: {"file": file},
                    processData: false,
                    contentType: false,
                    success: function (data){
                    },
                    error: function(e) {
                    }});
                var fileRef;
                return (fileRef = file.previewElement) != null ?
                    fileRef.parentNode.removeChild(file.previewElement) : void 0;
            },
            success: function(file, response)
            {

            },
            error: function(file, response)
            {
                return false;
            },
            init: function () {
                var public_path = '{{ asset('/') }}';
                @if(isset($product) && $product->media)
                var files = {!! json_encode($product->media) !!}
                    for (var i in files) {
                    var file = files[i]
                    var mockFile = { id: files[i].id, name: files[i].name, size: files[i].size};
                    this.emit("addedfile", mockFile);
                    this.emit("thumbnail", mockFile, public_path+'/'+files[i].path);
                    this.emit("complete", mockFile);

                    /*
                    this.options.addedfile.call(this, file.path)
                    file.previewElement.classList.add('dz-complete')*/
                }
                @endif
            }
        };
</script>