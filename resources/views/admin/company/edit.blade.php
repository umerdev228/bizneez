@extends('admin.layouts.app')
@section('content')
    @include('admin.layouts.topbar')
    <!-- Google Chart JS -->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <!-- Start Page content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if(session()->has('message'))
                        <div class="alert alert-success" style="width:803px ">
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-9 mt-5">
                            <div class="card-box">
                                <form method="POST" action="{{ route('admin.companies.update', ['id' => $company->id]) }}" enctype="multipart/form-data">
                                    @csrf
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="name">Name<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control valid" name="name" value="{{$company->name}}" id="name" required />
                                                @if ($errors->has('name'))
                                                    <span class="error">{{ $errors->first('name') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="phone">Phone<span class="text-danger">*</span></label>
                                                <input type="tel" class="form-control valid" name="phone" value="{{$company->phone}}" id="phone" required />
                                                @if ($errors->has('phone'))
                                                    <span class="error">{{ $errors->first('phone') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="url">URL<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control valid" name="url" value="{{$company->url}}" id="url" required />
                                                @if ($errors->has('url'))
                                                    <span class="error">{{ $errors->first('url') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="company_registration_no">Company Registration<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control valid" name="company_registration_no" value="{{$company->company_registration_no}}" id="company_registration_no" required />
                                                @if ($errors->has('company_registration_no'))
                                                    <span class="error">{{ $errors->first('company_registration_no') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="image">Company Logo<span class="text-danger">*</span></label>
                                                <input type="file" class="form-control valid" name="image" id="image"/>
                                                @if ($errors->has('image'))
                                                    <span class="error">{{ $errors->first('image') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <div class="p-4">
                                                    <img src="{{$company->logo}}" alt="{{$company->name}}" width="100px">
                                                    @if ($errors->has('image'))
                                                        <span class="error">{{ $errors->first('image') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="address">Address<span class="text-danger">*</span></label>
                                                <textarea class="form-control valid" name="address" id="address" required >{{$company->address}}</textarea>
                                                @if ($errors->has('address'))
                                                    <span class="error">{{ $errors->first('address') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="term_and_condition">Term And Conditions<span class="text-danger">*</span></label>
                                                <textarea class="form-control valid summernote" name="term_and_condition" id="term_and_condition" required >{!! $company->term_and_condition !!}</textarea>
                                                @if ($errors->has('term_and_condition'))
                                                    <span class="error">{{ $errors->first('term_and_condition') }}</span>
                                                @endif
                                            </div>
                                        </div>

{{--                                        <div class="col-sm-12">--}}
{{--                                            <div class="form-group">--}}
{{--                                                <label for="description">Description<span class="text-danger">*</span></label>--}}
{{--                                                <textarea class="form-control valid summernote" name="description" id="description" required >{{$company->description}}</textarea>--}}
{{--                                                @if ($errors->has('description'))--}}
{{--                                                    <span class="error">{{ $errors->first('description') }}</span>--}}
{{--                                                @endif--}}
{{--                                            </div>--}}
{{--                                        </div>--}}

                                    </div>
                                    <div class="form-group mt-3">
                                        <input class="btn btn-primary" type="submit" value="Update">
                                        <button onclick="history.back()" type="button" class="btn">Cancel</button>
                                    </div>
                                </form>
                            </div> <!-- end card-box -->
                        </div>

                    </div> <!-- container -->
                </div> <!-- content -->
            </div>
        </div>
    </div>

@endsection
