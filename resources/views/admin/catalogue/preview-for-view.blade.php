<style>
    .page-break {
        page-break-after: always;
    }
</style>
@for($i = 1; $i <= $data['page']; $i++)


    <table width="100%" border="0" style="border:1px solid #eee; background-color: #000; color: #fff">
        <tr>
            {{-- @if(isset($data['title_check'.$i])) --}}

            {{--  @if($data['title_check'.$i] == 'on') --}}
            <td width="200"><!-- <input type="button" value="Back" class="btn btn-info" onclick="history.back();"> --></td>
            <td align="center">{{$data['title'.$i]}}</td>
            {{--  @endif --}}
            {{-- @endif --}}


            @php
                $company = \App\Company::where('id', $data['company'.$i])->first();

                $image1 = '';
                $image2 = '';
                $image3 = '';
                $image4 = '';
                $image5 = '';
                $image6 = '';
                $image7 = '';
                $image8 = '';
                $image9 = '';

                if ($data['imageValue-'.$i.'-1']) {
                    $image1 = \App\Media::where('id', $data['imageValue-'.$i.'-1'])->first()->path;
                    }
                if ($data['imageValue-'.$i.'-2']) {
                    $image2 = \App\Media::where('id', $data['imageValue-'.$i.'-2'])->first()->path;
                    }
                if ($data['imageValue-'.$i.'-3']) {
                    $image3 = \App\Media::where('id', $data['imageValue-'.$i.'-3'])->first()->path;
                    }
                if ($data['imageValue-'.$i.'-4']) {
                    $image4 = \App\Media::where('id', $data['imageValue-'.$i.'-4'])->first()->path;
                    }
              
            @endphp
            <td align="center"><h2> </h2></td>
            <td align="center">
                <img src="https://bizneez.online/uploads/logo/b.png" style="height: 35px; margin-top:-10px">
            </td>
        </tr>
        <tr>
            <td colspan="3" align="center">
                <table width="90%">
                    <tr>
                        <td>
                            @if ($data['imageValue-'.$i.'-1'])
                                <img style="width:350px; height:200px;"  src="{{asset($image1)}}">
                                <p style="height: 50px; width:350px;  color: white;">{{ $data['description-'.$i.'-1'] }}</p>
                            @endif
                        </td>

                        <td>
                            @if ($data['imageValue-'.$i.'-2'])
                                <img style="width:350px; height:200px;"  src="{{asset($image2)}}">
                                <p style="height: 50px; width:350px; color: white;">{{ $data['description-'.$i.'-2'] }}</p>
                            @endif
                        </td>

                        <td>
                            @if ($data['imageValue-'.$i.'-3'])
                                <img style="width:350px; height:200px;"    src="{{asset($image3)}}">
                                <p style="height: 50px; width:350px; color: white;">{{ $data['description-'.$i.'-3'] }}</p>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td>
                            @if ($data['imageValue-'.$i.'-4'])
                                <img style="width:350px; height:200px;"   src="{{asset($image4)}}">
                                <p style="height: 50px; width:350px; color: white;">{{ $data['description-'.$i.'-4'] }}</p>
                            @endif
                        </td>

                         
                    </tr></table>
                
            </td>
        </tr>
     
    </table>
    @if($i <= $data['page'])
        <div class="page-break"></div>
    @endif


 
@endfor