<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeeSalariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_salaries', function (Blueprint $table) {
            $table->id();
            $table->integer('employee_id');
            $table->string('invoice_number')->nullable();
            $table->string('account_invoice')->nullable();
            $table->string('description')->nullable();
            $table->string('price')->nullable();
            $table->string('phone')->nullable();
            $table->string('discount')->nullable();
            $table->string('profit')->nullable();
            $table->string('vat')->nullable();
            $table->string('total')->nullable();
            $table->string('commission')->nullable();
            $table->string('quarter_bonuses')->nullable();
            $table->string('your_bonus')->nullable();
            $table->string('total_salary')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_salaries');
    }
}
