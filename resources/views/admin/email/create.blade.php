@extends('includes.meeting_section.master')

@section('content')

  <script src="https://cdn.tiny.cloud/1/j09y3dnf5jy8bx6cnr24ferb8jc1zin3krnczqnvi0h1eopk/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>
    
tinymce.init({
  selector: '#editor'
});


tinymce.init({
  selector: '#signature'
});

</script>


    <div class="col-lg-12"  style="margin-top: 100px;">
        <div class="card-box">
            <!-- Left sidebar -->
        @include('admin.email.left-side')
        <!-- End Left sidebar -->

            <div class="inbox-rightbar">

 

                <div class="mt-4">

                    <form method="POST" action="{{ route('admin.email.store') }}" enctype="multipart/form-data" novalidate>
                        @csrf
                        <div class="form-group">
                            <input required id="emailTo" name="to[]" type="email" class="form-control" placeholder="To">
                        </div>

                        <div class="form-group">
                            <input name="subject" type="text" class="form-control" placeholder="Subject">
                        </div>

                        <div class="form-group">
                            <textarea name="body" id="editor" cols="30" rows="10"></textarea>
                        </div>
                        <div class="form-group">
                            <textarea name="signature" id="signature" placeholder="signature" style="width:100%"></textarea>
                        </div>


                        <div class="form-group m-b-0">
                            <div class="text-right">
                                {{--                            <button type="button" class="btn btn-success waves-effect waves-light m-r-5"><i class="fa fa-floppy-o"></i></button>--}}
                                {{--                            <button type="button" class="btn btn-success waves-effect waves-light m-r-5"><i class="fa fa-trash-o"></i></button>--}}
                                <button class="btn btn-purple waves-effect waves-light">
                                    <span>Send</span>
                                    <i class="fa fa-send m-l-10"></i>
                                </button>
                            </div>
                        </div>

                    </form>
                </div> <!-- end card-->

            </div>

            <div class="clearfix"></div>
        </div>

    </div>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-beta/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
    <link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/examples/assets/app.css">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.0.4/popper.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.11.1/typeahead.bundle.min.js"></script>
    <script src="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>

    
    <script>
        var data = '{!! json_encode($contacts) !!}'
        //get data pass to json
        var task = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace("email"),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            local: JSON.parse(data) //your can use json type
        });
        task.initialize();

        console.log(data)

        var elt = $("#emailTo");
        elt.tagsinput({
            itemValue: "id",
            itemText: "email",
            typeaheadjs: {
                name: "email",
                displayKey: "email",
                source: task.ttAdapter()
            }
        });

    </script>
@endsection