@extends('admin.layouts.app')
@section('content')
@include('admin.layouts.topbar')
 <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="col-12 mt-5">
                        <div class="card-box">
                            <div class="text-center mt-2">
                                <div class="row">
                                    <div class="col-md-6 col-xl-3">
                                        <div class="card-box bg-custom widget-flat border-danger text-white"> <i class="mdi mdi-account-multiple"></i>
                                            <h3 class="m-b-10 open1">{{$total_user}}</h3>
                                            <h4 class="text-uppercase m-b-5">Total Customers</h4>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-xl-3">
                                        <div class="card-box widget-flat border-custom bg-danger text-white"> <i class="mdi mdi-cash-100"></i>
                                            <h3 class="m-b-10 pendding1">${{$total_payment}} </h3>
                                            <h4 class="text-uppercase m-b-5">Receivable</h4>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-xl-3">
                                        <div class="card-box widget-flat bg-info text-white"> <i class="mdi mdi-account-check"></i>
                                            <h3 class="m-b-10 unresolved1"> {{$premium_user}}</h3>
                                            <h4 class="text-uppercase m-b-5">Premium</h4>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-xl-3">
                                        <div class="card-box widget-flat border-success bg-pink text-white"> <i class="mdi mdi-account-off"></i>
                                            <h3 class="m-b-10 resolved1"> {{$trail_user}} </h3>
                                            <h4 class="text-uppercase m-b-5">Trial</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div><!-- end col -->
                    </div>
                    
        @if(session()->has('message'))
        <div class="alert alert-success"  id="myElem">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
          {{ session()->get('message') }}
        </div>
        @endif
        @if(session()->has('error'))
    <div class="alert alert-danger" > 
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
        {{ session()->get('error') }}
    </div>
@endif 
         
                            

 
                    <!-- 
                    <div class="row">
                        <div class="col-12">
                            <div class="card-box">
                              
                              <table class="table table-hover m-0 tickets-list table-actions-bar dt-responsive nowrap"  cellspacing="0" width="100%" id="dashboardlisting">
                              <thead>
                                <tr>
                                  <th>Name</th>
                                  <th>Email</th>
                                  <th>Phone</th>
                                  <th>Company</th>
                                  <th>Status</th>
                                  <th>Account Type</th>
                                  <th>Created Date</th>
                                  <th>Expiry Date</th> 
                                  <th class="hidden-sm">Action</th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  
                                  @foreach($merchant as $user)
                                  <tr> 
                                  <td>
                                    <a href="javascript: void(0);"> 

                                    @if($user->profile_pic=='') 
                                              <img src="{{url('/')}}/theme/admin/assets/images/users/avatar-1.jpg" alt="contact-img" title="contact-img" class="rounded-circle" />
                                    @else 
                                              <img src="{{url('/')}}/assets/images/users/{{$user->profile_pic}}" alt="contact-img" title="contact-img" class="rounded-circle" />
                                    @endif  

                   
                                   <span class="ml-2">{{$user->name}}</span> </a>
                                  </td>
                                  <td> {{$user->email}}</td>
                                  <td>{{$user->phone}}</td>
                                  <td> {{$user->company}}</td>
                                  <td><span class="badge badge-success">Active</span></td>
                                  <td> @if($user->inttype==1) Premium @else Trail @endif</td>
                                  <td> {{$user->created_at->format("D, j M Y g:i a")}} </td>
                                  <td> {{$user->expire_date}} </td>
                <td>
                                    <div class="btn-group dropdown">
                                    <a href="javascript: void(0);" class="table-action-btn dropdown-toggle arrow-none btn btn-light btn-sm" data-toggle="dropdown" aria-expanded="false">
                                     <i class="mdi mdi-dots-horizontal"></i>
                                    </a>
                        <div class="dropdown-menu dropdown-menu-right"> 
                              <a class="dropdown-item" href="{{ URL::to('admin/user/edit/' . $user->user_id ) }}">
                                <i class="mdi mdi-pencil mr-2 text-muted font-18 vertical-middle"></i>
                              Edit</a>  
                        <a class="dropdown-item" href="{{ URL::to('admin/user/view/' . $user->user_id ) }}">
                        <i class="mdi mdi-delete mr-2 text-muted font-18 vertical-middle"></i>View</a>
                        </div>
                                    </div>
                </td>
                                </tr>
                                  @endforeach
                            </tbody>
                            </table>
                                
                                
                                    </div> end card -->
                                    <!-- </div>end col -->
                                <!-- </div>
                                
                            </div>
                        </div> -->
                        
                                <!-- </div> container -->
                                <!-- </div> content --> 

                                <div class="content">
  <!--   <div class="container-fluid"> -->
        <!-- <div class="row">
            <div class="col-12"> --> 

                     <div class="row">
                        <div class="col-12">
                            <div class="card-box">  
                              <table class="table table-hover m-0 tickets-list table-actions-bar dt-responsive nowrap" cellspacing="0" width="100%"   id="normal-table">
                              <thead>
                             
                                <tr>
                                  <th>Company</th>
                                  <th>Phone</th>
                                   <th>Status</th>
                                  <th>Account Type</th>
                                  <th>Created Date</th>
                                  <th>Expiry Date</th> 
                                  <th class="hidden-sm" data-orderable="false"  >Action</th>
                                </tr>
                              </thead>
                              <tbody>
                             <!--     @foreach($merchant as $user)
                                 <tr>
                                    <td>{{$user->name}}</td>
                                    <td>${{$user->amount}}</td>
                                    <td>{{$user->created_at}}</td>
                                    <td>{{$user->expire_date}}</td>
                                    <td>success</td>
                                 </tr>
                                @endforeach  --> 

                             


                                @foreach($merchant as $user)
                       
                                 <?php 

                                  $newDate = date("Y-m-d");
                                  $newDate1 = date("Y-m-d H:i:s" ,strtotime($user->expire_date) );
                                   
                                  // $date1 = $user->created_at->format("D, j M Y g:i a");
                                  // $date2 = new DateTime();
                                    //echo $newDate.'-------'.$newDate1.'<br>';
                                  if(  $newDate1 < $newDate   ) 
                                  echo "<tr class='red'>";
                                  else
                                  echo "<tr>";




                                  ?>
                                   <td> {{$user->company}}</td>   
                                  <td>{{$user->phone}}</td>
                                  
                              <td> @if(($user->inttype==1 || $user->inttype==0) && ($newDate1 < $newDate ))  
                                    Inactive @else Active @endif</td>
                                  <td> @if($user->inttype==1) Premium @else Trial @endif</td>
                                  <td> {{$user->created_at->format("D, j M Y g:i a")}} </td>
                                  <td> {{$user->expire_date}} </td> 
                                    
                                <td>
                                    <div class="btn-group dropdown">
                                    <a href="javascript: void(0);" class="table-action-btn dropdown-toggle arrow-none btn btn-light btn-sm" data-toggle="dropdown" aria-expanded="false">
                                     <i class="mdi mdi-dots-horizontal"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right"> 
                                    <a class="dropdown-item" href="{{ URL::to('admin/user/edit/' . $user->user_id ) }}">
                                    <i class="mdi mdi-pencil mr-2 text-muted font-18 vertical-middle"></i>
                                    Edit</a>  
                                    <a class="dropdown-item" href="{{ URL::to('admin/user/view/' . $user->user_id ) }}">
                                     <i class="fa fa-eye mr-2 text-muted font-18 vertical-middle"></i>View</a>
                                     <a class="dropdown-item" onclick="return confirm('Are you sure to delete User or not?')" href="{{ URL::to('admin/user/delete/' . $user->user_id ) }}">
                                    <i class="mdi mdi-delete mr-2 text-muted font-18 vertical-middle"></i>
                                    Delete</a>

                                    @if($user->profile_status>4)
                                     <a class="dropdown-item" onclick="return confirm('Are you sure to expire User or not?')" href="{{ URL::to('admin/user/expire/' . $user->id ) }}">
                                     
                                    <i class="fa fa-warning mr-2 text-muted font-18 vertical-middle"></i>
                                    Expire</a>

                                    @endif
                                    </div>
                                    </div>
                                </td>
                        </tr>
                                  @endforeach
                                  
                            </tbody>
                            </table>
                                
                                
                                    </div> <!-- end card -->
                                    </div><!-- end col -->
                                </div>
                                
                            </div>
                        </div>
                        
                                </div> <!-- container -->
                                </div> <!-- content -->
                                <br>
                                <br>
                                <br>


                                @include('admin.layouts.footer')
                                @endsection

                                   <style>
                                .red{color: red}
                                 /*.alert-success{width: 500px;}*/
                                </style>  
