@extends('admin.layouts.app')
@section('content')
    @include('admin.layouts.topbar')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    @if(session()->has('message'))
                        <div class="alert alert-success mt-5" id="myElem" >
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-12 mt-5">
                            <div class="card">
                                <div class="card-body">
                                    <a type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-lg"><i class="fa fa-plus"></i>Add New Subscriber </a>
                                </div>
                                <div class="card-body">
                                    <table class="table table-hover m-0 tickets-list table-actions-bar dt-responsive nowrap" id="normal-table">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Email</th>
                                            <th data-orderable="false">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($subscribers as $subscriber)
                                            <tr>
                                                <td>{{$subscriber->id}}</td>
                                                <td>{{$subscriber->email}}</td>
                                                <td>
                                                    <div class="btn-group">
{{--                                                        <a class="btn" href="{{ route('admin.subscriber.edit', ['id' => $subscriber->id] ) }}">--}}
{{--                                                            <i class="mdi mdi-pencil"></i>--}}
{{--                                                        </a>--}}
                                                        <a class="btn" href="{{ route('admin.subscriber.delete',  $subscriber->id ) }}">
                                                            <i class="fa fa-close"></i>
                                                        </a>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <form action="{{route('admin.subscriber.store')}}" method="post">
                                @csrf
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Create Email</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="form-group w-100">
                                                    <label for="email">Email</label>
                                                    <input class="form-group form-control w-100" type="email" name="email" id="email" required>
                                                </div>
                                                <div class="form-group w-100">
                                                    <input class="btn btn-success float-right" type="submit" value="Add Email" id="subject">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
@endsection
