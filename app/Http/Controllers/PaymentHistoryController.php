<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\PaymentHistory;
use App\Packages;

use App\User;
use Carbon\Carbon;

use DB;

class PaymentHistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
          $this->middleware('auth');
    }

    public function index()
    {

       $payment_history = PaymentHistory::get();

        //$payment_history = PaymentHistory::with('user')->get();

        $packages = Packages::all();
        // dd($packages);
        $data = [
            'breadcrumb' => "Dashboard / PaymentHistory",
            'title' => "Users Payment",
            'payment_history'=> $payment_history,
            'packages' => $packages,

              ];
       return view('admin.payment_history')->with($data);
    }

    public function search(Request $request)
    {
        // dd($request->package );
        // $PaymentHistory = PaymentHistory::whereBetween('created_at',[$request->created,$request->end_created])->get();

        // dd('cvghjk');

        //dd($request->all());
        $PaymentHistory = PaymentHistory::query();

        if(!empty($request->package_name))
        {
            // dd($request->package_name);
            $PaymentHistory->where('package_id', $request->package_name);
        }

        if(!empty($request->start_date) && !empty($request->end_date))
        {

            $newDate = date("Y-m-d H:i:s", strtotime($request->start_date));
            $newDate1 = date("Y-m-d H:i:s", strtotime($request->end_date));
              // dd($newDate1);
            $PaymentHistory->whereBetween('payment_histories.created_at', array($newDate,$newDate1));

        }
        // dd($PaymentHistory->get());
        $packages = Packages::all();


       // if( $request->package_name ||  $request->package==0)
       //  {
       //      $pak = DB::table('payment_histories')
       //                  ->leftJoin('packages', 'payment_histories.package_id', '=', 'packages.id')
       //                  ->where('packages.status',$request->package)
       //                  ->select('payment_histories.*','packages.status as p_status', 'packages.id as p_id' )
       //                  ->get();

       //       return view('admin.payment_history',
       //          ['payment_history' => $pak,
       //          'packages' => $packages]
       //          );
       //  }
        // else
        // {
        // dd($request->all());
        if(isset($request->package))
        {
            // dd('succfully called');
            if ($request->package == 0)
            {
                // dd('not active');

            //     $PaymentHistory->join('packages', function ($join) use ($request) {
            //         $join->on('payment_histories.package_id', '=', 'packages.id')
            //             ->where('packages.status', '=', $request->package);
            //    });

                // dd($PaymentHistory->get());
            }
            else
            {
                // dd('is active');
            //    $PaymentHistory->join('packages', function ($join) use ($request) {
            //         $join->on('payment_histories.package_id', '=', 'packages.id')
            //             ->where('packages.status', '=', $request->package);
            //    });
            }

        }

            return view('admin.payment_history',
            ['payment_history' => $PaymentHistory->get(),
             'packages' => $packages,'breadcrumb' => 'Dashboard',]
            );
        }



        /* if($request->package && $request->package==1){ //active
            //$packages = Packages::where('status', $request->package);
            //$PaymentHistory =PaymentHistory::with('packagesCondition');
            $PaymentHistory->where('status', $request->package);
         } */
//dd($PaymentHistory);










        // $PaymentHistory = PaymentHistory::where('created_at','>=',$request->start_date)
        //         ->where('created_at','<=',$request->end_date)
        //         ->get();
        //          dd( $PaymentHistory);










        // elseif($request->$package->all('package'))
        // {
        //     $PaymentHistory = PaymentHistory::where('id')->get();
        //      return view('admin.payment_history', compact('data'));
        // }
        // dd('Awais khalid');

        // else
        // {
        //     ($request->created AND $request->end_created And $request->$package)
        //     {

        //         $PaymentHistory = PaymentHistory::list('packages');
        //         return view('admin.payment_history', compact('data'));
        //     }

        //     return redirect('admin.payment_history')->with('success','Search succfully.');


        //   }




    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PaymentHistory  $paymentHistory
     * @return \Illuminate\Http\Response
     */
    public function show(PaymentHistory $paymentHistory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PaymentHistory  $paymentHistory
     * @return \Illuminate\Http\Response
     */
    public function edit(PaymentHistory $paymentHistory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PaymentHistory  $paymentHistory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PaymentHistory $paymentHistory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PaymentHistory  $paymentHistory
     * @return \Illuminate\Http\Response
     */
    public function destroy(PaymentHistory $paymentHistory)
    {
        //
    }















}
