<?php

namespace App\Http\Controllers;

use App\Lead;
use Illuminate\Http\Request;

class LeadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $leads = Lead::all();
        $breadcrumb = 'Admin / Lead ';
        return view('admin.lead.index', compact('leads', 'breadcrumb'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $breadcrumb = 'Admin / Lead / Create';
        return view('admin.lead.create', compact('breadcrumb'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $lead = Lead::create([
            "title" => $request->title,
            "type" => $request->type,
            "source" => $request->source,
            "phone" => $request->phone,
            "email" => $request->email,
            "details" => $request->details,
        ]);

        if ($request->has('image')) {
            $imageName = time() . '.' . $request->image->extension();
            $request->image->move(public_path('/uploads/users/leads/'), $imageName);
            $lead = Lead::where('id', $lead->id)->update([
                "image" => '/uploads/users/leads/' . $imageName,
            ]);
        }

        toastr()->success('Lead Created Successfully');
        return redirect()->route('admin.leads.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Lead  $lead
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $lead = Lead::where('id', $id)->first();
        return view('admin.lead.edit');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Lead  $lead
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $lead = Lead::where('id', $id)->first();
        $breadcrumb = 'Admin / Lead / Edit';
        return view('admin.lead.edit', compact('lead', 'breadcrumb'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Lead  $lead
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        if ($request->has('image')) {
            $imageName = time().'.'.$request->image->extension();
            $request->image->move(public_path('/uploads/users/leads/'), $imageName);
            $lead = Lead::where('id', $id)->update([
                "image" => '/uploads/users/leads/' . $imageName,
            ]);
        }
        $lead = Lead::where('id', $id)->update([
            "title" => $request->title,
            "type" => $request->type,
            "source" => $request->source,
            "phone" => $request->phone,
            "email" => $request->email,
            "details" => $request->details,
        ]);

        toastr()->success('Lead Updated Successfully');
        return redirect()->route('admin.leads.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Lead  $lead
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Lead::where('id', $id)->delete();
        toastr()->success('Lead Deleted Successfully');
        return redirect()->route('admin.leads.index');
    }
}
