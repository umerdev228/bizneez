<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Merchant;
use App\Notification;
use App\History_log;
use Auth;
use Mail;
use Stripe;

class RegisterController extends Controller
{
	
    public function create(Request $request)
    {
         // dd('fdfdfd');
 
        request()->validate([ 
            'email' => 'required|unique:users',
            'phone' => 'required',
            'company' => 'required', 
            'fname' => 'required',
            'lname' => 'required',

            ]);
            //array me data store ho reh hai
            $data_user = [
                 'email' => $request->post('email'),
                 'name' => $request->post('fname') .' '.$request->post('lname') ,
                 'role' =>1,
                 // 'profile_status'=>$request->post('profile_status')
            ];


            // print_r($data_user);
             // dd($data_user);
            $user = User::create($data_user);    //insertion into user table for Auth Gate
            $lastInsertedId= $user->id;


            $password = Hash::make('test');
            $description =  '';
            $token = md5(rand());
            // $profile_status=$request->file('image');
            // $profile_status=rand().'.'. $profile_status->getClientOriginalExtension();
            // $profile_status->move(public_path('upload'),$profile_status);
            // $profile_status->move(public_path('upload'),$profile_status);


       //       if($request->file('file')){
       //         $file = $request->file('profile_pic');
       //         $name = $merchant->id.'_'.time().$file->getClientOriginalName();
       //         $file->move('public/merchant/', $name);  
       //         $merchant->profile_pic = $name;
       //         $merchant->save();
       // }
            $Today=date('y:m:d'); 
            // add 3 days to date
            $expire_date=Date('y:m:d', strtotime('+15 days'));

            $data = [
               
                'email' => $request->post('email'), 
                'user_id' =>$lastInsertedId,  
                'name' => $request->post('fname') .' '.$request->post('lname') ,
                'phone' => $request->post('phone'),  
                'company' => $request->post('company'), 
                'token' => $token,
                'expire_date' =>$expire_date,
                'inttype' => 0,  
                'profile_status' => 0
            ];

            Merchant::create($data); 
            //print_r($data); 
            //die();
    

            //create stripe account for new signup user
            /*Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
            $create_customer = Stripe\Customer::create([
                    'description' => 'TMS signup account',
                    'email' => $request->post('email'),
                    'name' => $request->post('fname') .' '.$request->post('lname'),
                    "source" => $request->stripeToken,
                ]);*/

            //===============end====================================

               //send email to signup user
                $name =   $request->post('fname') .' '.$request->post('lname');
                $title = "TMS New Signup";
                $url = env('APP_URL');
                Mail::to($request->post('email'))->send(new \App\Mail\MerchantRegistration($name, $title, $url, $token));  



                $complate_name = $request->post('fname') .' '.$request->post('lname');
                $description = $complate_name .' ' . "New Signup Trail User ".date("d. M Y"); ; 
                $data_notification = ['description' => $description, 'user_id' => $lastInsertedId,'inttype' => 1 , 'intstatus' => 0 ]; 
                 // dd($data_notification);
                History_log::create($data_notification); 


//notification process
                // $complate_name = $request->post('fname') .' '.$request->post('lname');
                // $description = $complate_name .' ' . "New Signup Trail User ".date("d. M Y"); ; 
                // $data_notification = ['detail' => $description, 'user_id' => $lastInsertedId ]; 
                // Notification::create($data_notification); 
//end notification process               






        return redirect('signup')->with('message','User is created successfully.Please check your email and set password.');
                    return redirect()->back()->with('message', 'Customer Created!'); 

}

 
 

public function verify(Request $request){ 
			    //dd('fefefe');
       if(Merchant::where('token',$request->get('v'))->count()){
        //dd('inside if');
             $merchant = Merchant::where('token', $request->get('v') )->first();  
             $data= [
                    'merchant' => $merchant,
                    'breadcrum' => 'Generate Password'
                ];
             return view('verify')->with($data); 
       }
       else
       {
        //dd('inside else');

        return redirect('signin')->with('message', 'Link is expired or you have already created password, please login.');
               
       }
   

      
}



public function reset_password(Request $request){  
 
     $merchant = Merchant::where('user_id', $request->post('user_id') )->first(); 

     $user = User::findOrFail($request->post('user_id')); 
     $user->password = Hash::make($request->post('password'));  
     $user->save();

     $merchant->token = '';
     $merchant->save();
     
     return redirect('signin')->with('message', 'Your password has been set please login.');
     // return redirect('signin')->with('message', 'You are setup password please login');
      
}
}


// // public function view($id){
// //      $user =   Merchant::where('user_id', $id )->first();  
// //      $data= [
// //                     'user' => $user,
// //                     'breadcrum' => 'Profile view'
// //                 ];
// //      return view('admin.user_view')->with($data); 
// // }



// // public function edit($id){
// //      $user =   Merchant::where('user_id', $id )->first(); 
// //      $name = explode(' ',$user->name);
// //      $data = [
// //             'user'=> $user, 
// //             'title'=> 'User Update', 
// //             'breadcrum'=>'Profile Update',
// //             'fname' => $name[0],
// //             'lname' => $name[1],
// //         ];
// //      return view('admin.user_edit')->with($data);  
// // }


// // public function update(Request $request )
// //     {   

// //         request()->validate([ 
// //             'phone' => 'required',
// //             'company' => 'required', 
// //             'fname' => 'required',
// //             'lname' => 'required',
             
// //             ]);
// //  //dd(Auth::user()->id);

// //         $merchant = Merchant::findOrFail($request->id); 
// //         $user = User::findOrFail($merchant->user_id);
 
// //         $merchant->name = $request->post('fname').' '.$request->post('lname');
// //         $merchant->phone = $request->post('phone'); 
// //         $merchant->description = $request->post('description');
// //         $merchant->save(); 
        
// //         return redirect()->back()->with('message', 'User Updated!'); 

        
// //     }





//  }
