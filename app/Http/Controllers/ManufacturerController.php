<?php

namespace App\Http\Controllers;

use App\Profile;
use App\Represent;
use App\Media;
use App\Category;
use App\ManufactureCategories;
use App\ManufactureRanges;
use App\ManufactureProducts;

use App\SiteSetting;
use Illuminate\Http\Request;
use App\Product;
use App\User;
use Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use PhpParser\Node\Expr\Array_;
use Session;
use App\ProductMedia;
use App\ManufactureImportProducts;
use Mail;
use \DB;
use App\Inventory;

class ManufacturerController extends Controller
{
     
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }



    public function index() {
 

        $manufacturers = User::role('manufacturer')->with('user')->with('profile')->orderBy('created_at', 'desc')->get();
        $data= [
            'manufacturers' => $manufacturers,
            'breadcrumb' => 'Dashboard / Manufacturer List',
            'url' => env('APP_ENV_LINK'),
            
        ];
        return view('admin.manufacturer.index')->with($data);
    }



    public function price_list(){
        $sessions = \request()->session()->get('categories_products');

        $media = Media::where('name', 'gallery')->get();
        $breadcrumb = ucfirst(\Illuminate\Support\Facades\Auth::user()->roles[0]->name)." / Manufacturer / New";
        $categories = Category::all();
        $products  = DB::table('mdd_price_list_csv')->get();
        return view('admin.manufacturer.price_list', compact('breadcrumb' , 'media' , 'products' , 'categories', 'sessions'));
    }
    public function image_gallry(){
        $sessions = \request()->session()->get('categories_products');

        $manufacturers = User::role('manufacturer')->get();
        $media = Media::where('name', 'gallery')->get();
        $breadcrumb = ucfirst(\Illuminate\Support\Facades\Auth::user()->roles[0]->name)." / Manufacturer / New";
        $categories = Category::all();
        $products  = DB::table('mdd_price_list_csv')->get();
        return view('admin.manufacturer.image_gallry', compact('breadcrumb' , 'media' , 'products' , 'categories', 'sessions', 'manufacturers'));
    }
    public function new_price_list(){
 
              $data = \request()->session()->get('new_price_list');
            //            \request()->session()->put('data', $data);
//        element_count
//        row_count
            //return view('admin.manufacturer.inventory.preview', compact('data'));


            return view('admin.manufacturer.new_price_list', compact('data'));



        $sessions = \request()->session()->get('categories_products');

        $media = Media::where('name', 'gallery')->get();
        $breadcrumb = ucfirst(\Illuminate\Support\Facades\Auth::user()->roles[0]->name)." / Manufacturer / New";
        $categories = Category::all();

        $products  = DB::table('mdd_price_list_csv')->get();
        return view('admin.manufacturer.new_price_list', compact('breadcrumb' , 'media' , 'products' , 'categories', 'sessions'));
        
    }

    public function inventory(){
        $sessions = \request()->session()->get('categories_products');

        $media = Media::where('name', 'gallery')->get();
        $breadcrumb = ucfirst(\Illuminate\Support\Facades\Auth::user()->roles[0]->name)." / Manufacturer / New";
        $categories = Category::all();

        $products  = DB::table('mdd_price_list_csv')->get();
        return view('admin.manufacturer.inventory.index', compact('breadcrumb' , 'media' , 'products' , 'categories', 'sessions'));
    }




    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {  
        //
        $sessions = \request()->session()->get('categories_products');

        $media = Media::where('name', 'gallery')->get();
        $breadcrumb = ucfirst(\Illuminate\Support\Facades\Auth::user()->roles[0]->name)." / Manufacturer / New";
        $categories = Category::all();

        $products  = DB::table('mdd_price_list_csv')->get();
        //$products =ManufactureImportProducts::with('media')->with('categories')->with('manufacturer')->orderBy('created_at', 'Asc')->get();

        $settings = SiteSetting::all();


        $COL2 = SiteSetting::where('data', 'COL2')->first()->key ?? 0;
        $COL7 = SiteSetting::where('data', 'COL7')->first()->key ?? 1;
        $COL4 = SiteSetting::where('data', 'COL4')->first()->key ?? 2;
        $COL5 = SiteSetting::where('data', 'COL5')->first()->key ?? 3;
        $COL6 = SiteSetting::where('data', 'COL6')->first()->key ?? 4;
        $COL8 = SiteSetting::where('data', 'COL8')->first()->key ?? 5;
        $COL10 = SiteSetting::where('data', 'COL10')->first()->key ?? 6;
        $COL11 = SiteSetting::where('data', 'COL11')->first()->key ?? 7;
        $COL12 = SiteSetting::where('data', 'COL12')->first()->key ?? 8;

        return view('admin.manufacturer.create', compact('breadcrumb' ,
            'media',
            'products',
            'categories',
            'COL2',
            'COL7',
            'COL4',
            'COL5',
            'COL6',
            'COL8',
            'COL10',
            'COL11',
            'COL12',
            'sessions'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {  
        //
//        $request['password'] = $password = Str::random(8);

        $validated = $request->validate([
            'first_name' => 'required',
            'email' => 'required',
        ]);

        $user = User::where('email', $request['email'])->first();
        if ($user == null) {
            User::create([
                "name" => $request['first_name'] . ' ' . $request['middle_name'] . ' ' . $request['last_name'],
                "first_name" =>' ',// $request['first_name'],
                "middle_name" =>' ', //$request['middle_name'],
                "last_name" =>' ',// $request['last_name'], 
                "email" => $request['email'],
                "password" => Hash::make($request['password']),
                "created_by" => Auth::id(),
            ]);
            $user = User::where('email', $request['email'])->first();
            $user->assignRole('manufacturer');
            Profile::create([
                "user_id" => $user->id,
                "phone" => $request['phone'],
                "company_keywords" => $request['company_keywords'],
                "mobile" => $request['mobile'],
                "assistant_number" => $request['assistant_number'],
                "address" => $request['address'],
                "location_id" => $request['location_id'],
                "web_url" => $request['web_url'],
                "area_id" => $request['area_id'],
                "description" => $request['description'],
            ]);
            if ($request->has('image')) {
                $imageName = time().'.'.$request->image->extension();
                $request->image->move(public_path('/uploads/users/manufacturers'), $imageName);
                Profile::where('user_id', $user->id)->update([
                    "image" => '/uploads/users/manufacturers/' . $imageName,
                ]);
            }
            foreach ($request->represent_name as $index => $item) {

                Represent::create([
                    'user_id' => $user->id,
                    'name' => $request->represent_name[$index],
                    'email' => $request->represent_email[$index],
                    'mobile' => $request->represent_mobile[$index],
                ]);
            }
//            Mail::to($user->email)->send(new \App\Mail\MerchantActivation($user->name, '', '', '', $request['password']));
        }
        toastr()->success('Manufacturer Created Successfully');
        return redirect()->route('admin.manufacturers.index');
    }

    public function manufacture_cat_product_add(Request $request){
            $data = $request->all();
  
             \request()->session()->put('new_price_list', $data);
            //            \request()->session()->put('data', $data);
//        element_count
//        row_count
            //return view('admin.manufacturer.inventory.preview', compact('data'));
 
 $json_data = json_encode($data);
 
 $data_in_decode = json_decode($json_data);
/*
 print_r( json_encode($data_in_decode->cat)  );
        dd(); */


$date =  date('Y-m-d'); 
Inventory::create([
                "user_id" => Auth::id(),
                "data_in_json" => json_encode($data_in_decode->cat)  ,
                "data_value_json" => json_encode($data_in_decode->p_name)  ,
                "created_date" =>$date, //$request['middle_name'],
               
            ]);
            return view('admin.manufacturer.new_price_list', compact('data'));

        $data = [];
        foreach ($request->cat as $index => $category) {
            $product = $request->p_name[$index];

            $val = [
                'category' => $category,
                'product' => $product,
            ];
            array_push($data, $val);
        }
        \request()->session()->put('categories_products', $data);
        return redirect()->back();

        if($request->category_top) {
            $i=0;
            foreach($request->category_top as $categories) {
                $category_id = ManufactureCategories::create([
                    "category_name" => $categories,
                    "manufacture_id" => Auth::id(),
                ]);

//=============saving rangess======================//
                $range1= 'range_'.$i;
                $range2= 'material_'.$i;
                $range3= 'leg_option_'.$i;
                $range4= 'fabric_option_'.$i;
                ManufactureRanges::create([
                    "manfacture_categories_id" =>  $category_id->id,
                    "range1" => $request->input( $range1 ),
                    "range2" => $request->input($range2),
                    "range3" =>$request->input($range3),
                    "range4" => $request->input($range4),
                ]);

//========================================================//



//=============saving products======================//
                $p_name= 'p_name_'.$i;
                $p_size= 'p_size_'.$i;
                $p_code= 'p_code_'.$i;
                $p_retail1= 'p_retial_price_'.$i;
                $p_retail2= 'p_retial_price2_'.$i;

                /*
        ManufactureProducts::create([                
                "manfacture_categories_id" =>  $category_id->id,
                "product_name" => $request->input( $p_name ),
                 "size" => $request->input($p_size),
                 "color_code" =>$request->input($p_code),
                 "retail_price1" => $request->input($p_retail1),
                 "retail_price2" => $request->input($p_retail2),
            ]);*/


                Product::create([
                    "user_id" =>  Auth::id(),
                    "name" => $request->input( $p_name ),
                    "range" => $request->input($p_size),
                    "color_type" =>$request->input($p_code),
                    "price" => $request->input($p_retail1),
                    "second_price" => $request->input($p_retail2),
                ]);

//========================================================//
                $i++;
            }//foreach

            /* if($request->product_sub_counter_0) {
        $i=0;
        foreach($request->product_sub_counter_0 as $sub_products){  
        }
    }
*/





        }//end if
 
        toastr()->success('Manufacturer Products are uploaded Successfully');
        return redirect()->route('admin.manufacturers.create');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {
        //
        $breadcrumb = ucfirst(\Illuminate\Support\Facades\Auth::user()->roles[0]->name)." / Manufacturer / Show";
        $id = (\request()->route()->parameter('id'));
        $user = User::with('roles')->where('id', $id)->with('profile')->first();
        return view('admin.manufacturer.show', compact('user', 'breadcrumb'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $breadcrumb = ucfirst(\Illuminate\Support\Facades\Auth::user()->roles[0]->name)." / Manufacturer / Edit";
        $manufacturer = User::role('manufacturer')->where('id', $id)->with('representatives')->with('profile')->first();
        $users = User::all();
        return view('admin.manufacturer.edit', compact('manufacturer', 'breadcrumb', 'users'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        //
        User::where('id', $id)->update([
            "name" => $request['first_name'] . ' ' . $request['middle_name'] . ' ' . $request['last_name'],
            "first_name" => $request['first_name'],
            "middle_name" => $request['middle_name'],
            "last_name" => $request['last_name'],
            "email" => $request['email'],
            "created_by" => $request['created_by'],
        ]);
        Profile::where('user_id', $id)->update([
            "phone" => $request['phone'],
            "mobile" => $request['mobile'],
            "assistant_number" => $request['assistant_number'],
            "company_keywords" => $request['company_keywords'],
            "address" => $request['address'],
            "location_id" => $request['location_id'],
            "web_url" => $request['web_url'],
            "area_id" => $request['area_id'],
            "description" => $request['description'],

        ]);
        if ($request->has('image')) {
            $imageName = time().'.'.$request->image->extension();
            $request->image->move(public_path('/uploads/users/manufacturer'), $imageName);
            Profile::where('user_id', $id)->update([
                "image" => '/uploads/users/manufacturer/' . $imageName,
            ]);
        }
        Represent::where('user_id', $id)->delete();
        foreach ($request->represent_name as $index => $item) {

            Represent::create([
                'user_id' => $id,
                'name' => $request->represent_name[$index],
                'email' => $request->represent_email[$index],
                'mobile' => $request->represent_mobile[$index],
            ]);
        }
        if ($request->has('password')) {
            User::where('id', $id)->update([
                "password" => $request['password'],
            ]);
        }
        toastr()->success('Manufacturer Updated Successfully');
        return redirect()->route('admin.manufacturers.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        User::where('id', $id)->delete();
        Profile::where('user_id', $id)->delete();
        toastr()->success('Manufacturer Deleted Successfully');
        return redirect()->route('admin.manufacturers.index');
    }
}
