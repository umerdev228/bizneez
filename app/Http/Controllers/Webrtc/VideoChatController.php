<?php

namespace App\Http\Controllers\Webrtc;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Pusher\Pusher;

class VideoChatController extends Controller
{
    //
    public function index(Request $request) {
        $user = $request->user();
        $other = \App\User::where('id', '!=', $user->id)->where('name', 'demo')->pluck('name', 'id');
        $others = \App\User::where('id', '!=', $user->id)->with('roles')->with('profile')->get();
        $role = auth()->user()->roles[0]->name;
        return view('webrtc.index')->with([
            'user' => collect($request->user()->only(['id', 'name'])),
            'other' => $other,
            'others' => $others,
            'role' => $role,
        ]);
    }
    public function clientToClient(Request $request) {
        $user = $request->user();
        $other = \App\User::where('id', '!=', $user->id)->where('name', 'demo')->pluck('name', 'id');
        $others = \App\User::where('id', '!=', $user->id)->with('roles')->with('profile')->get();
        $role = auth()->user()->roles[0]->name;
        return view('webrtc.client-to-client.index')->with([
            'user' => collect($request->user()->only(['id', 'name'])),
            'other' => $other,
            'others' => $others,
            'role' => $role,
        ]);
    }
    public function employeeToUser(Request $request) {
        $user = $request->user();
        $other = \App\User::where('id', '!=', $user->id)->where('name', 'demo')->pluck('name', 'id');
        $others = \App\User::where('id', '!=', $user->id)->with('roles')->with('profile')->get();
        $role = auth()->user()->roles[0]->name;
        return view('webrtc.employee-to-demo.index')->with([
            'user' => collect($request->user()->only(['id', 'name'])),
            'other' => $other,
            'others' => $others,
            'role' => $role,
        ]);
    }

    public function auth(Request $request) {
        $user = $request->user();
        $socket_id = $request->socket_id;
        $channel_name = $request->channel_name;
        $pusher = new Pusher(
            config('broadcasting.connections.pusher.key'),
            config('broadcasting.connections.pusher.secret'),
            config('broadcasting.connections.pusher.app_id'),
            [
                'cluster' => config('broadcasting.connections.pusher.options.cluster'),
                'encrypted' => true
            ]
        );
        return response(
            $pusher->presence_auth($channel_name, $socket_id, $user->id)
        );
    }

}
