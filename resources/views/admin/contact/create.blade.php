@extends('includes.meeting_section.master')
@section('content')
    <!-- Start Page content -->
<br>
            <div class="row">
                <div class="col-12">
                    <div class="row">
                        <div class="col-9 mt-5">
                            <div class="card-box">
                                <form method="POST" action="{{ route('admin.contact.store') }}" enctype="multipart/form-data">
                                    @csrf
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="name">Name<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control valid" name="name" id="name" required />
                                                @if ($errors->has('name'))
                                                    <span class="error">{{ $errors->first('name') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="email">Email<span class="text-danger">*</span></label>
                                                <input type="email" class="form-control valid" name="email" id="email" required />
                                                @if ($errors->has('email'))
                                                    <span class="error">{{ $errors->first('email') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="phone">Phone<span class="text-danger">*</span></label>
                                                <input type="number" class="form-control valid" name="phone" id="phone" required />
                                                @if ($errors->has('phone'))
                                                    <span class="error">{{ $errors->first('phone') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                               <!--  <textarea name="description" id="editor" cols="30" rows="10"></textarea> -->
                                                <label for="email">Company<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control valid" name="company" id="company" required />
                                                @if ($errors->has('company'))
                                                    <span class="error">{{ $errors->first('company') }}</span>
                                                @endif
                                            </div> 
                                        </div>

                                         <div class="col-sm-12">
                                            <div class="form-group"> 
                                                <label for="email">Upload Profile Picture<span class="text-danger">*</span></label>
                                                <input required type="file" name="file" />
                                            </div> 
                                        </div>

                                        <!--  <div class="col-sm-12">
                                            <div class="form-group"> 
                                                <label for="email">Email Receive ID</label>
                                                <select name="" class="form-control">
                                                    <option value="info@bizneez.net">info@bizneez.net</option>
                                                    <option value="info@bizneez.com">info@bizneez.com</option>
                                                    <option value="info@bizneez.co.uk">info@bizneez.co.uk</option>
                                                    <option value="">All</option>
                                                </select>
                                            </div> 
                                        </div> -->
                                    </div>
                                    <div class="form-group mt-3">

                                        <input class="btn btn-primary" type="submit" value="Create Contact">
                                        <button onclick="history.back()" type="button" class="btn">Cancel</button>
                                    </div>
                                </form>
                            </div> <!-- end card-box -->
                        </div>

                    </div> <!-- container -->
                </div> <!-- content -->
            </div>

@endsection
