@extends('admin.layouts.app')
@section('content')
@include('admin.layouts.topbar')
<!-- Google Chart JS -->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<!-- Start Page content -->
<div class="content">
 


     
      <div class="container-fluid" >
        <div class="row">
          <div class="col-12">
            <div class="row">
              <div class="col-12">
                <div class="card-box mt-5">
                  <table class="table table-hover m-0 tickets-list table-actions-bar dt-responsive nowrap" cellspacing="0" width="100%"   id="datatable">
                    <thead>
                      <tr>
                        <th>Date/Time</th>
                        <th>Notification</th>
                         
                        
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($notification_data as $notification)
                      <tr onclick="{{URL::to('admin/user/view/' . $notification->user_id ) }}" >
                        <td> {{$notification->created_at}}</td>
                        <td> {{$notification->description}}</td>
                      </tr>
                    
                      @endforeach
                      
                    </tbody>
                    
                  </table>
                  
                  
                  </div> <!-- end card -->
                  </div><!-- end col -->
                </div>
                
              </div>
            </div>
            
            </div> <!-- container -->
</div> <!-- content -->
            
            @include('admin.layouts.footer')
            @endsection
         
            <style>
              .arrow-none{margin-top:20px!important;}
            button{

            border-style: none;}
            .card-box{
              padding-right:  20px;
            }
            </style>