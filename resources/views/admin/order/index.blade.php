@extends('admin.layouts.app')
@section('content')
    @include('admin.layouts.topbar')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    @if(session()->has('message'))
                        <div class="alert alert-success mt-5" id="myElem" >
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-12 mt-5"> <a href="{{ route('admin.orders.create') }}" class="btn btn-success">Create Quotation</a>
                            <div class="card"><br><br><br>
                                <form action="{{ route('quotation.search') }}" method="post">
                                    @csrf
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div style="padding:5px;">
                                                <input style="margin-left:15px;" value="Select Month" name="startDate" id="startDate" class="date-picker custom-select2 float-md-left" />
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <select name="company" class="custom-select2 float-md-left">
                                                <option value="0">--Select Company--</option>
                                                @foreach($companies as $comp)
                                                    <option value="{{$comp->id}}">{{$comp->name}}</option>
                                                @endforeach
                                            </select>
                                            <input type="submit" value="Search" name="btn" class="btn">
                                        </div>

                                        <div class="col-md-4">
                                            <select name="country" class="custom-select2 float-md-left">
                                                <option value="0">--Select Country--</option>
                                                @foreach($locations as $location)
                                                    <option value="{{$location->id}}">{{ $location->name }}</option>
                                                @endforeach
                                            </select>
                                            <input type="submit" value="Search" name="btn" class="btn">
                                        </div>
                                    </div>

                                </form>

                                <div class="card-title">
                                    {{--                                    <h3>Custom Orders</h3>--}}
                                </div>
                                <div class="card-body">
                                
                                    <table class="table table-hover m-0 tickets-list table-actions-bar dt-responsive nowrap" id="normal-table">
                                        <thead>
                                        <tr>
                                            <th width="9%">Quotation No.</th>
                                            <th>Order By </th>
                                            <th>Contact Name</th>
{{--                                            <th>Company Name </th>--}}
                                            <th>Date</th>
                                            <th data-orderable="false"></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($custom_orders as $order)
                                            <?php
                                                $quotation = \App\Quotation::where('id', $order->quotation_id)->first();
                                                $quotation_contact = App\QuotationContact::where('id', $quotation->contact_id)->first();
                                            ?>
                                            <tr>
                                                <td>{{$order->id}}</td>
                                                <td>{{$order->user->name}}</td>
                                                <td>{{$quotation_contact->contact_person}}</td>
{{--                                                <td>{{$quotation_contact->company_address}}</td>--}}
                                                <td>{{$order->created_at}}</td>
                                                <td>
                                                    <div class="btn-group">
                                                        <a class="btn" href="{{ route('admin.orders.edit', ['id' => $order->id] ) }}">
                                                            <i class="mdi mdi-pencil"></i>
                                                        </a>
                                                        <a class="btn btn-dark" href="{{ route('quotation_show', ['id' => $order->id] ) }}">
                                                            View Quote
                                                        </a>&nbsp;
                                                        <a class="btn btn-info"   href="{{ route('quotation_show', ['id' => $order->id] ) }}">
                                                            Approve Order
                                                        </a>
                                                    </div>
                                                    <div class="btn-group">
                                                        <a class="btn btn-danger" href="{{ route('order.remove', ['id' => $order->id] ) }}">
                                                            <i class="fa fa-trash"></i>
                                                        </a>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('admin.layouts.footer')
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.js"></script>
        <link rel="stylesheet" type="text/css" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/themes/base/jquery-ui.css">
        <script>
            $(function() {
                $('.date-picker').datepicker( {
                    changeMonth: true,
                    changeYear: true,
                    showButtonPanel: true,
                    dateFormat: 'MM yy',
                    onClose: function(dateText, inst) {
                        var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
                        var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                        $(this).datepicker('setDate', new Date(year, month, 1));
                    }
                });
            });
        </script>
        <style>
            .ui-datepicker-calendar {
                display: none;
            }
            .custom-select2{    height: 35px;
                padding: 5px;
            }
        </style>
@endsection