<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailTemplate extends Model
{
    //
    protected $guarded = [];


    public function company() {
        return $this->belongsTo(Company::class, 'company_id', 'id');
    }

    public function media() {
        return $this->belongsTo(Media::class, 'image', 'id');
    }
}
