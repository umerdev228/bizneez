<div class="modal fade" id="priceListModal" tabindex="-1" role="dialog" aria-labelledby="priceListModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" style="min-width: 1000px;">
        <div class="modal-content">
            <div class="modal-header">
                Price Ranges
            </div>
            <div class="modal-body">
                <div class="col-lg-12">
                    <table class="table dt-responsive" id="example">
                        <thead>
                        <tr>
                            <th>Code </th>
                            <th>Product </th>
                            <th>Category </th>
                            <th>Range </th>

                            <th>Length </th>
                            <th>Width </th>
                            <th>Height </th>
                            <th>Depth </th>
                            <th>Price </th>
                            <th>Action </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($productsmdd as $product)
                            <tr style="font-size: 13px;">

                                <td>{{ $product->COL2 }}</td>

                                <td>{{ $product->COL7 }}</td>
                                <td>{{ $product->COL4 }}</td>
                                <td>{{ $product->COL5 }} ({{ $product->COL6 }})</td>

                                <td><span></span>{{ $product->COL10 }}</td>
                                <td><span></span>{{ $product->COL11 }}</td>
                                <td><span></span>{{ $product->COL12 }}</td>
                                <td><span></span>{{ $product->COL13 }}</td>
                                <td><span>&#163;</span>{{ str_replace("?","",$product->COL8)   }}</td>
                                <td>
                                    <button onclick="selectPriceList('{{json_encode($product)}}')" class="btn btn-success btn-sm">Select</button>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
