@extends('includes.meeting_section.master')

@section('content')
    <div class="col-lg-12"  style="margin-top: 100px;">
        <div class="card-box">
            <!-- Left sidebar -->
        @include('admin.email.left-side')
        <!-- End Left sidebar -->

            <div class="inbox-rightbar">

                {{--            <div class="" role="toolbar">--}}
                {{--                <div class="btn-group">--}}
                {{--                    <button type="button" class="btn btn-sm btn-light waves-effect"><i class="mdi mdi-archive font-18 vertical-middle"></i></button>--}}
                {{--                    <button type="button" class="btn btn-sm btn-light waves-effect"><i class="mdi mdi-alert-octagon font-18 vertical-middle"></i></button>--}}
                {{--                    <button type="button" class="btn btn-sm btn-light waves-effect"><i class="mdi mdi-delete-variant font-18 vertical-middle"></i></button>--}}
                {{--                </div>--}}
                {{--                <div class="btn-group">--}}
                {{--                    <button type="button" class="btn btn-sm btn-light dropdown-toggle waves-effect" data-toggle="dropdown" aria-expanded="false">--}}
                {{--                        <i class="mdi mdi-folder font-18 vertical-middle"></i>--}}
                {{--                        <b class="caret m-l-5"></b>--}}
                {{--                    </button>--}}
                {{--                    <div class="dropdown-menu">--}}
                {{--                        <span class="dropdown-header">Move to</span>--}}
                {{--                        <a class="dropdown-item" href="javascript: void(0);">Social</a>--}}
                {{--                        <a class="dropdown-item" href="javascript: void(0);">Promotions</a>--}}
                {{--                        <a class="dropdown-item" href="javascript: void(0);">Updates</a>--}}
                {{--                        <a class="dropdown-item" href="javascript: void(0);">Forums</a>--}}
                {{--                    </div>--}}
                {{--                </div>--}}
                {{--                <div class="btn-group">--}}
                {{--                    <button type="button" class="btn btn-sm btn-light dropdown-toggle waves-effect" data-toggle="dropdown" aria-expanded="false">--}}
                {{--                        <i class="mdi mdi-label font-18 vertical-middle"></i>--}}
                {{--                        <b class="caret m-l-5"></b>--}}
                {{--                    </button>--}}
                {{--                    <div class="dropdown-menu">--}}
                {{--                        <span class="dropdown-header">Label as:</span>--}}
                {{--                        <a class="dropdown-item" href="javascript: void(0);">Updates</a>--}}
                {{--                        <a class="dropdown-item" href="javascript: void(0);">Social</a>--}}
                {{--                        <a class="dropdown-item" href="javascript: void(0);">Promotions</a>--}}
                {{--                        <a class="dropdown-item" href="javascript: void(0);">Forums</a>--}}
                {{--                    </div>--}}
                {{--                </div>--}}

                {{--                <div class="btn-group">--}}
                {{--                    <button type="button" class="btn btn-sm btn-light dropdown-toggle waves-effect" data-toggle="dropdown" aria-expanded="false">--}}
                {{--                        <i class="mdi mdi-dots-horizontal font-18 vertical-middle"></i> More--}}
                {{--                    </button>--}}
                {{--                    <div class="dropdown-menu">--}}
                {{--                        <span class="dropdown-header">More Option :</span>--}}
                {{--                        <a class="dropdown-item" href="javascript: void(0);">Mark as Unread</a>--}}
                {{--                        <a class="dropdown-item" href="javascript: void(0);">Add to Tasks</a>--}}
                {{--                        <a class="dropdown-item" href="javascript: void(0);">Add Star</a>--}}
                {{--                        <a class="dropdown-item" href="javascript: void(0);">Mute</a>--}}
                {{--                    </div>--}}
                {{--                </div>--}}
                {{--            </div>--}}

                <div class="mt-4">

                    <form method="POST" action="{{ route('admin.email.reply.store') }}" enctype="multipart/form-data" novalidate>
                        @csrf
                        <div class="form-group">
                            <input required id="emailTo" name="to" value="{{$email}}" type="email" class="form-control" placeholder="To">
                        </div>

                        <div class="form-group">
                            <input name="subject" type="text" class="form-control" placeholder="Subject" required>
                        </div>

                        <div class="form-group">
                            <textarea name="body" id="editor" cols="30" rows="10"></textarea>
                        </div>

                        <div class="form-group m-b-0">
                            <div class="text-right">
                                {{--                            <button type="button" class="btn btn-success waves-effect waves-light m-r-5"><i class="fa fa-floppy-o"></i></button>--}}
                                {{--                            <button type="button" class="btn btn-success waves-effect waves-light m-r-5"><i class="fa fa-trash-o"></i></button>--}}
                                <button class="btn btn-purple waves-effect waves-light">
                                    <span>Send</span>
                                    <i class="fa fa-send m-l-10"></i>
                                </button>
                            </div>
                        </div>

                    </form>
                </div> <!-- end card-->

            </div>

            <div class="clearfix"></div>
        </div>

    </div> <!-- end Col -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <style href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/dropzone.css"></style>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/dropzone.js"></script>


    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-beta/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
    <link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/examples/assets/app.css">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.0.4/popper.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.11.1/typeahead.bundle.min.js"></script>
    <script src="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>

    <script>


    </script>
@endsection