<?php

namespace App\Exports;

use App\Order;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class InvoicesExport implements FromView
{
    public function view(): View
    {
        $orders = Order::with('product')->get();

        return view('admin.order.excelExportCustom', [
            'orders' => $orders
        ]);
    }
}