@extends('admin.layouts.app')
@section('content')
@include('admin.layouts.topbar')
<!-- Google Chart JS -->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<!-- Start Page content -->
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">
        
@if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif   
        <div class="row">
          <div class="col-9">
            <div class="card-box">

              <form action="{{ url('admin/setting/potal/update') }}" method="post"> 
                {{ csrf_field() }}
                <div class="form-row">
                  <div class="form-group col-md-12">
                    <label for="Name">Name<span class="text-danger">*</span></label>
                    <input type="text" value="{{$user->name}}" name="Name" autocomplete="off"  class="form-control" parsley-trigger="change" required="required">
                      @if ($errors->has('Name'))
                              <span class="error">{{ $errors->first('Name') }}</span>
                      @endif   
                    </div>
                    <div class="form-group col-md-12">
                    <label for="Email_ID">Email_ID<span class="text-danger">*</span></label>
                    <input type="text" name="Email_ID" value="{{$setting->email}}" autocomplete="off" class="form-control" parsley-trigger="change" required="required" />
                      @if ($errors->has('Email_ID'))
                              <span class="error">{{ $errors->first('Email_ID') }}</span>
                      @endif   
                    </div>
                    <div class="form-group col-md-12">
                    <label for="Stripe_ID">Stripe_Email<span class="text-danger">*</span></label>
                    <input type="text" name="Stripe_ID" value="{{$setting->stripe_email}}" autocomplete="off" class="form-control" parsley-trigger="change" required="required" />
                      @if ($errors->has('Stripe_ID'))
                              <span class="error">{{ $errors->first('Stripe_ID') }}</span>
                      @endif   
                    </div>
                     <div class="form-group col-md-12">
                    <label for="Stripe_Password">Stripe_Password<span class="text-danger">*</span></label>
                    <input type="text" name="Stripe_Password" value="{{$setting->stripe_password}}" autocomplete="off" class="form-control" parsley-trigger="change" required="required" />
                      @if ($errors->has('Stripe_Password'))
                              <span class="error">{{ $errors->first('Stripe_Password') }}</span>
                      @endif   
                    </div>
                    <div class="form-group col-md-12">
                    <label for="Address">Address<span class="text-danger">*</span></label>
                    <input type="text" name="Address" value="{{$setting->Address}}" autocomplete="off" class="form-control" parsley-trigger="change" required="required" />
                      @if ($errors->has('Address'))
                              <span class="error">{{ $errors->first('Address') }}</span>
                      @endif   
                    </div>
                    <div class="form-group col-md-12">
                    <label for="Phone">Phone<span class="text-danger">*</span></label>
                    <input type="text" name="Phone" pattern="^[0-9-+\s()]*$" value="{{$setting->phone}}" autocomplete="off" class="form-control" parsley-trigger="change" required="required" />
                      @if ($errors->has('Phone'))
                              <span class="error">{{ $errors->first('Phone') }}</span>
                      @endif   
                    </div>
                      <div class="form-group col-md-12">
                      <label for="Description">Description<span class="text-danger">*</span></label>
                       <textarea class="form-control summernote rounded-0" name="description"  id="exampleFormControlTextarea2" rows="5">{{$setting->description}}</textarea>
                      @if ($errors->has('description'))
                      <span class="error">{{ $errors->first('description') }}</span>
                      @endif
                    </div>
                 </div>
                <button type="submit" class="btn btn-primary">Update</button>
                <button onclick="history.back()" type="button" class="btn">Cancel</button>
              </form>
             
              </div> <!-- end card-box -->
            </div>
            
            </div> <!-- container -->
          </div>
        </div>
      </div>
    </div>
              @include('admin.layouts.footer')
            @endsection
            <style>
            .arrow-none{margin-top:20px!important;}
            </style>