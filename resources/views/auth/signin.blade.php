<!DOCTYPE html>

<html>
<head>
    <meta charset="utf-8">
    <title>Freedom - Responsive One Page HTML Template</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="Freedom - Responsive One Page HTML Template">
    <meta name="author" content="RB Web Design">

    <!-- STYLES -->
    <link href="{{asset('assets/login/assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/login/assets/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/login/assets/css/supersized.css')}}" rel="stylesheet">
    <link href="{{asset('assets/login/assets/css/animate.css')}}" rel="stylesheet">
    <link href="{{asset('assets/login/assets/css/owl.carousel.css')}}" rel="stylesheet">
    <link href="{{asset('assets/login/assets/css/owl.theme.cs')}}s" rel="stylesheet">
    <link href="{{asset('assets/login/assets/css/style.css')}}" rel="stylesheet">

    <!-- GOOGLE FONTS -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,300,400,500,600700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Montserrat%3A400%2C700&#038;ver=4.4.2' rel='stylesheet' type='text/css'>


    <style>
        .site-footer
        {
            background-color:#26272b;
            padding:45px 0 20px;
            font-size:11px;
            line-height:24px;
            color:#737373;
        }
        .site-footer p
        {
            background-color:#26272b;
            padding:45px 0 20px;
            font-size:11px;
            line-height:15px;
            color:#737373;
        }
        .site-footer hr
        {
            border-top-color:#bbb;
            opacity:0.5
        }
        .site-footer hr.small
        {
            margin:20px 0
        }
        .site-footer h6
        {
            color:#fff;
            font-size:11px;
            text-transform:uppercase;
            margin-top:5px;
            letter-spacing:2px
        }
        .site-footer a
        {
            color:#737373;
        }
        .site-footer a:hover
        {
            color:#3366cc;
            text-decoration:none;
        }
        .footer-links
        {
            padding-left:0;
            list-style:none
        }
        .footer-links li
        {
            display:block
        }
        .footer-links a
        {
            color:#737373
        }
        .footer-links a:active,.footer-links a:focus,.footer-links a:hover
        {
            color:#3366cc;
            text-decoration:none;
        }
        .footer-links.inline li
        {
            display:inline-block
        }
        .site-footer .social-icons
        {
            text-align:right
        }
        .site-footer .social-icons a
        {
            width:40px;
            height:40px;
            line-height:40px;
            margin-left:6px;
            margin-right:0;
            border-radius:100%;
            background-color:#33353d
        }
        .copyright-text
        {
            margin:0
        }
        @media (max-width:991px)
        {
            .site-footer [class^=col-]
            {
                margin-bottom:30px
            }
        }
        @media (max-width:767px)
        {
            .site-footer
            {
                padding-bottom:0
            }
            .site-footer .copyright-text,.site-footer .social-icons
            {
                text-align:center
            }
        }
        .social-icons
        {
            padding-left:0;
            margin-bottom:0;
            list-style:none
        }
        .social-icons li
        {
            display:inline-block;
            margin-bottom:4px
        }
        .social-icons li.title
        {
            margin-right:15px;
            text-transform:uppercase;
            color:#96a2b2;
            font-weight:700;
            font-size:11px
        }
        .social-icons a{
            background-color:#eceeef;
            color:#818a91;
            font-size:11px;
            display:inline-block;
            line-height:44px;
            width:44px;
            height:44px;
            text-align:center;
            margin-right:8px;
            border-radius:100%;
            -webkit-transition:all .2s linear;
            -o-transition:all .2s linear;
            transition:all .2s linear
        }
        .social-icons a:active,.social-icons a:focus,.social-icons a:hover
        {
            color:#fff;
            background-color:#29aafe
        }
        .social-icons.size-sm a
        {
            line-height:34px;
            height:34px;
            width:34px;
            font-size:11px
        }
        .social-icons a.facebook:hover
        {
            background-color:#3b5998
        }
        .social-icons a.twitter:hover
        {
            background-color:#00aced
        }
        .social-icons a.linkedin:hover
        {
            background-color:#007bb6
        }
        .social-icons a.dribbble:hover
        {
            background-color:#ea4c89
        }
        @media (max-width:767px)
        {
            .social-icons li.title
            {
                display:block;
                margin-right:0;
                font-weight:600
            }
        }
    </style>


</head>
<body id="top">
@php
    $sidebar_color = \App\SiteSetting::where('key', 'side_bar_color')->first()->data;
$header_logo = \App\SiteSetting::where('key', 'header_logo')->first()->data;
@endphp
@include('layout.header')

<!-- MAIN TOP -->
<div id="main-top" class="main-top">
    <div class="main-content">
        <div class="container content">
            <div class="row text-center">
                <h2>
                    FREEDOM IS
                    <div class="words words-1">
                        <span>LIFE</span>
                        <span>STYLE</span>
                        <span>ALL</span>
                    </div>
                </h2>
            </div>
            <div class="row text-center">
                <a href="#about" class="scroll-down"></a>
            </div>
        </div>
    </div>
</div>

<!-- MAIN CONTENT -->
<div id="main-content" class="main-content">

    <!-- ABOUT -->
    <div id="about" class="section padding padding-top">
        <div class="section-content">
            <div class="row text-center">
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <i class="fa fa-3x fa-bolt"></i>
                    <h2>CREATIVITY</h2>
                    <p class="hidden-xs">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ac lorem ut arcu molestie molestie.</p>
                </div>
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <i class="fa fa-3x fa-bullhorn"></i>
                    <h2>STRATEGY</h2>
                    <p class="hidden-xs">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ac lorem ut arcu molestie molestie.</p>
                </div>
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <i class="fa fa-3x fa-camera"></i>
                    <h2>DESIGN</h2>
                    <p class="hidden-xs">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ac lorem ut arcu molestie molestie.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="section about padding-top">
        <div class="row text-left">
            <div class="col-md-6 col-sm-12 col-xs-12 image">
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12 background">
                <div class="content">
                    <h3>WELCOME TO FREEDOM</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquid ex ea commodi consequat. Quis aute iure reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint obcaecat cupiditat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    <a href="#team" class="btn btn-primary">EXPLORE</a>
                </div>
            </div>
        </div>
    </div>

    <!-- TEAM -->
    <div id="team" class="section padding-top">
        <div class="container">
            <div class="section-heading">
                <h2>TEAM</h2>
                <p>Who We Are</p>
            </div>
            <div class="section-content">
                <div class="row text-center">
                    <div class="col-md-4 col-sm-12 col-xs-12 text-center">
                        <div class="team wow fadeInLeft" data-wow-duration="1s">
                            <img src="http://placehold.it/600x800" alt="" />
                            <div class="mask">
                                <h2>Samanta L.</h2>
                                <p>Web Designer</p>
                                <a href="#" class="info"><i class="fa fa-facebook"></i></a>
                                <a href="#" class="info"><i class="fa fa-twitter"></i></a>
                                <a href="#" class="info"><i class="fa fa-google-plus"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12 col-xs-12 text-center">
                        <div class="team wow fadeInDown" data-wow-duration="1s">
                            <img src="http://placehold.it/600x800" alt="" />
                            <div class="mask">
                                <h2>Pamela K.</h2>
                                <p>Web Designer</p>
                                <a href="#" class="info"><i class="fa fa-facebook"></i></a>
                                <a href="#" class="info"><i class="fa fa-twitter"></i></a>
                                <a href="#" class="info"><i class="fa fa-google-plus"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12 col-xs-12 text-center">
                        <div class="team wow fadeInRight" data-wow-duration="1s">
                            <img src="http://placehold.it/600x800" alt="" />
                            <div class="mask">
                                <h2>Micheal J.</h2>
                                <p>Web Developer</p>
                                <a href="#" class="info"><i class="fa fa-facebook"></i></a>
                                <a href="#" class="info"><i class="fa fa-twitter"></i></a>
                                <a href="#" class="info"><i class="fa fa-google-plus"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- SERVICES -->
    <div id="services" class="section services padding-top padding-bottom">
        <div class="container">
            <div class="section-heading">
                <h2>SERVICES</h2>
                <p>What We Do</p>
            </div>
            <div class="section-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquid ex ea commodi consequat. Quis aute iure reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint obcaecat cupiditat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                <div class="row text-center">
                    <div class="fact col-xs-6 col-md-3 col-lg-3 wow fadeInLeft" data-wow-duration="1s">
                        <i class="fa fa-3x fa-desktop"></i>
                        <h4>WEB DESIGN</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ac lorem ut arcu molestie molestie.</p>
                    </div>
                    <div class="fact col-xs-6 col-md-3 col-lg-3 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.25s">
                        <i class="fa fa-3x fa-code"></i>
                        <h4>WEB DEVELOPER</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ac lorem ut arcu molestie molestie.</p>
                    </div>
                    <div class="fact col-xs-6 col-md-3 col-lg-3 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.5s">
                        <i class="fa fa-3x fa-cogs"></i>
                        <h4>WEB MARKETING</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ac lorem ut arcu molestie molestie.</p>
                    </div>
                    <div class="fact col-xs-6 col-md-3 col-lg-3 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.75s">
                        <i class="fa fa-3x fa-flask"></i>
                        <h4>SEO & SEM</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ac lorem ut arcu molestie molestie.</p>
                    </div>
                </div>
                <div class="row text-center second">
                    <div class="fact col-xs-6 col-md-3 col-lg-3 wow fadeInRight" data-wow-duration="1s">
                        <i class="fa fa-3x fa-camera"></i>
                        <h4>PHOTOGRAPHY</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ac lorem ut arcu molestie molestie.</p>
                    </div>
                    <div class="fact col-xs-6 col-md-3 col-lg-3 wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.25s">
                        <i class="fa fa-3x fa-search-plus"></i>
                        <h4>ANALYTICS</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ac lorem ut arcu molestie molestie.</p>
                    </div>
                    <div class="fact col-xs-6 col-md-3 col-lg-3 wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.5s">
                        <i class="fa fa-3x fa-comment-o"></i>
                        <h4>CONSULTING</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ac lorem ut arcu molestie molestie.</p>
                    </div>
                    <div class="fact col-xs-6 col-md-3 col-lg-3 wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.75s">
                        <i class="fa fa-3x fa-users"></i>
                        <h4>CLIENT SUPPORT</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ac lorem ut arcu molestie molestie.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="info" class="section dark hidden-xs parallax-container" data-parallax="scroll" data-image-src="http://placehold.it/1200x1200" data-natural-width="1200" data-natural-height="1200">
        <div class="section-content">
            <div class="row text-center partner-table">
                <div class="partner col-xs-3 col-md-3 col-lg-3">
                    <h2>25+</h2>
                    <p>Clients</p>
                </div>
                <div class="partner col-xs-3 col-md-3 col-lg-3">
                    <h2>150+</h2>
                    <p>Projects</p>
                </div>
                <div class="partner col-xs-3 col-md-3 col-lg-3">
                    <h2>650+</h2>
                    <p>Coffee</p>
                </div>
                <div class="partner col-xs-3 col-md-3 col-lg-3">
                    <h2>1250+</h2>
                    <p>Codes</p>
                </div>
            </div>
        </div>
    </div>

    <!-- PORTFOLIO -->
    <div id="portfolio" class="section portfolio">
        <div class="section-content">
            <div class="work-item-wrapper">
                <div class="row work-item-list">
                    <div class="col-md-4 col-sm-6 col-xs-12 work-item">
                        <img src="http://placehold.it/600x500" alt="Work Description">
                        <div class="image-overlay">
                            <a href="assets/img/portfolio/work1-ori.jpg" class="media-popup" title="Work #1">
                                <div class="work-item-info">
                                    <h3>Work #1</h3>
                                    <p>Work Description</p>
                                    <i class="fa fa-search-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12 work-item">
                        <img src="http://placehold.it/600x500" alt="Work Description">
                        <div class="image-overlay">
                            <a href="assets/img/portfolio/work2-ori.jpg" class="media-popup" title="Work #2">
                                <div class="work-item-info">
                                    <h3>Work #2</h3>
                                    <p>Work Description</p>
                                    <i class="fa fa-search-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12 work-item">
                        <img src="http://placehold.it/600x500" alt="Work Description">
                        <div class="image-overlay">
                            <a href="assets/img/portfolio/work3-ori.jpg" class="media-popup" title="Work #3">
                                <div class="work-item-info">
                                    <h3>Work #3</h3>
                                    <p>Work Description</p>
                                    <i class="fa fa-search-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12 work-item">
                        <img src="http://placehold.it/600x500" alt="Work Description">
                        <div class="image-overlay">
                            <a href="assets/img/portfolio/work4-ori.jpg" class="media-popup" title="Work #4">
                                <div class="work-item-info">
                                    <h3>Work #4</h3>
                                    <p>Work Description</p>
                                    <i class="fa fa-search-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12 work-item">
                        <img src="http://placehold.it/600x500" alt="Work Description">
                        <div class="image-overlay">
                            <a href="assets/img/portfolio/work5-ori.jpg" class="media-popup" title="Work #5">
                                <div class="work-item-info">
                                    <h3>Work #5</h3>
                                    <p>Work Description</p>
                                    <i class="fa fa-search-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12 work-item">
                        <img src="http://placehold.it/600x500" alt="Work Description">
                        <div class="image-overlay">
                            <a href="assets/img/portfolio/work6-ori.jpg" class="media-popup" title="Work #6">
                                <div class="work-item-info">
                                    <h3>Work #6</h3>
                                    <p>Work Description</p>
                                    <i class="fa fa-search-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="purchase" class="section dark">
        <div class="section-content">
            <div class="container partner-table">
                <div class="col-md-12 col-sm-12 col-xs-12 text-center partner">
                    <a href="https://themeforest.net/item/freedom-responsive-one-page-html-template/16094269?ref=RBWebDesign" class="btn btn-primary btn-lg">Purchase Now</a>
                </div>
            </div>
        </div>
    </div>

    <!-- NEWSLETTER -->
    <div class="section newsletter">
        <div class="section-content">
            <div class="container">
                <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                    <h3 class="wow fadeInDown" data-wow-duration="1s">Subscribe To Our Newsletter</h3>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                    <input type="text" class="wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.5s" name="newsletter" placeholder="Enter your email..." />
                    <br />
                    <button type="submit" class="btn btn-primary btn-lg wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s" name="send">Subscribe</button>
                </div>
            </div>
        </div>
    </div>

    <!-- CLIENTS -->
    <div id="clients" class="section">
        <div class="section-content">
            <div class="container">
                <div id="owl-clients" class="owl-carousel">
                    <div>
                        <a href=""><img src="assets/img/clients/logo-client1.png" alt="" /></a>
                    </div>
                    <div>
                        <a href=""><img src="assets/img/clients/logo-client2.png" alt="" /></a>
                    </div>
                    <div>
                        <a href=""><img src="assets/img/clients/logo-client3.png" alt="" /></a>
                    </div>
                    <div>
                        <a href=""><img src="assets/img/clients/logo-client4.png" alt="" /></a>
                    </div>
                    <div>
                        <a href=""><img src="assets/img/clients/logo-client5.png" alt="" /></a>
                    </div>
                    <div>
                        <a href=""><img src="assets/img/clients/logo-client6.png" alt="" /></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- TESTIMONIALS -->
    <div id="testimonials" class="section testimonials parallax-container" data-parallax="scroll" data-image-src="http://placehold.it/1200x1200" data-natural-width="1200" data-natural-height="1200">
        <div class="section-heading">
            <h2>TESTIMONIALS</h2>
            <p>What Customers Say About Us</p>
        </div>
        <div class="section-content">
            <div class="container">
                <div id="owl-testimonials" class="owl-carousel">
                    <div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquid ex ea commodi consequat. Quis aute iure reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                        <p class="mini">- John K. -</p>
                    </div>
                    <div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua.</p>
                        <p class="mini">- Jim P. -</p>
                    </div>
                    <div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquid ex ea commodi consequat.</p>
                        <p class="mini">- Morgan F. -</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- CONTACT -->
    <div id="contact" class="section contact padding-top">
        <div class="section-heading">
            <h2>CONTACT</h2>
            <p>Contact Us</p>
        </div>
        <div class="section-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="contact-info">
                            <ul class="list-unstyled">
                                <li>
                                    <h2><i class="fa fa-home"></i> ADDRESS</h2>
                                    <p>123 Street, New York (USA)</p>
                                </li>
                                <li>
                                    <h2><i class="fa fa-phone"></i> PHONE</h2>
                                    <p>+123 456 789</p>
                                </li>
                                <li>
                                    <h2><i class="fa fa-envelope"></i> EMAIL</h2>
                                    <p><a href="mailto:info@myemail.com">info@myemail.com</a></p>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <div class="contact-form">
                            <div class="alert"></div>
                            <form id="contact-form" action="#" method="post">
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <label class="sr-only" for="name">Name:</label>
                                        <input type="text" id="name" name="name" placeholder="Name *" required>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <label class="sr-only" for="email">Email:</label>
                                        <input type="email" id="email" name="email" placeholder="Email *" required>
                                    </div>
                                </div>
                                <div class="row text-right">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <label class="sr-only" for="message">Message:</label>
                                        <textarea id="message" name="message" placeholder="Message *" required></textarea>
                                        <button type="submit" class="btn btn-primary btn-lg" name="send">SEND</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- SOCIAL -->
<div class="section social padding-top">
    <div class="section-content">
        <a href="">
            <div class="col-md-3 col-sm-3 col-xs-3 text-center facebook">
                <i class="fa fa-3x fa-facebook"></i>
            </div>
        </a>
        <a href="">
            <div class="col-md-3 col-sm-3 col-xs-3 text-center twitter">
                <i class="fa fa-3x fa-twitter"></i>
            </div>
        </a>
        <a href="">
            <div class="col-md-3 col-sm-3 col-xs-3 text-center google-plus">
                <i class="fa fa-3x fa-google-plus"></i>
            </div>
        </a>
        <a href="">
            <div class="col-md-3 col-sm-3 col-xs-3 text-center linkedin">
                <i class="fa fa-3x fa-linkedin"></i>
            </div>
        </a>
    </div>
</div>

<!-- FOOTER -->
<footer id="footer" class="section">
    <div class="container">
        <p class="copyright">&copy; 2016. Freedom. All Rights Reserved.</p>
    </div>
</footer>

<footer class="site-footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-6">
                <img width="200" src="{{$header_logo}}" alt="">
                <p style="font-size: 11px;" class="text-justify">
                    Bizneez is second to none. We provide office furniture and office design. We believe that with the right type of products and design layout can change any company over-night by inspiring your workforce to work better and more efficient, this is how you should see your self as a business. By investing in the right office design plan. Bizneez Group Ltd will then work around to suit your budget. We know the design world and we also know having the right type of office furniture makes a business succeed, so sourcing both the aspects of design and furniture will place you in the perfect solution when designing your business.
                </p>
            </div>
            <div class="col-xs-6 col-md-2">
                <!--     <h6>Categories</h6>
                    <ul class="footer-links">
                            <li><a href="">C</a></li>
                            <li><a href="">UI Design</a></li>
                            <li><a href="">PHP</a></li>
                            <li><a href="">Java</a></li>
                            <li><a href="">Android</a></li>
                            <li><a href="">Templates</a></li>
                    </ul> -->
            </div>

            <div class="col-xs-6 col-md-2">
                <!--  <h6>Quick Links</h6>
                 <ul class="footer-links">
                         <li><a href="">About Us</a></li>
                         <li><a href="">Contact Us</a></li>
                         <li><a href="">Contribute</a></li>
                         <li><a href="">Privacy Policy</a></li>
                         <li><a href="">Sitemap</a></li>
                 </ul> -->
            </div>

            <div class="col-xs-6 col-md-2">
                <!-- <h6>Quick Links</h6>
                <ul class="footer-links">
                        <li><a href="">About Us</a></li>
                        <li><a href="">Contact Us</a></li>
                        <li><a href="">Contribute</a></li>
                        <li><a href="policy/">Privacy Policy</a></li>
                        <li><a href="">Sitemap</a></li>
                </ul> -->
            </div>
        </div>
        <hr>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-6 col-xs-12">
                <p class="copyright-text">Copyright &copy; 2021 All Rights Reserved by

                </p>
            </div>

            <div class="col-md-4 col-sm-6 col-xs-12">
                <ul class="social-icons">
                    <li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a class="dribbble" href="#"><i class="fa fa-dribbble"></i></a></li>
                    <li><a class="linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>

<div class="back-to-top">
    <a href="#top"><i class="fa fa-4x fa-angle-up"></i></a>
</div>

<!-- JAVASCRIPT -->
<script src="{{asset('assets/login/assets/js/jquery-2.1.0.min.js')}}"></script>
<script src="{{asset('assets/login/assets/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/login/assets/js/jquery.easing.min.js')}}"></script>
<script src="{{asset('assets/login/assets/js/supersized.3.2.7.min.js')}}"></script>
<script src="{{asset('assets/login/assets/js/jquery.magnific-popup.min.js')}}"></script>
<script src="{{asset('assets/login/assets/js/wow.js')}}"></script>
<script src="{{asset('assets/login/assets/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('assets/login/assets/js/parallax.min.js')}}"></script>
<script src="{{asset('assets/login/assets/js/jquery.localScroll.min.js')}}"></script>
<script src="{{asset('assets/login/assets/js/jquery.scrollTo.min.js')}}"></script>
<script src="{{asset('assets/login/assets/js/main.js')}}"></script>
<script>
    new WOW().init();

    $(document).ready(function() {

        $("#owl-clients").owlCarousel({

            slideSpeed : 500,
            paginationSpeed : 1000,
            singleItem: false,
            items: 4,

            //Autoplay
            autoPlay : 3000,
            stopOnHover : false

        });

        $("#owl-testimonials").owlCarousel({

            slideSpeed : 1000,
            paginationSpeed : 2000,
            singleItem: true,
            items: 1,

            //Autoplay
            autoPlay : 5000,
            stopOnHover : false

        });

    });
</script>
</body>
</html>