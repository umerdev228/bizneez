@extends('admin.layouts.app')
@section('content')
@include('admin.layouts.topbar')
<!-- Google Chart JS -->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<!-- Start Page content -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                
                    @if(session()->has('message'))
    <div class="alert alert-success mt-5" id="myElem" >
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
        {{ session()->get('message') }}
    </div>

@endif   


                    <div class="row">
                        <div class="col-12 mt-5">
                            <div class="card-box">
                        
      
                              <a href="{{url('admin/categories_add')}}"  class="btn btn-primary" ><i class="fa fa-plus"></i>Add New client </a>
                              <table class="table table-hover m-0 tickets-list table-actions-bar dt-responsive nowrap" cellspacing="0" width="100%" id="normal-table">
                              <thead>
                                <tr>
                                  
                                  <th>Name</th>
                                  <th>Email</th>      
                                  <th>Phone</th>
                                  <th>Detail</th>
                                  <th>Created </th>      

                                  <th   data-orderable="false">Action</th>
                                </tr>
                              </thead>
                              <tbody>
                               
                                  
                                  @foreach($client as $pak)
                                  <tr> 
                                   
                                  <td>{{$pak->name}}</td>
                                  <td>{{$pak->email}}</td>
                                  <td>{{$pak->phone}}</td>
                                  <td>{{$pak->description}}</td> 
                                  <td>{{$pak->created_at}}</td>
                                  
                                  <td>
                                  <div class="btn-group dropdown">    
                                     <a href="{{ URL::to('admin/option/edit/' . $pak->id ) }}"><i class="mdi mdi-pencil"></i></a> 
                                     &nbsp;
                                     &nbsp;
                                     &nbsp;
                                     &nbsp;
                                     &nbsp;
                                        <a onclick="return confirm('Are you want to sure remove permanent or not?')" href="{{ URL::to('admin/option/remove/' . $pak->id ) }}"><i class="fa fa-close  btnclassforoption"   ></i></a> 
                                  </div>
                                   
                                  </td>
                                </tr>
                                  @endforeach
                            </tbody>
                            </table>
                                
                                
                                    </div> <!-- end card -->
                                    </div><!-- end col -->
                                </div>
                                
                            </div>
                        </div>
                        
                                </div> <!-- container -->
                                </div> <!-- content -->
                                
                                @include('admin.layouts.footer')
                                @endsection
                               