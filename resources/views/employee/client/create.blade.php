@extends('admin.layouts.app')
@section('content')
    @include('admin.layouts.topbar')
    <!-- Google Chart JS -->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <!-- Start Page content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if(session()->has('message'))
                        <div class="alert alert-success" style="width:803px ">
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-9 mt-5">
                            <div class="card-box">
                                <form method="POST" action="{{ route('employee.clients.store') }}" enctype="multipart/form-data">
                                    @csrf
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="first_name">First Name<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control valid" name="first_name" id="first_name" required />
                                                @if ($errors->has('first_name'))
                                                    <span class="error">{{ $errors->first('first_name') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="middle_name">Middle Name<span class="text-danger"></span></label>
                                                <input type="text" class="form-control valid" name="middle_name" id="middle_name"/>
                                                @if ($errors->has('middle_name'))
                                                    <span class="error">{{ $errors->first('middle_name') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="last_name">Last Name<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control valid" name="last_name" id="last_name" required />
                                                @if ($errors->has('last_name'))
                                                    <span class="error">{{ $errors->first('first_name') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="email">Email<span class="text-danger">*</span></label>
                                                <input type="email" class="form-control valid" name="email" id="email" required />
                                                @if ($errors->has('email'))
                                                    <span class="error">{{ $errors->first('email') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="phone">Phone<span class="text-danger">*</span></label>
                                                <input type="number" class="form-control valid" name="phone" id="phone" required />
                                                @if ($errors->has('phone'))
                                                    <span class="error">{{ $errors->first('phone') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="mobile">Mobile<span class="text-danger">*</span></label>
                                                <input type="number" class="form-control valid" name="mobile" id="mobile" required />
                                                @if ($errors->has('mobile'))
                                                    <span class="error">{{ $errors->first('mobile') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="assistant_number">Assistant Number<span class="text-danger">*</span></label>
                                                <input type="number" class="form-control valid" name="assistant_number" id="assistant_number" required />
                                                @if ($errors->has('assistant_number'))
                                                    <span class="error">{{ $errors->first('assistant_number') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="address">Address<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control valid" name="address" id="address" required />
                                                @if ($errors->has('address'))
                                                    <span class="error">{{ $errors->first('address') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="location_id">Location<span class="text-danger">*</span></label>
                                                <select class="form-control valid" name="location_id" id="location_id" required>
                                                    <option value="1">Location</option>
                                                </select>
                                                @if ($errors->has('location_id'))
                                                    <span class="error">{{ $errors->first('location_id') }}</span>

                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="area_id">Area<span class="text-danger">*</span></label>
                                                <select class="form-control valid" name="area_id" id="area_id" required>
                                                    <option value="1">Area</option>
                                                </select>
                                                @if ($errors->has('area_id'))
                                                    <span class="error">{{ $errors->first('area_id') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="image">Profile Photo<span class="text-danger">*</span></label>
                                                <input type="file" class="form-control valid" name="image" id="image" required />
                                                @if ($errors->has('image'))
                                                    <span class="error">{{ $errors->first('image') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="Description">Description<span class="text-danger">*</span></label>
                                                <textarea id="Description" class="form-control rounded-0" name="description" rows="5"></textarea>
                                                @if ($errors->has('description'))
                                                    <span class="error">{{ $errors->first('description') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group mt-3">
                                        <input class="btn btn-primary" type="submit" value="Update">
                                        <button onclick="history.back()" type="button" class="btn">Cancel</button>
                                    </div>
                                </form>
                            </div> <!-- end card-box -->
                        </div>

                    </div> <!-- container -->
                </div> <!-- content -->
            </div>
        </div>
    </div>

@endsection
