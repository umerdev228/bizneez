<?php

namespace App\Http\Controllers;

use App\ColorCode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ColorCodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $colors = ColorCode::all();
        return view('admin.color-code.index', compact('colors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        ColorCode::create([
            'code' => $request->color,
            'created_by' => Auth::id(),
        ]);
        toastr()->success('Color Code Added Successfully');
        return redirect()->route('admin.colors.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ColorCode  $colorCode
     * @return \Illuminate\Http\Response
     */
    public function show(ColorCode $colorCode)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ColorCode  $colorCode
     * @return \Illuminate\Http\Response
     */
    public function edit(ColorCode $colorCode)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ColorCode  $colorCode
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ColorCode $colorCode)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ColorCode  $colorCode
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        ColorCode::where('id', $id)->delete();
        toastr()->success('Color Code Deleted Successfully');
        return redirect()->back();
    }

    public function getAllColorCodes() {
        $colors = ColorCode::all('code');
        return response()->json([
            'type' => 'success',
            'colors' => $colors
        ]);
    }
}
