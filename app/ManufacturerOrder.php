<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ManufacturerOrder extends Model
{
    //
    protected $guarded = [];

    public function order() {
        return $this->belongsTo(Order::class, 'order_id', 'id')->with('products');
    }
    public function architect() {
        return $this->belongsTo(User::class, 'architect_id', 'id');
    }
}
