<?php

namespace App\Http\Controllers;

use App\Advertisement;
use App\Company;
use App\Media;
use Illuminate\Http\Request;

class AdvertisementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $advertisements = Advertisement::with('media')->get();
        return view('admin.search_engine.index', compact('advertisements'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $media = Media::where('name', 'gallery')->get();
        $companies = Company::all();
        return view('admin.search_engine.create', compact('media', 'companies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        Advertisement::create([
            'title' => $request->title,
            'platform' => $request->platform,
            'image' => $request->image,
            'company_id' => $request->company_id,
            'shape' => $request->shape,
            'date' => $request->date,
            'time' => $request->time,

        ]);
        toastr()->success('Advertisement Setting Created Successfully');

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Advertisement  $advertisement
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $advertisement = Advertisement::where('id', $id)->with('media')->first();
        $media = Media::where('name', 'gallery')->get();
        $companies = Company::all();
        return view('admin.search_engine.show', compact('advertisement', 'media', 'companies'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Advertisement  $advertisement
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $advertisement = Advertisement::where('id', $id)->with('media')->first();
        $media = Media::where('name', 'gallery')->get();
        $companies = Company::all();
        return view('admin.search_engine.edit', compact('advertisement', 'media', 'companies'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Advertisement  $advertisement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        Advertisement::where('id', $id)->update([
            'title' => $request->title,
            'platform' => $request->platform,
            'image' => $request->image,
            'company_id' => $request->company_id,
            'shape' => $request->shape,
            'date' => $request->date,
            'time' => $request->time,

        ]);
        toastr()->success('Advertisement Setting Updated Successfully');
        return redirect()->route('search_engine');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Advertisement  $advertisement
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Advertisement::where('id', $id)->delete();
        toastr()->success('Advertisement Setting Deleted Successfully');
        return redirect()->back();
    }
}
