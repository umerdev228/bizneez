<?php

namespace App\Http\Controllers;

use App\CategoryMedia;
use Illuminate\Http\Request;

class CategoryMediaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CategoryMedia  $categoryMedia
     * @return \Illuminate\Http\Response
     */
    public function show(CategoryMedia $categoryMedia)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CategoryMedia  $categoryMedia
     * @return \Illuminate\Http\Response
     */
    public function edit(CategoryMedia $categoryMedia)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CategoryMedia  $categoryMedia
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CategoryMedia $categoryMedia)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CategoryMedia  $categoryMedia
     * @return \Illuminate\Http\Response
     */
    public function destroy(CategoryMedia $categoryMedia)
    {
        //
    }
}
