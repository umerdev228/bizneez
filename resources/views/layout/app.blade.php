<!DOCTYPE html>
<html lang="en">
@php
    $sidebar_color = \App\SiteSetting::where('key', 'side_bar_color')->first()->data;
$header_logo = \App\SiteSetting::where('key', 'header_logo')->first()->data;
@endphp
@include ('layout.head')

<body>
<!-- Preloader Start -->
<div id="preloader-active">
    <div class="preloader d-flex align-items-center justify-content-center">
        <div class="preloader-inner position-relative">
            <div class="preloader-circle"></div>
            <div class="preloader-img pere-text">
                <img src="{{asset('assets/img/load.png')}}" alt="">
            </div>
        </div>
    </div>
</div>
<!-- Preloader Start -->
@include('layout.header')
@toastr_css

    
<!-- main content of page -->
@yield('content')
   

@include ('layout.footer')
@toastr_js
@toastr_render
  