<?php

namespace App\Http\Controllers;

use App\Imports\ProductRangeImport;
use App\ProductRange;
use App\User;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ProductRangeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $manufacturers = User::role('manufacturer')->with('ranges')->get();
        return view('admin.product.range.index',compact('manufacturers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProductRange  $productRange
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $ranges = ProductRange::where('created_by', $id)->with('images')->get();
        return view('admin.product.range.show', compact('ranges'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProductRange  $productRange
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductRange $productRange)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProductRange  $productRange
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProductRange $productRange)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProductRange  $productRange
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductRange $productRange)
    {
        //
    }

    public function import(Request $request) {
        Excel::import(new ProductRangeImport(),request()->file('file'));
        return redirect()->back();
    }

    public function getRangesByManufacturer(Request $request) {
        $ranges = ProductRange::where('created_by', $request->id)->get();
        return response()->json([
            'type' => 'success',
            'ranges' => $ranges,
            ]);
    }
}
