@extends('admin.layouts.app')
@section('content')
    @include('admin.layouts.topbar')
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="row">
                        <div class="col-12 mt-5">
                            <div class="card-box">
                                <div class="text-center mt-2">
                                    <div class="row">
                                        @if(Auth::user()->hasRole(['admin']))

                                            <div class="col-md-6 col-xl-2">
                                                <div class="card-box widget-flat border-success  text-white" style="background-color: #0857a7">
                                                    <a href="{{route('admin.products.index')}}">
                                                        <i class="mdi mdi-wallet-giftcard"></i>
                                                        <h3 class="m-b-10 resolved1"> {{ $product }} </h3>
                                                        <h4 class="text-uppercase m-b-5">Products</h4>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-xl-2">
                                                <div class="card-box widget-flat border-success bg-success text-white">
                                                    <a href="{{route('admin.orders.index')}}">
                                                        <i class="mdi mdi-rotate-left"></i>
                                                        <h3 class="m-b-10 resolved1"> {{ $orders }} </h3>
                                                        <h4 class="text-uppercase m-b-5"> Quotations</h4>
                                                    </a>
                                                </div>
                                            </div>

                                            <div class="col-md-6 col-xl-2">
                                                <div class="card-box widget-flat border-success text-white" style="background-color: #8d06d8">
                                                    <a href="{{route('admin.orders.index')}}">
                                                        <i class="mdi mdi-format-align-top"></i>
                                                        <h3 class="m-b-10 resolved1"> 0 </h3>
                                                        <h4 class="text-uppercase m-b-5"> CRM</h4>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-xl-2">
                                                <div class="card-box widget-flat border-custom  text-white" style="background-color: #d05933">
                                                    <a href="{{route('admin.employees.index')}}">
                                                        <i class="mdi mdi-human-handsdown"></i>
                                                        <h3 class="m-b-10 pendding1">{{$employee}} </h3>
                                                        <h4 class="text-uppercase m-b-5">Employees</h4>
                                                    </a>
                                                </div>
                                            </div>


                                            <div class="col-md-6 col-xl-2">
                                                <div class="card-box  widget-flat border-danger text-white" style="background-color: #b5d2cf">
                                                    <a href="{{ route('admin.manufacturers.index', ['role' => Auth::user()->roles[0]->name]) }}">
                                                        <i class="mdi mdi-account-multiple"></i>
                                                        <h3 class="m-b-10 open1">{{$manufacturer}}</h3>
                                                        <h4 class="text-uppercase m-b-5">Manufactures</h4>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-xl-2">
                                                <div class="card-box widget-flat  text-white" style="background-color: #bfbfdf">
                                                    <a href="{{ route('admin.architects.index') }}">
                                                        <i class="mdi mdi-account-check"></i>
                                                        <h3 class="m-b-10 unresolved1"> {{$architect}}</h3>
                                                        <h4 class="text-uppercase m-b-5"> Architects</h4>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-xl-2">
                                                <div class="card-box widget-flat border-custom text-white" style="background-color: #e3e2ce">
                                                    <a href="{{route('admin.clients.index')}}">
                                                        <i class="mdi mdi-cash-100"></i>
                                                        <h3 class="m-b-10 pendding1">{{$client}} </h3>
                                                        <h4 class="text-uppercase m-b-5">Clients</h4>
                                                    </a>
                                                </div>
                                            </div>
                                          
                                            <div class="col-md-6 col-xl-2">
                                                <div class="card-box widget-flat border-success  text-white" style="background-color: #bd0533">
                                                    <a href="">
                                                        <i class="mdi mdi-account-multiple-plus"></i>
                                                        <h3 class="m-b-10 resolved1"> 0 </h3>
                                                        <h4 class="text-uppercase m-b-5">Procurement</h4>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-xl-2">
                                                <div class="card-box widget-flat border-success text-white" style="background-color: #7f7cfd">
                                                    <a href="">
                                                        <i class="mdi mdi-cash-multiple"></i>
                                                        <h3 class="m-b-10 resolved1"> 0 </h3>
                                                        <h4 class="text-uppercase m-b-5">Finance</h4>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-xl-2">
                                                <div class="card-box widget-flat border-success bg-dark text-white">
                                                    <a href="{{route('admin.categories.index')}}">
                                                        <i class="mdi mdi-link-variant-off"></i>
                                                        <h3 class="m-b-10 resolved1"> {{ $categories }} </h3>
                                                        <h4 class="text-uppercase m-b-5">Categories</h4>
                                                    </a>
                                                </div>
                                            </div>
                                          


                                            <div class="col-md-6 col-xl-2">
                                                <div class="card-box widget-flat border-success  text-white" style="background-color: #f9b35d">
                                                    <a href="{{route('admin.mobile.uploads.media')}}">
                                                        <i class="mdi mdi-chemical-weapon"></i>
                                                        <h3 class="m-b-10 resolved1">  0 </h3>
                                                        <h4 class="text-uppercase m-b-5"> AI</h4>
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if(Auth::user()->hasRole(['employee']))
                                                <div class="col-md-6 col-xl-2">
                                                    <div class="card-box widget-flat bg-info text-white">
                                                        <a href="{{ route('employee.architects.index', ['role' => Auth::user()->roles[0]->name]) }}">
                                                            <i class="mdi mdi-account-check"></i>
                                                            <h3 class="m-b-10 unresolved1"> {{$architect}}</h3>
                                                            <h4 class="text-uppercase m-b-5"> Architects</h4>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-xl-2">
                                                    <div class="card-box widget-flat border-custom bg-danger text-white">
                                                        <a href="{{route('employee.clients.index', ['role' => Auth::user()->roles[0]->name])}}">
                                                            <i class="mdi mdi-cash-100"></i>
                                                            <h3 class="m-b-10 pendding1">{{$client}} </h3>
                                                            <h4 class="text-uppercase m-b-5">Clients</h4>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-xl-2">
                                                    <div class="card-box widget-flat border-success bg-pink text-white">
                                                        <a href="{{route('employee.products.index')}}">
                                                            <i class="mdi mdi-rocket"></i>
                                                            <h3 class="m-b-10 resolved1"> {{ $product }} </h3>
                                                            <h4 class="text-uppercase m-b-5">Products</h4>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-xl-2">
                                                    <div class="card-box widget-flat border-success bg-pink text-white">
                                                        <a href="{{route('employee.orders.index')}}">
                                                            <i class="mdi mdi-rotate-left"></i>
                                                            <h3 class="m-b-10 resolved1"> {{ $orders }} </h3>
                                                            <h4 class="text-uppercase m-b-5"> Quotations</h4>
                                                        </a>
                                                    </div>
                                                </div>
                                            @endif
                                            @if(Auth::user()->hasRole(['architect']))
                                                <div class="col-md-6 col-xl-2">
                                                    <div class="card-box widget-flat border-success bg-pink text-white">
                                                        <a href="{{route('architect.products.index')}}">
                                                            <i class="mdi mdi-rocket"></i>
                                                            <h3 class="m-b-10 resolved1"> {{ $product }} </h3>
                                                            <h4 class="text-uppercase m-b-5">Products</h4>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-xl-2">
                                                    <div class="card-box widget-flat border-success bg-pink text-white">
                                                        <a href="{{route('architect.orders.index')}}">
                                                            <i class="mdi mdi-rotate-left"></i>
                                                            <h3 class="m-b-10 resolved1"> {{ $orders }} </h3>
                                                            <h4 class="text-uppercase m-b-5"> Quotations</h4>
                                                        </a>
                                                    </div>
                                                </div>
                                            @endif
                                            @if(Auth::user()->hasRole(['manufacturer']))
                                                <div class="col-md-6 col-xl-2">
                                                    <div class="card-box widget-flat border-success bg-pink text-white">
                                                        <a href="{{route(Auth::user()->roles[0]->name.'.orders.index')}}">
                                                            <i class="mdi mdi-rotate-left"></i>
                                                            <h3 class="m-b-10 resolved1"> {{ $orders }} </h3>
                                                            <h4 class="text-uppercase m-b-5">Purchase Orders</h4>
                                                        </a>
                                                    </div>
                                                </div>
                                            @endif
                                            {{--                                    <div class="col-md-6 col-xl-2">--}}
                                            {{--                                        <div class="card-box widget-flat border-success bg-pink text-white">--}}
                                            {{--                                            <a href="">--}}
                                            {{--                                                <i class="mdi mdi-rotate-left"></i>--}}
                                            {{--                                                <h3 class="m-b-10 resolved1"> 0 </h3>--}}
                                            {{--                                                <h4 class="text-uppercase m-b-5">Invoices</h4>--}}
                                            {{--                                            </a>--}}
                                            {{--                                        </div>--}}
                                            {{--                                    </div>--}}
                                            {{--                                    <div class="col-md-6 col-xl-2">--}}
                                            {{--                                        <div class="card-box widget-flat border-success bg-pink text-white">--}}
                                            {{--                                            <a href="">--}}
                                            {{--                                                <i class="mdi mdi-feather"></i>--}}
                                            {{--                                                <h3 class="m-b-10 resolved1"> 0 </h3>--}}
                                            {{--                                                <h4 class="text-uppercase m-b-5">Quotations</h4>--}}
                                            {{--                                            </a>--}}
                                            {{--                                        </div>--}}
                                            {{--                                    </div>--}}

                                            <div class="col-md-6 col-xl-2">
                                                <div class="card-box widget-flat border-success bg-pink text-white">
                                                    <a href="">
                                                        <i class="mdi mdi-ungroup"></i>
                                                        <h3 class="m-b-10 resolved1"> 0 </h3>
                                                        <h4 class="text-uppercase m-b-5">Projects</h4>
                                                    </a>
                                                </div>
                                            </div>
                                    </div>

                                    <div class="row  mt-8">
                                        <div class="col-md-6 mt-8">
                                            <br>
                                            <div class="card-box">
        
                                                <div class="chart mt-4" id="line-chart">
                                                    <div style="position: relative;">
                                                        <div dir="ltr" style="position: relative; width: 745px; height: 340px;">
                                                            <div aria-label="A chart." style="position: absolute; left: 0px; top: 0px; width: 100%; height: 100%;">
                                                                <svg width="745" height="340" aria-label="A chart." style="overflow: hidden;">
                                                                    <defs id="_ABSTRACT_RENDERER_ID_0">
                                                                        <clipPath id="_ABSTRACT_RENDERER_ID_1">
                                                                            <rect x="60" y="20" width="671" height="300"></rect>
                                                                        </clipPath>
                                                                    </defs>
                                                                    <rect x="0" y="0" width="745" height="340" stroke="none" stroke-width="0" fill="#ffffff"></rect>
                                                                    <g>
                                                                        <rect x="299" y="3" width="192" height="14" stroke="none" stroke-width="0" fill-opacity="0" fill="#ffffff"></rect>
                                                                        <g>
                                                                            <rect x="299" y="3" width="76" height="14" stroke="none" stroke-width="0" fill-opacity="0" fill="#ffffff"></rect>
                                                                            <g>
                                                                                <text text-anchor="start" x="332" y="14.9" font-family="Roboto" font-size="14" stroke="none" stroke-width="0" fill="#222222">Quotation</text>
                                                                            </g>
                                                                            <path d="M299,10L327,10" stroke="#4eb7eb" stroke-width="3" fill-opacity="1" fill="none"></path>
                                                                            <circle cx="313" cy="10" r="3" stroke="none" stroke-width="0" fill="#4eb7eb"></circle>
                                                                        </g>
                                                                        <g>
                                                                            <rect x="398" y="3" width="93" height="14" stroke="none" stroke-width="0" fill-opacity="0" fill="#ffffff"></rect>
                                                                            <g>
                                                                                <text text-anchor="start" x="431" y="14.9" font-family="Roboto" font-size="14" stroke="none" stroke-width="0" fill="#222222">Invoices</text>
                                                                            </g>
                                                                            <path d="M398,10L426,10" stroke="#f1556c" stroke-width="3" fill-opacity="1" fill="none"></path>
                                                                            <circle cx="412" cy="10" r="3" stroke="none" stroke-width="0" fill="#f1556c"></circle>
                                                                        </g>
                                                                    </g>
                                                                    <g>
                                                                        <rect x="60" y="20" width="671" height="300" stroke="none" stroke-width="0" fill-opacity="0" fill="#ffffff"></rect>
                                                                        <g clip-path="url(file:///F:/xampp/htdocs/projects/highdmin-13/highdmin-13/Highdmin_v1.3/Admin/HTML/vertical/chart-google.html#_ABSTRACT_RENDERER_ID_1)">
                                                                            <g>
                                                                                <rect x="60" y="319" width="671" height="1" stroke="none" stroke-width="0" fill="#f5f5f5"></rect>
                                                                                <rect x="60" y="276" width="671" height="1" stroke="none" stroke-width="0" fill="#f5f5f5"></rect>
                                                                                <rect x="60" y="234" width="671" height="1" stroke="none" stroke-width="0" fill="#f5f5f5"></rect>
                                                                                <rect x="60" y="191" width="671" height="1" stroke="none" stroke-width="0" fill="#f5f5f5"></rect>
                                                                                <rect x="60" y="148" width="671" height="1" stroke="none" stroke-width="0" fill="#f5f5f5"></rect>
                                                                                <rect x="60" y="105" width="671" height="1" stroke="none" stroke-width="0" fill="#f5f5f5"></rect>
                                                                                <rect x="60" y="63" width="671" height="1" stroke="none" stroke-width="0" fill="#f5f5f5"></rect>
                                                                                <rect x="60" y="20" width="671" height="1" stroke="none" stroke-width="0" fill="#f5f5f5"></rect>
                                                                                <rect x="60" y="298" width="671" height="1" stroke="none" stroke-width="0" fill="#fbfbfb"></rect>
                                                                                <rect x="60" y="255" width="671" height="1" stroke="none" stroke-width="0" fill="#fbfbfb"></rect>
                                                                                <rect x="60" y="212" width="671" height="1" stroke="none" stroke-width="0" fill="#fbfbfb"></rect>
                                                                                <rect x="60" y="170" width="671" height="1" stroke="none" stroke-width="0" fill="#fbfbfb"></rect>
                                                                                <rect x="60" y="127" width="671" height="1" stroke="none" stroke-width="0" fill="#fbfbfb"></rect>
                                                                                <rect x="60" y="84" width="671" height="1" stroke="none" stroke-width="0" fill="#fbfbfb"></rect>
                                                                                <rect x="60" y="41" width="671" height="1" stroke="none" stroke-width="0" fill="#fbfbfb"></rect>
                                                                            </g>
                                                                            <g>
                                                                                <rect x="60" y="319" width="671" height="1" stroke="none" stroke-width="0" fill="#333333"></rect>
                                                                            </g>
                                                                            <g>
                                                                                <path d="M108.35714285714286,137.9642857142857C108.35714285714286,137.9642857142857,172.16666666666669,160.46047619046905,204.07142857142856,160.3892857142857C235.97619047619045,160.31809523810236,267.8809523809524,146.61392857142857,299.7857142857143,137.53714285714284C331.6904761904762,128.46035714285713,363.5952380952381,117.24785714285713,395.5,105.92857142857142C427.40476190476187,94.6092857142857,459.30952380952374,57.5190476189764,491.21428571428567,69.62142857142857C523.1190476190475,81.72380952388073,555.0238095238095,173.55952380953093,586.9285714285714,178.54285714285714C618.8333333333333,183.52619047618333,682.6428571428571,99.52142857142857,682.6428571428571,99.52142857142857" stroke="#4eb7eb" stroke-width="3" fill-opacity="1" fill="none"></path>
                                                                                <path d="M108.35714285714286,293.87142857142857C108.35714285714286,293.87142857142857,172.16666666666669,278.9214285714286,204.07142857142856,276.7857142857143C235.97619047619045,274.65,267.8809523809524,288.17619047618336,299.7857142857143,281.0571428571429C331.6904761904762,273.93809523810233,363.5952380952381,244.03809523810236,395.5,234.07142857142856C427.40476190476187,224.10476190475478,459.30952380952374,246.8857142857143,491.21428571428567,221.25714285714287C523.1190476190475,195.62857142857143,555.0238095238095,83.14761904754783,586.9285714285714,80.29999999999998C618.8333333333333,77.45238095245216,682.6428571428571,204.17142857142858,682.6428571428571,204.17142857142858" stroke="#f1556c" stroke-width="3" fill-opacity="1" fill="none"></path>
                                                                            </g>
                                                                        </g>
                                                                        <g>
                                                                            <circle cx="108.35714285714286" cy="137.9642857142857" r="3" stroke="none" stroke-width="0" fill="#4eb7eb"></circle>
                                                                            <circle cx="204.07142857142856" cy="160.3892857142857" r="3" stroke="none" stroke-width="0" fill="#4eb7eb"></circle>
                                                                            <circle cx="299.7857142857143" cy="137.53714285714284" r="3" stroke="none" stroke-width="0" fill="#4eb7eb"></circle>
                                                                            <circle cx="395.5" cy="105.92857142857142" r="3" stroke="none" stroke-width="0" fill="#4eb7eb"></circle>
                                                                            <circle cx="491.21428571428567" cy="69.62142857142857" r="3" stroke="none" stroke-width="0" fill="#4eb7eb"></circle>
                                                                            <circle cx="586.9285714285714" cy="178.54285714285714" r="3" stroke="none" stroke-width="0" fill="#4eb7eb"></circle>
                                                                            <circle cx="682.6428571428571" cy="99.52142857142857" r="3" stroke="none" stroke-width="0" fill="#4eb7eb"></circle>
                                                                            <circle cx="108.35714285714286" cy="293.87142857142857" r="3" stroke="none" stroke-width="0" fill="#f1556c"></circle>
                                                                            <circle cx="204.07142857142856" cy="276.7857142857143" r="3" stroke="none" stroke-width="0" fill="#f1556c"></circle>
                                                                            <circle cx="299.7857142857143" cy="281.0571428571429" r="3" stroke="none" stroke-width="0" fill="#f1556c"></circle>
                                                                            <circle cx="395.5" cy="234.07142857142856" r="3" stroke="none" stroke-width="0" fill="#f1556c"></circle>
                                                                            <circle cx="491.21428571428567" cy="221.25714285714287" r="3" stroke="none" stroke-width="0" fill="#f1556c"></circle>
                                                                            <circle cx="586.9285714285714" cy="80.29999999999998" r="3" stroke="none" stroke-width="0" fill="#f1556c"></circle>
                                                                            <circle cx="682.6428571428571" cy="204.17142857142858" r="3" stroke="none" stroke-width="0" fill="#f1556c"></circle>
                                                                        </g>
                                                                        <g>
                                                                            <g>
                                                                                <text text-anchor="middle" x="108.35714285714286" y="334.2" font-family="Roboto" font-size="12" stroke="none" stroke-width="0" fill="#222222">2010</text>
                                                                            </g>
                                                                            <g>
                                                                                <text text-anchor="middle" x="204.07142857142856" y="334.2" font-family="Roboto" font-size="12" stroke="none" stroke-width="0" fill="#222222">2011</text>
                                                                            </g>
                                                                            <g>
                                                                                <text text-anchor="middle" x="299.7857142857143" y="334.2" font-family="Roboto" font-size="12" stroke="none" stroke-width="0" fill="#222222">2012</text>
                                                                            </g>
                                                                            <g>
                                                                                <text text-anchor="middle" x="395.5" y="334.2" font-family="Roboto" font-size="12" stroke="none" stroke-width="0" fill="#222222">2013</text>
                                                                            </g>
                                                                            <g>
                                                                                <text text-anchor="middle" x="491.21428571428567" y="334.2" font-family="Roboto" font-size="12" stroke="none" stroke-width="0" fill="#222222">2014</text>
                                                                            </g>
                                                                            <g>
                                                                                <text text-anchor="middle" x="586.9285714285714" y="334.2" font-family="Roboto" font-size="12" stroke="none" stroke-width="0" fill="#222222">2015</text>
                                                                            </g>
                                                                            <g>
                                                                                <text text-anchor="middle" x="682.6428571428571" y="334.2" font-family="Roboto" font-size="12" stroke="none" stroke-width="0" fill="#222222">2016</text>
                                                                            </g>
                                                                            <g>
                                                                                <text text-anchor="end" x="53.66666666666667" y="323.7" font-family="Roboto" font-size="12" stroke="none" stroke-width="0" fill="#444444">0</text>
                                                                            </g>
                                                                            <g>
                                                                                <text text-anchor="end" x="53.66666666666667" y="280.9857" font-family="Roboto" font-size="12" stroke="none" stroke-width="0" fill="#444444">200</text>
                                                                            </g>
                                                                            <g>
                                                                                <text text-anchor="end" x="53.66666666666667" y="238.2714" font-family="Roboto" font-size="12" stroke="none" stroke-width="0" fill="#444444">400</text>
                                                                            </g>
                                                                            <g>
                                                                                <text text-anchor="end" x="53.66666666666667" y="195.5571" font-family="Roboto" font-size="12" stroke="none" stroke-width="0" fill="#444444">600</text>
                                                                            </g>
                                                                            <g>
                                                                                <text text-anchor="end" x="53.66666666666667" y="152.8429" font-family="Roboto" font-size="12" stroke="none" stroke-width="0" fill="#444444">800</text>
                                                                            </g>
                                                                            <g>
                                                                                <text text-anchor="end" x="53.66666666666667" y="110.1286" font-family="Roboto" font-size="12" stroke="none" stroke-width="0" fill="#444444">1,000</text>
                                                                            </g>
                                                                            <g>
                                                                                <text text-anchor="end" x="53.66666666666667" y="67.4143" font-family="Roboto" font-size="12" stroke="none" stroke-width="0" fill="#444444">1,200</text>
                                                                            </g>
                                                                            <g>
                                                                                <text text-anchor="end" x="53.66666666666667" y="24.7" font-family="Roboto" font-size="12" stroke="none" stroke-width="0" fill="#444444">1,400</text>
                                                                            </g>
                                                                        </g>
                                                                    </g>
                                                                    <g>
                                                                        <g>
                                                                            <text text-anchor="middle" x="16.53333333333333" y="170" font-family="Roboto" font-size="12" transform="rotate(-90 16.53333333333333 170)" stroke="none" stroke-width="0" fill="#222222">Sales and Expenses</text>
                                                                            <path d="M6.333333333333323,320L6.333333333333342,20L18.333333333333343,20L18.33333333333332,320Z" stroke="none" stroke-width="0" fill-opacity="0" fill="#ffffff"></path>
                                                                        </g>
                                                                    </g>
                                                                    <g></g>
                                                                </svg>
                                                                <div aria-label="A tabular representation of the data in the chart." style="position: absolute; left: -10000px; top: auto; width: 1px; height: 1px; overflow: hidden;">
                                                                    <table>
                                                                        <thead>
                                                                        <tr>
                                                                            <th>Year</th>
                                                                            <th>Quotation</th>
                                                                            <th>Invoices</th>
                                                                        </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        <tr>
                                                                            <td>2010</td>
                                                                            <td>850</td>
                                                                            <td>120</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>2011</td>
                                                                            <td>745</td>
                                                                            <td>200</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>2012</td>
                                                                            <td>852</td>
                                                                            <td>180</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>2013</td>
                                                                            <td>1,000</td>
                                                                            <td>400</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>2014</td>
                                                                            <td>1,170</td>
                                                                            <td>460</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>2015</td>
                                                                            <td>660</td>
                                                                            <td>1,120</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>2016</td>
                                                                            <td>1,030</td>
                                                                            <td>540</td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                
                                                        <div></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <br>
                                            <div class="card-box">
             
                                                <div class="chart mt-4" id="area-chart">
                                                    <div style="position: relative;">
                                                        <div dir="ltr" style="position: relative; width: 737px; height: 340px;">
                                                            <div aria-label="A chart." style="position: absolute; left: 0px; top: 0px; width: 100%; height: 100%;">
                                                                <svg width="737" height="340" aria-label="A chart." style="overflow: hidden;">
                                                                    <defs id="_ABSTRACT_RENDERER_ID_2">
                                                                        <clipPath id="_ABSTRACT_RENDERER_ID_3">
                                                                            <rect x="59" y="20" width="663" height="300"></rect>
                                                                        </clipPath>
                                                                    </defs>
                                                                    <rect x="0" y="0" width="737" height="340" stroke="none" stroke-width="0" fill="#ffffff"></rect>
                                                                    <g>
                                                                        <rect x="530" y="3" width="192" height="14" stroke="none" stroke-width="0" fill-opacity="0" fill="#ffffff"></rect>
                                                                        <g>
                                                                            <rect x="530" y="3" width="76" height="14" stroke="none" stroke-width="0" fill-opacity="0" fill="#ffffff"></rect>
                                                                            <g>
                                                                                <text text-anchor="start" x="563" y="14.9" font-family="Roboto" font-size="14" stroke="none" stroke-width="0" fill="#222222">Quotation</text>
                                                                            </g>
                                                                            <rect x="530" y="10" width="28" height="7" stroke="none" stroke-width="0" fill-opacity="0.3" fill="#e3eaef"></rect>
                                                                            <path d="M530,10L558,10" stroke="#e3eaef" stroke-width="2" fill-opacity="1" fill="none"></path>
                                                                            <circle cx="544" cy="10" r="3" stroke="none" stroke-width="0" fill="#e3eaef"></circle>
                                                                        </g>
                                                                        <g>
                                                                            <rect x="629" y="3" width="93" height="14" stroke="none" stroke-width="0" fill-opacity="0" fill="#ffffff"></rect>
                                                                            <g>
                                                                                <text text-anchor="start" x="662" y="14.9" font-family="Roboto" font-size="14" stroke="none" stroke-width="0" fill="#222222">Sales</text>
                                                                            </g>
                                                                            <rect x="629" y="10" width="28" height="7" stroke="none" stroke-width="0" fill-opacity="0.3" fill="#02c0ce"></rect>
                                                                            <path d="M629,10L657,10" stroke="#02c0ce" stroke-width="2" fill-opacity="1" fill="none"></path>
                                                                            <circle cx="643" cy="10" r="3" stroke="none" stroke-width="0" fill="#02c0ce"></circle>
                                                                        </g>
                                                                    </g>
                                                                    <g>
                                                                        <rect x="59" y="20" width="663" height="300" stroke="none" stroke-width="0" fill-opacity="0" fill="#ffffff"></rect>
                                                                        <g clip-path="url(file:///F:/xampp/htdocs/projects/highdmin-13/highdmin-13/Highdmin_v1.3/Admin/HTML/vertical/chart-google.html#_ABSTRACT_RENDERER_ID_3)">
                                                                            <g>
                                                                                <rect x="59" y="319" width="663" height="1" stroke="none" stroke-width="0" fill="#f5f5f5"></rect>
                                                                                <rect x="59" y="269" width="663" height="1" stroke="none" stroke-width="0" fill="#f5f5f5"></rect>
                                                                                <rect x="59" y="219" width="663" height="1" stroke="none" stroke-width="0" fill="#f5f5f5"></rect>
                                                                                <rect x="59" y="170" width="663" height="1" stroke="none" stroke-width="0" fill="#f5f5f5"></rect>
                                                                                <rect x="59" y="120" width="663" height="1" stroke="none" stroke-width="0" fill="#f5f5f5"></rect>
                                                                                <rect x="59" y="70" width="663" height="1" stroke="none" stroke-width="0" fill="#f5f5f5"></rect>
                                                                                <rect x="59" y="20" width="663" height="1" stroke="none" stroke-width="0" fill="#f5f5f5"></rect>
                                                                                <rect x="59" y="294" width="663" height="1" stroke="none" stroke-width="0" fill="#fbfbfb"></rect>
                                                                                <rect x="59" y="244" width="663" height="1" stroke="none" stroke-width="0" fill="#fbfbfb"></rect>
                                                                                <rect x="59" y="194" width="663" height="1" stroke="none" stroke-width="0" fill="#fbfbfb"></rect>
                                                                                <rect x="59" y="145" width="663" height="1" stroke="none" stroke-width="0" fill="#fbfbfb"></rect>
                                                                                <rect x="59" y="95" width="663" height="1" stroke="none" stroke-width="0" fill="#fbfbfb"></rect>
                                                                                <rect x="59" y="45" width="663" height="1" stroke="none" stroke-width="0" fill="#fbfbfb"></rect>
                                                                            </g>
                                                                            <g>
                                                                                <g>
                                                                                    <path d="M59.5,319.5L59.5,319.5L59.5,107.70833333333331L169.83333333333331,133.87083333333334L280.16666666666663,107.20999999999998L390.5,70.33333333333331L500.8333333333333,27.974999999999966L611.1666666666666,155.04999999999998L721.5,62.85833333333335L721.5,319.5L721.5,319.5Z" stroke="none" stroke-width="0" fill-opacity="0.3" fill="#e3eaef"></path>
                                                                                </g>
                                                                                <g>
                                                                                    <path d="M59.5,319.5L59.5,319.5L59.5,289.6L169.83333333333331,269.6666666666667L280.16666666666663,274.65L390.5,219.83333333333331L500.8333333333333,204.88333333333333L611.1666666666666,40.43333333333334L721.5,184.95L721.5,319.5L721.5,319.5Z" stroke="none" stroke-width="0" fill-opacity="0.3" fill="#02c0ce"></path>
                                                                                </g>
                                                                            </g>
                                                                            <g>
                                                                                <rect x="59" y="319" width="663" height="1" stroke="none" stroke-width="0" fill="#333333"></rect>
                                                                            </g>
                                                                            <g>
                                                                                <path d="M59.5,107.70833333333331L169.83333333333331,133.87083333333334L280.16666666666663,107.20999999999998L390.5,70.33333333333331L500.8333333333333,27.974999999999966L611.1666666666666,155.04999999999998L721.5,62.85833333333335" stroke="#e3eaef" stroke-width="2" fill-opacity="1" fill="none"></path>
                                                                                <path d="M59.5,289.6L169.83333333333331,269.6666666666667L280.16666666666663,274.65L390.5,219.83333333333331L500.8333333333333,204.88333333333333L611.1666666666666,40.43333333333334L721.5,184.95" stroke="#02c0ce" stroke-width="2" fill-opacity="1" fill="none"></path>
                                                                            </g>
                                                                        </g>
                                                                        <g>
                                                                            <circle cx="59.5" cy="107.70833333333331" r="3" stroke="none" stroke-width="0" fill="#e3eaef"></circle>
                                                                            <circle cx="169.83333333333331" cy="133.87083333333334" r="3" stroke="none" stroke-width="0" fill="#e3eaef"></circle>
                                                                            <circle cx="280.16666666666663" cy="107.20999999999998" r="3" stroke="none" stroke-width="0" fill="#e3eaef"></circle>
                                                                            <circle cx="390.5" cy="70.33333333333331" r="3" stroke="none" stroke-width="0" fill="#e3eaef"></circle>
                                                                            <circle cx="500.8333333333333" cy="27.974999999999966" r="3" stroke="none" stroke-width="0" fill="#e3eaef"></circle>
                                                                            <circle cx="611.1666666666666" cy="155.04999999999998" r="3" stroke="none" stroke-width="0" fill="#e3eaef"></circle>
                                                                            <circle cx="721.5" cy="62.85833333333335" r="3" stroke="none" stroke-width="0" fill="#e3eaef"></circle>
                                                                            <circle cx="59.5" cy="289.6" r="3" stroke="none" stroke-width="0" fill="#02c0ce"></circle>
                                                                            <circle cx="169.83333333333331" cy="269.6666666666667" r="3" stroke="none" stroke-width="0" fill="#02c0ce"></circle>
                                                                            <circle cx="280.16666666666663" cy="274.65" r="3" stroke="none" stroke-width="0" fill="#02c0ce"></circle>
                                                                            <circle cx="390.5" cy="219.83333333333331" r="3" stroke="none" stroke-width="0" fill="#02c0ce"></circle>
                                                                            <circle cx="500.8333333333333" cy="204.88333333333333" r="3" stroke="none" stroke-width="0" fill="#02c0ce"></circle>
                                                                            <circle cx="611.1666666666666" cy="40.43333333333334" r="3" stroke="none" stroke-width="0" fill="#02c0ce"></circle>
                                                                            <circle cx="721.5" cy="184.95" r="3" stroke="none" stroke-width="0" fill="#02c0ce"></circle>
                                                                        </g>
                                                                        <g>
                                                                            <g>
                                                                                <text text-anchor="middle" x="59.5" y="334.2" font-family="Roboto" font-size="12" stroke="none" stroke-width="0" fill="#222222">2010</text>
                                                                            </g>
                                                                            <g>
                                                                                <text text-anchor="middle" x="169.83333333333331" y="334.2" font-family="Roboto" font-size="12" stroke="none" stroke-width="0" fill="#222222">2011</text>
                                                                            </g>
                                                                            <g>
                                                                                <text text-anchor="middle" x="280.16666666666663" y="334.2" font-family="Roboto" font-size="12" stroke="none" stroke-width="0" fill="#222222">2012</text>
                                                                            </g>
                                                                            <g>
                                                                                <text text-anchor="middle" x="390.5" y="334.2" font-family="Roboto" font-size="12" stroke="none" stroke-width="0" fill="#222222">2013</text>
                                                                            </g>
                                                                            <g>
                                                                                <text text-anchor="middle" x="500.8333333333333" y="334.2" font-family="Roboto" font-size="12" stroke="none" stroke-width="0" fill="#222222">2014</text>
                                                                            </g>
                                                                            <g>
                                                                                <text text-anchor="middle" x="611.1666666666666" y="334.2" font-family="Roboto" font-size="12" stroke="none" stroke-width="0" fill="#222222">2015</text>
                                                                            </g>
                                                                            <g>
                                                                                <text text-anchor="middle" x="721.5" y="334.2" font-family="Roboto" font-size="12" stroke="none" stroke-width="0" fill="#222222">2016</text>
                                                                            </g>
                                                                            <g>
                                                                                <text text-anchor="end" x="53.66666666666667" y="323.7" font-family="Roboto" font-size="12" stroke="none" stroke-width="0" fill="#444444">0</text>
                                                                            </g>
                                                                            <g>
                                                                                <text text-anchor="end" x="53.66666666666667" y="273.8667" font-family="Roboto" font-size="12" stroke="none" stroke-width="0" fill="#444444">200</text>
                                                                            </g>
                                                                            <g>
                                                                                <text text-anchor="end" x="53.66666666666667" y="224.0333" font-family="Roboto" font-size="12" stroke="none" stroke-width="0" fill="#444444">400</text>
                                                                            </g>
                                                                            <g>
                                                                                <text text-anchor="end" x="53.66666666666667" y="174.2" font-family="Roboto" font-size="12" stroke="none" stroke-width="0" fill="#444444">600</text>
                                                                            </g>
                                                                            <g>
                                                                                <text text-anchor="end" x="53.66666666666667" y="124.36670000000001" font-family="Roboto" font-size="12" stroke="none" stroke-width="0" fill="#444444">800</text>
                                                                            </g>
                                                                            <g>
                                                                                <text text-anchor="end" x="53.66666666666667" y="74.5333" font-family="Roboto" font-size="12" stroke="none" stroke-width="0" fill="#444444">1,000</text>
                                                                            </g>
                                                                            <g>
                                                                                <text text-anchor="end" x="53.66666666666667" y="24.7" font-family="Roboto" font-size="12" stroke="none" stroke-width="0" fill="#444444">1,200</text>
                                                                            </g>
                                                                        </g>
                                                                    </g>
                                                                    <g>
                                                                        <g>
                                                                            <text text-anchor="middle" x="17.23333333333333" y="170" font-family="Roboto" font-size="14" transform="rotate(-90 17.23333333333333 170)" stroke="none" stroke-width="0" fill="#222222">Sales and Expenses</text>
                                                                            <path d="M5.333333333333323,320L5.333333333333342,20L19.333333333333343,20L19.33333333333332,320Z" stroke="none" stroke-width="0" fill-opacity="0" fill="#ffffff"></path>
                                                                        </g>
                                                                    </g>
                                                                    <g></g>
                                                                </svg>
                                                                <div aria-label="A tabular representation of the data in the chart." style="position: absolute; left: -10000px; top: auto; width: 1px; height: 1px; overflow: hidden;">
                                                                    <table>
                                                                        <thead>
                                                                        <tr>
                                                                            <th>Year</th>
                                                                            <th>Quotation</th>
                                                                            <th>Invoices</th>
                                                                        </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        <tr>
                                                                            <td>2010</td>
                                                                            <td>850</td>
                                                                            <td>120</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>2011</td>
                                                                            <td>745</td>
                                                                            <td>200</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>2012</td>
                                                                            <td>852</td>
                                                                            <td>180</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>2013</td>
                                                                            <td>1,000</td>
                                                                            <td>400</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>2014</td>
                                                                            <td>1,170</td>
                                                                            <td>460</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>2015</td>
                                                                            <td>660</td>
                                                                            <td>1,120</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>2016</td>
                                                                            <td>1,030</td>
                                                                            <td>540</td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
          
                                                        <div></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>









                                    <!--
                                    <div class="row">
                                        <div class="col-xl-6">
                                            <div class="card-box">
                                                <h4 class="header-title">Order Overview</h4>

                                                <div id="website-stats" style="height: 350px;" class="flot-chart mt-5"></div>

                                            </div>
                                        </div>

                                        <div class="col-xl-6">
                                            <div class="card-box">
                                                <h4 class="header-title">Sales Overview</h4>

                                                <div id="combine-chart">
                                                    <div id="combine-chart-container" class="flot-chart mt-5" style="height: 350px;">
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div> -->
                                </div>
                            </div>
                        </div><!-- end col -->
                    </div>
                </div>
            </div>


            <div class="container">
                <div class="main-body p-0">
<!--                    <div class="inner-wrapper">
   

                        &lt;!&ndash; Inner main &ndash;&gt;
                        <div class="inner-main">
                            <h2>Bizneez Social Media</h2>

                            &lt;!&ndash; Inner main body &ndash;&gt;

                            &lt;!&ndash; Forum List &ndash;&gt;
                            <div class="inner-main-body p-2 p-sm-3 collapse forum-content show">

                                <div>
                                    <img src="https://bizneez.net/assets/images/section2.jpg" class="img-responsive" style="height: 400px" />
                                    <h3>This is test title</h3>
                                    <div class="tab-pane fade show active" id="posts" role="tabpanel" aria-labelledby="posts-tab">
                                        <div class="form-group">
                                            <label class="sr-only" for="message">post</label>
                                            <textarea class="form-control" id="message" rows="3" placeholder="What are you thinking?"></textarea>
                                            <input type="button" class="btn btn-primary m-2" value="Submit">
                                        </div>

                                    </div>
                                </div>


                                <div class="card mb-2">
                                    <div class="card-body p-2 p-sm-3">
                                        <div class="media forum-item">
                                            <a href="#" data-toggle="collapse" data-target=".forum-content"><img src="https://bootdey.com/img/Content/avatar/avatar1.png" class="mr-3 rounded-circle" width="50" alt="User" /></a>
                                            <div class="media-body">
                                                <h6><a href="#" data-toggle="collapse" data-target=".forum-content" class="text-body">Realtime fetching data</a></h6>
                                                <p class="text-secondary">
                                                    lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet
                                                </p>
                                                <p class="text-muted"><a href="javascript:void(0)">drewdan</a> replied <span class="text-secondary font-weight-bold">13 minutes ago</span></p>
                                            </div>
                                            <div class="text-muted small text-center align-self-center">
                                                <span class="d-none d-sm-inline-block"><i class="fa fa-bookmark"></i> 19</span>
                                                <span><i class="fa fa-comment ml-2"></i> 3</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card mb-2">
                                    <div class="card-body p-2 p-sm-3">
                                        <div class="media forum-item">
                                            <a href="#" data-toggle="collapse" data-target=".forum-content"><img src="https://bootdey.com/img/Content/avatar/avatar2.png" class="mr-3 rounded-circle" width="50" alt="User" /></a>
                                            <div class="media-body">
                                                <h6><a href="#" data-toggle="collapse" data-target=".forum-content" class="text-body">Laravel 7 database backup</a></h6>
                                                <p class="text-secondary">
                                                    lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet
                                                </p>
                                                <p class="text-muted"><a href="javascript:void(0)">jlrdw</a> replied <span class="text-secondary font-weight-bold">3 hours ago</span></p>
                                            </div>
                                            <div class="text-muted small text-center align-self-center">
                                                <span class="d-none d-sm-inline-block"><i class="fa fa-bookmark"></i> 19</span>
                                                <span><i class="fa fa-comment ml-2"></i> 3</span>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="card mb-2">
                                    <div class="card-body p-2 p-sm-3">
                                        <div class="media forum-item">
                                            <a href="#" data-toggle="collapse" data-target=".forum-content"><img src="https://bootdey.com/img/Content/avatar/avatar3.png" class="mr-3 rounded-circle" width="50" alt="User" /></a>
                                            <div class="media-body">
                                                <h6><a href="#" data-toggle="collapse" data-target=".forum-content" class="text-body">Http client post raw content</a></h6>
                                                <p class="text-secondary">
                                                    lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet
                                                </p>
                                                <p class="text-muted"><a href="javascript:void(0)">ciungulete</a> replied <span class="text-secondary font-weight-bold">7 hours ago</span></p>
                                            </div>
                                            <div class="text-muted small text-center align-self-center">
                                                <span class="d-none d-sm-inline-block"><i class="fa fa-bookmark"></i> 19</span>
                                                <span><i class="fa fa-comment ml-2"></i> 3</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card mb-2">
                                    <div class="card-body p-2 p-sm-3">
                                        <div class="media forum-item">
                                            <a href="#" data-toggle="collapse" data-target=".forum-content"><img src="https://bootdey.com/img/Content/avatar/avatar4.png" class="mr-3 rounded-circle" width="50" alt="User" /></a>
                                            <div class="media-body">
                                                <h6><a href="#" data-toggle="collapse" data-target=".forum-content" class="text-body">Top rated filter not working</a></h6>
                                                <p class="text-secondary">
                                                    lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet
                                                </p>
                                                <p class="text-muted"><a href="javascript:void(0)">bugsysha</a> replied <span class="text-secondary font-weight-bold">11 hours ago</span></p>
                                            </div>
                                            <div class="text-muted small text-center align-self-center">
                                                <span class="d-none d-sm-inline-block"><i class="fa fa-bookmark"></i> 19</span>
                                                <span><i class="fa fa-comment ml-2"></i> 3</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card mb-2">
                                    <div class="card-body p-2 p-sm-3">
                                        <div class="media forum-item">
                                            <a href="#" data-toggle="collapse" data-target=".forum-content"><img src="https://bootdey.com/img/Content/avatar/avatar5.png" class="mr-3 rounded-circle" width="50" alt="User" /></a>
                                            <div class="media-body">
                                                <h6><a href="#" data-toggle="collapse" data-target=".forum-content" class="text-body">Create a delimiter field</a></h6>
                                                <p class="text-secondary">
                                                    lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet
                                                </p>
                                                <p class="text-muted"><a href="javascript:void(0)">jackalds</a> replied <span class="text-secondary font-weight-bold">12 hours ago</span></p>
                                            </div>
                                            <div class="text-muted small text-center align-self-center">
                                                <span class="d-none d-sm-inline-block"><i class="fa fa-bookmark"></i> 19</span>
                                                <span><i class="fa fa-comment ml-2"></i> 3</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card mb-2">
                                    <div class="card-body p-2 p-sm-3">
                                        <div class="media forum-item">
                                            <a href="#" data-toggle="collapse" data-target=".forum-content"><img src="https://bootdey.com/img/Content/avatar/avatar1.png" class="mr-3 rounded-circle" width="50" alt="User" /></a>
                                            <div class="media-body">
                                                <h6><a href="#" data-toggle="collapse" data-target=".forum-content" class="text-body">One model 4 tables</a></h6>
                                                <p class="text-secondary">
                                                    lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet
                                                </p>
                                                <p class="text-muted"><a href="javascript:void(0)">bugsysha</a> replied <span class="text-secondary font-weight-bold">14 hours ago</span></p>
                                            </div>
                                            <div class="text-muted small text-center align-self-center">
                                                <span class="d-none d-sm-inline-block"><i class="fa fa-bookmark"></i> 19</span>
                                                <span><i class="fa fa-comment ml-2"></i> 3</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card mb-2">
                                    <div class="card-body p-2 p-sm-3">
                                        <div class="media forum-item">
                                            <a href="#" data-toggle="collapse" data-target=".forum-content"><img src="https://bootdey.com/img/Content/avatar/avatar1.png" class="mr-3 rounded-circle" width="50" alt="User" /></a>
                                            <div class="media-body">
                                                <h6><a href="#" data-toggle="collapse" data-target=".forum-content" class="text-body">Auth attempt returns false</a></h6>
                                                <p class="text-secondary">
                                                    lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet
                                                </p>
                                                <p class="text-muted"><a href="javascript:void(0)">michaeloravec</a> replied <span class="text-secondary font-weight-bold">18 hours ago</span></p>
                                            </div>
                                            <div class="text-muted small text-center align-self-center">
                                                <span class="d-none d-sm-inline-block"><i class="fa fa-bookmark"></i> 19</span>
                                                <span><i class="fa fa-comment ml-2"></i> 3</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
             
                            </div>
                            &lt;!&ndash; /Forum List &ndash;&gt;

                            &lt;!&ndash; Forum Detail &ndash;&gt;
                            <div class="inner-main-body p-2 p-sm-3 collapse forum-content">
                                <a href="#" class="btn btn-light btn-sm mb-3 has-icon" data-toggle="collapse" data-target=".forum-content"><i class="fa fa-arrow-left mr-2"></i>Back</a>
                                <div class="card mb-2">
                                    <div class="card-body">
                                        <div class="media forum-item">
                                            <a href="javascript:void(0)" class="card-link">
                                                <img src="https://bootdey.com/img/Content/avatar/avatar1.png" class="rounded-circle" width="50" alt="User" />
                                                <small class="d-block text-center text-muted">Newbie</small>
                                            </a>
                                            <div class="media-body ml-3">
                                                <a href="javascript:void(0)" class="text-secondary">Mokrani</a>
                                                <small class="text-muted ml-2">1 hour ago</small>
                                                <h5 class="mt-1">Realtime fetching data</h5>
                                                <div class="mt-3 font-size-sm">
                                                    <p>Hellooo :)</p>
                                                    <p>
                                                        I'm newbie with laravel and i want to fetch data from database in realtime for my dashboard anaytics and i found a solution with ajax but it dosen't work if any one have a simple solution it will be
                                                        helpful
                                                    </p>
                                                    <p>Thank</p>
                                                </div>
                                            </div>
                                            <div class="text-muted small text-center">
                                                <span class="d-none d-sm-inline-block"><i class="far fa-eye"></i> 19</span>
                                                <span><i class="far fa-comment ml-2"></i> 3</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card mb-2">
                                    <div class="card-body">
                                        <div class="media forum-item">
                                            <a href="javascript:void(0)" class="card-link">
                                                <img src="https://bootdey.com/img/Content/avatar/avatar2.png" class="rounded-circle" width="50" alt="User" />
                                                <small class="d-block text-center text-muted">Pro</small>
                                            </a>
                                            <div class="media-body ml-3">
                                                <a href="javascript:void(0)" class="text-secondary">drewdan</a>
                                                <small class="text-muted ml-2">1 hour ago</small>
                                                <div class="mt-3 font-size-sm">
                                                    <p>What exactly doesn't work with your ajax calls?</p>
                                                    <p>Also, WebSockets are a great solution for realtime data on a dashboard. Laravel offers this out of the box using broadcasting</p>
                                                </div>
                                                <button class="btn btn-xs text-muted has-icon"><i class="fa fa-heart" aria-hidden="true"></i>1</button>
                                                <a href="javascript:void(0)" class="text-muted small">Reply</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            &lt;!&ndash; /Forum Detail &ndash;&gt;

                            &lt;!&ndash; /Inner main body &ndash;&gt;
                        </div>
                        &lt;!&ndash; /Inner main &ndash;&gt;
                    </div>-->

                    <!-- New Thread Modal -->
                    <div class="modal fade" id="threadModal" tabindex="-1" role="dialog" aria-labelledby="threadModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <form>
                                    <div class="modal-header d-flex align-items-center bg-primary text-white">
                                        <h6 class="modal-title mb-0" id="threadModalLabel">New Discussion</h6>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label for="threadTitle">Title</label>
                                            <input type="text" class="form-control" id="threadTitle" placeholder="Enter title" autofocus="" />
                                        </div>
                                        <textarea class="form-control summernote" style="display: none;"></textarea>

                                        <div class="custom-file form-control-sm mt-3" style="max-width: 300px;">
                                            <input type="file" class="custom-file-input" id="customFile" multiple="" />
                                            <label class="custom-file-label" for="customFile">Attachment</label>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
                                        <button type="button" class="btn btn-primary">Post</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- container -->
    </div> <!-- content -->


@endsection

<style>
    .footer{display: none;}
    .logo{margin-top:20px;}
</style>