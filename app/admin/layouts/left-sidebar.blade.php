<!-- ========== Left Sidebar Start ========== -->
<div class="left side-menu">

    <div class="slimscroll-menu" id="remove-scroll">

        <!-- LOGO -->
        <div class="topbar-left">
            <br>
            <a href="{{ url('admin/dashboard')}}" class="logo">
                <span>
                    <img src="{{URL::asset('theme/admin/assets/images/logo.jpeg')}}" alt="" height="22">
                </span>
                <i>
                    <img src="{{URL::asset('theme/admin/assets/images/logo_sm.png')}}" alt="" height="28">
                </i>


            </a>
        </div>

        <!-- User box -->
        <div class="user-box">
            <div class="user-img">
                <img src="{{URL::asset('theme/admin/assets/images/users/avatar-1.jpg')}}" alt="user-img" title="Mat Helme" class="rounded-circle img-fluid">
            </div>
            <h5><a href="#">Admin</a> </h5>
            <p class="text-muted">Admin Head</p>
        </div>

        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <ul class="metismenu metismenu without-tooltip mt-5" id="side-menu">
                <!--<li class="menu-title">Navigation</li>-->
                <li>
                    <a href="{{ url('admin/dashboard')}}">
                        <i class="fa fa-dashboard"></i><span> Dashboard </span>
                    </a>
                </li>
                                                 
                <li>
                    <a href="{{ url('admin/option')}}">
                        <i class="fa fa-tasks"></i> <span> Package Option </span>
                    </a>
                </li>
                <li>
                    <a href="{{ url('admin/packages')}}">
                        <i class="fi-layers"></i> <span> Packages </span>
                    </a>
                </li>


                <li>
                    <a href="{{ url('admin/payment_history')}}">
                        <i class="mdi mdi-currency-usd"    >
                                  
                        </i> <span> Payment History </span>
                    </a>
                </li>
                          
  
                   
            </ul>








            <ul class="metismenu with-tooltip mt-5" id="side-menu">
                <!--<li class="menu-title">Navigation</li>-->
                <li data-toggle="tooltip" data-placement="right" title="" data-original-title="Dashboard">
                    <a href="{{ url('admin/dashboard')}}">
                        <i class="fa fa-dashboard"></i>
                    </a>
                </li >
                <li data-toggle="tooltip" data-placement="right" title="" data-original-title="Package Option">
                    <a href="{{ url('admin/option')}}">
                        <i class="fa fa-tasks"></i> <span> Package Option </span>
                    </a>
                </li>
                <li data-toggle="tooltip" data-placement="right" title="" data-original-title="Packages">
                    <a href="{{ url('admin/packages')}}">
                        <i class="fi-layers"></i> <span> Packages </span>
                    </a>
                </li>


                <li data-toggle="tooltip" data-placement="right" title="" data-original-title="Payment History">
                    <a href="{{ url('admin/payment_history')}}">
                        <i class="mdi mdi-currency-usd"    >
                                  
                        </i> <span> Payment History </span>
                    </a>
                </li>
            </ul>
        </div>
        <!-- Sidebar -->

        <div class="clearfix"></div>

    </div>
    <!-- Sidebar -left -->

</div>
<!-- Left Sidebar End -->


<!--       <style>
     .mdi-currency-usd{
          font-size: 70px;
        }
      </style> -->