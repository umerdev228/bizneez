@extends('admin.layouts.app')
<style>
    .dropzone {
        background: white;
        border-radius: 5px;
        border: 2px dashed rgb(0, 135, 247);
        border-image: none;
        width: 100%;
        /*max-width: 500px;*/
        margin-left: auto;
        margin-right: auto;
    }

    .bootstrap-filestyle {
        display: none;
    }
    .label-info {
        background-color: #5bc0de;
        display: inline-block;
        padding: 0.2em 0.6em 0.3em;
        font-size: 75%;
        font-weight: 700;
        line-height: 1;
        color: #fff;
        text-align: center;
        white-space: nowrap;
        vertical-align: baseline;
        border-radius: 0.25em;
    }

</style>
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    @if(session()->has('message'))
                        <div class="alert alert-success mt-5" id="myElem" >
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-12 mt-5">
                            <div class="card">
                                <div class="card-body">
                                    <a href="{{route('admin.categories.create')}}"  class="btn btn-primary" ><i class="fa fa-plus"></i>Add New Template </a>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="color"> Colors <span class="text-danger"></span></label>
                                        <div id="dropzone">
                                            <div class="dropzone needsclick" id="colors-upload">
                                                <div class="dz-message needsclick">
                                                    Upload Your Product Color Images
                                                </div>
                                            </div>
                                        </div>
                                        <br/>
                                        <div id="preview-color-image" style="display: none;">
                                            <div class="dz-preview dz-file-preview">
                                                <div class="dz-image">
                                                    <img data-dz-thumbnail=""/>
                                                </div>
                                                <div class="dz-details">
                                                    <div class="dz-size"><span data-dz-size=""></span></div>
                                                    <div class="dz-filename"><span data-dz-name=""></span></div></div>
                                                <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress=""></span></div>
                                                <div class="dz-error-message"><span data-dz-errormessage=""></span></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- end col -->
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <style href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/dropzone.css"></style>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/dropzone.js"></script>

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

    <script>
        var dropzone = new Dropzone(document.getElementById('colors-upload'), {
            url: "{{route('admin.template.store')}}", // Set the url
            previewTemplate: document.querySelector('#preview-color-image').innerHTML,
            parallelUploads: 2,
            thumbnailHeight: 120,
            thumbnailWidth: 120,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            filesizeBase: 1000,
            thumbnail: function(file, dataUrl) {
                if (file.previewElement) {
                    file.previewElement.classList.remove("dz-file-preview");
                    var images = file.previewElement.querySelectorAll("[data-dz-thumbnail]");
                    for (var i = 0; i < images.length; i++) {
                        var thumbnailElement = images[i];
                        thumbnailElement.alt = file.name;
                        thumbnailElement.src = dataUrl;
                    }
                    setTimeout(function() { file.previewElement.classList.add("dz-image-preview"); }, 1);
                }
            }
        });
    </script>

@endsection
