@extends('admin.layouts.app')
@section('content')
    @include('admin.layouts.topbar')
    <!-- Google Chart JS -->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <!-- Start Page content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if(session()->has('message'))
                        <div class="alert alert-success" style="width:803px ">
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-9 mt-5">
                            <div class="card-box">
                                <form method="POST" action="{{ route('admin.clients.update', ['id' => $client->id]) }}" enctype="multipart/form-data">
                                    @csrf
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="first_name">Company Name<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control valid" name="company_keywords" value="{{$client->profile->company_keywords}}" id="company_keywords" required />
                                                @if ($errors->has('company_keywords'))
                                                    <span class="error">{{ $errors->first('company_keywords') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="first_name">First Name<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control valid" name="first_name" value="{{$client->first_name}}" id="first_name" required />
                                                @if ($errors->has('first_name'))
                                                    <span class="error">{{ $errors->first('first_name') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="middle_name">Middle Name<span class="text-danger"></span></label>
                                                <input type="text" class="form-control valid" name="middle_name" value="{{$client->middle_name}}" id="middle_name"/>
                                                @if ($errors->has('middle_name'))
                                                    <span class="error">{{ $errors->first('middle_name') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="last_name">Last Name<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control valid" name="last_name" value="{{$client->last_name}}" id="last_name" required />
                                                @if ($errors->has('last_name'))
                                                    <span class="error">{{ $errors->first('first_name') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="email">Email<span class="text-danger">*</span></label>
                                                <input type="email" class="form-control valid" name="email" value="{{$client->email}}" id="email" required />
                                                @if ($errors->has('email'))
                                                    <span class="error">{{ $errors->first('email') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="phone">Phone<span class="text-danger">*</span></label>
                                                <input type="tel" class="form-control valid" name="phone" value="{{$client->profile->phone}}" id="phone" required />
                                                @if ($errors->has('phone'))
                                                    <span class="error">{{ $errors->first('phone') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                    <!--             <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="mobile">Mobile<span class="text-danger">*</span></label>
                                                <input type="number" class="form-control valid" name="mobile" id="mobile" required />
                                                @if ($errors->has('mobile'))
                                        <span class="error">{{ $errors->first('mobile') }}</span>
                                                @endif
                                            </div>
                                        </div> -->

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="address">Address<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control valid" name="address" value="{{$client->profile->address}}" id="address" required />
                                                @if ($errors->has('address'))
                                                    <span class="error">{{ $errors->first('address') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="location_id">Location<span class="text-danger">*</span></label>
                                                <input placeholder="Please Enter Location" type="text" class="form-control valid" name="location_id" value="{{$client->profile->location_id}}" id="location_id"  />
                                                @if ($errors->has('location_id'))
                                                    <span class="error">{{ $errors->first('location_id') }}</span>

                                                @endif
                                            </div>
                                        </div>


                                        <div id="representative">
                                            @foreach($client->representatives as $index => $representative)
                                                <div id="{{$index}}" class="row">

                                                    <div class="col-sm-3">
                                                        <div class="form-group">
                                                            <label for="location_id">Represent Name </label>
                                                            <input placeholder="Please Represent Name" type="text" class="form-control valid" name="represent_name[]" value="{{$representative->name}}" id="represent_name"  />
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="form-group">
                                                            <label for="area_id">Represent Email </label>
                                                            <input placeholder="Please enter represent email" type="text" class="form-control valid" name="represent_email[]" value="{{$representative->email}}" id="represent_email"  />
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="form-group">
                                                            <label for="area_id">Represent Mobile </label>
                                                            <input placeholder="Please enter represent mobile" type="text" class="form-control valid" name="represent_mobile[]" value="{{$representative->mobile}}" id="represent_mobile"  />
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="form-group">
                                                            @if($index < 1)
                                                                <button type="button" onclick="appendFields()" style="border-radius: 10px" class="btn btn-success valid align-baseline"><i class="fa fa-plus"></i> </button>
                                                            @endif
                                                            @if($index > 0)
                                                                <button type="button" onclick="removeField({{$index}})" style="border-radius: 10px" class="btn btn-danger valid align-baseline"><i class="fa fa-minus"></i> </button>
                                                            @endif

                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>










                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="image">Company Logo </label>
                                                <input type="file" class="form-control valid" name="image" id="image" >
                                                @if ($errors->has('image'))
                                                    <span class="error">{{ $errors->first('image') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="Description">Description<span class="text-danger">*</span></label>
                                                <textarea  placeholder="Exp: We are Client of office furniture" id="Description" class="form-control rounded-0" name="description" rows="5">{{$client->profile->description}}</textarea>
                                                @if ($errors->has('description'))
                                                    <span class="error">{{ $errors->first('description') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group mt-3">

                                        <input class="btn btn-primary" type="submit" value="Update">
                                        <button onclick="history.back()" type="button" class="btn">Cancel</button>
                                    </div>
                                </form>
                            </div> <!-- end card-box -->
                        </div>

                    </div> <!-- container -->
                </div> <!-- content -->
            </div>
        </div>
    </div>

    <script>
        var i = '{!! count($client->representatives) !!}'
        function appendFields() {
            i = i+1
            console.log('clicked')
            var html_input =
                `
<div id="`+i+`" class="row">
 <div class="col-sm-3">
 <div class="form-group">
 <label for="location_id">Represent Name </label>
<input placeholder="Please Represent Name" type="text" class="form-control valid" name="represent_name[]" value="" id="represent_name"  />
</div>
</div>
<div class="col-sm-3">
<div class="form-group">
<label for="area_id">Represent Email </label>
<input placeholder="Please enter represent email" type="text" class="form-control valid" name="represent_email[]" value="" id="represent_email"  />
</div>
</div>
<div class="col-sm-3">
<div class="form-group">
<label for="area_id">Represent Mobile </label>
<input placeholder="Please enter represent mobile" type="text" class="form-control valid" name="represent_mobile[]" value="" id="represent_mobile"  />
</div>
</div>
<div class="col-sm-3">
<div class="form-group">
<button onclick="appendFields()" type="button" style="border-radius: 10px" class="btn btn-success valid align-baseline"><i class="fa fa-plus"></i> </button>
<button onclick="removeField(`+i+`)" type="button" style="border-radius: 10px" class="btn btn-danger valid align-baseline"><i class="fa fa-minus"></i> </button>
</div>
</div>
</div>`

            $('#representative').append(html_input)

        }

        function removeField(id) {
            console.log('removeField', id)
            $('#'+id).remove();

        }
    </script>
@endsection


