<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Cookie;
use App\User;
use App\Merchant;
use App\PaymentHistory;
use App\Packages;

class ApiController extends Controller
{
/**
* Create a new controller instance.
*
* @return void
*/
public function __construct()
{
//$this->middleware('auth');
}
/**
* Display a listing of the resource.
*
* @return \Illuminate\Http\Response
*/




public function getPackagesJson(){
      $packages = Packages::where('status', 1)->get();
     // $packages = Packages::all();
     $data = ['packages' => $packages,];
     return response()->json($packages);
  }



 public function get_payment_history(Request $request){  
   $user_info = User::where('email', $request->email )->first();
   if($user_info){  
        $payment_history = PaymentHistory::where('user_id', $user_info->id)->get();
             if( $payment_history)
             {
                 //return json_encode($payment_history);
                 return response()->json($payment_history);
            }
            else
            {
              return false;
            }
}else{
    return false;
}

}





    
    
}