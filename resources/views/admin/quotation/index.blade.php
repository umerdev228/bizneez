@extends('admin.layouts.app')
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    @if(session()->has('message'))
                        <div class="alert alert-success mt-5" id="myElem" >
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-12 mt-5">
                            <div class="card">
                                <div class="card-body">
                                    <table class="table table-hover m-0 tickets-list table-actions-bar dt-responsive nowrap" width="100%" id="normal-table">
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Date</th>
                                            <th data-orderable="false">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($quotations as $quotation)
                                            <tr>
                                                <td>{{$quotation->name}}</td>
                                                <td>{{$quotation->date}}</td>
                                                <td>
                                                    <div class="btn-group">
                                                        <a class="btn" href="{{ route('admin.quotations.delete', ['id' => $quotation->id] ) }}">
                                                            <i class="fa fa-close"></i>
                                                        </a>
                                                        <a class="btn" href="{{ asset($quotation->path) }}" download="{{$quotation->name}}">
                                                            <i class="mdi mdi-download"></i>
                                                        </a>
                                                    </div>

                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div> <!-- end card -->
                            </div> <!-- end card -->
                        </div><!-- end col -->
                    </div>

                </div>
            </div>

        </div> <!-- container -->
    </div> <!-- content -->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

@endsection
