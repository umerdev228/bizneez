@extends('admin.layouts.app')
@section('content')
    @include('admin.layouts.topbar')
    <!-- Google Chart JS -->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <!-- Start Page content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    @if(session()->has('message'))
                        <div class="alert alert-success">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            {{ session()->get('message') }}
                        </div>
                    @endif
                        @if(session()->has('error'))
                            <div class="alert alert-danger" >
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                {{ session()->get('error') }}
                            </div>
                    @endif
                        <div class="row mt-5">
                            <div class="col-9">
                                <div class="card">
                                    <div class="card-header">
                                        Profile Update
                                    </div>
                                    <div class="card-body">
                                        <form action="{{route('profile.update', ['role' => Auth::user()->roles[0]->name])}}" method="post" enctype="multipart/form-data">
                                            @csrf
                                            <div class="form-row">
                                                <div class="form-group col-md-12">
                                                    <label for="first_name">First Name<span class="text-danger">*</span></label>
                                                    <input type="text" name="first_name" value="{{$user->first_name}}" autocomplete="off"  class="form-control" required>
                                                    @if ($errors->has('first_name'))
                                                        <span class="error">{{ $errors->first('first_name') }}</span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-row">
                                                <div class="form-group col-md-12">
                                                    <label for="middle_name">Middle Name<span class="text-danger">*</span></label>
                                                    <input type="text" name="middle_name" value="{{$user->middle_name}}" autocomplete="off"  class="form-control">
                                                    @if ($errors->has('middle_name'))
                                                        <span class="error">{{ $errors->first('middle_name') }}</span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-row">
                                                <div class="form-group col-md-12">
                                                    <label for="last_name">Last Name<span class="text-danger">*</span></label>
                                                    <input type="text" name="last_name" value="{{$user->last_name}}" autocomplete="off" class="form-control" required>
                                                    @if ($errors->has('last_name'))
                                                        <span class="error">{{ $errors->first('last_name') }}</span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-row">
                                                <div class="form-group col-md-12">
                                                    <label for="email">Email<span class="text-danger">*</span></label>
                                                    <input type="email" name="email" value="{{$user->email}}" autocomplete="off" class="form-control" required>
                                                    @if ($errors->has('email'))
                                                        <span class="error">{{ $errors->first('email') }}</span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-row">
                                                <div class="form-group col-md-12">
                                                    <label for="description">Description<span class="text-danger">*</span></label>
                                                    <textarea name="description" autocomplete="off" class="form-control">{{$user->profile->description}}</textarea>
                                                    @if ($errors->has('description'))
                                                        <span class="error">{{ $errors->first('description') }}</span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-row">
                                                <div class="form-group col-md-12">
                                                    <label for="description">Phone<span class="text-danger">*</span></label>
                                                    <input type="text" name="phone" value="{{$user->profile->phone}}" autocomplete="off" class="form-control">
                                                    @if ($errors->has('phone'))
                                                        <span class="error">{{ $errors->first('phone') }}</span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-row">
                                                <div class="form-group col-md-12">
                                                    <label for="description">Mobile<span class="text-danger">*</span></label>
                                                    <input type="text" name="mobile" value="{{$user->profile->mobile}}" autocomplete="off" class="form-control">
                                                    @if ($errors->has('mobile'))
                                                        <span class="error">{{ $errors->first('mobile') }}</span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-row">
                                                <div class="form-group col-md-12">
                                                    <label for="description">Assistance Number<span class="text-danger">*</span></label>
                                                    <input type="text" name="assistant_number" value="{{$user->profile->assistant_number}}" class="form-control">
                                                    @if ($errors->has('assitant_number'))
                                                        <span class="error">{{ $errors->first('assitant_number') }}</span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-row">
                                                <div class="form-group col-md-12">
                                                    <label for="old_password">Old Password<span class="text-danger"></span></label>
                                                    <input type="password" name="old_password" value="{{$user->old_password}}" class="form-control">
                                                    @if ($errors->has('old_password'))
                                                        <span class="error">{{ $errors->first('old_password') }}</span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-row">
                                                <div class="form-group col-md-12">
                                                    <label for="new_password">New Password<span class="text-danger"></span></label>
                                                    <input type="password" name="new_password" value="{{$user->new_password}}" class="form-control">
                                                    @if ($errors->has('new_password'))
                                                        <span class="error">{{ $errors->first('new_password') }}</span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-row">
                                                <div class="form-group col-md-12">
                                                    <label for="image">Profile Photo<span class="text-danger"></span></label>
                                                    <input type="file" name="image" value="{{$user->profile->image}}" class="form-control">
                                                    @if ($errors->has('image'))
                                                        <span class="error">{{ $errors->first('image') }}</span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-row">
                                                <div class="form-group col-md-12">
                                                    <label for="address">Address<span class="text-danger">*</span></label>
                                                    <textarea name="address" class="form-control">{{$user->profile->address}}</textarea>
                                                    @if ($errors->has('address'))
                                                        <span class="error">{{ $errors->first('address') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                            <button type="submit" class="btn btn-primary">Update</button>
                                            <button onclick="history.back()" type="button" class="btn">Cancel</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- container -->
                </div>
            </div>
        </div>
    </div>
    @include('admin.layouts.footer')
@endsection