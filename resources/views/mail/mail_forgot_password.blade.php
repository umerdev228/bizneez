<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title> Bizneez Office Furniture Supplier | Office Furniture London </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="margin: 0; padding: 0; background: #cccccc; font-family: arial;">
    <table border="0" cellpadding="0" cellspacing="0" width="100%"> 
        <tr>
            <td style="padding: 10px 0 30px 0;">
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border: 1px solid #cccccc; border-collapse: collapse;">
                    <tr>
                        <td align="center" bgcolor="#efefef" style="padding: 15px 0 15px 0; color: #153643; font-size: 12px;">Does this email not look right? Try<a href='{{ $link }}' style="text-decoration: underline; color:#0b5578 ">View in browser</a></td>
                    </tr>
                    <tr>
                        <td align="center" bgcolor="#fff" style="padding: 30px 0 20px 0; color: #153643; font-size: 28px; font-weight: bold;  ">
                            <img src="{{ $message->embed('theme/admin/assets/images/logo.jpeg')}}">
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#fff" style="padding: 0px 25px 0px 25px;">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td style="padding: 0px 0 10px 0; font-weight: bold; color:#153643;">
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding: 0px 0 10px 0; color: #153643;   font-size: 14px; line-height: 20px;">
                                        We have recevied a request of reset password.
                                        Your can reset your password from the following link.
                                        <br>
                                       If you did not request to reset your password ,
                                       please ignore this message. 
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                <tr style="  box-sizing: border-box; font-size: 14px; margin: 0;">
                                <td class="content-block" itemprop="handler" itemscope itemtype="http://schema.org/HttpActionHandler"
                                style="box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 10px 30px;"
                                valign="top">
                                </td>
                                </tr>
                                <tr>
                                    <a href="{{ $link }}" style="color: #3297cf; background: #0b5578; color:#fff; text-decoration: none; padding: 10px 20px">Reset Password</a>
                                </tr>
                                <tr></tr>
                                <tr>
                                    <td style="padding: 50px 0 50px 0; color: #153643;   font-size: 14px; line-height: 20px;">
                                        Regards,<br/>
                                        Bizneez

                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#0b5578" style="padding: 30px 30px 30px 30px;">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td style="color: #ffffff;   font-size: 12px; margin-left: 17px; " width="50%">
                                        Copyright © 2016 bizneez.co.uk All rights reserved.
                                        <br/>
                                    </td>
                                    <td align="right" width="25%">
                                        <table border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="font-family: Arial, sans-serif; font-size: 12px;  ">
                                                <a href="https://twitter.com/"><i class="fab fa-twitter-square"></i></a>
                                                   
                                                </td>
                                                <td style="font-size: 0; line-height: 0;" width="20">&nbsp;</td>
                                                <td style="  font-size: 10px;  ">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>

