@extends('admin.layouts.app')
@section('content')
    @include('admin.layouts.topbar')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    @if(session()->has('message'))
                        <div class="alert alert-success mt-5" id="myElem" >
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            {{ session()->get('message') }}
                        </div>
                    @endif
                   
                    <div class="row">
                        <div class="col-12 mt-5">
                            <div class="card">

                                <div class="card-header">
                                    <h3>AR Gallery</h3>
                                </div>
                                <div class="card-body">
                                    <div class="row m-2">
                                        @foreach($media as $image)
                                        <div class="col-lg-3 p-2">
                                            <img src="{{asset('/storage/'.$image->path)}}" alt="" srcset="" width="150px">
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div><!-- end col -->
                    </div>

                </div>
            </div>
        </div> <!-- container -->
    </div> <!-- content -->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
@endsection
