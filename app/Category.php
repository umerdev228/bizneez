<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //

    protected $guarded = [];

    public function products() {
        return $this->belongsToMany(Product::class, 'product_categories', 'product_id', 'category_id')->with('media');
    }

    public function childs() {
        return $this->hasMany(Category::class, 'parent', 'id');
    }
}
