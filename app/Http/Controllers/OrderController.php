<?php

namespace App\Http\Controllers;

use App\Category;
use App\Company;
use App\Contact;
use App\Exports\InvoicesExport;
use App\Exports\OrderExport;
use App\Location;
use App\ManufacturerOrder;
use App\Media;
use App\Order;
use App\QuotationContact;
use App\QuotationProduct;
use App\SiteSetting;
use Cart;
use App\OrderProduct;
use App\Product;
use App\Quotation;
use App\User;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use PhpParser\Node\Expr\Array_;
use \DB;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $locations = Location::all();
        if (Auth::user()->hasRole(['admin'])) {
            $orders = Order::where('order_type', 'default')->with('user')->with('products')->orderBy('id', 'desc')->get();
        }
        elseif (Auth::user()->hasRole(['manufacturer'])) {
            $orders = ManufacturerOrder::where('manufacturer_id', Auth::id())->with('architect')->with('order')->orderBy('id', 'desc')->get();
        }
        else {
            $orders = Order::where('order_type', 'default')->where('user_id', Auth::id())->with('user')->with('products')->orderBy('id', 'desc')->get();
        }
        if (Auth::user()->hasRole(['manufacturer'])) {
            $breadcrumb = ucfirst(Auth::user()->roles[0]->name)." / Purchase / Orders";
            return view('manufacturer.order.index', compact('orders', 'locations', 'breadcrumb'));
        }
        else {
            $breadcrumb = ucfirst(Auth::user()->roles[0]->name)." / Orders / Quotation";
            $custom_orders = Order::where('order_type', 'custom')->get();
            $companies = Company::all();


            return view('admin.order.index', compact('orders', 'locations', 'breadcrumb', 'custom_orders','companies'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $id = Order::latest()->first()->id ?? 0;
        $number = sprintf('%06d', json_encode($id+1));

        $insertion = "-";
        $index = 3;
        $serial = substr_replace($number, $insertion, $index, 0);

        $productsmdd  = DB::table('mdd_price_list_csv')->get();

        $manufacturers = User::role('manufacturer')->with('ranges')->get();
        $breadcrumb = ucfirst(Auth::user()->roles[0]->name)." / Quotation / Create";
        $companies = Company::all();
        $products = Product::with('media')->with('categories')->with('manufacturer')->orderBy('created_at', 'desc')->get();
        $categories = Category::where('parent', 0)->with('childs')->get();
        $media = Media::where('item_type', 'product-range')->get();
        
        $ranges  = DB::table('mdd_price_list_csv')->get();
        //$ranges  = DB::table('mdd_price_list_csv')->select(['COL5','COL6','COL7','COL8'])->get();

        $c = Company::where('by_default', 1)->first();
        $termconditions = $c->term_and_condition;

        return view('admin.order.create', compact('products', 'productsmdd', 'breadcrumb', 'media', 'categories', 'companies', 'manufacturers', 'serial', 'ranges', 'termconditions'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $products = Cart::getContent();
        $order = Order::create([
            'user_id' => Auth::id(),
            'status' => 'pending',
        ]);
        foreach ($products as $index => $product) {
            $product_ = Product::with('user')->with('media')->where('id', $product->id)->first();
            OrderProduct::create([
                'product_id' => $product->id,
                'size_id' => $product->attributes->size,
                'order_id' => $order->id,
                'quantity' => $product->quantity,
                'price' => $product_->price,
            ]);
        }
        Cart::clear();
        toastr()->success('Order Created Successfully');
        return redirect()->route('admin.orders.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $breadcrumb = ucfirst(Auth::user()->roles[0]->name)." / Order / Product / List";
        $order = Order::where('id', $id)->with('products')->first();
        $total = 0;
        $products = $order->products;
        $order_id = $order->id;
        foreach ($products as $product) {
            $total += (integer)$product->price * $product->pivot->quantity;
        }

        return view('admin.order.show', compact('order', 'products', 'order_id', 'breadcrumb', 'total'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $order = Order::where('id', $id)->with('quotation')->first();
        $quotation = Quotation::where('id', $order->quotation_id)->first();
        $products  = QuotationProduct::where('quotation_id', $order->quotation_id)->get();
        $company = Company::where('id', $quotation->company_id)->first();
        $contact = QuotationContact::where('id', $quotation->id)->first();
        $manufacturers = User::role('manufacturer')->with('ranges')->get();
        $productsmdd  = DB::table('mdd_price_list_csv')->get();

        $companies = Company::all();


        return view('admin.order.edit', compact('order', 'quotation', 'products', 'company', 'companies', 'contact', 'manufacturers', 'productsmdd'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Order::where('id', $id)->delete();
        toastr()->success('Quotation Deleted Successfully');
        return redirect()->back();
    }


    public function approve($id)
    {
        //
        $manufacturers = Array();
        $products = Array();
        $order = Order::where('id', $id)->with('products')->with('user')->first();
        foreach ($order->products as $product) {
            if (!in_array($product->user_id, $manufacturers)) {
                array_push($manufacturers, $product->user_id);
            }
        }
        foreach ($manufacturers as $manufacturer) {
            $man = User::where('id', $manufacturer)->first();
            $details = [
                'title' => 'New Order Received',
                'body' => 'You have new order please check orders.'
            ];
            \Mail::to($man->email)->send(new \App\Mail\OrderMailManufacturer($details));
            array_push($products, $order->products->where('user_id', $manufacturer));
            $orders = $order->products->where('user_id', $manufacturer);
            $file_name = 'Quo-'.date('YmdHis').'.pdf';

            view()->share(['orders' => $orders, 'order' => $order, 'name' => $file_name]);
            PDF::setOptions(['dpi' => 100, 'defaultFont' => 'sans-serif']);
            $pdf = PDF::loadView('admin.order.pdfQuotation')->setPaper('a4', 'landscape')->setWarnings(false);
            $pdf->save(public_path('/uploads/orders/quotations/').$file_name);
            Quotation::create([
                'name' => $file_name,
                'manufacturer_id' => $manufacturer,
                'order_id' => $id,
                'date' => date('Y-m-d H:i:s'),
                'path' => '/uploads/orders/quotations/'.$file_name
            ]);
            ManufacturerOrder::create([
                'manufacturer_id' => $manufacturer,
                'order_id' => $id,
                'architect_id' => $order->user_id,
            ]);
        }

        $man = User::where('id', $order->user_id)->first();
        $details = [
            'title' => 'Your Order Approved',
            'body' => 'Check Updates Please'
        ];
        \Mail::to($man->email)->send(new \App\Mail\OrderMailManufacturer($details));
        $orders = $order->products;
        view()->share('orders',$orders);
        $file_name = 'Quo-'.date('YmdHis').'.pdf';
        PDF::setOptions(['dpi' => 100, 'defaultFont' => 'sans-serif']);
        $pdf = PDF::loadView('admin.order.pdfQuotation')->setPaper('a4', 'landscape')->setWarnings(false);
        $pdf->save(public_path('/uploads/orders/quotations/').$file_name);
        Quotation::create([
            'name' => $file_name,
            'architect_id' => $man->id,
            'order_id' => $id,
            'date' => date('Y-m-d H:i:s'),
            'path' => '/uploads/orders/quotations/'.$file_name
        ]);
        Order::where('id', $id)->update([
            'status' => 'approve'
        ]);
        toastr()->success('Order Approved Successfully');
        return redirect()->back();
    }


    public function decline($id)
    {
        //
        Order::where('id', $id)->update([
            'status' => 'decline'
        ]);
        toastr()->success('Order Declined Successfully');
        return redirect()->back();
    }

    public function exportExcelCustomDesign ()
    {
        return Excel::download(new OrderExport(), 'invoices.xlsx');
    }

    public function exportExcelCustomDesignPost ()
    {
        return Excel::download(new OrderExport(), 'invoices.xlsx');
    }

    public function pdfExportQuotation (Request $request)
    {
        $qutation = Quotation::where('order_id', $request->order_id)->where('architect_id', '!=', null)->where('architect_id', Auth::id())->first();
        return response()->download(public_path($qutation->path));

        $orders = json_decode($request->products);
        view()->share('orders',$orders);
        $file_name = 'Quo-'.date('YmdHis').'.pdf';
        PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif']);
        $pdf = PDF::loadView('admin.order.pdfQuotation');
        $pdf->save(public_path('/uploads/orders/quotations/').$file_name);
        Quotation::create([
            'name' => $file_name,
            'order_id' => $file_name,
            'date' => date('Y-m-d H:i:s'),
            'path' => '/uploads/orders/quotations/'.$file_name
        ]);
        return $pdf->download('pdfview.pdf');
        return view('admin.order.pdfQuotation', compact('orders'));
    }

    public function orderInvoiceShow($id) {
        $order = Order::where('id', $id)->with('products')->first();
        $total = 0;
        foreach ($order->products as $product) {
            $total += $product->price * $product->quantity;
        }
        return view('architect.invoice.show', compact('order', 'total'));
    }


    public function search(Request $request) {
        $locations = Location::all();
        $quotations = Array();
        $month = date('m', strtotime($request->startDate));
        $year = date('Y', strtotime($request->startDate));

        if ($request->company !== "0" && $request->country !== "0") {
            $companies = Company::where('id', $request['company'])->orWhere('location_id', $request['country'])->pluck('id');
            $quotations = Quotation::whereIn('company_id', $companies)->whereMonth('created_at', $month)->whereYear('created_at', $year)->pluck('id');
        }
        elseif ($request->company !== "0") {
            $companies = Company::where('id', $request['company'])->pluck('id');
            $quotations = Quotation::whereIn('company_id', $companies)->whereMonth('created_at', $month)->whereYear('created_at', $year)->pluck('id');
        }
        elseif ($request->country !== "0") {
            $companies = Company::orWhere('location_id', $request['country'])->pluck('id');
            $quotations = Quotation::whereIn('company_id', $companies)->whereMonth('created_at', $month)->whereYear('created_at', $year)->pluck('id');
        }
        else {
            $companies = Company::pluck('id');
            $quotations = Quotation::whereIn('company_id', $companies)->whereMonth('created_at', $month)->whereYear('created_at', $year)->pluck('id');
        }

//        $quotations = $quotations->whereMonth('created_at', $month)->whereYear('created_at', $year)->pluck('id');
        $custom_orders = Order::whereIn('quotation_id', $quotations)->get();
        $breadcrumb = ucfirst(Auth::user()->roles[0]->name)." / Orders / Quotation";
        $companies = Company::all();

        return view('admin.order.index', compact( 'locations', 'breadcrumb', 'custom_orders','companies'));
    }

    public function updateOrderPriceListColumn(Request $request) {
        $data = SiteSetting::where('key', $request->index)->first();
        if($data) {
            SiteSetting::where('key', $request->index)->update([
                'data' => $request->id,
            ]);
        }
        else {
            SiteSetting::create([
                'data' => $request->id,
                'key' => $request->index,
            ]);
        }
        \request()->session()->put($request->index, $request->id);
        return response()->json([
            'type' => 'success'
        ]);
    }

    public function draggable() {
        return view('admin.order.draggable');
    }

}
