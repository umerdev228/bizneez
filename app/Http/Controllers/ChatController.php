<?php

namespace App\Http\Controllers;

use App\Chat;
use App\Events\MessageSent;
use App\Message;
use App\User;
use App\AuthCode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;

class ChatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::with('messages')->orderBy("created_at", "DESC")->get();
        $roles = Role::with('users')->get();
        $breadcrumb = 'CRM / Text Chat';
        return view('admin.chat.chat', compact('users', 'roles', 'breadcrumb'));
    }


    public function video_calling() {
        $users = User::with('messages')->orderBy("created_at", "DESC")->get();
        $roles = Role::with('users')->get();
        $auth_code = AuthCode::all();
        $breadcrumb = 'Video Calling';
        $logged = Auth::user();
        return view('admin.video-chat.video_calling', compact('users', 'roles', 'breadcrumb' , 'auth_code', 'logged'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Chat  $chat
     * @return \Illuminate\Http\Response
     */
    public function show(Chat $chat)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Chat  $chat
     * @return \Illuminate\Http\Response
     */
    public function edit(Chat $chat)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Chat  $chat
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Chat $chat)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Chat  $chat
     * @return \Illuminate\Http\Response
     */
    public function destroy(Chat $chat)
    {
        //
    }

    public function fetchMessages()
    {
        return Message::with('sender')->with('receiver')->get();
    }

    /**
     * Persist message to database
     *
     * @param  Request $request
     * @return Response
     */
    public function sendMessage(Request $request)
    {
        $user = Auth::user();

        $message = Message::create([
            'sender_id' => Auth::id(),
            'receiver_id' => $request['receiver']['id'],
            'message' => $request['message']
        ]);

//        $message = $user->messages()->create([
//            'message' => $request->input('message')
//        ]);
        $sender = User::where('id', $request['sender']['id'])->first();
        $receiver = User::where('id', $request['receiver']['id'])->first();


        broadcast(new MessageSent($message))->toOthers();

        $messages = Message::with('sender')->with('receiver')->get();
        return response()->json([
            'type' => 'success',
            'status' => 'Message Sent!',
            'messages' => $messages,
        ]);

        return ['status' => 'Message Sent!'];
    }

}
