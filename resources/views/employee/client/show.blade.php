@extends('admin.layouts.app')
@section('content')
    <div class="content">
        <div class="container-fluid" style="margin-top: 75px;">
            <div class="row">
                <div class="col-12">
                    @if(session()->has('message'))
                        <div class="alert alert-success mt-5" id="myElem" >
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-lg-8">
                            <div class="card-box task-detail">
                                <div class="media mt-0 m-b-30">
                                    <img class="d-flex mr-3 rounded-circle" alt="64x64" src="{{asset($user->profile->image)}}" style="width: 48px; height: 48px;">
                                    <div class="media-body">
                                        <h5 class="media-heading mb-0 mt-0">{{ $user->name }}</h5>
{{--                                        @if(count($user->categories) > 0)--}}
{{--                                            @foreach($user->categories as $category)--}}
{{--                                                <span class="badge badge-danger">{{ $category->name }}</span>--}}
{{--                                            @endforeach--}}
{{--                                        @endif--}}
                                    </div>
                                </div>
                                <h4 class="m-b-20">Address</h4>
                                <p class="text-muted"> {{ $user->profile->address }} </p>
                                <ul class="list-inline task-dates m-b-0 mt-5">
                                    <li>
                                        <h5 class="m-b-5">Role</h5>
                                        <p> {{ ucfirst($user->roles[0]->name) }}</p>
                                    </li>

                                    <li>
                                        <h5 class="m-b-5">Mobile</h5>
                                        <p>{{ $user->profile->mobile }}</p>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
{{--                                <div class="assign-team mt-4">--}}
{{--                                    <h5 class="m-b-5">Manufacturer</h5>--}}
{{--                                    <div>--}}
{{--                                        <a href="#"> <img class="rounded-circle thumb-sm" alt="64x64" src="{{asset($user->profile->image)}}"> </a>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
                                <div class="clearfix"></div>
                                <div class="assign-team mt-4">
{{--                                    <h5 class="m-b-5">Colors</h5>--}}
{{--                                    <div style="background-color: {{$user->color}} !important; width: 50px; height: 50px; border-radius: 50px;">--}}
{{--                                    </div>--}}
{{--                                    <div>--}}
{{--                                        @if(count($user->media) > 0)--}}
{{--                                            @foreach($user->media as $media)--}}
{{--                                                <a href="#"> <img class="rounded-circle thumb-sm" alt="64x64" src="{{asset($media->path)}}"> </a>--}}
{{--                                            @endforeach--}}
{{--                                        @endif--}}
{{--                                    </div>--}}
                                </div>
{{--                                <div class="attached-files mt-4">--}}
{{--                                    <h5 class="">Images</h5>--}}
{{--                                    <div class="files-list">--}}
{{--                                        @if(count($user->media) > 0)--}}
{{--                                            @foreach($user->media as $media)--}}
{{--                                                <div class="file-box">--}}
{{--                                                    <a href=""><img src="{{asset($media->path)}}" class="img-responsive img-thumbnail" alt="attached-img"></a>--}}
{{--                                                    <p class="font-13 mb-1 text-muted"><small>File one</small></p>--}}
{{--                                                </div>--}}
{{--                                        @endforeach--}}
{{--                                        @endif--}}
{{--                                    </div>--}}
{{--                                </div>--}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
