<?php

namespace App\Http\Controllers;

use App\Appointment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class AppointmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $appointments = Appointment::all();
        $breadcrumb = 'Demo Requests';
        return view('admin.appointment.index', compact('appointments', 'breadcrumb'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        Appointment::create([
            "name" => $request->name,
            "company_name" => $request->company_name,
            "telephone" => $request->telephone,
            "email" => $request->email,
            "date" => $request->date,
            "appointment_time" => $request->appointment_time,
//            "number_of_directors" => $request->number_of_directors,
            "number_of_employees" => $request->number_of_employees,
        ]);

        $data["email"] = 'kem@bizneez.net';
        $data["title"] = 'Appointment Request';
        $data["body"] = 'Name: '.$request->name.'<br>'.
            'Email: '.$request->email.'<br>'.
            'Phone: '.$request->telephone.'<br>'.
            'Company Time: '.$request->company_name.'<br>'.
            'Appointment Time: '.$request->appointment_time.'<br>'.
            'Number of Employees: '.$request->number_of_employees.'<br>';


        Mail::send('mail.custom_mail', $data, function($message)use($data) {
            $message->to($data["email"], $data["email"])
                ->subject($data["title"]);
        });

        toastr()->success('Demo Appointment Request Submitted Successfully');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function show(Appointment $appointment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function edit(Appointment $appointment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Appointment $appointment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Appointment $appointment)
    {
        //
    }
}
