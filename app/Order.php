<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    //
    protected $guarded = [];

    public function user() {
        return $this->belongsTo(User::class, 'user_id', 'id')->with('profile');
    }
    public function quotation() {
        return $this->hasOne(Quotation::class, 'id', 'quotation_id');
    }

    public function products() {
        return $this->belongsToMany(Product::class, 'order_products',  'order_id',  'product_id')->with('manufacturer')->with('colors')->with('media')->withPivot('quantity', 'price');;
    }
}
