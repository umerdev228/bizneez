@extends('admin.layouts.app')
@section('content')

    <style>
        .dropzone {
            background: white;
            border-radius: 5px;
            border: 2px dashed rgb(0, 135, 247);
            border-image: none;
            width: 100%;
            /*max-width: 500px;*/
            margin-left: auto;
            margin-right: auto;
        }

        .bootstrap-filestyle {
            display: none;
        }
        .label-info {
            background-color: #5bc0de;
            display: inline-block;
            padding: 0.2em 0.6em 0.3em;
            font-size: 75%;
            font-weight: 700;
            line-height: 1;
            color: #fff;
            text-align: center;
            white-space: nowrap;
            vertical-align: baseline;
            border-radius: 0.25em;
        }

    </style>
    <!-- Google Chart JS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/min/dropzone.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/dropzone.js"></script>
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    @if(session()->has('message'))
                        <div class="alert alert-success mt-5" id="myElem" >
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-12 mt-5">
                            <a type="button" data-toggle="modal" data-target="#imageUploadModal" href="javascript:void(0)"  class="btn btn-primary" ><i class="fa fa-plus"></i>
                                Add New Product Range
                            </a>
                            <div class="card">
                                <div class="card-body">
                                    @foreach($ranges as $range)
                                    <div class="col-lg-12">
                                        <h3>{{$range->name}}</h3>
                                        <div class="row">
                                            @foreach($range->images as $image)
                                            <div class="col-lg-2">
                                                <img width="150px" class="img img-responsive m-2" src="{{asset($image->path)}}">
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal" id="imageUploadModal">
                <div class="modal-dialog modal-lg">
                    <form action="{{route('manufacturer.product.range.store')}}" method="post">
                        @csrf
                        <div class="modal-content">
                            <div class="modal-header">
                                <button onclick="oncloseClear()" type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="modal-title" id="myLargeModalLabel">Upload Products Range Images</h4>
                            </div>
                            <div class="modal-body">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="name">Enter Range</label>
                                        <input class="form-control" type="text" name="name" id="name" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="image"> Images <span class="text-danger"></span></label>
                                        <div id="dropzone">
                                            <div class="dropzone needsclick" id="gallery-image-upload">
                                                <div class="dz-message needsclick">
                                                    Upload Products Range Images
                                                </div>
                                            </div>
                                        </div>
                                        <br/>
                                        <div id="gallery-preview-image" style="display: none;">
                                            <div class="dz-preview dz-file-preview">
                                                <div class="dz-image">
                                                    <img data-dz-thumbnail=""/>
                                                </div>
                                                <div class="dz-details">
                                                    <div class="dz-size"><span data-dz-size=""></span></div>
                                                    <div class="dz-filename"><span data-dz-name=""></span></div></div>
                                                <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress=""></span></div>
                                                <div class="dz-error-message"><span data-dz-errormessage=""></span></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <input class="btn btn-success" type="submit">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <style href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/dropzone.css"></style>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/dropzone.js"></script>

    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>

    <script !src="">
        Dropzone.autoDiscover = false;

        var dropzone = new Dropzone(document.getElementById('gallery-image-upload'), {
            url: "{{route('product.range.images.upload')}}", // Set the url
            previewTemplate: document.querySelector('#gallery-preview-image').innerHTML,
            parallelUploads: 2,
            thumbnailHeight: 120,
            thumbnailWidth: 120,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            filesizeBase: 1000,
            thumbnail: function(file, dataUrl) {
                if (file.previewElement) {
                    file.previewElement.classList.remove("dz-file-preview");
                    var images = file.previewElement.querySelectorAll("[data-dz-thumbnail]");
                    for (var i = 0; i < images.length; i++) {
                        var thumbnailElement = images[i];
                        thumbnailElement.alt = file.name;
                        thumbnailElement.src = dataUrl;
                    }
                    setTimeout(function() { file.previewElement.classList.add("dz-image-preview"); }, 1);
                }
            }

        });

        function oncloseClear() {
            axios.get('{{route('delete.uploaded.images')}}')
            .then(function (response) {
                console.log(response.data)
            })
            dropzone.removeAllFiles()
            console.log(dropzone)
        }

    </script>
@endsection
