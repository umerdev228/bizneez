@extends('admin.layouts.app')
@section('content')
    @include('admin.layouts.topbar')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    @if(session()->has('message'))
                        <div class="alert alert-success mt-5" id="myElem" >
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-12 mt-5">
                            <div class="card">
                                <div class="card-body">
                                    <a href="{{route('admin.leads.create')}}"  class="btn btn-primary" ><i class="fa fa-plus"></i>Add New Lead </a>
                                </div>
                                <div class="card-body">
                                    <table class="table table-hover m-0 tickets-list table-actions-bar dt-responsive nowrap" width="100%" id="normal-table">
                                        <thead>
                                        <tr>
                                            <th>Title</th>
                                            <th> Image </th>
                                            <th>Email</th>
                                            <th>Type</th>
                                            <th>Source</th>
                                            <th>Phone</th>
{{--                                            <th> Details </th>--}}
                                            <th data-orderable="false">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($leads as $lead)
                                            <tr>
                                                <td>{{$lead->title}}</td>
                                                <td>
                                                    @if($lead->image)
                                                        <img src="{{asset($lead->image)}}" width="100">
                                                    @endif
                                                </td>
                                                <td>{{$lead->email}}</td>
                                                <td>{{$lead->type}}</td>
                                                <td>{{$lead->source}}</td>
                                                <td>{{$lead->phone}}</td>
{{--                                                <td>{{$lead->details}}</td>--}}
                                                <td>
                                                    <div class="btn-group">
                                                        <a class="btn" href="{{ route('admin.leads.edit', ['id' => $lead->id] ) }}">
                                                            <i class="mdi mdi-pencil"></i>
                                                        </a>
                                                        <a class="btn" href="{{ route('admin.leads.delete', ['id' => $lead->id] ) }}">
                                                            <i class="fa fa-close"></i>
                                                        </a>
{{--                                                        <a class="btn" href="{{ route('admin.leads.show', ['id' => $lead->id, 'role' => Auth::user()->roles[0]->name] ) }}">--}}
{{--                                                            <i class="mdi mdi-eye"></i>--}}
{{--                                                        </a>--}}
                                                    </div>

                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div> <!-- end card -->
                            </div> <!-- end card -->
                        </div><!-- end col -->
                    </div>

                </div>
            </div>

        </div> <!-- container -->
    </div> <!-- content -->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

    @include('admin.layouts.footer')
@endsection
