<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\File;
//use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
// use  Illuminate\Support\Facades\Input;
// use Illuminate\Support\Facades\Input;
//use App\Customer;
use App\User;
use App\Client;
//use App\Timezone;
use Auth;
use Mail;

//use App\Notification;
//use App\History;


 

class ClientsController extends Controller
{
     
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       
       $this->middleware('auth');
    }

 

    public function index(){  
        $client = Client::all(); 
           $data= [
                    'client' => $client,
                    'breadcrum' => 'Dashboard / Clients List',
                     'url' => env('APP_ENV_LINK'),
                ];
                return view('admin.client')->with($data);
    }

 
public function getting_start(){ //after login page
    
     $this->checkExpire(Auth::user()->id);

     $merchant = Merchant::where('user_id', Auth::user()->id )->with('user')->first();
      $data= [
            'title'=>"Updated information successfully", 
            'breadcrum' => 'Getting Started',
            'profile_status' => 1,
            'merchant' => $merchant,
            'url' => env('APP_ENV_LINK'),
          ];

        return view('getting-started')->with($data);
}
 public function confirm_detail(Request $request){
        
      $merchant = Merchant::where('user_id', Auth::user()->id )->first();
        
      
      $merchant->name = $request->fname. ' '.$request->lname;
       
      $merchant->phone = $request->phone;
        
      if($merchant->profile_status==0)
        $merchant->profile_status = 1;
     $merchant->save();
      if($request->has('profile_pic')){
       
               $file = $request->file('profile_pic');
              
               if(!empty($file)){
              $name = $merchant->id.'_'.time().$file->getClientOriginalName();
                } 
                
               $file->move('assets/images/users', $name);  
               $merchant->profile_pic = $name;
               $merchant->save();
       }
              $data= [
            'title'=>"Updated information successfully", 
            'breadcrum' => 'Getting Started', 
            'merchant' => $merchant
          ];
        return redirect('get_start_dashboard');
        //return view('getting-started')->with($data);  
        //return redirect('getting-started')->with('message', 'You are setup password please login'); 
}

 public function confirm_reset_activation(Request $request){ 
 

      $merchant = Merchant::where('user_id', Auth::user()->id)->first();
      // dd($merchant->token);

     /* if($merchant->email!= $request->id) //if not mtach email
      {
            return $merchant->email;
      }*/
    //  return 2;
      
                $name =   $merchant->name;
                $title = "Reset Activation";
                $url = env('APP_URL');
                $token = "";
                
                Mail::to($request->id)->send(new \App\Mail\MerchantActivation($name, $title, $url, $token));  
                
                $merchant->profile_status = 2;  //update profile status 
                $merchant->save();    

                return 1;

      /*if($request->file('file')){
               $file = $request->file('profile_pic');
               $name = $merchant->id.'_'.time().$file->getClientOriginalName();
               $file->move('public/merchant/', $name);  
               $merchant->profile_pic = $name;
               $merchant->save();
       }
         //return view('getting-started');

          $data= [
            'title'=>"Updated information successfully", 
            'breadcrum' => 'Getting Started',
            'profile_status' => 1,
            'merchant' => $merchant
          ];

        return view('getting-started')->with($data);  */
        //return redirect('getting-started')->with('message', 'You are setup password please login'); 
}



public function confirm_url(Request $request){ 
 
      $merchant = Merchant::where('user_id', Auth::user()->id)->first();
      $check_url_exit=Merchant::where('url',$request->id)->count();

      if($check_url_exit==0){
         $merchant->profile_status = 3;  //update profile status
         $merchant->url = $request->id;
         $merchant->language = $request->lng;
         $merchant->save();
         return 1;    
      }
      else
      { //url is already exist
        return 2;
      }
 
      
}


public function channels(Request $request){ 
 
      $merchant = Merchant::where('user_id', Auth::user()->id)->first();
        
         $merchant->profile_status = 5;  //update profile status
         $merchant->channel_email = $request->channel_email;
         $merchant->channel_phone = $request->channel_phone;
         $merchant->channel_social = $request->channel_social;
         $merchant->channel_chatt = $request->channel_chatt;
         $merchant->channel_forums = $request->channel_forums;
         $merchant->save();
         return true;    
      
 
      
}


public function support_team_invitation(Request $request){
          //dd('awaisaasassa');
 // for($i=0;$i<count(array($request->tagsinput));$i++){

 
 //                //print $request->tagsinput[$i].'<br>';
            
             
 //            $title = "TMS Support Team Invtation";
 //            $name = "";
 //            $url = "";
 //            $token = "";
 //            Mail::to((array($request->tagsinput)))->send(new \App\Mail\MerchantSuportTeam($name, $title, $url, $token));

 //            $merchant = Merchant::where('user_id', Auth::user()->id)->first();
 //            $merchant->profile_status = 5;  //update profile status 
 //            $merchant->save(); 

 //             return redirect('get_start_dashboard')->with('message', 'Thank you!! you are completed the profile setup, please <a href="">Join Now</a>');;
 //            }
      














          // print count($request->tagsinput); 
           // dd('awa');
   // dd($request->tagsinput);
  if(!empty($request->tagsinput))
  {
          for($i=0;$i<count($request->tagsinput);$i++){
                //print $request->tagsinput[$i].
            $title = "TMS Support Team Invtation";
            $name = "";
            $url = "";
            $token = "";
            Mail::to($request->tagsinput[$i])->send(new \App\Mail\MerchantSuportTeam($name, $title, $url, $token));

            $merchant = Merchant::where('user_id', Auth::user()->id)->first();
            $merchant->profile_status = 5;  //update profile status 
            $merchant->save(); 

             // return redirect('get_start_dashboard')->with('message', 'Thank you!! you are completed the profile setup, please <a href="">Join Now</a>');;


          }
  }

  return redirect('get_start_dashboard')->with('message', 'No Tags has been entered.');

 }

public function call_api(){
    $merchant = Merchant::where('user_id', Auth::user()->id)->with('user')->first();
    /* print "<pre>";
     print_r($merchant); dd();*/



}
                                                                                       
 
}
