<?php

use Illuminate\Database\Seeder;

class AdminSettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
  
          public function run()
    {
         DB::table('setting')->insert([
            
            'email' => 'admin@admin.com',
            'stripe_email' => 'stripe@stripe.com',
            'stripe_password' => 'stripe',
            'Address' => 'Cybexo inc',
            'description' => 'Cybexo',
            'phone' => '123456789'
            
        ]);
    }
    
}
