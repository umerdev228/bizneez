<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Quotation</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

</head>
<body>

<div class="container">
    <div class="col-12">
        <div class="row">
            <div class="float-right">
                <h4>PROFORMA</h4>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="float-left">
                    <span class="img">
                        <img src="{{public_path('/uploads/logo/img.png')}}">
                    </span>
                </div>
                <div class="float-right">
                    <div class="row">
                        <span class="col-4">
                            Quote Number:
                        </span>
                        <span class="col-4">
                            order serial number
                        </span>
                    </div>
                    <div class="row">
                        <span class="col-4">
                            Quote Date:
                        </span>
                        <span class="col-4">
{{--                            {{ $orders[0]->created_at }}--}}
                        </span>
                    </div>
                </div>

            </div>
        </div>
        <div class="col">
            <table>
                <tr>
                    <td>
                        Bizneez.co.uk Ltd
                    </td>
                </tr>

                <tr>
                    <td>
                        243 Hall Lane, London, E4 8HX
                    </td>
                </tr>

                <tr>
                    <td>
                        Tel: 020 8524 5294
                    </td>
                </tr>

                <tr>
                    <td>
                        Registration: 11668911
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>CONRAD BIRCH CONSTRUCTION</td>
                </tr>

                <tr>
                    <td>Contact Person:
                    </td>
                    <td>YASMIN
                    </td>
                </tr>

                <tr>
                    <td></td>
                    <td>CONRAD BIRCH CONSTRUCTION</td>
                </tr>

                <tr>
                    <td>Delivery Address:
                    </td>
                    <td>43-81 GREENWICH HIGH ROAD SE10
                    </td>
                </tr>
                <tr>
                    <td>Invoice Address:</td>
                    <td>Same as above</td>
                </tr>

            </table>
        </div>
    </div>
    <table>
        <thead>
            <tr>
                <th>Image</th>
                <th>Description</th>
                <th>Qty</th>
                <th>Price</th>
                <th>Discount</th>
                <th>New Price</th>
                <th>Total</th>
            </tr>
        </thead>
        <tbody>
        @foreach($orders as $order)
            <tr>
                <td>
                    @if(count($order->media) > 0)
                        <img src="{{public_path($order->media[0]->path)}}" width="100">
                    @endif
                </td>
                <td>{{ $order->description }}</td>
                <td>{{ $order->quantity }}</td>
                <td><span>&#163;</span>{{ $order->price }}</td>
                <td><span>&#163;</span>{{ $order->discount }}</td>
                <td>New Price</td>
                <td>Total</td>
            </tr>
        @endforeach

        </tbody>


    </table>


</div>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>