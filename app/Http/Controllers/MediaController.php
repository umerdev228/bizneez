<?php

namespace App\Http\Controllers;

use App\Media;
use App\Product;
use App\ProductRange;
use App\User;
use Illuminate\Http\Request;
use Spatie\LaravelImageOptimizer\Facades\ImageOptimizer;

class MediaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $image = $request->file('file');
        $imageName = $image->getClientOriginalName();
        $image->move(public_path('uploads/images/products/gallery/'),$imageName);

        $media = Media::create([
            'name' => 'gallery',
            'item_type' => 'product',
            'path' => 'uploads/images/products/gallery/' . $imageName,
        ]);
        return response()->json(['type' => 'success', 'message' => 'Product Image Uploaded Successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Media  $media
     * @return \Illuminate\Http\Response
     */
    public function show(Media $media)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Media  $media
     * @return \Illuminate\Http\Response
     */
    public function edit(Media $media)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Media  $media
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Media $media)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Media  $media
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Media::where('id', $id)->delete();
        toastr()->success('Image Deleted Successfully');
        return redirect()->back();
    }

    public function imageUpload(Request $request) {
        $id = Array();
//        dd(json_decode(\request()->session()->get('product-images'))[0]);

        if (\request()->session()->get('product-images')) {
            $id = (json_decode(\request()->session()->get('product-images')));
        }
        $image = $request->file('file');
        $imageName = $image->getClientOriginalName();
        $image->move(public_path('uploads/images/products/'),$imageName);

        $media = Media::create([
            'item_type' => 'product',
            'path' => 'uploads/images/products/' . $imageName,
        ]);
        array_push($id, $media);
        \request()->session()->put('product-images', json_encode($id));
        return response()->json(['success'=>$imageName]);
    }

    public function colorImageUpload(Request $request) {
        $id = Array();
//        dd(json_decode(\request()->session()->get('product-images'))[0]);

        if (\request()->session()->get('product-color-images')) {
            $id = (json_decode(\request()->session()->get('product-color-images')));
        }
        $image = $request->file('file');
        $imageName = $image->getClientOriginalName();
        $image->move(public_path('uploads/images/products/'),$imageName);

        $media = Media::create([
            'item_type' => 'product_color',
            'path' => 'uploads/images/products/' . $imageName,
        ]);
        array_push($id, $media);
        \request()->session()->put('product-color-images', json_encode($id));
        return response()->json(['success'=>$imageName]);
    }

    public function productImageRemove($id) {
        Media::where('id', $id)->delete();
        return response()->json(['type' => "success"]);
    }

    public function productColorImageRemove($id) {
        Media::where('id', $id)->delete();
        return redirect()->back();
    }

    public function imageDelete(Request $request) {
        Media::where('id', $request['id'])->delete();
        return response()->json(['result' => 'success', 'message' => 'Image Deleted Successfully']);
    }

    public function download(Request $request) {
        return response()->download($request->path);
    }

    public function addToSessions(Request $request) {

        $id = Array();
        $type = 'add';
//        dd(json_decode(\request()->session()->get('product-images'))[0]);

        if (\request()->session()->get('product-images')) {
            $id = (json_decode(\request()->session()->get('product-images')));
        }
//        dd($id);
        foreach ($id as $index => $i) {
            if($i->id == $request['id']) {
                $type = 'remove';
                unset($id[$index]);
            }
        }
        if ($type == 'add') {
            $media = Media::where('id', $request['id'])->first();
            array_push($id, $media);
        }
        \request()->session()->put('product-images', json_encode($id));

        return response()->json([
            'type' => $type,
        ]);
    }

    public function storeApiMediaFiles(Request $request)
    {
        if(!$request->hasFile('file')) {
            return response()->json(['upload_file_not_found'], 400);
        }


        $allowedfileExtension=['pdf','jpg','png'];
        $files = $request->file('file');
        $errors = [];
        $media = [];

        foreach ($files as $file) {

            $extension = $file->getClientOriginalExtension();

            $check = in_array($extension,$allowedfileExtension);

                foreach($request->file as $mediaFiles) {

                    $path = $mediaFiles->store('/uploads/api/gallery/images');
                    $name = $mediaFiles->getClientOriginalName();

                    //store image file into directory and db
                    $save = new Media();
                    $save->name = $name;
                    $save->source = 'api';
                    $save->path = $path;
                    $save->save();
                    array_push($media, $save);
                }


            return response()->json([
                'type' => 'success',
                'media' => $media,
                'message' => 'file_uploaded'
            ], 200);

        }
        return response()->json(['type' => 'error']);
    }

    public function allMobileUploads() {
        $media = Media::where('source', 'api')->get();
        $breadcrumb = 'Mobile Uploads';
        return view('admin.ai.gallery.index', compact('media', 'breadcrumb'));
        dd($media);
    }


    public function storeApiMediaFile(Request $request)
    {
        if(!$request->hasFile('file')) {
            return response()->json(['upload_file_not_found'], 400);
        }


        $allowedfileExtension=['pdf','jpg','png'];
        $file = $request->file('file');
        $errors = [];
        $media = [];


            $extension = $file->getClientOriginalExtension();

            $check = in_array($extension,$allowedfileExtension);

                $mediaFiles = $request->file;

                $path = $mediaFiles->store('/uploads/api/gallery/images');
                $name = $mediaFiles->getClientOriginalName();

                //store image file into directory and db
                $save = new Media();
                $save->name = $name;
                $save->source = 'api';
                $save->path = $path;
                $save->save();



            return response()->json([
                'type' => 'success',
                'media' => $save,
                'message' => 'file_uploaded'
            ], 200);


        return response()->json(['type' => 'error']);
    }

    public function productRangeImages(Request $request) {
        $id = Array();

//        dd(json_decode(\request()->session()->get('product-images'))[0]);

        if (\request()->session()->get('product-range-images')) {
            $id = (json_decode(\request()->session()->get('product-range-images')));
        }
        $image = $request->file('file');
        $imageName = $image->getClientOriginalName();
        $image->move(public_path('uploads/images/products/range/'),$imageName);
        $pathToImage = 'uploads/images/products/range/'.$imageName;
        $media = Media::create([
            'item_type' => 'product-range',
            'path' => 'uploads/images/products/range/' . $imageName,
        ]);
        array_push($id, $media);
        \request()->session()->put('product-range-images', json_encode($id));
        ImageOptimizer::optimize($pathToImage);

        return response()->json(['success'=>$imageName]);
    }

    public function getImagesBymanufacturer(Request $request) {
        $ranges = ProductRange::where('created_by', $request->id)->get();
        $ranges_id = ProductRange::where('created_by', $request->id)->pluck('id');
        $images = Media::whereIn('item_id', $ranges_id)->get();
        $quantity = count($images);
        return response()->json([
            'type' => 'success',
            'images' => $images,
            'ranges' => $ranges,
            'quantity' => $quantity,
        ]);
    }
    public function getImagesByRange(Request $request) {
        $images = Media::where('item_id', $request->id)->get();
        $quantity = count($images);
        return response()->json([
            'type' => 'success',
            'images' => $images,
            'quantity' => $quantity,
        ]);
    }

    public function removeImageFromSession() {
        \request()->session()->forget('product-range-images');
        return response()->json(['type' => 'success']);
    }

    public function screenShare() {
        return view('admin.screenshare.index');
    }

    public function seo() {
        $manufacturers = User::role('manufacturer')->with('ranges')->get();
        return view('admin.product.seo.index', compact('manufacturers'));
    }

    public function seoPost(Request $request) {
        $myString = $request->product_images;
        $productImages = explode(',', $myString);
        foreach ($productImages as $image) {
            Media::where('id', $image)->update([
                "keywords" => $request->keywords,
                "title" => $request->title,
                "description" => $request->image_description,
            ]);
        }
        toastr()->success('Image SEO Updated Successfully');
        return redirect()->back();
    }
}
