@extends('admin.layouts.app')
@section('content')
    @include('admin.layouts.topbar')
    <!-- Google Chart JS -->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <!-- Start Page content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if(session()->has('message'))
                            <div class="alert alert-success" style="width:803px ">
                                {{ session()->get('message') }}
                            </div>
                        @endif
                        <div class="row">
                            <div class="col-9 mt-5">
                                <div class="card-box">
                                    <form method="POST" action="{{ route('admin.manufacturers.update', ['id' => $manufacturer->id]) }}" enctype="multipart/form-data">
                                        <div class="col-9 mt-5">
                                            <div class="card-box">
                                                @csrf
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label for="first_name">Manufacturer Name<span class="text-danger">*</span></label>
                                                            <input type="text" class="form-control valid" name="first_name" value="{{$manufacturer->first_name}}" id="first_name" required />
                                                            @if ($errors->has('first_name'))
                                                                <span class="error">{{ $errors->first('first_name') }}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label for="area_id">Job Description<span class="text-danger">*</span></label>
                                                            <input placeholder="Job Description Exp: Director" type="text" class="form-control valid" name="job_description" value="{{$manufacturer->job_description}}" value="" id="job_description"  />
                                                            @if ($errors->has('job_description'))
                                                                <span class="error">{{ $errors->first('job_description') }}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label for="email">Email<span class="text-danger">*</span></label>
                                                            <input type="email" class="form-control valid" name="email" id="email" value="{{$manufacturer->email}}" required />
                                                            @if ($errors->has('email'))
                                                                <span class="error">{{ $errors->first('email') }}</span>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label for="web_url">Web URL</label>
                                                            <input type="text" class="form-control valid" name="web_url" value="{{$manufacturer->profile->web_url}}" id="web_url"/>
                                                            @if ($errors->has('web_url'))
                                                                <span class="error">{{ $errors->first('web_url') }}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label for="address">Address<span class="text-danger">*</span></label>
                                                            <input type="text" class="form-control valid" name="address" value="{{$manufacturer->profile->address}}" id="address" required />
                                                            @if ($errors->has('address'))
                                                                <span class="error">{{ $errors->first('address') }}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label for="phone">Phone<span class="text-danger">*</span></label>
                                                            <input type="tel" class="form-control valid" name="phone" value="{{$manufacturer->phone}}" id="phone" required />
                                                            @if ($errors->has('phone'))
                                                                <span class="error">{{ $errors->first('phone') }}</span>
                                                            @endif
                                                        </div>
                                                    </div>



                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label for="area_id">Enter Password<span class="text-danger">*</span></label>
                                                            <input placeholder="Please Enter Password" type="password" class="form-control valid" name="password" value="" id="password"  />
                                                            @if ($errors->has('password'))
                                                                <span class="error">{{ $errors->first('password') }}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label for="area_id">Confirm Password<span class="text-danger">*</span></label>
                                                            <input placeholder="Please Enter Password" type="password" class="form-control valid" name="password" value="" id="password2"  />
                                                            @if ($errors->has('password'))
                                                                <span class="error">{{ $errors->first('password') }}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div id="representative">
                                                        @foreach($manufacturer->representatives as $index => $representative)
                                                            <div id="r{{$index}}" class="row">

                                                                <div class="col-sm-3">
                                                                    <div class="form-group">
                                                                        <label for="location_id">Represent Name </label>
                                                                        <input placeholder="Please Represent Name" type="text" class="form-control valid" name="represent_name[]" value="{{$representative->name}}" id="represent_name"  />
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <div class="form-group">
                                                                        <label for="area_id">Represent Email </label>
                                                                        <input placeholder="Please enter represent email" type="text" class="form-control valid" name="represent_email[]" value="{{$representative->email}}" id="represent_email"  />
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <div class="form-group">
                                                                        <label for="area_id">Represent Mobile </label>
                                                                        <input placeholder="Please enter represent mobile" type="text" class="form-control valid" name="represent_mobile[]" value="{{$representative->mobile}}" id="represent_mobile"  />
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <div class="form-group">
                                                                        @if($index < 1)
                                                                            <button type="button" onclick="appendFieldsR()" style="border-radius: 10px" class="btn btn-success valid align-baseline"><i class="fa fa-plus"></i> </button>
                                                                        @endif
                                                                        @if($index > 0)
                                                                            <button type="button" onclick="removeFieldR({{$index}})" style="border-radius: 10px" class="btn btn-danger valid align-baseline"><i class="fa fa-minus"></i> </button>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label for="image">Company Logo<span class="text-danger">*</span></label>
                                                            <input type="file" class="form-control valid" name="image" id="image"/>
                                                            @if ($errors->has('image'))
                                                                <span class="error">{{ $errors->first('image') }}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-12">
                                                        <div class="form-group">
                                                            <label for="Description">Manufacturer Description<span class="text-danger">*</span></label>
                                                            <textarea id="Description" class="form-control rounded-0" placeholder="Exp: We are manufacturer of office furniture" name="description" rows="5">{{$manufacturer->profile->description}}</textarea>
                                                            @if ($errors->has('description'))
                                                                <span class="error">{{ $errors->first('description') }}</span>
                                                            @endif
                                                        </div>
                                                    </div>


                                                    <div class="col-12">
                                                        <div class="form-group">
                                                            <label for="company_keywords">Company Keywords<span class="text-danger">*</span></label>
                                                            <textarea class="form-control rounded-0" placeholder="" id="company_keywords" name="company_keywords" rows="5">{{$manufacturer->profile->company_keywords}}</textarea>
                                                            @if ($errors->has('company_keywords'))
                                                                <span class="error">{{ $errors->first('company_keywords') }}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group mt-3">
                                                    <input class="btn btn-primary" type="submit" value="Update Manufacturer">
                                                    <button onclick="history.back()" type="button" class="btn">Cancel</button>
                                                </div>
                                            </div> <!-- end card-box -->
                                        </div>
                                    </form>
                                </div> <!-- end card-box -->
                            </div>

                        </div> <!-- container -->
                </div> <!-- content -->
            </div>
        </div>
    </div>








    <script>
        var i = '{!! count($manufacturer->representatives) !!}'
        function appendFieldsR() {
            i = i+1
            console.log('clicked')
            //$('#company_keywords').val()='fefe';
            var html_input =
                `
                                                    <div id="r`+i+`" class="row">
                                                        <div class="col-sm-3">
                                                            <div class="form-group">
                                                                <label for="location_id">Representative Name </label>
                                                                <input placeholder="Please Enter Representative Name" type="text" class="form-control valid" name="represent_name[]" value="" id="represent_name"  />
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <div class="form-group">
                                                                <label for="area_id">Representative Email </label>
                                                                <input placeholder="Please Enter Representative email" type="text" class="form-control valid" name="represent_email[]" value="" id="represent_email"  />
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <div class="form-group">
                                                                <label for="area_id">Representative Phone </label>
                                                                <input placeholder="Please Enter Representative Phone" type="text" class="form-control valid" name="represent_mobile[]" value="" id="represent_mobile"  />
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <div class="form-group">
                                                                <button onclick="removeFieldR(`+i+`)" style="border-radius: 10px; margin-top: 29px;" class="btn btn-danger valid align-baseline"><i class="fa fa-minus"></i> </button>
                                                            </div>
                                                        </div>
                                                    </div>`
            $('#representative').append(html_input)
        }
        function removeFieldR(id) {

            $('#r'+id).remove();
        }
    </script>

@endsection
