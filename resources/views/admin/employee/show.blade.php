@extends('admin.layouts.app')
@section('content')
    <div class="content">
        <div class="container-fluid" style="margin-top: 75px;">
            <div class="row">
                <div class="col-lg-12">
                    @if(session()->has('message'))
                        <div class="alert alert-success mt-5" id="myElem" >
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            {{ session()->get('message') }}
                        </div>
                    @endif
                        <div class="row">
                                <div class="card">
                                    <div class="card-box">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="row">
                                                    <div class="col-lg-3">
                                                        <img src="{{asset($user->profile->image)}}" width="200px" class="img-circle" id="image" >

                                                    </div>
                                                    <div class="col-lg-9">
                                                        <h5>{{$user->name}}</h5>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="">
                                                    <div class="tab-content">
                                                        <div class="tab-pane active show" id="create-employee">
                                                            <div class="card">
                                                                <div class="card-box">
                                                                    <div class="row">
                                                                        <div class="col-sm-6">
                                                                            <div class="form-group">
                                                                                <label for="first_name">First Name<span class="text-danger">*</span></label>
                                                                                <input disabled type="text" class="form-control valid" name="first_name" value="{{$user->first_name}}" id="first_name" required />
                                                                                @if ($errors->has('first_name'))
                                                                                    <span class="error">{{ $errors->first('first_name') }}</span>
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-6">
                                                                            <div class="form-group">
                                                                                <label for="middle_name">Middle Name<span class="text-danger"></span></label>
                                                                                <input disabled type="text" class="form-control valid" name="middle_name" value="{{$user->middle_name}}" id="middle_name"/>
                                                                                @if ($errors->has('middle_name'))
                                                                                    <span class="error">{{ $errors->first('middle_name') }}</span>
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-6">
                                                                            <div class="form-group">
                                                                                <label for="last_name">Last Name<span class="text-danger">*</span></label>
                                                                                <input disabled onfocusout="generate_email()" type="text" class="form-control valid" name="last_name" value="{{$user->last_name}}" id="last_name" required />
                                                                                @if ($errors->has('last_name'))
                                                                                    <span class="error">{{ $errors->first('first_name') }}</span>
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-6">
                                                                            <div class="form-group">
                                                                                <label for="email">Email<span class="text-danger">*</span></label>
                                                                                <input disabled type="email" class="form-control valid" name="email" id="email" value="{{$user->email}}" required />
                                                                                @if ($errors->has('email'))
                                                                                    <span class="error">{{ $errors->first('email') }}</span>
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-6">
                                                                            <div class="form-group">
                                                                                <label for="phone">Phone<span class="text-danger">*</span></label>
                                                                                <input disabled type="tel" class="form-control valid" name="phone" id="phone" value="{{$user->profile->phone}}" required />
                                                                                @if ($errors->has('phone'))
                                                                                    <span class="error">{{ $errors->first('phone') }}</span>
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-6">
                                                                            <div class="form-group">
                                                                                <label for="mobile">Mobile</label>
                                                                                <input disabled type="number" class="form-control valid" name="mobile" id="mobile" value="{{$user->profile->mobile}}" />
                                                                                @if ($errors->has('mobile'))
                                                                                    <span class="error">{{ $errors->first('mobile') }}</span>
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-6">
                                                                            <div class="form-group">
                                                                                <label for="assistant_number">Assistant Number</label>
                                                                                <input disabled type="text" class="form-control valid" name="assistant_number" id="assistant_number" value="{{$user->profile->assistant_number}}"   />
                                                                                @if ($errors->has('assistant_number'))
                                                                                    <span class="error">{{ $errors->first('assistant_number') }}</span>
                                                                                @endif
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-sm-6">
                                                                            <div class="form-group">
                                                                                <label for="assistant_number">Demo Code<span class="text-danger">*</span></label>
                                                                                <input disabled type="text" class="form-control valid" name="demo_code" id="demo_code" value="{{$user->profile->demo_code}}" required />
                                                                                @if ($errors->has('demo_code'))
                                                                                    <span class="error">{{ $errors->first('demo_code') }}</span>
                                                                                @endif
                                                                            </div>
                                                                        </div>



                                                                        <div class="col-sm-6">
                                                                            <div class="form-group">
                                                                                <label for="address">Address<span class="text-danger">*</span></label>
                                                                                <input disabled type="text" class="form-control valid" name="address" id="address" value="{{$user->profile->address}}" required />
                                                                                @if ($errors->has('address'))
                                                                                    <span class="error">{{ $errors->first('address') }}</span>
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-6">
                                                                            <div class="form-group">
                                                                                <label for="location_id">Location</label>
                                                                                <input disabled placeholder="Please Enter Location" type="text" class="form-control valid" name="location_id" value="{{$user->profile->location_id}}"  id="location_id"  />
                                                                                @if ($errors->has('location_id'))
                                                                                    <span class="error">{{ $errors->first('location_id') }}</span>

                                                                                @endif
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-sm-6">
                                                                            <div class="form-group">
                                                                                <label for="area_id">Job Description<span class="text-danger">*</span></label>
                                                                                <select disabled class="form-control valid" name="area_id" id="area_id" >
                                                                                    <option @if($user->profile->area_id === '1') selected @endif value="1">Managing director</option>
                                                                                    <option @if($user->profile->area_id === '2') selected @endif value="2">Sales director</option>
                                                                                    <option @if($user->profile->area_id === '3') selected @endif value="3">Regional manager</option>
                                                                                    <option @if($user->profile->area_id === '4') selected @endif value="4">Sales manager</option>
                                                                                    <option @if($user->profile->area_id === '5') selected @endif value="5">Team leader</option>
                                                                                    <option @if($user->profile->area_id === '6') selected @endif value="6">Sales consultant</option>
                                                                                </select>

                                                                                @if ($errors->has('job_description'))
                                                                                    <span class="error">{{ $errors->first('job_description') }}</span>
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-6">
                                                                            <div class="form-group">
                                                                                <label for="area_id">Team Building</label>
                                                                                <select disabled class="form-control valid" name="team_building" id="team_building" value="{{$user->profile->team_building}}" >
                                                                                    <option value="1">1</option>
                                                                                    <option value="2">2</option>
                                                                                    <option value="3">3</option>
                                                                                    <option value="4">4</option>
                                                                                    <option value="5">5</option>
                                                                                    <option value="6">6</option>
                                                                                    <option value="7">7</option>
                                                                                    <option value="8">8</option>
                                                                                    <option value="9">9</option>
                                                                                    <option value="10">10</option>

                                                                                </select>

                                                                                @if ($errors->has('job_description'))
                                                                                    <span class="error">{{ $errors->first('job_description') }}</span>
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </div>

                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
