@extends('admin.layouts.app')
@section('content')
    @include('admin.layouts.topbar')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    @if(session()->has('message'))
                        <div class="alert alert-success mt-5" id="myElem" >
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-12 mt-5">
                            <div class="card">
                                <div class="card-body">
                                    <a href="{{route('employee.clients.create')}}"  class="btn btn-primary" ><i class="fa fa-plus"></i>Add New Client </a>
                                </div>
                                <div class="card-body">
                                    <table class="table table-hover m-0 tickets-list table-actions-bar dt-responsive nowrap" width="100%" id="normal-table">
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Profile</th>
                                            <th>Email</th>
                                            <th>Phone</th>
                                            <th>Detail</th>
                                            <th>Created </th>
                                            <th data-orderable="false">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($clients as $client)
                                            <tr>
                                                <td>{{$client->name}}</td>
                                                <td><img src="{{asset($client->profile->image)}}" width="100"></td>
                                                <td>{{$client->email}}</td>
                                                <td>{{$client->profile->phone}}</td>
                                                <td>{{$client->profile->description}}</td>
                                                <td>{{$client->created_at}}</td>
                                                <td>
                                                    <div class="btn-group">
                                                        <a class="btn" href="{{ route('employee.clients.edit', ['id' => $client->id] ) }}">
                                                            <i class="mdi mdi-pencil"></i>
                                                        </a>
                                                        <a class="btn" href="{{ route('employee.clients.delete', ['id' => $client->id] ) }}">
                                                            <i class="fa fa-close"></i>
                                                        </a>
                                                        <a class="btn" href="{{ route('employee.clients.show', ['id' => $client->id] ) }}">
                                                            <i class="mdi mdi-eye"></i>
                                                        </a>
                                                    </div>

                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div> <!-- end card -->
                            </div> <!-- end card -->
                        </div><!-- end col -->
                    </div>

                </div>
            </div>

        </div> <!-- container -->
    </div> <!-- content -->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

    @include('admin.layouts.footer')
@endsection
