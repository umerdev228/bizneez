<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Bizneez') }}</title>
    <meta content="Coderthemes" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- App favicon -->
    <link rel="shortcut icon" href="{{asset('/theme/admin/assets/images/favicon.png')}}">
    <!-- Tooltipster css -->
    <link rel="stylesheet" href="{{ asset('/theme/admin/plugins/tooltipster/tooltipster.bundle.min.css') }}">
    <link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel = "stylesheet">

    <!-- Plugins css-->
    <link href="{{asset('/theme/admin/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{asset('/theme/admin/plugins/switchery/switchery.min.css')}}" />
    <link href="{{asset('/theme/admin/plugins/footable/css/footable.core.css')}}" rel="stylesheet">

    <!-- App css -->
    <link href="{{asset('/theme/admin/assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('/theme/admin/assets/css/icons.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('/theme/admin/assets/css/metismenu.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('/theme/admin/assets/css/style.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('/theme/admin/assets/css/metismenu.min.css')}}" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <script>var APP_URL = '{!! url('/') !!}';</script>

    <style>
        .screen-call{color:#fff;}
        .square {
            height: 150px;
            width: 200px;
            background-color: #555;
            margin:4px;
        }
        .screen-share{
            border: 4px solid #ff0000;
            width: 100%;
            min-height: 550px;
            background-color:#6d6969
        }
        .inner-box{font-size: 11px; }
        .inner-box a{color:#fff;}
        .active{border: 2px solid #fff;}

        .active:focus {
            outline: 0 !important;
        }
    </style>


</head>
<body style="background-color: rgb(49 49 49)">

<div id="app">
    <div class="screen-call">

            <video-chat-webrtc-employee-to-demo
                    role="{{ $role }}"
                    :user="{{ $user }}"
                    :others="{{ $others }}"
                    :other="{{ $other }}"
                    pusher-key="{{ config('broadcasting.connections.pusher.key') }}"
                    pusher-cluster="{{ config('broadcasting.connections.pusher.options.cluster') }}"

                    turn_url="{{ env('TURN_SERVER_URL') }}"
                    turn_username="{{ env('TURN_SERVER_USERNAME') }}"
                    turn_credential="{{ env('TURN_SERVER_CREDENTIAL') }}"
            ></video-chat-webrtc-employee-to-demo>

    </div>
    {{--    <div class="row h-100">--}}
    {{--        <div class="col-md-4 left-side">--}}

    {{--            <div class="row mt-4">--}}
    {{--                <div class="col-md-6"><div class="square">--}}
    {{--                        <div class="inner-box">--}}
    {{--                            <p>Admin video</p>--}}
    {{--                        </div>--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--                @foreach($auth_code as $auth)--}}
    {{--                    <div class="col-md-6">--}}
    {{--                        <div class="square">--}}
    {{--                            <div class="inner-box">--}}
    {{--                                <p>Access Code: {{$auth->code}}</p>--}}
    {{--                                <p>Screen: Demo {{$auth->id}}</p>--}}
    {{--                                <p>--}}
    {{--                                    <a href=''>bizneez.net/live-demo-{{$auth->code}}</a>--}}
    {{--                                </p>--}}
    {{--                            </div>--}}
    {{--                        </div>--}}
    {{--                    </div>--}}
    {{--                @endforeach--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--        <div class="col-md-6  mt-4">--}}
    {{--            <h3>Shared Screen</h3>--}}
    {{--            <div class="screen-share w-100 h-100">--}}
    {{--                <div class="inner-box" id="inner-box">--}}
    {{--                    <p>Access Code: 209xxx</p>--}}
    {{--                    <p>Screen: Demo5</p>--}}
    {{--                    <p><a href=''>bizneez.net/live-demo-209xxx</a></p>--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--    </div>--}}
</div>
<script src="{{asset('js/app.js')}}"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="{{asset('/theme/admin/assets/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('/theme/admin/assets/js/metisMenu.min.js')}}"></script>
<script src="{{asset('/theme/admin/assets/js/waves.js')}}"></script>
<script src="{{asset('/theme/admin/assets/js/jquery.slimscroll.js')}}"></script>
<script src="{{asset('/theme/admin/plugins/footable/js/footable.all.min.js')}}"></script>
<script src="{{asset('/theme/admin/plugins/switchery/switchery.min.js')}}"></script>
<script src="{{asset('/theme/admin/plugins/bootstrap-tagsinput/js/bootstrap-tagsinput.min.js')}}"></script>
<script src="{{asset('/theme/admin/plugins/select2/js/select2.min.js')}}" type="text/javascript"></script>
<script src="{{asset('/theme/admin/plugins/bootstrap-select/js/bootstrap-select.js')}}" type="text/javascript"></script>
<script src="{{asset('/theme/admin/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js')}}" type="text/javascript"></script>
<script src="{{asset('/theme/admin/plugins/bootstrap-maxlength/bootstrap-maxlength.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{asset('/theme/admin/plugins/autocomplete/jquery.autocomplete.min.js')}}"></script>
<script type="text/javascript" src="{{asset('/theme/admin/plugins/autocomplete/countries.js')}}"></script>
<script type="text/javascript" src="{{asset('/theme/admin/assets/pages/jquery.autocomplete.init.js')}}"></script>
<script type="text/javascript" src="{{asset('/theme/admin/assets/pages/jquery.form-advanced.init.js')}}"></script>
<script src="{{asset('/theme/admin/assets/js/jquery.core.js')}}"></script>
<script src="{{asset('/theme/admin/assets/js/jquery.app.js')}}"></script>
{{--    <script src="{{asset('/theme/admin/plugins/autocomplete/jquery.mockjax.js')}}"></script>--}}
<script src="{{asset('/theme/admin/assets/js/modernizr.min.js')}}"></script>
<script src="{{asset('/theme/admin/plugins/counterup/jquery.counterup.min.js')}}"></script>
<script src="{{asset('/theme/admin/plugins/waypoints/lib/jquery.waypoints.min.js')}}"></script>
<script src="{{asset('/theme/admin/plugins/jquery-knob/excanvas.js')}}"></script>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />



</body>
</html>
