<?php

use Illuminate\Database\Seeder;

class SiteSettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\SiteSetting::create([
            'key' => 'cc_email',
            'data' => 'admin@mail.com'
        ]);
        \App\SiteSetting::create([
            'key' => 'side_bar_color',
            'data' => '#b8ffe0'
        ]);
        \App\SiteSetting::create([
            'key' => 'header_color',
            'data' => '#b8ffe0'
        ]);
        \App\SiteSetting::create([
            'key' => 'header_logo',
            'data' => '/uploads/logo/1615361798.png'
        ]);
        \App\SiteSetting::create([
            'key' => 'text_color',
            'data' => '#000000'
        ]);
        \App\SiteSetting::create([
            'key' => 'facebook_link',
            'data' => 'www.google.com'
        ]);
        \App\SiteSetting::create([
            'key' => 'instagram_link',
            'data' => 'www.google.com'
        ]);
        \App\SiteSetting::create([
            'key' => 'twitter_link',
            'data' => 'www.google.com'
        ]);
        \App\SiteSetting::create([
            'key' => 'linkedIn_link',
            'data' => 'www.google.com'
        ]);
    }
}
