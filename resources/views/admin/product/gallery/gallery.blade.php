<button type="button" class="btn btn-custom btn-rounded w-md waves-effect waves-light float-right" data-toggle="modal" data-target=".bs-example-modal-lg">
    <i class="mdi mdi-upload"></i> Upload Files</button>
<h4 class="header-title m-b-30">Product Images Gallery</h4>
<div class="col-lg-12">

    <div class="row">
        <div class="col-lg-9">
            <div class="row" id="range-images">
                {{--            @foreach($media as $image)--}}
                {{--                <div class="col-lg-3">--}}
                {{--                    <div class="">--}}
                {{--                        <a href="{{ route('gallery.products.images.delete', ['id' => $image->id]) }}" class="file-close">--}}
                {{--                            <i class="mdi mdi-close-circle"></i>--}}
                {{--                        </a>--}}
                {{--                        <div class="file-img-box">--}}
                {{--                            <img width="200px" src="{{ asset($image->path) }}" >--}}
                {{--                        </div>--}}
                {{--                        <div class="file-man-title">--}}
                {{--                        </div>--}}
                {{--                    </div>--}}
                {{--                </div>--}}
                {{--            @endforeach--}}
            </div>
        </div>
        <div class="col-lg-3">

            <select onchange="manufacturerChange()" name="manufacturer" id="manufacturer" class="form-control">
                <option>--Select Manufacture--</option>
                @foreach($manufacturers as $manufacturer)
                    <option value="{{$manufacturer->id}}">{{ $manufacturer->name }}</option>
                @endforeach
            </select> <br>

            <select onchange="rangeChange()" name="ranges" id="ranges" class="form-control">
                <option>--Select Ranges--</option>
            </select> <br>

            <div class="mt-3">
                <div class="form-group">
                    <label for="keywords">Keyword</label>
                    <input class="form-control" type="text" name="keywords" id="keywords" required>
                </div>

                <div class="form-group">
                    <label for="title">Title</label>
                    <input class="form-control" type="text" name="title" id="title" required>
                </div>

                <div class="form-group">
                    <label for="image_description">Description</label>
                    <input class="form-control" type="text" name="image_description" id="image_description" required>
                </div>
            </div>
        </div>

    </div>
    <div class="form-group mt-3">
        <input class="btn btn-primary" type="submit" value="Create Product">
        <button onclick="history.back()" type="button" class="btn">Cancel</button>
    </div>
</div>


<script !src="">

    let selectedImages = [];

    function manufacturerChange(){
        let sec = document.getElementById('maufacturer-images')
        let id = document.getElementById('manufacturer').value
        axios.post(APP_URL+'/getImagesBymanufacturer', {id: id}).then(response => {
            console.log(response.data)
            if (response.data.type === 'success') {
                response.data.ranges.forEach(function (range) {
                    console.log(range)
                    let ele =
                        `<option value="`+range.id+`">`+range.name+`</option>`
                    $('#ranges').append(ele)
                })
            }
        });
    }

    function rangeChange(){
        let sec = document.getElementById('maufacturer-images')
        let id = document.getElementById('ranges').value
        axios.post(APP_URL+'/getImagesByRange', {id: id}).then(response => {
            console.log(response.data)
            if (response.data.type === 'success') {
                $('#range-images').empty()
                response.data.images.forEach(function (image) {
                    console.log(image)
                    let ele =  `
                                <div id="`+image.id+`" onclick="onSelectImage(`+image.id+`)" class="col-lg-3 col-xl-2 border-success">
                                    <img width="100px" src="`+APP_URL+`/`+image.path+`" alt="icon" class="p-2">
                                </div>`
                    $('#range-images').append(ele)
                })
            }
        });
    }

    function onSelectImage(id) {

        if (selectedImages.includes(id)) {
            let index = selectedImages.indexOf(id)
            selectedImages.splice(index, 1);
            $('#'+id).css('border', '')
        }
        else {
            selectedImages.push(id)
            $('#'+id).css('border', '2px solid')
        }
        $('#product_images').val(selectedImages)
    }
</script>