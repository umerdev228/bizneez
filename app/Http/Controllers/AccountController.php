<?php

namespace App\Http\Controllers;

use App\Account;
use Illuminate\Http\Request;

class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $breadcrumb = 'Admin / Account ';
        $accounts = Account::all();
        return view('admin.account.index', compact('accounts', 'breadcrumb'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $breadcrumb = 'Admin / Account / Create';
        return view('admin.account.create', compact('breadcrumb'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $lead = Account::create([
            "name" => $request->name,
            "type" => $request->type,
            "date_time" => $request->date_time,
            "amount" => $request->amount,
            "details" => $request->details,
            "email" => $request->email,
            "amount_type" => $request->amount_type,
        ]);


        toastr()->success('Account Created Successfully');
        return redirect()->route('admin.accounts.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Account  $account
     * @return \Illuminate\Http\Response
     */
    public function show(Account $account)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Account  $account
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $breadcrumb = 'Admin / Account / Edit ';
        $account = Account::where('id', $id)->first();

        return view('admin.account.edit', compact('account', 'breadcrumb'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Account  $account
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $lead = Account::where('id', $id)->update([
            "name" => $request->name,
            "type" => $request->type,
            "date_time" => $request->date_time,
            "amount" => $request->amount,
            "details" => $request->details,
            "email" => $request->email,
            "amount_type" => $request->amount_type,
        ]);
        toastr()->success('Account Updated Successfully');
        return redirect()->route('admin.accounts.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Account  $account
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Account::where('id', $id)->delete();
        toastr()->success('Account Deleted Successfully');
        return redirect()->route('admin.accounts.index');
    }
}
