@extends('admin.layouts.app')
@section('content')
    @include('admin.layouts.topbar')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    @if(session()->has('message'))
                        <div class="alert alert-success mt-5" id="myElem" >
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-12 mt-5">
                            <div class="card">
                                <div class="card-body">
                                    <a href="{{route('admin.manufacturers.create')}}"  class="btn btn-primary" ><i class="fa fa-plus"></i>Add New Manufacturer </a>
                                </div>
                                <div class="card-body">
                                    <table class="table table-hover m-0 tickets-list table-actions-bar dt-responsive nowrap" width="100%" id="normal-table">
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Profile</th>
                                            <th>Email</th>
                                            <th>Phone</th>
                                            <th>Detail</th>
                                            <th>Created By</th>
                                            <th data-orderable="false">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($manufacturers as $manufacturer)
                                            <tr>
                                                <td>{{$manufacturer->name}}</td>
                                                <td><img src="{{asset($manufacturer->profile->image)}}" width="100"></td>
                                                <td>{{$manufacturer->email}}</td>
                                                <td>{{$manufacturer->profile->phone}}</td>
                                                <td>{{$manufacturer->profile->description}}</td>
                                                <td>{{$manufacturer->user !== null ? $manufacturer->user->name : null}}</td>
                                                <td>
                                                    <div class="btn-group">
                                                        <a class="btn" href="{{ route('admin.manufacturers.edit', ['id' => $manufacturer->id] ) }}">
                                                            <i class="mdi mdi-pencil"></i>
                                                        </a>
                                                        <a class="btn" href="{{ route('admin.manufacturers.delete', ['id' => $manufacturer->id] ) }}">
                                                            <i class="fa fa-close"></i>
                                                        </a>
                                                        <a class="btn" href="{{ route('admin.manufacturers.show', ['id' => $manufacturer->id, 'role' => Auth::user()->roles[0]->name] ) }}">
                                                            <i class="mdi mdi-eye"></i>
                                                        </a>
                                                    </div>

                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div> <!-- end card -->
                        </div><!-- end col -->
                    </div>

                </div>
            </div>

        </div> <!-- container -->
    </div> <!-- content -->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

    @include('admin.layouts.footer')
@endsection
