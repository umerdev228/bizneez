<?php

namespace App\Http\Controllers;

use App\Company;
use App\Media;
use App\Product;
use Illuminate\Http\Request;
use App\User;
use App\Category;
use \DB;

class DesignController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::with('messages')->orderBy("created_at", "DESC")->get();
        $media = Media::where('name', 'gallery')->get();

        $companies = Company::all();
        $categories = Category::all();
        $breadcrumb = 'Dashboard / Design & Build';
        $products  = DB::table('mdd_price_list_csv')->get(); 
        return view('admin.technology.design_and_build', compact('users', 'companies',  'breadcrumb', 'media' ,'products' , 'categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        $companies = Company::all();
        $categories = Category::all();
        $breadcrumb = 'Dashboard / Design and Build';
        $products  = DB::table('mdd_price_list_csv')->get();
        return view('admin.technology.create', compact( 'companies',  'breadcrumb' ,'products' , 'categories'));

    }

    public function show(Request $request)
    {
        //
        $images = [];
        $data = $request->all();
        $count = $data['rows'];

        $companies = Company::all();
        $company = Company::where('id', $request->company_id)->first();
        if($request->hasfile('files'))
        {

            foreach($request->file('files') as $image)
            {
                $name=$image->getClientOriginalName();
                $image->move(public_path().'/images/', $name);
                $images[] = '/images/'.$name;
            }
        }
        $breadcrumb = 'Dashboard / Design Build / Show';
        return view('admin.advertisement.preview', compact('breadcrumb', 'data', 'count', 'companies', 'company', 'images'));

    }
 
}
