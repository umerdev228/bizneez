<div class="navbar navbar-fixed-top">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#main-nav" aria-expanded="true">
        <span class="sr-only">Menu</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        </button>
        <a href="#top" class="navbar-title">
              <img width="200" src="{{$header_logo}}" alt=""> </a>
           <!--  <img width="200" src="https://bizneez.net//uploads/logo/1614849038.png" alt=""></a>  -->
        <div class="tag-line">Together we work better for a brighter future</div>
    </div>
    <nav class="navbar-collapse collapse" id="main-nav" aria-expanded="true" style="">
        <form id="asw-login-form" name="login" method="POST" action="{{url('login')}}">   @csrf
            <h2 class="login-heading" align="center">Login</h2>
            <div class="row login">
                <div class="col-md-4"> <input id="email" placeholder="Email" name="email" class="form-control required input-text" tabindex="1" type="email">
                @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror</div>
            <div class="col-md-4"> <input id="password" placeholder="Password" name="password" class="form-control required input-text" tabindex="2" type="password">
            <a  data-toggle="modal" data-target="#exampleModal" href="javascript:void(0)" class="text-white"  >Forgot Password</a>
            @error('password')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror</div>
        <div class="col-md-4">  <button  type="submit" class="btn2"  >Login</button></div>

    </div>
</form>
</nav>
</div>
