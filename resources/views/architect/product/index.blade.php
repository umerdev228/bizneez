@extends('admin.layouts.app')
@section('content')
    @include('admin.layouts.topbar')

    <style>
        h3.h3{text-align:center;margin:1em;text-transform:capitalize;font-size:1.7em;}

        /********************* shopping Demo-1 **********************/
        .product-grid{font-family:Raleway,sans-serif;text-align:center;padding:0 0 72px;border:1px solid rgba(0,0,0,.1);overflow:hidden;position:relative;z-index:1}
        .product-grid .product-image{position:relative;transition:all .3s ease 0s}
        .product-grid .product-image a{display:block}
        .product-grid .product-image img{width:auto;height:200px}
        .product-grid .pic-1{opacity:1;transition:all .3s ease-out 0s}
        .product-grid:hover .pic-1{opacity:1}
        .product-grid .pic-2{opacity:0;position:absolute;top:0;left:0;transition:all .3s ease-out 0s}
        .product-grid:hover .pic-2{opacity:1}
        .product-grid .social{width:150px;padding:0;margin:0;list-style:none;opacity:0;transform:translateY(-50%) translateX(-50%);position:absolute;top:60%;left:50%;z-index:1;transition:all .3s ease 0s}
        .product-grid:hover .social{opacity:1;top:50%}
        .product-grid .social li{display:inline-block}
        .product-grid .social li a{color:#fff;background-color:#333;font-size:16px;line-height:40px;text-align:center;height:40px;width:40px;margin:0 2px;display:block;position:relative;transition:all .3s ease-in-out}
        .product-grid .social li a:hover{color:#fff;background-color:#ef5777}
        .product-grid .social li a:after,.product-grid .social li a:before{content:attr(data-tip);color:#fff;background-color:#000;font-size:12px;letter-spacing:1px;line-height:20px;padding:1px 5px;white-space:nowrap;opacity:0;transform:translateX(-50%);position:absolute;left:50%;top:-30px}
        .product-grid .social li a:after{content:'';height:15px;width:15px;border-radius:0;transform:translateX(-50%) rotate(45deg);top:-20px;z-index:-1}
        .product-grid .social li a:hover:after,.product-grid .social li a:hover:before{opacity:1}
        .product-grid .product-discount-label,.product-grid .product-new-label{color:#fff;background-color:#ef5777;font-size:12px;text-transform:uppercase;padding:2px 7px;display:block;position:absolute;top:10px;left:0}
        .product-grid .product-discount-label{background-color:#333;left:auto;right:0}
        .product-grid .rating{color:#FFD200;font-size:12px;padding:12px 0 0;margin:0;list-style:none;position:relative;z-index:-1}
        .product-grid .rating li.disable{color:rgba(0,0,0,.2)}
        .product-grid .product-content{background-color:#fff;text-align:center;padding:12px 0;margin:0 auto;position:absolute;left:0;right:0;bottom:-27px;z-index:1;transition:all .3s}
        .product-grid:hover .product-content{bottom:0}
        .product-grid .title{font-size:13px;font-weight:400;letter-spacing:.5px;text-transform:capitalize;margin:0 0 10px;transition:all .3s ease 0s}
        .product-grid .title a{color:#828282}
        .product-grid .title a:hover,.product-grid:hover .title a{color:#ef5777}
        .product-grid .price{color:#333;font-size:17px;font-family:Montserrat,sans-serif;font-weight:700;letter-spacing:.6px;margin-bottom:8px;text-align:center;transition:all .3s}
        .product-grid .price span{color:#999;font-size:13px;font-weight:400;text-decoration:line-through;margin-left:3px;display:inline-block}
        .product-grid .add-to-cart{color:#000;font-size:13px;font-weight:600}
        @media only screen and (max-width:990px){.product-grid{margin-bottom:30px}
        }

        .fa-eye, .fa-eye-slash {
            border: none;
            padding: 9px 13px;
            background: #0000 !important;
        }
    </style>

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    @if(session()->has('message'))
                        <div class="alert alert-success mt-5" id="myElem" >
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            {{ session()->get('message') }}
                        </div>
                    @endif
                        <div class="row">
                            <div class="col-12 mt-5">
                                <div class="card-header">
                                    Categories Filter
                                </div>
                                <div class="card">
                                    <div class="">
                                        <div class="row">
                                            @foreach($categories as $category)
                                                <div class="p-5">
                                                    <div class="dropdown">
                                                        @if(count($category->childs) > 0)
                                                            <a href="{{ route('admin.product.filter.by.category', ['id' => $category->id]) }}" class="btn btn-tumblr dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                {{ $category->name }}
                                                            </a>
                                                        @else
                                                            <a class="btn btn-tumblr" href="{{ route('admin.product.filter.by.category', ['id' => $category->id]) }}">{{ $category->name }}</a>
                                                        @endif
                                                        @if(count($category->childs) > 0)
                                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                                <a class="dropdown-item" href="{{ route('admin.product.filter.by.category', ['id' => $category->id]) }}">{{ $category->name }}</a>
                                                                @foreach($category->childs as $child)
                                                                    <a class="dropdown-item" href="{{ route('admin.product.filter.by.category', ['id' => $child->id]) }}">{{ $child->name }}</a>
                                                                @endforeach
                                                            </div>
                                                        @endif
                                                    </div>
                                                    {{--                                                <a href="{{ route('admin.product.filter.by.category', ['id' => $category->id]) }}" onclick="" class="btn btn-default btn-outline-dark">{{ $category->name }}</a>--}}
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <div class="row">
                        <div class="col-12 mt-5">
                            <div class="card">
                                <div class="mt-5">
                                    <div class="row">
                                        @foreach($products as $product)
                                            <div class="col-3 mt-5 ml-5">
                                                <div class="product-grid">
                                                    <div class="product-image">
                                                        <a href="#">
                                                            @foreach($product->media as $index => $media)
                                                                @if($index < 2)
                                                                    <img class="pic-{{ $index + 1 }}" src="{{ asset($media->path) }}">
                                                                @endif
                                                            @endforeach
                                                        </a>
                                                        <ul class="social">
{{--                                                            <li><a href="{{ route('employee.products.show', ['id' => $product->id]) }}" data-tip="View"><i class="fa fa-eye"></i></a></li>--}}
{{--                                                            <li><a href="" data-tip="Add to Wishlist"><i class="fa fa-shopping-bag"></i></a></li>--}}
{{--                                                            <li><a href="javascript:void(0)" onclick="addToCart({{$product->id}})" data-tip="Add to Cart"><i class="fa fa-shopping-cart"></i></a></li>--}}
                                                        </ul>
{{--                                                        <span class="product-new-label">Sale</span>--}}
                                                        <span class="product-discount-label"><span>&#163;</span>{{ $product->discount }}</span>
                                                    </div>
                                                    <ul class="rating">
                                                        <li class="fa fa-star"></li>
                                                        <li class="fa fa-star"></li>
                                                        <li class="fa fa-star"></li>
                                                        <li class="fa fa-star"></li>
                                                        <li class="fa fa-star disable"></li>
                                                    </ul>
                                                    <div class="product-content">
                                                        <h3 class="title"><a href="{{ route('employee.products.show', ['id' => $product->id]) }}">{{ $product->name }}</a></h3>
                                                        <div class="price">
                                                            <span>&#163;</span>{{ $product->price }}
{{--                                                            <span><span>&#163;</span>{{ $product->second_price }}</span>--}}
                                                        </div>
                                                        <a class="add-to-cart btn btn-info" href="javascript:void(0)" onclick="addToCart({{$product->id}})">+ Add To Quotation</a>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div><!-- end col -->
                        </div><!-- end col -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--    @include('admin.layouts.footer')--}}
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script>
        const CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        function addToCart(id) {
            $.ajax({
                type:'POST',
                url:"{{ route('add.to.cart') }}",
                data:{_token: CSRF_TOKEN, id:id},
                success:function(response){
                    toastr.success('Added Successfully')
                }
            });
        }
    </script>
@endsection
