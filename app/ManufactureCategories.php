<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ManufactureCategories extends Model
{
    
    protected $table = 'manufacture_categories';
    protected $guarded = [];

     protected $fillable = [
        'category_name', 'manufacture_id',  
    ];
}
