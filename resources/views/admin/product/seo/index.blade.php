@extends('admin.layouts.app')
@section('content')

    <div class="content" style="margin-top: 100px;">
        <div class="container-fluid">
            <div class="mt-5">
                <div class="col-lg-12 mt-5">
{{--                    <button type="button" class="btn btn-custom btn-rounded w-md waves-effect waves-light float-right" data-toggle="modal" data-target=".bs-example-modal-lg">--}}
{{--                        <i class="mdi mdi-upload"></i> Upload Files</button>--}}
                    <h4 class="header-title m-b-30">Product Images Gallery</h4>
                    <div class="col-lg-12">
                        <form action="{{route('admin.product.images.seo')}}" method="post">
                        @csrf
                        <input type="text" style="display:none;" name="product_images" id="product_images">
                        <div class="row">
                            <div class="col-lg-9">
                                <div class="row" id="range-images"></div>
                            </div>
                            <div class="col-lg-3">

                                <select onchange="manufacturerChange()" name="manufacturer" id="manufacturer" class="form-control">
                                    <option>--Select Manufacture--</option>
                                    @foreach($manufacturers as $manufacturer)
                                        <option value="{{$manufacturer->id}}">{{ $manufacturer->name }}</option>
                                    @endforeach
                                </select> <br>

                                <select onchange="rangeChange()" name="ranges" id="ranges" class="form-control">
                                    <option>--Select Ranges--</option>
                                </select> <br>

                                <div class="mt-3">
                                    <div class="form-group">
                                        <label for="keywords">Keyword</label>
                                        <input class="form-control" type="text" name="keywords" id="keywords" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="title">Title</label>
                                        <input class="form-control" type="text" name="title" id="title" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="image_description">Description</label>
                                        <input class="form-control" type="text" name="image_description" id="image_description" required>
                                    </div>
                                    <div class="form-group mt-3">
                                        <input class="btn btn-primary" type="submit" value="Save">
                                        <button onclick="history.back()" type="button" class="btn">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


<script !src="">

    let selectedImages = [];
    let lastSelectedImage = 0;
    let imagesData = [];

    function manufacturerChange(){
        let sec = document.getElementById('maufacturer-images')
        let id = document.getElementById('manufacturer').value
        axios.post(APP_URL+'/getImagesBymanufacturer', {id: id}).then(response => {
            console.log(response.data)
            if (response.data.type === 'success') {
                response.data.ranges.forEach(function (range) {
                    console.log(range)
                    let ele =
                        `<option value="`+range.id+`">`+range.name+`</option>`
                    $('#ranges').append(ele)
                })
            }
        });
    }

    function rangeChange(){
        let sec = document.getElementById('maufacturer-images')
        let id = document.getElementById('ranges').value
        axios.post(APP_URL+'/getImagesByRange', {id: id}).then(response => {
            console.log(response.data)
            if (response.data.type === 'success') {
                $('#range-images').empty()
                imagesData = []
                imagesData = response.data.images
                response.data.images.forEach(function (image) {
                    console.log(image)
                    let ele =  `
                                <div id="`+image.id+`" onclick="onSelectImage(`+image.id+`)" class="col-lg-3 col-xl-2 border-success">
                                    <img width="100px" src="`+APP_URL+`/`+image.path+`" alt="icon" class="p-2">
                                </div>`
                    $('#range-images').append(ele)
                })
            }
        });
    }

    function onSelectImage(id) {
        $('#keywords').val()
        $('#title').val()
        $('#image_description').val()


        let current_image = imagesData.find(image => image.id === id)
        console.log(current_image)
        if (lastSelectedImage !== '') {
            $('#'+lastSelectedImage).css('border', '')
        }
        lastSelectedImage = id
        $('#'+id).css('border', '2px solid green')
        $('#product_images').val(id)
        $('#keywords').val(current_image.keywords)
        $('#title').val(current_image.title)
        $('#image_description').val(current_image.description)


    }
</script>


@endsection