@extends('admin.layouts.app')
@section('content')
    @include('admin.layouts.topbar')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    @if(session()->has('message'))
                        <div class="alert alert-success mt-5" id="myElem" >
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-12 mt-5">
                            <div class="card">
                                <div class="card-body">
                                    <a href="{{route('manufacturer.categories.create')}}"  class="btn btn-primary" ><i class="fa fa-plus"></i>Add New Category </a>
                                </div>
                                <div class="card-body">
                                    <table class="table table-hover m-0 tickets-list table-actions-bar dt-responsive nowrap" width="100%" id="normal-table">
                                        <thead>
                                        <tr>
                                            <th>Image</th>
                                            <th>Name</th>
                                            <th>Description</th>
                                            <th>Created </th>
                                            <th data-orderable="false">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($categories as $category)
                                            <tr>
                                                <td>
                                                    <img src="{{asset($category->image)}}" alt="Category Image" width="50">
                                                </td>
                                                <td>{{$category->name}}</td>
                                                <td>{{$category->description}}</td>
                                                <td>{{$category->created_at}}</td>
                                                <td>
                                                    <div class="btn-group">
                                                        <a class="btn" href="{{ route('manufacturer.categories.edit', ['id' => $category->id] ) }}">
                                                            <i class="mdi mdi-pencil"></i>
                                                        </a>
                                                        <a class="btn" href="{{ route('manufacturer.categories.delete', ['id' => $category->id] ) }}">
                                                            <i class="fa fa-close"></i>
                                                        </a>
                                                        <a class="btn" href="{{ route('manufacturer.categories.show', ['id' => $category->id, 'role' => Auth::user()->roles[0]->name] ) }}">
                                                            <i class="mdi mdi-eye"></i>
                                                        </a>
                                                    </div>

                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div><!-- end col -->
                    </div>
                </div>
            </div>
        </div> <!-- container -->
    </div> <!-- content -->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    @include('admin.layouts.footer')
@endsection
