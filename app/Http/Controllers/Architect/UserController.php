<?php

namespace App\Http\Controllers\Architect;

use App\Http\Controllers\Controller;
use App\Packages;
use App\Profile;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit() {
        $user = User::where('id', 1)->first();
        return view('admin.profile.edit', ['user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        User::where('id', Auth::id())->update([
            "name" => $request->first_name . ' ' . $request->middle_name . ' ' . $request->last_name,
            "first_name" => $request->first_name,
            "middle_name" => $request->middle_name,
            "last_name" => $request->last_name,
            "email" => $request->email,
        ]);

        Profile::where('user_id', Auth::id())->update([
            "description" => $request->description,
            "phone" => $request->phone,
            "mobile" => $request->mobile,
            "assistant_number" => $request->assistant_number,
            "address" => $request->address,
        ]);
        toastr()->success('Profile Updated Successfully');
        return redirect()->route('admin.account');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function password(Request $request){
        $user = User::where('id', Auth::id())->with('profile')->first();
        return view('admin.profile.password', ['user' => $user]);
    }


    public function passwordUpdate(Request $request){
        $user = User::where('id', Auth::id())->first();
        if(!Hash::check($request->current_password, $user->password)) {
            toastr()->error('Password not matching');
            return redirect()->back()->with('error', 'The specified password does not match the database password');
        }
        else {
            request()->validate([
                'password' => 'required|min:3|confirmed',
            ]);
            $user->password = Hash::make($request->password);
            $user->save();
        }
        toastr()->success('Password Updated Successfully');
        return redirect('admin/account')->with('message', 'Your password updated successfully.');
    }
}
