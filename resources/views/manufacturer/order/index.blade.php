@extends('admin.layouts.app')
@section('content')
    @include('admin.layouts.topbar')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">

                    @if(session()->has('message'))
                        <div class="alert alert-success mt-5" id="myElem" >
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-12 mt-5">
                            <div class="card">
                                <div class="card-body">
                                    <table class="table table-hover m-0 tickets-list table-actions-bar dt-responsive nowrap" id="normal-table">
                                        <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Products</th>
                                            <th>Status </th>
                                            <th>Order By </th>
                                            <th>Created </th>
                                            <th data-orderable="false">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($orders as $order)
                                            <tr>
                                                <td>{{$order->id}}</td>
                                                <td>{{count($order->order->products)}}</td>
                                                <td>{{$order->order->status}}</td>
                                                <td>{{$order->architect->name}}</td>
                                                <td>{{$order->created_at}}</td>
                                                <td>
                                                    <div class="btn-group">
                                                        <a class="btn btn-dark" href="{{ route('manufacturer.orders.show', ['id' => $order->order_id] ) }}">
                                                            View Products
                                                        </a>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        <tfoot>
                                        <tr>
                                            <th>Id</th>
                                            <th>Products</th>
                                            <th>Status </th>
                                            <th>Order By </th>
                                            <th>Created </th>
                                            <th data-orderable="false">Action</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('admin.layouts.footer')
@endsection
