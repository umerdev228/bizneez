@extends('admin.layouts.app')
@section('content')
    @include('admin.layouts.topbar')
    <!-- Google Chart JS -->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <!-- Start Page content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if(session()->has('message'))
                        <div class="alert alert-success" style="width:803px ">
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-9 mt-5">
                            <div class="card-box">
                                <form method="POST" action="{{ route('admin.accounts.update', ['id' => $account->id]) }}" enctype="multipart/form-data">
                                    @csrf
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="name">Name<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control valid" name="name" value="{{$account->name}}" id="name" required />
                                                @if ($errors->has('name'))
                                                    <span class="error">{{ $errors->first('name') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="type">Type<span class="text-danger">*</span></label>
                                                <select class="form-control form-check" name="type" id="type">
                                                    <option @if($account->type === 'employee') selected @endif value="employee">Employee</option>
                                                    <option @if($account->type === 'manufacturer') selected @endif value="manufacturer">Client</option>
                                                    <option @if($account->type === 'manufacturer') selected @endif value="manufacturer">Manufacturer</option>
                                                </select>
                                                @if ($errors->has('type'))
                                                    <span class="error">{{ $errors->first('type') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="date_time">Date Time<span class="text-danger">*</span></label>
                                                <input type="datetime-local" class="form-control valid" name="date_time" value="{{$account->date_time}}" id="date_time" required />
                                                @if ($errors->has('date_time'))
                                                    <span class="error">{{ $errors->first('date_time') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="email">Email<span class="text-danger">*</span></label>
                                                <input type="email" class="form-control valid" name="email" id="email" value="{{$account->email}}" required />
                                                @if ($errors->has('email'))
                                                    <span class="error">{{ $errors->first('email') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="amount"> Amount </label>
                                                <input type="text" class="form-control valid" name="amount" value="{{$account->amount}}" id="amount" >
                                                @if ($errors->has('amount'))
                                                    <span class="error">{{ $errors->first('amount') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="amount_type">Amount Type<span class="text-danger">*</span></label>
                                                <select class="form-control form-check" name="amount_type" id="amount_type">
                                                    <option  @if($account->type === 'debit') selected @endif value="debit">Debit</option>
                                                    <option  @if($account->type === 'credit') selected @endif value="credit">Credit</option>
                                                </select>
                                                @if ($errors->has('amount_type'))
                                                    <span class="error">{{ $errors->first('amount_type') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="details">Details<span class="text-danger">*</span></label>
                                                <textarea id="details" class="form-control rounded-0" name="details" rows="5">{{$account->details}}</textarea>
                                                @if ($errors->has('details'))
                                                    <span class="error">{{ $errors->first('details') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group mt-3">
                                        <input class="btn btn-primary" type="submit" value="Update Account">
                                        <button onclick="history.back()" type="button" class="btn">Cancel</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
