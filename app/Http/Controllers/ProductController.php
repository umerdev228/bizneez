<?php

namespace App\Http\Controllers;

use App\Category;
use App\Media;
use App\Product;
use App\ProductCategory;
use App\ProductMedia;
use App\Size;
use App\User;
use Illuminate\Support\Str;
use PhpParser\Node\Expr\Array_;
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use \DB;

class ProductController extends Controller
{
    public function __construct()
    {

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $breadcrumb = ucfirst(Auth::user()->roles[0]->name)." / Product ";
     
 $data  = DB::table('book12')->get(); 

/*foreach($data as $prod){
 

    $product = Product::create([
                'sku'=>$prod->COL_1,
        'description'=> $prod->COL_2,
        'price' => $prod->COL_3,
        'second_price' =>$prod->COL_4,
        'discount' => $prod->COL_5,
        'name' => 'Mobel Linea',
            ]);


}
dd($product);
*/

        $products = Product::with('media')->with('categories')->with('manufacturer')->orderBy('created_at', 'Asc')->get();

        return view('admin.product.index', compact('products', 'breadcrumb'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $breadcrumb = ucfirst(Auth::user()->roles[0]->name)." / Product / New";

        $categories = Category::all();
        $media = Media::where('name', 'gallery')->get();
        $manufacturers = User::role('manufacturer')->with('ranges')->get();
        return view('admin.product.create', compact('categories', 'breadcrumb', 'media', 'manufacturers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $myString = $request->product_images;
        $productImages = explode(',', $myString);

        $productColorImages = json_decode(\request()->session()->get('product-color-images'));
        $check = Product::where('slug', Str::slug($request->name))->get();
        if (!count($check) > 0) {
            $product = Product::create([
                'name' => $request->name,
                'slug' => Str::slug($request->name),
                'description' => $request->description,
                'price' => $request->price,
                'second_price' => $request->second_price,
                'discount' => $request->discount,
                'quantity' => $request->quantity,
                'color_type' => $request->color_type,
                'range' => $request->range,
                'user_id' => Auth::id(),
            ]);

            foreach ($request->categories as $category) {
                $categories = explode(',', $category);
                foreach ($categories as $category) {
                    ProductCategory::create([
                        'product_id' => $product->id,
                        'category_id' => $category,
                    ]);
                }
            }
            if (count($productColorImages) > 0) {
                foreach ($productColorImages as $productColorImage) {
                    Media::where('id', $productColorImage->id)->update([
                        'name' => $product->name,
                        'item_id' => $product->id,
                    ]);
                }
                \request()->session()->forget('product-color-images');
            }
            if (count($productImages) > 0) {
                foreach ($productImages as $productImage) {
                    Media::where('id', $productImage->id)->update([
                        'name' => $product->name,
                        'item_id' => $product->id,
                    ]);
                }
            }
            if ($request->has('size')) {
                $myString = $request->size;
                $sizes = explode(',', $myString);
                if (count($sizes) > 0) {
                    foreach ($sizes as $size) {
                        Size::create([
                            'product_id' => $product->id,
                            'name' => $size
                        ]);
                    }
                }
            }
            \request()->session()->forget('product-images');
        }

        toastr()->success('Product Created Successfully');
        return redirect()->route('admin.products.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $breadcrumb = ucfirst(Auth::user()->roles[0]->name)." / Product / Show";
        $id = (\request()->route()->parameter('id'));
        $product = Product::where('id', $id)->with('colors')->with('media')->with('categories')->first();
//        dd($product->colors);
        return view('admin.product.show', compact('product', 'breadcrumb'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        //
        $breadcrumb = ucfirst(Auth::user()->roles[0]->name)." / Product / Edit";

        $id = (\request()->route()->parameter('id'));
        $product = Product::where('id', $id)->with('colors')->with('media')->with('categories')->first();
        $product_category = ProductCategory::where('product_id', $id)->pluck('category_id')->toArray();
        $categories = Category::all();
        $sizes = Size::where('product_id', $id)->get();
        $s = '';

        if (count($sizes) > 0) {
            foreach ($sizes as $index => $size) {
                $s .= $size->name;
                $s .= $index + 1 < count($sizes) ? ', ' : '';
            }
        }
        $sizes = $s;
        return view('admin.product.edit', compact('product', 'categories', 'product_category', 'breadcrumb', 'sizes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->has('size')) {
            Size::where('product_id', $id)->delete();
            $myString = $request->size;
            $sizes = explode(',', $myString);
            if (count($sizes) > 0) {
                foreach ($sizes as $size) {
                    Size::create([
                        'product_id' => $id,
                        'name' => $size
                    ]);
                }
            }
        }
        $product = Product::where('id', $id)->update([
            'name' => $request->name,
            'slug' => Str::slug($request->name),
            'description' => $request->description,
            'price' => $request->price,
            'discount' => $request->discount,
            'quantity' => $request->quantity,
        ]);
        ProductCategory::where('product_id', $id)->delete();
        foreach ($request->categories as $category) {
            $categories = explode(',', $category);
            foreach ($categories as $category) {
                ProductCategory::create([
                    'product_id' => $id,
                    'category_id' => $category,
                ]);
            }
        }
        $product = Product::where('id', $id)->first();
        $productImages = json_decode(\request()->session()->get('product-images'));
        $productColorImages = json_decode(\request()->session()->get('product-color-images'));
        if ($productColorImages) {
            if (count($productColorImages) > 0) {
                foreach ($productColorImages as $productColorImage) {
                    Media::where('id', $productColorImage->id)->update([
                        'name' => $product->name,
                        'item_id' => $id,
                    ]);
                }
                \request()->session()->forget('product-color-images');
            }
        }
        if ($productImages) {
            if (count($productImages) > 0) {
                foreach ($productImages as $productImage) {
                    Media::where('id', $productImage->id)->update([
                        'name' => $product->name,
                        'item_id' => $id,
                    ]);
                }
            }
        }
        \request()->session()->forget('product-images');
        toastr()->success('Product Updated Successfully');
        return redirect()->route('admin.products.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Product::where('id', $id)->delete();
        ProductCategory::where('product_id', $id)->delete();
        Media::where('item_id', $id)->where('item_type', 'product')->delete();
        toastr()->success('Product Deleted Successfully');
        return redirect()->back();
    }

    public function productImageCreateSession(Request $request) {

        $image = $request->file('file');
        $imageName = $image->getClientOriginalName();
        $image->move(public_path('uploads/images/products/'),$imageName);

        $media = Media::create([
            'name' => 'session',
            'item_id' => 'session',
            'item_type' => 'product',
            'path' => '/uploads/images/products/' . $imageName,
        ]);

        return response()->json(['type' => 'success', 'data' => $media->id]);

    }

    public function productColorCreateSession(Request $request) {
        dd($request->all());
//        Session::put('variableName', $value);
    }


    public function filterByCategory($id) {
        $breadcrumb = ucfirst(Auth::user()->roles[0]->name)." / Product / Index";
        $category = Category::where('id', $id)->with('products')->first();
        $products = $category->products;
        $categories = Category::where('parent', 0)->with('childs')->get();
        return view('admin.order.create', compact('products', 'breadcrumb', 'categories'));
    }

}
