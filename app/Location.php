<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    //
    protected $guarded = [];

    public function parent() {
        return $this->belongsTo(Location::class, 'parent_id');
    }

}
