<?php

namespace App\Imports;

use App\ProductRange;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ToModel;

class ProductRangeImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $products  = DB::table('mdd_price_list_csv')->insert([
            'COL1' => $row[0],
            'COL2' => $row[1],
            'COL3' => $row[2],
            'COL4' => $row[3],
            'COL5' => $row[4],
            'COL6' => $row[5],
            'COL7' => $row[6],
            'COL8' => $row[7],
            'COL9' => $row[8],
            'COL10' => $row[9],
            'COL11' => $row[10],
            'COL12' => $row[11],
            'COL13' => $row[12],
        ]);

    }
}
