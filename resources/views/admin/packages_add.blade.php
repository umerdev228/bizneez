@extends('admin.layouts.app')
@section('content')
@include('admin.layouts.topbar')
<!-- Google Chart JS -->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<!-- Start Page content -->
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">
        
@if(session()->has('message'))
    <div class="alert alert-success   mt-5 col-md-9" id="myElem" >
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
        {{ session()->get('message') }}
    </div>
@endif


        
@if(session()->has('error'))
    <div class="alert alert-danger   mt-5  col-md-9"  id="myElem">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
        {{ session()->get('error') }}
    </div>
@endif   

        <div class="row">
          <div class="col-9 mt-5">
            <div class="card-box">
              <form action="{{ url('admin/packages/store') }}" method="POST"> 
                {{ csrf_field() }}
                <div class="form-row">
                  <div class="form-group col-md-6">
                    <label for="FName">Package Name<span class="text-danger">*</span></label>
                    <input type="text" maxlength="20"  value="{{ old('name') }}" name="name" autocomplete="off"  class="form-control" parsley-trigger="change" required="required">
                      @if ($errors->has('name'))
                              <span class="error">{{ $errors->first('name') }}</span>
                      @endif   

                  </div>
              <!--     <div class="form-group col-md-6">
                    <label for="nickname">Nickname<span class="text-danger">*</span></label>
                    <input type="text" name="nickname" placeholder="yearly,monthly etc.." class="form-control" parsley-trigger="change" required="required">
                    @if ($errors->has('nickname'))
                              <span class="error">{{ $errors->first('nickname') }}</span>
                    @endif   
                  </div> -->


                   <div class="form-group col-md-6">
                    <label for="Price">Price<span class="text-danger">*</span></label>
                    <input type="text" name="price" value="{{ old('price') }}" autocomplete="off" maxlength="35" class="form-control" parsley-trigger="change" required="required" pattern="[0-9]+(\.[0-9]{1,2})?%?"/>
                      @if ($errors->has('price'))
                              <span class="error">{{ $errors->first('price') }}</span>
                      @endif   
                  </div>
               </div>
                <div class="form-row ">
                  <div class="form-group col-md-6">
                    <label for="Duration">Duration<span class="text-danger">*</span></label>
                    <select type="text" name="duration" class="form-control" data-placeholder="Duration" parsley-trigger="change" required="required" tabindex="-1" aria-hidden="true">
                      <option value="month">1 Month</option>
                      <option value="6month">6 Month</option> 
                      <option value="year">Yearly</option>
                    </select>
                  </div>

                <!--   <div class="form-group col-md-6">
                    <label for="Duration">If Monthly</label>
                    <select type="text" name="month_interval" class="form-control" data-placeholder="month_interval" parsley-trigger="change" tabindex="-1" aria-hidden="true">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option>
                        </select>
                  </div> -->
              </div>
                @php
                  if(isset($errors))
                  {
                    foreach($errors as $error)
                    {
                      echo $error;
                    }
                  }

                @endphp
                <div class="form-group">
                  <label for="Description">Pacakge Options</label>
                  <div class="form-row">
                  @foreach($option as $opt)
                    <div class="col-md-4">
                      <input type="checkbox" value="{{$opt->id}}" name="chk[]"   /> {{$opt->option_name}}
                    </div>
                  @endforeach
                   </div>
                </div>
                <div class="form-group">
                  <label for="Description">Description<span class="text-danger">*</span></label>
                  <textarea class="form-control" rows="5" maxlength="30"   spellcheck="false" id="description"  name="description"></textarea>
                  @if ($errors->has('description'))
                              <span class="error">{{ $errors->first('description') }}</span>
                  @endif   

                  <!-- input style="margin-top: 10px" type="checkbox" id="check" name="InActive" >
                  <label for="check1"> Active Package</label>
                  <input style="margin-top: 10px" type="checkbox" id="check" name="InActive" >
                  <label for="check"> Inactive Package</label><br> -->

                 <!-- 
                  <input type="radio" id="active" name="status" value="Active">
                  <label for="active">Active</label>
                  <input type="radio" id="InActive" name="status" value="inactive">
                  <label for="inactive">InActive</label>
                   -->
                </div>

                <button type="submit" class="btn btn-primary">Save</button>
                <button onclick="history.back()" type="button" class="btn">Cancel</button>

              </form>
              </div> <!-- end card-box -->
            </div>
            
            </div> <!-- container -->
            </div> <!-- content -->
              @include('admin.layouts.footer')
            @endsection
            