@extends('layout.app')


@section('content')
<div class="support-company-area gray-bg pt-5 pb-5">
            <div class="container">
                <div class="row ">
                    <div class="col-12">
                        <h2 class="contact-title">Contact</h2>
                    </div>
                    <div class="col-lg-8"> 
                         @if(session()->has('message'))
    <div class="alert alert-success"  >
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
        {{ session()->get('message') }}
    </div>
    @endif
    @if(session()->has('error'))
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
        {{ session()->get('error') }}
    </div>
    @endif


    
    
                        <form class="form-contact contact_form" action="{{url('contactus')}}" method="POST" >
                            {{ csrf_field() }}  
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <input class="form-control valid" name="name" maxlength="30" 
                                        pattern="[A-Za-z\s]+" id="name" type="text" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter your name'" placeholder="Enter your name" required="required">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <input class="form-control valid" name="email" id="email" type="email" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter email address'" maxlength="30" placeholder="Email" required="required">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <input class="form-control" name="phone" id="subject" type="text" onfocus="this.placeholder = ''"  pattern="^[0-9-+\s()]*$" onblur="this.placeholder = 'Enter Phone'" placeholder="Enter Phone" maxlength="15" required="required">
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <textarea class="form-control w-100" name="description" id="message" cols="30" rows="9" maxlength="200" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter Message'" placeholder=" Enter Message"></textarea>
                                    </div>
                                </div>

                            </div>
                            <div class="form-group mt-3">
                            <button type="submit" class="genric-btn info">Send</button>
                               
                            </div>
                        </form>
                    </div>
                    <div class="col-lg-3 offset-lg-1">


                        <div class="media contact-info">
                            <span class="contact-info__icon"><i class="ti-home"></i></span>
                            <div class="media-body">
                                <h3>Bizneez</h3>
                                <p>175-B Sector C Commercial Bahria Town Lahore.</p>
                            </div>
                        </div>
                        <div class="media contact-info">
                            <span class="contact-info__icon"><i class="ti-tablet"></i></span>
                            <div class="media-body">
                                <h3>+92 42 3786 2953</h3>
                                <p>Mon to Fri 9am to 6pm</p>
                            </div>
                        </div>
                        <div class="media contact-info">
                            <span class="contact-info__icon"><i class="ti-email"></i></span>
                            <div class="media-body">
                                <h3>info@cybexo.com</h3>
                                <p>Send us your query anytime!</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
    </div>
<script>
    function myFunction() {
  var x = document.getElementById("myInput");
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }
}




function viewPassword()
{
  var passwordInput = document.getElementById('password-field');
  var passStatus = document.getElementById('pass-status');
 
  if (passwordInput.type == 'password'){
    passwordInput.type='text';
    passStatus.className='fa fa-eye-slash';
    
  }
  else{
    passwordInput.type='password';
    passStatus.className='fa fa-eye';
  }
}




//     function mouseoverPass(obj) {
//   var obj = document.getElementById('password');
//   obj.type = "text";
// }
// function mouseoutPass(obj) {
//   var obj = document.getElementById('password');
//   obj.type = "password";
// }

// $(document).ready(function(){
// $('.pass_show').append('<span class="ptxt">Show</span>');  
// });
  

// $(document).on('click','.pass_show .ptxt', function(){ 

// $(this).text($(this).text() == "Show" ? "Hide" : "Show"); 

// $(this).prev().attr('type', function(index, attr){return attr == 'password' ? 'text' : 'password'; }); 

// }); 
 </script>
 <!-- <style>
  .pass_show{position: relative} 

.pass_show .ptxt { 

position: absolute; 

top: 66%; 

right: 10px; 

z-index: 1; 

color: #2d7bf4; 

margin-top: -10px; 

cursor: pointer; 

transition: .3s ease all; 

} 

.pass_show .ptxt:hover{color: #333333;} 

.msg_password_mismatch{display:none;}
</style> -->


    @endsection

