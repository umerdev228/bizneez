<?php

namespace App\Http\Controllers;

use App\Catalogue;
use App\CataloguePage;
use App\ProductRange;
use App\User;
use App\CatalogueMail;
use PDF;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;

class CataloguePageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data = $request->all();
        $manufacturers = User::role('manufacturer')->get();
        $ranges = ProductRange::all();

        if ($request->button == 'download') {
            $path = public_path('pdf/catalogue');
            PDF::setOptions(['dpi' => 100, 'defaultFont' => 'sans-serif', 'isPhpEnabled' => true,]);
            view()->share(['data' => $data, 'manufacturers' => $manufacturers, 'ranges' => $ranges]);

       
 

            $pdf = PDF::loadView('admin.catalogue.preview')->setPaper('a4', 'landscape')->setWarnings(false); 
            $fileName = rand() . '.pdf' ;
            $pdf->save($path . '/' . $fileName);


             CatalogueMail::create([
                "json_data" => $fileName,
                // "logo" => $catalogue->id,
                "user_id" =>Auth::id(),
                 
            ]);
            //Storage::put('catalogue.pdf', $pdf->output());
            return $pdf->download($fileName);
        }
        if ($request->button == 'preview') {
            return view('admin.catalogue.preview-for-view', compact('data', 'manufacturers', 'ranges'));
        }

        $catalogue = Catalogue::create([
            'name' => '4 pages',
            'page' => $request->page,
        ]);

        $pages = (integer)$request->page;
        for($i = 1; $i <= $pages; $i++) {
//            $image = $request->file('logo'.$i);
//            $imageName = $image->getClientOriginalName();
//            $image->move(public_path('uploads/logo/'),$imageName);
            $company = \App\Company::where('id', $request['company'.$i])->first();

            CataloguePage::create([
                "company_id" => $request['company'.$i],
                "catalogue_id" => $catalogue->id,
                "logo" => $company->logo,
                "title" => $request['title'.$i],
                "manufacturer_id" => $request['manufacturer'.$i],
                "range_id" => $request['range'.$i],
                "image1" => $request['imageValue-'.$i.'-1'],
                "image2" => $request['imageValue-'.$i.'-2'],
                "image3" => $request['imageValue-'.$i.'-3'],
                "description1" => $request['description-'.$i.'-1'],
                "description2" => $request['description-'.$i.'-2'],
                "description3" => $request['description-'.$i.'-3'],
                'option_1' => $request['option_'.$i.'_1'] ? true : false,
                'option_2' => $request['option_'.$i.'_2'] ? true : false,
                'option_3' => $request['option_'.$i.'_3'] ? true : false,
            ]);
        }

        return redirect()->back();

    }

   public function show_saved_list(Request $request){
      $catalog_saved_list = CatalogueMail::where(  "user_id", Auth::id()  )->get();
      return view('admin.catalogue.catalogue-saved-list', compact('catalog_saved_list'));

   }


   public function send_email_attachment(Request $request){
        $data["email"] = $request->email;//'waqas.ger@gmail.com';//'kem@bizneez.net';
        $data["title"] = 'Bizneez Catalogue Attachment PDF';
        $data["body"] = ' '.$request->email_body.'<br><br><br>Attachment: <br><a href="https://bizneez.online/pdf/catalogue/'.$request->pdf.'">Download</a><br>';
             


        Mail::send('mail.custom_mail', $data, function($message)use($data) {
            $message->to($data["email"], $data["email"])
                ->subject($data["title"]);
        });

        toastr()->success('Email with Attachment pdf is sent Successfully');
         return redirect()->back();

   }

    /**
     * Display the specified resource.
     *
     * @param  \App\CataloguePage  $cataloguePage
     * @return \Illuminate\Http\Response
     */
    public function show(CataloguePage $cataloguePage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CataloguePage  $cataloguePage
     * @return \Illuminate\Http\Response
     */
    public function edit(CataloguePage $cataloguePage)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CataloguePage  $cataloguePage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CataloguePage $cataloguePage)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CataloguePage  $cataloguePage
     * @return \Illuminate\Http\Response
     */
    public function destroy(CataloguePage $cataloguePage)
    {
        //
    }

    public function preview(Request $request) {
        dd($request->all());
    }
}
