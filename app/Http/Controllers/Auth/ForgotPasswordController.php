<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Auth;
use App\Merchant;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Lang;
use App\Mail\ForgotMail;
use Illuminate\Support\Facades\Mail;
use Password;
use Str;
use DB;
use Carbon\Carbon;


class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */
    
    use SendsPasswordResetEmails;
   
    public function broker()
    {
        return Password::broker();
    }
    
   // public function url()
   //  {
   //      $format = static::randomElement(static::$urlFormats);

   //      return $this->generator->parse($format);
   //  }

    private function sendResetEmail($email, $token)
    {
        //Retrieve the user from the database
        $user = DB::table('users')->where('email', $email)->select('name', 'email')->first();
        //Generate, the password reset link. The token generated is embedded in the link
        // "{{ env('APP_URL') }}"
//        $url = "http://test.public.Bizneez.net";
        // $url = env('APP_URL_MAIN');
        $link = url('/') . '/password/reset/' . $token . '?email=' . urlencode($user->email);
        // dd($link);
        try {

           Mail::to($user->email)->send(new ForgotMail($link));
           return redirect('mail.mail_forgot_password')->with('message', 'Email is sent successfully.' );

        } catch (\Exception $e) {

            return redirect('mail.mail_forgot_password')->with('error', 'Email is not sending.' );

        }
    }

    public function sendResetLinkEmail(Request $request)
    { 
       
        $merchant = Merchant::where('email', $request->post('email') )->first(); 
        // dd($merchant);
        if ($merchant == null) {

            return redirect('forgot')->with('message', 'Email is not Found Please Signup first.' );
        }
        else
        {   
            // dd('awa');
            $token_generate=DB::table('password_resets')->insert([
                'email' => $request->email,
                 'token'=> Str::random(60),
                 'created_at' => Carbon::now()
                 ]);
            // $tokenData = DB::table('password_resets')->where('email', $request->email)->first();
            $tokenData = DB::table('password_resets')->where('email', $request->email)->orderBy('created_at' ,'DESC')->first();
            // dd($tokenData);
            $user = DB::table('users')->where('email',  $request->email)->select('email')->first();
            // dd('here');
            
            if ($this->sendResetEmail($request->email, $tokenData->token)) {
                return redirect()->back()->with('status', trans('A reset link has been sent to your email address.'));
            } else {
                return redirect()->back()->withErrors(['error' => trans('A Network Error occurred. Please try again.')]);
            }
        }
 }


    //    protected function sendResetLinkResponse(Request $request, $response)
    // {
    //     return redirect('forgot')->with('message', 'Email is sent successfully.' );
    // }










 


      
}
