<?php

namespace App\Http\Controllers;

use App\Employee;
use App\EmployeeSalary;
use App\Profile;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class EmployeeController extends Controller
{

      public static function quickRandom($length = 16)
    {
        $pool = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

        return substr(str_shuffle(str_repeat($pool, 5)), 0, $length);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $employees = User::role('employee')->with('user')->with('profile')->get();
        $data= [
            'employees' => $employees,
            'breadcrumb' => 'Dashboard / Employee List',
            'url' => env('APP_ENV_LINK'),
        ];
        return view('admin.employee.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $breadcrumb = ucfirst(Auth::user()->roles[0]->name)." / Employee / New";

        $random = Str::random(6);
        $random = rand(100,999).$this->quickRandom(3);
        return view('admin.employee.create', compact('breadcrumb','random'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

//        $request['password'] = $password = Str::random(8);

        $validated = $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            'password' => 'required',
        ]);
        $user = User::where('email', $request['email'])->first();
        if ($user == null) {
            $employee = User::create([
                "name" => $request['first_name'] . ' ' . $request['middle_name'] . ' ' . $request['last_name'],
                "first_name" => $request['first_name'],
                "middle_name" => $request['middle_name'],
                "last_name" => $request['last_name'],
                "email" => $request['email'],
                "created_by" => Auth::id(),
                "password" => Hash::make($request['password']),
            ]);
            $user = User::where('email', $request['email'])->first();
            $user->assignRole('employee');

            $video_call_demo = url('/').'/video_call/'.$request['demo_code'];
            Profile::create([
                "user_id" => $user->id,
                "phone" => $request['phone'],
                "mobile" => $request['mobile'],
                "assistant_number" => $request['assistant_number'],
                "address" => $request['address'],
                "location_id" => $request['location_id'],
                "area_id" => $request['area_id'],
                "description" => $request['first_name'],
                "team_building" => $request['team_building'],
                "demo_code" => $request['demo_code'],
                "video_call_demo" => $video_call_demo,
            ]);
            if ($request->has('image')) {
                $imageName = time().'.'.$request->image->extension();
                $request->image->move(public_path('/uploads/users/employees/'), $imageName);
                Profile::where('user_id', $user->id)->update([
                    "image" => '/uploads/users/employees/' . $imageName,
                ]);
            }
//            Mail::to($user->email)->send(new \App\Mail\MerchantActivation($user->name, '', '', '', $request['password']));
        }

     /*   EmployeeSalary::create([
            'employee_id' => $user->id,
            'invoice_number' => $request->invoice_number,
            'account_invoice' => $request->account_invoice,
            'description' => $request->description,
            'price' => $request->price,
            'phone' => $request->phone,
            'discount' => $request->discount,
            'profit' => $request->profit,
            'vat' => $request->vat,
            'total' => $request->total,
            'commission' => $request->commission,
            'quarter_bonuses' => $request->quarter_bonuses,
            'your_bonus' => $request->your_bonus,
            'total_salary' => $request->total_salary,
        ]);*/

        toastr()->success('Employee Created Successfully');
        return redirect()->route('admin.employees.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $breadcrumb = ucfirst(Auth::user()->roles[0]->name)." / Employee / Show";
        $id = (\request()->route()->parameter('id'));
        $user = User::with('roles')->where('id', $id)->with('salary')->with('profile')->first();
        return view('admin.employee.show', compact('user', 'breadcrumb'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $breadcrumb = ucfirst(Auth::user()->roles[0]->name)." / Employee / Edit";
        $users = User::all();
        $employee = User::role('employee')->where('id', $id)->with('profile')->with('salary')->first();
        $childs = User::where('created_by', $id)->get();
        return view('admin.employee.edit', compact('employee', 'breadcrumb', 'users', 'childs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        User::where('id', $id)->update([
            "name" => $request['first_name'] . ' ' . $request['middle_name'] . ' ' . $request['last_name'],
            "first_name" => $request['first_name'],
            "middle_name" => $request['middle_name'],
            "last_name" => $request['last_name'],
            "email" => $request['email'],
            "created_by" => Auth::id(),
        ]);
        $video_call_demo = url('/').'/video_call/'.$request['demo_code'];

        Profile::where('user_id', $id)->update([
            "phone" => $request['phone'],
            "mobile" => $request['mobile'],
            "assistant_number" => $request['assistant_number'],
            "address" => $request['address'],
            "location_id" => $request['location_id'],
            "area_id" => $request['area_id'],
            "description" => $request['first_name'],
            "team_building" => $request['team_building'],
            "demo_code" => $request['demo_code'],
            "video_call_demo" => $video_call_demo,
        ]);
        if ($request->has('image')) {
            $imageName = time().'.'.$request->image->extension();
            $request->image->move(public_path('/uploads/users/employees'), $imageName);
            Profile::where('user_id', $id)->update([
                "image" => '/uploads/users/employees/' . $imageName,
            ]);
        }
        if ($request->has('password')) {
            User::where('id', $id)->update([
                "password" => $request['password'],
            ]);
        }

//        EmployeeSalary::where('id', $id)->update([
//            'invoice_number' => $request->invoice_number,
//            'account_invoice' => $request->account_invoice,
//            'description' => $request->description,
//            'price' => $request->price,
//            'phone' => $request->phone,
//            'discount' => $request->discount,
//            'profit' => $request->profit,
//            'vat' => $request->vat,
//            'total' => $request->total,
//            'commission' => $request->commission,
//            'quarter_bonuses' => $request->quarter_bonuses,
//            'your_bonus' => $request->your_bonus,
//            'total_salary' => $request->total_salary,
//        ]);
        toastr()->success('Employee Updated Successfully');
        return redirect()->route('admin.employees.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        User::where('id', $id)->delete();
        Profile::where('user_id', $id)->delete();
        toastr()->success('Employee Deleted Successfully');
        return redirect()->route('admin.employees.index');
    }
}
