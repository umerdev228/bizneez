<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'admin@admin.com',
            'password' => Hash::make('123456'),
        ]);
        $manufacturer = DB::table('users')->insert([
            'name' => 'manufacturer',
            'email' => 'manufacturer@bizneez.com',
            'password' => Hash::make('123456'),
        ]);
        $architect = DB::table('users')->insert([
            'name' => 'architect',
            'email' => 'architect@bizneez.com',
            'password' => Hash::make('123456'),
        ]);
        $client = DB::table('users')->insert([
            'name' => 'client',
            'email' => 'client@bizneez.com',
            'password' => Hash::make('123456'),
        ]);
        $user = DB::table('users')->insert([
            'name' => 'user',
            'email' => 'user@bizneez.com',
            'password' => Hash::make('123456'),
        ]);
        $user = DB::table('users')->insert([
            'name' => 'employeee',
            'email' => 'employeee@bizneez.com',
            'password' => Hash::make('123456'),
        ]);
        $user = DB::table('users')->insert([
            'name' => 'dealer',
            'email' => 'dealer@bizneez.com',
            'password' => Hash::make('123456'),
        ]);

        $admin = User::first();
        $manufacturer = User::where('name', 'manufacturer')->first();
        $architect = User::where('name', 'architect')->first();
        $client = User::where('name', 'client')->first();
        $employee = User::where('name', 'employeee')->first();
        $user = User::where('name', 'user')->first();
        $dealer = User::where('name', 'dealer')->first();

        $role = Role::create(
            ['name' => 'admin'],
        );
        $manufacturerRole = Role::create(
            ['name' => 'manufacturer'],
        );
        $architectRole = Role::create(
            ['name' => 'architect'],
        );
        $clientRole = Role::create(
            ['name' => 'client'],
        );
        $employeeRole = Role::create(
            ['name' => 'employee'],
        );
        $userRole = Role::create(
            ['name' => 'user'],
        );
        $dealerRole = Role::create(
            ['name' => 'dealer'],
        );
        $demoRole = Role::create(
            ['name' => 'demo'],
        );

        $permission = Permission::create(['name' => 'admin.permission.all']);
        $role->givePermissionTo($permission);
        $permission->assignRole($role);
        $admin->assignRole($role);
        $manufacturer->assignRole($manufacturerRole);
        $architect->assignRole($architectRole);
        $client->assignRole($clientRole);
        $employee->assignRole($employeeRole);
        $user->assignRole($userRole);
        $dealer->assignRole($dealerRole);

        $users = User::all();
        foreach ($users as $user) {
            \App\Profile::create([
                'user_id' => $user->id,
            ]);
        }
    }
}
