<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Advertisement extends Model
{
    //
    protected $guarded = [];

    public function media() {
        return $this->belongsTo(Media::class, 'image', 'id');
    }
}
