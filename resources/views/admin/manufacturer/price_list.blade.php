@extends('admin.layouts.app')
@section('content')
    <!-- Start Page content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card-box">
                        <h4 class="header-title m-t-0 m-b-30"> </h4>
                    </div>
                    <form action="{{route('AdminManufactureCategoryAdd')}}" method="post">
                        <div id="individual_products" class="ml-4">
                            <input type="text" name="row_count" id="row_count" style="display:none;">
                            <input type="text" name="element_count" id="element_count" style="display:none;">
                            @csrf
                            <div >
                                <div  class="row" id="product">
                                    <div class="col-md-4 no-padding">
                                        <input type="text" name="category_top[]" id="cateogry_top0" placeholder="" class="form-control float-left height-50" list="cat"/>
                                        &nbsp;
                                        <button id="top-plus" type="button" onclick="appendFields(0)" class="btn btn-primary btn-custom height-50 valid align-baseline rounded">
                                            <i class="fa fa-plus"></i>
                                        </button>
                                        <button id="top-tick" type="button" onclick="show_category_name('category0','cateogry_top0')" class="btn btn-custom height-50 btn-success valid align-baseline rounded">
                                            <i class="fa fa-check"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <br>
                            <br>
                            <hr class="style1">
                            <br>
                            <div class="cat_product" id="category">
                                <div class="row" id="prd0">
                                    <div class="col remove-pad no-padding row">
                                        <div id="element0" class="row pb-3" style="flex-wrap: nowrap;  overflow-x: auto;">
                                            <div class="p-2">
                                                <label for="p_name_0_0" style="top: -22px; font-weight: 500;" class="category0 lbl"></label>
                                                <input class="form-control width-200 right height-50"  name="p_name[]0" id="p_name_0_0">
                                                <input type="hidden" name="cat[]0" id="cat_0_0">
                                            </div>
                                        </div>
                                        &nbsp;<button type="button" onclick="remove_element('category')"  class="btn mt-2  red valid btn-custom align-baseline rounded"> <i class="fa fa-minus"></i> </button>
                                        <!--  <button type="button" class="btn valid mt-2 btn-success btn-custom align-baseline rounded"><i class="fa fa-check"></i> </button> -->
                                        &nbsp;<button type="button"  class="btn mt-2 btn-custom valid align-baseline rounded purp3">
                                            <i class="fa fa-arrow-down"></i>
                                        </button>
                                    </div>
                                </div>
                                <div id="sub_row0"></div>
                            </div>
                        </div>
                        <div class="mt-5 center">
                            <input type="submit" class="btn btn-primary " value="Save & Submit" name="btn" />
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <style>
        .side-menu .topbar-left{top:24px;}
        .notification-list .noti-icon{margin-top:20px;}
        .btn_align_red{position: absolute;
            left: 79%;
            top: 22px;}
        .btn_align_perpal{position: absolute;
            left: 92%;
            top: 22px;}
        .object-lable{    text-align: center;
            width: 270px;}
        .custom-txt{width: 79%;}
        .lbl{
            position: absolute;
            top: -20px;
            left: 11px;
        }
        .lbl2{    top: -22px;
            position: absolute;
            /* left: 4px; */
            margin-left: -203px;
        }
        .category{font-weight: bold; font-size:20px}
        .no-padding{
            padding-left: 2px;
            padding-right: 0px;
        }

        .waves-light{width: 75%;}
        .btn-success{font-size: 16px;}
        .remove-pad{padding-right: 0px; padding-left: 0px;}
        .purpal{background-color: #a311d8}
        .red{background-color: red; color: #fff}
        .right{float: left}
        .blue{background-color: #2d7bf4; color: #fff}
        .purple{background: purple; color: #fff}
        .purp3{background: purple; color: #fff}
        .brown{background-color:#07922e; color: #fff}

        .file[type="file"] {
            display: none;
        }
        .height-50{height: 34px!important;}
        .width-200{width:200px; margin-right: 7px;}
        #cateogry_top0{  margin-left: -11px; width:200px; }
        #p_name_0_0{  margin-left: 12px; }
        .mt-c{    margin-top: 4px;}
        .btn-custom   {
            height: 34px;     margin: 0px 2px;
            padding-top: 3px;
        }
        .btn-custom-mr{margin: 0px 17px; height: 34px;}
        .btn-custom.focus, .btn-custom:focus, .btn-outline-custom.focus, .btn-outline-custom:focus, .btn-custom:not([disabled]):not(.disabled).active, .btn-custom:not([disabled]):not(.disabled):active, .show > .btn-custom.dropdown-toggle{box-shadow: none!important;}
        .btn-height-34{height: 34px;}
    </style>
    <script type="text/javascript"  src="https://code.jquery.com/jquery-1.6.4.js"></script>
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css">
    <script type="text/javascript"  src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
    <script !src="">
        var selectIncrement = 0;
        var i = 0;
        var j = 0;
        var c =0;


        function show_category_name(class_name,category_id){
            console.log(class_name, category_id)
            var cls = '.'+class_name;
            var cat_id = '#'+category_id;
            $(cls).text( $(cat_id).val() );
            $('#cat_0_0').val( $(cat_id).val() );
            $("#top-tick").hide();
            // $("#top-plus").hide();
            $("#cateogry_top0").val('');
            $('#top-plus').prop('disabled', true);
        }
        function appendFields() {
            add_element('element0');
            $("#cateogry_top0").val('');
            $('#top-plus').prop('disabled', true);
        }
        function remove_element(element_id){
            i--
            $('#'+element_id).remove();
            document.getElementById('row_count').value = i

        }
        function removeElement(element_id){
            console.log(element_id)
            selectIncrement--
            $(element_id).remove();
            document.getElementById('element_count').value = selectIncrement
        }
        function add_element(element_id){
            selectIncrement++
            //console.log(element_id)
            category_name = $('#cateogry_top0').val();
            var element = '#'+element_id;
            let ele_select = 'select_'+selectIncrement;
            var element_data =
                `<div  class="p-2" id="`+ele_select+`">
                    <label for="p_name_`+i+`_`+selectIncrement+`" class="lbl2" >
                        `+category_name+`
                    </label>
                    <input type="hidden" name="cat[]`+selectIncrement+`" value="`+category_name+`">
                    <input class="form-control height-50 width-200 right" name="p_name[]`+selectIncrement+`" id="p_name_`+i+`_`+selectIncrement+`">
                    <a style='margin-right: 8px;height: 34px;margin-left: -40px;' href="javascript:void(0)" onclick="removeElement(`+selectIncrement+`)" class="btn btn-danger">
                        <i class="fa fa-close  mt-c"></i>
                    </a>
                </div>`;
            $(element).append(element_data);
            document.getElementById('element_count').value = selectIncrement
        }
        // $(document).ready(function() {
        $('#top-plus').prop('disabled', true);
        $('#cateogry_top0').keyup(function() {
            if($(this).val() != '') {
                $('#top-plus').prop('disabled', false);
            }
        });
        $(".purp3").live('click', function() {
            i++
            var $row    = $(this).closest('.cat_product');
            var $clone = $row.clone();
            // $clone.find(':text').val('');
            $row.after($clone);
            document.getElementById('row_count').value = i

        });
        // });
    </script>

@endsection
