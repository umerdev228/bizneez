@extends('layout.app')


@section('content')
<main>
 <div class="support-company-area">
            <div class="container">
                <div class="row mb-5 mt-5">
                    <div class="col-12 ml-3 5">
                        <h2 class="contact-title">Frequently Asked Questions</h2>
                    </div>
                    <div class="col-lg-12">
                        <!-- Start Faq -->
                        <div class="accordion-container mt-5 mb-5">
                          <div class="set">
                            <a href="#">
                             What is Bizneez?
                              <i class="fa fa-plus"></i>
                            </a>
                            <div class="content ml-3">
                              <p>Bizneez is an online customer support system from Cybexo Inc.It lets you streamline your company's customer support and helps you to efficiently manage your customers as you scale.Cybexo TMS tracks, prioritizes,and follow up on customers requests, at one place.</p>
                            </div>
                          </div>
                          <div class="set">
                            <a href="#">
                              How do I can create a new account on Bizneez TMS?
                              <i class="fa fa-plus"></i>
                            </a>
                            <div class="content ml-3">
                              <p> You can create account from under tms.net, using the 'Sign up' option. TMS will collect your contact information before creating a new Trial Account for you.</p>
                            </div>
                          </div>
                          <div class="set">
                            <a href="#">
                              How can I create new ticket? 
                              <i class="fa fa-plus"></i>
                            </a>
                            <div class="content ml-3">
                              <p>After login to your account you can see a button on the top with name “New”. Click on it and create new ticket.</p>
                            </div>
                          </div>
                          <div class="set">
                            <a href="#">
                              Can I download my agents’ or customers’ record in my system? 
                              <i class="fa fa-plus"></i> 
                            </a>
                            <div class="content ml-3">
                              <p>Yes you can. It’s very easy to download any record from Cybexo TMS to your system. With one click on download button you can download data in excel sheet form. </p>
                            </div>
                          </div>
                          <div class="set">
                            <a href="#">
                             How to add new agents? 
                              <i class="fa fa-plus"></i> 
                            </a>
                            <div class="content ml-3">
                              <p >Its simple and user friendly process. Use this guide tool to add new agent:<br>
                                • Login from merchant side.<br>
                                • Go to your left side navigation bar > <strong>Agents List.</strong><br>
                                • Select agents list option. On the top of this page, click “New” button.  

                              </p>
                            </div>
                          </div>
                              <div class="set">
                            <a href="#">
                             Can I change my ticket’s priority or status? 
                              <i class="fa fa-plus"></i> 
                            </a>
                            <div class="content ml-3">
                              <p >Follow the steps in following guidelines:<br>
                                • Login your account.<br>
                                • Go to navigation bar > tickets list.<br>
                                • You will find priority and status columns there.
                              </p>
                            </div>
                          </div>
                        </div>
                        <!-- End Faq -->
                    </div>
                    
                </div>
            </div>
            </div>
        </div>
    </main>


  
@endsection
 