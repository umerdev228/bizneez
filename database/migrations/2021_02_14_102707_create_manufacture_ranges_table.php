<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateManufactureRangesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manufacture_ranges', function (Blueprint $table) {
            $table->id();
            $table->integer('manfacture_categories_id')->nullable();
            $table->string('range1')->nullable();
            $table->string('range2')->nullable();
            $table->string('range3')->nullable();
            $table->string('range4')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('manufacture_ranges');
    }
}
