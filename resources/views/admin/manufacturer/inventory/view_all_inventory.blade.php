@extends('admin.layouts.app')
@section('content')
<!-- Start Page content -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12"> <br><br><br><br>
                
                <div class="row">
                    <div class="col-md-6"></div>  
                    <div class="col-md-6 mt-2" align="right"><a href="{{ url('admin/manufacturers/inventory')}}" class="btn btn-primary"> Add New Inventory</a></div>
                    
                </div>
          


  <form action="{{route('inventorySearch')}}" method="post">
  @csrf
<lable>Please select month</lable>
<div class="row">
    <div class="col-md-3">  
<input placeholder="Please select month" class="form-control" type="month" id="start" name="start"  min="2020-07" value="2021-07">
</div>
    <div class="col-md-6"><input name="btnSearch"  type="submit" value="Search" class="btn btn-success" /></div>
</div>

</form>





<table class="table table-striped mt-5">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Inventory Detail</th>
      <th scope="col">Detail</th>
      <th scope="col">Total</th>
    </tr>
  </thead>
  <tbody>
    @foreach($all_inventory as $inventory)
   @php
    $total=0;
  
    //$json_decode_data = json_decode($inventory->data_in_json);
    //print "<pre>";
    //print_r($json_decode_data); die();
    $value_json =substr( $inventory->data_value_json,1,-1);  
    $inv_values_ary = explode(',', $value_json);  //print_r($inv_values_ary); die();
    for($i=0;$i<sizeof($inv_values_ary);$i++){
        $total = $total + substr($inv_values_ary[$i],1,-1);
    }
    $sub_total = $sub_total + $total;
   @endphp
    <tr>
      <th scope="row">{{$inventory->id}}</th>
      <td> {{$inventory->data_in_json}} <br>{{$inventory->data_value_json}}</td>
      <td>{{$inventory->created_date}}</td>
      <td>£{{$total}}</td>
    </tr>

     @endforeach


    <tr style="font-weight: bold">
      <th scope="row"></th>
      <td></td>
      <td>Total</td>
      <td>£{{$sub_total}}</td>
    </tr>
  </tbody>
</table>




                
            </div>
        </div>
    </div>
</div>


@endsection