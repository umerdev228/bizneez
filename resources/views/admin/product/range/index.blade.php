@extends('admin.layouts.app')
@section('content')
    @include('admin.layouts.topbar')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    @if(session()->has('message'))
                        <div class="alert alert-success mt-5" id="myElem" >
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-12 mt-5">
                            <div class="card">
                                <div class="card-body">
                                    <table class="table table-hover m-0 tickets-list table-actions-bar dt-responsive nowrap" id="example">
                                        <thead>
                                        <tr>
                                            <th>Manufacturer Name</th>
                                            <th>Number of Ranges</th>
                                            <th data-orderable="false"></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($manufacturers as $manufacturer)
                                            <tr>
                                                <td>
                                                    {{$manufacturer->name}}
                                                </td>
                                                <td>
                                                    {{count($manufacturer->ranges)}}
                                                </td>
                                                <td>
                                                    <a class="btn" href="{{ route('admin.product.range.show', ['id' => $manufacturer->id] ) }}">
                                                        <i class="mdi mdi-eye"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>

                                </div> <!-- end card -->
                            </div><!-- end col -->
                        </div><!-- end col -->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

