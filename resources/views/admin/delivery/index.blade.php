@extends('admin.layouts.app')
@section('content')

   <div class="row">
      <div class="col-md-12">
         <div id="app">
            <delivery-component
                    :all-events="{{ json_encode($events, true) }}"
            ></delivery-component>
         </div>
      </div>
      {{--        <div class="col-md-2">--}}
      {{--            <div style="    margin-top: 228px;">--}}
      {{--                <div class="calendar calendar-first" id="calendar_first" >--}}
      {{--                    <div class="calendar_header">--}}
      {{--                        <button class="switch-month switch-left"> <i class="fa fa-chevron-left"></i></button>--}}
      {{--                        <h2></h2>--}}
      {{--                        <button class="switch-month switch-right"> <i class="fa fa-chevron-right"></i></button>--}}
      {{--                    </div>--}}
      {{--                    <div class="calendar_weekdays"></div>--}}
      {{--                    <div class="calendar_content"></div>--}}
      {{--                </div>--}}
      {{--            </div>--}}
      {{--        </div>--}}
   </div>

   <script src="{{asset('/js/app.js')}}"></script>


@endsection

<style>
   @import url(https://fonts.googleapis.com/css?family=Muli:400, 300);
   body { margin-top: 3%; }

   .calendar, .calendar_weekdays, .calendar_content {
      max-width: 300px;
   }
.logo{padding-top: 24px;}
   .calendar {
      float: left;
      margin-bottom: 0;
      background-color: #061469;
      padding: 20px;
      width: 225%;
      color: #fff;
   }


   .calendar_content, .calendar_weekdays, .calendar_header {
      position: relative;
      overflow: hidden;
   }
   .calendar_weekdays div {
      display:inline-block;
      vertical-align:top;
   }
   .calendar_weekdays div, .calendar_content div {
      width: 14.28571%;
      overflow: hidden;
      text-align: center;
      background-color: transparent;
      color: #6f6f6f;
      font-size: 14px;
   }
   .calendar_content div {
      border: 1px solid transparent;
      float: left;
   }
   .calendar_content div:hover {
      border: 1px solid #dcdcdc;
      cursor: default;
   }
   .calendar_content div.blank:hover {
      cursor: default;
      border: 1px solid transparent;
   }
   .calendar_content div.past-date {
      color: #d5d5d5;
   }
   .calendar_content div.today {
      font-weight: bold;
      font-size: 14px;
      color: #87b633;
      border: 1px solid #dcdcdc;
   }
   .calendar_content div.selected {
      background-color: #f0f0f0;
   }
   .calendar_header {
      width: 100%;
      text-align: center;
   }
   .calendar_header h2 {
      padding: 0 10px;
      font-family:'Muli', sans-serif;
      font-weight: 300;
      font-size: 18px;
      color: #87b633;
      float:left;
      width:70%;
      margin: 0 0 10px;
   }
   button.switch-month {
      background-color: transparent;
      padding: 0;
      outline: none;
      border: none;
      color: #dcdcdc;
      float: left;
      width:15%;
      transition: color .2s;
   }
   button.switch-month:hover {
      color: #87b633;
   }
</style>
