
 
<!-- Start right Content here -->
<!-- ============================================================== -->

<div class="content-page">
    <!-- Top Bar Start -->
    <div class="topbar">
        <nav class="navbar-custom">
            <ul class="list-unstyled topbar-right-menu float-right mb-0 ">

                @php
                    $notification = \App\History_log::where('intstatus',0)->orderBy('id', 'desc')->get();
                @endphp


                <li class="dropdown notification-list" style="margin-right: 2px; margin-top: 3px;"> <a  onclick="notification_counter_update();"  class="nav-link dropdown-toggle arrow-none custom_css" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false"> <i class="fi-bell noti-icon"></i> <span class="badge badge-danger badge-pill noti-icon-badge">{{count($notification)}}</span> </a>
                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-animated dropdown-lg  smooth-scroll list-unstyled" style="margin-right: 15px;">
              
                        <!-- item-->
                        <div class="dropdown-item noti-title">
                            <h5 class="m-0"><span class="float-right"> @if(count($notification)>0)
                                        <a href="{{url('admin/notification')}}" class="text-dark">View All </a>@endif </span>Notification</h5>
                        </div>
                        <div class="slimScrollDiv " style="position: relative; overflow: hidden; width: auto; height: 414px; ">
                            <div class="slimscroll" id="notification-list" style="max-height: 230px; overflow: auto; width: auto; height: 414px;">
                            @foreach($notification as $data)
                                <!-- item-->
                                    <?php
                                    if($data->inttype == 1)
                                        $notification_msg = 'New user registered(Trail)';
                                    else
                                        $notification_msg = 'Prenium User';
                  
                                    ?>
                  
                                    <a href="{{ URL::to('admin/user/view/' . $data->user_id ) }}" class="dropdown-item notify-item">
                                        <div class="notify-icon bg-info"><i class="mdi mdi-account-plus"></i></div>
                                        <p class="notify-details">{{$notification_msg}}<small class="text-muted"> {{$data->description}} </small></p>
                                    </a>

                                @endforeach

                     

                            </div>
                        </div>
                    </div>
                </li>





                <li class="hide-phone app-search">
                    <form action="{{url('admin/dashboard')}}"  method="get" role="search">
                        <input name="txtSearch" type="text" placeholder="Search..." class="form-control">
                        <button type="submit"><i class="fa fa-search"></i></button>
                    </form>
                </li>
                <li class="dropdown notification-list">

                    <a class="nav-link dropdown-toggle nav-user" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                        <img src="{{asset('/theme/admin/assets/images/users/avatar-1.jpg')}}" alt="user" class="rounded-circle"> <span class="ml-1"> {{Auth::user()->name}} <em class="mdi mdi-chevron-down"></em> </span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-animated profile-dropdown">
                        <!-- item-->
                        <div class="dropdown-item noti-title">
                            <h6 class="text-overflow m-0">Welcome !</h6>
                        </div>

                        <!-- item href="javascript:void(0);" -->
                        <a   href="{!! url('admin/account') !!}" class="dropdown-item notify-item">
                            <i class="fi-head"></i> <span>Account</span>
                        </a>
                        <a   href="{!! url('admin/setting') !!}" class="dropdown-item notify-item">
                            <i class="fa fa-gear"></i> <span>Setting</span>
                        </a>
  
                        <!-- item-->
                        <a  href="{{ route('logout') }}" class="dropdown-item notify-item">
                            <i class="fi-power"></i> <span>Logout</span>
                        </a>
                    </div>
                </li>
            </ul>
            <ul class="list-inline menu-left mb-0">
                <li class="float-left">
                    <button class="button-menu-mobile open-left disable-btn">
                        <i class="dripicons-menu"></i>
                    </button>
                </li>
                <li>
                    <div class="page-title-box">
                        <h4 class="page-title" >Administator </h4>
                        <p><a href="/admin/dashboard">{{$breadcrumb ?? ''}}</a></p>
                    </div>
                </li>
            </ul>
        </nav>
    </div>
        
    <style>
        .topscroller{position: relative; overflow: hidden; width: auto; height: 414px;}
        .bottomscroller{max-height: 230px; overflow: auto; width: auto; height: 414px;}
    </style>