<?php

namespace App\Http\Controllers;

use App\Company;
use App\Order;
use App\Product;
use App\Quotation;
use App\QuotationContact;
use App\QuotationProduct;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use \DB;
use App\CatalogueMail;
use Illuminate\Support\Facades\Mail;
use PDF;


class QuotationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $breadcrumb = ucfirst(Auth::user()->roles[0]->name)." / Invoices / List";

        $quotations = Quotation::orderBy('created_at', 'desc')->get();
        return view('admin.quotation.index', compact('quotations', 'breadcrumb'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function get_ranges(){
          $ranges  = DB::table('mdd_price_list_csv')->select(['COL5','COL6','COL7','COL8'])->get();
          
          $option ='<label for="area_id">Ranges </label><select onchange="get_price(this)" class="form-control" id="rng">';
          foreach($ranges as $rng){
            $option .= '<option value="'.substr($rng->COL8,1).'">'.$rng->COL5. ' ' .$rng->COL6. ' '.$rng->COL7. ' £' .substr($rng->COL8,1).'</option>';
          }
          $option .= '</select>';
          return $option;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $products = [];
        $images_catalogue =[];
        $vat = $request->vat;
        $company = Company::where('id', $request['company'])->first();
        $contact = QuotationContact::create([
            'contact_person' => $request->contact_person,
            'company_address' => $company->address,
            'invoice_address' => $request->invoice_address,
            'client_address' => $request->client_address,
        ]);
        $quotation = Quotation::create([
            'date' => date('Y-m-d H:i:s'),
            'vat' => $vat,
            'vat_value_hid' => $request->vat_value_hid,
            'company_id' => $company->id,
            'contact_id' => $contact->id,
        ]);
        $order = Order::create([
            'serial_number' => $request->serial_number,
            'user_id' => Auth::id(),
            'quotation_id' => $quotation->id,
            'status' => 'pending',
            'order_type' => 'custom',
        ]);
        foreach ($request->qty as $index => $qty) {

            $imagepath = '';
            if ($request->has('file'.$index)) {
                $key = 'file'.$index;
                if($request->$key !== null){
                    $imagepath = '/'.$request->$key;
                }
            }
            $pro = QuotationProduct::create([
                'quotation_id' => $quotation->id,
                'description' => $request->description[$index],
                'qty' => $request->qty[$index],
                'price' => $request->price[$index],
                'discount_price' => $request->discount_price[$index],
                'percentage' => $request->percentage[$index],
                'image' => $imagepath,
                'title' => $request->title[$index],
                'keywords' => $request->keywords[$index],
                'image_description' => $request->image_description[$index]
            ]);
            array_push($products, $pro);
            array_push($images_catalogue, $imagepath);
        }

        if($request->presentation){ 
            $company = Company::where('id', $company->id)->first();
            $data['logo']= $company->logo; 
            $data['images_catalogue'] = $images_catalogue;

 
            $manufacturers=2;
            $ranges = '';
            $path = public_path('pdf/catalogue');
            PDF::setOptions(['dpi' => 100, 'defaultFont' => 'sans-serif', 'isPhpEnabled' => true,]);
            view()->share(['data' => $data, 'manufacturers' => $manufacturers, 'ranges' => $ranges]); 
            $pdf = PDF::loadView('admin.catalogue.preview-quote')->setPaper('a4', 'landscape')->setWarnings(false); 
            $fileName = 'quote_'.rand() . '.pdf' ;
            $pdf->save($path . '/' . $fileName); 

            CatalogueMail::create([
                "json_data" => $fileName, 
                "user_id" =>Auth::id(), 
            ]);
        }

        $contact_person = $request->contact_person;
        $company_address = $request->company_address;
        $invoice_address = $request->invoice_address;
        $vat_value_hid = $request->vat_value_hid;
        if ($request->button == 'Preview') {
            Order::where('id', $order->id)->update([
                'termconditionpreview' => $request->termconditions,
            ]);
            $termconditions = $request->termconditions;
            return view('admin.quotation.show' , compact('products', 'termconditions', 'contact_person','company_address','invoice_address', 'vat', 'company' , 'vat_value_hid', 'order'));
        }
        else {
            return redirect()->route('admin.orders.index');
        }        // return redirect()->back();
    }

    public function quotationShow($id) {
        $order = Order::where('id', $id)->first();
        $quotation = Quotation::where('id', $order->quotation_id)->first();
        $company = Company::where('id', $quotation->company_id)->first();
        $vat = $quotation->vat;
        $products = QuotationProduct::where('quotation_id', $quotation->id)->get();
        $contact = QuotationContact::where('id', $quotation->contact_id)->first();
        $contact_person = $contact->contact_person;
        $company_address = $contact->company_address;
        $invoice_address = $contact->invoice_address;

        $termconditions = $order->termconditionpreview;

        return view('admin.quotation.show' , compact('termconditions','products', 'contact_person','company_address','invoice_address', 'vat', 'company', 'order'));
    }

    public function adminQutationShow($id) {
        $order = Order::where('id', $id)->with('products')->first();
        $total = 0;
        foreach ($order->products as $product) {
            $total += $product->price * $product->quantity;
        }
        return view('admin.quotation.show', compact('order', 'total'));
    }



    /**
     * Display the specified resource.
     *
     * @param  \App\Quotation  $quotation
     * @return \Illuminate\Http\Response
     */
    public function show(Quotation $quotation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Quotation  $quotation
     * @return \Illuminate\Http\Response
     */
    public function edit(Quotation $quotation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Quotation  $quotation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $order = Order::where('id', $id)->first();
        $quotation = Quotation::where('id', $order->id)->first();
        $contact = QuotationContact::where('id', $quotation->contact_id)->first();

        $products = [];
        $vat = $request->vat;
        $company = Company::where('id', $request['company'])->first();
        QuotationContact::where('id', $contact->id)->update([
            'contact_person' => $request->contact_person,
            'company_address' => $company->address,
            'invoice_address' => $request->invoice_address,
            'client_address' => $request->client_address,
        ]);
        Quotation::where('id', $quotation->id)->update([
            'date' => date('Y-m-d H:i:s'),
            'vat' => $vat,
            'vat_value_hid' => $request->vat_value_hid,
            'company_id' => $company->id,
            'contact_id' => $contact->id,
        ]);
        Order::where('id', $order->id)->update([
            'serial_number' => $request->serial_number,
            'user_id' => Auth::id(),
            'quotation_id' => $quotation->id,
            'status' => 'pending',
            'order_type' => 'custom',
        ]);
        QuotationProduct::where('quotation_id', $quotation->id)->delete();
        foreach ($request->qty as $index => $qty) {

            $imagepath = '';
            if ($request->has('file'.$index) !== null) {
                if ($request->has('file'.$index)) {
                    $key = 'file'.$index;
                    if($request->$key !== null){

                    if($request->$key[0] !== '/') {
                        $imagepath = '/'.$request->$key;
                    }
                    else{
                        $imagepath = $request->$key;
                    }
                    }
                }
            }

            $pro = QuotationProduct::create([
                'quotation_id' => $quotation->id,
                'description' => $request->description[$index],
                'qty' => $request->qty[$index],
                'price' => $request->price[$index],
                'discount_price' => $request->discount_price[$index],
                'percentage' => $request->percentage[$index],
                'image' => $imagepath,
            ]);
            array_push($products, $pro);
        }

        $products = QuotationProduct::where('quotation_id', $quotation->id)->get();
        $contact_person = $request->contact_person;
        $company_address = $request->company_address;
        $invoice_address = $request->invoice_address;
        $vat_value_hid = $request->vat_value_hid;
        if ($request->button == 'Preview') {
            Order::where('id', $order->id)->update([
                'termconditionpreview' => $request->termconditions,
            ]);
            $termconditions = $request->termconditions;
            return view('admin.quotation.show' , compact('products', 'termconditions', 'contact_person','company_address','invoice_address', 'vat', 'company' , 'vat_value_hid', 'order'));
        }
        else {
            return redirect()->route('admin.orders.index');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Quotation  $quotation
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Quotation::where('id', $id)->delete();
        toastr()->success('Quotation Deleted Successfully');

        return redirect()->back();
    }
}
