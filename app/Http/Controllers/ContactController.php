<?php

namespace App\Http\Controllers;
use App\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth');
    }
  
    public function index()
    { 

        $contact = Contact::paginate(8);

        $data = [
            'contact' => $contact,
            'breadcrumb' => 'Dashboard / contact'
        ];

        return view('admin.contact.index')->with($data);
    }

    public function create(Request $request)
    {
        return view('admin.contact.create');
 
        $data = [
               
            'email' => $request->post('email'),
            'name' => $request->post('name'),
            'phone' => $request->post('phone'),
            'description' => $request->post('description'),

        ];
        Contact::create($data);
        return redirect('contact')->with('message', 'Message sent successfully.');
    }

    public function store(Request $request) {
        $imageName = '';
        if ($request->has('file')) {
            $image = $request->file('file');
            $imageName = $image->getClientOriginalName();
            $image->move(public_path('uploads/images/contacts/'), $imageName);
        }
        Contact::create([
            "name" => $request->name,
            "email" => $request->email,
            "description" => $request->message,
            "company" =>' ',// $request->company,
            "image" => 'uploads/images/contacts/'.$imageName,
        ]);
        $data["email"] = 'kem@bizneez.net';
        $data["title"] = 'Contact Us';
        $data["body"] = 'Name: '.$request->name.'<br>'.
            'Email: '.$request->email.'<br>'.
            'Details: '.$request->message.'<br>';



        Mail::send('mail.custom_mail', $data, function($message)use($data) {
            $message->to($data["email"], $data["email"])
                ->subject($data["title"]);
        });
        toastr()->success('Request submitted successfully');
        return redirect()->back();
    }

     public function destroy($id)
    {
        //
        Contact::where('id', $id)->delete();
        return redirect()->back();
    }
 


}
