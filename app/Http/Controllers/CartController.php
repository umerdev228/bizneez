<?php

namespace App\Http\Controllers;

use App\Product;
use Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $cartCollection = Cart::getContent();
        $cartTotalQuantity = Cart::getTotalQuantity();
        $total = Cart::getTotal();
        $breadcrumb = ucfirst(Auth::user()->roles[0]->name)." / Quotation ";
        return view('admin.cart.index', compact('breadcrumb', 'cartTotalQuantity', 'cartCollection', 'total'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function addToCart(Request $request) {
        Session::all();
        $product = Product::where('id', $request['id'])->first();

        Cart::add(array(
            'id' => $product->id,
            'name' => $product->name,
            'price' => $product->price,
            'quantity' => 1,
            'attributes' => array(
                'size' => 0
            )
        ));

        $cartTotalQuantity = Cart::getTotalQuantity();
        return response()->json(['type' => 'success', 'message' => 'Item Added to Cart Successfully', 'quantity' => $cartTotalQuantity]);
    }

    public function getCartTotalItem() {
        $cartCollection = Cart::getContent();
        $items = (count($cartCollection));
        $cartTotalQuantity = Cart::getTotalQuantity();
        return response()->json(['type' => 'success', 'message' => 'Item Added to Cart Successfully', 'quantity' => $cartTotalQuantity, 'items' => $cartCollection, 'itemsCount' => $items]);
    }

    public function incrementToCartItem(Request $request) {
        Cart::update($request['id'], [
            'quantity' => 1,
        ]);
        $cartCollection = Cart::getContent();
        $cartTotalQuantity = Cart::getTotalQuantity();
        $total = Cart::getTotal();
        $item_quantity = Cart::get($request['id']);
        $item_quantity = $item_quantity['quantity'];

        return response()->json([
            'type' => 'success',
            'message' => 'Item Added to Cart Successfully',
            'quantity' => $cartTotalQuantity,
            'items' => $cartCollection,
            'item_quantity' => $item_quantity,
            'total' => $total,
        ]);
    }

    public function decrementToCartItem(Request $request) {
        Cart::update($request['id'], [
            'quantity' => -1,
        ]);
        $cartCollection = Cart::getContent();
        $cartTotalQuantity = Cart::getTotalQuantity();
        $total = Cart::getTotal();
        $item_quantity = Cart::get($request['id']);
        $item_quantity = $item_quantity['quantity'];
        return response()->json([
            'type' => 'success',
            'message' => 'Item Added to Cart Successfully',
            'quantity' => $cartTotalQuantity,
            'items' => $cartCollection,
            'item_quantity' => $item_quantity,
            'total' => $total,
        ]);
    }

    public function updateCartItemSize(Request $request) {
        Cart::update($request['id'], [
            'attributes' => array(
                'size' => $request['size_id']
            )
        ]);
        return response()->json([
            'type' => 'success',
            'message' => 'Item Size Updated Successfully',
        ]);
    }

    public function deleteCartItem(Request $request) {
        Cart::remove($request['id']);
        $cartCollection = Cart::getContent();
        $cartTotalQuantity = Cart::getTotalQuantity();
        $total = Cart::getTotal();
        return response()->json([
            'type' => 'success',
            'message' => 'Item Added to Cart Successfully',
            'quantity' => $cartTotalQuantity,
            'items' => $cartCollection,
            'total' => $total,
        ]);
    }
}
