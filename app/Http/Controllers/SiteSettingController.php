<?php

namespace App\Http\Controllers;

use App\SiteSetting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SiteSettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $breadcrumb = 'Site Settings';
        $settings = SiteSetting::all();
        $side_bar_color = SiteSetting::where('key', 'side_bar_color')->first()->data;
        $header_color = SiteSetting::where('key', 'header_color')->first()->data;
        $text_color = SiteSetting::where('key', 'text_color')->first()->data;

        $facebook_link = SiteSetting::where('key', 'facebook_link')->first()->data ?? 'www.google.com';
        $instagram_link = SiteSetting::where('key', 'instagram_link')->first()->data ?? 'www.google.com';
        $twitter_link = SiteSetting::where('key', 'twitter_link')->first()->data ?? 'www.google.com';
        $linkedIn_link = SiteSetting::where('key', 'linkedIn_link')->first()->data ?? 'www.google.com';
        $cc_email = SiteSetting::where('key', 'cc_email')->first()->data ?? 'mail@gmail.com';
        return view('admin.site-settings.index', compact(
            'settings',
            'side_bar_color',
            'header_color',
            'text_color',
            'breadcrumb',
            'facebook_link',
            'instagram_link',
            'twitter_link',
            'linkedIn_link',
            'cc_email'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SiteSetting  $siteSetting
     * @return \Illuminate\Http\Response
     */
    public function show(SiteSetting $siteSetting)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SiteSetting  $siteSetting
     * @return \Illuminate\Http\Response
     */
    public function edit(SiteSetting $siteSetting)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SiteSetting  $siteSetting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SiteSetting $siteSetting)
    {
        //
        if ($request->has('header_logo')) {
            $imageName = time().'.'.$request->header_logo->extension();
            $request->header_logo->move(public_path('uploads/logo'), $imageName);
            SiteSetting::where('key', 'header_logo')->update(['data' => '/uploads/logo/'.$imageName]);
        }

        SiteSetting::where('key', 'side_bar_color')->update(['data' => $request->side_bar_color]);
        SiteSetting::where('key', 'header_color')->update(['data' => $request->header_color]);
        SiteSetting::where('key', 'text_color')->update(['data' => $request->text_color]);


        SiteSetting::where('key', 'facebook_link')->update(['data' => $request->facebook_link]);
        SiteSetting::where('key', 'instagram_link')->update(['data' => $request->instagram_link]);
        SiteSetting::where('key', 'twitter_link')->update(['data' => $request->twitter_link]);
        SiteSetting::where('key', 'linkedIn_link')->update(['data' => $request->linkedIn_link]);
        SiteSetting::where('key', 'cc_email')->update(['data' => $request->cc_email]);



        return redirect()->route('dashboard', ['role' => Auth::user()->roles[0]->name]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SiteSetting  $siteSetting
     * @return \Illuminate\Http\Response
     */
    public function destroy(SiteSetting $siteSetting)
    {
        //
    }
}
