@extends('admin.layouts.app')
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    @if(session()->has('message'))
                        <div class="alert alert-success mt-5" id="myElem" >
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-12 mt-5">
                            <div class="card">
                                <div class="card-body">
                                    <a href="{{route('admin.auth.codes.create')}}"  class="btn btn-primary" ><i class="fa fa-plus"></i>Add New Code </a>
                                </div>
                                <div class="card-body">
                                    <table class="table table-hover m-0 tickets-list table-actions-bar dt-responsive nowrap" width="100%" id="normal-table">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Code</th>
                                            <th>Status</th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($codes as $employee)
                                            <tr>
                                                <td>{{$employee->id}}</td>
                                                <td>{{$employee->code}}</td>
                                                <td>{{$employee->status}}</td>
                                                <td>
                                                    <div class="btn-group">
                                                        <a class="btn" href="{{ route('admin.auth.codes.delete', ['id' => $employee->id] ) }}">
                                                            <i class="fa fa-close"></i>
                                                        </a>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div> <!-- end card -->
                            </div> <!-- end card -->
                        </div><!-- end col -->
                    </div>

                </div>
            </div>

        </div> <!-- container -->
    </div> <!-- content -->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

    @include('admin.layouts.footer')
@endsection
