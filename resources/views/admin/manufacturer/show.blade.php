@extends('admin.layouts.app')
@section('content')
    <div class="content">
        <div class="container-fluid" style="margin-top: 75px;">
            <div class="row">
                <div class="col-12">
                    @if(session()->has('message'))
                        <div class="alert alert-success mt-5" id="myElem" >
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            {{ session()->get('message') }}
                        </div>
                    @endif
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-3">
                                        <img src="{{asset($user->profile->image)}}" width="200px" class="img-circle" id="image" >

                                    </div>
                                    <div class="col-lg-9">
                                        <h5>{{$user->name}}</h5>
                                    </div>
                                </div>
                            </div>
                            <div class="">
                                <div class="card-box">
                                    <form method="POST" action="{{ route('admin.manufacturers.update', ['id' => $user->id]) }}" enctype="multipart/form-data">
                                        <div class="col-lg-12 mt-5">
                                            <div class="card-box">
                                                @csrf
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label for="first_name">Manufacturer Name<span class="text-danger">*</span></label>
                                                            <input disabled type="text" class="form-control valid" name="first_name" value="{{$user->first_name}}" id="first_name" required />
                                                            @if ($errors->has('first_name'))
                                                                <span class="error">{{ $errors->first('first_name') }}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label for="area_id">Job Description<span class="text-danger">*</span></label>
                                                            <input disabled placeholder="Job Description Exp: Director" type="text" class="form-control valid" name="job_description" value="{{$user->job_description}}" value="" id="job_description"  />
                                                            @if ($errors->has('job_description'))
                                                                <span class="error">{{ $errors->first('job_description') }}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label for="email">Email<span class="text-danger">*</span></label>
                                                            <input disabled type="email" class="form-control valid" name="email" id="email" value="{{$user->email}}" required />
                                                            @if ($errors->has('email'))
                                                                <span class="error">{{ $errors->first('email') }}</span>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label for="web_url">Web URL</label>
                                                            <input disabled type="text" class="form-control valid" name="web_url" value="{{$user->profile->web_url}}" id="web_url"/>
                                                            @if ($errors->has('web_url'))
                                                                <span class="error">{{ $errors->first('web_url') }}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label for="address">Address<span class="text-danger">*</span></label>
                                                            <input disabled type="text" class="form-control valid" name="address" value="{{$user->profile->address}}" id="address" required />
                                                            @if ($errors->has('address'))
                                                                <span class="error">{{ $errors->first('address') }}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label for="phone">Phone<span class="text-danger">*</span></label>
                                                            <input disabled type="tel" class="form-control valid" name="phone" value="{{$user->phone}}" id="phone" required />
                                                            @if ($errors->has('phone'))
                                                                <span class="error">{{ $errors->first('phone') }}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div id="representative">
                                                            @foreach($user->representatives as $index => $representative)
                                                                <div id="r{{$index}}" class="row">

                                                                    <div class="col-sm-4">
                                                                        <div class="form-group">
                                                                            <label for="location_id">Represent Name </label>
                                                                            <input disabled placeholder="Please Represent Name" type="text" class="form-control valid" name="represent_name[]" value="{{$representative->name}}" id="represent_name"  />
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-4">
                                                                        <div class="form-group">
                                                                            <label for="area_id">Represent Email </label>
                                                                            <input disabled placeholder="Please enter represent email" type="text" class="form-control valid" name="represent_email[]" value="{{$representative->email}}" id="represent_email"  />
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-4">
                                                                        <div class="form-group">
                                                                            <label for="area_id">Represent Mobile </label>
                                                                            <input disabled placeholder="Please enter represent mobile" type="text" class="form-control valid" name="represent_mobile[]" value="{{$representative->mobile}}" id="represent_mobile"  />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                    <div class="col-12">
                                                        <div class="form-group">
                                                            <label for="Description">Manufacturer Description<span class="text-danger">*</span></label>
                                                            <textarea disabled id="Description" class="form-control rounded-0" placeholder="Exp: We are manufacturer of office furniture" name="description" rows="5">{{$user->profile->description}}</textarea>
                                                            @if ($errors->has('description'))
                                                                <span class="error">{{ $errors->first('description') }}</span>
                                                            @endif
                                                        </div>
                                                    </div>


                                                    <div class="col-12">
                                                        <div class="form-group">
                                                            <label for="company_keywords">Company Keywords<span class="text-danger">*</span></label>
                                                            <textarea disabled class="form-control rounded-0" placeholder="" id="company_keywords" name="company_keywords" rows="5">{{$user->profile->company_keywords}}</textarea>
                                                            @if ($errors->has('company_keywords'))
                                                                <span class="error">{{ $errors->first('company_keywords') }}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>

                                            </div> <!-- end card-box -->
                                        </div>
                                    </form>
                                </div> <!-- end card-box -->
                            </div>

                        </div> <!-- container -->
                </div>
            </div>
        </div>
    </div>
@endsection
