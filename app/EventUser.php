<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventUser extends Model
{
    //

    protected $guarded = [];

    public function event() {
        return $this->belongsTo(Event::class, 'event_id')->with('users')->with('comments');
    }

    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }
}
