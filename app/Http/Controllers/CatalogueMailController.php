<?php

namespace App\Http\Controllers;

use App\catalogueMail;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CatalogueMailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\catalogueMail  $catalogueMail
     * @return \Illuminate\Http\Response
     */
    public function show(catalogueMail $catalogueMail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\catalogueMail  $catalogueMail
     * @return \Illuminate\Http\Response
     */
    public function edit(catalogueMail $catalogueMail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\catalogueMail  $catalogueMail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, catalogueMail $catalogueMail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\catalogueMail  $catalogueMail
     * @return \Illuminate\Http\Response
     */
    public function destroy(catalogueMail $catalogueMail)
    {
        //
    }
}
