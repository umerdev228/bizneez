<?php

namespace App\Http\Controllers;

use App\Category;
use App\Media;
use App\Profile;
use App\Represent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use App\User;
class DealerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $dealers = User::role('dealer')->with('profile')->with('representatives')->get();
        $breadcrumb = ucfirst(\Illuminate\Support\Facades\Auth::user()->roles[0]->name)." / Dealer / New";

        return view('admin.dealer.index', compact('breadcrumb' , 'dealers'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categories = Category::all();
        $breadcrumb = ucfirst(\Illuminate\Support\Facades\Auth::user()->roles[0]->name)." / Dealer / Add New Dealer";

        return view('admin.dealer.create', compact('breadcrumb', 'categories'  ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validated = $request->validate([
            'name' => 'required',
            'email' => 'required',
        ]);
        $user = User::where('email', $request['email'])->first();
        if ($user == null) {
            User::create([
                "name" => $request['name'],
                "email" => $request['email'],
                "password" => Hash::make($request->password),
                "created_by" => Auth::id(),
            ]);
            $user = User::where('email', $request['email'])->first();
            $user->assignRole('dealer');
            Profile::create([
                "user_id" => $user->id,
                "phone" => $request['phone'],
                "address" => $request['address'],
                "description" => $request['description'],
                "company_keywords" => $request['company_keywords'],
                "web" => $request['web'],
            ]);
            if ($request->has('image')) {
                $imageName = time().'.'.$request->image->extension();
                $request->image->move(public_path('/uploads/users/dealers'), $imageName);
                Profile::where('user_id', $user->id)->update([
                    "image" => '/uploads/users/dealers/' . $imageName,
                ]);
            }
            foreach ($request->represent_name as $index => $item) {

                Represent::create([
                    'user_id' => $user->id,
                    'name' => $request->represent_name[$index],
                    'email' => $request->represent_email[$index],
                    'mobile' => $request->represent_mobile[$index],
                    'job_description' => $request->job_description[$index],
                ]);
            }
//            Mail::to($user->email)->send(new \App\Mail\MerchantActivation($user->name, '', '', '', $request['password']));
        }
        toastr()->success('Dealers Created Successfully');
        return redirect()->route('admin.dealers.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Dealer  $dealer
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $breadcrumb = ucfirst(\Illuminate\Support\Facades\Auth::user()->roles[0]->name)." / Manufacturer / Show";
        $id = (\request()->route()->parameter('id'));
        $user = User::with('roles')->where('id', $id)->with('profile')->first();
        return view('admin.dealer.show', compact('user', 'breadcrumb'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Dealer  $dealer
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

        $dealer = User::role('dealer')->where('id', $id)->with('profile')->with('representatives')->first();
        $breadcrumb = ucfirst(\Illuminate\Support\Facades\Auth::user()->roles[0]->name)." / Dealer / New";

        return view('admin.dealer.edit', compact('breadcrumb' , 'dealer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Dealer  $dealer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validated = $request->validate([
            'name' => 'required',
            'email' => 'required',
        ]);
        User::where('id', $id)->update([
            "name" => $request['name'],
            "email" => $request['email'],
            "password" => Hash::make($request->password),
            "created_by" => Auth::id(),
        ]);
        if ($request->password) {
            User::where('id', $id)->update([
                "password" => Hash::make($request->password),
            ]);
        }
        Profile::where('user_id', $id)->update([
            "user_id" => $id,
            "phone" => $request['phone'],
            "address" => $request['address'],
            "description" => $request['description'],
            "company_keywords" => $request['company_keywords'],
            "web" => $request['web'],
        ]);
        if ($request->has('image')) {
            $imageName = time().'.'.$request->image->extension();
            $request->image->move(public_path('/uploads/users/dealers'), $imageName);
            Profile::where('user_id', $id)->update([
                "image" => '/uploads/users/dealers/' . $imageName,
            ]);
        }
        Represent::where('user_id', $id)->delete();
        foreach ($request->represent_name as $index => $item) {
            Represent::where('user_id', $id)->create([
                'user_id' => $id,
                'name' => $request->represent_name[$index],
                'email' => $request->represent_email[$index],
                'mobile' => $request->represent_mobile[$index],
                'job_description' => $request->job_description[$index],
            ]);
        }
//            Mail::to($user->email)->send(new \App\Mail\MerchantActivation($user->name, '', '', '', $request['password']));

        toastr()->success('Dealer Updated Successfully');
        return redirect()->route('admin.dealers.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Dealer  $dealer
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        User::where('id', $id)->delete();
        Profile::where('user_id', $id)->delete();
        Represent::where('user_id', $id)->delete();
        toastr()->success('Dealer Deleted Successfully');
        return redirect()->route('admin.dealers.index');
    }

    public function dealers(){

        //

    }

    public function dealer_add(){

    }
}
