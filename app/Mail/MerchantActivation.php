<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MerchantActivation extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
   public $name;
   public $url;
   public $title;
   public $token;
   public $password;


    public function __construct($name,$title,$url,$token, $password)
    {
         
        $this->name = $name;
        $this->url = $url;
        $this->title = $title;
        $this->token = $token;
        $this->password = $password;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
 
        return $this->from('info@Bizneez.com')->view('mail.new_user_welcome', ['url' => $this->url, 'token' => $this->token , 'name'=>$this->name, 'password' => $this->password]);
         //   return view('pages_merchant_admin.customer_list', compact('customer_list','title'));
    }
}
