@extends('admin.layouts.app')
@section('content')
@include('admin.layouts.topbar')
<!-- Google Chart JS -->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<!-- Start Page content -->
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">
        
@if(session()->has('message'))
    <div class="alert alert-success">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
        {{ session()->get('message') }}
    </div>
@endif   
@if(session()->has('error'))
    <div class="alert alert-danger" > 
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
        {{ session()->get('error') }}
    </div>
@endif 
      <!-- <a href="{{url('admin/setting/potal/update/{id}')}}">Update</a> -->
        <div class="row mt-5">
          <div class="col-9">
            <div class="card-box">
              <form action="{{ url('admin/setting/potal/update') }}" method="post"> 
                {{ csrf_field() }}
                <div class="form-row">
                  <div class="form-group col-md-12">
                    <label for="Name">Name<span class="text-danger">*</span></label>
                    <input type="text"   name="Name" value="{{$user->name}}" autocomplete="off"  class="form-control" parsley-trigger="change" required="required">
                      @if ($errors->has('Name'))
                              <span class="error">{{ $errors->first('Name') }}</span>
                      @endif   
                    </div>
                    <div class="form-group col-md-12">
                    <label for="Email_ID">Email<span class="text-danger">*</span></label>
                    <input type="text" name="Email_ID"  value="{{$setting->email}}" autocomplete="off" class="form-control" parsley-trigger="change" required="required" />
                      @if ($errors->has('Email_ID'))
                              <span class="error">{{ $errors->first('email') }}</span>
                      @endif   
                    </div>
                    <div class="form-group col-md-12">
                    <label for="Stripe_ID">Stripe ID<span class="text-danger">*</span></label>
                    <input type="text" name="Stripe_ID" value="{{$setting->stripe_email}}"  autocomplete="off" class="form-control" parsley-trigger="change" required="required" />
                      @if ($errors->has('Stripe_ID'))
                              <span class="error">{{ $errors->first('stripe_email') }}</span>
                      @endif   
                    </div>
                     <div class="form-group col-md-12">
                    <label for="Stripe_Password">Stripe Password<span class="text-danger">*</span></label>
                    <input type="text" name="Stripe_Password"  value="{{$setting->stripe_password}}" autocomplete="off" class="form-control" parsley-trigger="change" required="required" />
                      @if ($errors->has('Stripe_Password'))
                              <span class="error">{{ $errors->first('Stripe_Password') }}</span>
                      @endif   
                    </div>
                    <div class="form-group col-md-12">
                    <label for="Address">Address<span class="text-danger">*</span></label>
                    <input type="text" name="Address" value="{{$setting->address}}"  autocomplete="off" class="form-control" parsley-trigger="change" required="required" />
                      @if ($errors->has('Address'))
                              <span class="error">{{ $errors->first('Address') }}</span>
                      @endif   
                    </div>
                    <div class="form-group col-md-12">
                    <label for="Phone">Phone<span class="text-danger">*</span></label>
                    <input type="text" name="Phone" value="{{$setting->phone}}"  autocomplete="off" class="form-control" parsley-trigger="change" required="required" />
                      @if ($errors->has('Phone'))
                              <span class="error">{{ $errors->first('Phone') }}</span>
                      @endif   
                    </div>
                      <div class="form-group col-md-12">
                      <label for="Description">Description</label>
                       <textarea class="form-control rounded-0" name="description"  id="exampleFormControlTextarea2" rows="5">{{$setting->description}}</textarea>
                      @if ($errors->has('description'))
                      <span class="error">{{ $errors->first('description') }}</span>
                      @endif
                    </div>
                 </div>
                <button type="submit" class="btn btn-primary">Update</button>
                <button onclick="history.back()" type="button" class="btn">Cancel</button>
              </form>
             
              </div> <!-- end card-box -->
            </div>
            
            </div> <!-- container -->
          </div>
        </div>
      </div>
    </div>
              @include('admin.layouts.footer')
            @endsection
            <style>
            .arrow-none{margin-top:20px!important;}
            .alert-success{width: 800px;}
            .alert-danger{width: 800px;}
            </style>