@extends('admin.layouts.app')
@section('content')
@include('admin.layouts.topbar')
<!-- Google Chart JS -->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<!-- Start Page content -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                @if(session()->has('message'))
    <div class="alert alert-success  mt-5" id="myElem"  >
    
         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
        {{ session()->get('message') }}
    </div>
@endif 



@if(session()->has('error'))
    <div class="alert alert-danger" > 
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
        {{ session()->get('error') }}
    </div>
@endif 
                    
                    <div class="row">
                        <div class="col-12 mt-5">
                            <div class="card-box">
                              <a href="{{url('admin/packages/create')}}" class="btn"><i class="fa fa-plus"></i> Add New Package</a>
                              <a href="{{url('admin/option')}}" class="btn"><i class="fa fa-plus"></i>   Package Option</a>


                              <table class="table table-hover m-0 tickets-list table-actions-bar dt-responsive nowrap" cellspacing="0" width="100%" id="datatable">
                              <thead>
                                <tr>
                                  <th>Package</th>
                                  <th width="300">Description</th>  
                                  <th>Duration</th>
                                  <th>Price</th>
                                  <th >Active</th>  
                                  
                                  <th class="hidden-sm" data-orderable="false"
>Action</th>
                                </tr>
                              </thead>
                              <tbody>
                               
                                  
                                  @foreach($packages as $pak)
                                  <tr> 
                                  <td><a href="javascript: void(0);"> 

                                  <span class="ml-2">{{$pak->name}}</span> </a></td>
                                   <td width="300"> {!! substr($pak->description,0,50) !!}</td>  
                                  <td>{{$pak->package_duration}}</td>
                                  <td>{{$pak->amount}}</td>
                                  <td>@if($pak->status==0) Inactive @else Active @endif</td> 
                                  
                              
                                   
                                  <td><div class="btn-group dropdown">  
                                     <a href="{{ URL::to('admin/packages/edit/' . $pak->id ) }}">Update</a> 
                                  </div>
                                    </div></td>
                                </tr>
                                  @endforeach
                            </tbody>
                            </table>
                                
                                
                                    </div> <!-- end card -->
                                    </div><!-- end col -->
                                </div>
                                
                            </div>
                        </div>
                        
                                </div> <!-- container -->
                                </div> <!-- content -->
                                
                                @include('admin.layouts.footer')
                                @endsection
                       