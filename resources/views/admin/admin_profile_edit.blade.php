@extends('admin.layouts.app')
@section('content')
@include('admin.layouts.topbar')
<!-- Google Chart JS -->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<!-- Start Page content -->
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">
        
@if(session()->has('message'))
    <div class="alert alert-success">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
        {{ session()->get('message') }}
    </div>
@endif   
@if(session()->has('error'))
    <div class="alert alert-danger" > 
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
        {{ session()->get('error') }}
    </div>
@endif 
      <!-- <a href="{{url('admin/setting/potal/update/{id}')}}">Update</a> -->
        <div class="row mt-5">
          <div class="col-9">
            <div class="card-box">
              <form action="{{ route('AdminProfileUpdate') }}" method="post"> 
                {{ csrf_field() }}
                <div class="form-row">
                  <div class="form-group col-md-12">
                    <label for="Name">Name<span class="text-danger">*</span></label>
                    <input type="text"   name="Name" value="{{$user->name}}" autocomplete="off"  class="form-control" parsley-trigger="change" required="required">
                      @if ($errors->has('Name'))
                              <span class="error">{{ $errors->first('Name') }}</span>
                      @endif   
                    </div>
                    
                 </div>
                <button type="submit" class="btn btn-primary">Update</button>
                <button onclick="history.back()" type="button" class="btn">Cancel</button>
              </form>
             
              </div> <!-- end card-box -->
            </div>
            
            </div> <!-- container -->
          </div>
        </div>
      </div>
    </div>
              @include('admin.layouts.footer')
            @endsection
            <style>
            .arrow-none{margin-top:20px!important;}
            .alert-success{width: 800px;}
            .alert-danger{width: 800px;}
            </style>