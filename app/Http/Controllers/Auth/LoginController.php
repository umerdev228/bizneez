<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
 

    use AuthenticatesUsers;
 
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
//    protected $redirectTo = RouteServiceProvider::HOME;



    public function redirectTo() {
        $role = (\Illuminate\Support\Facades\Auth::user()->roles[0]->name);
        return redirect()->route('dashboard', ['role' => $role]);
    }

    public function showLoginForm()
    {
        return view('signin');
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
         $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        $credentials = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);

        if ($login =  Auth::attempt($credentials)) {
            \App\User::where('id', Auth::id())->update(['status' => 'offline']);

            $request->session()->regenerate();

            return redirect('login')->with('message' , 'please sign up first');
        }
        else
        {
            toastr()->error('Please enter correct email and password');

            return view('signin')->with(['error' => 'Please enter correct email and password.', 'credential' => $request]);
        }
    }



}
