@extends('admin.layouts.app')
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    @if(session()->has('message'))
                        <div class="alert alert-success mt-5" id="myElem" >
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            {{ session()->get('message') }}
                        </div>
                    @endif

                    <form action="{{ route('admin.companies.setdefault') }}" method="post">  @csrf
                        <input type="hidden" value="" name="company_id" id="company_id">

                    <div class="row">
                        <div class="col-12 mt-5">
                            <div class="card">
                                <div class="card-body">
                                    <a href="{{route('admin.companies.create')}}"  class="btn btn-primary" ><i class="fa fa-plus"></i>Add New Company </a>
                                </div>
                                <div class="card-body">
                                    <table class="table table-hover m-0 tickets-list table-actions-bar dt-responsive nowrap" width="100%" id="normal-table">
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Company</th>
                                            <th>Logo</th>
                                            <th>Address</th>
                                            <th>Phone</th>
                                            <th>URL</th>
{{--                                            <th>Company Registration No</th>--}}
{{--                                            <th>Term And Condition</th>--}}
{{--                                            <th>Description</th>--}}
                                            <th data-orderable="false">Action</th>
                                        </tr>
                                        </thead>  
                                        <tbody>
                                        @foreach($companies as $company)
                                            <tr>
                                                <td>{{$company->name}}</td>
                                                <td><input @if($company->by_default==1) checked @endif type="radio" value="{{$company->id}}" name="rdo" > By Default</td>
                                                <td><img src="{{asset($company->logo)}}" width="100"></td>
                                                <td>{{$company->address}}</td>
                                                <td>{{$company->phone}}</td>
                                                <td>{{$company->url}}</td>
{{--                                                <td>{{$company->company_registration_no}}</td>--}}
{{--                                                <td>{{$company->term_and_condition}}</td>--}}
{{--                                                <td>{{$company->description}}</td>--}}
                                                <td>
                                                    <div class="btn-group">
                                                        <a class="btn" href="{{ route('admin.companies.edit', ['id' => $company->id] ) }}">
                                                            <i class="mdi mdi-pencil"></i>
                                                        </a>
                                                        <a class="btn" href="{{ route('admin.companies.delete', ['id' => $company->id] ) }}">
                                                            <i class="fa fa-close"></i>
                                                        </a>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>
    $( document ).ready(function() {
    $('input[type=radio]').on('change', function() {   
    $('#company_id').val(this.value);   
    $(this).closest("form").submit();
});
    });
</script>