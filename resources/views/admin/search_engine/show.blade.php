@extends('admin.layouts.app')
@section('content')
    @include('admin.layouts.topbar')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    @if(session()->has('message'))
                        <div class="alert alert-success mt-5" id="myElem" >
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    <br><br><br><br><br>
                    <form action="{{route('admin.advertisement.update', ['id' => $advertisement->id])}}" method="post">
                        @csrf

                        <input disabled style="display: none;" type="text" name="image" value="{{$advertisement->image}}"  id="images">
                        <input disabled style="display: none;" type="text" name="shape" value="{{$advertisement->shape}}"  id="shape">

                        <div class="row">
                            <div class="col-md-4">
                                <div>
                                    <h3>Search Engine/Advertisement</h3>
                                    <div class="form-group">
                                        <select disabled name="platform" class="form-control">
                                            <option @if($advertisement->platform === '1') selected @endif value="1">Direct Marketing</option>
                                            <option @if($advertisement->platform === '2') selected @endif value="2">Home page</option>
                                            <option @if($advertisement->platform === '3') selected @endif value="3">Search Engine</option>
                                            <option @if($advertisement->platform === '4') selected @endif value="4">Sign in Page</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix">
                                    <br><br><br><br>
                                </div>
                                <div class="well">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div id="datetimepicker1" class="input-append date">
                                                <input disabled value="{{$advertisement->date}}" data-format="dd/MM/yyyy hh:mm:ss" type="date" name="date">
                                                <span class="add-on">
                                                        <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                                                        </i>
                                                    </span>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div id="datetimepicker1" class="input-append date">
                                                <input disabled value="{{$advertisement->time}}" data-format="hh:mm:ss" type="time" name="time">
                                                <span class="add-on">
                                                        <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                                                        </i>
                                                    </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group" >
                                    <img width="200px" id="uploadedImage" src="{{asset($advertisement->media->path)}}"  data-toggle="modal" data-target="#imageGallery">
                                    <input disabled value="{{$advertisement->title}}" name="title" type="text" class="w-75 form-control" id="exampleFormControlInput1" placeholder="title">
                                </div>
                                <div class="form-group">
{{--                                    <div align="center"><button type="submit" class="btn btn-primary mb-2">Update</button></div>--}}
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="card">
                                    <div class="card-header">Choose Shape</div>
                                    <div @if($advertisement->shape === 'square') style="border: 2px solid red" @endif id="square" class="square"></div>
                                    <div @if($advertisement->shape === 'vertical-banner') style="border: 2px solid red" @endif id="vertical-banner" class="vertical-banner"></div>
                                    <div @if($advertisement->shape === 'horizontal-banner') style="border: 2px solid red" @endif id="horizontal-banner" class="horizontal-banner"></div>
                                </div>
                                {{--                                    <img src="{{asset('assets/images/shap.png')}}">--}}
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="imageGallery" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    Image Gallery
                </div>
                <div class="modal-body">
                    <div class="col-12">
                        <div class="row">
                            @foreach($media as $image)
                                <div id="{{$image->id}}" class="col- border-success m-2 ">
                                    <img class="" onclick="selectImages({{$image->id}}, {{$image}})" width="150px" src="{{asset($image->path)}}" alt="" srcset="">
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="float-right">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">Done</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
<script !src="">
    let public_path = '{{asset('/')}}';
    let selectedImages = []
    let lastSelectedImage = ''
    let lastShapeSelected = ''
    function selectImages(id, image) {
        $('#uploadedImage').attr('src', public_path+image.path);
        if (lastSelectedImage !== '') {
            $('#'+lastSelectedImage).css('border', '')
        }
        lastSelectedImage = id
        $('#'+id).css('border', '2px solid red')
        $('#images').val(id)
    }

    function selectShape(id){
        if (lastShapeSelected !== '') {
            $('#'+lastShapeSelected).css('border', '2px solid black')
        }
        lastShapeSelected = id
        $('#'+id).css('border', '2px solid red')
        $('#shape').val(id)
    }

</script>

<style>
    .fa-custom{  font-size: 200px!important;}
    .square {
        margin: 4px;
        height: 100px;
        width: 100px;
        border: 2px solid black;
    }
    .vertical-banner {
        margin: 4px;
        height: 100px;
        width: 200px;
        border: 2px solid black;
    }
    .horizontal-banner {
        margin: 4px;
        height: 200px;
        width: 100px;
        border: 2px solid black;
    }
</style>