<?php

namespace App\Http\Controllers;
use App\Categories;
use App\Employee;
use App\Order;
use App\Product;
use Illuminate\Support\Facades\File;
//use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
//use App\Customer;
use App\User;
use App\Merchant;
use App\PaymentHistory;
//use App\Timezone;
use Auth;
use Mail;
use Stripe;
// use Email;
// use Name;
//use App\Notification;
//use App\History;


 

class DashboardController extends Controller
{
     
     /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function index() {
        $manufacturer = User::role('manufacturer')->count();
        $architect = User::role('architect')->count();
        $client = User::role('client')->count();
        $employee = User::role('employee')->count();
        $categories = Categories::count();
        if (Auth::user()->hasRole(['manufacturer'])) {
            $product = Product::where('user_id', Auth::id())->count();
            $orders = Order::where('user_id', Auth::id())->count();
        }
        else {
            $product = Product::count();
            $orders = Order::count();
        }
        $total_payment=PaymentHistory::sum('amount');
        $breadcrumb = 'Dashboard';
        return view('admin.dashboard',
            compact(
                'manufacturer',
                'architect',
                'client',
                'breadcrumb',
                'total_payment',
                'categories',
                'employee',
                'product',
                'orders'
            ));
    }


    public function ajaxNotification(){
     $notification = \App\History_log::where('intstatus',0)->orderBy('id', 'desc')->get();

$html_data = '';
     
                 foreach($notification as $data){ 
                   if($data->type == 1)
                       $notification_msg = 'New user registered(Trail)'; 
                   else
                       $notification_msg = 'Prenium User'; 
                    
                  $url ="admin/user/view/".$data->user_id ;
              $html_data .= '    <a href="'. url( $url ).'" class="dropdown-item notify-item">
                 <div class="notify-icon bg-info"><i class="mdi mdi-account-plus"></i></div>
                 <p class="notify-details">'. $notification_msg .'<small class="text-muted"> '. $data->description .'</small></p> </a>'; 
}
                  
         return $html_data;


}    
 




 
     /**
     * Insert resource in database/create new agent.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
         
 
        request()->validate([ 
            'email' => 'required|unique:users',
          //'phone' =>'required',
            'phone' => 'required',
            'company' => 'required', 
            'fname' => 'required',
            'lname' => 'required',

            ]);

            $data_user = [
                 'email' => $request->post('email'),
                 'name' => $request->post('fname') .' '.$request->post('lname') ,
                 'role' =>1
                  ];
            $user = User::create($data_user);    //insertion into user table for Auth Gate
            $lastInsertedId= $user->id;


          //  $password = Hash::make('test');
            $description =  '';
            $token = md5(rand());
            $Today=date('y:m:d'); 
            // add 3 days to date
            $expire_date=Date('y:m:d', strtotime('+15 days'));
            $data = [
                'email' => $request->post('email'), 
                'user_id' =>$lastInsertedId,  
                'name' => $request->post('fname') .' '.$request->post('lname') ,
                'phone' => $request->post('phone'),  
                'company' => $request->post('company'), 
                'token' => $token,
                'expire_date' =>$expire_date,
                'inttype' => 0,  
                'profile_status' =>0 
            ];
            Merchant::create($data); 
            //print_r($data); die();
            //create stripe account for new signup user
            /*Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
            $create_customer = Stripe\Customer::create([
                    'description' => 'TMS signup account',
                    'email' => $request->post('email'),
                    'name' => $request->post('fname') .' '.$request->post('lname'),
                    "source" => $request->stripeToken,
                ]);*/

            //===============end====================================

               //send email to signup user
                $name =   $request->post('name');
                $title = "TMS New Signup";
                $url = env('APP_URL');
                    Mail::to($request->post('email'))->send(new \App\Mail\MerchantRegistration($name, $title, $url, $token));  
                    return redirect('signup')->with('message', 'User created successfully, Please check your email and set password.');
                    //return redirect()->back()->with('message', 'Customer Created!'); 

}

 

public function verify(Request $request){ 

       if(Merchant::where('token',$request->get('v'))->count()){
             $merchant = Merchant::where('token', $request->get('v') )->first();  
             $data= [
                    'merchant' => $merchant,
                    'breadcrumb' => 'Generate Password'
                ];
                return view('verify')->with($data); 
       }
       else
       {
                return redirect('signin')->with('message', 'Link is expired or you have already created password, please login');
       }
   

      
}




public function reset_password(Request $request){  
      // return view('admin.password');
     $merchant = Merchant::where('user_id', $request->post('user_id') )->first(); 

     $user = User::findOrFail($request->post('user_id')); 
     $user->password = Hash::make($request->post('password'));  
     $user->save();

     $merchant->token = '';
     $merchant->save();
     return redirect('signin')->with('message', 'You are setup password please login');
      
}
public function Search(Request $request)
    {
        // dd('sadasd');

       //  $query = $request->get('query');

       //  $pages = Email::where('Email', 'LIKE', "%$query%")->get();

       //  $posts = Name::where('Email', 'LIKE', "%$query%")
       //              ->orWhere('Name', 'LIKE', "%$query%")
       //              ->get();

       // return view('dashboard', compact('pages', 'posts'));
      // {
      //  $data = DB::table('tbl_students')
      //    ->where('name', 'like', '%'.$query.'%')
      //    ->orWhere('email', 'like', '%'.$query.'%')
      //    ->orWhere('address', 'like', '%'.$query.'%')
      //    ->get();
         
      // }
      // else
      // {
      //  $data = DB::table('tbl_students')
      //    ->orderBy('id', 'desc')
      //    ->get();
      // }
    } 


   
  


 
}
