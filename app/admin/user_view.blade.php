@extends('admin.layouts.app')
@section('content')
@include('admin.layouts.topbar')
<!-- Google Chart JS -->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<!-- Start Page content -->
<div class="content">
                    <div class="container-fluid"> 
                        <div class="row">
                            <div class="col-sm-12 mt-5">
                                <!-- meta -->
                                <div class="profile-user-box card-box bg-info">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <span class="float-left mr-3">{{-- <img src="assets/images/users/avatar-2.jpg" alt="" class="thumb-lg rounded-circle"> --}} 
                                            @if($user->profile_pic=='') 
                                            <img src="{{url('/')}}/theme/admin/assets/images/users/avatar-1.jpg" alt="contact-img" title="contact-img" class="thumb-lg rounded-circle" />
                                            @else 
                                            <img src="{{url('/')}}/assets/images/users/{{$user->profile_pic}}" alt="contact-img" title="contact-img" class="thumb-lg rounded-circle" />
                                            @endif  
                                            </span>
                                            <div class="media-body text-white">
                                                <h4 class="mt-4 mb-1 font-16">{{$user->name}}</h4>
                                                <p class=" mt-0 mb-1 font-16">
                                                 <span class="m-l-15">{{$user->email}}</span></p>
                                            
                                                <!-- <p class="font-13 text-light"> User Experience Specialist</p>
                                                <p class="text-light mb-0">California, United States</p> -->
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="text-right mt-4">
                                                <button onclick="location.href='{{ URL::to('admin/user/edit/' . $user->user_id ) }}' " type="button" class="btn btn-light waves-effect">
                                                <i class="mdi mdi-account-settings-variant mr-1"></i> Edit Profile
                                                </button>
    <!-- <button  onclick="location.href='{{URL::to('send_payment' )}}' " type="button" class="btn btn-light waves-effect">
                                                    <i class="mdi mdi-account-settings-variant mr-1"></i> Send Payment Request
                                                </button> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--/ meta -->
                            </div>
                        </div>
                        <!-- end row -->
                        <div class="row">
                            <div class="col-xl-8 ">
                                <!-- Personal-Information -->
                                <div class="card-box">
                                    <h4 class="header-title mt-0 m-b-20">Personal Information 
                                        @if($user->inttype==1)
                                        <span style="color:red">(Premium)</span>
                                        @else
                                        <span style="color:red">(Trial)</span>
                                        @endif
                                    </h4>
                                    <div class="panel-body">
                                        <p class="text-muted font-13">
                                         {!! $user->description !!}
                                        </p>
                                        <hr/>
                                        <div class="text-left">
                                            <!-- <p class="text-muted font-13"><strong>Full Name :</strong> <span class="m-l-15">{{$user->name}}</span></p> -->
                                            <p class="text-muted font-13"><strong>Mobile :</strong><span class="m-l-15">{{$user->phone}}</span></p>
                                            <!-- <p class="text-muted font-13"><strong>Email :</strong> <span class="m-l-15">{{$user->email}}</span></p> -->
                                            <!-- <p class="text-muted font-13"><strong>Location :</strong> <span class="m-l-15">USA</span></p> -->
											<p class="text-muted font-13"><strong>Account Type :</strong> <span class="m-l-15">Basic</span></p>
											<p class="text-muted font-13"><strong>Created Date :</strong> <span class="m-l-15">{{$user->created_at->format("D, j M Y g:i a")}}</span></p>
											<p class="text-muted font-13"><strong>Expiry Date :</strong> <span class="m-l-15">{{$user->expire_date}}</span></p>
                                            <p class="text-muted font-13"><strong>ID:</strong> <span class="m-l-15">{{$user->user_id}}</span></p>
                                    @if (count($payment_history) > 0 )       
                        <table  class="table"> 
                            <thead class="black white-text">
                             <!--  <th>userid</th> -->       
                                <tr>
                                <th>Amount</th>
                                <th>Transactionid</th>
                                <th>Package_Name</th>
                                <th>Expire_date</th>
                                </tr>
                            </thead>
                            <tr>
                            @foreach($payment_history as $data)
                                <td>{{$data->amount}}</td>
                                <td> {{$data->transaction_id}}</td>
                                <td>{{$data->packages->name}}</td>
                                <td> {{$data->expire_date}}</td>
                            </tr>
                            @endforeach
                            @else
                                  <p style="color: red">Payment history is empty</p>
                            @endif
                        </table>
      </div>
{{--  <!--  <td> {{$data->user_id}}</td> -->
                                        <ul class="social-links list-inline m-t-20 m-b-0">
                                            <li class="list-inline-item">
                                                <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Facebook"><i class="fa fa-facebook"></i></a>
                                            </li>
                                            <li class="list-inline-item">
                                                <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Twitter"><i class="fa fa-twitter"></i></a>
                                            </li>
                                            <li class="list-inline-item">
                                                <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Skype"><i class="fa fa-skype"></i></a>
                                            </li>
                                        </ul> --}}
										
								 <div class="row">

                                    {{-- <div class="col-sm-4">
                                        <div class="card-box tilebox-one">
                                            <i class="icon-layers float-right text-muted"></i>
                                            <h6 class="text-muted text-uppercase mt-0">Orders</h6>
                                            <h2 class="m-b-20" data-plugin="counterup">1,587</h2>
                                            <span class="badge badge-custom"> +11% </span> <span class="text-muted">From previous period</span>
                                        </div>
                                    </div> 

                                    <div class="col-sm-4">
                                        <div class="card-box tilebox-one">
                                            <i class="icon-paypal float-right text-muted"></i>
                                            <h6 class="text-muted text-uppercase mt-0">Revenue</h6>
                                            <h2 class="m-b-20">$<span data-plugin="counterup">46,782</span></h2>
                                            <span class="badge badge-danger"> -29% </span> <span class="text-muted">From previous period</span>
                                        </div>
                                    </div> 

                                    <div class="col-sm-4">
                                        <div class="card-box tilebox-one">
                                            <i class="icon-rocket float-right text-muted"></i>
                                            <h6 class="text-muted text-uppercase mt-0">Product Sold</h6>
                                            <h2 class="m-b-20" data-plugin="counterup">1,890</h2>
                                            <span class="badge badge-custom"> +89% </span> <span class="text-muted">Last year</span>
                                        </div>
                                    </div>  --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                        <!-- end row -->
                </div> <!-- container -->
            </div> <!-- content -->  
                    @include('admin.layouts.footer')
                    @endsection
                    <style>
                    .arrow-none{margin-top:20px!important;}
                 </style>