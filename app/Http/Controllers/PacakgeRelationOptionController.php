<?php

namespace App\Http\Controllers;

use App\pacakge_relation_option;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PacakgeRelationOptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\pacakge_relation_option  $pacakge_relation_option
     * @return \Illuminate\Http\Response
     */
    public function show(pacakge_relation_option $pacakge_relation_option)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\pacakge_relation_option  $pacakge_relation_option
     * @return \Illuminate\Http\Response
     */
    public function edit(pacakge_relation_option $pacakge_relation_option)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\pacakge_relation_option  $pacakge_relation_option
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, pacakge_relation_option $pacakge_relation_option)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\pacakge_relation_option  $pacakge_relation_option
     * @return \Illuminate\Http\Response
     */
    public function destroy(pacakge_relation_option $pacakge_relation_option)
    {
        //
    }
}
