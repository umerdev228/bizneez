<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ config('app.name', 'Bizneez') }}</title>
    <meta content="Coderthemes" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- App favicon -->
    <link rel="shortcut icon" href="{{asset('/theme/admin/assets/images/favicon.png')}}">
    <!-- Tooltipster css -->
    <link rel="stylesheet" href="{{ URL::asset('/theme/admin/plugins/tooltipster/tooltipster.bundle.min.css') }}">
    <link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel = "stylesheet">
    <!-- Plugins css-->
    <link href="{{asset('/theme/admin/plugins/bootstrap-tagsinput/css/bootstrap-tagsinput.css')}}" rel="stylesheet" />
    <link href="{{asset('/theme/admin/plugins/bootstrap-select/css/bootstrap-select.min.css')}}" rel="stylesheet" />
    <link href="{{asset('/theme/admin/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{asset('/theme/admin/plugins/switchery/switchery.min.css')}}" />
    <link href="{{asset('/theme/admin/plugins/footable/css/footable.core.css')}}" rel="stylesheet">
    <!-- App css -->
    <link href="{{asset('/theme/admin/assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('/theme/admin/assets/css/icons.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('/theme/admin/assets/css/metismenu.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('/theme/admin/assets/css/style.css')}}" rel="stylesheet" type="text/css" />
    <style>
        .side-menu {
            z-index: 999999;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.css">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <script>var APP_URL = '{!! url('/') !!}';</script>
    @toastr_css
    <script src="{{asset('/theme/admin/assets/js/modernizr.min.js')}}"></script>
</head>
<body style="background-color: #fff">
<div class="container">
    <div>
        <a href="{{route('admin.orders.edit', ['id' => $order->id])}}" class="btn btn-secondary hidden-print"> Back </a>
        <input type="button" id="btnSave" class="btn btn-primary hidden-print" value="Edit" />
        <button class="btn btn-primary hidden-print" onclick="myFunction()">
            <span class="glyphicon glyphicon-print" aria-hidden="true"></span>
            Print
        </button>
    </div>
    <div style="height: auto">
        <div class="row" >

            <img src="{!! asset($company->logo) !!}" class="top-logo img-responsive " />
        </div>
        <div class="row">
            <div class="col-md-6"></div>
            <div class="col-md-6">
                <h3 class="text-right" style="width:68%">PROFORMA</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <table class="table">
                    <tr>
                        <td  class="bg-gray ">Contact Info</td>
                        <td>{{$contact_person}}</td>
                    </tr>
                    <tr>
                        <td class="bg-gray" style="width:26%">Company Address</td>
                        <td>{{$company->address}}</td>
                    </tr>
                    <tr>
                        <td class="bg-gray">Invoice Address</td>
                        <td>{{$invoice_address}}</td>
                    </tr>
                </table>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-6 text-right">Quote Number:</div>
                    <div class="col-md-6 ">Quotation {{$order->serial_number}}</div>
                </div>
                <div class="row">
                    <div class="col-md-6 text-right">Quote Date:</div>
                    <div class="col-md-6"><?=date('Y-m-d')?></div>
                </div>
            </div>
        </div>
        <div class="row">
            <table class="table ml-3">
                <thead class="bg-gray">
                <tr>
                    <th scope="col"></th>
                    <th scope="col">Description</th>
                    <th scope="col">Qty</th>
                    <th scope="col">Retail Price</th>
                    <th scope="col">Discount(%)</th>
                    <th scope="col">New Price</th>
                    <th scope="col">Total</th>
                </tr>
                </thead> <?php $sub_total =0; ?>
                @foreach($products as $prod)
                    <tr>
                        <td width="150">
                            @if($prod->image !== "")
                                <img class="img-css" src="{{asset($prod->image)}}">
                            @endif
                        </td>
                        <td>{{$prod->description}}</td>
                        <td>{{$prod->qty}}</td>
                        <td> £{{$prod->price}}</td>
                        <td>{{$prod->percentage}}</td>
                        <?php
                        $new_price = $prod->price - ($prod->price * $prod->percentage)/100  ;
                        $total = $new_price * $prod->qty;
                        $sub_total = $total + $sub_total;
                        ?>
                        <td> £{{$new_price}}</td>
                        <td> £{{$total}}</td>
                    </tr>
                @endforeach
                <tbody>
                </tbody>
            </table>
        </div>
        <div class="row">
            <div class="col-md-6"></div>
            <div class="col-md-6">    <table class="table table-striped">
                    <tr>
                        <td>Sub Total</td><td id="sub_total" class="text-right" style="padding-right: 65px;"> £{{$sub_total}}</td>
                    </tr>
                    @php
                        $vat_value_hid = $sub_total / 100 * $vat
                    @endphp
                    <tr>
                        <td>VAT <small>({{$vat}}%)</small></td><td class="text-right" style="padding-right: 65px;" id="vat"> £{{$vat_value_hid}}</td>
                    </tr>
                    <tr>
                        <td><b>Gross Total</b></td><td class="text-right" id="gross_total" style="padding-right: 65px;"> £{{$sub_total + $vat_value_hid}}</td>
                    </tr>
                </table></div>
        </div>
    </div>



 




       <div class="term_condition" >
        @if($termconditions)
        <div style="margin-top:1500px">
            {!! $company->term_and_condition !!}
        </div>
        @endif

    </div>
    <br><br><br><br><br><br><br><br><br>

    <div id="footer">  <div class="row">
            <div class="col-md-4">
                <h5>{{$company->name}}</h5>
            </div>
            <div class="col-md-4">
                {{$company->address}}
            </div>
            <div class="col-md-4" align="center">
                <p class="mt-2">{{$company->url}}</p>
            </div></div>
        <!--         <footer>
                    <div class="container-fluid" style="background-color: #ccced0; padding: 10px;">
                        <div class="row justify-content-center mx-auto">
                            <div class="col-12 px-xs-0 col-md-12">


                                <div class="row">
                                    <div class="col-md-4">
                                         <h5>BIZNEEZ.NET LTD</h5>
                                    </div>
                                      <div class="col-md-4">
                                         Kemp House, 160 City Road, London, United Kingdom, EC1V 2NX, Company 13181278
                                    </div>
                                      <div class="col-md-4" align="center">
                                        <p class="mt-2">www.bizneez.net</p>
                                    </div>


                </div>

            </div>
        </div>
    </div>
</footer> -->
    </div>









</body></html>
<style>

    div#footer {
        position:fixed;
        bottom:0px;
        left:0px;
        width:100%;
        color:#CCC;
        background:#333;
        padding:8px;
    }
    .hd{    background: #fff;
        width: fit-content;
        padding: 5px; font-size: 13px;
        color: #fff;}
    .top-logo{
        width: 290px;
        height: 75px;
        padding: 10px;
        margin-left: 16px;
    }
    .bg-gray{
       /* background-color: #708398; color: #fff;*/
    }
    .editable{ background:#EAEAEA}
    .img-css{    width: 105px;
        height: 102px;}
</style>
<script type="text/javascript" src="//code.jquery.com/jquery-2.0.2.js"></script>
<script>
    function myFunction() {
        window.print();
    }
//alert($('.table').height());
    $("#btnSave").click(function(){
        var $div=$('.quatation'), isEditable=$div.is('.editable');
        $("#btnSave").attr('value', 'Update');
        $('div').prop('contenteditable',!isEditable).toggleClass('editable');
    })
</script>