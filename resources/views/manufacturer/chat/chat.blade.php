<!-- resources/views/chat.blade.php -->


@extends('includes.meeting_section.master')

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    @if(session()->has('message'))
                        <div class="alert alert-success mt-5" id="myElem" >
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    <div id="app">
                        <div class="card">
                            <div class="card-header">Chats</div>

                            <div class="card-body">
                                <chat-messages :messages="messages"></chat-messages>
                            </div>
                            <div class="card-footer">
                                <chat-form
                                        v-on:messagesent="addMessage"
                                        :user="{{ Auth::user() }}"
                                ></chat-form>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script src="{{asset('/js/app.js')}}"></script>

@endsection