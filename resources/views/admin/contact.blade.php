@extends('admin.layouts.app')
@section('content')
@include('admin.layouts.topbar')
<!-- Google Chart JS -->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<!-- Start Page content -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                
                    @if(session()->has('message'))
    <div class="alert alert-success mt-5" id="myElem" >
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
        {{ session()->get('message') }}
    </div>

@endif   


                    <div class="row">
                        <div class="col-12 mt-5">
                            <div class="card-box">
                         
    
                              <table class="table table-hover m-0 tickets-list table-actions-bar dt-responsive nowrap" cellspacing="0" width="100%" id="normal-table">
                              <thead>
                                <tr>
                                  
                                  <th>Contact Name</th>   
                                  <th>Phone no</th>
                                  <th>Email</th>
                                  <th>Description</th>   


                                </tr>
                              </thead>
                              <tbody>
                               
                                  
                                  @foreach($contact as $cont)
                                  <tr> 
                                  <td>{{$cont->name}}</td>
                                   <td>{{$cont->email}}</td>
                                  <td>{{$cont->phone}}</td>
                                  <td>{{$cont->description}}</td>
                                <td>
                                  
                                   
                                  </td>
                                </tr>
                                  @endforeach
                            </tbody>
                            </table>
                                
                                
                                    </div> <!-- end card -->
                                    </div><!-- end col -->
                                </div>
                                
                            </div>
                        </div>
                        
                                </div> <!-- container -->
                                </div> <!-- content -->
                                
                                @include('admin.layouts.footer')
                                @endsection
                               