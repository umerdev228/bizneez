@extends('admin.layouts.app')
@section('content')
@include('admin.layouts.topbar')
<!-- Google Chart JS -->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<!-- Start Page content -->
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">
        

        <div class="row">
          <div class="col-9 mt-5">
            <div class="card-box">
              
             
              <!-- <a href="{{url('admin/packages/add')}}" class="btn"><i class="fa fa-plus"></i> Add New Package</a>
              <br>
              <br> -->
@if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif   

 
 

              <form action="{{ url('admin/option/update') }}" method="post"> 
                {{ csrf_field() }}
                <input type="hidden" value="{{$option->id}}" name="id" />
                
                  <div class="form-group col-md-6">
                    <label for="FName">Package option<span class="text-danger">*</span></label>
                    <input type="text" id="package_option_name" maxlength="20" value="{{$option->option_name}}" name="package_option_name" class="form-control" parsley-trigger="change" required="required" />
                      @if ($errors->has('package_option_name'))
                              <span class="error">{{ $errors->first('package_option_name') }}</span>
                      @endif   

                  </div>
                <!-- <div class="form-group">
                  <label for="Description">Option Name<span class="text-danger"></span></label>
                  <textarea class="form-control" rows="5" spellcheck="false" id="description" name="description"></textarea>
                  @if ($errors->has('description'))
                              <span class="error">{{ $errors->first('description') }}</span>
                  @endif   
                </div> -->


                
                <button type="submit" class="btn btn-primary" style="margin-left: 15px">Save</button>
                <button onclick="history.back()" type="button" class="btn">Cancel</button>
              </form>
              </div> <!-- end card-box -->
            </div>
            
            </div> <!-- container -->
            </div> <!-- content -->
            



            @include('admin.layouts.footer')
            @endsection
            <style>
            .arrow-none{margin-top:20px!important;}
            </style>