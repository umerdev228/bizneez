<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Packageoptions;

class PackageOption extends Controller
{

	public function __construct()
    {
          $this->middleware('auth');
    }


	public function create(Request $request)
    {
         
             $data = [
            'option'        => 'Add Packages Option | unique:package_option_name',
            'breadcrumb' => 'OptionList',
             
        ];  
       return view('admin.option_add')->with($data);
    }
     public function show()
    { 

       $option = Packageoptions::all(); 
        $data = [
            'option' => 'Packages Option',
            'breadcrumb' => 'Dashboard / Packages Option List',
            'packages_option' => $option
        ];  
       return view('admin.option')->with($data);
    }

   // public function create()
   //  {
   //     $data = ['title' => 'Package Option'];
   //     return view('admin.option_add')->with($data); 


   //  }
    public function store(Request $request)
    {
     
    	request()->validate(['package_option_name' => 'required|max:20', ]);
      

    	$data_package = ['option_name' => $request->package_option_name, ];
      // dd($data_package);
    	$package = Packageoptions::create($data_package); 
      $data = ['breadcrumb' => 'Dashboard / Packages Option List' , 'message'=>'Option is added successfully.', ];
      // return view('admin.option')->with($data);
    	 return redirect('admin/option')->with('message', 'Option is added successfully .');


    }
    public function edit($id)
    {
 
		$option = Packageoptions::findOrFail($id);

 		$data = [
            'option'        => $option,
            'breadcrumb' => 'option Edit',
        ];  
    return view('admin.option_edit')->with($data);
       
    }
     public function update(Request $request)
    {
    	// dd('hsajhifadbgvdb bnb gvhsjb');
         // request()->validate([  
         //    'package_option_name' => 'required', 
         //    ]);

    		 
          $option = Packageoptions::findOrFail($request->id); 
          
          $option->option_name = $request->package_option_name;
          $option->save();
          return redirect('admin/option')->with('message', 'Option is updated successfuly.'); 



    }



     public function destroy(Request $request)
    {  
          Packageoptions::destroy($request->id);
          return redirect('admin/option')->with('message', 'Option is delete successful.'); 

    }
   

}
