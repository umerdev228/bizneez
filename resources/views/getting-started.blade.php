<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8" />
    <title> Bizneez Office Furniture Supplier | Office Furniture London </title>
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<!-- App favicon -->
<link rel="shortcut icon" href="theme/admin_ui/assets/images/favicon.png">

<!-- Tooltipster css -->
<link rel="stylesheet" href="theme/admin_ui/plugins/tooltipster/tooltipster.bundle.min.css">

<!-- Custom box css -->
<link href="theme/admin_ui/plugins/custombox/css/custombox.min.css" rel="stylesheet">

<!-- Summernote css -->
<link href="theme/admin_ui/plugins/summernote/summernote-bs4.css" rel="stylesheet" />

<!-- Select 2 -->
<link href="theme/admin_ui/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" />
<link href="theme/admin_ui/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />

<!-- Tags Input css -->
<link href="theme/admin_ui/plugins/bootstrap-tagsinput/css/bootstrap-tagsinput.css" rel="stylesheet" />
<!-- Tags Input Image  style.bundle-->
<link href="{{ asset('theme/assets/js/input-image/prismjs.bundle.css') }}" rel="stylesheet" type="text/css" />
{{-- <link href="{{ asset('theme/assets/js/input-image/style.bundle.css') }}" rel="stylesheet" type="text/css" /> --}}

<!-- App css -->
<link href="theme/admin_ui/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="theme/admin_ui/assets/css/icons.css" rel="stylesheet" type="text/css" />
<link href="theme/admin_ui/assets/css/metismenu.min.css" rel="stylesheet" type="text/css" />
<link href="theme/admin_ui/assets/css/style.css" rel="stylesheet" type="text/css" />
<script src="theme/admin_ui/assets/js/modernizr.min.js"></script>
<script>


</script>
<script>
  
  function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        var image_x = document.getElementById('old_pic');

        reader.onload = function (e) {
          $('#blah').removeClass('d-none');
          if(image_x != null)
          {
              image_x.parentNode.removeChild(image_x);
          }  
          $('#blah')
                .attr('src', e.target.result)
                .width(100)
                .height(100);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

  </script>

</head>

<body class="enlarged" data-keep-enlarged="true">

<!-- Begin page -->
<div id="wrapper"> 
  
  <!-- ========== Left Sidebar Start ========== -->
  <div class="left side-menu">
    <div class="slimscroll-menu" id="remove-scroll"> 
      
      <!-- LOGO -->
      <div class="topbar-left"> <a href="index.html" class="logo"> <span> <img src="assets/images/logo.png" alt="" height="22"> </span> <i> <img src="assets/images/logo_sm.png" alt="" height="28"> </i> </a> </div>
      
      <!-- User box -->
      <div class="user-box">
        <div class="user-img"> <img src="assets/images/users/avatar-1.jpg" alt="user-img" title="Mat Helme" class="rounded-circle img-fluid"> </div>
        <h5><a href="#">Maxine Kennedy</a> </h5>
        <p class="text-muted">Admin Head</p>
      </div>
      
      <!--- Sidemenu -->
      <div id="sidebar-menu">
        <ul class="metismenu" id="side-menu">
          
          <!--<li class="menu-title">Navigation</li>-->
          
          <li  data-toggle="tooltip" data-placement="right" title="" data-original-title="Dashboard"> <a href="#"> <i class="fa fa-dashboard"></i><span> Dashboard </span> </a> </li>
          <li  data-toggle="tooltip" data-placement="right" title="" data-original-title="Tickets"> <a href="#"> <i class="fa fa-ticket"></i> <span> Tickets </span> </a> </li>
          <li  data-toggle="tooltip" data-placement="right" title="" data-original-title="Contacts"> <a href="#"> <i class="fa fa-users"></i><span> Contacts </span> </a> </li>
          <li  data-toggle="tooltip" data-placement="right" title="" data-original-title="Solutions"> <a href="#"> <i class="fa fa-check-square-o"></i><span> Solutions </span> </a> </li>
          <li  data-toggle="tooltip" data-placement="right" title="" data-original-title="Bots"> <a href="#"> <i class="mdi mdi-robot"></i><span> Bots </span> </a> </li>
          <li  data-toggle="tooltip" data-placement="right" title="" data-original-title="Analytics" > <a href="#"> <i class="fa fa-bar-chart-o"></i> <span> Analytics </span> </a> </li>
          <li  data-toggle="tooltip" data-placement="right" title="" data-original-title="Admin"> <a href="#"> <i class="fa fa-gear"></i><span> Admin </span> </a> </li>
        </ul>
      </div>
 

      <!-- Sidebar -->
      
      <div class="clearfix"></div>
    </div>
    <!-- Sidebar -left --> 
    
  </div>
  <!-- Left Sidebar End --> 
  
  <!-- ============================================================== --> 
  <!-- Start right Content here --> 
  <!-- ============================================================== -->
  
  <div class="content-page"> 
    
    <!-- Top Bar Start -->
    <div class="topbar">
      <nav class="navbar-custom">
       
        <ul class="list-inline menu-left mb-0">
          <li class="float-left">
            <button class="button-menu-mobile open-left disable-btn"> <i class="dripicons-menu"></i> </button>
          </li>
          <li class="mt-3 mr-4">
             
            <button onclick="location.href='{{ route('logout') }}'" type="button" class="btn btn-danger pull-right">Sign Out</button>
            {{-- <input type="button"  value="Call2Functions" /> <div class="dropdown-menu" aria-labelledby="btnGroupDrop2"> <a class="dropdown-item" href="#">New ticket</a> <a class="dropdown-item" href="#">New email</a> <a class="dropdown-item" href="#">New contact</a> <a class="dropdown-item" href="#">New company</a> </div> --}}
          </li>
          <li>
            <div class="page-title-box">
              <h4 class="page-title">Get started  </h4>  
            </div>
          </li>
        </ul>
      </nav>
    </div>



    <!-- Top Bar End --> 
   
    <!-- Start Page content -->
{{--    <form id="frmCall">
     <input type="text" id="name" name="name" value="{{$merchant->name}}" > <br>
     <input type="text" id="mail" name="mail" value="{{$merchant->email}}" > <br>
     <input type="text" id="pwd" name="pwd" value="{{$merchant->user->password}}" > <br>
     <input type="text" id="ph" name="ph" value="{{$merchant->phone}}" > <br>
   </form>  {{$url}}   
{{ env('APP_ENV_LINK')}} --}}
    <div class="content">
      <div class="container-fluid">
      @if(session()->has('message') || $merchant->profile_status==5)    
         <!--  <div class="alert alert-success" role="alert" align="center">
            Thank you!! you are completed the profile setup, please <br><a class="btn btn-secondary" onclick="function( fun1() fun2());" id="url_confirm">Click Here to Start Demo</a> <br> Automatically Redirect to TMS in 3 Seconds.
          </div>-->
            <div class="alert alert-success" role="alert" align="center">
            Thank you!! you are completed the profile setup, please <br><!-- <a class="btn btn-secondary" href="{{ env('APP_URL')}}/api?name={{$merchant->name}}&mail={{$merchant->email}}&ph={{$merchant->phone}}&pwd={{$merchant->user->password}}&exp_date={{$merchant->expire_date}}" id="start_now"> --><a class="btn btn-secondary"  href="{{ env('APP_URL')}}/api?name={{$merchant->name}}&mail={{$merchant->email}}&ph={{$merchant->phone}}&pwd={{$merchant->user->password}}&exp_date={{$merchant->expire_date}}" onclick="func_logout();">Click Here to Start Demo</a> <br> <!-- Automatically Redirect to TMS in 3 Seconds. -->
          </div>
 

<!--  url_confirm<script src="./assets/js/vendor/jquery-1.12.4.min.js"> <input type="button" id="url_confirm" onclick="function1();function2();" value="Call2Functions" /></script>
 <script>
  $(document).ready(function () { 
    window.setTimeout(function () {
        location.href = "http://tms.cybexo.net/api?name={{$merchant->name}}&mail={{$merchant->email}}&ph={{$merchant->phone}}&pwd={{$merchant->user->password}}&exp_date={{$merchant->expire_date}}";
    }, 5000);
});
 </script> -->
      @endif    
        <div class="row">
		     <div class="col-sm-6 col-xl-4">
            <div class="card-box slimscroll">
			         <h3 class="m-b-20 mt-4">Get started with TMS</h3>
	             <div class="progress mb-2">
                   @if(  $merchant->profile_status==0)  
                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100">40%</div>  @endif
                    @if($merchant->profile_status!='' && $merchant->profile_status==1)  
                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" style="width: 50%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">50%</div>
                    @endif
                    @if($merchant->profile_status!='' && $merchant->profile_status==2)
                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" style="width: 60%;" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100">60%</div>
                    @endif
                    @if($merchant->profile_status!='' && $merchant->profile_status==3)
                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" style="width: 80%;" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100">80%</div>
                    @endif
                    @if($merchant->profile_status!='' && $merchant->profile_status==4)
        	         <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" style="width: 90%;" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100">90%</div>
                  @endif
                 @if($merchant->profile_status!='' && $merchant->profile_status==5)
                 <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" style="width: 100%;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">100%</div>
                 @endif  
              </div>
				<div class="accordion" id="getting-started">
					<div class="">
  					<div class="card">
    						<div class="card-header text-dark collapsed" id="headingOne" data-toggle="collapse" data-target="#gt-0" aria-expanded="false" aria-controls="collapseOne">
    							<h5 class="m-0 text-left">Sign up for TMS <i class="fa fa-check-circle pull-right text-success font-18"></i></h5>
    						</div>
  					</div>
            <div class="card">
      						<div class="card-header text-dark collapsed" id="headingTwo" data-toggle="collapse" data-target="#gt-1" aria-expanded="false"     aria-controls="collapseOne">
      						  	<h5 class="m-0 text-left">Set up your helpdesk <small class="pull-right ">5 mins</small></h5>
      						</div>
                <div id="gt-1" class="collapse show" aria-labelledby="headingTwo" data-parent="#getting-started">
      							<div class="tabs-vertical-env" >
          								<ul class="nav tabs-vertical mb-0 " >
          									<li class="nav-item"> <a href="#update-account" data-toggle="tab" aria-expanded="false" class="nav-link active show">
                             <i class="fi-cog mr-2"></i> Update your account details    @if($merchant->profile_status!='' && $merchant->profile_status>=1)  <i class="fa fa-check-circle pull-right text-success font-18"></i>  @endif </a> </li>
                              <li class="nav-item"> <a href="#active-account" data-toggle="tab" aria-expanded="true" class="nav-link"> 
                              <i class="fi-cog mr-2"></i> 
                              <i class="fa fa-check-circle pull-right text-success font-18" id="tick2"></i> 
                              Activate your account   @if($merchant->profile_status!='' && $merchant->profile_status>=2)  <i class="fa fa-check-circle pull-right text-success font-18"></i>  @endif  </a> </li>
          									   <li class="nav-item"> <a href="#personalize" data-toggle="tab" aria-expanded="false" class="nav-link">
                              <i class="fa fa-check-circle pull-right text-success font-18" id="tick3"></i> 
                              @if($merchant->profile_status!='' && $merchant->profile_status>=3) <i class="fa fa-check-circle pull-right text-success font-18"></i> @endif <i class="fi-cog mr-2"></i> Personalize your helpdesk </a> </li>
          									 <li id="list4" class="nav-item"> <a href="#support-channel" data-toggle="tab" aria-expanded="false" class="nav-link"> 
                              <i class="fa fa-check-circle pull-right text-success font-18" id="tick4"></i>  @if($merchant->profile_status!='' && $merchant->profile_status>=4) <i class="fa fa-check-circle pull-right text-success font-18"></i> @endif 
                              <i class="fi-cog mr-2"></i> Choose your support channels</a> </li>
                              <li class="nav-item"> <a href="#invite-team" data-toggle="tab" aria-expanded="false" class="nav-link"> @if($merchant->profile_status!='' && $merchant->profile_status>=6) <i class="fa fa-check-circle pull-right text-success font-18"></i> @endif  <i class="fi-cog mr-2"></i> Invite your team</a> </li>
          							  </ul>
      							</div>
  						</div>
					</div>
						
						<div class="card">
							<div class="card-header text-dark collapsed" id="headingThree" data-toggle="collapse" data-target="#gt-2" aria-expanded="false" aria-controls="collapseOne">
								<h5 class="m-0 text-left">Start managing conversations <small class="pull-right ">2 mins</small></h5>
							</div>

							<div id="gt-2" class="collapse" aria-labelledby="headingThree" data-parent="#getting-started">
								<div class="tabs-vertical-env" >
									<ul class="nav tabs-vertical mb-0 " >

										<li class="nav-item"> <a href="#quick-overview" data-toggle="tab" aria-expanded="false" class="nav-link"> <i class="fi-play mr-2"></i> Get a quick overview of tickets </a> </li>

										<li class="nav-item"> <a href="#" data-toggle="tab" aria-expanded="false" class="nav-link"> <i class="fa fa-ticket mr-2"></i> Take a tour of the ticket list page </a> </li>

										<li class="nav-item"> <a href="#explore-features" data-toggle="tab" aria-expanded="false" class="nav-link"> <i class="fi-grid mr-2"></i> Take a tour of the ticket list page </a> </li>

								  </ul>
								</div>
							</div>
						</div>
						
						<div class="card">
							<div class="card-header text-dark collapsed" id="headingFour" data-toggle="collapse" data-target="#gt-3" aria-expanded="false" aria-controls="collapseOne">
								<h5 class="m-0 text-left">Improve your agents' productivity <small class="pull-right ">3 mins</small></h5>
							</div>

							<div id="gt-3" class="collapse" aria-labelledby="headingFour" data-parent="#getting-started">
								<div class="tabs-vertical-env" >
									<ul class="nav tabs-vertical mb-0 " >

										<li class="nav-item"> <a href="#" data-toggle="tab" aria-expanded="false" class="nav-link"> <i class="fi-play mr-2"></i> Introduction to productivity features </a> </li>

										<li class="nav-item"> <a href="#" data-toggle="tab" aria-expanded="false" class="nav-link"> <i class="fa fa-ticket mr-2"></i> Try using a canned response in a ticket </a> </li>

										<li class="nav-item"> <a href="#" data-toggle="tab" aria-expanded="false" class="nav-link"> <i class="fi-grid mr-2"></i> Explore more productivity features </a> </li>

								  </ul>
								</div>
							</div>
						</div>
						 </div>
				</div>
			 </div>
			   </div>
          <!-- end col -->
          <div class="col-sm-6 col-xl-8 " >
			         <div class="card-box task-detail slimscroll">
                <div class="tab-content p-3">
                <div class="tab-pane active show " id="update-account">
					       <div class="inbox-widget " >
						    <h3 class="m-b-20 ml-4">Confirm your details   </h3>
						    
						 <form class="col-12" action="{{ route('confirm_detail') }}" method="POST" enctype="multipart/form-data">  
               
                @csrf
                <div class="p-3" >
  						      {{-- @if($merchant->profile_pic == 0)                         
                    <img src="avatar-1.jpg" id="blah" alt="avatar-1.jpg" width="82">
                    @else
                        <img  src="{!! url('/') !!}/assets/images/users/{{$merchant->profile_pic}}" id="blah" alt="{{$merchant->profile_pic}}" width="82"  class=" rounded-circle"  />  
                       
                    @endif
                      <span class="text-muted ml-2">Upload profile pic</span>
                       

                      <input type="file" name="profile_pic" id="myImage1"  >   --}}
                      <div class="image-input image-input-outline" id="kt_image_1">
                        <?php 
                         // dd($merchant->profile_pic);
                          ?>
                        @if($merchant->profile_pic == 0)
                          <div class="image-input-wrapper" style="background-image: url({{ url('/') }}/avatar-1.jpg)"></div>
                          @else
                          <div class="image-input-wrapper" style="background-image: url({{ url('/') }}/assets/images/users/{{$merchant->profile_pic}})"></div>
                          @endif
                          <label class=""  data-action="change" data-toggle="tooltip" title="" data-original-title="Change Pic">
                           <i class="fa fa-edit fa-lg text-muted"></i>
                           <input type="file" name="profile_pic" accept=".png, .jpg, .jpeg"/>
                           <input type="hidden" name="profile_pic"/>
                          </label>
                         
                          <span class="" data-action="cancel" data-toggle="tooltip" title="Cancel">
                           <i class="fa fa-window-close fa-lg text-muted"></i>
                          </span>
                         </div>  

                </div>
                

              @php $name = explode(' ', $merchant->name); @endphp 
							<div class="form-row">
								<div class="form-group col-md-5">
									<label for="fname4" class="col-form-label">First Name</label>
									<input type="text" name="fname" value="{{$name[0]}}" pattern="[A-Za-z\s]+" autocomplete="off" maxlength="20" class="form-control" id="fname" placeholder="">
								</div>
								<div class="form-group col-md-5">
									<label for="lname" class="col-form-label">Last Name</label>
									<input type="text" class="form-control" value="{{$name[1]}}" pattern="[A-Za-z\s]+" autocomplete="off" maxlength="20" name="lname" id="lname" placeholder="">
								</div>
							</div>
              <div class="form-row">
							<div class="form-group col-md-5">
								<label for="email" class="col-form-label">Email</label>
								<input type="email" readonly="readonly" class="form-control" value="{{$merchant->email}}" id="email" name="email" placeholder="">
							</div>
							<div class="form-group col-md-5">
								<label for="phone" class="col-form-label">Phone</label>
								<input pattern="^[0-9-+\s()]*$" minlength="4"  maxlength="15" type="tel" class="form-control" value="{{$merchant->phone}}" id="phone" name="phone" placeholder="">
							</div>
              
              
            </div>
							<button type="submit" @if($merchant->profile_status!='') @endif class="btn btn-custom">Update</button>
						</form>
            <!-- disabled="disabled" ya cheez added kr ne hai profile_status ka bad  -->


					</div>
                </div>
                <div class="tab-pane" id="active-account">
					       <h3 class=" m-b-20">Activate your TMS account</h3>
			  		     <p class="font-16 text-muted">Please check your inbox for a verification email from us and click on the activation link to unlock access to all of the features in TMS.</p>
					<div class="row">
						<div class="col-xl-8">
							<p class="">Verifying your email helps us preserve your support URL and unlock additional capabilities in Bizneez TMS. Look out for an activation email we sent to the address below.</p>
					<form action="#" method="post">
          @csrf		

          <div class="alert alert-danger alert-email" role="alert" >
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                  </button>
            This email is not exist
          </div>

           <div class="alert alert-success alert-email-success" id="alert"  role="alert" >
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                  </button>
            Email is sent.
          </div>


							<div class="form-row">
								<div class="form-group col-md-7">
                  <input type="text" class="form-control"  value="{{$merchant->email}}" placeholder="{{$merchant->email}}">
									  <input type="hidden" class="form-control" id="active_email" name="active_email" value="{{$merchant->email}}" >  
								</div>
								<div class="form-group col-md-5">
									<!-- <button id="reset_activation" @if($merchant->profile_status >= 2 ) disabled="disabled" @endif type="button" class="btn btn-custom">Reset Activation</button> -->

                  <button   type="button" class="btn btn-primary" id="reset_activation" @if($merchant->profile_status != 1 ) disabled="disabled" @endif  class="btn btn-custom">Reset Activation</button>

                  <button class="buttonload">
                   <i class="fa fa-spinner fa-spin"></i>Loading
                    </button>
								</div>
							</div>
					</form>		
						
						</div>
						<div class="col-md-4">
							<img src="assets/images/activate-account.png" alt="" height="210">
						</div>
					</div>
					
                </div>
                <div class="tab-pane" id="personalize">
					<h3 class=" m-b-20">Personalize your helpdesk</h3>
			  		<p class="font-16">Confirm your Cybexo TMS URL</p>
					
					<div class="card m-b-30 text-white bg-custom">
						<div class="card-body">
							<div class="alert alert-warning p-2" role="alert"><i class="mdi mdi-alert-circle font-18"></i> Please note that you can change your domain only once</div>



          <div class="alert alert-danger alert-email1" id="myElem"   >
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
            Please Enter Domain Name.
          </div>


           <div class="alert alert-success alert-email-success1" id="myElem" role="alert" >
           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                  </button>
            Updated sucessfully.
          </div>



							<div class="row">
								<div class="col-lg-3">
								<img src="{{url('/')}}/assets/img/personalise-your-helpdesk.png" alt="" height="100">
							</div>
							
							<div class="col-lg-9">
							{{-- 	<div class="text-muted"> --}}
                <div>
									Make it easy for your customers to get in touch with you by associating your brand name with your Bizneez TMS account. Later, when you’re ready, you can choose to use your own custom domain.
								</div>
								
								<div class="form-row mt-2">
									<div class="form-group col-md-8">
										
											<input type="text" class="form-control"  id="subdomain" var="empty" placeholder="Cybexo" required="">
											<span class="text-muted pull-right domain-txt">.tms.com</span>
									
									</div>
									<div class="form-group col-md-4">  
									  <button  @if($merchant->profile_status !=2 ) disabled="disabled" @endif id="url_confirm" type="button"   class="btn btn-custom">Confirm</button>
                  <!-- disabled="disabled"  ya mani remove keya hai >= 3 ) ka bad add kr na hai -->
									</div>
								</div>
								
								
							</div>
							</div>
									
							
						</div>
					</div>
					<p class="font-16">Change Cybexo TMS to suit your language and brand</p> 
									<div class="form-group col-md-5">
										<label for="Type">Language</label>
											<select class="form-control select2" id="language" name="language">
												<option value="English">English</option>
												<option value="German">German</option>
												<option value="French">French</option>
												<option value="Arabic">Arabic</option>
												<option value="Urdu">Urdu</option>
											</select>
                  </div>
					
                </div>
                <div class="tab-pane" id="support-channel">
					<h3 class=" m-b-20">Choose your support channels</h3>
			  	
 <div class="alert alert-success alert-email-success2" id="myElem" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                  </button>
            Updated Sccessfully.
          </div>

          	<p class="font-16 text-muted">TMS lets you talk to your customers via email, chat, phone, social media and more.<br> Pick the ones you want to use for your business and get started with them right away.</p>
				

					<label for="email-channel" onclick="channel_update_email()" class="btn btn-primary"><i class="mdi mdi-email mdi-16px"></i> <span class="font-16" >Email</span> <input @if($merchant->channel_email==1) checked="checked" @endif onclick="channel_update_email()" type="checkbox" id="email-channel" class="badgebox">
            <span class"badge">&check;</span></label>


           
					<label for="phone-channel" onclick="channel_update_phone()" class="btn btn-primary" ><i class="mdi mdi-phone mdi-16px"></i> <span class="font-16">Phone</span> <input onclick="channel_update_phone()" @if($merchant->channel_phone==1) checked="checked" @endif  type="checkbox" id="phone-channel" class="badgebox"><span class="badge">&check;</span></label>

					<label for="social-channel" onclick="channel_update_social()" class="btn btn-primary"><i class="mdi mdi-vector-triangle mdi-16px" ></i>  <span class="font-16">Social</span>  <input onclick="channel_update_social()"  type="checkbox" @if($merchant->channel_social==1) checked="checked" @endif  id="social-channel" class="badgebox"><span class="badge">&check;</span></label>

					<label for="chat-channel" onclick="channel_update_chatt()" class="btn btn-primary"><i class="mdi mdi-wechat mdi-16px"></i> <span class="font-16">Chat</span> <input onclick="channel_update_chatt()" type="checkbox" @if($merchant->channel_chatt==1) checked="checked" @endif id="chat-channel" class="badgebox"><span class="badge">&check;</span></label>

					<label for="forum-channel" onclick="channel_update_forums()" class="btn btn-primary"><i class="mdi mdi-forum mdi-16px"></i> <span class="font-16">Forums</span> <input onclick="channel_update_forums()" @if($merchant->channel_forums==1) checked="checked" @endif  type="checkbox" id="forum-channel" class="badgebox"><span class="badge">&check;</span></label>
		 
 
					<div>
            <input type="hidden" value="" name="channel_email" id="channel_email" /> 
            <input type="hidden" value="" name="channel_phone" id="channel_phone" />  
            <input type="hidden" value="" name="channel_social" id="channel_social" />
            <input type="hidden" value="" name="channel_chatt" id="channel_chatt" />
            <input type="hidden" value="" name="channel_forums" id="channel_forums" />

            <button type="button"  @if($merchant->profile_status !=3 ) disabled="disabled" @endif id="channels" class="btn btn-custom mt-3">Update</button>
            <!-- disabled="disabled" button me se mani ya line remove ki hai -->
          </div>
					
                </div>


          

          				  <div class="tab-pane" id="invite-team">
                            	<h3 class=" m-b-20">Bring in your support team</h3>
                            	<p class="font-16 text-muted">Add your support team to your TMS account, explore the product and set it up the way you want during your trial period.</p>
                            	<div align="center"><img src="assets/images/invite-team.png" alt="" height="300"></div>
          					  <div class="form-group">
          						<label for="">Email addresses</label>
                        <form action="{{ route('support_team_invitation') }}" method="POST">
                          @csrf
    <select multiple data-role="tagsinput" name="tagsinput[]" placeholder="name@email.com," required></select>
                						<button id="send_invites" type="submit" @if($merchant->profile_status!=5) disabled="disabled" @endif class="btn btn-custom mt-3">Send invites</button>
                        </form>
          					  </div>
          				</div>
          


				  <div class="tab-pane" id="quick-overview">
                  	<h3 class=" m-b-20">Manage all of your conversations from one place</h3>
                  	<p class="font-16 text-muted">Organize incoming messages, send out replies and manage them effortlessly from your team inbox.</p>
                  	<div align="center"><iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2FCybexo%2Fvideos%2F501993677169318%2F&show_text=0&width=560" width="560" height="315" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe></div>
					  
				</div>
				  
				  <div class="tab-pane" id="explore-features">
                  	<h3 class=" m-b-20">Looking for more ways to manage conversations?</h3>
                  	<p class="font-16 text-muted">Check out more ticketing features in TMS that can help you organize your support</p>
                  				<div class="card-box task-detail explore-features">
                                    <div class="media mt-0 m-b-5">
                                        <img class="d-flex mr-3" alt="64x64" src="theme/admin_ui/assets/images/create-groups.png" height="100">
                                        <div class="media-body">
											<p class="font-weight-bold">Create groups to distribute tickets across teams</p>
                                            <p>Make sure people work on the right issue and improve efficiency. Organize your team into groups and create specific focus areas for different agents in your team.</p>
											<div><button type="submit" class="btn btn-custom">Explore</button></div>
                                        </div>
                                    </div>
                                </div>
					  
					  			<div class="card-box task-detail explore-features">
                                    <div class="media mt-0 m-b-5">
                                        <img class="d-flex mr-3" alt="64x64" src="theme/admin_ui/assets/images/set-resolution.png" height="100">
                                        <div class="media-body">
											<p class="font-weight-bold">Set resolution targets with SLAs policies</p>
                                            <p>Define your service levels for tickets with different priorities. Set expectations with your customers on how long they’ll have to wait for you to resolve their issue.</p>
											<div><button type="submit" class="btn btn-custom">Explore</button></div>
                                        </div>
                                    </div>
                                </div>
					  
					 			 <div class="card-box task-detail explore-features">
                                    <div class="media mt-0 m-b-5">
                                        <img class="d-flex mr-3" alt="64x64" src="theme/admin_ui/assets/images/customize-ticket.png" height="100">
                                        <div class="media-body">
											<p class="font-weight-bold">SCustomize the ticket form to suit your business</p>
                                            <p>Define your service levels for tickets with different priorities. Set expectations with your customers on how long they’ll have to wait for you to resolve their issue.</p>
											<div><button type="submit" class="btn btn-custom">Explore</button></div>
                                        </div>
                                    </div>
                                </div>
					  
					  			<div class="card-box task-detail explore-features">
                                    <div class="media mt-0 m-b-5">
                                        <img class="d-flex mr-3" alt="64x64" src="theme/admin_ui/assets/images/collaborate.png" height="100">
                                        <div class="media-body">
											<p class="font-weight-bold">SCustomize the ticket form to suit your business</p>
                                            <p>Define your service levels for tickets with different priorities. Set expectations with your customers on how long they’ll have to wait for you to resolve their issue.</p>
											<div><button type="submit" class="btn btn-custom">Explore</button></div>
                                        </div>
                                    </div>
                                </div>
					  
				</div>
				  
              </div>
            </div>
          </div>
          <!-- end col --> 
        </div>
        
        <!-- end row --> 
        
      </div>
      <!-- container --> 
      
    </div>
    <!-- content -->
    
    <footer class="footer"> 2020 © Ticket Management System. </footer>
  </div>
  
  <!-- ============================================================== --> 
  <!-- End Right content here --> 
  <!-- ============================================================== --> 
  
</div>
<!-- END wrapper --> 

<!-- jQuery  --> 
         <script src="theme/admin_ui/assets/js/jquery.min.js"></script>
        <script src="theme/admin_ui/assets/js/bootstrap.bundle.min.js"></script>
        <script src="theme/admin_ui/assets/js/metisMenu.min.js"></script>
        <script src="theme/admin_ui/assets/js/waves.js"></script>
        <script src="theme/admin_ui/assets/js/jquery.slimscroll.js"></script>
	

        <script src="theme/admin_ui/plugins/switchery/switchery.min.js"></script>
        <script src="theme/admin_ui/plugins/bootstrap-tagsinput/js/bootstrap-tagsinput.min.js"></script>
        <script src="theme/admin_ui/plugins/select2/js/select2.min.js" type="text/javascript"></script>
        <script src="theme/admin_ui/plugins/bootstrap-select/js/bootstrap-select.js" type="text/javascript"></script>
        {{-- <script src="theme/admin_ui/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js" type="text/javascript"></script> --}}
        <script src="theme/admin_ui/plugins/bootstrap-maxlength/bootstrap-maxlength.js" type="text/javascript"></script>

  
        <!-- Init Js file -->
        {{-- <script type="text/javascript" src="theme/admin_ui/assets/pages/jquery.form-advanced.init.js"></script> --}}

        <!-- App js -->
        <script src="theme/admin_ui/assets/js/jquery.core.js"></script>
        <script src="theme/admin_ui/assets/js/jquery.app.js"></script>

            <!-- Image Input -->

    <script src="{{ asset('theme/assets/js/input-image/scripts.bundle.js') }}"></script>
    <script src="{{ asset('theme/assets/js/input-image/image-input.js') }}"></script>

    
    <!-- Image Input -->
             
<script>


  //    window.history.forward(); 
  //    function noBack() { 
    //        window.history.forward(); 
      //s  } 
 
 function channel_update_email(){   
              if($('#channel_email').val() == '' )
                  $('#channel_email').val(1)  ;
              else
                  $('#channel_email').val('') ;  
            }   
              function channel_update_phone(){  
              if($('#channel_phone').val() == '' )
                  $('#channel_phone').val(1)  ;
              else
                  $('#channel_phone').val('') ;  
            }    

               function channel_update_social(){  
              if($('#channel_social').val() == '' )
                  $('#channel_social').val(1);
              else
                  $('#channel_social').val('');  
            }  
               function channel_update_chatt(){  
              if($('#channel_chatt').val() == '' )
                  $('#channel_chatt').val(1);
              else
                  $('#channel_chatt').val('') ;  
            }  

              function channel_update_forums(){  
              if($('#channel_forums').val() == '' )
                  $('#channel_forums').val(1)  ;
              else
                  $('#channel_forums').val('') ;  
            }  
 

$( "#reset_activation" ).click(function() {   
   
     
    var url_call = "{!! url('/') !!}/confirm_reset_activation";   
             var email = $('#active_email').val();  
           //  alert(email);
            

$(".buttonload").show();
$("#reset_activation").hide();

            $.ajax({ 
                type : 'get',
                url  : url_call,  
                data: { id:email  },
                contentType: 'application/json; charset=utf-8',  
                success :  function(data){ 
                  if(data==2){   
                    $(".alert-email").show();  
                     $(".alert-email-success").hide(); 
                   }
                   else{
                    $(".alert-email-success").show();
                    setTimeout(function() 
                    { $(".alert-email-success").hide(); 
                    }, 5000);
                    $("#tick2").show();  
                    $(".alert-email").hide();
                     $('.bg-success').css({
                          'width' : '60%' 
                     });
                    $( ".bg-success" ).text( "60%" );  
                    $("#reset_activation").attr("disabled", true);
                    $("#url_confirm").attr("disabled", false);


$(".buttonload").hide();
$("#reset_activation").show();


                   }  
                 } 
        });//ajax
 
});



$( "#url_confirm" ).click(function() {   
   var url_call = "{!! url('/') !!}/confirm_url";   
             var domain = $('#subdomain').val();  
             var lng = $('#language').val();
             var empty = true;
                $.ajax({ 
                type : 'get',
                url  : url_call,  
                data: { id:domain, lng:lng  },
                contentType: 'application/json; charset=utf-8',  
                success :  function(data){   
                  if(data==1){   
                    $(".alert-email1").remove();
                    $(".alert-email-success1").show();
                    $("#tick3").show();

                   
                    $('.bg-success').css({
                          'width' : '80%' 
                     });
                    $( ".bg-success" ).text( "80%" );  
                    $("#url_confirm").attr("disabled", true); 
                    $("#channels").attr("disabled", false); 


                   }
                   else{
                    $(".alert-email1").show();
                    $(".alert-email-success1").hide(); 
                     setTimeout(function() 
                    { $(".alert-email-success1").hide(); 
                    }, 5000); 
                    $(".alert-email1").show();
                   }  
                 } 
        });//ajax

});

$( "#channels" ).click(function() {   
   var url_call = "{!! url('/') !!}/channels";     
             var channel_email = $('#channel_email').val();
             var channel_phone = $('#channel_phone').val();
             var channel_social = $('#channel_social').val();
             var channel_chatt = $('#channel_chatt').val();
             var channel_forums = $('#channel_forums').val();  
             
            $.ajax({ 
                type : 'get',
                url  : url_call,  
                data: { channel_email:channel_email,channel_phone:channel_phone,channel_social:channel_social,channel_chatt:channel_chatt, channel_forums:channel_forums },
                contentType: 'application/json; charset=utf-8',  
                success :  function(data){  
                  if(data){   
                    //$(".alert-email2").show();
                    $(".alert-email-success2").show();  $("#tick4").show();

                    $('.bg-success').css({
                          'width' : '90%' 
                     });
                    $( ".bg-success" ).text( "90%" );
                    $("#channels").attr("disabled", true);
                    $("#send_invites").attr("disabled", false);




setTimeout(function(){
   window.location.reload(1);
}, 2000);



                     //  $( "#list4" ).text(  '<i class="fa fa-check-circle pull-right text-success font-18">' );
                   }
                   /*else{
                    $(".alert-email-success2").show();
                    $(".alert-email2").hide();
                   }  */
                 } 
        });//ajax

});

 
 
/* $( "#start_now" ).click(function() {   
    //var url_call = "{!! env('APP_ENV_LINK') !!}/api";     
      var url_call ="http://local-tms.com/api";       
            $.ajax({ 
                type : 'get',
                url  : url_call,  
                data : {email=aaa,pw=fefef,ph=333333,name=feefa},
                contentType: 'application/json; charset=utf-8',  
                success :  function(data){  //alert(data);
                  if(data){  
                    //$(".alert-email2").show();
                  //  $(".alert-email-success2").show();  $("#tick4").show(); 
                     //  $( "#list4" ).text(  '<i class="fa fa-check-circle pull-right text-success font-18">' );
                   }
                     
        });//ajax

});
 */
  $(function () {
    $('.button-checkbox').each(function () {

        // Settings
        var $widget = $(this),
            $button = $widget.find('button'),
            $checkbox = $widget.find('input:checkbox'),
            color = $button.data('color'),
            settings = {
                on: {
                    icon: 'glyphicon glyphicon-check'
                },
                off: {
                    icon: 'glyphicon glyphicon-unchecked'
                }
            };

        // Event Handlers
        $button.on('click', function () {
            $checkbox.prop('checked', !$checkbox.is(':checked'));
            $checkbox.triggerHandler('change');
            updateDisplay();
        });
        $checkbox.on('change', function () {
            updateDisplay();
        });

        // Actions
        function updateDisplay() {
            var isChecked = $checkbox.is(':checked');

            // Set the button's state
            $button.data('state', (isChecked) ? "on" : "off");

            // Set the button's icon
            $button.find('.state-icon')
                .removeClass()
                .addClass('state-icon ' + settings[$button.data('state')].icon);

            // Update the button's color
            if (isChecked) {
                $button
                    .removeClass('btn-default')
                    .addClass('btn-' + color + ' active');
            }
            else {
                $button
                    .removeClass('btn-' + color + ' active')
                    .addClass('btn-default');
            }
        }

        // Initialization
        function init() {

            updateDisplay();

            // Inject the icon if applicable
            if ($button.find('.state-icon').length == 0) {
                $button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i> ');
            }
        }
        init();
    });
});

    
    

$("button").click(function() {
  var $btn = $(this);
  $btn.button('loading');
  // Then whatever you actually want to do i.e. submit form
  // After that has finished, reset the button state using
  setTimeout(function() {
    $btn.button('reset');
  }, 6000);
});


    $(function() {
        $('#fname').on('keypress', function(e) {
            if (e.which == 32){
               
                return false;
            }
        });
});
        
        $(function() {
        $('#lname').on('keypress', function(e) {
            if (e.which == 32){
               
                return false;
            }
        });
});



 


function func_logout(){
     
    var url_call = "{!! url('/') !!}/logout";   

    $.ajax({ 
                type : 'get',
                url  : url_call,  
                 
                contentType: 'application/json; charset=utf-8',  
                success :  function(data){  
                 } 
    });//ajax
 


}
//for the image function in the  


//   function readURL(input) {

//     if (input.files && input.files[0]) {
//        var reader = new FileReader();
//         reader.onload = function (e) {
//            // alert('asdsadasdasdsadasd');
//              $('#blah').removeClass('d-none');
//             // alert('asdsadasdasdsadasd');
//           $('#blah')
       

//                .attr('src', e.target.result)
//                .width(108)
//                .height(100);
//        };

//        reader.readAsDataURL(input.files[0]);
//    }
// }


// $("#myImage1").change(function(){
//   // alert('asdsadasdasdsadasd');
//    readURL(this);
// });

 </script>

<style>
  #tick2,#tick3,#tick4,.alert-email{display: none;}
   .alert-email-success{display: none;}

    .alert-email1{display: none;}
   .alert-email-success1{display: none;}

     .alert-email2{display: none;}
   .alert-email-success2{display: none;}
 .alert-danger{
 color: #000000;
 }
 .alert-success{
   color: #000000; 
 }
.buttonload {
  background-color: #2d7bf4; /* Green background */
  border: none; /* Remove borders */
  color: white; /* White text */
      padding: 4px 23px;
 /* Some padding */
  font-size: 16px; /* Set a font-size */
  display: none;
}

/* Add a right margin to each icon */
.fa {
  margin-left: -12px;
  margin-right: 8px;
}
</style>

 </body>
</html>