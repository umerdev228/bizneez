<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
use App\ManufacturerOrder;
use App\Order;
use App\OrderProduct;
use App\Product;
use Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $orders = Order::where('user_id', Auth::id())->with('user')->with('products')->orderBy('id', 'desc')->get();
        $breadcrumb = ucfirst(Auth::user()->roles[0]->name)." / Orders / Quotation";
        return view('employee.order.index', compact('orders', 'breadcrumb'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $products = Cart::getContent();
        $order = Order::create([
            'user_id' => Auth::id(),
            'status' => 'pending',
        ]);
        foreach ($products as $index => $product) {
            $product_ = Product::with('user')->with('media')->where('id', $product->id)->first();
            OrderProduct::create([
                'product_id' => $product->id,
                'order_id' => $order->id,
                'quantity' => $product->quantity,
                'price' => $product_->price,
            ]);
        }
        Cart::clear();
        toastr()->success('Order Created Successfully');
        return redirect()->route('employee.orders.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $breadcrumb = ucfirst(Auth::user()->roles[0]->name)." / Order / Product / List";
        $order = Order::where('id', $id)->with('products')->first();
        $total = 0;
        $products = $order->products;
        $order_id = $order->id;
        foreach ($products as $product) {
            $total += (integer)$product->price * $product->pivot->quantity;
        }

        return view('admin.order.show', compact('order', 'products', 'order_id', 'breadcrumb', 'total'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
    }

    
    public function orderInvoiceShow($id) {
        $order = Order::where('id', $id)->with('products')->first();
        $total = 0;
        foreach ($order->products as $product) {
            $total += $product->price * $product->quantity;
        }
        return view('employee.invoice.show', compact('order', 'total'));
    }



}
