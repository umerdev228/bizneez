@extends('layout.app')


@section('content')
 <main>

       <div class="testimonial-area testimonial-padding pb-0 gray-bg">
            <div class="container-fluid">
                <!-- Testimonial contents -->
                <div class="row d-flex justify-content-center sect-bg pt-40 pb-80">
                   <div class="section-tittle text-center">
                        <h2 class="mb-1">Get the Full Picture in One View</h2>
                        <h5>See the customer’s communication history in a single timeline for all channels.</h5>
                            <div class="mt-50"><a href="{{ url('signup')}}" class="genric-btn info">start free trial</a></div>
                        </div>
                    
                </div>
            </div>
        </div>
        
        
         
    <div class="support-company-area fix pt-50 pb-50">
            <div class="container">
                
                <div class="section-tittle text-center">
                    <h2>Use AI-Powered Reply Box. We built it to Make Agents Faster.</h2>
					<div class="support-caption">
                        <p class="font-weight-bold">AI reply-box generates auto-responses. It saves time and provides accurate answers. Now it’s easier, just stop writing for the same responses, and create canned replies for regular use like greetings or full replies. Don’t change tab for internal notes, it is now visible to your team members in the same ticket timeline.</p>
                    </div>
                </div>
                
                <div class="row align-items-center">
                    <div class="col-xl-4 col-lg-6">
                        <div class="right-caption">
                            <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
              <a class="nav-link active" id="feature-tab-1" data-toggle="pill" href="#feature-1" role="tab" aria-controls="feature-1" aria-selected="true">Let Them ask away</a>
              <a class="nav-link" id="feature-tab-2" data-toggle="pill" href="#feature-2" role="tab" aria-controls="feature-2" aria-selected="false">Deliver contextual support</a>
              <a class="nav-link" id="feature-tab-3" data-toggle="pill" href="#feature-3" role="tab" aria-controls="feature-3" aria-selected="false">Know when customers are frustrated</a>
              <a class="nav-link" id="feature-tab-4" data-toggle="pill" href="#feature-4" role="tab" aria-controls="feature-4" aria-selected="false">Make it your</a>
            
            </div>
                        </div>
                    </div>
                    <div class="col-xl-8 col-lg-6">
                        <div class="support-location-img">
                            <div class="tab-content" id="v-pills-tabContent">
              <div class="tab-pane fade show active" id="feature-1" role="tabpanel" aria-labelledby="feature-tab-1"><img src="assets/img/feat-1.jpg" alt=""></div>
              <div class="tab-pane fade" id="feature-2" role="tabpanel" aria-labelledby="feature-tab-2"><img src="assets/img/feat-1.jpg" alt=""></div>
              <div class="tab-pane fade" id="feature-3" role="tabpanel" aria-labelledby="feature-tab-3"><img src="assets/img/feat-1.jpg" alt=""></div>
              <div class="tab-pane fade" id="feature-4" role="tabpanel" aria-labelledby="feature-tab-4"><img src="assets/img/feat-1.jpg" alt=""></div>
              <div class="tab-pane fade" id="feature-5" role="tabpanel" aria-labelledby="feature-tab-5"><img src="assets/img/feat-1.jpg" alt=""></div>
            </div>
                            
                        </div>
                    </div>
					<div class="section-tittle text-center">
					<div class="support-caption">
                        <p class="font-weight-bold">Use advanced filtering techniques to search your specific task. You can set unlimited filters to search  anything. Analyze where your customers are struggling with the most based on article linking in the reply box and recurring keywords.
					</p>
                    </div>
					</div>
                </div>
            </div>
        </div>
        
        
        
        
         <div class="testimonial-area testimonial-padding pb-0 gray-bg">
            <div class="container-fluid">
               <!-- Testimonial contents --> 
                <div class="row d-flex justify-content-center sect-bg pt-40 pb-80">
                   <div class="section-tittle text-center p-4">
					   <h2>Create Your Custom Workflows,<br/>Because We Don’t Make rules. You do.</h2>
                        <h2 class="mb-1">Sign up for TMS today</h2>
                        <h5>Start your 15-days free trial. No credit card required. No strings attached.</h5>
                            <div class="mt-50"><a href="{{ url('signup')}}" class="genric-btn info">start free trial</a></div>
                        </div>
                    
                </div>
            </div>
        </div>
        
    </main>
    <footer>
        <!-- Footer Start
        <div class="footer-area footer-bg footer-padding">
            <div class="container">
                <div class="row d-flex justify-content-between">
                     <div class="col-xl-3 col-lg-3 col-md-4 col-sm-5">
                        <div class="single-footer-caption mb-50">
                            <div class="footer-tittle">
                                <h4>Important Link</h4>
                                <ul>
                                    <li><a href="#"> View Project</a></li>
                                    <li><a href="#">Contact Us</a></li>
                                    <li><a href="#">Testimonial</a></li>
                                    <li><a href="#">Proparties</a></li>
                                    <li><a href="#">Support</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-5">
                        <div class="single-footer-caption mb-50">
                            <div class="footer-tittle">
                                <h4>Important Link</h4>
                                <ul>
                                    <li><a href="#"> View Project</a></li>
                                    <li><a href="#">Contact Us</a></li>
                                    <li><a href="#">Testimonial</a></li>
                                    <li><a href="#">Proparties</a></li>
                                    <li><a href="#">Support</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-5">
                        <div class="single-footer-caption mb-50">
                            <div class="footer-tittle">
                                <h4>Contact Info</h4>
                                <ul>
                                    <li>
                                    <p>Address :175-B Sector C Commercial Bahria Town Lahore.</p>
                                    </li>
                                    <li><a href="#">Phone : +92 42 3786 2953</a></li>
                                    <li><a href="#">Email : info@tms.com</a></li>
                                </ul>
                            </div>

                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-5">
                        <div class="single-footer-caption mb-50">
                            <div class="footer-tittle">
                                <h4>EXISTING USERS</h4>
                                    <div class="mt-0">
                                        <a href="#" class="genric-btn info">LOGIN</a>
                                    </div>
                                
                                    <div class="mt-20">
                                        <a href="#"><img src="theme/assets/img/playstore.png" width="150" alt=""></a>
                                    </div>
                                
                                
                             <div class="footer-social mt-20">
                                 <a href="#"><i class="fab fa-facebook"></i></a>
                                 <a href="#"><i class="fab fa-twitter-square"></i></a>
                                 <a href="#"><i class="fab fa-linkedin"></i></a>
                             </div>
                        
                            
                            </div>
                        </div>
                    </div>
                </div>
              
 
                
            </div>
        </div>
        footer-bottom area -->
      <!--   <div class="footer-bottom-area footer-bg">
            <div class="container">
                <div class="footer-border">
                     <div class="row d-flex justify-content-between align-items-center">
                         <div class="col-xl-10 col-lg-10 ">
                             <div class="footer-copy-right">
                                 <p>Copyright &copy;<script>document.write(new Date().getFullYear());</script> Ticket Management System. All rights reserved.</p>
                             </div>
                         </div>
                     </div>
                </div>
            </div>
        </div> -->
        <!-- Footer End
 
    </main>
-->

  
@endsection