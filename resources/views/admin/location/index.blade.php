@extends('admin.layouts.app')
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    @if(session()->has('message'))
                        <div class="alert alert-success mt-5" id="myElem" >
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-12 mt-5">
                            <a href="{{ route('admin.location.create') }}" class="btn btn-success">Create Location</a>
                            <div class="card">
                                <div class="card-body">
                                    <table class="table table-hover m-0 tickets-list table-actions-bar dt-responsive nowrap" id="normal-table">
                                        <thead>
                                        <tr>
                                            <th>id</th>
                                            <th>Name</th>
                                            <th> Slug </th>
                                            <th> Parent Id </th>
                                            <th> Latitude </th>
                                            <th> Longitude </th>
                                            <th data-orderable="false">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        @foreach($locations as $location)
                                            <tr>
                                                <td>{{$location->id}}</td>
                                                <td>{{$location->name}}</td>
                                                <td>{{$location->slug}}</td>
                                                <td>{{$location->parent_id}}</td>
                                                <td>{{$location->latitude}}</td>
                                                <td>{{$location->longitude}}</td>
                                                <td>
                                                    <div class="btn-group">
                                                        <a class="btn" href="{{ route('admin.location.edit', ['slug' => $location->slug] ) }}">
                                                            <i class="mdi mdi-pencil"></i>
                                                        </a>
                                                        <a class="btn" href="{{ route('admin.location.delete', ['slug' => $location->slug] ) }}">
                                                            <i class="fa fa-close"></i>
                                                        </a>
                                                    </div>

                                                </td>
                                            </tr>
                                        @endforeach

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection