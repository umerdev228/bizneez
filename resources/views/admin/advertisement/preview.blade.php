@extends('admin.layouts.app')
@section('content')
    <!-- Google Chart JS -->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <!-- Start Page content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <br><br><br><br>
                    <div class="tab-pane" id="messages">
                        {{--                        <form action="{{route('AdminManufactureCategoryAdd')}}" method="post">--}}
                        {{--                            @csrf--}}
                        <input type="text" name="rows" id="rows" style="display:none;">
                        <div class="col-md-4 form-group ml-0">

                            @if(isset($company))
                                <h5>{{$company->name}}</h5>
                            @endif                            {{--                            <select class="form-control" name="company_id" id="company_id" required>--}}
                            {{--                                <option value="0">--Select Company--</option>--}}
                            {{--                                @foreach($companies as $company)--}}
                            {{--                                    <option @if((integer)$data['company_id'] === $company->id) selected @endif value="{{$company->id}}">{{$company->name}}</option>--}}
                            {{--                                @endforeach--}}
                            {{--                            </select>--}}
                        </div>
                        <div id="individual_products" class="ml-4">
                            <div>
                                <div  class="row" id="product">
                                    {{--                                    <div class="col-md-4 no-padding">--}}
                                    {{--                                        <input type="text" name="category_top[]" id="cateogry_top0" placeholder="" class="form-control float-left height-50 " list="cat" />--}}
                                    {{--                                        &nbsp;<button id="top-plus" type="button" onclick="appendFields(0)" class="btn btn-primary btn-custom height-50 valid align-baseline rounded"><i class="fa fa-plus"></i> </button>--}}
                                    {{--                                        <!-- <button type="button" onclick="appendFields()" class="btn btn-secondary btn-custom red height-50 valid align-baseline rounded"><i class="fa fa-minus"></i> </button> -->--}}
                                    {{--                                        <button id="top-tick" type="button" onclick="show_category_name('category0','cateogry_top0')" class="btn btn-custom height-50 btn-success valid align-baseline rounded"><i class="fa fa-check"></i> </button>--}}

                                    {{--                                    </div>--}}
                                </div>
                            </div>

                            <hr class="style1">

                            <div class="mb-5 mt-5" id="category">
                                @for($i = 0; $i < $count; $i++)
                                    @foreach($data['p_name_'.$i.'_'] as $index => $d)

                                        <div class="" id="prd0">
                                            <div class="col-12 p-0">
                                                <div id="element0" class="row pb-3" >
                                                    @for($i = 0; $i < $count; $i++)
                                                        <div class="p-2">
                                                            <lable for="p_name{{$i}}" style="top: -22px; font-weight: 500;" class="category0 lbl"><strong>{{$data['cat_name_'.$i.'_'][$index]}}</strong></lable>
                                                            <input type="hidden" name="cat_name_{{$i}}_[]" value="{{$data['cat_name_'.$i.'_'][$index]}}">
                                                            {{--                                                            <input value="{{$data['p_name_'.$i.'_'][$index]}}" class="form-control width-200 right height-50"  name="p_name_{{$i}}_[]" id="p_name{{$i}}">--}}
                                                            <p class=""  name="p_name_{{$i}}_[]" id="p_name{{$i}}">{{$data['p_name_'.$i.'_'][$index]}}</p>

                                                        </div>
                                                    @endfor
                                                </div>
                                                @endforeach
                                                @foreach($images as $image)
                                                    <img src="{{$image}}" width="50px">
                                                    @endforeach
                                            </div>
                                        </div>
                                        @endfor
                                        <div id="sub_row0"></div>
                            </div>

                        </div>


                        {{--                        </form>--}}
                    </div>


                </div>

            </div>
        </div>
    </div>
    <style>
        .side-menu .topbar-left{top:24px;}
        .notification-list .noti-icon{margin-top:20px;}
        .btn_align_red{position: absolute;
            left: 79%;
            top: 22px;}
        .btn_align_perpal{position: absolute;
            left: 92%;
            top: 22px;}
        .object-lable{    text-align: center;
            width: 270px;}
        .custom-txt{width: 79%;}
        .lbl{
            position: inherit;
            top: -20px;
            /*left: 11px;*/
        }
        .lbl2{    top: -22px;
            position: absolute;
            /* left: 4px; */
            margin-left: -203px;
        }
        .category{font-weight: bold; font-size:20px}
        .no-padding{
            padding-left: 2px;
            padding-right: 0px;
        }

        .waves-light{width: 75%;}
        .btn-success{font-size: 16px;}
        .remove-pad{padding-right: 0px; padding-left: 0px;}
        .purpal{background-color: #a311d8}
        .red{background-color: red; color: #fff}
        .right{float: left}
        .blue{background-color: #2d7bf4; color: #fff}
        .purple{background: purple; color: #fff}
        .purp3{background: purple; color: #fff}
        .brown{background-color:#07922e; color: #fff}

        .file[type="file"] {
            display: none;
        }
        .height-50{height: 34px!important;}
        .width-200{width:200px; margin-right: 7px;}
        #cateogry_top0{  margin-left: -11px; width:200px; }
        #p_name0{  margin-left: 12px; margin-bottom: 20px; }
        .mt-c{    margin-top: 4px;}
        .btn-custom   {
            height: 34px;     margin: 0px 2px;
            padding-top: 3px;
        }
        .btn-custom-mr{margin: 0px 17px; height: 34px;}
        .btn-custom.focus, .btn-custom:focus, .btn-outline-custom.focus, .btn-outline-custom:focus, .btn-custom:not([disabled]):not(.disabled).active, .btn-custom:not([disabled]):not(.disabled):active, .show > .btn-custom.dropdown-toggle{box-shadow: none!important;}
        .btn-height-34{height: 34px;}
    </style>

    <script>
        var selectIncrement = 1;
        var i = 1;
        var j = 0;
        var c =0;
        function add_element2(element_id){
            var element = '#'+element_id;
            let ele_input = 'input_'+selectIncrement;
            console.log(ele_input)
            var element_data = `<div class="p-2" id="`+ele_input+`"><input placeholder="" type="text" value="" name="range_0" class="form-control width-200 float-left height-50"><a style='margin-right: 8px;height: 34px;' href="javascript:void(0)" class="btn btn-danger " onclick="removeElement(`+ele_input+`)"><i class="fa fa-close  mt-c"></i></a></div>`;
            $(element).append(element_data);
            selectIncrement++
        }
        function add_element(element_id){
            //console.log(element_id)
            category_name = $('#cateogry_top0').val();
            var element = '#'+element_id;
            let ele_select = 'select_'+selectIncrement;
            var element_data = `<div  class="p-2" id="`+ele_select+`">
                                    <label for="p_name`+element_id+`" class="lbl2" >`+category_name+`</label>
                                    <input type="hidden" name="cat_name_`+selectIncrement+`_[]" value="`+category_name+`">
                                    <input class="form-control height-50 width-200 right" name="p_name_`+selectIncrement+`_[]" id="p_name`+element_id+`">
                                    <a style='margin-right: 8px;height: 34px;margin-left: -40px;' href="javascript:void(0)" onclick="removeElement(`+ele_select+`)" class="btn btn-danger  ">
                                        <i class="fa fa-close  mt-c"></i>
                                    </a>
                                </div>`;
            $(element).append(element_data);
            selectIncrement++
            $('#rows').val(selectIncrement)
        }
        function remove_element(element_id){
            $('#'+element_id).remove();
        }
        function remove_element2(element_id){
            $('#'+element_id).remove();
        }
        function removeElement(element_id){
            console.log(element_id)
            $(element_id).remove();
        }

        function show_category_name(class_name,category_id){
            var  cls = '.'+class_name;
            var cat_id = '#'+category_id;
            $(cls).text( $(cat_id).val() );
            $('#p_name0').val( $(cat_id).val() );
            $("#top-tick").hide();
            // $("#top-plus").hide();
            $("#cateogry_top0").val('');
            $('#top-plus').prop('disabled', true);
        }
        function show_category_name2(class_name,category_id){
            var  cls = '.'+class_name;
            var cat_id = '#'+category_id;
            $(cls).text( $(cat_id).val() );
            $('#p_2name0').val( $(cat_id).val() );
            $("#top-tick").hide();
            // $("#top-plus").hide();
            $("#cateogry_2top0").val('');
            $('#top-plus').prop('disabled', true);
        }
        function show_sub_product(id){
            c=c+1;
            var render_div = '#prd'+id;
            var html_product_data =
                `<h5 class="category0"></h5>
                    <div class="row" id="id="prd_`+c+`">

                        <div class="col remove-pad no-padding row">
                            <div id="element0" class="row">
                                <div class="p-2" id="element_`+c+`">
                                    <input class="form-control width-200 right height-50" name="p_name_0" id="p_name0">
                                </div>
                            </div>
                            <button type="button" onclick="add_element('element_`+c+`')"  class="btn btn-custom mt-2 brown valid align-baseline rounded"> <i class="fa fa-arrow-right"></i> </button>

                            <button type="button" onclick="func_common_remove(`+'prd_'+c+`)"   class="btn btn-custom mt-2 red valid align-baseline rounded"> <i class="fa fa-minus"></i> </button>
                            <button type="button" onclick="show_sub_product(0)"  class="btn btn-primary btn-custom mt-2 valid align-baseline rounded"><i class="fa fa-plus"></i> </button>
                            <button type="button" class="btn  valid btn-success btn-custom mt-2 align-baseline rounded"><i class="fa fa-check"></i> </button>
                            <button type="button" onclick="clonetreetimes('category')" class="btn mt-2 btn-success btn-custom valid align-baseline purple rounded"><i class="fa fa-arrow-down"></i> </button>
                        </div>
                </div>`

            $('#category').append(html_product_data);
        }

        function func_add_row_range(){
            c=c+1;
            var html_input_category_ranges =
                ` `;
            $('#pro_ranges').append(html_input_category_ranges);
        }
        function func_add_range(){
            c=c+1;
            var html_input_category_ranges =
                `<div class="col-md-12" id="row_ranges_`+c+`" >
                    <div class="row">
                        <div id="ranges`+c+`" class="row">
                            <div class="p-2">
                                <input placeholder="" type="text" value="" id="range_0" name="range_0_`+c+`" class="form-control width-200 height-50 float-left">
                            </div>
                        </div>
                        <button type="button" onclick="add_element2('ranges`+c+`')"  class="btn mt-2 btn-custom btn-success brown valid align-baseline rounded"><i class="fa fa-arrow-right"></i></button>
                        <button type="button" onclick="func_common_remove('#row_ranges_`+c+`')"  class="btn btn-custom btn-danger red mt-2 btn-secondary valid align-baseline rounded"><i class="fa fa-minus"></i> </button>
                        <button type="button" onclick="func_add_range()" class="btn btn-primary valid btn-custom mt-2 align-baseline rounded"><i class="fa fa-plus"></i> </button>
                        <button type="button" class="btn btn-success btn-custom valid align-baseline mt-2 rounded"><i class="fa fa-check"></i> </button>
                        <button type="button" onclick="func_add_row_range()" class="btn btn-custom purple mt-2  valid align-baseline rounded"><i class="fa fa-arrow-down"></i> </button>
                    </div>
                </div>`;
            $('#pro_ranges').append(html_input_category_ranges);
        }
        function func_common_remove(id){
            $(id).remove();
        }
        function show_subrow() {
            j = j+1;
            $('#product_sub_counter_0').val(j);
            //console.log('clicked')
            var html_input =
                `<div class="row" id="sub_row`+j+`">
                    <div class="col-md-1"></div>
                        <div class="col-md-2 remove-pad">
                            <input type="text" value="`+$('#p_name').val()+`" name="p_name_0_`+j+`" placeholder="exp Product Name" class="form-control w-75" />
                        </div>
                        <div class="col-md-2 remove-pad">
                            <input type="text" value="`+$('#p_size').val()+`" name="p_size_0_`+j+`"  placeholder="exp Size Options" class="form-control w-75" />
                        </div>
                        <div class="col-md-1 remove-pad">
                            <input type="text" value="`+$('#p_code').val()+`" name="p_code_0_`+j+`" placeholder="exp Code" class="form-control w-75" />
                        </div>
                        <div class="col-md-1 remove-pad">
                            <input type="text" value="`+$('#p_retial_price').val()+`" name="p_retail_price1_0_`+j+`"  placeholder="exp Retail Price" class="form-control">
                        </div>
                        <div class="col-md-1 remove-pad">
                            <input type="text" value="`+$('#p_retial_price2').val()+`" name="p_retial_price2_0_`+j+`" placeholder="exp Retail Price" class="form-control">
                        </div>
                        <div class="col-md-3 remove-pad">
                            <button type="button" onclick="show_subrow_remove1(`+j+`)"  class="btn btn-secondary btn-danger valid align-baseline rounded "><i class="fa fa-minus"></i> </button>
                        </div>

                    </div>
                    <div id="sub_row`+j+`"></div>`

            $('#sub_row0').append(html_input);
        }
        function show_subrow_remove1(id) {
            $('#sub_row'+id).remove();
        }
        function show_subrow_inner(e,f=0) { //alert(e);
            var f=f+1;
            var html_input =
                `<div class="row" id="sub2_row`+e+`">
                            <div class="col-md-1">

                            </div>
                            <div class="col-md-2 remove-pad">
                                <input type="text" value="`+$('#p_name').val()+`"  name="p_name_`+e+`_`+f+`" placeholder="" class="form-control w-75" />
                            </div>

                            <div class="col-md-3 remove-pad">
                                <button type="button"   class="btn btn-custom btn-primary valid align-baseline rounded"><i class="fa fa-plus"></i> </button>
                                <button type="button" onclick="show_subrow_remove(`+e+`)"  class="btn btn-custom btn-danger btn-secondary valid align-baseline rounded "><i class="fa fa-minus"></i> </button>
                                <button type="button" class="btn btn-success btn-custom valid align-baseline rounded"><i class="fa fa-check"></i> </button>
                                <button type="button" class="btn  purple valid btn-custom align-baseline rounded"><i class="fa fa-arrow-down"></i> </button>

                            </div>

                        </div>`
            $("#sub1_row"+e).append(html_input);
        }
        function show_subrow_remove(id) {
            $('#sub2_row'+id).remove();
        }
        var c = 0;
        function appendFields() {
            add_element('element0');
            $("#cateogry_top0").val('');
            $('#top-plus').prop('disabled', true);
        }

        function removeField(id) {
            console.log('removeField', id)
            $('#'+id).remove();
            $('#top_row_c'+id).remove();
            $('#c'+id).remove();
            $('#ranges'+id).remove();

        }

        function removeField2(id) {
            console.log('removeField', id)
            $('#'+id).remove();
        }
        function appendFieldsR() {
            i = i+1
            console.log('clicked')
            //$('#company_keywords').val()='fefe';
            var html_input =
                `
                                                    <div id="r`+i+`" class="row">
                                                        <div class="col-sm-3">
                                                            <div class="form-group">
                                                                <label for="location_id">Representative Name </label>
                                                                <input placeholder="Please Enter Representative Name" type="text" class="form-control valid" name="represent_name[]" value="" id="represent_name"  />
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <div class="form-group">
                                                                <label for="area_id">Representative Email </label>
                                                                <input placeholder="Please Enter Representative email" type="text" class="form-control valid" name="represent_email[]" value="" id="represent_email"  />
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <div class="form-group">
                                                                <label for="area_id">Representative Phone </label>
                                                                <input placeholder="Please Enter Representative Phone" type="text" class="form-control valid" name="represent_mobile[]" value="" id="represent_mobile"  />
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <div class="form-group">
                                                                <button onclick="removeFieldR(`+i+`)" style="border-radius: 10px; margin-top: 29px;" class="btn btn-danger valid align-baseline"><i class="fa fa-minus"></i> </button>
                                                            </div>
                                                        </div>
                                                    </div>`
            $('#representative').append(html_input)
        }
        function removeFieldR(id) {

            $('#r'+id).remove();
        }

    </script>
@endsection

<script type="text/javascript"  src="//code.jquery.com/jquery-1.6.4.js"></script>
<script>
    $(".purple").live('click', function() {
        var $row    = $(this).closest('.row');
        var $clone = $row.clone();
        // $clone.find(':text').val('');
        $row.after($clone);
    });
    $(".purp3").live('click', function() {
        var $row    = $(this).closest('.cat_product');
        var $clone = $row.clone();
        // $clone.find(':text').val('');
        $row.after($clone);
    });
    $(document).ready(function() {
        $('#top-plus').prop('disabled', true);
        $('#cateogry_top0').keyup(function() {
            if($(this).val() != '') {
                $('#top-plus').prop('disabled', false);
            }
        });
    });
</script>
