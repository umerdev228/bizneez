@extends('admin.layouts.app')
@section('content')
    @include('admin.layouts.topbar')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    @if(session()->has('message'))
                        <div class="alert alert-success mt-5" id="myElem" >
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-12 mt-5">
                            <div class="card">
                            <div class="card-body">
                                <a href="{{route('manufacturer.products.create')}}"  class="btn btn-primary" ><i class="fa fa-plus"></i>
                                    Add New Product
                                </a>
                            </div>
                                <div class="card-body">
                                    <table class="table table-hover m-0 tickets-list table-actions-bar dt-responsive nowrap" id="example">
                                        <thead>
                                        <tr>
                                            <th>Image</th>
                                            <th>Id</th>
                                            <th>Name</th>
                                            <th>Price</th>
                                            <th>Discount</th>
                                            <th>Quantity</th>
                                            <th>Colors</th>
                                            <th>Manufacturer </th>
                                            <th>Created </th>
                                            @if(Auth::user()->hasRole(['admin', 'manufacturer']))
                                                <th data-orderable="false">Action</th>
                                            @endif

                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($products as $product)
                                            <tr>
                                                <td>
                                                    <img src="{{count($product->media) > 0 ? asset($product->media[0]->path) : ''}}" alt="">
                                                </td>
                                                <td>{{$product->id}}</td>
                                                <td>{{$product->name}}</td>
                                                <td><span>&#163;</span>{{$product->price}}</td>
                                                <td><span>&#163;</span>{{$product->discount}}</td>
                                                <td>{{$product->quantity}}</td>
                                                <td>
                                                    @foreach($product->colors as $index => $color)
                                                    @if($index < 5)
                                                        <img class="img-thumbnail img-fluid" src="{{asset($color->path)}}" alt="" width="10" srcset="">
                                                    @endif
                                                    @endforeach
                                                </td>
                                                <td>{{$product->manufacturer->name}}</td>
                                                <td>{{$product->created_at}}</td>
                                                @if(Auth::user()->hasRole(['manufacturer']))
                                                    <td>
                                                        <div class="btn-group">
                                                            <a class="btn" href="{{ route('manufacturer.products.edit', ['id' => $product->id] ) }}">
                                                                <i class="mdi mdi-pencil"></i>
                                                            </a>
                                                            <a class="btn" href="{{ route('manufacturer.products.delete', ['id' => $product->id] ) }}">
                                                                <i class="mdi mdi-close"></i>
                                                            </a>
                                                            <a class="btn" href="{{ route('manufacturer.products.show', ['id' => $product->id] ) }}">
                                                                <i class="mdi mdi-eye"></i>
                                                            </a>
                                                        </div>
                                                    </td>
                                                @endif
                                            </tr>
                                        @endforeach
                                    </table>
                                    @if(Auth::user()->hasRole(['architect']))
                                        <div class="float-right">
                                            <form action="{{ route('architect.products.order.store') }}" method="POST">
                                                @csrf
                                                <input type="hidden" name="products[]" id="selected_products">
                                                <input type="hidden" name="quantity[]" id="selected_products_quantity">
                                                <input class="btn btn-success" type="submit" value="Order Selected Item" id="submit_button">
                                            </form>
                                        </div>
                                    @endif
                                </div> <!-- end card -->
                            </div><!-- end col -->
                        </div><!-- end col -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('admin.layouts.footer')
@endsection
