@extends('admin.layouts.app')
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    @if(session()->has('message'))
                        <div class="alert alert-success mt-5" id="myElem" >
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-12 mt-5">
                            <div class="card">
                                <div class="card-body">
                                    <a href="{{route('admin.employees.create')}}"  class="btn btn-primary" ><i class="fa fa-plus"></i>Add New Employee </a>
                                </div>
                                <div class="card-body">
                                    <table class="table table-hover m-0 tickets-list table-actions-bar dt-responsive nowrap" width="100%" id="normal-table">
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Link</th>
                                            <th>Code</th>
                                            <th>Profile</th>
                                            <th>Email</th>
                                            <th>Phone</th>
                                            <th>Detail</th>
                                            <th>Created </th>
                                            <th data-orderable="false">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($employees as $employee)
                                            <tr>
                                                <td>{{$employee->name}}</td>
                                                <td>{{ $employee->profile->video_call_demo }} </td>
                                                <td>{{ $employee->profile->demo_code }}</td>
                                                <td>
                                                    @if($employee->profile->image)
                                                        <img src="{{asset($employee->profile->image)}}" width="100">
                                                    @endif
                                                </td>
                                                <td>{{$employee->email}}</td>
                                                <td>{{$employee->profile->phone}}</td>
                                                <td>{{$employee->profile->description}}</td>
                                                <td>{{$employee->user !== null ? $employee->user->name : null}}</td>
                                                <td>
                                                    <div class="btn-group">
                                                        <a class="btn" href="{{ route('admin.employees.edit', ['id' => $employee->id] ) }}">
                                                            <i class="mdi mdi-pencil"></i>
                                                        </a>
                                                        <a class="btn" href="{{ route('admin.employees.delete', ['id' => $employee->id] ) }}">
                                                            <i class="fa fa-close"></i>
                                                        </a>
                                                        <a class="btn" href="{{ route('admin.employees.show', ['id' => $employee->id, 'role' => Auth::user()->roles[0]->name] ) }}">
                                                            <i class="mdi mdi-eye"></i>
                                                        </a>
                                                    </div>

                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div> <!-- end card -->
                            </div> <!-- end card -->
                        </div><!-- end col -->
                    </div>

                </div>
            </div>

        </div> <!-- container -->
    </div> <!-- content -->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

    @include('admin.layouts.footer')
@endsection
