<?php

namespace App\Http\Controllers;

use App\AuthCode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class AuthCodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $codes = AuthCode::all();
        return view('admin.auth-code.index', compact('codes'));
    }


    public function get_auth_code_webrtc()
    {
        //
        $codes = AuthCode::all();
        return response()->json($codes);
       // return view('admin.auth-code.index', compact('codes'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $random = Str::random(6);
        $random = rand(100,999).$this->quickRandom(3);
        AuthCode::create([
            'code' => strtoupper($random),
            'status' => 'active'
        ]);
        return redirect()->back();
    }


       public function generateCode()
    {
        //
        $random = Str::random(6);
        $random = rand(100,999).$this->quickRandom(3);
        $codes=AuthCode::create([
            'code' => strtoupper($random),
            'status' => 'active'
        ]);
       return response()->json($codes);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AuthCode  $authCode
     * @return \Illuminate\Http\Response
     */
    public function show(AuthCode $authCode)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AuthCode  $authCode
     * @return \Illuminate\Http\Response
     */
    public function edit(AuthCode $authCode)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AuthCode  $authCode
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AuthCode $authCode)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AuthCode  $authCode
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        AuthCode::where('id', $id)->delete();
        return redirect()->back();
    }


public function removeGeneratedCode($id)
    {
        //
        AuthCode::where('id', $id)->delete();
        $codes = "";
         return response()->json($codes);
        //return redirect()->back();
    }


    public static function quickRandom($length = 16)
    {
        $pool = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

        return substr(str_shuffle(str_repeat($pool, 5)), 0, $length);
    }
}
