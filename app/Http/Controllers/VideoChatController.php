<?php

namespace App\Http\Controllers;

use App\User;
use App\VideoChat;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Pusher\Pusher;
use Pusher\PusherException;

class VideoChatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $code = $request['code'];
        $breadcrumb = 'CRM / Video Calling';
        $user = $request->user();
        $other = \App\User::where('id', '!=', $user->id)->where('name', 'demo')->pluck('name', 'id');
        $others = \App\User::where('id', '!=', $user->id)->with('roles')->with('profile')->get();
        $logged = User::where('id', Auth::id())->with('roles')->first();
        $role = auth()->user()->roles[0]->name;
//        return view('admin.video-chat.index')->with([
        return view('webrtc.index')->with([
            'user' => collect($request->user()->only(['id', 'name'])),
            'others' => $others,
            'other' => $other,
            'breadcrumb' => $breadcrumb,
            'logged' => $logged,
            'code' => $code,
            'role' => $role,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\VideoChat  $videoChat
     * @return \Illuminate\Http\Response
     */
    public function show(VideoChat $videoChat)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\VideoChat  $videoChat
     * @return \Illuminate\Http\Response
     */
    public function edit(VideoChat $videoChat)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\VideoChat  $videoChat
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, VideoChat $videoChat)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\VideoChat  $videoChat
     * @return \Illuminate\Http\Response
     */
    public function destroy(VideoChat $videoChat)
    {
        //
    }



    public function auth(Request $request) {
        $user = $request->user();
        $socket_id = $request->socket_id;
        $channel_name = $request->channel_name;
        try {
            $pusher = new Pusher(
                config('broadcasting.connections.pusher.key'),
                config('broadcasting.connections.pusher.secret'),
                config('broadcasting.connections.pusher.app_id'),
                [
                    'cluster' => config('broadcasting.connections.pusher.options.cluster'),
                    'encrypted' => true
                ]
            );
        } catch (PusherException $e) {
            dd($e);
        }
        return response(
            $pusher->presence_auth($channel_name, $socket_id, $user->id)
        );
    }
}
