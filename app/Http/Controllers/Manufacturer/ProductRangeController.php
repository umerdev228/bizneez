<?php

namespace App\Http\Controllers\Manufacturer;

use App\Media;
use App\ProductRange;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ProductRangeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $ranges = ProductRange::where('created_by', Auth::id())->with('images')->get();
        return view('manufacturer.product.range.index', compact('ranges'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $range = ProductRange::create([
            'name' => $request->name,
            'created_by' => Auth::id(),
        ]);
        $images = json_decode(\request()->session()->get('product-range-images'));
        foreach ($images as $image) {
            Media::where('id', $image->id)->update([
                'item_id' => $range->id,
            ]);
        }
        \request()->session()->forget('product-range-images');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProductRange  $productRange
     * @return \Illuminate\Http\Response
     */
    public function show(ProductRange $productRange)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProductRange  $productRange
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductRange $productRange)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProductRange  $productRange
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProductRange $productRange)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProductRange  $productRange
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductRange $productRange)
    {
        //
    }

}
