@extends('admin.layouts.app')
@section('content')
    @include('admin.layouts.topbar')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    @if(session()->has('message'))
                        <div class="alert alert-success mt-5" id="myElem" >
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    <br><br><br><br><br>
                    <form action="{{route('admin.email.template.store')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <input type="text" style="display: none;" name="images" id="images">
                        <div class="row">
                            <div class="col-md-3">
                                <div>
                                    <h3>Bulk Mail</h3>

                                    <div class="form-group">
                                        <select class="form-control" name="company" required>
                                            <option disabled selected>--Select Company--</option>
                                            @foreach($companies as $company)
                                                <option value="{{$company->id}}">{{$company->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <img src="{{asset('assets/images/icon-excel-20.jpg')}}"  style="width: 144px;"><br>
                                    <input required type="file" name="email" id="email">
                                    <a class="btn btn-primary me-2 waves-effect waves-light" target="_blank" role="button">Select Excel file<i class="fa fa-download"></i></a>
                                </div>
                                <div class="clearfix">
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                </div>


                            </div>
                            <div class="col-md-9" style="border: 2px solid black">

                                <div class="form-group">
                                    <label for="exampleFormControlInput1">Please Enter Text</label>
                                    <input required type="text" name="title" class="form-control" id="exampleFormControlInput1" placeholder="" >
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                    <div class="form-group">
{{--                                        <i class="fa fa-picture-o fa-custom" aria-hidden="true"></i>--}}
{{--                                    <button class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target="#imageGallery" type="button">--}}
{{--                                        Select Image from Library--}}
{{--                                        <i class="fa fa-download"></i>--}}
{{--                                    </button>--}}
                                    </div>
                                    </div>
                                    <div class="form-group" align="center">
                                        <img data-toggle="modal" data-target="#imageGallery" id="uploadedImage" src="{{asset('assets/images/img.png')}}" alt="">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="exampleFormControlTextarea1">Please enter Text</label>
                                    <textarea required name="body" class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                                </div>
                                <div class="row text-light" style="background-color: #1d1e1f; margin: 0px;">
                                    <div class="col-md-2">Bizneez.net</div>
                                    <div class="col-md-3"><a href="">Unsubscribe</a></div>
                                    <div class="col-md-5" align="center">Kemp House, 160 City Road, London, United Kingdom, EC1V 2NX</div>
                                    <div class="col-md-3 float-right" align="right">+44(0)20 8524 5294

                                    </div>
                                </div>
                                <br>
                                <div class="form-group">
                                    <div align="center">
                                        <button name="button" value="submit" type="submit" class="btn btn-primary mb-2">Save</button>
                                        <button name="button" value="preview" type="submit" class="btn btn-primary mb-2">Preview</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>

    <div class="modal fade" id="imageGallery" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    Image Gallery
                </div>
                <div class="modal-body">
                    <div class="col-12">
                        <div class="row">
                            @foreach($images as $image)
                                <div id="{{$image->id}}" class="col- border-success m-2 ">
                                    <img class="" onclick="selectImages({{$image->id}}, {{$image}})" width="150px" src="{{asset($image->path)}}" alt="" srcset="">
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="float-right">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">Done</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

<script !src="">
    let public_path = '{{asset('/')}}';
    let selectedImages = []
    let lastSelectedImage = ''
    function selectImages(id, image) {
        $('#uploadedImage').attr('src', public_path+image.path);
        if (lastSelectedImage !== '') {
            $('#'+lastSelectedImage).css('border', '')
        }
        lastSelectedImage = id
        $('#'+id).css('border', '2px solid green')
        $('#images').val(id)


        // if (selectedImages.includes(id)) {
        //     let index = selectedImages.indexOf(id);
        //     if (index > -1) {
        //         selectedImages.splice(index, 1);
        //     }
        //     $('#'+id).css('border', '')
        // }
        // else {
        //     selectedImages.push(id)
        //     $('#'+id).css('border', '2px solid green')
        // }
        // $('#images').val(selectedImages)
    }

</script>

<style>
    .fa-custom{  font-size: 200px!important;}
</style>