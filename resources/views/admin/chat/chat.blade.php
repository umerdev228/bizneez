<!-- resources/views/chat.blade.php -->

@extends('includes.meeting_section.master')

@section('content')
    <div  style="margin-top: 70px !important;"></div>

    <div class="col-12" id="app">
            <chat-index
                    :user="{{Auth::user()}}"
                    :users="{{$users}}"
                    :roles="{{$roles}}"
            ></chat-index>

    </div>

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script src="{{asset('js/app.js')}}"></script>

@endsection

<style>
    .inbox-widget .inbox-item .inbox-item-author{ padding-left: 45px!important; }
    .inbox-widget .inbox-item{border:none!important;}
</style>