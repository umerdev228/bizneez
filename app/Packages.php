<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Packages extends Model
{
    //
    protected $table = 'packages';


    protected $guarded = [];


    public function options()
    {
       return $this->belongsTo('App\PacakgeRelationOption' ,'id', 'package_id' );
    }

    public function packagesOptions(){
          return $this->hasMany('App\PacakgeRelationOption','package_id', 'id' );
    }


}
