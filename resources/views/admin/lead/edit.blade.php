@extends('admin.layouts.app')
@section('content')
    @include('admin.layouts.topbar')
    <!-- Google Chart JS -->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <!-- Start Page content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if(session()->has('message'))
                        <div class="alert alert-success" style="width:803px ">
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-9 mt-5">
                            <div class="card-box">
                                <form method="POST" action="{{ route('admin.leads.update', ['id' => $lead->id]) }}" enctype="multipart/form-data">
                                    @csrf
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="title">Title<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control valid" name="title" value="{{$lead->title}}" id="title" required />
                                                @if ($errors->has('title'))
                                                    <span class="error">{{ $errors->first('title') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="type">Type<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control valid" name="type" id="type" value="{{$lead->type}}" required />
                                                @if ($errors->has('type'))
                                                    <span class="error">{{ $errors->first('type') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="source">Source<span class="text-danger"></span></label>
                                                <select class="form-control form-check" name="source" id="source">
                                                    <option @if($lead->source === 'web') selected @endif value="web">Web</option>
                                                    <option @if($lead->source === 'email') selected @endif value="email">Email</option>
                                                    <option @if($lead->source === 'phone') selected @endif value="phone">Phone</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="last_name">phone<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control valid" name="phone" value="{{$lead->phone}}" id="phone" required />
                                                @if ($errors->has('phone'))
                                                    <span class="error">{{ $errors->first('phone') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="email">Email<span class="text-danger">*</span></label>
                                                <input type="email" class="form-control valid" name="email" value="{{$lead->email}}" id="email" required />
                                                @if ($errors->has('email'))
                                                    <span class="error">{{ $errors->first('email') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="image"> Image </label>
                                                <input type="file" class="form-control valid" name="image" id="image" >
                                                @if ($errors->has('image'))
                                                    <span class="error">{{ $errors->first('image') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="details">Details<span class="text-danger">*</span></label>
                                                <textarea id="details" class="form-control rounded-0" name="details" rows="5">{{$lead->details}}</textarea>
                                                @if ($errors->has('details'))
                                                    <span class="error">{{ $errors->first('details') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group mt-3">
                                        <input class="btn btn-primary" type="submit" value="Update Lead">
                                        <button onclick="history.back()" type="button" class="btn">Cancel</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
