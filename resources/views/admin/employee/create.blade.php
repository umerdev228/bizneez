@extends('admin.layouts.app')
@section('content')
    @include('admin.layouts.topbar')
    <!-- Google Chart JS -->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <!-- Start Page content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if(session()->has('message'))
                            <div class="alert alert-success" style="width:803px ">
                                {{ session()->get('message') }}
                            </div>
                        @endif

                        <div class="row">
                            <div class="col-9 mt-5">
                                <form id="employee-form" method="POST" action="{{ route('admin.employees.store') }}" enctype="multipart/form-data">
                                    @csrf
                                    <ul class="nav nav-tabs">
                                        <li class="nav-item">
                                            <a href="#create-employee" data-toggle="tab" aria-expanded="false" class="nav-link active show">
                                                <i class="fi-torso mr-2"></i> Create Employee
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="#salary" data-toggle="tab" aria-expanded="true" class="nav-link">
                                                <i class="fi-gallary mr-2"></i>Salary Commission
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="#analytics-perform" data-toggle="tab" aria-expanded="false" class="nav-link">
                                                <i class="fi-bookmark mr-2"></i>Analytics and Performance
                                            </a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane active show" id="create-employee">
                                            <div class="card">
                                                <div class="card-box">

                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="first_name">First Name<span class="text-danger">*</span></label>
                                                                <input type="text" class="form-control valid" name="first_name" id="first_name" required />
                                                                @if ($errors->has('first_name'))
                                                                    <span class="error">{{ $errors->first('first_name') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                       <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="middle_name">Middle Name<span class="text-danger"></span></label>
                                                                <input type="text" class="form-control valid" name="middle_name" id="middle_name"/>
                                                                @if ($errors->has('middle_name'))
                                                                    <span class="error">{{ $errors->first('middle_name') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="last_name">Last Name<span class="text-danger">*</span></label>
                                                                <input onfocusout="generate_email()" type="text" class="form-control valid" name="last_name" id="last_name" required />
                                                                @if ($errors->has('last_name'))
                                                                    <span class="error">{{ $errors->first('first_name') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="email">Email<span class="text-danger">*</span></label>
                                                                <input type="email" class="form-control valid" name="email" id="email" required />
                                                                @if ($errors->has('email'))
                                                                    <span class="error">{{ $errors->first('email') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="phone">Phone<span class="text-danger">*</span></label>
                                                                <input type="tel" class="form-control valid" name="phone" id="phone" required />
                                                                @if ($errors->has('phone'))
                                                                    <span class="error">{{ $errors->first('phone') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>


                                                         <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="phone">Password<span class="text-danger">*</span></label>
                                                                <input type="password" class="form-control valid" name="password" id="password" required />
                                                                @if ($errors->has('password'))
                                                                    <span class="error">{{ $errors->first('password') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>

                                                         <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="phone">Confirm Password<span class="text-danger">*</span></label>
                                                                <input type="password" class="form-control valid" name="c_password" id="c_password" required />

                                                            </div>
                                                        </div>




                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="mobile">Mobile</label>
                                                                <input type="number" class="form-control valid" name="mobile" id="mobile" />
                                                                @if ($errors->has('mobile'))
                                                                    <span class="error">{{ $errors->first('mobile') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="assistant_number">Assistant Number</label>
                                                                <input type="text" class="form-control valid" name="assistant_number" id="assistant_number"   />
                                                                @if ($errors->has('assistant_number'))
                                                                    <span class="error">{{ $errors->first('assistant_number') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>

                                                    <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="assistant_number">Demo Code<span class="text-danger">*</span></label>
                                                                <input type="text" class="form-control valid" name="demo_code" id="demo_code" value="{{$random}}" required />
                                                                @if ($errors->has('demo_code'))
                                                                    <span class="error">{{ $errors->first('demo_code') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>



                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="address">Address<span class="text-danger">*</span></label>
                                                                <input type="text" class="form-control valid" name="address" id="address" required />
                                                                @if ($errors->has('address'))
                                                                    <span class="error">{{ $errors->first('address') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="location_id">Location</label>
                                                                <input placeholder="Please Enter Location" type="text" class="form-control valid" name="location_id" value="" id="location_id"  />
                                                                @if ($errors->has('location_id'))
                                                                    <span class="error">{{ $errors->first('location_id') }}</span>

                                                                @endif
                                                            </div>
                                                        </div>




                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="area_id">Job Description<span class="text-danger">*</span></label>
                                                                <select class="form-control valid" name="area_id" id="area_id" >
                                                                    <option value="1">Managing director</option>
                                                                    <option value="2">Sales director</option>
                                                                    <option value="3">Regional manager</option>
                                                                    <option value="4">Sales manager</option>
                                                                    <option value="5">Team leader</option>
                                                                    <option value="6">Sales consultant</option>
                                                                </select>

                                                                @if ($errors->has('job_description'))
                                                                    <span class="error">{{ $errors->first('job_description') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="image">Company Logo</label>
                                                                <input type="file" class="form-control valid" name="image" id="image"/>
                                                                @if ($errors->has('image'))
                                                                    <span class="error">{{ $errors->first('image') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="area_id">Team Building</label>
                                                                <select class="form-control valid" name="team_building" id="team_building" >
                                                                    <option value="1">1</option>
                                                                    <option value="2">2</option>
                                                                    <option value="3">3</option>
                                                                    <option value="4">4</option>
                                                                    <option value="5">5</option>
                                                                    <option value="6">6</option>
                                                                    <option value="7">7</option>
                                                                    <option value="8">8</option>
                                                                    <option value="9">9</option>
                                                                    <option value="10">10</option>

                                                                </select>

                                                                @if ($errors->has('job_description'))
                                                                    <span class="error">{{ $errors->first('job_description') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>


                                                          <div class="col-sm-12">
                                                            <div class="form-group mt-3 float-right">
                                                                <button form="employee-form" class="btn btn-primary" type="submit" value="Save">Save</button>
                                                                <button onclick="history.back()" type="button" class="btn">Cancel</button>
                                                            </div>
                                                        </div>



                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="salary">
                                            <div class="card">
                                                <div class="card-box">
                                                   <div class="row"> 


                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="area_id">Enter Salary Commission</label>
                                                                 <input type="text" class="form-control valid" name="assistant_number" id="assistant_number"   />
                                                                @if ($errors->has('assistant_number'))
                                                                    <span class="error">{{ $errors->first('assistant_number') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>


                                                          <div class="col-sm-12">
                                                            <div class="form-group mt-3 float-right">
                                                                <button form="employee-form" class="btn btn-primary" type="submit" value="Save">Save</button>
                                                                <button onclick="history.back()" type="button" class="btn">Cancel</button>
                                                            </div>
                                                        </div>


 

                                                </div>
                                            </div>
                                        </div></div>
                                        <div class="tab-pane" id="analytics-perform">



 
                  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Login Logout', 'Daily'],
          ['Daily Talk to users',     11],
          ['Sent Quotation',      2],
          ['Login System',      2],
           
        ]);

        var options = {
          title: 'Employee Statistic Activities',
          'width':800,
          'height':800,
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
      }
    </script>                   

                                     
                                 

                                   
                                        <div id="piechart" ></div>
                                    
                            
                              




                                        </div>
                                    </div>
                                </form>

                          <!--       </form> -->
                            </div>
                        </div>
                </div>
            </div>
        </div>

@endsection


<script>
    function generate_email(){
      first_name = $('#first_name').val();
      last_name = $('#last_name').val();
      generate_email = first_name+'.'+last_name+'@bizneez.net';
      $('#email').val(generate_email);

    }
</script>