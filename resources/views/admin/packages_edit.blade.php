@extends('admin.layouts.app')
@section('content')
@include('admin.layouts.topbar')
<!-- Google Chart JS -->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<!-- Start Page content -->
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">
        
@if(session()->has('message'))
    <div class="alert alert-success col-9">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
        {{ session()->get('message') }}
    </div>
@endif   
@if(session()->has('error'))
    <div class="alert alert-danger col-9" >
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
        {{ session()->get('error') }}
    </div>
@endif   
        <div class="row">
          <div class="col-9 mt-5">
            <div class="card-box">
              <form action="{{ url('admin/packages/update') }}" method="POST"> 
                     {{ csrf_field() }}
                     <input type="hidden" value="{{$packages->id}}" name="id" />
                <div class="form-row">
                  <div class="form-group col-md-6">
                    <label for="FName">Package Name<span class="text-danger">*</span></label>
                    <input type="text" value="{{$packages->name}}"  maxlength="35" name="name" autocomplete="off" class="form-control" parsley-trigger="change" required="required">
                      @if ($errors->has('title'))
                              <span class="error">{{ $errors->first('title') }}</span>
                      @endif   

                  </div>
      

                   <div class="form-group col-md-6">
                    <label for="Price">Price<span class="text-danger">*</span></label>
                    <input type="text" value="{{$packages->amount}}" name="price" class="form-control" parsley-trigger="change" required="required" />
                      @if ($errors->has('price'))
                              <span class="error">{{ $errors->first('price') }}</span>
                      @endif   
                  </div>
                     </div>
                <div class="form-row ">
                  <div class="form-group col-md-6">
                    <label for="Duration">Duration<span class="text-danger">*</span></label>
                    <select type="text" name="duration" class="form-control" data-placeholder="Duration" parsley-trigger="change" required="required" tabindex="-1" aria-hidden="true">
                      <option value="month" @if($packages->package_duration=='month') selected @endif >Monthly</option> 
                      <option value="month" @if($packages->package_duration=='6month') selected @endif >6 Month</option> 
                      <option value="year" @if($packages->package_duration=='year') selected @endif >Yearly</option>
                    </select>
                  </div>

                  <!-- <div class="form-group col-md-6">
                    <label for="Duration">If Monthly</label>
                    <select type="text" name="month_interval"  class="form-control" data-placeholder="month_interval" parsley-trigger="change" tabindex="-1" aria-hidden="true">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option>
                    </select>
                  </div> -->
              </div>

                <div class="form-group">
                  <label for="Description">Pacakge Options</label>
                  <div class="form-row">
                  @foreach($option as $opt)
                  @php  
                  $options = DB::table('pacakge_relation_options')->select('*')->where('package_id', $packages->id)->where('option_id',$opt->id)->get();  
                   @endphp
                    <div class="col-md-3">
                     <input type="checkbox" value="{{$opt->id}}" @if(count($options)>0) checked="checked" @endif name="chk[]" /> {{$opt->option_name}}
                    </div>
                  @endforeach
                   </div>
                </div>
                <div class="form-group">
                <label for="Description">Description<span class="text-danger">*</span></label>
                
                 <textarea class="form-control" rows="5"  maxlength="30" spellcheck="false" id="description" name="description">{{$packages->description}}</textarea>
                  @if ($errors->has('description'))
                  <span class="error">{{ $errors->first('description') }}</span>
                  @endif  
                  <br> 
                  <input type="radio" id="active" checked="checked" name="status" value="1">
                  <label for="active">Active</label>

                  <!-- <label for="OR">OR</label> -->
                  <br>
                  
                  <input type="radio" id="Inactive"  name="status" value="0">
                  <label for="Inactive">Inactive</label>
                  
                </div>

                <button type="submit" class="btn btn-primary">Save</button>
                <button onclick="history.back()" type="button" class="btn">Cancel</button>
              </form>
              </div> <!-- end card-box -->
            </div>
            
            </div> <!-- container -->
            </div> <!-- content -->
            
            @include('admin.layouts.footer')
            @endsection
           
            <script>
              jQuery(document).ready(function(){
                  $('.summernote').summernote({
                      height: 150,                 // set editor height
                      minHeight: null,             // set minimum height of editor
                      maxHeight: null,             // set maximum height of editor
                      focus: false                 // set focus to editable area after initializing summernote
                  });

                  $('.inline-editor').summernote({
                      airMode: true
                  });
              });
            </script>