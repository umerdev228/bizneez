<?php

namespace App\Http\Controllers\Architect;

use App\Category;
use App\Http\Controllers\Controller;
use App\Media;
use App\Product;
use App\ProductCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $breadcrumb = ucfirst(Auth::user()->roles[0]->name)." / Product / Index";
        $products = Product::with('media')->with('categories')->with('manufacturer')->orderBy('created_at', 'desc')->get();
        $categories = Category::where('parent', 0)->get();
        return view('architect.product.index', compact('products', 'breadcrumb', 'categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $breadcrumb = ucfirst(Auth::user()->roles[0]->name)." / Product / New";
        $categories = Category::all();
        return view('admin.product.create', compact('categories', 'breadcrumb'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $check = Product::where('slug', Str::slug($request->name))->get();
        if (!count($check) > 0) {
            $product = Product::create([
                'name' => $request->name,
                'slug' => Str::slug($request->name),
                'description' => $request->description,
                'price' => $request->price,
                'second_price' => $request->second_price,
                'discount' => $request->discount,
                'quantity' => $request->quantity,
                'user_id' => Auth::id(),
            ]);

            foreach ($request->categories as $category) {
                ProductCategory::create([
                    'product_id' => $product->id,
                    'category_id' => $category,
                ]);
            }
            if ($request->has('image')) {
                foreach ($request->image as $image) {
                    $imageName = $image->getClientOriginalName();
                    $image->move(public_path('/uploads/images/products/'.$product->id.'/'),$imageName);

                    Media::create([
                        'name' => $product->name,
                        'item_id' => $product->id,
                        'item_type' => 'product',
                        'path' => '/uploads/images/products/' . $product->id . '/' . $imageName,
                    ]);
                }
            }
            if ($request->has('color')) {
                foreach ($request->color as $image) {
                    $imageName = $image->getClientOriginalName();
                    $image->move(public_path('/uploads/images/products/' . $product->id . '/color/' ),$imageName);

                    Media::create([
                        'name' => $product->name,
                        'item_id' => $product->id,
                        'item_type' => 'product_color',
                        'path' => '/uploads/images/products/' . $product->id . '/color/' . $imageName,
                    ]);
                }
            }
        }
        toastr()->success('Product Created Successfully');
        return redirect()->route('architect.products.index', ['role' => Auth::user()->roles[0]->name]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $breadcrumb = ucfirst(Auth::user()->roles[0]->name)." / Product / Show";
        $id = (\request()->route()->parameter('id'));
        $product = Product::where('id', $id)->with('colors')->with('media')->with('categories')->first();
//        dd($product->colors);
        return view('architect.product.show', compact('product', 'breadcrumb'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        //
        $breadcrumb = ucfirst(Auth::user()->roles[0]->name)." / Product / Edit";

        $id = (\request()->route()->parameter('id'));
        $product = Product::where('id', $id)->with('colors')->with('media')->with('categories')->first();
        $product_category = ProductCategory::where('product_id', $id)->pluck('category_id')->toArray();
        $categories = Category::all();
//        dd($product, $product_category, $categories);

        return view('admin.product.edit', compact('product', 'categories', 'product_category', 'breadcrumb'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::where('id', $id)->update([
            'name' => $request->name,
            'slug' => Str::slug($request->name),
            'description' => $request->description,
            'price' => $request->price,
            'discount' => $request->discount,
            'quantity' => $request->quantity,
        ]);
        ProductCategory::where('product_id', $id)->delete();
        foreach ($request->categories as $category) {
            ProductCategory::create([
                'product_id' => $id,
                'category_id' => $category,
            ]);
        }
        $product = Product::where('id', $id)->first();
        if ($request->has('color')) {
            foreach ($request->color as $image) {
                $imageName = $image->getClientOriginalName();
                $image->move(public_path('/uploads/images/products/' . $id . '/color/' ),$imageName);

                Media::create([
                    'name' => $product->name,
                    'item_id' => $product->id,
                    'item_type' => 'product_color',
                    'path' => '/uploads/images/products/' . $id . '/color/' . $imageName,
                ]);
            }
        }
        toastr()->success('Product Updated Successfully');
        return redirect()->route('admin.products.index', ['role' => Auth::user()->roles[0]->name]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Product::where('id', $id)->delete();
        ProductCategory::where('product_id', $id)->delete();
        Media::where('item_id', $id)->where('item_type', 'product')->delete();
        return redirect()->back();
    }

    public function productImageCreateSession(Request $request) {
        $image = $request->file('file');
        $imageName = $image->getClientOriginalName();
        $image->move(public_path('uploads/images/products/'),$imageName);

        $media = Media::create([
            'name' => 'session',
            'item_id' => 'session',
            'item_type' => 'product',
            'path' => '/uploads/images/products/' . $imageName,
        ]);
        return response()->json(['type' => 'success', 'data' => $media->id]);
    }

    public function productColorCreateSession(Request $request) {
        dd($request->all());
//        Session::put('variableName', $value);
    }

    public function filterByCategory($id) {
        $breadcrumb = ucfirst(Auth::user()->roles[0]->name)." / Product / Index";
        $category = Category::where('id', $id)->with('products')->first();
        $products = $category->products;
        $categories = Category::all();
        return view('architect.product.index', compact('products', 'breadcrumb', 'categories'));
    }



}
