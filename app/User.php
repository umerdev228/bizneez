<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'first_name', 'last_name', 'middle_name', 'email', 'password', 'status', 'created_by'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function profile() {
        return $this->belongsTo(Profile::class, 'id', 'user_id');
    }

    public function user() {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function messages()
    {
        return $this->hasMany(Message::class);
    }

    public function representatives() {
        return $this->hasMany(Represent::class, 'user_id', 'id');
    }

    public function ranges() {
        return $this->hasMany(ProductRange::class, 'created_by', 'id')->with('images');
    }

    public function salary() {
        return $this->belongsTo(EmployeeSalary::class, 'id', 'employee_id');
    }
}
