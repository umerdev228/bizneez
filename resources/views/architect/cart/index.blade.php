@extends('admin.layouts.app')
@section('content')
    @include('admin.layouts.topbar')

    <style>
        h3.h3{text-align:center;margin:1em;text-transform:capitalize;font-size:1.7em;}

        .fa-eye, .fa-eye-slash {
            border: none;
            padding: 9px 13px;
            background: #0000 !important;
        }
    </style>

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    @if(session()->has('message'))
                        <div class="alert alert-success mt-5" id="myElem" >
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-12 mt-5">
                            <div class="card">
                                <div class="card-body">
                                    <table class="table table-hover m-0 tickets-list table-actions-bar dt-responsive nowrap">
                                        <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Name</th>
                                                <th>Size</th>
                                                <th>Quantity</th>
                                                <th>Action</th>
                                                <th>Price</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($cartCollection as $product)
                                            <tr id="row-{{$product->id}}">

                                                <td>{{$product->id}}</td>
                                                <td>{{$product->name}}</td>
                                                <td>
                                                    @php
                                                        $sizes = \App\Size::where('product_id', $product->id)->get();
                                                    @endphp
                                                    @if(count($sizes) > 0)
                                                        <select class="form-control rounded-0" name="size-{{$product->id}}" id="size-{{$product->id}}" onchange="UpdateSize({{$product->id}})">
                                                            <option value="0" selected disabled> Choose Size </option>
                                                            @foreach($sizes as $size)
                                                                <option value="{{$size->id}}"> {{ $size->name }} </option>
                                                            @endforeach
                                                        </select>
                                                    @endif
                                                </td>
                                                <td>
                                                    <div class="row">
                                                        <button onclick="increment({{$product->id}})" class="btn btn-default"><span class="fa fa-plus"></span></button>
                                                        <input id="quantity-{{$product->id}}" value="{{ $product->quantity }}" type="number" class="form-control" style="width: 50px;">
                                                        <button onclick="decrement({{$product->id}})" class="btn btn-default"><span class="fa fa-minus"></span></button>
                                                    </div>
                                                </td>
                                                <td>
                                                    <button class="btn btn-danger" onclick="deleteCartItem({{ $product->id }})"><i class="fa fa-times"></i></button>
                                                </td>
                                                <td><span>&#163;</span>{{$product->price}}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th>Total: <span>&#163;</span><span id="total_price">{{ $total }}</span></th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                    @if(count($cartCollection) > 0)
                                        <div class="card-body">
                                            <div class="float">
                                                <div class="float-right">
                                                    <a href="{{ route('architect.products.order.store') }}" class="btn btn-success">Confirm Order</a>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div><!-- end col -->
                        </div><!-- end col -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--    @include('admin.layouts.footer')--}}
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        function addToCart(id) {
            $.ajax({
                type:'POST',
                url:"{{ route('add.to.cart') }}",
                data:{_token: CSRF_TOKEN, id:id},
                success:function(response){
                    console.log(response)
                }
            });
        }
    </script>

    <script>
        function increment (id) {
            let CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                type:'POST',
                data: {_token: CSRF_TOKEN, id: id},
                url:"{{ route('cart.increment.quantity') }}",
                success:function(response){
                    if (response.type === 'success') {
                        document.querySelector('#quantity-'+id).value = response.item_quantity
                        document.querySelector('#total_price').textContent = response.total
                        toastr.success('Incremented Successfully')
                    }
                }
            });
        }

        function decrement (id) {
            let CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                type:'POST',
                data: {_token: CSRF_TOKEN, id: id},
                url:"{{ route('cart.decrement.quantity') }}",
                success:function(response){
                    if (response.type === 'success') {
                        document.querySelector('#quantity-'+id).value = response.item_quantity
                        document.querySelector('#total_price').textContent = response.total
                        toastr.success('Decremented Successfully')
                    }
                }
            });
        }

        function deleteCartItem (id) {
            let CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                type:'POST',
                data: {_token: CSRF_TOKEN, id: id},
                url:"{{ route('cart.item.delete') }}",
                success:function(response) {
                    if (response.type === 'success') {
                        $('#row-'+id).remove();
                        document.querySelector('#total_price').textContent = response.total
                        toastr.success('Removed Successfully')
                    }
                }
            });
        }
        function UpdateSize(id) {
            console.log(id, document.getElementById('size-'+id).value)
            let size = document.getElementById('size-'+id).value
            let CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                type:'POST',
                data: {_token: CSRF_TOKEN, id: id, size_id: size},
                url:"{{ route('cart.item.update.size') }}",
                success:function(response) {
                    if (response.type === 'success') {
                        toastr.success('Update Size Successfully')
                    }
                }
            });
        }

    </script>
@endsection
