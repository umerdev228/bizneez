@extends('admin.layouts.app')
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    @if(session()->has('message'))
                        <div class="alert alert-success mt-5" id="myElem" >
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-12 mt-5">
                            <div class="card">
                                <div class="card-body">
                                    <a data-toggle="modal" data-target="#myModal" href="javascript:void(0)"  class="btn btn-success" ><i class="fa fa-plus"></i>Add New Color </a>
                                </div>
                                <div class="card-body">
                                    <table class="table table-hover m-0 tickets-list table-actions-bar dt-responsive nowrap" width="100%" id="normal-table">
                                        <thead>
                                        <tr>
{{--                                            <th>Created By</th>--}}
                                            <th>Color Code</th>
{{--                                            <th>Key</th>--}}
{{--                                            <th>Phone</th>--}}
                                            <th data-orderable="false">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($colors as $color)
                                            <tr>
{{--                                                <td>{{$color->name}}</td>--}}
                                                <td><div style="width: 25px; height: 25px; background-color: {{$color->code}}"></div></td>
{{--                                                <td>{{$color->email}}</td>--}}
                                                <td>
                                                    <div class="btn-group">
                                                        <a class="btn" href="{{ route('admin.colors.delete', ['id' => $color->id] ) }}">
                                                            <i class="fa fa-close"></i>
                                                        </a>
                                                    </div>

                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div> <!-- end card -->
                        </div><!-- end col -->
                    </div>
                </div>
            </div>
        </div> <!-- container -->
    </div> <!-- content -->

    <!-- The Modal -->
    <div class="modal" id="myModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="{{route('admin.colors.store')}}" method="post">
                    @csrf
                    <div class="modal-header">
                        <h4 class="modal-title">Add New Color</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="color">Color</label>
                            <input class="form-control form-group" type="color" name="color" id="color">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success">Add Color</button>
                    </div>
                </form>

            </div>
        </div>
    </div>

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

@endsection
