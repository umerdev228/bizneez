<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToCataloguePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('catalogue_pages', function (Blueprint $table) {
            //
            $table->boolean('option_1')->nullable();
            $table->boolean('option_2')->nullable();
            $table->boolean('option_3')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('catalogue_pages', function (Blueprint $table) {
            //
        });
    }
}
