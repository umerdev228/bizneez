<?php

namespace App\Http\Controllers;
//use Illuminate\Support\Facades\File;
//use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
//use Illuminate\Support\Facades\Hash;
use Session;
use Stripe;
use App\User;
use App\Merchant;
use App\PaymentHistory;
use App\Packages;
use App\History_log;
use Auth;
use App\Packageoptions;
//use Mail;
 

 
  
class StripePaymentController extends Controller
{

     public function __construct()
    {
         $this->middleware('auth');
    }
 
    public function pricing(){ 
      $packages = Packages::where('status', 1)->get();
 
       // $packages = Packages::with('packagesOptions')->get();
       $option = Packageoptions::get();
       //print "<pre>";
       //print_r($packages); die();
       //dd($packages);
       $data = ['packages' => $packages, 'option' =>$option,];
       return view('pricing')->with($data);
    }

    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function stripe(Request $request)
    {
        $package = Packages::where('id', $request->get('p_id') )->first(); 
         $data = [
            'price' => $request->price, 
            'customer_name' => Auth::user()->name,
            'email' => Auth::user()->email,
            'package' => $package,
        ];
        return view('stripe')->with($data);
    }


/*public function stripe_getting_history(Request $request)
    {
          $stripe = new \Stripe\StripeClient(
          'sk_test_QMX38t3rVMu1SxAaM1FS4OjJ00wLlUhQd8' 
          );
        return  $stripe->issuing->transactions->retrieve(
          'ch_1GuEteLP8fXawno0l2m3N7s4',
          []
          );
    }*/

  
    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function stripePost(Request $request)
    {

        // dd('basdasdas');
        $users_strip_account_exist = Merchant::where('user_id',Auth::user()->id)->where('stripe_customer_id','')->count();
        Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        
 

 //=============================================================
 //===================== Reoccuring Payment Process ============
 //=============================================================
/*        if($users_strip_account_exist==1){
                //Step1: Create Customer for Reoccuring billing
               $create_customer = Stripe\Customer::create([
                    'description' => 'My First Test Customer (created for API docs)',
                    'email' => $request->customer_email,
                    'name' => $request->customer_name,
                    "source" => $request->stripeToken,
                ]);
                //Step2: Create Invoice
                $create_invoice = \Stripe\InvoiceItem::create([
                    'customer' => $create_customer->id,
                    'amount' => $request->amount * 100,
                    'currency' => 'usd',
                    'description' => 'Subscription Fee',
                ]);
                $transaction_id = $create_invoice->id;
                //Step3: Assign Subsribed Plan
                $subscription = Stripe\Subscription::create([
                    'customer' =>   $create_customer->id,
                    'items' => [['plan' => 'plan_HEWOBWE0XaLWEZ' ]],
                ]); 


        }//end if
 

 if($users_strip_account_exist==1){ 
    }
    else
    {
        $return = Stripe\Charge::create ([
                "amount" => $request->amount * 100,
                "currency" => "usd",
                "source" => $request->stripeToken,
                "description" => "Test payment from TMS user become Premium" 
        ]);
        $transaction_id = $return->id;
    }*/

                // dd($complate_name);

  $return = Stripe\Charge::create ([
                "amount" => $request->amount * 100,
                "currency" => "usd",
                "source" => $request->stripeToken,
                "description" => "Test payment from TMS user become Premium." ,
                
        ]);

                // $complate_name = $request->post('fname') .' '.$request->post('lname'); Merchant::where('user_id',Auth::user()->id )->with('user')->first();
                $complate_name = User::where('id' ,Auth::user()->id )->first();
                $description = $complate_name->name .' ' . "is now prenium".date("d. M Y");  
                $data_notification = ['description' => $description,  'user_id' => Auth::user()->id,'inttype' => 2 , 'intstatus' => 0 ]; 
                 // dd($data_notification);
                History_log::create($data_notification); 


        $transaction_id = $return->id;
        $date = date('Y-m-d');
        $package_days = '+ '.$request->package_days. ' days';
        //date_add($date,date_interval_create_from_date_string($package_days));
        //$expire_date= date_format($date,"Y-m-d");

        //$expire_date=Date('Y-m-d', strtotime($f)); 
        $expire_date=date('Y-m-d', strtotime($date.  $package_days));

        $data = [
            'user_id' => Auth::user()->id,
            'amount' => $request->amount,
            'transaction_id' => $transaction_id,
            'expire_date' => $expire_date,
            'package_id' => $request->package_id,
        ];

        $user = PaymentHistory::create($data);     
               
        $merchant = Merchant::where('user_id',Auth::user()->id )->with('user')->first();
        $merchant->expire_date = $expire_date;
        $merchant->inttype = 1;
       
            /*if($users_strip_account_exist==1)
                $merchant->stripe_customer_id = $create_customer->id;*/
       
        $merchant->save();

        $this->curl_update_expiry_date($merchant->email,$expire_date);


        $data_post = [
            'expire_date' => $expire_date,
            'inttype' =>1,
            'user_id' => Auth::user()->id,
        ];
        return view('stripe_after_payment')->with($data_post);

       /* print "<pre>";    
        print_r($return);
        dd();
*/
       // Session::flash('success', 'Payment successful!');
          
       // return back();
    }


       Public function curl_update_expiry_date($email_id,$exp_date){
        //print "test"; die();
          $ch = curl_init();
          
          $url = env('APP_URL'). "api/update_expire_date?email=".$email_id."&expire_date=".$exp_date;
          curl_setopt($ch,CURLOPT_URL,$url);
          //curl_setopt($ch,CURLOPT_POST, 0);                //0 for a get request
          //curl_setopt($ch,CURLOPT_POSTFIELDS,$postvars);
          curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch,CURLOPT_CONNECTTIMEOUT ,3);
          curl_setopt($ch,CURLOPT_TIMEOUT, 20);
          $response = curl_exec($ch);
        //  print "curl response is:" . $response;
          curl_close ($ch);
 
    }


}