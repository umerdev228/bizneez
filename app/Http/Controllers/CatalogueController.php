<?php

namespace App\Http\Controllers;

use App\Catalogue;
use App\Company;
use App\Media;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CatalogueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $breadcrumb = 'Catalogue';
        $manufacturers = User::role('manufacturer')->get();
        $images = Media::where('name', 'gallery')->get();
        $companies = Company::all();
        $productsmdd  = DB::table('mdd_price_list_csv')->get();

        return view('admin.catalogue.index', compact('breadcrumb', 'manufacturers', 'images', 'companies', 'productsmdd'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $breadcrumb = 'Create Template';
        return view('admin.template.create', compact('breadcrumb'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $image = $request->file('file');
        $imageName = $image->getClientOriginalName();
        $image->move(public_path('/uploads/images/templates/'),$imageName);

        Media::create([
            'item_type' => 'template',
            'name' => 'template',
            'path' => '/uploads/images/templates/' . $imageName,
        ]);

        return response()->json([
            'type' => 'success',
            'message' => 'Image Uploaded Successfully'
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Catalogue  $catalogue
     * @return \Illuminate\Http\Response
     */
    public function show(Catalogue $catalogue)
    {
          
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Catalogue  $catalogue
     * @return \Illuminate\Http\Response
     */
    public function edit(Catalogue $catalogue)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Catalogue  $catalogue
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Catalogue $catalogue)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Catalogue  $catalogue
     * @return \Illuminate\Http\Response
     */
    public function destroy(Catalogue $catalogue)
    {
        //
    }

    public function catalogueStore(Request $request) {
        $image = $request->file('file');
        $imageName = $image->getClientOriginalName();
        $image->move(public_path('/uploads/images/catalogue/'),$imageName);

        Media::create([
            'item_type' => 'template',
            'name' => 'template',
            'path' => '/uploads/images/catalogue/' . $imageName,
        ]);

        Catalogue::create([
           'name' => $request['name'],
           'description' => $request['description'],
           'background' => $request['background'],
           'template' => $request['template'],
        ]);
    }
}
