@extends('admin.layouts.app')
@section('content')
@include('admin.layouts.topbar')
<!-- Google Chart JS -->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<!-- Start Page content -->
<div class="content">
  
  <form action="{{url('admin/payment/search')}}" method="get" role="search">
    {{ csrf_field() }}
    <div class="row">
            <!-- <div class="col-md-3" style="margin-left: 30px ">
              <select name="package_name" class="form-control">
                <option value="0"> --Select Package-- </option>
                @foreach($packages as $pak)
                <option value="{{$pak->id}}">{{$pak->name}}</option>
                @endforeach
              </select>
            </div> -->
            <div class="col-md-3 mt-5" style="margin-left: 30px ">
            <select name="package_name" class="form-control">
              <option value="" > --Select Package-- </option>
              @foreach($packages as $pak)
                <option value="{{$pak->id}}">{{$pak->name}}</option>
              @endforeach
            </select>
      </div>
      <div class="col-md-3 mt-5">
            <input type="text" autocomplete="off" name="start_date" class="form-control" value="" placeholder="Start Date mm/dd/yyyy" id="datepicker1">
      </div>
      <div class="col-md-3 mt-5">
        <input type="text" autocomplete="off" name="end_date" class="form-control" value="" placeholder="End Date mm/dd/yyyy" id="datepicker2">
      </div>
      <div class="col-md-3 mt-5 ">
        <select name="package" class="form-control">
          <option value="">--Package Status--</option>
          <option @if(request()->package && request()->package==0) selected @endif value="0">In Active</option>
          <option @if(request()->package && request()->package==1) selected @endif  value="1">Active</option>
        </select>
      </div>

      <button type="submit" class="btn btn-info mt-5">Search</button>
   </div>   
 </form>
    <!-- for search method -->
    <!--  <input name="txtSearch" type="text" placeholder="Search..." class="form-control"> -->
    
    
    
    <!-- <div class="col-md-2">
      <input class="btn" type="submit" value="Saerch" /></div>
      
      <form action=""> {{ csrf_field() }}
        <div class="row">
          <div class="col-md-2"> <select name="" class="form-control">
            <option value="0">--Select Package--</option>
            @foreach($packages as $pak)
            <option value="{{$pak->id}}">{{$pak->name}}</option>
            @endforeach
          </select></div>
          <div class="col-md-2"><input type="text" name="created" class="form-control" value="" placeholder="mm/dd/yyyy" id="datepicker1"></div>
          <div class="col-md-2"></div>
          <div class="col-md-2"><input class="btn" type="submit" value="Saerch" /></div>
        </div>
        -->


     
      <div class="container-fluid" >
        <div class="row">
          <div class="col-12">
            <div class="row">
              <div class="col-12 ">
                <div class="card-box">
                  <table class="table table-hover m-0 tickets-list table-actions-bar dt-responsive nowrap" cellspacing="0" width="100%"   id="datatable">
                    <thead>
                      <tr>
                        <th>User Name</th>
                        <th>Amount</th>
                        <th>Payment Date</th>
                        <th>Expire</th>
                        <th data-orderable="false"
>Status</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($payment_history as $payment)
                        @php 
                            $user = App\User::where('id', $payment->user_id)->first(); 


                            if (is_null($user)) {
                            $name = 'User Removed';
                            }
                            else 
                            {
                           $name = $user->name;
                          }
                        @endphp
                      <tr>
                        <td> {{$name}}    </td>
                        <td> {{$payment->amount}}</td>
                        <td>{{$payment->created_at}}</td>
                        <td>{{$payment->expire_date}}</td>
                        <td>success</td>
                      </tr>
                    
                      @endforeach
                      
                    </tbody>
                  </table>
                  
                  
                  </div> <!-- end card -->
                  </div><!-- end col -->
                </div>
                
              </div>
            </div>
            
            </div> <!-- container -->
</div> <!-- content -->
            
            @include('admin.layouts.footer')
            @endsection
         
            <style>
              .arrow-none{margin-top:20px!important;}
            button{

            border-style: none;}
            .card-box{
              padding-right:  20px;
            }
            </style>