<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

 

class Architect extends Model
{
    protected $table = 'users';
    protected $guarded = [];

    public function user() {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

}