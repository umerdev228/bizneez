@extends('admin.layouts.app')
@section('content')
@include('admin.layouts.topbar')
<!-- Google Chart JS -->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<!-- Start Page content -->
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">
        @if(session()->has('message'))
        <div class="alert alert-success" style="width:803px ">
          {{ session()->get('message') }}
        </div>
        @endif
        <div class="row">
          <div class="col-9 mt-5">
            <div class="card-box">
              <form method="POST" action="{{ route('admin/user/update') }}">
                        @csrf
                        <input type="hidden" value="{{$user->id}}" name="id">
                        <div class="row">
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label for="Description">First Name<span class="text-danger">*</span></label>
                              <input class="form-control valid" name="fname" id="fname" type="text" autocomplete="off" onfocus="this.placeholder = ''" value="{{$fname}}" onblur="this.placeholder = 'First Name'"  pattern="[A-Za-z\s]+"  maxlength="20" required="required" />
                              @if ($errors->has('fname'))
                              <span class="error">{{ $errors->first('fname') }}</span>
                              @endif
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label for="Description">Last Name<span class="text-danger">*</span></label>
                              <input  value="{{$lname}}" class="form-control valid" name="lname" autocomplete="off" id="lname" type="text" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Last Name'"  pattern="[A-Za-z\s]+"  maxlength="20" required="required" />
                              @if ($errors->has('lname'))
                              <span class="error">{{ $errors->first('lname') }}</span>
                              @endif
                            </div>
                          </div>
                          <div class="col-12">
                            <div class="form-group">
                              <label for="Description">Company<span class="text-danger">*</span></label>
                              <input class="form-control"  value="{{$user->company}}" name="company"  id="company" type="text" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Company'" pattern="[A-Za-z0-9@\s]+" required="required"   maxlength="30" />
                              @if ($errors->has('company'))
                              <span class="error">{{ $errors->first('company') }}</span>
                              @endif
                            </div>
                          </div>
                          <div class="col-12">
                            <div class="form-group">
                              <label for="Description">Phone<span class="text-danger">*</span></label>
                              <input class="form-control"  value="{{$user->phone}}" name="phone" id="phone" pattern="^[0-9-+\s()]*$" minlength="4"  maxlength="15" type="tel" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Phone'"  required="required" />
                              @if ($errors->has('phone'))
                              <span class="error">{{ $errors->first('phone') }}</span>
                              @endif
                            </div>
                          </div>
                          <div class="col-12">
                            <div class="form-group">
                              <label for="Description">Description<span class="text-danger">*</span></label>
                              <!-- <textarea class="form-control summernote" rows="5" spellcheck="false" id="description" name="description">{{$user->description}}</textarea> -->
                             
                             <!--  <textarea  rows="5" spellcheck="false" id="description" name="description" >{{$user->description}}</textarea> -->
                               <textarea class="form-control rounded-0" name="description" maxlength="100"  id="exampleFormControlTextarea2" rows="5">{{$user->description}}</textarea>
                            <!-- ///////////////////////////////// text editer value="{{$user->description}}" -->
                              @if ($errors->has('description'))
                              <span class="error">{{ $errors->first('description') }}</span>
                              @endif
                            </div>
                          </div>
                        </div>
                        <div class="form-group mt-3">
                          
                          <input class="btn btn-primary" type="submit" value="Update">
                          <button onclick="history.back()" type="button" class="btn">Cancel</button>
                        </div>
              </form>
              </div> <!-- end card-box -->
            </div>
            
            </div> <!-- container -->
            </div> <!-- content -->

            @include('admin.layouts.footer')
            @endsection
            <style>
            .arrow-none{margin-top:20px!important;}
            </style>
<script>
    jQuery(document).ready(function(){
    $('.summernote').summernote({
    height: 150,                 // set editor height
    minHeight: null,             // set minimum height of editor
    maxHeight: null,             // set maximum height of editor
    focus: false                 // set focus to editable area after initializing summernote
    });
$('.inline-editor').summernote({
airMode: true
});    
});



</script>