@extends('admin.layouts.app')
@section('content')
    @include('admin.layouts.topbar')
    <!-- Google Chart JS -->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <!-- Start Page content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if(session()->has('message'))
                        <div class="alert alert-success" style="width:803px ">
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-9 mt-5">
                            <div class="card-box">
                                <form method="POST" action="{{ route('admin.clients.store') }}" enctype="multipart/form-data">
                                    @csrf
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="company_name">Company Name<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control valid" name="company_name" id="company_name" required />
                                                @if ($errors->has('company_name'))
                                                    <span class="error">{{ $errors->first('company_name') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="first_name">First Name<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control valid" name="first_name" id="first_name" required />
                                                @if ($errors->has('first_name'))
                                                    <span class="error">{{ $errors->first('first_name') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="middle_name">Middle Name<span class="text-danger"></span></label>
                                                <input type="text" class="form-control valid" name="middle_name" id="middle_name"/>
                                                @if ($errors->has('middle_name'))
                                                    <span class="error">{{ $errors->first('middle_name') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="last_name">Last Name<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control valid" name="last_name" id="last_name" required />
                                                @if ($errors->has('last_name'))
                                                    <span class="error">{{ $errors->first('first_name') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="email">Email<span class="text-danger">*</span></label>
                                                <input type="email" class="form-control valid" name="email" id="email" required />
                                                @if ($errors->has('email'))
                                                    <span class="error">{{ $errors->first('email') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="phone">Phone<span class="text-danger">*</span></label>
                                                <input type="tel" class="form-control valid" name="phone" id="phone" required />
                                                @if ($errors->has('phone'))
                                                    <span class="error">{{ $errors->first('phone') }}</span>
                                                @endif
                                            </div>
                                        </div>
                            <!--             <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="mobile">Mobile<span class="text-danger">*</span></label>
                                                <input type="number" class="form-control valid" name="mobile" id="mobile" required />
                                                @if ($errors->has('mobile'))
                                                    <span class="error">{{ $errors->first('mobile') }}</span>
                                                @endif
                                            </div>
                                        </div> -->
                                     
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="address">Address<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control valid" name="address" id="address" required />
                                                @if ($errors->has('address'))
                                                    <span class="error">{{ $errors->first('address') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="team_building">Team Building<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control valid" name="team_building" id="team_building" required />
                                                @if ($errors->has('team_building'))
                                                    <span class="error">{{ $errors->first('team_building') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="demo_code">Demo Code<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control valid" name="demo_code" id="demo_code" required />
                                                @if ($errors->has('demo_code'))
                                                    <span class="error">{{ $errors->first('demo_code') }}</span>
                                                @endif
                                            </div>
                                        </div>


                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="location_id">Location<span class="text-danger">*</span></label>
                                                 <input placeholder="Please Enter Location" type="text" class="form-control valid" name="location_id" value="" id="location_id"  />
                                                @if ($errors->has('location_id'))
                                                    <span class="error">{{ $errors->first('location_id') }}</span>

                                                @endif
                                            </div>
                                        </div>
                                         <div class="col-sm-6">

                                         </div>

                                        <div id="representative" style="width:100%; margin-left:19px">
                                            <div id="0" class="row">
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label for="location_id">Representative Name </label>
                                                        <input placeholder="Please Enter Representative Name" type="text" class="form-control valid" name="represent_name[]" value="" id="represent_name"  />
                                                        @if ($errors->has('location_id'))
                                                            <span class="error">{{ $errors->first('represent_name') }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label for="area_id">Representative Email </label>
                                                        <input placeholder="Please Enter Representative email" type="text" class="form-control valid" name="represent_email[]" value="" id="represent_email"  />
                                                        @if ($errors->has('represent_email'))
                                                            <span class="error">{{ $errors->first('represent_email') }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label for="area_id">Representative Phone </label>
                                                        <input placeholder="Please Enter Representative Phone" type="text" class="form-control valid" name="represent_mobile[]" value="" id="represent_mobile"  />
                                                        @if ($errors->has('represent_mobile'))
                                                            <span class="error">{{ $errors->first('represent_mobile') }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <button type="button" onclick="appendFieldsR()" style="    margin-top: 30px;border-radius: 10px" class="btn btn-success valid align-baseline"><i class="fa fa-plus"></i> </button>
                                                        {{--                                                                <button style="border-radius: 10px" class="btn btn-danger valid align-baseline"><i class="fa fa-minus"></i> </button>--}}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="image">Company Logo </label>
                                                <input type="file" class="form-control valid" name="image" id="image" >
                                                @if ($errors->has('image'))
                                                    <span class="error">{{ $errors->first('image') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="Description">Description<span class="text-danger">*</span></label>
                                                <textarea  placeholder="Exp: We are Client of office furniture" id="Description" class="form-control rounded-0" name="description" rows="5"></textarea>
                                                @if ($errors->has('description'))
                                                    <span class="error">{{ $errors->first('description') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group mt-3">

                                        <input class="btn btn-primary" type="submit" value="Create Client">
                                        <button onclick="history.back()" type="button" class="btn">Cancel</button>
                                    </div>
                                </form>
                            </div> <!-- end card-box -->
                        </div>

                    </div> <!-- container -->
                </div> <!-- content -->
            </div>
        </div>
    </div>







    <script>
        var i = 0
        function appendFieldsR() {
            i = i+1
            console.log('clicked')
            //$('#company_keywords').val()='fefe';
            var html_input =
                `
                                                    <div id="r`+i+`" class="row">
                                                        <div class="col-sm-3">
                                                            <div class="form-group">
                                                                <label for="location_id">Representative Name </label>
                                                                <input placeholder="Please Enter Representative Name" type="text" class="form-control valid" name="represent_name[]" value="" id="represent_name"  />
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <div class="form-group">
                                                                <label for="area_id">Representative Email </label>
                                                                <input placeholder="Please Enter Representative email" type="text" class="form-control valid" name="represent_email[]" value="" id="represent_email"  />
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <div class="form-group">
                                                                <label for="area_id">Representative Phone </label>
                                                                <input placeholder="Please Enter Representative Phone" type="text" class="form-control valid" name="represent_mobile[]" value="" id="represent_mobile"  />
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <div class="form-group">
                                                                <button onclick="removeFieldR(`+i+`)" style="border-radius: 10px; margin-top: 29px;" class="btn btn-danger valid align-baseline"><i class="fa fa-minus"></i> </button>
                                                            </div>
                                                        </div>
                                                    </div>`
            $('#representative').append(html_input)
        }
        function removeFieldR(id) {

            $('#r'+id).remove();
        }
    </script>@endsection
