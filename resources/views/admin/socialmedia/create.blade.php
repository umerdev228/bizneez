@extends('admin.layouts.app')
@section('content')


<style>
  .fa-custom{  font-size: 200px!important;}
  .logo{padding-top: 24px;}
</style>

<script !src="">
  let currentPage = 1;
  let currentId = 1;
  let selectedElement = 0;
  let imagesData = '{!! json_encode($images) !!}';

  let public_path = '{{asset('/')}}';
  let selectedId = '';
  let selectedInputId = '';
  let lastSelectedImage = '';
  function selectImage(id, inputId) {
    selectedId = id
    selectedInputId = inputId
    $('#imageGallery').modal('toggle')
  }

  function selectImages(id) {
    if (lastSelectedImage !== '') {
      $('#'+lastSelectedImage).css('border', '')
    }
    let image = JSON.parse(imagesData).find(image => image.id === JSON.parse(id))
    $('#image').val(image.path)
    console.log(image.path)
    $('#'+id).css('border', '2px solid green')
    $('#'+selectedId).attr('src', public_path+image.path);
    $('#'+selectedInputId).val(id)
    lastSelectedImage = id
  }
</script>



  <script src="https://cdn.tiny.cloud/1/j09y3dnf5jy8bx6cnr24ferb8jc1zin3krnczqnvi0h1eopk/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
   <script> 
      tinymce.init({
  selector: '#detail_socil_media',
  height: 200,
  width:900,
  menubar: false,
  plugins: [
    'advlist autolink lists link image charmap print preview anchor',
    'searchreplace visualblocks code fullscreen',
    'insertdatetime media table paste code help wordcount'
  ],
  toolbar: 'undo redo | formatselect | ' +
  'bold italic backcolor | alignleft aligncenter ' +
  'alignright alignjustify | bullist numlist outdent indent | ' +
  'removeformat | help',
  content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
});

    </script>

 
  @include('admin.layouts.topbar')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          @if(session()->has('message'))
            <div class="alert alert-success mt-5" id="myElem" >
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              {{ session()->get('message') }}
            </div>
          @endif

          <br><br><br><br>
            <div class="form-group">
              <a class="btn btn-info" href="{{route('social_media.index')}}">View All Social Media</a>
            </div>
            <br>
            <form method="post" action="{{route('social.media.store')}}">
              @csrf
              <div class="col-lg-12">


                <div class="form-group">
                  <input type="hidden" name="image" id="image">
                  <img width="350px" class="pb-2" id="image-1-1" onclick="selectImage('image-1-1', 'imageValue-1-1')" src="{{asset('assets/images/img-icon.png')}}">
            <!--       <input style="width: 350px; padding: 2px 0 0 0;" type="text" name="detail" class="form-control p-2" id="exampleFormControlInput1" placeholder="Please Enter Text Here"> -->

 <textarea id="detail_socil_media"  ></textarea>


                  <div class="row">
                    <div class="col-md-6">
                      <div id="datetimepicker1" class="input-append date">
                        <input  style="width: 175px; padding: 2px;" name="date" data-format="dd/MM/yyyy hh:mm:ss" type="date" />

                     


                        <input style="width: 170px; padding: 2px 2px 0 0;" name="time" data-format="hh:mm:ss" type="time" />
                        <span class="add-on">
                          <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                          </i>
                        </span>
                      </div>
                    </div>
                  </div>

                </div>
                <div class="form-group">
                  <button name="button" value="submit" type="submit" class="btn btn-primary mb-2">SAVE</button>
                  <button name="button" value="preview" type="submit" class="btn btn-primary mb-2">Preview</button>
                </div>
              </div>
            </form>

        </div>
      </div>
    </div>
  </div>
  @include('admin.catalogue.includes.image-gallery')

@endsection
