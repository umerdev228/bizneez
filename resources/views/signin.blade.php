<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Bizneez.net</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="Freedom - Responsive One Page HTML Template">
    <meta name="author" content="RB Web Design">
    <!-- STYLES -->
    <link href="{{asset('assets/login/assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/login/assets/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/login/assets/css/supersized.css')}}" rel="stylesheet">
    <link href="{{asset('assets/login/assets/css/animate.css')}}" rel="stylesheet">
    <link href="{{asset('assets/login/assets/css/owl.carousel.css')}}" rel="stylesheet">
    <link href="{{asset('assets/login/assets/css/owl.theme.cs')}}s" rel="stylesheet">
    <link href="{{asset('assets/login/assets/css/style.css')}}" rel="stylesheet">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- GOOGLE FONTS -->
    <link href='//fonts.googleapis.com/css?family=Open+Sans:400italic,300,400,500,600700' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Montserrat%3A400%2C700&#038;ver=4.4.2' rel='stylesheet' type='text/css'>

    <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

    @toastr_css

   
 <script>
 (function () {
   var e,i=["https://fastbase.com/fscript.js","5fvHK0txe7","script"],a=document,s=a.createElement(i[2]);
   s.async=!0,s.id=i[1],s.src=i[0],(e=a.getElementsByTagName(i[2])[0]).parentNode.insertBefore(s,e)
 })();
</script>
</head>
<body id="top">
@php
    $sidebar_color = \App\SiteSetting::where('key', 'side_bar_color')->first()->data;
    $header_logo = \App\SiteSetting::where('key', 'header_logo')->first()->data;
@endphp
@include('layout.header')
<!-- MAIN TOP -->
        

<div id="main-top" class="main-top" style="background-image: url('{{asset('assets/images/banner1.jpeg')}}');margin-top: -20px;">
    <div class="main-content">
        <div class="container content">
            <div class="row text-center">
                <p align="center" style="font-size: 58px;">
                    Designed to keep you selling
                </p>
                <a type="button" data-toggle="modal" data-target="#liveModal"
                   class="btn btn-primary btn-lg">Live Demo</a>
                <a href="#" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#appointment-modal">Book Appointment</a>
            </div>
            <div class="row text-center">
                <a href="#about" class="scroll-down"></a>
            </div>
        </div>
    </div>
</div>
<!-- MAIN CONTENT -->
<div id="main-content" class="main-content">
    <!-- ABOUT -->
    <div id="about" class="section padding padding-top">
        <div class="section-content">
            <div class="row text-center">
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <img src="https://cdn4.iconfinder.com/data/icons/flat-pro-business-set-4/32/quote-512.png" style="    width: 53px;" />
                    <h2>Automated Quotation</h2>
                    <p class="hidden-xs">Streamlining your quotes with accuracy and speed, Improve your efficiency and improve the way you work.</p>
                </div>
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <i class="fa fa-3x fa-windows crm_icon"></i>
                    <h2>CRM/CMS System</h2>
                    <p class="hidden-xs">Appointment scheduler, messenger, Video Calling, notes & more. Be ahead and on time, Keeping you ahead in business.</p>
                </div>
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <img src="https://th.bing.com/th/id/R53d8523c0af03f12c7e3884b2670a381?rik=IphOlHXZsTTqhQ&riu=http%3a%2f%2fchepri.com%2fwp-content%2fuploads%2f2015%2f11%2fmobile-ux-graphic.png&ehk=zTaLf2jAYbN3l%2fcZDHuy1doMw3VBBGI%2blwh8xIqP8fI%3d&risl=&pid=ImgRaw" style="    width: 53px;" >
                    <h2>App Development</h2>
                    <p class="hidden-xs">Applications that enhance your company profile as well as improving your customer service with a zero fault system. Go ahead and reach for the stars.
 </p>
                </div>
            </div>
        </div>
    </div>
    <div class="section about padding-top">
        <div class="row text-left">
            <div class="col-md-6 col-sm-12 col-xs-12 image" style="background-image: url('{{asset('assets/images/left.jpeg')}}'); background-repeat: no-repeat;">
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12 background">
                <div class="content">
                    <h3>WELCOME TO BIZNEEZ.net</h3>
                    <p>Our co-founders set out to build a customer relationship management (CRM) application that helps users visualize their sales processes and administrative processing. They knew from experience that in sales, as in life, you can’t control your results, but you can control your actions. Our knowledge, as well as experience, will place you and your company where it needs to be from now and in the future. Our application is far more superior than the average system, we believe that having the right tools will make you a leading force in tomorrow's world but also far more knowledgeable in the way you work. As technology is always evolving so does our system, keeping you and your company moving forward and beyond.</p>
                    <a href="#team" class="btn btn-primary">EXPLORE</a>
                            
                <!--    <div class="row explore">
                                <div class="col-md-2 "><img src="{{asset('assets/images/explore.png')}}" class="img-responsive"></div>
                                <div class="col-md-6 align-left align-middle" align="left"><p class="align-middle">Remove working, we have covered</p></div>
                            </div> -->
                </div>
            </div>
        </div>
    </div>
    <!-- TEAM -->
    <!--     <div id="team" class="section padding-top">
                <div class="container">
                    <div class="section-heading">
                        <h2>TEAM</h2>
                        <p>Who We Are</p>
                    </div>
                    <div class="section-content">
                        <div class="row text-center">
                            <div class="col-md-4 col-sm-12 col-xs-12 text-center">
                                <div class="team wow fadeInLeft" data-wow-duration="1s">
                                    <img src="http://placehold.it/600x800" alt="" />
                                    <div class="mask">
                                        <h2>Samanta L.</h2>
                                        <p>Web Designer</p>
                                        <a href="#" class="info"><i class="fa fa-facebook"></i></a>
                                        <a href="#" class="info"><i class="fa fa-twitter"></i></a>
                                        <a href="#" class="info"><i class="fa fa-google-plus"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12 text-center">
                                <div class="team wow fadeInDown" data-wow-duration="1s">
                                    <img src="http://placehold.it/600x800" alt="" />
                                    <div class="mask">
                                        <h2>Pamela K.</h2>
                                        <p>Web Designer</p>
                                        <a href="#" class="info"><i class="fa fa-facebook"></i></a>
                                        <a href="#" class="info"><i class="fa fa-twitter"></i></a>
                                        <a href="#" class="info"><i class="fa fa-google-plus"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12 text-center">
                                <div class="team wow fadeInRight" data-wow-duration="1s">
                                    <img src="http://placehold.it/600x800" alt="" />
                                    <div class="mask">
                                        <h2>Micheal J.</h2>
                                        <p>Web Developer</p>
                                        <a href="#" class="info"><i class="fa fa-facebook"></i></a>
                                        <a href="#" class="info"><i class="fa fa-twitter"></i></a>
                                        <a href="#" class="info"><i class="fa fa-google-plus"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            -->
    <!-- SERVICES -->
    <div id="services" class="section services padding-top padding-bottom">
        <div class="container">
            <div class="section-heading">
                <h2>BIZNEEZ.net SERVICES</h2>
                <p>We are experts in technological applications</p>
            </div>
            <div class="section-content">
                <p>
Our service is bespoke to the client, whether you require a bespoke platform or standard issued system we are here to help. We have over 25 years of experience in programming and technology, knowledge in the workplace along with a vast array of processing information is key to a work perfect environment. Integrating your old tireless working life, into a super-efficient future, in turn, bringing you all the tools needed to collaborate your everyday business into a super-efficient web-based application saving you time and money and improving your way of business, which we are sure to give you an experience like no other.</p>
                <div class="row text-center">
                    <div class="fact col-xs-12 col-sm-12 col-md-3 col-lg-3 wow fadeInLeft" data-wow-duration="1s">
                        <!-- <i class="fa fa-3x fa-superpowers"></i> -->
                        <img src="{{asset('assets/images/4.png')}}" style="width: 50px; padding-bottom: 10px;   "  >
                        <h4>Automated Purchasing</h4>
                        <p>Streamline your purchasing system instantly, just don’t tell the admin staff
 </p>
                    </div>
                    <div class="fact col-xs-12 col-sm-12  col-md-3 col-lg-3 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.25s">
                        <!--  <i class="fa fa-3x fa-code"></i> -->
                        <img src="{{asset('assets/images/12.png')}}" style="width: 50px; padding-bottom: 14px;   "  >
                        <h4>Automated Invoice</h4>
                        <p>Automate your invoicing system with a click of a button, no more need for paperwork</p>
                    </div>
                    <div class="fact col-xs-12 col-sm-12  col-md-3 col-lg-3 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.5s">
                        <!-- <i class="fa fa-3x fa-users"></i> -->
                        <img src="https://cdn2.iconfinder.com/data/icons/business-management-50/64/x-08-128.png" style="width: 72px;margin-bottom: -10px;">
                        <h4>Client collaboration</h4>
                        <p>Liaise with clients with ease, super productive ways of collaboration, Impress your customers</p>
                    </div>
                    <div class="fact col-xs-12 col-sm-12  col-md-3 col-lg-3 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.75s">
                        <!-- <i class="fa fa-3x fa-cogs"></i> -->
                        <img src="{{asset('assets/images/ar.png')}}" style="width: 50px; padding-bottom: 13px;   "  >
                        <h4>Augmented reality ( AR)</h4>
                        <p>Visualization is a thing of the past see products as a reality with our direct information source </p>
                    </div>
                </div>
                <div class="row text-center second">
                    <div class="fact col-xs-12 col-sm-12  col-md-3 col-lg-3 wow fadeInRight" data-wow-duration="1s">
                        <!--  <i class="fa fa-3x fa-podcast"></i> -->
                        <img src="{{asset('assets/images/ai.png')}}" style="width: 62px; padding-bottom: 6px; "  >
                        <h4>Artificial Intelligence</h4>
                        <p>Improve your workload and save on time and money, application learning is a must in today’s world</p>
                    </div>
                    <div class="fact col-xs-12 col-sm-12  col-md-3 col-lg-3 wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.25s">
                        <!--  <i class="fa fa-3x fa-window-restore" style="padding-bottom: 6px;"></i> -->
                        <img src="{{asset('assets/images/rfid.png')}}" style="width: 62px; padding-bottom: 6px; "  >
                        <h4>RFID</h4>
                        <p>Scan all your inventory and stock items with this quick efficient RFID Technology. You’ll never be lost</p>
                    </div>
                    <div class="fact col-xs-12 col-sm-12  col-md-3 col-lg-3 wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.5s">
                        <img src="{{asset('assets/images/emp.png')}}" style="width: 98px;  "  >
                        <h4>Employee Management</h4>
                        <p>Control your employee infrastructure, team building, management control, working together</p>
                    </div>
                    <div class="fact col-xs-12 col-sm-12  col-md-3 col-lg-3 wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.75s">
                        <!-- <i class="fa fa-3x fa-line-chart"></i>  -->
                        <img src="{{asset('assets/images/marketing-advertising.png')}}" style="width: 62px; padding-bottom: 6px; "  >
                        <h4>Advertising & Marketing</h4>
                        <p>No more hustle and bustle advertising where you need and direct source marketing</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section about">
        <div class="row text-left">
                    
            <div class="col-md-6 col-sm-12 col-xs-12 background" style=" background: #414241;">
                <div class="content">
                    <h3 style="color: #fff"><b>SMART FINANCE FOR YOUR TECHNOLOGY</b></h3>
                    <p>You don’t have to use cash or bank facilities to fund your technology project! Instead of tying up large amounts of capital, a smarter alternative could be to use a leased facility. Leasing is a tax-efficient and cost-effective method of achieving your ideal equipment whilst retaining capital in your business.

                    <ul style="color: #fff"><li style="padding-bottom: 5px;">Tax advantages if you lease your technology project, the repayments are 100% allowable against taxable profit.</li> <br>
                        <li>Maximize cash flow - no large upfront payment Cash is the lifeblood of any business. Leasing means no large up-front payment so you keep the cash in your business, preserving liquidity and freeing up valuable working capital for use elsewhere.</li></ul></p>
                    <!-- <a href="#team" class="btn btn-primary">EXPLORE</a> -->
                                
                                
                </div>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12 image" style="background-image: url('{{asset('assets/images/section2.jpg')}}'); background-repeat: no-repeat;">
                            
            </div>
        </div>
    </div>
    <div id="services" class="section services padding-top padding-bottom">
        <div class="container">
            <div class="row text-center">
                <div class="fact col-xs-12  col-sm-12  col-md-3 col-lg-3 wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.25s">
                    <!--  <i class="fa fa-3x fa-object-ungroup"></i> -->
                    <img src="{{asset('assets/images/share-screen.png')}}" style="width: 62px; padding-bottom: 1px; "  >
                    <h4>Share screen/ Live Project</h4>
                    <p>Share your works, be a part of  all one system for the work together play together, works for all</p>
                </div>
                <div class="fact col-xs-12  col-sm-12  col-md-3 col-lg-3 wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.5s">
                    <!-- <i class="fa fa-3x fa-comment-o"></i> -->
                    <img src="{{asset('assets/images/lms.png')}}" style="width: 62px; padding-bottom: 1px; "  >
                    <h4>Lead Management System</h4>
                    <p>Lead Management system, see the benefits of a no loss business opportunity
</p>
                </div>
                <div class="fact col-xs-12  col-sm-12  col-md-3 col-lg-3 wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.75s">
                    <!-- <i class="fa fa-3x fa-users"></i> -->
                    <img src="{{asset('assets/images/arch.png')}}" style="width: 62px; padding-bottom: 1px; "  >
                    <h4>Architects Management</h4>
                    <p>Show your works to the world, impress your clients with a system that sings and dances to the tune of tomorrow </p>
                </div>
                <div class="fact col-xs-12  col-sm-12  col-md-3 col-lg-3 wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.75s">
                    <!--  <i class="fa fa-3x fa-life-ring"></i> -->
                    <img src="{{asset('assets/images/search-engine.png')}}" style="width: 70px; padding-bottom: 3px; "  >
                    <h4>Search Engine</h4>
                    <p>No more SEO!, direct Bizneez.com search engine integration the only search engine you will need</p>
                </div>
            </div>
            <div class="row text-center second">
                <div class="fact col-xs-12  col-sm-12  col-md-3 col-lg-3 wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.75s">
                    <img src="{{asset('assets/images/global.png')}}" style="width: 70px; padding-bottom: 3px; "  >
                    <h4>Global Infrastructure</h4>
                    <p>Be ahead of the game, this system is built to take you globally, bring it on!
 </p>
                </div>
                <div class="fact col-xs-12  col-sm-12  col-md-3 col-lg-3 wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.75s">
                    <img src="{{asset('assets/images/multi.png')}}" style="width: 86px; padding-bottom: 13px; "  >
                    <h4>Multi-Company System</h4>
                    <p>A must tool for any entrepreneur, this application will operate your multi-company portfolio</p>
                </div>
                <div class="fact col-xs-12  col-sm-12  col-md-3 col-lg-3 wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.75s">
                    <img src="{{asset('assets/images/rec.png')}}" style="width: 64px;  padding-bottom: 7px; "  >
                    <h4>Recruitment</h4>
                    <p>A feature of recruiting in house, more suitable for the best of recruiters, keeps you clean and productive</p>
                </div>
                            
                <div class="fact col-xs-12  col-sm-12  col-md-3 col-lg-3 wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.75s">
                    <img src="{{asset('assets/images/p.png')}}" style="width: 90px;  "  >
                    <h4>New Technology</h4>
                    <p>Ever-evolving technology that’s not too expensive and always on time, be ahead of the game</p>
                </div>
            </div>
                        
        </div>
    </div>
                
                
    <div id="info" class="section dark hidden-xs parallax-container" data-parallax="scroll" style="background-image: url('{{asset('assets/images/1.png')}}'); "  >
        <div class="section-content">
            <div class="row text-center partner-table">
                <div class="partner col-xs-3 col-md-3 col-lg-3">
                    <h2>125+</h2>
                    <p>Clients</p>
                </div>
                <div class="partner col-xs-3 col-md-3 col-lg-3">
                    <h2>1150+</h2>
                    <p>Manufacturers</p>
                </div>
                <div class="partner col-xs-3 col-md-3 col-lg-3">
                    <h2>11650+</h2>
                    <p>Product Ranges</p>
                </div>
                <div class="partner col-xs-3 col-md-3 col-lg-3">
                    <h2>200+</h2>
                    <p>Employee</p>
                </div>
            </div>
        </div>
    </div>
    <!-- PORTFOLIO -->
    <div id="portfolio" class="section portfolio">
        <div class="section-content">
            <div class="work-item-wrapper">
                <div class="row work-item-list">
                    <div class="col-md-4 col-sm-6 col-xs-12 work-item">
                        <img src="{{asset('assets/images/work6.jpg')}}" alt="New Advanced CRM System">
                        <div class="image-overlay">
                            <a  class="media-popup" title="New Advanced CRM System" >
                                <div class="work-item-info">
                                    <h3>New Advanced CRM System</h3>
                                    <p>World most famous and reliable application that meet the customer requirements in an ownable way</p>
                                    <i class="fa fa-search-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12 work-item">
                        <img src="{{asset('assets/images/work22.jpeg')}}" alt="Fast Collaboration">
                        <div class="image-overlay">
                            <a href="{{asset('assets/login/assets/img/portfolio/work2-ori.jpg')}}" class="media-popup" title="Faster Collaboration">
                                <div class="work-item-info">
                                    <h3>Faster Collaboration</h3>
                                    <p>Bizneez.net has capabilities of quick communication between users through live chat or video calling or mobile app call</p>
                                    <i class="fa fa-search-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12 work-item">
                        <img src="{{asset('assets/images/work5.jpg')}}" alt="Unlimited Ranges of Products">
                        <div class="image-overlay">
                            <a href="{{asset('assets/login/assets/img/portfolio/work3-ori.jpg')}}" class="media-popup" title="Unlimited Ranges of Products">
                                <div class="work-item-info">
                                                
                                    <h3>Unlimited Ranges of Products</h3>
                                    <p>Bizneez.net has the capabilities, to manage unlimited ranges of products via an import excel sheet or a build your own option tool.</p>
                                    <i class="fa fa-search-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12 work-item">
                        <img src="{{asset('assets/images/work2.jpeg')}}" alt="Technologies">
                        <div class="image-overlay">
                            <a href="{{asset('assets/login/assets/img/portfolio/work4-ori.jpg')}}" class="media-popup" title="Technologies">
                                <div class="work-item-info">
                                    <h3>Technologies</h3>
                                    <p>We have the latest technology of system to fulfill the requirements of customers through a mobile app, live chat, portal view, and Artificial Intelligence</p>
                                    <i class="fa fa-search-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12 work-item">
                        <img src="{{asset('assets/images/6.jpg')}}" alt="Advertising & Marketing">
                        <div class="image-overlay">
                            <a href="{{asset('assets/login/assets/img/portfolio/work6.jpg')}}" class="media-popup" title="Advertising & Marketing">
                                <div class="work-item-info">
                                    <h3>Advertising & Marketing</h3>
                                    <p>Bizneez.net portal has strong features of advertising & marketing of company quickly and perfectly </p>
                                    <i class="fa fa-search-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12 work-item">
                        <img src="{{asset('assets/images/work1.jpeg')}}" alt="Automated Quoting System">
                        <div class="image-overlay">
                            <a href="{{asset('assets/login/assets/img/portfolio/work5-ori.jpg')}}" class="media-popup" title="Automated Quoting System">
                                <div class="work-item-info">
                                    <h3>Automated Quoting System</h3>
                                    <p>Streamlining your quotes with accuracy and speed, Improve your efficiency and improve the way you work.</p>
                                                
                                    <i class="fa fa-search-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                                
                </div>
            </div>
        </div>
    </div>
    <div id="purchase" class="section dark">
        <div class="section-content">
            <div class="container partner-table">
                <div class="col-md-12 col-sm-12 col-xs-12 text-center partner">
                    <a href="#" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#appointment-modal">Book Appointment</a>
                </div>
            </div>
        </div>
    </div>
    <!-- NEWSLETTER -->
    <div class="section newsletter">
        <div class="section-content">
            <div class="container">
                <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                    <h3 class="wow fadeInDown" data-wow-duration="1s">Subscribe To Our Newsletter</h3>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                    <input type="text" class="wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.5s" name="newsletter" placeholder="Enter your email..." />
                    <br />
                    <button type="submit" class="btn btn-primary btn-lg wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s" name="send">Subscribe</button>
                </div>
            </div>
        </div>
    </div>
    <!-- CLIENTS -->
    <div id="clients" class="section">
        <div class="section-content">
            <div class="container">
                <div id="owl-clients" class="owl-carousel">
                    <div>
                        <a href=""><img src="{{asset('assets/login/assets/img/clients/logo-client1.png')}}" alt="" /></a>
                    </div>
                    <div>
                        <a href=""><img src="{{asset('assets/login/assets/img/clients/logo-client2.png')}}" alt="" /></a>
                    </div>
                    <div>
                        <a href=""><img src="{{asset('assets/login/assets/img/clients/logo-client3.png')}}" alt="" /></a>
                    </div>
                    <div>
                        <a href=""><img src="{{asset('assets/login/assets/img/clients/logo-client4.png')}}" alt="" /></a>
                    </div>
                    <div>
                        <a href=""><img src="{{asset('assets/login/assets/img/clients/logo-client5.png')}}" alt="" /></a>
                    </div>
                    <div>
                        <a href=""><img src="{{asset('assets/login/assets/img/clients/logo-client6.png')}}" alt="" /></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section section6" style="background-image: url('{{asset('assets/images/bg-testimonial.jpeg')}}'); height: 172px">
        <div class="row">
            <div class="col-md-4 col-sm-12 col-xs-12 image" align="right" ><!-- <img class="img-responsive" src="https://www.nasa.gov/sites/default/files/thumbnails/image/switchbacks-sun-web.gif"> -->
                <div class="content" style="    margin-left: 103px; margin-top: -73px;">
                    <h5>Coming Soon!</h5>
                               
                </div>

            </div>
            <div class="col-md-4 col-sm-12 col-xs-12" >
                <div class="content" style="    margin-left: 103px; margin-top: -77px;">
                    <h5>Bizneez.com<p class="sub1" style="margin-top: -20px;">The search engine for your business</p></h5>
                               
                </div>
            </div>
            <div class="col-md-4">
                <div style=" margin-top: -77px;">  <h5><span style="font-size: 20px;">adverts@bizneez.net</span></h5></div>
            </div>
        </div></div>
    <!-- TESTIMONIALS -->
<!-- <div id="testimonials" class="section testimonials parallax-container" data-parallax="scroll" data-image-src="{{asset('assets/images/1.png')}}" data-natural-width="1200" data-natural-height="1200">
                        <div class="section-heading">
                            <h2>TESTIMONIALS</h2>
                            <p>What Customers Say About Us</p>
                        </div>
                        <div class="section-content">
                            <div class="container">
                                <div id="owl-testimonials" class="owl-carousel">
                                    <div>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquid ex ea commodi consequat. Quis aute iure reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                                        <p class="mini">- John K. -</p>
                                    </div>
                                    <div>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua.</p>
                                        <p class="mini">- Jim P. -</p>
                                    </div>
                                    <div>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquid ex ea commodi consequat.</p>
                                        <p class="mini">- Morgan F. -</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> -->
    <!-- CONTACT -->
    <div id="contact" class="section contact padding-top">
        <div class="section-heading">
            <h2>CONTACT</h2>
            <p>Contact Us</p>
        </div>
        <div class="section-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="contact-info">
                            <ul class="list-unstyled">
                                <li>
                                    <h2><i class="fa fa-home"></i> ADDRESS</h2>
                                    <p>Kemp House, 160 City Road, London, United Kingdom, EC1V 2NX</p>
                                </li>
                                <li>
                                    <h2><i class="fa fa-phone"></i> PHONE</h2>
                                    <p>+44(0)20 8524 5294</p>
                                </li>
                                <li>
                                    <h2><i class="fa fa-envelope"></i> EMAIL</h2>
                                    <p><a href="mailto:info@myemail.com">info@bizneez.net</a></p>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <div class="contact-form">
                            <div class="alert"></div>
                            <form id="contact-form" action="{{route('contact.store')}}" method="post">
                                @csrf
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <label class="sr-only" for="name">Name:</label>
                                        <input type="text" id="name" name="name" placeholder="Name *" required>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <label class="sr-only" for="email">Email:</label>
                                        <input type="email" id="email" name="email" placeholder="Email *" required>
                                    </div>
                                </div>
                                <div class="row text-right">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <label class="sr-only" for="message">Message:</label>
                                        <textarea id="message" name="message" placeholder="Message *" required></textarea>
                                        <button type="submit" class="btn btn-primary btn-lg" name="send">SEND</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- SOCIAL -->
<div class="section social padding-top">
    <div class="section-content">
        <a href="">
            <div class="col-md-3 col-sm-3 col-xs-3 text-center facebook">
                <i class="fa fa-3x fa-facebook"></i>
            </div>
        </a>
        <a href="">
            <div class="col-md-3 col-sm-3 col-xs-3 text-center twitter">
                <i class="fa fa-3x fa-twitter"></i>
            </div>
        </a>
        <a href="">
            <div class="col-md-3 col-sm-3 col-xs-3 text-center google-plus">
                <i class="fa fa-3x fa-google-plus"></i>
            </div>
        </a>
        <a href="">
            <div class="col-md-3 col-sm-3 col-xs-3 text-center linkedin">
                <i class="fa fa-3x fa-linkedin"></i>
            </div>
        </a>
    </div>
</div>
<!-- FOOTER -->
<footer class="site-footer" style="padding-top:150px;">
    <div class="container">
        <div class="row"><br>
            <div class="col-sm-12 col-md-6">
                <img width="200" src="{{$header_logo}}" alt=""><br>
                <b>Together we work better for a brighter future</b>
                <p style="font-size: 11px;" class="text-justify">
                    Bizneez.net is a new start-up company that sees the missing pieces that can place you and your business where it needs to be. Our team has spent many years thinking and putting into play the research of our application. We have given many hours and tireless nights to reach a solution that can help your company achieve more. We believe that with the start of this application we can continue to evolve in this rapidly expensive technological future and bring you a cost-effective solution for both us and your business.<br><br>
                                    
                </p>
            </div>
            <div class="col-xs-6 col-md-3">
                <h6>Services</h6>
                <ul class="footer-links">
                    <li><a href="">Web Portal</a></li>
                    <li><a href="">Download VMC App</a></li>
                    <li><a href="">Download AR App</a></li>
                    <li><a href="">Download RIFD App</a></li>
                    <li><a href="">Get free Quote</a></li>
                    <li><a href="">Our Products</a></li>
                </ul>
            </div>
            <div class="col-xs-6 col-md-3">
                <h6>Quick Links</h6>
                <ul class="footer-links">
                    <li><a href="">About Us</a></li>
                    <li><a href="">Contact Us</a></li>
                    <li><a href="">Contribute</a></li>
                    <li><a href="">Privacy Policy</a></li>
                    <li><a href="">Sitemap</a></li>
                </ul>
            </div>
            <div class="col-xs-6 col-md-2">
                <!-- <h6>Quick Links</h6>
                                <ul class="footer-links">
                                    <li><a href="">About Us</a></li>
                                    <li><a href="">Contact Us</a></li>
                                    <li><a href="">Contribute</a></li>
                                    <li><a href="policy/">Privacy Policy</a></li>
                                    <li><a href="">Sitemap</a></li>
                                </ul> -->
            </div>
        </div>
    
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-6 col-xs-12">
                <p class="copyright-text">Copyright &copy; bizneez.net Limited 2021 All Rights Reserved by
                </p>
            </div>
            <!--  <div class="col-md-4 col-sm-6 col-xs-12">
                <ul class="social-icons">
                    <li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a class="dribbble" href="#"><i class="fa fa-dribbble"></i></a></li>
                    <li><a class="linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>
                </ul>
            </div> -->
        </div>
    </div>
</footer>
<div class="back-to-top">
    <a href="#top"><i class="fa fa-4x fa-angle-up"></i></a>
</div>
<!-- JAVASCRIPT -->
<script src="{{asset('assets/login/assets/js/jquery-2.1.0.min.js')}}"></script>
<script src="{{asset('assets/login/assets/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/login/assets/js/jquery.easing.min.js')}}"></script>
<script src="{{asset('assets/login/assets/js/supersized.3.2.7.min.js')}}"></script>
<script src="{{asset('assets/login/assets/js/jquery.magnific-popup.min.js')}}"></script>
<script src="{{asset('assets/login/assets/js/wow.js')}}"></script>
<script src="{{asset('assets/login/assets/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('assets/login/assets/js/parallax.min.js')}}"></script>
<script src="{{asset('assets/login/assets/js/jquery.localScroll.min.js')}}"></script>
<script src="{{asset('assets/login/assets/js/jquery.scrollTo.min.js')}}"></script>
<script src="{{asset('assets/login/assets/js/main.js')}}"></script>


@toastr_js
@toastr_render


<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            ...
        </div>
    </div>
</div>

@php

    $timeSlots = Array
    (
        '00:00',
        '03:00',
        '06:00',
        '09:00',
        '12:00',
        '15:00',
        '18:00',
        '21:00',
    )
@endphp

<!-- appointment modal -->
<div class="modal fade appointment-modal" id="appointment-modal" tabindex="-1" role="dialog" aria-labelledby="appointmentModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <form action="{{route('demo.appointment')}}" method="post">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="text-center">
                        <h1 class="modal-title" id="appointmentModalLabel">Appointment Booking</h1>
                    </div>
{{--                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
{{--                        <span aria-hidden="true">&times;</span>--}}
{{--                    </button>--}}
                </div>
                <div class="modal-body">
                    @csrf
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-lg-6 form-group">
                                <label for="name">Name*</label>
                                <input class="form-control" type="text" name="name" id="name" required="required">
                            </div>
                            <div class="col-lg-6 form-group">
                                <label for="company_name">Company Name</label>
                                <input class="form-control" type="text" name="company_name" id="company_name">
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-lg-6 form-group">
                                <label for="telephone">Telephone*</label>
                                <input class="form-control" type="text" name="telephone" id="telephone" required="required">
                            </div>
                            <div class="col-lg-6 form-group">
                                <label for="email">Email*</label>
                                <input class="form-control" type="email" name="email" id="email" required="required">
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-lg-6 form-group">
                                <label for="date">Appointment Date</label>
                                <input class="form-control" type="date" name="date" id="date">
                            </div>
                            <div class="col-lg-3 form-group">
                                <label for="appointment_time">Available Slots</label>
                                <!-- <select class="custom-select form-control"  name="appointment_time" id="appointment_time"> -->
                                   <!--  @foreach($timeSlots as $slot)
                                        <option style="border: 2px solid; background-color: #FF781E;" value="{{$slot}}">{{$slot}}</option>
                                    @endforeach -->
                                     <!-- <option value="07.00 AM">07.00 AM</option>
                                     <option value="08.00 AM">08.00 AM</option>
                                     <option value="09.00 AM">09.00 AM</option>
                                     <option value="10.00 AM">10.00 AM</option>
                                     <option value="11.00 AM">11.00 AM</option>
                                     <option value="12.00 PM">12.00 PM</option>
                                     <option value="01.00 PM">01.00 PM</option>
                                     <option value="02.00 PM">02.00 PM</option>
                                     <option value="03.00 PM">03.00 PM</option>
                                     <option value="04.00 PM">04.00 PM</option>
                                     <option value="05.00 PM">05.00 PM</option>
                                     <option value="06.00 PM">06.00 PM</option>
                                     <option value="07.00 PM">07.00 PM</option>
                                      
                                    
                                </select> -->

<div class="custom-select" style="width:200px;">
  <select name="appointment_time" id="appointment_time">
                                     <option value="07.00 AM">07.00 AM</option>
                                     <option value="08.00 AM">08.00 AM</option>
                                     <option value="09.00 AM">09.00 AM</option>
                                     <option value="10.00 AM">10.00 AM</option>
                                     <option value="11.00 AM">11.00 AM</option>
                                     <option value="12.00 PM">12.00 PM</option>
                                     <option value="01.00 PM">01.00 PM</option>
                                     <option value="02.00 PM">02.00 PM</option>
                                     <option value="03.00 PM">03.00 PM</option>
                                     <option value="04.00 PM">04.00 PM</option>
                                     <option value="05.00 PM">05.00 PM</option>
                                     <option value="06.00 PM">06.00 PM</option>
                                     <option value="07.00 PM">07.00 PM</option>
  </select>
</div>
 
                            </div>

                             <div class="col-lg-3 form-group">
                                <label for="appointment_time">No of Employees</label>
                                <input class="form-control" type="number" name="number_of_employees" id="number_of_employees">
                            </div>

                        </div>
                    </div>

                    <!-- <div class="col-lg-12">
                        <div class="row">
                            <div class="col-lg-6 form-group">
                                <label for="number_of_directors">Number Of Directors</label>
                                <input class="form-control" type="number" name="number_of_directors" id="number_of_directors">
                            </div>
                            <div class="col-lg-6 form-group">
                                <label for="number_of_employees">Number Of Employees</label>
                                <input class="form-control" type="number" name="number_of_employees" id="number_of_employees">
                            </div>
                        </div>
                    </div> -->

                    <div class="text-center"><br><br><p></p>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- appointment modal -->
<div class="modal fade" id="liveModal" tabindex="-1" role="dialog" aria-labelledby="liveModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{route('demo.login')}}" method="post">
                <div class="modal-header">
                    <!--  <h5 class="modal-title center" id="liveModalLabel">
                        Demo Login
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button> -->
                    <div align="center"> 
                        <h1 class="modal-title" id="appointmentModalLabel">Verification Code</h1>
                        <p>Please enter 6-digit Code</p>
                    </div>
                </div>
                <div class="modal-body" align="center">
                    @csrf
                    <fieldset name='number-code' data-number-code-form>
                        <!--  <legend>Enter Code</legend> -->
                        <input class="code p-0" type="text" maxlength="1" min='0' max='9' name='number_code_0' data-number-code-input='0' required />
                        <input class="code p-0" type="text" maxlength="1" min='0' max='9' name='number_code_1' data-number-code-input='1' required /> <span class="dash"> - </span>
                        <input class="code p-0" type="text" maxlength="1" min='0' max='9' name='number_code_2' data-number-code-input='2' required />
                        <input class="code p-0" type="text" maxlength="1" min='0' max='9' name='number_code_3' data-number-code-input='3' required /> <span class="dash"> - </span>
                        <input class="code p-0" type="text" maxlength="1" min='0' max='9' name='number_code_4' data-number-code-input='4' required />
                        <input class="code p-0" type="text" maxlength="1" min='0' max='9' name='number_code_5' data-number-code-input='5' required />
                    </fieldset>
                </div>
                <div class="modal-footer" align="center">
                    <div align="center"><button type="submit" class="btn btn-primary">Go Live</button></div>
                </div>
            </form>
        </div>
    </div>
</div>


<script>
    new WOW().init();
    $(document).ready(function() {
        $("#owl-clients").owlCarousel({
            slideSpeed : 500,
            paginationSpeed : 1000,
            singleItem: false,
            items: 4,
            //Autoplay
            autoPlay : 3000,
            stopOnHover : false
        });
        $("#owl-testimonials").owlCarousel({
            slideSpeed : 1000,
            paginationSpeed : 2000,
            singleItem: true,
            items: 1,
            //Autoplay
            autoPlay : 5000,
            stopOnHover : false
        });
    });
</script>

<script !src="">


    var x, i, j, l, ll, selElmnt, a, b, c;
/* Look for any elements with the class "custom-select": */
x = document.getElementsByClassName("custom-select");
l = x.length;
for (i = 1; i < l; i++) {
  selElmnt = x[i].getElementsByTagName("select")[0];
  ll = selElmnt.length;
  /* For each element, create a new DIV that will act as the selected item: */
  a = document.createElement("DIV");
  a.setAttribute("class", "select-selected");
  a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
  x[i].appendChild(a);
  /* For each element, create a new DIV that will contain the option list: */
  b = document.createElement("DIV");
  b.setAttribute("class", "select-items select-hide");
  for (j = 1; j < ll; j++) {
    /* For each option in the original select element,
    create a new DIV that will act as an option item: */
    c = document.createElement("DIV");
    c.innerHTML = selElmnt.options[j].innerHTML;
    c.addEventListener("click", function(e) {
        /* When an item is clicked, update the original select box,
        and the selected item: */
        var y, i, k, s, h, sl, yl;
        s = this.parentNode.parentNode.getElementsByTagName("select")[0];
        sl = s.length;
        h = this.parentNode.previousSibling;
        for (i = 0; i < sl; i++) {
          if (s.options[i].innerHTML == this.innerHTML) {
            s.selectedIndex = i;
            h.innerHTML = this.innerHTML;
            y = this.parentNode.getElementsByClassName("same-as-selected");
            yl = y.length;
            for (k = 0; k < yl; k++) {
              y[k].removeAttribute("class");
            }
            this.setAttribute("class", "same-as-selected");
            break;
          }
        }
        h.click();
    });
    b.appendChild(c);
  }
  x[i].appendChild(b);
  a.addEventListener("click", function(e) {
    /* When the select box is clicked, close any other select boxes,
    and open/close the current select box: */
    e.stopPropagation();
    closeAllSelect(this);
    this.nextSibling.classList.toggle("select-hide");
    this.classList.toggle("select-arrow-active");
  });
}

function closeAllSelect(elmnt) {
  /* A function that will close all select boxes in the document,
  except the current select box: */
  var x, y, i, xl, yl, arrNo = [];
  x = document.getElementsByClassName("select-items");
  y = document.getElementsByClassName("select-selected");
  xl = x.length;
  yl = y.length;
  for (i = 0; i < yl; i++) {
    if (elmnt == y[i]) {
      arrNo.push(i)
    } else {
      y[i].classList.remove("select-arrow-active");
    }
  }
  for (i = 0; i < xl; i++) {
    if (arrNo.indexOf(i)) {
      x[i].classList.add("select-hide");
    }
  }
}

/* If the user clicks anywhere outside the select box,
then close all select boxes: */
document.addEventListener("click", closeAllSelect);



    // Elements
    const numberCodeForm = document.querySelector('[data-number-code-form]');
    const numberCodeInputs = [...numberCodeForm.querySelectorAll('[data-number-code-input]')];

    // Event callbacks
    const handleInput = ({target}) => {
        if (!target.value.length) { return target.value = null; }

        const inputLength = target.value.length;
        let currentIndex = Number(target.dataset.numberCodeInput);

        if (inputLength > 1) {
            const inputValues = target.value.split('');

            inputValues.forEach((value, valueIndex) => {
                const nextValueIndex = currentIndex + valueIndex;

                if (nextValueIndex >= numberCodeInputs.length) { return; }

                numberCodeInputs[nextValueIndex].value = value;
            });

            currentIndex += inputValues.length - 2;
        }

        const nextIndex = currentIndex + 1;

        if(nextIndex < numberCodeInputs.length) {
            numberCodeInputs[nextIndex].focus();
        }
    }

    const handleKeyDown = e => {
        const {code, target} = e;

        const currentIndex = Number(target.dataset.numberCodeInput);
        const previousIndex = currentIndex - 1;
        const nextIndex = currentIndex + 1;

        const hasPreviousIndex = previousIndex >= 0;
        const hasNextIndex = nextIndex <= numberCodeInputs.length - 1

        switch(code) {
            case 'ArrowLeft':
            case 'ArrowUp':
                if (hasPreviousIndex) {
                    numberCodeInputs[previousIndex].focus();
                }
                e.preventDefault();
                break;

            case 'ArrowRight':
            case 'ArrowDown':
                if (hasNextIndex) {
                    numberCodeInputs[nextIndex].focus();
                }
                e.preventDefault();
                break;
            case 'Backspace':
                if (!e.target.value.length && hasPreviousIndex) {
                    numberCodeInputs[previousIndex].value = null;
                    numberCodeInputs[previousIndex].focus();
                }
                break;
            default:
                break;
        }
    }

    // Event listeners
    numberCodeForm.addEventListener('input', handleInput);
    numberCodeForm.addEventListener('keydown', handleKeyDown);



    var x, i, j, l, ll, selElmnt, a, b, c;
    /* Look for any elements with the class "custom-select": */
    x = document.getElementsByClassName("custom-select");
    l = x.length;
    for (i = 0; i < l; i++) {
        selElmnt = x[i].getElementsByTagName("select")[0];
        ll = selElmnt.length;
        /* For each element, create a new DIV that will act as the selected item: */
        a = document.createElement("DIV");
        a.setAttribute("class", "select-selected");
        a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
        x[i].appendChild(a);
        /* For each element, create a new DIV that will contain the option list: */
        b = document.createElement("DIV");
        b.setAttribute("class", "select-items select-hide");
        for (j = 1; j < ll; j++) {
            /* For each option in the original select element,
            create a new DIV that will act as an option item: */
            c = document.createElement("DIV");
            c.innerHTML = selElmnt.options[j].innerHTML;
            c.addEventListener("click", function(e) {
                /* When an item is clicked, update the original select box,
                and the selected item: */
                var y, i, k, s, h, sl, yl;
                s = this.parentNode.parentNode.getElementsByTagName("select")[0];
                sl = s.length;
                h = this.parentNode.previousSibling;
                for (i = 0; i < sl; i++) {
                    if (s.options[i].innerHTML == this.innerHTML) {
                        s.selectedIndex = i;
                        h.innerHTML = this.innerHTML;
                        y = this.parentNode.getElementsByClassName("same-as-selected");
                        yl = y.length;
                        for (k = 0; k < yl; k++) {
                            y[k].removeAttribute("class");
                        }
                        this.setAttribute("class", "same-as-selected");
                        break;
                    }
                }
                h.click();
            });
            b.appendChild(c);
        }
        x[i].appendChild(b);
        a.addEventListener("click", function(e) {
            /* When the select box is clicked, close any other select boxes,
            and open/close the current select box: */
            e.stopPropagation();
            closeAllSelect(this);
            this.nextSibling.classList.toggle("select-hide");
            this.classList.toggle("select-arrow-active");
        });
    }

    function closeAllSelect(elmnt) {
        /* A function that will close all select boxes in the document,
        except the current select box: */
        var x, y, i, xl, yl, arrNo = [];
        x = document.getElementsByClassName("select-items");
        y = document.getElementsByClassName("select-selected");
        xl = x.length;
        yl = y.length;
        for (i = 0; i < yl; i++) {
            if (elmnt == y[i]) {
                arrNo.push(i)
            } else {
                y[i].classList.remove("select-arrow-active");
            }
        }
        for (i = 0; i < xl; i++) {
            if (arrNo.indexOf(i)) {
                x[i].classList.add("select-hide");
            }
        }
    }

    /* If the user clicks anywhere outside the select box,
    then close all select boxes: */
    document.addEventListener("click", closeAllSelect);
</script>

</body>
</html>


<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-202551261-1">
</script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-202551261-1');
</script>
