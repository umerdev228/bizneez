@extends('admin.layouts.app')
@section('content')
    <link rel="stylesheet" type="text/css" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/themes/base/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css">

    <div class="content">
        <div class="container-fluid">
            <div style="margin-top: 50px;">
                    <div class="row pb-2 mb-5">
                        <div class="form-control">
                            <label for="file">Upload Image</label>
                            <input type="file" id="file" onchange="readURL(this);" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12" id="imgThumbnailPreview" style="height: 500px; background: #0ace96">
                            <img id="blah" src="" />
                        </div>
                    </div>
            </div>
        </div>

        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.js"></script>
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>

        <script !src="">
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#blah')
                            .attr('src', e.target.result)
                            .attr('width', '150px');
                    };
                    $('#blah').draggable();

                    reader.readAsDataURL(input.files[0]);
                }
            }
        </script>
@endsection