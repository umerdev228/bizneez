@extends('admin.layouts.app')
@section('content')
    @include('admin.layouts.topbar')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    @if(session()->has('message'))
                        <div class="alert alert-success mt-5" id="myElem" >
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    <br><br><br><br>
                    <div class="form-group">
{{--                        <a class="btn btn-info" href="{{route('social_media.index')}}">View All Social Media</a>--}}
                    </div>
                    <br>
                   
                        @csrf
                        <div class="col-lg-12">
                            <div class="form-group">
                                <input type="hidden" name="image" id="image">
                                <img width="350px" class="pb-2" id="image-1-1" onclick="selectImage('image-1-1', 'imageValue-1-1')" src="{{asset($media->image)}}">
                                <input disabled style="width: 350px; padding: 2px 0 0 0;" type="text" name="detail" value="{{$media->detail}}" class="form-control p-2" id="exampleFormControlInput1" placeholder="Please Enter Text Here">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div id="datetimepicker1" class="input-append date">
                                            <input disabled  style="width: 175px; padding: 2px;" name="date" value="{{$media->date}}" data-format="dd/MM/yyyy hh:mm:ss" type="date" />

                                            <input disabled style="width: 170px; padding: 2px 2px 0 0;" name="time" value="{{$media->time}}" data-format="hh:mm:ss" type="time" />
                                            <span class="add-on">
                                                    <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                                                    </i>
                                                </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
 <button name="button" type="button" onclick="location.href='{{ route('social_media') }}'" class="btn btn-primary mb-2">Back</button> 
                              <!--   <button name="button" value="preview" type="submit" class="btn btn-primary mb-2">Save Preview</button>  -->
                            </div>
                            </div>
                        </div>
                   

                </div>
            </div>
        </div>
    </div>

@endsection
<style>
    .fa-custom{  font-size: 200px!important;}
    .logo{padding-top: 24px;}
</style>

<script !src="">
    let currentPage = 1;
    let currentId = 1;
    let selectedElement = 0;
{{--    let imagesData = '{!! json_encode($images) !!}';--}}

    let public_path = '{{asset('/')}}';
    let selectedId = '';
    let selectedInputId = '';
    let lastSelectedImage = '';
    function selectImage(id, inputId) {
        selectedId = id
        selectedInputId = inputId
        $('#imageGallery').modal('toggle')
    }

    function selectImages(id) {
        if (lastSelectedImage !== '') {
            $('#'+lastSelectedImage).css('border', '')
        }
        let image = JSON.parse(imagesData).find(image => image.id === JSON.parse(id))
        $('#image').val(image.path)
        console.log(image.path)
        $('#'+id).css('border', '2px solid green')
        $('#'+selectedId).attr('src', public_path+image.path);
        $('#'+selectedInputId).val(id)
        lastSelectedImage = id
    }
</script>