<?php

namespace App\Http\Controllers;

use App\Profile;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class ArchitectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $architects = User::role('architect')->with('profile')->with('user')->get();
        $data= [
            'architects' => $architects,
            'breadcrumb' => 'Dashboard / Architects List',
            'url' => env('APP_ENV_LINK'),
        ];
        return view('admin.architect.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $breadcrumb = ucfirst(Auth::user()->roles[0]->name)." / Architecture / New";
        return view('admin.architect.create', compact('breadcrumb'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request['password'] = $password = Str::random(8);

        $validated = $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
        ]);

        $user = User::where('email', $request['email'])->first();
        if ($user == null) {
            User::create([
                "name" => $request['first_name'] . ' ' . $request['middle_name'] . ' ' . $request['last_name'],
                "first_name" => $request['first_name'],
                "middle_name" => $request['middle_name'],
                "last_name" => $request['last_name'],
                "email" => $request['email'],
                "created_by" => Auth::id(),
                "password" => Hash::make($request['password']),
            ]);
            $user = User::where('email', $request['email'])->first();
            $user->assignRole('architect');
            Profile::create([
                "user_id" => $user->id,
                "phone" => $request['phone'],
                "mobile" => $request['mobile'],
                "assistant_number" => $request['assistant_number'],
                "address" => $request['address'],
                "location_id" => $request['location_id'],
                "area_id" => $request['area_id'],
                "description" => $request['first_name'],
            ]);
            if ($request->has('image')) {
                $imageName = time().'.'.$request->image->extension();
                $request->image->move(public_path('/uploads/users/architects'), $imageName);
                Profile::where('user_id', $user->id)->update([
                    "image" => '/uploads/users/architects/' . $imageName,
                ]);
            }
//            Mail::to($user->email)->send(new \App\Mail\MerchantActivation($user->name,'', '', '', $password));
        }
        toastr()->success('Architect Created Successfully');
        return redirect()->route('admin.architects.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $breadcrumb = ucfirst(Auth::user()->roles[0]->name)." / Architect / Show";
        $id = (\request()->route()->parameter('id'));
        $user = User::with('roles')->where('id', $id)->with('profile')->first();
        $users = User::all();
        return view('admin.architect.show', compact('user', 'breadcrumb', 'users'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $breadcrumb = ucfirst(Auth::user()->roles[0]->name)." / Architecture / Edit";
        $users = User::all();
        $architect = User::role('architect')->where('id', $id)->with('profile')->first();
        return view('admin.architect.edit', compact('architect', 'breadcrumb', 'users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        User::where('id', $id)->update([
            "name" => $request['first_name'] . ' ' . $request['middle_name'] . ' ' . $request['last_name'],
            "first_name" => $request['first_name'],
            "middle_name" => $request['middle_name'],
            "last_name" => $request['last_name'],
            "email" => $request['email'],
            "created_by" => $request['created_by'],
        ]);
        Profile::where('user_id', $id)->update([
            "phone" => $request['phone'],
            "mobile" => $request['mobile'],
            "assistant_number" => $request['assistant_number'],
            "address" => $request['address'],
            "location_id" => $request['location_id'],
            "area_id" => $request['area_id'],
            "description" => $request['first_name'],

        ]);
        if ($request->has('image')) {
            $imageName = time().'.'.$request->image->extension();
            $request->image->move(public_path('/uploads/users/architects'), $imageName);
            Profile::where('user_id', $id)->update([
                "image" => '/uploads/users/architects/' . $imageName,
            ]);
        }
        if ($request->has('password')) {
            User::where('id', $id)->update([
                "password" => $request['password'],
            ]);
        }
        toastr()->success('Architect Updated Successfully');
        return redirect()->route('admin.architects.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        User::where('id', $id)->delete();
        Profile::where('user_id', $id)->delete();
        toastr()->success('Architect Deleted Successfully');
        return redirect()->route('admin.architects.index');
    }
}
