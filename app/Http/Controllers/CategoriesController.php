<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Categories;
use Illuminate\Support\Facades\Auth;

class CategoriesController extends Controller
{

    public function index()
    {
        $categories = Categories::all();
        $data = [
            'categories' => $categories, 'breadcrumb' => 'Dashboard / Categories List',
        ];
        return view('admin.categories')->with($data);
    }

    public function create()
    {
        $breadcrumb = ucfirst(Auth::user()->roles[0]->name)." / Category / New";
        return view('items.create', compact('breadcrumb'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'description' => 'required',
        ]);
  
        Item::create($request->all());
   
        return redirect()->route('items.index')
                        ->with('success','Item created successfully.');
    }
   



      /**
     * Display the specified resource.
     *
     * @param  \App\Item  $Item
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $breadcrumb = ucfirst(Auth::user()->roles[0]->name)." / Category / Show";
        $id = (\request()->route()->parameter('id'));
        $category = Categories::where('id', $id)->first();
        return view('admin.category.show', compact('category', 'breadcrumb'));
    }
   
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Item  $Item
     * @return \Illuminate\Http\Response
     */
    public function edit(Item $item)
    {
        return view('items.edit',compact('item'));
    }
  


}
