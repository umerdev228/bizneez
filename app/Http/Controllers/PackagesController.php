<?php

namespace App\Http\Controllers;

use App\Packages;
use Illuminate\Http\Request;
use Stripe;
use App\Packageoptions;
use App\PacakgeRelationOption;


class PackagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function __construct()
    {
          $this->middleware('auth');
    }
    

     public function index()
    {
       $packages = Packages::all();

       $data = ['packages' => $packages, 'breadcrumb' => 'Dashboard / packages',];
       return view('admin.packages')->with($data);
    }



  

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
       $option = Packageoptions::all();

       $data = ['title' => 'Add New Package', 'option' => $option ,'breadcrumb' => 'Dashboard / packages Add',];
      
       return view('admin.packages_add')->with($data); 


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
            
       if($request->chk == 0 ){
        return redirect('admin/packages_add')->with('error', 'Please select at least one option'); 
            
        }
        else
        {

            request()->validate([ 
                // 'nickname' => 'required|unique:packages', 
            'name' => 'required | unique:packages',
            'duration' => 'required', 
            'price' => 'required',
            'description' => 'required| max: 30',
            ]);    
        }
  //  dd(count($request->chk));
    /*    //information are store about the stripe 
        Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));-
        //Array me data store ho reh hai

        $plan = \Stripe\Plan::create(
[            'currency' => 'usd',
            'interval' => $request->duration,//'month', 
            'interval_count' =>$request->month_interval,
            'product' => env('STRIPE_PACKAGE_KEY'),
           // 'nickname' =>  $request->nickname,
            'amount' =>  $request->price * 100,
        ]);
         //return $plan;
*/
        // dd($request->all());
        if($request->duration=='month'){
            $total_days =   30;
        }
        else if($request->duration=='6month'){
            $total_days = 180;
        }
        else if($request->duration=='year'){
            $total_days = 360;
        }
        //for status
        // $package = Packages::get('name');
        // dd($package);
        $data_package = [
                 'name' => $request->name,
                 'package_duration' => $request->duration ,
                 'amount' =>$request->price, 
                 'description'=> $request->description,  
                 'days' => $total_days,
                 'status' => 1,
            ];

            // dd($data_package);
        $package = Packages::create($data_package);
        foreach($request->chk as $option){
            $data_package_option = [
                'package_id' => $package->id,
                'option_id'=> $option
            ];
            PacakgeRelationOption::create($data_package_option); 
        }
         
        //$msge = "Added successfuly";
        //return view('admin.packages_add')->with('message', 'Package created successfully'); 
         return redirect('admin/packages')->with('message', 'Package is created successfully.'); 

 
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Packages  $packages
     * @return \Illuminate\Http\Response
     */
    public function show(Packages $packages)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Packages  $packages
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
     
        //$packages = Packages::where('id',$id)->with('options')->get();//findOrFail($id);           
        $packages = Packages::findOrFail($id);           
        $option = Packageoptions::all();
        $data = ['title' => 'Update Package', 'option' => $option , 'packages'=> $packages, 'breadcrumb'=> 'Package Update'];
        return view('admin.packages_edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Packages  $packages
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
 
    {           
      
             request()->validate([  
            'name' => 'required', 
            'duration' => 'required', 
            'price' => 'required',
            'description' => '  required | max: 30', 
            'status'=> 'required',
            ]);
      
  // dd('zxcvghjk');
         if($request->chk && count($request->chk)>0){ 
 
         PacakgeRelationOption::where('package_id', $request->id)->delete();
         foreach($request->chk as $option){ 
            //PacakgeRelationOption::findOrFail($option)->delete();
            
                $data_package_option = [
                    'package_id' => $request->id,
                    'option_id'=> $option
                ];
                PacakgeRelationOption::create($data_package_option); 
            }
         } 
         else{
            return redirect('admin/packages/edit/'. $request->id)->with('error', 'Sorry!!! You are not selected packages options'); 
         }
                 
            
          $packages = Packages::findOrFail($request->id); 
          $packages->description = $request->description;
          $packages->name = $request->name;
          $packages->package_duration = $request->duration;
          $packages->amount = $request->price;
          $packages->status = $request->status;
        // dd($packages->status);
          $packages->save();

          return redirect('admin/packages')->with('message', 'Package is updated successfully.'); 
        }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Packages  $packages
     * @return \Illuminate\Http\Response
     */
    public function destroy(Packages $packages)
    {
        //
    }
}
