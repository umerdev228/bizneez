<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SocialMediaAdvertisementComment extends Model
{
    //
    protected $guarded = [];
}
