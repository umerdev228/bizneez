@extends('admin.layouts.app')
@section('content')
    @include('admin.layouts.topbar')
    <!-- Google Chart JS -->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <!-- Start Page content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if(session()->has('message'))
                            <div class="alert alert-success" style="width:803px ">
                                {{ session()->get('message') }}
                            </div>
                        @endif

                        <div class="row">
                            <div class="col-9 mt-5">
                                <form id="employee-form" method="POST" action="{{ route('admin.employees.store') }}" enctype="multipart/form-data">
                                    @csrf
                                    <ul class="nav nav-tabs">
                                        <li class="nav-item">
                                            <a href="#create-employee" data-toggle="tab" aria-expanded="false" class="nav-link active show">
                                                <i class="fi-torso mr-2"></i> Create Employee
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="#salary" data-toggle="tab" aria-expanded="true" class="nav-link">
                                                <i class="fi-gallary mr-2"></i>Salary
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="#analytics-perform" data-toggle="tab" aria-expanded="false" class="nav-link">
                                                <i class="fi-bookmark mr-2"></i>Analytics and Performance
                                            </a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane active show" id="create-employee">
                                            <div class="card">
                                                <div class="card-box">

                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="first_name">First Name<span class="text-danger">*</span></label>
                                                                <input type="text" class="form-control valid" name="first_name" id="first_name" required />
                                                                @if ($errors->has('first_name'))
                                                                    <span class="error">{{ $errors->first('first_name') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="middle_name">Middle Name<span class="text-danger"></span></label>
                                                                <input type="text" class="form-control valid" name="middle_name" id="middle_name"/>
                                                                @if ($errors->has('middle_name'))
                                                                    <span class="error">{{ $errors->first('middle_name') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="last_name">Last Name<span class="text-danger">*</span></label>
                                                                <input type="text" class="form-control valid" name="last_name" id="last_name" required />
                                                                @if ($errors->has('last_name'))
                                                                    <span class="error">{{ $errors->first('first_name') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="email">Email<span class="text-danger">*</span></label>
                                                                <input type="email" class="form-control valid" name="email" id="email" required />
                                                                @if ($errors->has('email'))
                                                                    <span class="error">{{ $errors->first('email') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="phone">Phone<span class="text-danger">*</span></label>
                                                                <input type="tel" class="form-control valid" name="phone" id="phone" required />
                                                                @if ($errors->has('phone'))
                                                                    <span class="error">{{ $errors->first('phone') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="mobile">Mobile<span class="text-danger">*</span></label>
                                                                <input type="number" class="form-control valid" name="mobile" id="mobile" required />
                                                                @if ($errors->has('mobile'))
                                                                    <span class="error">{{ $errors->first('mobile') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="assistant_number">Assistant Number<span class="text-danger">*</span></label>
                                                                <input type="number" class="form-control valid" name="assistant_number" id="assistant_number" required />
                                                                @if ($errors->has('assistant_number'))
                                                                    <span class="error">{{ $errors->first('assistant_number') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="address">Address<span class="text-danger">*</span></label>
                                                                <input type="text" class="form-control valid" name="address" id="address" required />
                                                                @if ($errors->has('address'))
                                                                    <span class="error">{{ $errors->first('address') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="location_id">Location</label>
                                                                <input placeholder="Please Enter Location" type="text" class="form-control valid" name="location_id" value="" id="location_id"  />
                                                                @if ($errors->has('location_id'))
                                                                    <span class="error">{{ $errors->first('location_id') }}</span>

                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="area_id">Job Description<span class="text-danger">*</span></label>
                                                                <select class="form-control valid" name="area_id" id="area_id" required>
                                                                    <option value="1">Managing director</option>
                                                                    <option value="2">Sales director</option>
                                                                    <option value="3">Regional manager</option>
                                                                    <option value="4">Sales manager</option>
                                                                    <option value="5">Team leader</option>
                                                                    <option value="6">Sales consultant</option>
                                                                </select>

                                                                @if ($errors->has('job_description'))
                                                                    <span class="error">{{ $errors->first('job_description') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="image">Company Logo<span class="text-danger">*</span></label>
                                                                <input type="file" class="form-control valid" name="image" id="image"/>
                                                                @if ($errors->has('image'))
                                                                    <span class="error">{{ $errors->first('image') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="area_id">Team Building</label>
                                                                <select class="form-control valid" name="area_id" id="area_id" required>
                                                                    <option value="1">1</option>
                                                                    <option value="2">2</option>
                                                                    <option value="3">3</option>
                                                                    <option value="4">4</option>
                                                                    <option value="5">5</option>
                                                                    <option value="6">6</option>
                                                                    <option value="7">7</option>
                                                                    <option value="8">8</option>
                                                                    <option value="9">9</option>
                                                                    <option value="10">10</option>

                                                                </select>

                                                                @if ($errors->has('job_description'))
                                                                    <span class="error">{{ $errors->first('job_description') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="salary">
                                            <div class="card">
                                                <div class="card-box">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="invoice_number">Invoice Number<span class="text-danger">*</span></label>
                                                                <input type="text" class="form-control valid" name="invoice_number" id="invoice_number" required />
                                                                @if ($errors->has('invoice_number'))
                                                                    <span class="error">{{ $errors->first('invoice_number') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="account_invoice">Account Invoices<span class="text-danger"></span></label>
                                                                <input type="text" class="form-control valid" name="account_invoice" id="account_invoice"/>
                                                                @if ($errors->has('account_invoice'))
                                                                    <span class="error">{{ $errors->first('account_invoice') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="description">Description<span class="text-danger">*</span></label>
                                                                <input type="text" class="form-control valid" name="description" id="description" required />
                                                                @if ($errors->has('description'))
                                                                    <span class="error">{{ $errors->first('description') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="price">Price<span class="text-danger">*</span></label>
                                                                <input type="text" class="form-control valid" name="price" id="price" required />
                                                                @if ($errors->has('price'))
                                                                    <span class="error">{{ $errors->first('price') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="phone">Phone<span class="text-danger">*</span></label>
                                                                <input type="tel" class="form-control valid" name="phone" id="phone" required />
                                                                @if ($errors->has('phone'))
                                                                    <span class="error">{{ $errors->first('phone') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="discount">Discount<span class="text-danger">*</span></label>
                                                                <input type="text" class="form-control valid" name="discount" id="discount" required />
                                                                @if ($errors->has('discount'))
                                                                    <span class="error">{{ $errors->first('discount') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="profit">Profit<span class="text-danger">*</span></label>
                                                                <input type="text" class="form-control valid" name="profit" id="profit" required />
                                                                @if ($errors->has('profit'))
                                                                    <span class="error">{{ $errors->first('profit') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="vat">VAT%<span class="text-danger">*</span></label>
                                                                <input type="text" class="form-control valid" name="vat" id="vat" required />
                                                                @if ($errors->has('vat'))
                                                                    <span class="error">{{ $errors->first('vat') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="total">Total</label>
                                                                <input type="text" class="form-control valid" name="total" value="" id="total"  />
                                                                @if ($errors->has('total'))
                                                                    <span class="error">{{ $errors->first('total') }}</span>

                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="commission">Commission</label>
                                                                <input type="text" class="form-control valid" name="commission" id="commission"/>
                                                                @if ($errors->has('commission'))
                                                                    <span class="error">{{ $errors->first('commission') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="quarter_bonuses">Quarter Bonuses </label>
                                                                <input type="text" class="form-control valid" name="quarter_bonuses" value="" id="quarter_bonuses"  />
                                                                @if ($errors->has('quarter_bonuses'))
                                                                    <span class="error">{{ $errors->first('quarter_bonuses') }}</span>

                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="your_bonus">Your Bonuses</label>
                                                                <input type="text" class="form-control valid" name="your_bonus" value="" id="your_bonus"  />
                                                                @if ($errors->has('your_bonus'))
                                                                    <span class="error">{{ $errors->first('your_bonus') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="total_salary">Total Salary</label>
                                                                <input type="text" class="form-control valid" name="total_salary" value="" id="total_salary"  />
                                                                @if ($errors->has('total_salary'))
                                                                    <span class="error">{{ $errors->first('total_salary') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <div class="form-group mt-3 float-right">
                                                                <button form="employee-form" class="btn btn-primary" type="submit" value="Save">Save</button>
                                                                <button onclick="history.back()" type="button" class="btn">Cancel</button>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="analytics-perform">
{{--                                            <h1>analytics-perform</h1>--}}
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                </div>
            </div>
        </div>

@endsection
