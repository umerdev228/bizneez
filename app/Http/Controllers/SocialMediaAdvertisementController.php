<?php

namespace App\Http\Controllers;

use App\Category;
use App\Media;
use App\SocialMediaAdvertisement;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;

class SocialMediaAdvertisementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $media = SocialMediaAdvertisement::all();
        $breadcrumb = 'Dashboard / Social Media / All';

        return view('admin.socialmedia.index', compact(  'breadcrumb', 'media'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $users = User::with('messages')->orderBy("created_at", "DESC")->get();
        $media = Media::where('name', 'gallery')->get();
        $roles = Role::with('users')->get();
        $categories = Category::all();
        $images = Media::where('name', 'gallery')->get();
        $breadcrumb = 'Dashboard / Social Media / Create';
        $products  = DB::table('mdd_price_list_csv')->get();
        return view('admin.socialmedia.create', compact('users', 'roles', 'breadcrumb', 'media' ,'products' , 'categories', 'images'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
        //
        $data = $request->all();

        $media = SocialMediaAdvertisement::create([
            'social_media' => 'facebook',
            'image' => $request->image,
            'date' => $request->date,
            'time' => $request->time,
            'detail' =>$request->detail,
        ]);

        if ($request->button === 'preview') {  
            toastr()->success('Post Save Successfully');

            return view('admin.socialmedia.show', compact('media'));
        }
        else {
            toastr()->success('Post Save Successfully');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SocialMediaAdvertisement  $socialMediaAdvertisement
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
 
        $media = SocialMediaAdvertisement::where('id', $id)->first();

        $breadcrumb = 'Dashboard / Social Media / Show';
        return view('admin.socialmedia.show', compact('media', 'breadcrumb'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SocialMediaAdvertisement  $socialMediaAdvertisement
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $media = SocialMediaAdvertisement::where('id', $id)->first();

//        $media = Media::where('name', 'gallery')->get();


        $images = Media::where('name', 'gallery')->get();
        $breadcrumb = 'Dashboard / Social Media / Edit';

        return view('admin.socialmedia.edit', compact('breadcrumb', 'images', 'media'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SocialMediaAdvertisement  $socialMediaAdvertisement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        SocialMediaAdvertisement::where('id', $id)->update([
            'social_media' => $request->social_media,
            'image' => $request->image,
            'date' => $request->date,
            'time' => $request->time,
            'detail' => $request->detail,
        ]);
        toastr()->success('Post Updated Successfully');
        return redirect()->route('social_media.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SocialMediaAdvertisement  $socialMediaAdvertisement
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        SocialMediaAdvertisement::where('id', $id)->delete();
        toastr()->success('Post Deleted Successfully');
        return redirect()->route('social_media.index');

    }

    public function post() {
        $media = SocialMediaAdvertisement::with('comments')->orderBy('id','DESC')->paginate(5);
        $breadcrumb = 'Dashboard / Social Media / Posts';

        return view('admin.social_media_post.index', compact(  'breadcrumb', 'media'));

    }

    public function like($id) {
        $post = SocialMediaAdvertisement::where('id', $id)->first();
        $likes = (integer)$post->likes + 1;
        SocialMediaAdvertisement::where('id', $id)->update([
            'likes' => $likes
        ]);
        toastr()->success('Post Liked');
        return redirect()->back();
    }
}
