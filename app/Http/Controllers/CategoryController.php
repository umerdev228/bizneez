<?php

namespace App\Http\Controllers;

use App\Categories;
use App\Category;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $categories = Category::all();
        $data= [
            'categories' => $categories,
            'breadcrumb' => 'Dashboard / Clients List',
            'url' => env('APP_ENV_LINK'),
        ];
        return view('admin.category.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categories = Category::where('parent', 0)->get();
        $breadcrumb = ucfirst(Auth::user()->roles[0]->name)." / Category / New";
        return view('admin.category.create', compact('breadcrumb', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $category = Category::create([
            'name' => $request->name,
            'description' => $request->description,
            'parent' => $request->parent !== null ? $request->parent : 0,
        ]);
        if ($request->has('image')) {
            $image = $request->image;
            $imageName = $image->getClientOriginalName();
            $image->move(public_path('/uploads/images/categories/'),$imageName);
            $category->update(['image' => '/uploads/images/categories/'.$imageName]);
        }
        toastr()->success('Category Created Successfully');
        return redirect()->route('admin.categories.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $breadcrumb = ucfirst(Auth::user()->roles[0]->name)." / Category / Show";
        $id = (\request()->route()->parameter('id'));
        $category = Categories::where('id', $id)->first();
        return view('admin.category.show', compact('category', 'breadcrumb'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $breadcrumb = ucfirst(Auth::user()->roles[0]->name)." / Category / Edit";
        $categories = Category::where('parent', 0)->get();
        $category = Category::where('id', $id)->first();
        return view('admin.category.edit', compact('category', 'breadcrumb', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        Category::where('id', $id)->update([
            'name' => $request->name,
            'description' => $request->description,
            'parent' => $request->parent,
        ]);
        toastr()->success('Category Updated Successfully');
        return redirect()->route('admin.categories.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Category::where('id', $id)->delete();
        toastr()->success('Category Deleted Successfully');
        return redirect()->route('admin.categories.index');
    }
}
